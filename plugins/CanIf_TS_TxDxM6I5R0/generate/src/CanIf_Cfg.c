/**
 * \file
 *
 * \brief AUTOSAR CanIf
 *
 * This file contains the implementation of the AUTOSAR
 * module CanIf.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!CODE!]
[!AUTOSPACING!]


/*
 *  Possible Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 11.3 (advisory)
 *    A cast should not be performed between a pointer type and an integral
 *    type.
 *
 *    Reason:
 *    Implicit conversion of the NULL_PTR #define to other pointer types.
 *
 */

/*==================[inclusions]=============================================*/
[!INCLUDE "../include/CanIf_PreCompileMacros.m"!]

#define CANIF_NO_CFGCLASSMIX_REQUIRED
#define CANIF_NO_PBCFG_REQUIRED
#include <CanIf.h>                /* CanIf Types */
#include <CanIf_Int.h>

[!/* *** include transceiver drivers if necessary *** */!]
[!IF "CanIfPublicCfg/CanIfSingleCanTrcvAPIInfixEnable = 'true'"!]
  [!FOR "i"="0" TO "$trcvDriversNum - 1"!]
    [!VAR "trcvInfix"!][!CALL "CreateTrcvInfixByIdx", "idx"="$i"!][!ENDVAR!]
    [!IF "$trcvInfix = ''"!]
#include <CanTrcv.h>              /* AUTOSAR CanTrcv definitions */
    [!ELSE!]
#include <CanTrcv_[!"$trcvInfix"!].h>   /* AUTOSAR CanTrcv definitions */
    [!ENDIF!]
  [!ENDFOR!]
[!ELSE!]
#include <CanTrcv.h>              /* AUTOSAR CanTrcv definitions */
[!ENDIF!]

/* do not indirectly include PduR_SymbolicNames_PBcfg.h via PduR_CanIf.h */
#define PDUR_NO_PBCFG_REQUIRED

[!LOOP "text:split($ulCbFuncLUT)"!]
  [!VAR "tmpUpperLayerName"="text:split( ., ':' )[1]"!]
  [!IF "$tmpUpperLayerName = 'PDUR'"!]
#include <PduR_CanIf.h>            /* PduR callback function declarations */
  [!ENDIF!][!//
  [!IF "$tmpUpperLayerName = 'CAN_TP'"!]
#include <CanTp_Cbk.h>             /* CanTp callback function declarations */
  [!ENDIF!][!//
  [!IF "$tmpUpperLayerName = 'CAN_NM'"!]
#include <CanNm_Cbk.h>             /* CanNm callback function declarations */
  [!ENDIF!][!//
[!ENDLOOP!]

[!LOOP "CanIfPublicCfg/CanIfPublicCddHeaderFile/*"!][!//
#include <[!"."!]>   /* CDD header file */
[!ENDLOOP!][!//

/*==================[macros]=================================================*/

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

/*==================[internal constants]=====================================*/

/*==================[external constants]=====================================*/

#define CANIF_START_SEC_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

[!IF "CanIfPublicCfg/CanIfPublicTrcvSupport = 'true'"!]
/** \brief Configuration of CanTrcv function pointers */
CONST( CanIf_CanTrcvConfigType, CANIF_APPL_CONST )
  CanIf_CanTrcvConfig[[!"$trcvDriversNum"!]] =
{
[!FOR "i"="0" TO "$trcvDriversNum - 1"!]
  [!IF "CanIfPublicCfg/CanIfSingleCanTrcvAPIInfixEnable = 'true'"!]
    [!VAR "trcvInfix"!][!CALL "CreateTrcvInfixByIdx", "idx"="$i"!]_[!ENDVAR!]
  [!ELSE!]
    [!VAR "trcvInfix" = "''"!]
  [!ENDIF!]
  {
    &CanTrcv_[!"$trcvInfix"!]SetOpMode,
    &CanTrcv_[!"$trcvInfix"!]GetOpMode,
    &CanTrcv_[!"$trcvInfix"!]GetBusWuReason,
    &CanTrcv_[!"$trcvInfix"!]SetWakeupMode,
#if CANIF_CANTRCV_WAKEUP_SUPPORT == STD_ON
  [!SELECT "CanIfTrcvDrvCfg/*[position() = ($i+1)]"!]
  [!IF "count(CanIfTrcvCfg/*[CanIfTrcvWakeupSupport = 'true']) > 0"!]
    &CanTrcv_[!"$trcvInfix"!]CheckWakeup,
  [!ELSE!]
    NULL_PTR,
  [!ENDIF!]
  [!ENDSELECT!]
#endif /* CANIF_CANTRCV_WAKEUP_SUPPORT */
#if CANIF_PUBLIC_PN_SUPPORT == STD_ON
  [!VAR "CanTrcvPnSupport"!][!CALL "GetPnSupportByIdx", "idx"="$i"!][!ENDVAR!]
  [!IF "$CanTrcvPnSupport = 'true'"!]
    &CanTrcv_[!"$trcvInfix"!]ClearTrcvWufFlag,
    &CanTrcv_[!"$trcvInfix"!]CheckWakeFlag
  [!ELSE!]
    &CanIf_ClearTrcvWufFlag_HlpNoPn,
    &CanIf_CheckWakeFlag_HlpNoPn
  [!ENDIF!]
#endif /* CANIF_PUBLIC_PN_SUPPORT */
  },
[!ENDFOR!]
};
[!ENDIF!]


#if (CANIF_ANY_UL_CBK_SUPPORT == STD_ON)
/** \brief Configuration of upper layer callback function pointers */
CONST( CanIf_CbkFctPtrTblType, CANIF_APPL_CONST ) CanIf_CbkFctPtrTbl[] =
{
  [!LOOP "text:split($ulCbFuncLUT)"!]
  {
    [!VAR "tmpUpperLayerName"="text:split( ., ':' )[1]"!]
     /* [!"$tmpUpperLayerName"!] */
    [!VAR "tmpRxIndication"="text:split( ., ':' )[3]"!]
    [!VAR "tmpCddRxIndication"="text:split( ., ':' )[4]"!]
    [!VAR "tmpTxConfirmation"="text:split( ., ':' )[5]"!]
    [!VAR "tmpDlcFailedNofif"="text:split( ., ':' )[6]"!]
    [!VAR "tmpDlcPassedNofif"="text:split( ., ':' )[7]"!]
#if (CANIF_UL_RX_INDICATION_SUPPORT == STD_ON)
    [!"$tmpRxIndication"!],
#endif /* CANIF_UL_RX_INDICATION_SUPPORT == STD_ON */
#if (CANIF_CDD_RX_INDICATION_SUPPORT == STD_ON)
    [!"$tmpCddRxIndication"!],
#endif /* CANIF_CDD_RX_INDICATION_SUPPORT == STD_ON */
#if (CANIF_TX_CONFIRMATION_SUPPORT == STD_ON)
    [!"$tmpTxConfirmation"!],
#endif /* CANIF_TX_CONFIRMATION_SUPPORT == STD_ON */
#if (CANIF_DLC_ERROR_NOTIF_SUPPORT == STD_ON)
    [!"$tmpDlcFailedNofif"!],
#endif /* CANIF_DLC_ERROR_NOTIF_SUPPORT == STD_ON */
#if (CANIF_DLC_PASSED_NOTIF_SUPPORT == STD_ON)
    [!"$tmpDlcPassedNofif"!],
#endif  /* CANIF_DLC_PASSED_NOTIF_SUPPORT == STD_ON */
  },
  [!ENDLOOP!]
};
#endif /* CANIF_ANY_UL_CBK_SUPPORT == STD_ON */

#define CANIF_STOP_SEC_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

/*==================[external data]==========================================*/

[!SELECT "as:modconf('CanIf')[1]/CanIfPublicCfg"!]

#define CANIF_START_SEC_VAR_NO_INIT_8
#include <MemMap.h>

VAR( CanIf_ControllerModeType, CANIF_VAR )
  CanIf_CanControllerMode[[!"CanIfPublicMaxCtrl"!]U];

/** \brief PDU modes
 **
 ** This array contains the PDU mode of all configured controllers.
 */
VAR( CanIf_PduGetModeType, CANIF_VAR )
  CanIf_PduMode[[!"CanIfPublicMaxCtrl"!]U];

#if( ( CANIF_WAKEUP_VALIDATION_API == STD_ON ) || \
     ( CANIF_PUBLIC_TXCONFIRM_POLLING_SUPPORT == STD_ON )  || \
     ( CANIF_PUBLIC_PN_SUPPORT == STD_ON) )

/** \brief Controller flags
 **
 ** This array contains the wakeup flags of all configured
 ** controllers. If a bit/flag has the value 1 it is set.
 ** Bit masks are:
 ** CANIF_WAKEUP_MASK             Wakeup flag
 ** CANIF_WAKEUP_VALIDATION_MASK  Wakeup validation flag
 ** CANIF_TX_CONFIRMATION_MASK    Tx confirmation flag
 ** CANIF_PN_FILTER_ACTIVE_MASK   Partial networking filter flag
 */
VAR( uint8, CANIF_VAR )
  CanIf_CanControllerFlags[[!"CanIfPublicMaxCtrl"!]U];

#endif /* ( CANIF_WAKEUP_VALIDATION_API == STD_ON ) || \
          ( CANIF_PUBLIC_TXCONFIRM_POLLING_SUPPORT == STD_ON ) */

#if( CANIF_READTXPDU_NOTIF_STATUS_API == STD_ON )
/** \brief Bit-array of Tx notification flags (if enabled) */
VAR( uint8, CANIF_VAR )
  CanIf_TxLPduNotifyFlags[([!IF "node:exists(CanIfPublicMaxTxNotifyPdus)"!][!"CanIfPublicMaxTxNotifyPdus"!][!ELSE!]1[!ENDIF!]U+7U)/8U];
#endif /* CANIF_READTXPDU_NOTIF_STATUS_API == STD_ON */

#if( CANIF_READRXPDU_NOTIF_STATUS_API == STD_ON )
/** \brief Bit-array of Rx notification flags (if enabled) */
VAR( uint8, CANIF_VAR )
  CanIf_RxLPduNotifyFlags[([!IF "node:exists(CanIfPublicMaxRxNotifyPdus)"!][!"CanIfPublicMaxRxNotifyPdus"!][!ELSE!]1[!ENDIF!]U+7U)/8U];
#endif /* CANIF_READRXPDU_NOTIF_STATUS_API == STD_ON */

#if( CANIF_READRXPDU_DATA_API == STD_ON )
/** \brief Array of Rx buffers */
VAR( uint8, CANIF_VAR ) CanIf_RxBuffer[[!IF "node:exists(CanIfPublicMaxRxBuffer)"!][!"CanIfPublicMaxRxBuffer"!][!ELSE!]1[!ENDIF!]U];
#endif /* CANIF_READRXPDU_NOTIF_STATUS_API == STD_ON */

#define CANIF_STOP_SEC_VAR_NO_INIT_8
#include <MemMap.h>

#define CANIF_START_SEC_VAR_NO_INIT_16
#include <MemMap.h>

#if CANIF_PUBLIC_TX_BUFFERING == STD_ON
/** \brief Array of a HTH's number of pending Tx requests */
VAR( CanIf_TxBufferIndexType, CANIF_VAR )
  CanIf_NrOfPendingTxRequests[[!IF "node:exists(CanIfPublicMaxHths)"!][!"CanIfPublicMaxHths"!][!ELSE!]1[!ENDIF!]U];

/** \brief Pending transmit request buffers
 **
 ** This array stores pending transmit request IDs. It is named as array (8)
 ** in the CAN interface design.
 */
VAR( CanIf_LPduIndexType, CANIF_VAR )
  CanIf_PendingTxBuffers[[!IF "node:exists(CanIfPublicMaxTxBuffers)"!][!"CanIfPublicMaxTxBuffers"!][!ELSE!]1[!ENDIF!]U];
#endif /* CANIF_PUBLIC_TX_BUFFERING */

#define CANIF_STOP_SEC_VAR_NO_INIT_16
#include <MemMap.h>

#define CANIF_START_SEC_VAR_NO_INIT_32
#include <MemMap.h>

#if( CANIF_SETDYNAMICTXID_API == STD_ON )
/** \brief Array for dynamic CAN IDs
 **
 ** This array contains the current set CAN ID for dynamic Tx L-PDUs.
 */
 VAR( uint32, CANIF_VAR )
  CanIf_DynamicTxLPduCanIds[[!IF "node:exists(CanIfPublicMaxDynTxPdus)"!][!"CanIfPublicMaxDynTxPdus"!][!ELSE!]1[!ENDIF!]U];
#endif /* CANIF_SETDYNAMICTXID_API == STD_ON */

#define CANIF_STOP_SEC_VAR_NO_INIT_32
#include <MemMap.h>

#define CANIF_START_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>

#if CANIF_PUBLIC_TX_BUFFERING == STD_ON
/** \brief Array of Tx buffers
 **
 ** This is the array of Tx buffers used during message transmission if Tx
 ** buffering is enabled.
 */
VAR( CanIf_TxBufferType, CANIF_VAR )
  CanIf_TxBuffer[[!IF "node:exists(CanIfPublicMaxTxBuffers)"!][!"CanIfPublicMaxTxBuffers"!][!ELSE!]1[!ENDIF!]U];
#endif /* CANIF_PUBLIC_TX_BUFFERING */

#define CANIF_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>

[!ENDSELECT!]

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

/*==================[end of file]============================================*/
[!ENDCODE!][!//
