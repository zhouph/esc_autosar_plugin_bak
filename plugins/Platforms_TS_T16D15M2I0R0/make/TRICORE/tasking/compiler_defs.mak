# \file
#
# \brief AUTOSAR Platforms
#
# This file contains the implementation of the AUTOSAR
# module Platforms.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# CONFIGURATION (in tricore_tasking_cfg.mak)
#
# TOOLPATH_COMPILER (required)
# ASM_OPT           (required)
# CPP_OPT           (required)
# CC_OPT            (required)
# LC_OPT            (required)
# AR_OPT            (required)
# ASS_OPT           (required)
# IEEE_OPT          (optional)
# HEX_OPT           (optional)
# DEBUG_OUTPUT      (optional)
# CREATE_PREPROCESSOR_FILE   (optional)

#################################################################
# REQUIRED DEFINITIONS
#
#################################################################
# Each <architecture>_<compiler>_defs.mak file must
# define a list of file suffixes. These suffixes are used in the
# core part to map the basic makefile interface to the compiler
# makefile interface.
# All suffixes start with the suffix name followed by the string
# _FILE_SUFFIX. The suffixes are compiler specific and must be
# therefore defined in the compiler *_defs file.

OBJ_FILE_SUFFIX   =o
OBJ_FILE_SUFFIX_2 =
LIB_FILE_SUFFIX   =a
ASM_FILE_SUFFIX   =s
ASM_FILE_SUFFIX_2 =asm
CC_FILE_SUFFIX    =c
CC_FILE_SUFFIX_2  =C
CPP_FILE_SUFFIX   =cpp
CPP_FILE_SUFFIX_2 =CPP
H_FILE_SUFFIX     =h
H_FILE_SUFFIX_2   =
PRE_FILE_SUFFIX   =pre
ABS_FILE_SUFFIX   =elf
HEX_FILE_SUFFIX   =mot
MAP_FILE_SUFFIX   =map
INC_FILE_SUFFIX   =inc
INA_FILE_SUFFIX   =ina

#################################################################
# Besides the suffix definition a prefix must be defined for all included directories
# and for all preprocessor macros.
#
INCLUDE_PREFIX    =-I
MACRO_PREFIX      =-D


#################################################################
# REGISTRY

#################################################################
# The basic makefile interface provides some global variables
# which allows the registration of resources.


#################################################################
# Each basic software package has a name. This name shall be
# registered in the global variable SSC_PLUGINS.
SSC_PLUGINS          += tricore_tasking

#################################################################
# Each Public Makefile for a plugin should define
# a variable that contains a list of the names of all other required
# plugins. For this purpose the name of the package
# followed by the suffix _DEPENDENT_PLUGINS shall be used.
#
tricore_tasking_DEPENDENT_PlUGINS = base_make

#################################################################
# The suffixes _VERSION and _DESCRIPTION are additional and
# have an informative character.
#
tricore_tasking_VERSION           = 1.00.01
tricore_tasking_DESCRIPTION       = Compiler PlugIn for Tasking TriCore


#################################################################
# All rules defined in a plugin shall be registerd in one global
# variable. This allows to list all available rules.
MAKE_CLEAN_RULES+=compiler_clean

#################################################################
# For each rule a short description shall be defined by the help
# of the _DESCRIPTION suffix.
compiler_clean_DESCRIPTION  = -	-clean all compiler specific files *.o, *.src ...


#################################################################
# The next three variables allows the registration of directories
# which shall be used as include path.
CC_INCLUDE_PATH  +=
CPP_INCLUDE_PATH +=
ASM_INCLUDE_PATH +=


#################################################################
# The variable FIRST_BUILD_TARGET is used as a entry in the dependency
# section of the BUILD_ALL rule. This means the last output file
# of the compiler (*.hex, *.elf or *.out) must be defined as value.
# The file <architecture>_<compiler>_rules.mak must contain a rule
# to build this file.
FIRST_BUILD_TARGET = $(BIN_OUTPUT_PATH)\$(PROJECT).elf

#DEPEND_GCC_OPTS +=-D__TASKING__


#################################################################
# This macro returns a list of all macros e.g.: -DXXX -DXXX.
# The abstract definition of macros in PREPROCESSOR_DEFINES will
# be transformed in the compiler specific macro definition.
GET_PREPROCESSOR_DEFINES = $(foreach DEF,$(PREPROCESSOR_DEFINES),$(MACRO_PREFIX)$($(DEF)_KEY)=$($(DEF)_VALUE))


#################################################################
# OPTIONAL DEFINITIONS
#
#################################################################
# Define variables specific for tasking tools.
# they are needed by some included make files


#################################################################
# Name of the toolchain - C compiler and assembler
CC         = $(TOOLPATH_COMPILER)\bin\cctc


#################################################################
# Name of the assembler
ASM        = $(TOOLPATH_COMPILER)\bin\astc

#################################################################
# Name of the linker
LINK       = $(TOOLPATH_COMPILER)\bin\ltc


#################################################################
# Name of the preprocessor and the c compiler
CPRE       = $(TOOLPATH_COMPILER)\bin\ctc


#################################################################
# Name of the archiver
LIB        = $(TOOLPATH_COMPILER)\bin\artc

######### TOOLCHAIN ##############
TOOLCHAIN  = tasking

#################################################################
# The macro GET_LOC_FILE expands to the name of the linker/locator
# file.
GET_LOC_FILE = $(if $(EXT_LOCATOR_FILE),$(EXT_LOCATOR_FILE),$(LOC_FILE))