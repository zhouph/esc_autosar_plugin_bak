/**
 * \file
 *
 * \brief AUTOSAR CanSM
 *
 * This file contains the implementation of the AUTOSAR
 * module CanSM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* set macro that enables merged compilation macros */
#define TS_MERGED_COMPILE STD_ON

/* First of all include all header files, the C-files depend on.
 * This prevent us for overlapping memory sections because all included files are protected for
 * further inclusion. */
#include <TSAutosar.h>        /* EB specific standard types */
#include <ComStack_Types.h>   /* AUTOSAR communication stack types */
#include <TSAtomic_Assign.h>  /* Atomic assignment macros */
#include "CanSM_Internal.h"   /* Module private API */
#include "CanSM.h"            /* Module public API */
#include "CanSM_Int_Stc.h"    /* Module internal static header */
#include "CanSM_Cfg.h"        /* Module configuration */
#include "CanSM_InternalCfg.h"  /* Module generated internal configuration */
#include "CanSM_Cbk.h"        /* Module callback API */
#include "CanSM_ComM.h"       /* Module ComM API */
#include "CanSM_SchM.h"       /* Module SchM API */
#include "CanSM_TxTimeoutException.h" /* Module CanNm API */
#include "CanSM_Version.h"    /* Version information */
#include "CanSM_Api.h"        /* Module public API */
#include "CanSM_Api_Static.h" /* Module public API */
#include <CanIf.h>            /* access CAN controllers & transceiver drivers */
#include <ComM.h>             /* Communication Manager's types */
#include <ComM_BusSm.h>       /* ComM BusSm API */
#if ( CANSM_PROD_ERR_HANDLING_BUS_OFF == TS_PROD_ERR_REP_TO_DEM )
#include <Dem.h>              /* Diagnostic Event Manager */
#endif /* ( CANSM_PROD_ERR_HANDLING_BUS_OFF == TS_PROD_ERR_REP_TO_DEM ) */
#if (CANSM_DEV_ERROR_DETECT == STD_ON)
#include <Det.h>              /* Development Error Tracer */
#endif


/* All source files which contains sections other than *_SEC_CODE (or no sections) have to be
 * included before the memory mapping in this file to protect them for overlapping memory
 * sections. */
#include "CanSM.c"

/* list of files that include only memory abstraction CODE segments */
/* start code section declaration */
#define CANSM_START_SEC_CODE
#include <MemMap.h>

#include "CanSM_Cbk.c"
#include "CanSM_GetCurrentComMode.c"
#include "CanSM_GetVersionInfo.c"
#include "CanSM_Init.c"
#include "CanSM_MainFunction.c"
#include "CanSM_RequestComMode.c"
#include "CanSM_StateMachine.c"

/* stop code section declaration */
#define CANSM_STOP_SEC_CODE
#include <MemMap.h>

