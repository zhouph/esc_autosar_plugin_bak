[!/**
 * \file
 *
 * \brief AUTOSAR CanSM
 *
 * This file contains the implementation of the AUTOSAR
 * module CanSM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//
[!NOCODE!]
[!IF "not(var:defined('CANSM_MACROS_M'))"!]
[!VAR "CANSM_MACROS_M"="'true'"!]

[!VAR "numNetworks"="num:i(count(CanSMConfiguration/*[1]/CanSMManagerNetwork/*))"!]

[!IF "$numNetworks > 1"!]
  [!/* check if network handles are consecutive */!]
  [!VAR "networkHandles"="''"!]
  [!LOOP "node:order(CanSMConfiguration/*[1]/CanSMManagerNetwork/*,'as:ref(CanSMComMNetworkHandleRef)/ComMChannelId')"!]
    [!VAR "networkHandles"="concat($networkHandles,' ',num:i(as:ref(CanSMComMNetworkHandleRef)/ComMChannelId))"!]
  [!ENDLOOP!]
  [!/* snip leading space */!]
  [!VAR "networkHandles"="substring($networkHandles,2)"!]
  [!/* now check */!]
  [!VAR "nwHandlesConsecutive"="'true'"!]
  [!VAR "x"="text:split($networkHandles)[1]"!]
  [!VAR "nwHandleOffset"="$x"!]
  [!LOOP "text:split($networkHandles)[position()>1]"!]
    [!IF ". != ($x + 1)"!]
      [!VAR "nwHandlesConsecutive"="'false'"!]
    [!ENDIF!]
    [!VAR "x"="num:i(.)"!]
  [!ENDLOOP!]
[!ENDIF!]

[!ENDIF!]
[!ENDNOCODE!][!//
