/**
 * \file
 *
 * \brief AUTOSAR CanSM
 *
 * This file contains the implementation of the AUTOSAR
 * module CanSM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!INCLUDE "CanSM_Macros.m"!][!//
#if (!defined CANSM_CFG_H)
#define CANSM_CFG_H
/*==================[includes]==============================================*/

#include <ComStack_Types.h>         /* AUTOSAR communication stack types */

#include <CanSM_Int_Stc.h>          /* Module internal static header */
#include <CanSM_Api_Static.h>       /* CanSM_ConfigType */
#include <CanSM_Det_Cfg.h>          /* CANSM_DEV_ERROR_DETECT */

#include <CanIf.h>                  /* CAN Interface */
[!IF "$useDEM = 'true'"!]
#include <Dem.h>                    /* AUTOSAR Dem */
[!ENDIF!]

/*==================[macros]================================================*/




/*------------------[Defensive programming]---------------------------------*/
[!SELECT "CanSMDefensiveProgramming"!][!//

#if (defined CANSM_DEFENSIVE_PROGRAMMING_ENABLED)
#error CANSM_DEFENSIVE_PROGRAMMING_ENABLED is already defined
#endif
/** \brief Defensive programming usage
 **
 ** En- or disables the usage of the defensive programming */
#define CANSM_DEFENSIVE_PROGRAMMING_ENABLED   [!//
[!IF "(../CanSMGeneral/CanSMDevErrorDetect  = 'true') and (CanSMDefProgEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined CANSM_PRECONDITION_ASSERT_ENABLED)
#error CANSM_PRECONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Precondition assertion usage
 **
 ** En- or disables the usage of precondition assertion checks */
#define CANSM_PRECONDITION_ASSERT_ENABLED     [!//
[!IF "(../CanSMGeneral/CanSMDevErrorDetect  = 'true') and (CanSMDefProgEnabled = 'true') and (CanSMPrecondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined CANSM_POSTCONDITION_ASSERT_ENABLED)
#error CANSM_POSTCONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Postcondition assertion usage
 **
 ** En- or disables the usage of postcondition assertion checks */
#define CANSM_POSTCONDITION_ASSERT_ENABLED    [!//
[!IF "(../CanSMGeneral/CanSMDevErrorDetect  = 'true') and (CanSMDefProgEnabled = 'true') and (CanSMPostcondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined CANSM_UNREACHABLE_CODE_ASSERT_ENABLED)
#error CANSM_UNREACHABLE_CODE_ASSERT_ENABLED is already defined
#endif
/** \brief Unreachable code assertion usage
 **
 ** En- or disables the usage of unreachable code assertion checks */
#define CANSM_UNREACHABLE_CODE_ASSERT_ENABLED [!//
[!IF "(../CanSMGeneral/CanSMDevErrorDetect  = 'true') and (CanSMDefProgEnabled = 'true') and (CanSMUnreachAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined CANSM_INVARIANT_ASSERT_ENABLED)
#error CANSM_INVARIANT_ASSERT_ENABLED is already defined
#endif
/** \brief Invariant assertion usage
 **
 ** En- or disables the usage of invariant assertion checks */
#define CANSM_INVARIANT_ASSERT_ENABLED        [!//
[!IF "(../CanSMGeneral/CanSMDevErrorDetect  = 'true') and (CanSMDefProgEnabled = 'true') and (CanSMInvariantAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined CANSM_STATIC_ASSERT_ENABLED)
#error CANSM_STATIC_ASSERT_ENABLED is already defined
#endif
/** \brief Static assertion usage
 **
 ** En- or disables the usage of static assertion checks */
#define CANSM_STATIC_ASSERT_ENABLED           [!//
[!IF "(../CanSMGeneral/CanSMDevErrorDetect  = 'true') and (CanSMDefProgEnabled = 'true') and (CanSMStaticAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!ENDSELECT!][!//


[!SELECT "as:modconf('CanSM')[1]"!][!//
[!LOOP "CanSMConfiguration/*[1]/CanSMManagerNetwork/*"!]

/** \brief Export symbolic name value */
#define CanSM_[!"@name"!] [!"as:ref(CanSMComMNetworkHandleRef)/ComMChannelId"!]U

#if (!defined CANSM_NO_SYMBOLS_WITHOUT_PREFIX)
/** \brief Export symbolic name value without prefix */
#define [!"@name"!] [!"as:ref(CanSMComMNetworkHandleRef)/ComMChannelId"!]U
#endif /* !defined CANSM_NO_SYMBOLS_WITHOUT_PREFIX */
[!ENDLOOP!]

/** \brief Switch for DEM to DET reporting */
#define CANSM_PROD_ERR_HANDLING_BUS_OFF   [!//
[!IF "$useDEM = 'true'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/CanSMBusOffReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

[!IF "ReportToDem/CanSMBusOffReportToDem = 'DET'"!][!//
/** \brief Det error ID, if DEM to DET reporting is enabled */
#define CANSM_E_DEMTODET_BUS_OFF          [!"ReportToDem/CanSMBusOffReportToDemDetErrorId"!]U
[!ENDIF!][!//

[!ENDSELECT!][!//
/** \brief Switch for version info API */
[!IF "as:modconf('CanSM')[1]/CanSMGeneral/CanSMVersionInfoApi = 'true'"!][!//
#define CANSM_VERSION_INFO_API STD_ON
[!ELSE!][!//
#define CANSM_VERSION_INFO_API STD_OFF
[!ENDIF!][!//

/** \brief Switch enhanced bus-off reporting to BswM on or off */
[!IF "as:modconf('CanSM')[1]/CanSMGeneral/CanSMEnhancedBusOffReporting = 'true'"!][!//
#define CANSM_ENHANCED_BUSOFF_REPORTING STD_ON
[!ELSE!][!//
#define CANSM_ENHANCED_BUSOFF_REPORTING STD_OFF
[!ENDIF!][!//

/** \brief Partial Networking Support */
#define CANSM_PNSUPPORT [!IF "$globalPnSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Maximum amount of mode change request repetitions */
#define CANSM_MODEREQ_MAX [!"CanSMConfiguration/*[1]/CanSMModeRequestRepetitionMax"!]U

/** \brief Time duration after which a mode change request is repeated */
#define CANSM_MODEREQ_REPEAT_TIME [!CALL "CalcToTicks", "time"="CanSMConfiguration/*[1]/CanSMModeRequestRepetitionTime", "maxTicks"="65534"!]U

/** \brief use transceiver driver calls or not */
#define CANSM_USE_TRANSCEIVER [!IF "$useTransceiver = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Transceiver resources present in code */
#define CANSM_TRCV_RESOURCES [!IF "$usePnSupportOrTransceiver = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]


[!IF "$useBorTimeTxEnsured = 'true'"!][!//
/** \brief Duration until bus-off free communication is assumed */
[!IF "$nwSpecificBorTimeTxEnsured = 'true'"!][!//
#define CANSM_BOR_TIME_TX_ENSURED(nw) (CanSM_NetworkConfig[(nw)].BorTimeTxEnsured)
[!ELSE!][!//
[!/* num:max() has the purpose to select a single value out of the identical values */!][!//
#define CANSM_BOR_TIME_TX_ENSURED(nw) [!CALL "CalcToTicks", "time"="num:max(CanSMConfiguration/*[1]/CanSMManagerNetwork/*[CanSMBorTxConfirmationPolling = 'false']/CanSMBorTimeTxEnsured)", "maxTicks"="65534"!]U
[!// maxTicks covers CanSM.Design.20092
[!ENDIF!][!//
[!ENDIF!][!//

/** \brief Duration to stay in level 1 check before restart of communication */
[!IF "$nwSpecificBorTimeL1 = 'true'"!][!//
#define CANSM_BOR_TIME_L1(nw) (CanSM_NetworkConfig[(nw)].BorTimeL1)
[!ELSE!][!//
#define CANSM_BOR_TIME_L1(nw) [!CALL "CalcToTicks", "time"="CanSMConfiguration/*[1]/CanSMManagerNetwork/*[1]/CanSMBorTimeL1", "maxTicks"="65535"!]U
[!// maxTicks covers CanSM.Design.20092
[!ENDIF!][!//

/** \brief Duration to stay in level 2 check before restart of communication */
[!IF "$nwSpecificBorTimeL2 = 'true'"!][!//
#define CANSM_BOR_TIME_L2(nw) (CanSM_NetworkConfig[(nw)].BorTimeL2)
[!ELSE!][!//
#define CANSM_BOR_TIME_L2(nw) [!CALL "CalcToTicks", "time"="CanSMConfiguration/*[1]/CanSMManagerNetwork/*[1]/CanSMBorTimeL2", "maxTicks"="65535"!]U
[!// maxTicks covers CanSM.Design.20092
[!ENDIF!][!//

/** \brief bus-off recovery L1 to L2 Threshold */
[!IF "$nwSpecificBorCounterL1ToL2 = 'true'"!][!//
#define CANSM_BOR_COUNTER_L1_TO_L2(nw) (CanSM_NetworkConfig[(nw)].BorCounterL1ToL2)
[!ELSE!][!//
#define CANSM_BOR_COUNTER_L1_TO_L2(nw) [!"CanSMConfiguration/*[1]/CanSMManagerNetwork/*[1]/CanSMBorCounterL1ToL2"!]U
[!ENDIF!][!//

/** \brief BOR Tx Confirmation Polling */
[!IF "$nwSpecificBorTxConfirmationPolling = 'true'"!][!//
#define CANSM_BOR_TX_CONFIRMATION_POLLING(nw) (CanSM_NetworkConfig[(nw)].BorTxConfirmationPolling)
[!ELSE!][!//
#define CANSM_BOR_TX_CONFIRMATION_POLLING(nw) [!//
[!IF "CanSMConfiguration/*[1]/CanSMManagerNetwork/*[1]/CanSMBorTxConfirmationPolling = 'true'"!][!//
TRUE
[!ELSE!][!//
FALSE
[!ENDIF!][!//
[!ENDIF!][!//

[!IF "$numNetworks > 1"!][!//
/** \brief Partial networking */
#define CANSM_PARTIALNETWORKING(nw) (CanSM_NetworkConfig[(nw)].PartialNetworking)
[!ELSE!][!//
/** \brief Partial networking */
#define CANSM_PARTIALNETWORKING(nw) [!IF "CanSMConfiguration/*[1]/CanSMManagerNetwork/*[1]/CanSMActivatePN = 'true'"!]TRUE[!ELSE!]FALSE[!ENDIF!][!//
[!ENDIF!]

/** \brief Use CanIf_GetTxConfirmationState API */
#define CANSM_USE_CANIF_GETTXCONFIRMATIONSTATE [!//
[!IF "($nwSpecificBorTxConfirmationPolling = 'true') or (CanSMConfiguration/*[1]/CanSMManagerNetwork/*[1]/CanSMBorTxConfirmationPolling = 'true')"!][!//
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!][!//

/** \brief Use CanSMBorTimeTxEnsured for bus-off check */
#define CANSM_USE_BOR_TIME_TX_ENSURED [!IF "$useBorTimeTxEnsured = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief number of networks used */
#define CANSM_NETWORKS_NUM [!"$numNetworks"!]U

/** \brief number of controllers used */
#define CANSM_CONTROLLERS_NUM [!"$numControllers"!]U

/** \brief highest controller id used + 1 */
#define CANSM_CONTROLLERS_ID_NUM [!"num:i($ctrlMax + 1)"!]U

/** \brief special value to indicate that CanSM_ControllerModeIndication was not invoked
 *  with a meaningful value since last reset
 */
#define CANSM_CTRLMODETYPE_RESET 0U

/** \brief CanSM_ControllerModeIndication was last invoked with CANIF_CS_SLEEP */
#define CANSM_CTRLMODETYPE_SLEEP 1U

/** \brief CanSM_ControllerModeIndication was last invoked with CANIF_CS_STARTED */
#define CANSM_CTRLMODETYPE_STARTED 2U

/** \brief CanSM_ControllerModeIndication was last invoked with CANIF_CS_STOPPED */
#define CANSM_CTRLMODETYPE_STOPPED 3U

/** \brief number of transceivers used */
#define CANSM_TRANSCEIVERS_NUM [!"$numTransceivers"!]U

[!IF "$usePnSupportOrTransceiver = 'true'"!]

/** \brief highest transceiver id used + 1 */
#define CANSM_TRANSCEIVERS_ID_NUM [!"num:i($trcvMax + 1)"!]U

/** \brief CanSM_TransceiverModeIndication was last invoked with CANTRCV_TRCVMODE_NORMAL */
#define CANSM_TRCVMODETYPE_NORMAL 0U

/** \brief special value to indicate that CanSM_TransceiverModeIndication was not
 *  invoked with a meaningful value since last reset
 */
#define CANSM_TRCVMODETYPE_RESET 1U

/** \brief CanSM_TransceiverModeIndication was last invoked with CANTRCV_TRCVMODE_STANDBY */
#define CANSM_TRCVMODETYPE_STANDBY 2U

[!ENDIF!]

[!IF "$numNetworks = 1"!]
[!SELECT "CanSMConfiguration/*[1]/CanSMManagerNetwork/*[1]"!]

[!IF "$usePnSupportOrTransceiver = 'true'"!]
/** \brief Transceiver id for given network */
[!IF "node:exists(CanSMTransceiverId)"!]
#define CANSM_NW_CONFIG_TRCV_ID(nw) [!"num:i(as:ref(CanSMTransceiverId)/CanIfTrcvId)"!]U
[!ELSE!]
#define CANSM_NW_CONFIG_TRCV_ID(nw) CANSM_TRANSCEIVER_ID_NONE
[!ENDIF!]
[!ENDIF!]

[!IF "$useDEM = 'true'"!]
/** \brief DEM Event to report bus off error for given network */
#define CANSM_NW_CONFIG_DEM_EVENT_ID_BUS_OFF(nw) [!//
[!IF "node:exists(CanSMDemEventParameterRefs/CANSM_E_BUS_OFF)"!][!//
[!"node:value(as:ref(CanSMDemEventParameterRefs/CANSM_E_BUS_OFF)/DemEventId)"!]U
[!ELSE!][!//
CANSM_INVALID_DEM_EVENTID /* Disabled for this network */
[!ENDIF!][!//
[!ENDIF!]

/** \brief check if network handle is valid */
#define CANSM_NW_HANDLE_IS_VALID(nw) ((nw) == [!"num:i(as:ref(CanSMComMNetworkHandleRef)/ComMChannelId)"!]U)

/** \brief convert network array index to handle */
#define CANSM_NW_HANDLE_FROM_IDX(nw) ((NetworkHandleType)[!"num:i(as:ref(CanSMComMNetworkHandleRef)/ComMChannelId)"!]U)

[!ENDSELECT!]

/** \brief Index of first controller of this network in controller list */
#define CANSM_NW_CONFIG_CTRL_START(nw) 0U

/** \brief Number of controllers in this network */
#define CANSM_NW_CONFIG_CTRL_NUM(nw) [!"$numControllers"!]U

/** \brief access to bor state machine timer of network \a nw */
#define CANSM_NW_INFO_TIMER(nw) (CanSM_NetworkInfo.BoTimer)

/** \brief access to bus-off counter of network \a nw */
#define CANSM_NW_INFO_BOR_COUNTER(nw) (CanSM_NetworkInfo.BoCounter)

/** \brief access to current state of network \a nw */
#define CANSM_NW_INFO_STATE(nw) (CanSM_NetworkInfo.NwState)

/** \brief access to bus-off event flag of network \a nw */
#define CANSM_NW_INFO_BUSOFF(nw) (CanSM_NetworkInfo.BusOffEvent)

/** \brief access to mode change request repetitions timer of network \a nw */
#define CANSM_NW_INFO_MODE_REQUEST_REPETITION_TIMER(nw) (CanSM_NetworkInfo.ModeReqRepetitionTimer)

#if (CANSM_PNSUPPORT == STD_ON)
/** \brief access to requested State member of network \a nw */
#define CANSM_NW_INFO_REQ_STATE(nw) (CanSM_NetworkInfo.reqState)
#endif /* CANSM_PNSUPPORT == STD_ON */

/** \brief access to current internal state of network \a nw */
#define CANSM_NW_INFO_INTERNALSTATE(nw) (CanSM_NetworkInfo.InternalState)

[!IF "$usePnSupportOrTransceiver = 'true'"!]
/** \brief access to last transceiver mode indication of network \a nw */
#define CANSM_NW_INFO_TRCVMODEINDICATION(nw) (CanSM_NetworkInfo.TrcvModeIndication)
[!ENDIF!]

[!IF "as:modconf('CanSM')[1]/CanSMGeneral/CanSMPNSupport = 'true'"!]
/** \brief access to last CanSM_ClearTrcvWufFlagIndication of network \a nw */
#define CANSM_NW_INFO_TRCVCLEARWUFINDICATION(nw) (CanSM_NetworkInfo.TrcvClearWUFIndication)

/** \brief access to last CanSM_CheckTransceiverWakeFlagIndication of network \a nw */
#define CANSM_NW_INFO_TRCVCHECKWAKEFLAGINDICATION(nw) (CanSM_NetworkInfo.TrcvCheckWakeFlagIndication)
[!ENDIF!]


/** \brief access to mode change request repetitions counter of network \a nw */
#define CANSM_NW_INFO_MODE_REQUEST_REPETITION_COUNTER(nw) (CanSM_NetworkInfo.ModeReqRepetitionCounter)

/** \brief convert network handle to array index */
#define CANSM_NW_IDX_FROM_HANDLE(nw) 0U

[!ELSE!] [!/* IF "$numNetworks = 1" */!]
[!IF "$usePnSupportOrTransceiver = 'true'"!]
/** \brief Transceiver id for given network */
#define CANSM_NW_CONFIG_TRCV_ID(nw) (CanSM_NetworkConfig[(nw)].TrcvId)
[!ENDIF!]

[!IF "$useDEM = 'true'"!]
/** \brief DEM Event to report bus off error for given network */
#define CANSM_NW_CONFIG_DEM_EVENT_ID_BUS_OFF(nw) (CanSM_NetworkConfig[(nw)].EventIdBusOff)
[!ENDIF!]

[!IF "$singleControllers = 'true'"!]
/** \brief Index of controller of this network */
#define CANSM_NW_CONFIG_CTRL_START(nw) (nw)

/** \brief Number of controllers in this network */
#define CANSM_NW_CONFIG_CTRL_NUM(nw) 1U
[!ELSE!]
/** \brief Index of first controller of this network in controller list */
#define CANSM_NW_CONFIG_CTRL_START(nw) (CanSM_CanNetworkControllersStart[(nw)])

/** \brief Number of controllers in this network */
#define CANSM_NW_CONFIG_CTRL_NUM(nw) (CanSM_CanNetworkControllersNum[(nw)])
[!ENDIF!]

/** \brief access to bor state machine timer of network \a nw */
#define CANSM_NW_INFO_TIMER(nw) (CanSM_NetworkInfo[(nw)].BoTimer)

/** \brief access to bus-off counter of network \a nw */
#define CANSM_NW_INFO_BOR_COUNTER(nw) (CanSM_NetworkInfo[(nw)].BoCounter)

/** \brief access to current state of network \a nw */
#define CANSM_NW_INFO_STATE(nw) (CanSM_NetworkInfo[(nw)].NwState)

/** \brief access to bus-off event flag of network \a nw */
#define CANSM_NW_INFO_BUSOFF(nw) (CanSM_NetworkInfo[(nw)].BusOffEvent)

/** \brief access to current internal state of network \a nw */
#define CANSM_NW_INFO_INTERNALSTATE(nw) (CanSM_NetworkInfo[(nw)].InternalState)

/** \brief access to mode change request repetitions timer of network \a nw */
#define CANSM_NW_INFO_MODE_REQUEST_REPETITION_TIMER(nw) (CanSM_NetworkInfo[(nw)].ModeReqRepetitionTimer)

#if (CANSM_PNSUPPORT == STD_ON)
/** \brief access to requested State member of network \a nw */
#define CANSM_NW_INFO_REQ_STATE(nw) (CanSM_NetworkInfo[(nw)].reqState)
#endif /* CANSM_PNSUPPORT == STD_ON */

[!IF "$usePnSupportOrTransceiver = 'true'"!]
/** \brief access to last transceiver mode indication of network \a nw */
#define CANSM_NW_INFO_TRCVMODEINDICATION(nw) (CanSM_NetworkInfo[(nw)].TrcvModeIndication)
[!ENDIF!]

[!IF "as:modconf('CanSM')[1]/CanSMGeneral/CanSMPNSupport = 'true'"!]
/** \brief access to last CanSM_ClearTrcvWufFlagIndication of network \a nw */
#define CANSM_NW_INFO_TRCVCLEARWUFINDICATION(nw) (CanSM_NetworkInfo[(nw)].TrcvClearWUFIndication)

/** \brief access to last CanSM_CheckTransceiverWakeFlagIndication of network \a nw */
#define CANSM_NW_INFO_TRCVCHECKWAKEFLAGINDICATION(nw) (CanSM_NetworkInfo[(nw)].TrcvCheckWakeFlagIndication)
[!ENDIF!]

/** \brief access to mode change request repetitions counter of network \a nw */
#define CANSM_NW_INFO_MODE_REQUEST_REPETITION_COUNTER(nw) (CanSM_NetworkInfo[(nw)].ModeReqRepetitionCounter)

[!IF "$nwHandlesConsecutive = 'true'"!]
[!IF "$nwHandleOffset > 0"!]
/** \brief check if network handle is valid */
#define CANSM_NW_HANDLE_IS_VALID(nw) (((nw) >= [!"$nwHandleOffset"!]U) && ((nw) < [!"num:i($nwHandleOffset + $numNetworks)"!]U))

/** \brief convert network handle to array index */
#define CANSM_NW_IDX_FROM_HANDLE(nw) ((nw) - [!"$nwHandleOffset"!]U)

/** \brief convert network array index to handle */
#define CANSM_NW_HANDLE_FROM_IDX(nw) ((NetworkHandleType)((nw) + [!"$nwHandleOffset"!]U))
[!ELSE!]
/** \brief check if network handle is valid */
#define CANSM_NW_HANDLE_IS_VALID(nw) ((nw) < [!"num:i($nwHandleOffset + $numNetworks)"!]U)

/** \brief convert network handle to array index */
#define CANSM_NW_IDX_FROM_HANDLE(nw) (nw)

/** \brief convert network array index to handle */
#define CANSM_NW_HANDLE_FROM_IDX(nw) ((NetworkHandleType)(nw))
[!ENDIF!][!/* IF "$nwHandleOffset > 0" */!]
[!ELSE!]
/** \brief check if network handle is valid */
#define CANSM_NW_HANDLE_IS_VALID(nw) (CanSM_NetworkHandleIsValid(nw))

/** \brief convert network handle to array index */
#define CANSM_NW_IDX_FROM_HANDLE(nw) (CanSM_GetNetworkIndexFromHandle(nw))

/** \brief convert network array index to handle */
#define CANSM_NW_HANDLE_FROM_IDX(nw) (CanSM_GetNetworkHandleFromIndex(nw))
[!ENDIF!]
[!ENDIF!] [!/* IF "$numNetworks = 1" */!]

[!IF "$singleControllers = 'true'"!]
  [!IF "$numNetworks = 1"!]
/** \brief Get controller \a ctrl from controller list */
#define CANSM_NW_CONFIG_CTRL(ctrl) [!"num:i(as:ref(CanSMConfiguration/*[1]/CanSMManagerNetwork/*[1]/CanSMController/*[1]/CanSMControllerId)/CanIfCtrlId)"!]U
  [!ELSE!]
/** \brief Get controller \a ctrl from controller list */
#define CANSM_NW_CONFIG_CTRL(ctrl) (CanSM_NetworkConfig[(ctrl)].ControllerId)
  [!ENDIF!]
[!ELSE!]
/** \brief Get controller \a ctrl from controller list */
#define CANSM_NW_CONFIG_CTRL(ctrl) (CanSM_CanNetworkControllersList[(ctrl)])
[!ENDIF!]

/** \brief Convert Controller id to array index for internal network data
 *
 * \param[in] ctrl Controller Id
 *
 * \return The array index of the network, CANSM_NETWORKS_NUM on lookup failure
 */
#define CANSM_NW_IDX_FROM_CONTROLLER(ctrl) \
  ( (ctrl) < CANSM_CONTROLLERS_ID_NUM ? CanSM_ControllerNetworkLut[(ctrl)] : CANSM_NETWORKS_NUM )

[!IF "$usePnSupportOrTransceiver = 'true'"!]
/** \brief Convert Transceiver id to array index for internal network data
*
* \param[in] trcv Transceiver Id
*
* \return The array index of the network, CANSM_NETWORKS_NUM on lookup failure
*/
#define CANSM_NW_IDX_FROM_TRCV(trcv) \
  ( (trcv) < CANSM_TRANSCEIVERS_ID_NUM ? CanSM_TrcvNetworkLut[(trcv)] : CANSM_NETWORKS_NUM )
[!ENDIF!]

/*==================[type definitions]======================================*/

[!IF "$numNetworks > 1"!]
/** \brief configuration data for one network */
typedef struct {
[!IF "$nwSpecificBorTimeL1 = 'true'"!]
  uint16 BorTimeL1; /**< Duration to stay in level 1 check before restart of communication
                       \note Since bus-off events occur asynchronous this value is incremented by
                       one (refer to design, CanSM.Design.20085) */
[!ENDIF!]
[!IF "$nwSpecificBorTimeL2 = 'true'"!]
  uint16 BorTimeL2; /**< Duration to stay in level 2 check before restart of communication
                       \note Since bus-off events occur asynchronous this value is incremented by
                       one (refer to design, CanSM.Design.20085) */
[!ENDIF!]
[!IF "$nwSpecificBorTimeTxEnsured = 'true'"!]
  uint16 BorTimeTxEnsured; /**< Duration until bus-off free communication is assumed */
[!ENDIF!]
[!IF "$useDEM = 'true'"!]
  Dem_EventIdType EventIdBusOff; /**< Event Id to be passed to Dem */
[!ENDIF!]
[!IF "$singleControllers = 'true'"!]
  uint8 ControllerId; /**< Id of CAN controller */
[!ENDIF!]
[!IF "$usePnSupportOrTransceiver = 'true'"!]
  uint8 TrcvId; /**< ID of Transceiver */
[!ENDIF!]
[!IF "$nwSpecificBorCounterL1ToL2 = 'true'"!]
  uint8 BorCounterL1ToL2; /**< bus-off recovery L1 to L2 Threshold */
[!ENDIF!]
[!IF "$nwSpecificBorTxConfirmationPolling = 'true'"!]
  boolean BorTxConfirmationPolling; /**< bus-off recovery Tx Confirmation Polling */
[!ENDIF!]
[!IF "as:modconf('CanSM')[1]/CanSMGeneral/CanSMPNSupport = 'true'"!]
  boolean PartialNetworking; /**< partial networking activated for this network */
[!ENDIF!]
} CanSM_NetworkConfigType;
[!ENDIF!]

/** \brief States and sub-states of internal state machine, most taken from ASR40 SWS */
typedef enum {
  CANSM_S_NOIN,                         /**< State according to ASR40 SWS Figure 7-1 */

  CANSM_S_RNOCO_CAN_CC_STOPPED,         /**< State according to ASR40 SWS rev 3 Figure 7-4 */
  CANSM_S_RNOCO_CAN_CC_SLEEP,           /**< State according to ASR40 SWS rev 3 Figure 7-4 */
#if (CANSM_USE_TRANSCEIVER == STD_ON)
  CANSM_S_RNOCO_CAN_TRCV_NORMAL,        /**< State according to ASR40 SWS rev 3 Figure 7-4 */
  CANSM_S_RNOCO_CAN_TRCV_STANDBY,       /**< State according to ASR40 SWS rev 3 Figure 7-4 */
#endif /* CANSM_USE_TRANSCEIVER == STD_ON */

#if (CANSM_PNSUPPORT == STD_ON)
  CANSM_S_PN_CLEAR_WUF,                 /**< State according to ASR40 rev 3 SWS Figure 7-3 */
  CANSM_S_PN_CC_STOPPED,                /**< State according to ASR40 rev 3 SWS Figure 7-3 */
  CANSM_S_PN_TRCV_NORMAL,               /**< State according to ASR40 rev 3 SWS Figure 7-3 */
  CANSM_S_PN_TRCV_STANDBY,              /**< State according to ASR40 rev 3 SWS Figure 7-3 */
  CANSM_S_PN_CC_SLEEP,                  /**< State according to ASR40 rev 3 SWS Figure 7-3 */
  CANSM_S_PN_CHECK_WFLAG_IN_CC_SLEEP,   /**< State according to ASR40 rev 3 SWS Figure 7-3 */
  CANSM_S_PN_CHECK_WFLAG_IN_NOT_CC_SLEEP,/**< State according to ASR40 rev 3 SWS Figure 7-3 */
#endif /* (CANSM_PNSUPPORT == STD_ON) */


  CANSM_S_NOCO,                         /**< State according to ASR40 SWS Figure 7-1 */

#if (CANSM_USE_TRANSCEIVER == STD_ON)
  CANSM_S_RFUCO_CAN_TRCV_NORMAL,        /**< State according to ASR40 SWS Figure 7-3 */
#endif /* CANSM_USE_TRANSCEIVER == STD_ON */
  CANSM_S_RFUCO_CAN_CC_STOPPED,         /**< State according to ASR40 SWS Figure 7-3 */
  CANSM_S_RFUCO_CAN_CC_STARTED,         /**< State according to ASR40 SWS Figure 7-3 */

  CANSM_S_FUCO_BUS_OFF_CHECK,           /**< State according to ASR40 SWS Figure 7-4 */
  CANSM_S_FUCO_NO_BUS_OFF,              /**< State according to ASR40 SWS Figure 7-4 */

  CANSM_S_FUCO_RESTART_CC,              /**< State according to ASR40 SWS Figure 7-4 */
  CANSM_S_FUCO_TX_OFF,                  /**< State according to ASR40 SWS Figure 7-4 */

  CANSM_S_SICO                          /**< State according to ASR40 SWS Figure 7-1 */
} CanSM_InternalStateType;

/** \brief Transitions between states of type CanSM_InternalStateType.
 *
 * The transitions are executed by calling CanSM_DoTransitionSequence.
 */
typedef enum
{
  CANSM_T_NONE,                         /**< Special value: no transition shall be fired */

  CANSM_T_RNOCO_INITIAL,                /**< Initial transition to RNOCO (E_PRE_NO_COM) */
  CANSM_T_RNOCO_DEINIT_INITIAL,         /**< Choice between Partial Network on/off */

  CANSM_T_RNOCO_CC_INITIAL,             /**< Transition to CANSM_S_RNOCO_CAN_CC_STOPPED */
  CANSM_T_RNOCO_CC_STOPPED,             /**< Transition according to ASR40 SWS rev 3 Figure 7-4 */
  CANSM_T_RNOCO_CC_SLEEP,               /**< Transition according to ASR40 SWS rev 3 Figure 7-4 */

#if (CANSM_USE_TRANSCEIVER == STD_ON)
  CANSM_T_RNOCO_TRCV_NORMAL,            /**< Transition according to ASR40 SWS rev 3 Figure 7-4 */
#endif /* CANSM_USE_TRANSCEIVER == STD_ON */

#if (CANSM_PNSUPPORT == STD_ON)
  CANSM_T_PN_DEINIT,                    /**< Transition to CANSM_S_PN_CLEAR_WUF */
  CANSM_T_PN_CLEAR_WUF_INDICATED,       /**< Transition to CANSM_S_PN_CC_STOPPED */
  CANSM_T_PN_CC_STOPPED_INDICATED,      /**< Transition to CANSM_S_PN_TRCV_NORMAL */
  CANSM_T_PN_TRCV_NORMAL_INDICATED,     /**< Transition to CANSM_S_PN_TRCV_STANDBY */
  CANSM_T_PN_TRCV_STANDBY_INDICATED,    /**< Transition to CANSM_S_PN_CC_SLEEP */
  CANSM_T_PN_CC_SLEEP_INDICATED,        /**< Transition to CANSM_S_PN_CHECK_WFLAG_IN_CC_SLEEP */
  CANSM_T_PN_CC_SLEEP_TIMEOUT,          /**< Transition to CANSM_S_PN_CHECK_WFLAG_IN_NOT_CC_SLEEP */
#endif /* (CANSM_PNSUPPORT == STD_ON) */

  CANSM_T_RNOCO_FINAL,                  /**< Transition to CANSM_S_NOCO */

  CANSM_T_RFUCO_INITIAL,                /**< Branches to CANSM_T_RFUCO_TRCV_INITIAL (if transceiver
                                         *   is present) or CANSM_T_RFUCO_TRCV_NORMAL */
#if (CANSM_USE_TRANSCEIVER == STD_ON)
  CANSM_T_RFUCO_TRCV_INITIAL,           /**< Transition to CANSM_S_RFUCO_CAN_TRCV_NORMAL */
#endif /* CANSM_USE_TRANSCEIVER == STD_ON */
  CANSM_T_RFUCO_TRCV_NORMAL,            /**< Transition according to ASR40 SWS Figure 7-3 */
  CANSM_T_RFUCO_CC_STOPPED,             /**< Transition according to ASR40 SWS Figure 7-3 */
  CANSM_T_RFUCO_CC_STARTED,             /**< Transition according to ASR40 SWS Figure 7-3 */

  CANSM_T_FUCO_SICO,                    /**< Transition from CANSM_S_FUCO_BUS_OFF_CHECK and
                                         *   CANSM_S_FUCO_NO_BUS_OFF to CANSM_S_SICO */
  CANSM_T_FUCO_NOCO,                    /**< Transition from CANSM_S_FUCO to CANSM_S_NOCO */
  CANSM_T_SICO_FUCO,                    /**< Transition according to ASR40 SWS Figure 7-1 */

  CANSM_T_FUCO_HANDLE_BUS_OFF,          /**< All transitions to CANSM_S_FUCO_RESTART_CC */
  CANSM_T_FUCO_TX_OFF,                  /**< Transition according to ASR40 SWS Figure 7-4 */
  CANSM_T_FUCO_TX_ON,                   /**< Transition according to ASR40 SWS Figure 7-4 */
  CANSM_TRY_T_FUCO_BUS_OFF_PASSIVE      /**< Try transition according to ASR40 SWS Figure 7-4,
                                         *   unless there is a concurrent transition to
                                         *   silent communication */
} CanSM_TransitionType;

/** \brief runtime information for one network */
typedef struct {
  uint16 BoTimer; /**< timer for bus-off recovery state machine */
  uint16 ModeReqRepetitionTimer; /**< timer for mode change request repetitions */
#if (CANSM_PNSUPPORT == STD_ON)
  uint8 reqState; /** < store requested state for CanSM_TxTimeoutException */
#endif /* CANSM_PNSUPPORT == STD_ON */
  uint8 InternalState; /**< current internal state */
  uint8 NwState; /**< current network state */
  uint8 BusOffEvent; /**< indicates that a bus-off event has occured */
  uint8 BoCounter; /**< bus-off counter */
  uint8 ModeReqRepetitionCounter; /**< mode change request repetition counter */
#if (CANSM_TRCV_RESOURCES == STD_ON)
  uint8 TrcvModeIndication; /**< indication by last CanSM_TransceiverModeIndication
                                 (CanSM specific representation), or CANSM_TRCVMODETYPE_RESET */
#endif /* CANSM_TRCV_RESOURCES == STD_ON */
#if (CANSM_PNSUPPORT == STD_ON)
  boolean TrcvClearWUFIndication; /**< indication by last CanSM_ClearTrcvWufFlagIndication */
  boolean TrcvCheckWakeFlagIndication; /**< indication by last CanSM_CheckTransceiverWakeFlagIndication */
#endif /* CANSM_PNSUPPORT == STD_ON */
} CanSM_NetworkInfoType;

/*==================[external constants]====================================*/
[!IF "$numNetworks > 1"!]
#define CANSM_START_SEC_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

/** \brief configuration of all networks */
extern CONST(CanSM_NetworkConfigType, CANSM_APPL_CONST) CanSM_NetworkConfig[CANSM_NETWORKS_NUM];

#define CANSM_STOP_SEC_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>
[!ENDIF!]

#define CANSM_START_SEC_CONFIG_DATA_8
#include <MemMap.h>

/** \brief Empty configuration structure to be passed to CanSM_Init() */
extern CONST(CanSM_ConfigType, CANSM_APPL_CONST) [!"as:modconf('CanSM')[1]/CanSMConfiguration/*[1]/@name"!];

[!IF "$singleControllers != 'true'"!]
/**
 * \brief start index of associated controllers in controller list
 *
 * index is network id.
 */
extern CONST(uint8, CANSM_APPL_CONST) CanSM_CanNetworkControllersStart[CANSM_NETWORKS_NUM];

/**
 * \brief number of controllers associated to a network
 *
 * index is network id.
 *
 * \note extra element at end of array to determine the end of controller list
 */
extern CONST(uint8, CANSM_APPL_CONST) CanSM_CanNetworkControllersNum[CANSM_NETWORKS_NUM];

/**
 * \brief all controllers of all networks (ordered)
 *
 * \note network association is given through CanSM_CanNetworkControllersStart
 */
extern CONST(uint8, CANSM_APPL_CONST) CanSM_CanNetworkControllersList[CANSM_CONTROLLERS_NUM];
[!ENDIF!]

/**
 * \brief controller to network handle lookup table
 *
 * \note may contain holes, these have the value CANSM_NETWORKS_NUM
 */
extern CONST(uint8, CANSM_APPL_CONST) CanSM_ControllerNetworkLut[CANSM_CONTROLLERS_ID_NUM];

[!IF "$usePnSupportOrTransceiver = 'true'"!]
/**
 * \brief transceiver to network handle lookup table
 *
 * \note may contain holes, these have the value CANSM_NETWORKS_NUM
 */
extern CONST(uint8, CANSM_APPL_CONST) CanSM_TrcvNetworkLut[CANSM_TRANSCEIVERS_ID_NUM];
[!ENDIF!]

#define CANSM_STOP_SEC_CONFIG_DATA_8
#include <MemMap.h>

/*==================[external data]=========================================*/

#define CANSM_START_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>

[!IF "$numNetworks = 1"!]
/** \brief run-time information for all networks */
extern VAR(CanSM_NetworkInfoType, CANSM_VAR) CanSM_NetworkInfo;
[!ELSE!]
/** \brief run-time information for all networks */
extern VAR(CanSM_NetworkInfoType, CANSM_VAR) CanSM_NetworkInfo[CANSM_NETWORKS_NUM];
[!ENDIF!]

/** \brief Contains indication by last CanSM_ControllerModeIndication (CanSM specific
 *  representation), or CANSM_CTRLMODETYPE_RESET
 */
extern VAR(uint8, CANSM_VAR) CanSM_CtrlModeIndication[CANSM_CONTROLLERS_ID_NUM];

#define CANSM_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>

/*==================[external function declarations]========================*/

[!IF "($numNetworks > 1) and ($nwHandlesConsecutive = 'false')"!]

#define CANSM_START_SEC_CODE
#include <MemMap.h>

/** \brief Convert Network Handle to array index for internal data
 *
 * \pre validity of the network handle has been checked. Otherwise the
 *      return value is undefined
 *
 * \param[in] nw A valid network handle
 *
 * \return The array index of the network
 */

extern FUNC(CanSM_NetworkIndexType, CANSM_CODE) CanSM_GetNetworkIndexFromHandle(
  NetworkHandleType nw
);

/** \brief Convert Network Handle to array inedx for internal data
 *
 * \param[in] nwidx A valid network array index (not checked here)
 *
 * \return The handle of the network
 */
extern FUNC(NetworkHandleType, CANSM_CODE) CanSM_GetNetworkHandleFromIndex(
  CanSM_NetworkIndexType nwidx
);

/** \brief Check if Network Handle is valid
 *
 * \param[in] nw A network handle
 *
 * \retval FALSE The network handle is invalid
 * \retval TRUE The network handle is valid
 */
extern FUNC(boolean, CANSM_CODE) CanSM_NetworkHandleIsValid(
  NetworkHandleType nw
);

#define CANSM_STOP_SEC_CODE
#include <MemMap.h>

[!ENDIF!]

#endif /* if !defined( CANSM_CFG_H ) */
/*==================[end of file]===========================================*/

