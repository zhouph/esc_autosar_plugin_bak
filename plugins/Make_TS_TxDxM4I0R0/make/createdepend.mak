# \file
#
# \brief AUTOSAR Make
#
# This file contains the implementation of the AUTOSAR
# module Make.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.


##################################################################
# VARIABLES
##################################################################

############################################################
# DEPEND_CPP_OPTIONS_FILE specifies of the  file holding include
# paths and settings for the preprocessor(CPP)
#
DEPEND_CPP_OPTIONS_FILE := $(DEP_OUTPUT_PATH)/depend.cpp_options

############################################################
# force rebuilding of dependencies if target depend is
# invoked
ifeq (depend,$(strip $(MAKECMDGOALS)))
	DEPEND_FORCE := depend_force
endif

############################################################
# Add options provided by the user to the preprocessor
# options
CPP_OPTS += $(DEPEND_GCC_OPTS)


##################################################################
# MACROS
##################################################################

############################################################
# Get name of object file for file name passed as first parameter
#
# Parameters:
# 	File name
depend_get_target = $(call GET_OBJ_OUTPUT_PATH,$1)\$(basename $(notdir $(subst \,/,$1))).$(OBJ_FILE_SUFFIX)


############################################################
# The next rule solves the dependencies for all source files *.c/*.cpp.
#
# The gcc produces an output file for each source file. This file
# will be stored in the folder $(PROJECT_ROOT)\outupt\depend and has
# the file suffix *.mak. Besides the correlation between object file and source file a list of
# all required header files is part of this so called dependency makefile.
#
# In case the gcc fails to parse the input file it creates an output file
# nonetheless, which is empty !
# Therefore it is required to generate the dependencies
# using a different strategy.
#
# gcc options:
#  -x: specify the language (here: c; required as files with a file
#      extension other than *.c is recognized as such
#      (can't be used just now as some assembler files of the OS use
#       preprocessor statement in a way not covered by the ISO standard)
#  -MM: output a rule suitable for make describing the dependencies
#  -MF: file to write the dependencies to
#  -MT: specify the target of the rule (here: object file and dependency file)
#
# For example:
# $(PROJECT_OUTPUT_PATH)/depend/os_depend.mak contains:
#
# C:\Work\myproject\output\obj\my_obj1.obj: \
#              C:\Work\myproject\source\application\my_source1.c \
#              C:\Work\myproject\source\application\my_source2.h \
#              C:\Work\myproject\source\application\my_source3.h \
#              C:\Work\myproject\source\application\my_source4.h
#
$(filter-out $(GCC_IGNORE_LIST),$(CC_TO_MAK_BUILD_LIST)) : $(DEPEND_FORCE) |  $(DEPEND_CPP_OPTIONS_FILE) $(DIRECTORIES_TO_CREATE)
	$(SEPARATOR) [depend]	$(notdir $@)
	$(CPP) @$(DEPEND_CPP_OPTIONS_FILE) -undef -MM -MF "$@" -MT "$(call depend_get_target,$@)" -MT "$@" "$(getSourceFile)" || if exist "$(getSourceFile)" echo $(call depend_get_target,$@) $@ : $(getSourceFile) > $@

############################################################
# The variable GCC_IGNORE_LIST contains a list of files for that
# the gcc shall be not called. The next rule creates the correlation
# between object file and source file without calling the gcc.
#
$(GCC_IGNORE_LIST) : | $(DIRECTORIES_TO_CREATE)
	$(SEPARATOR) [depend]	$(notdir $@)
	@if exist "$(getSourceFile)" echo $(call GET_OBJ_OUTPUT_PATH,$@)\$(basename $(notdir $(subst \,/,$@))).$(OBJ_FILE_SUFFIX) $@ : $(getSourceFile) > $@


############################################################
# depend_print_line
# create echo command with newline at the end
# used in rules so that the single line in the rule
# does not exceed the maximum allowed length for a  command line
#
define depend_print_line
	@echo $1

endef

############################################################
# Creates the dependencies for library files
# For this purpose all dependencies between libraries and objects
# will be dumped into a file.
#
# For example:
#
# $(PROJECT_OUTPUT_PATH)/depend/my_lib_1.mak contains:
#   C:\Work\my_lib_1.a : C:\Work\my_obj1.o
#   C:\Work\my_lib_1.a : C:\Work\my_obj2.o

$(LIB_TO_MAK_BUILD_LIST):$(DEP_OUTPUT_PATH)\\%.$(MAK_FILE_SUFFIX):$(DEPEND_FORCE) |  $(DIRECTORIES_TO_CREATE)
	$(SEPARATOR) [depend lib]	$(notdir $@)
	$(call depend_print_line, $@ : $(call GET_LIB_OUTPUT_PATH,$(*))\$*.$(LIB_FILE_SUFFIX) > $@)
	$(foreach SRC, $(basename $(notdir $(subst \,/,$($*_FILES)))), \
	   $(call depend_print_line, $(call GET_LIB_OUTPUT_PATH,$*)\$*.$(LIB_FILE_SUFFIX) : $(call GET_OBJ_OUTPUT_PATH,$(SRC))\$(SRC).$(OBJ_FILE_SUFFIX) >> $@) \
	)


###################################################################
# The target clean_depend deletes all files which were created
# by the dependency mechanism.
#
MAKE_CLEAN_RULES += clean_depend
clean_depend_DESCRIPTION = -	-remove folder holding the dependencies ($(subst $(PROJECT_OUTPUT_PATH)\,,$(DEP_OUTPUT_PATH)))

clean_depend:
	$(START_MESSAGE_CLEAN)	$(subst $(PROJECT_OUTPUT_PATH)\,,$(DEP_OUTPUT_PATH))
	$(RMTREE) $(DEP_OUTPUT_PATH)

##################################################################
# Rule to create the dependency files
#
depend : $(LIB_TO_MAK_BUILD_LIST) $(CC_TO_MAK_BUILD_LIST)

# Create dependency files for building a post build binary 
depend_postBuildBinary:
	$(MAKE) -f Makefile.mak TS_BUILD_POST_BUILD_BINARY=TRUE depend

##################################################################
# Rule for forcing the execution of another rule or target
depend_force:


##################################################################
# Register the make depend rules
MAKE_GLOBAL_RULES +=depend

depend_DESCRIPTION = -		-creates all dependency makefiles (output/depend/*.mak)

.PHONY : depend clean_depend depend_print_line depend_force
