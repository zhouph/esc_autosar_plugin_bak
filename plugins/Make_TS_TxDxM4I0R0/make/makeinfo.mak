# \file
#
# \brief AUTOSAR Make
#
# This file contains the implementation of the AUTOSAR
# module Make.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# This file belongs to the implementation of the base_make package.
# It contains functionality to create a file which contains the
# dependencies for a C/CPP file.

#################################################################
# Define location of file containing information of a run of
# make
#
MAKE_INFO_FILE := $(MAKE_OUTPUT_PATH)\make.info

#################################################################
# Define location of file containing information of a run of
# make
#
MAKE_INFO += \
	PROJECT=$(PROJECT) \
	TARGET=$(TARGET) \
	DERIVATE=$(DERIVATE)


#################################################################
# Create file containing information of a run of make
#
$(MAKE_INFO_FILE):
	$(PRE_BUILD_BLOCK)
	$(foreach INFO, $(MAKE_INFO), $(call SC_CMD_AR_HELPER,@echo $(INFO)>> $@))

###################################################################
# The target clean_makeinfo removes the file containing information
# of a run of make.
#
clean_makeinfo:
	$(START_MESSAGE_CLEAN)	$(notdir $(subst \,/,$(MAKE_INFO_FILE)))
	$(RM) $(MAKE_INFO_FILE)

##################################################################
# Register the clean target
MAKE_CLEAN_RULES  +=clean_makeinfo

.PHONY : $(MAKE_INFO_FILE) clean_makeinfo
