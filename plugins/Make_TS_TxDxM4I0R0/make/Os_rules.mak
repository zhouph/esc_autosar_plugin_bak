# \file
#
# \brief AUTOSAR Make
#
# This file contains the implementation of the AUTOSAR
# module Make.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# this file just references the AutosarOS_rules.mak makfiles
# in the variant directory
ifeq ($(OS_ROOT),)
include $(SSC_ROOT)\Os_$(Os_VARIANT)\make\Os_rules.mak
else
include $(OS_ROOT)/make/Os_rules.mak
endif

