# \file
#
# \brief AUTOSAR Make
#
# This file contains the implementation of the AUTOSAR
# module Make.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.


##################################################################
# VARIABLES
##################################################################

############################################################
# CPP_OPTIONS_FILE specifies of the  file holding include
# paths and settings for the preprocessor(CPP)
#
CPP_OPTIONS_FILE := $(CPP_OUTPUT_PATH)\preprocess.cpp_options



#################################################################
# CC_TO_CPP_BUILD_LIST holds the files to be created when the
# preprocess target is invoked
CC_TO_CPP_BUILD_LIST := $(foreach SRC, $(TEMP_ALL_CC_FILES), $(CPP_OUTPUT_PATH)\$(basename $(notdir $(subst \,/,$(SRC)))).$(CPP_FILE_SUFFIX))


##################################################################
# Register preprocess targets
#
MAKE_GLOBAL_RULES +=preprocess
preprocess_DESCRIPTION = -		-creates preprocessed files in $(subst $(PROJECT_OUTPUT_PATH)\,,$(CPP_OUTPUT_PATH))
MAKE_CLEAN_RULES += clean_preprocess
clean_preprocess_DESCRIPTION = -	-remove folder holding the preprocessed files ($(subst $(PROJECT_OUTPUT_PATH)\,,$(CPP_OUTPUT_PATH)))


##################################################################
# TARGETS
##################################################################

###################################################################
# The target clean_preprocess deletes the folder where the 
# preprocessed files are located
#
clean_preprocess:
	$(START_MESSAGE_CLEAN)	$(subst $(PROJECT_OUTPUT_PATH)\,,$(CPP_OUTPUT_PATH))
	$(RMTREE) $(CPP_OUTPUT_PATH)


##################################################################
# Rule to create the preprocessency files
#
preprocess: $(CC_TO_CPP_BUILD_LIST)

##################################################################
# Rule to force the excution of certain rules
#
force_preprocess:

##################################################################
# RULES
##################################################################

############################################################
# Create preprocessor output
#
# gcc preprocessor options:
#  -E: Only preprocess files
$(CC_TO_CPP_BUILD_LIST) : $(CPP_OPTIONS_FILE) force_preprocess
	$(PRE_BUILD_BLOCK)
	-$(CPP) @$(CPP_OPTIONS_FILE) -E -C -o "$@" "$(getSourceFile)"
	$(POST_BUILD_BLOCK)

.PHONY : preprocess clean_preprocess force_preprocess
