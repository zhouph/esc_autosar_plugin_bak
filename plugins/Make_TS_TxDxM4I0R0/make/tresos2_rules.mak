# \file
#
# \brief AUTOSAR Make
#
# This file contains the implementation of the AUTOSAR
# module Make.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS


#################################################################
# Generate the default SWCDs (via mode generate_swcd)
# If set to ASR31: The AUTOSAR 3.1 SWCDs are generated instead (mode=generate_asr31_swcd)
# If set to ASR32: The AUTOSAR 3.2 SWCDs are generated instead (mode=generate_asr32_swcd)
# If set to ASR40: The AUTOSAR 4.0 SWCDs are generated instead (mode=generate_asr32_swcd)
TRESOS2_GENERATE_SWCD_MODE ?= DEFAULT

#################################################################
# If set to TRUE, all System Description files are first converted
# to one temporary existing arxml file with latest provided AUTOSAR version.
# This temporary file is then used as input for the generation of
# source code in EB tresos Studio.
# Note: This parameter does not affect code generation in project mode
TRESOS2_CONVERT_TO_SYSTEM_MODEL40 ?= FALSE
TRESOS2_SYSTEM_MODEL40_FILENAME ?= SystemModel2.tdb@tdb:4.0

#################################################################
# REGISTRY

DIRECTORIES_TO_CREATE += $(TRESOS2_OUTPUT_PATH) $(TRESOS2_OUTPUT_PATH)\make
MAKE_CLEAN_RULES      +=
MAKE_GENERATE_RULES   += 01_tresos2_call_generator
MAKE_COMPILE_RULES    +=
#MAKE_DEBUG_RULES     +=
MAKE_CONFIG_RULES     +=
#MAKE_ADD_RULES       +=

tresos2_clean_DESCRIPTION             := -   -removes all files created by Tresos2
tresos2_call_generator_DESCRIPTION    := -   -generates Tresos2 files (output)
start_tresos2_conf_DESCRIPTION        := -   -starts the Tresos2 configurator

#################################################################
# Generate the tresos 2.X plugins
#

################################################################
# generate a list of config files for generator call
#
# tresos needs configuration files for both, the modules which shall
# be generarted and for those whose configuration is just read

TRESOS2_CONF_FILES = $(call unique,\
 $(foreach TRESOS2_GEN_PLUGIN,$(TRESOS2_GEN_PLUGINS),$($(TRESOS2_GEN_PLUGIN)_CONF_FILE)) \
 $(foreach TRESOS2_NOGEN_PLUGIN,$(TRESOS2_NOGEN_PLUGINS),$($(TRESOS2_NOGEN_PLUGIN)_CONF_FILE)) \
 $(foreach TRESOS2_STUB_PLUGINS,$(TRESOS2_STUB_PLUGINS),$($(TRESOS2_STUB_PLUGINS)_CONF_FILE)))

################################################################
# generate a list of modules having a config file
#
TRESOS2_CONF_MODULE_IDS = $(call unique, $(PLUGINS_TO_GENERATE) $(if $(Resource_VARIANT),Resource_$(Resource_VARIANT)) $(foreach MOD,$(TRESOS2_ADDITIONAL_RSC_MODULES),$(if $($(MOD)_VARIANT),$(MOD)_$($(MOD)_VARIANT))))
TRESOS2_CONF_STUBS      = $(filter-out $(TRESOS2_GEN_PLUGINS),$(basename $(notdir $(TRESOS2_CONF_FILES))))
TRESOS2_CONF_STUBS_IDS  = $(foreach MOD,$(patsubst %Stub,%,$(TRESOS2_CONF_STUBS)),$(if $($(MOD)Stub_VARIANT),$(MOD)_$($(MOD)Stub_VARIANT)))
TRESOS2_CONF_STUBS_40_IDS  = $(foreach MOD,$(patsubst %Stub,%,$(TRESOS2_CONF_STUBS)),$(if $($(MOD)Stub_40_VARIANT),$(MOD)_$($(MOD)Stub_40_VARIANT)))

# create a semicolon-separated list. Value for -DEcuResourceModuleIds (generator
# call, legacy mode only)
TRESOS2_CONF_ECU_RESOURCEMODULE_IDS=$(subst $(BLANK),;,$(strip $(TRESOS2_CONF_MODULE_IDS) $(TRESOS2_CONF_STUBS_IDS) $(TRESOS2_CONF_STUBS_40_IDS)))

################################################################
# default location for files holding the system description
# when generating in the legacy mode
#
# TRESOS2_SYSD_GENSWCD_FILES:
#    Files passed in generation mode 'generate_swcd'
#
ifeq ($(TRESOS2_PROJECT_NAME),)
# input sysd files for the "generate_swcd" step
TRESOS2_SYSD_GENSWCD_FILES ?=\
 $(addsuffix @sysd,$(wildcard $(PROJECT_ROOT)/config/generate_swcd/*.arxml))

# static sysd files
TRESOS2_SYSD_ALL_FILES ?=\
 $(addsuffix @sysd,$(wildcard $(PROJECT_ROOT)/config/*.arxml))

# generated sysd files
TRESOS2_SYSD_ALL_FILES +=\
 $(addsuffix @sysd,$(wildcard $(PROJECT_ROOT)/output/generated/swcd/*.arxml))

TRESOS2_SYSD_ALL_FILES += $(TRESOS2_SYSD_GENSWCD_FILES)
TRESOS2_SYSD_ALL_FILES += $(TRESOS2_SYSD_FILES)
endif

################################################################
# assemble the whole plugin name including the variant string.  tresos
# will just generate these plugins
ifeq ($(MODULE),)
  PLUGINS_TO_GENERATE:=$(foreach TRESOS2_GEN_PLUGIN,$(TRESOS2_GEN_PLUGINS),$(TRESOS2_GEN_PLUGIN)_$($(TRESOS2_GEN_PLUGIN)_VARIANT))
  PLUGINS_TO_GENERATE_PRJ:=
else
  PLUGINS_TO_GENERATE:=$(MODULE)_$($(MODULE)_VARIANT)
  PLUGINS_TO_GENERATE_PRJ:=$(PLUGINS_TO_GENERATE)
endif

################################################################
# check for clean targets; for a clean target, the
# generator should not be called

# if no target is given, call the generator
ifeq (-$(strip $(MAKECMDGOALS)),-)
TRESOS2_GENERATOR_MODE := CALL_GEN
else
# If a a target es given and the target is not clean or
# clean_depend, call the generator.
ifneq (-$(strip $(filter-out $(strip $(MAKE_CLEAN_RULES) clean clean_depend $(MAKE_CONFIG_RULES) $(MAKE_ADD_RULES)),$(strip $(MAKECMDGOALS)))),-)
TRESOS2_GENERATOR_MODE := CALL_GEN
endif
endif

# if no config files are given in legacy mode, the generator should not be invoked
ifeq ($(TRESOS2_PROJECT_NAME),)
ifeq ($(strip $(TRESOS2_CONF_FILES)),)
TRESOS2_GENERATOR_MODE := NO_ACTION
endif
endif

ifeq ($(TRESOS2_GENERATOR_MODE),CALL_GEN)

ifeq ($(TRESOS2_WORKSPACE_DIR),)
# Use temporary workspace by default, if the test does not already use a
# self-defined workspace.
# Default can be switched-off by setting TRESOS2_USE_TMP_WORKSPACE to be
# empty.
TRESOS2_USE_TMP_WORKSPACE ?= 1
endif

# If TRESOS2_USE_TMP_WORKSPACE is set, then tresos Studio will always
# use a temporary Eclipse workspace
ifneq ($(TRESOS2_USE_TMP_WORKSPACE),)
#
# temporary workspace directory
ifeq ($(TRESOS2_PROJECT_NAME),)
TRESOS2_TMPWKSP := $(TRESOS2_OUTPUT_PATH)\studio_temp_workspace
else
TRESOS2_TMPWKSP := $(PROJECT_ROOT)\..\workspace_$(TRESOS2_PROJECT_NAME)
endif
TRESOS2_TMPWKSP_OPTION := -data $(TRESOS2_TMPWKSP)
endif

################################################################
# Provide required generation options for SWCD generation
ifeq ($(TRESOS2_GENERATE_SWCD_MODE), ASR31)
TRESOS2_GENERATE_SWCD_MODE_OPTION := generate_asr31_swcd
else
ifeq ($(TRESOS2_GENERATE_SWCD_MODE), ASR32)
TRESOS2_GENERATE_SWCD_MODE_OPTION := generate_asr32_swcd
else
ifeq ($(TRESOS2_GENERATE_SWCD_MODE), ASR40)
TRESOS2_GENERATE_SWCD_MODE_OPTION := generate_asr40_swcd
else
TRESOS2_GENERATE_SWCD_MODE_OPTION := generate_swcd
endif
endif
endif

# TRESOS2_AUTOCONFIGURE_TARGETS contains all targets for which
# tresos will be called with the autconfigure wizard.
TRESOS2_AUTOCONFIGURE_TARGETS ?= SwdUpdater_Trigger
TRESOS2_AUTOCONFIGURE_OPTIONS_SwdUpdater_Trigger := 

################################################################
# this rule invokes the tresos2 template generator
# select legacy mode or project mode by the TRESOS2_PROJECT_NAME
#
# If TRESOS2_SUPPRESS_GENERATE_SWCD is set, then the "generate_swcd"
# step is skipped in legacy mode.  This should only be used in
# situations were the the generated SWCD files are definitely not used
# in the final generation step.
01_tresos2_call_generator:
ifeq ($(TRESOS2_PROJECT_NAME),)
	@if not exist $(TRESOS2_OUTPUT_PATH) @mkdir $(TRESOS2_OUTPUT_PATH)
ifeq ($(TRESOS2_SUPPRESS_GENERATE_SWCD),)
	$(SEPARATOR) GENERATING tresos2 SWCD Files via generator mode $(TRESOS2_GENERATE_SWCD_MODE_OPTION)
	call $(TRESOS2_GENERATOR) $(TRESOS2_TMPWKSP_OPTION) -DEcuResourceModuleIds=$(TRESOS2_CONF_ECU_RESOURCEMODULE_IDS) -DValidate=FALSE -Dosek.orti=TRUE -Dosek.html=FALSE -Dtarget=$(CPU) -Dderivate=$(DERIVATE) $(TRESOS2_SWCD_OPTIONS) $(TRESOS2_USER_OPTIONS) legacy make $(TRESOS2_GENERATE_SWCD_MODE_OPTION) $(TRESOS2_SYSD_GENSWCD_FILES) $(TRESOS2_CONF_FILES) $(PLUGINS_TO_GENERATE:%=-g %) -o $(TRESOS2_OUTPUT_PATH) -u
endif
	@$(MAKE) -f Makefile.mak 01_tresos2_call_generator_src
else
	$(SEPARATOR) GENERATING tresos2 Source Files
ifneq ($(TRESOS2_USE_TMP_WORKSPACE),)
	@if exist $(TRESOS2_TMPWKSP) del /f /s /q $(TRESOS2_TMPWKSP) > nul
	call $(TRESOS2_GENERATOR) $(TRESOS2_TMPWKSP_OPTION) importProject $(PROJECT_ROOT)
endif
	$(foreach AUTOCONFIGURE_TARGET, $(TRESOS2_AUTOCONFIGURE_TARGETS), \
		$(call SC_CMD_AR_HELPER, $(TRESOS2_GENERATOR) $(TRESOS2_TMPWKSP_OPTION) $(TRESOS2_OPTIONS) $(TRESOS2_USER_OPTIONS) autoconfigure $(TRESOS2_PROJECT_NAME) $(AUTOCONFIGURE_TARGET) $(TRESOS2_AUTOCONFIGURE_OPTIONS_$(AUTOCONFIGURE_TARGET))) \
	)
	call $(TRESOS2_GENERATOR) $(TRESOS2_TMPWKSP_OPTION) $(TRESOS2_OPTIONS) $(TRESOS2_USER_OPTIONS) make generate $(TRESOS2_PROJECT_NAME) $(PLUGINS_TO_GENERATE:%=-g %)
endif
	$(END_BLOCK)

################################################################
# Provide system model input files for source code generation
ifeq ($(TRESOS2_CONVERT_TO_SYSTEM_MODEL40), TRUE)
TRESOS2_SYSTEM_MODEL_INPUT_FILES = $(TRESOS2_SYSTEM_MODEL40_FILENAME)
else
TRESOS2_SYSTEM_MODEL_INPUT_FILES = $(TRESOS2_SYSD_ALL_FILES)
endif

################################################################
# this rule invokes the tresos2 template generator
# select legacy mode or project mode by the TRESOS2_PROJECT_NAME
#
01_tresos2_call_generator_src:
	@if not exist $(TRESOS2_OUTPUT_PATH) @mkdir $(TRESOS2_OUTPUT_PATH)
ifeq ($(TRESOS2_CONVERT_TO_SYSTEM_MODEL40), TRUE)
	$(SEPARATOR) Temporary convert all System Description files to temporary AUTOSAR 4.0 TDB system model
	call $(TRESOS2_GENERATOR) $(TRESOS2_TMPWKSP_OPTION) $(TRESOS2_OPTIONS) -DValidate=FALSE legacy convert $(TRESOS2_SYSD_ALL_FILES) $(TRESOS2_SYSTEM_MODEL40_FILENAME)
endif
	$(SEPARATOR) GENERATING tresos2 Source Files
	call $(TRESOS2_GENERATOR) $(TRESOS2_TMPWKSP_OPTION) -DEcuResourceModuleIds=$(TRESOS2_CONF_ECU_RESOURCEMODULE_IDS) -DValidate=TRUE -Dosek.orti=TRUE -Dosek.html=FALSE -Dtarget=$(CPU) -Dderivate=$(DERIVATE) $(Rte_OPTIONS) $(TRESOS2_OPTIONS) $(TRESOS2_USER_OPTIONS) legacy make generate $(TRESOS2_SYSTEM_MODEL_INPUT_FILES)  $(TRESOS2_CONF_FILES) $(PLUGINS_TO_GENERATE:%=-g %) -o $(TRESOS2_OUTPUT_PATH) -u
	$(RM) $(TRESOS2_SYSTEM_MODEL40_FILENAME)
ifeq ($(TRESOS2_CONVERT_TO_SYSTEM_MODEL40), TRUE)
	$(SEPARATOR) DELETE temporary AUTOSAR 4.0 TDB system model
	$(RM) $(TRESOS2_SYSTEM_MODEL40_FILENAME)
endif

endif

ifeq ($(TRESOS2_GENERATOR_MODE),NO_ACTION)
################################################################
# this rule is a dummy target for make clean
#
01_tresos2_call_generator:
endif
