This directory contains the 3.82 release of GNU Make.


Downloading
-----------

GNU Make can be obtained in many different ways.  See a description here:

  http://www.gnu.org/software/software.html


Documentation
-------------

GNU make is fully documented in the GNU Make manual, which is contained
in this distribution as the file make.texinfo.  You can also find
on-line and preformatted (PostScript and DVI) versions at the FSF's web
site.  There is information there about ordering hardcopy documentation.

  http://www.gnu.org/
  http://www.gnu.org/doc/doc.html
  http://www.gnu.org/manual/manual.html


Development
-----------

GNU Make development is hosted by Savannah, the FSF's online development
management tool.  Savannah is here:

  http://savannah.gnu.org

And the GNU Make development page is here:

  http://savannah.gnu.org/projects/make/

You can find most information concerning the development of GNU Make at
this site.


Bug Reporting
-------------

You can send GNU make bug reports to <bug-make@gnu.org>.  Please see the
section of the GNU make manual entitled `Problems and Bugs' for
information on submitting useful and complete bug reports.

You can also use the online bug tracking system in the Savannah GNU Make
project to submit new problem reports or search for existing ones:

  http://savannah.gnu.org/bugs/?group=make

If you need help using GNU make, try these forums:

  help-make@gnu.org
  help-utils@gnu.org
  news:gnu.utils.help
  news:gnu.utils.bug

  http://savannah.gnu.org/support/?group=make

You may also find interesting patches to GNU Make available here:

  http://savannah.gnu.org/patch/?group=make

Note these patches are provided by our users as a service and we make no
statements regarding their correctness.  Please contact the authors
directly if you have a problem or suggestion for a patch available on
this page.


-------------------------------------------------------------------------------
Copyright (C) 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997,
1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009,
2010 Free Software Foundation, Inc.
This file is part of GNU Make.

GNU Make is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the License, or (at your option) any later
version.

GNU Make is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.