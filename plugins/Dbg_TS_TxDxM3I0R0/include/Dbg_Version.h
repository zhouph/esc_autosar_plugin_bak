/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DBG_VERSION_H)
#define DBG_VERSION_H

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

/*------------------[AUTOSAR vendor identification]-------------------------*/

#if (defined DBG_VENDOR_ID)
#error DBG_VENDOR_ID is already defined
#endif
/** \brief AUTOSAR vendor identification: Elektrobit Automotive GmbH */
#define DBG_VENDOR_ID         1U

/*------------------[AUTOSAR module identification]-------------------------*/

#if (defined DBG_MODULE_ID)
#error DBG_MODULE_ID already defined
#endif
/** \brief AUTOSAR module identification */
#define DBG_MODULE_ID         57U

/*------------------[AUTOSAR release version identification]----------------*/

#if (defined DBG_AR_RELEASE_MAJOR_VERSION)
#error DBG_AR_RELEASE_MAJOR_VERSION already defined
#endif
/** \brief AUTOSAR release major version */
#define DBG_AR_RELEASE_MAJOR_VERSION     4U

#if (defined DBG_AR_RELEASE_MINOR_VERSION)
#error DBG_AR_RELEASE_MINOR_VERSION already defined
#endif
/** \brief AUTOSAR release minor version */
#define DBG_AR_RELEASE_MINOR_VERSION     0U

#if (defined DBG_AR_RELEASE_REVISION_VERSION)
#error DBG_AR_RELEASE_REVISION_VERSION already defined
#endif
/** \brief AUTOSAR release revision version */
#define DBG_AR_RELEASE_REVISION_VERSION  3U

/*------------------[AUTOSAR module version identification]------------------*/

#if (defined DBG_SW_MAJOR_VERSION)
#error DBG_SW_MAJOR_VERSION already defined
#endif
/** \brief AUTOSAR module major version */
#define DBG_SW_MAJOR_VERSION             3U

#if (defined DBG_SW_MINOR_VERSION)
#error DBG_SW_MINOR_VERSION already defined
#endif
/** \brief AUTOSAR module minor version */
#define DBG_SW_MINOR_VERSION             0U

#if (defined DBG_SW_PATCH_VERSION)
#error DBG_SW_PATCH_VERSION already defined
#endif
/** \brief AUTOSAR module patch version */
#define DBG_SW_PATCH_VERSION             4U

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

#endif /* if !defined( DBG_VERSION_H ) */
/*==================[end of file]============================================*/
