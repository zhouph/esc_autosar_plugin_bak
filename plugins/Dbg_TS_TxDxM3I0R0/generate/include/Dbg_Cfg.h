/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DBG_CFG_H)
#define DBG_CFG_H

/* 
 * !LINKSTO Dbg.FileStructure.Dbg010,1
 */

/*==================[includes]==============================================*/
[!CODE!]
[!AUTOSPACING!]
[!SELECT "as:modconf('Dbg')[1]"!]
[!INCLUDE "Dbg.m"!]

#include <TSAutosar.h>              /* EB specific standard types */
#include <Std_Types.h>             /* AUTOSAR standard types (ON/OFF) */
#include <Dbg_Cfg_Hw.h>

/*==================[version check]=========================================*/
#define DBG_GEN_MODULE_ID           [!"node:value(./CommonPublishedInformation/ModuleId)"!]U
#define DBG_GEN_VENDOR_ID           [!"node:value(./CommonPublishedInformation/VendorId)"!]U
#define DBG_GEN_AR_MAJOR_VERSION    [!"node:value(./CommonPublishedInformation/ArMajorVersion)"!]U
#define DBG_GEN_AR_MINOR_VERSION    [!"node:value(./CommonPublishedInformation/ArMinorVersion)"!]U
#define DBG_GEN_AR_PATCH_VERSION    [!"node:value(./CommonPublishedInformation/ArPatchVersion)"!]U
#define DBG_GEN_SW_MAJOR_VERSION    [!"node:value(./CommonPublishedInformation/SwMajorVersion)"!]U
#define DBG_GEN_SW_MINOR_VERSION    [!"node:value(./CommonPublishedInformation/SwMinorVersion)"!]U
#define DBG_GEN_SW_PATCH_VERSION    [!"node:value(./CommonPublishedInformation/SwPatchVersion)"!]U

/*==================[macros]================================================*/

[!IF "node:exists(./DbgGeneral/DbgVersionInfoApi)"!]
  [!IF "node:value(./DbgGeneral/DbgVersionInfoApi)='false'"!]
#define DBG_GET_VERSION_INFO_API             STD_OFF
  [!ELSE!][!//
#define DBG_GET_VERSION_INFO_API             STD_ON
  [!ENDIF!]
[!ELSE!][!//
#define DBG_GET_VERSION_INFO_API             STD_OFF
[!ENDIF!]

[!IF "node:value(./DbgGeneral/DbgDevErrorDetect)='false'"!]
#define DBG_DEV_ERROR_DETECT                 STD_OFF
[!ELSE!][!//
#define DBG_DEV_ERROR_DETECT                 STD_ON
[!ENDIF!]

[!IF "node:value(./DbgGeneral/DbgValueTracing)='false'"!]
#define DBG_VALUE_TRACING                    STD_OFF
[!ELSE!][!//
#define DBG_VALUE_TRACING                    STD_ON
[!ENDIF!]

[!VAR "configCount"!][!CALL "DBG_RES_PREDEF_DID_GROUP_COUNT","M_resGroupName"="'Dbg.PredefDid.Os'"!][!ENDVAR!]
[!IF "$configCount > 0"!]
#define DBG_TRACE_OS                         STD_ON
[!ELSE!][!//
#define DBG_TRACE_OS                         STD_OFF
[!ENDIF!]
[!VAR "configCount"!][!CALL "DBG_RES_PREDEF_DID_GROUP_COUNT","M_resGroupName"="'Dbg.PredefDid.Det'"!][!ENDVAR!]
[!IF "$configCount > 0"!]
#define DBG_TRACE_DET                        STD_ON
[!ELSE!][!//
#define DBG_TRACE_DET                        STD_OFF
[!ENDIF!]
[!VAR "configCount"!][!CALL "DBG_RES_PREDEF_DID_GROUP_COUNT","M_resGroupName"="'Dbg.PredefDid.RteCall'"!][!ENDVAR!]
[!IF "$configCount > 0"!]
#define DBG_TRACE_RTE_CALL                   STD_ON
[!ELSE!][!//
#define DBG_TRACE_RTE_CALL                   STD_OFF
[!ENDIF!]
[!VAR "configCount"!][!CALL "DBG_RES_PREDEF_DID_GROUP_COUNT","M_resGroupName"="'Dbg.PredefDid.RteVfb'"!][!ENDVAR!]
[!IF "$configCount > 0"!]
#define DBG_TRACE_RTE_VFB                    STD_ON
[!ELSE!][!//
#define DBG_TRACE_RTE_VFB                    STD_OFF
[!ENDIF!]
[!VAR "configCount"!][!CALL "DBG_RES_PREDEF_DID_GROUP_COUNT","M_resGroupName"="'Dbg.PredefDid.RteCom'"!][!ENDVAR!]
[!IF "$configCount > 0"!]
#define DBG_TRACE_RTE_COM                    STD_ON
[!ELSE!][!//
#define DBG_TRACE_RTE_COM                    STD_OFF
[!ENDIF!]
[!VAR "configCount"!][!CALL "DBG_RES_PREDEF_DID_GROUP_COUNT","M_resGroupName"="'Dbg.PredefDid.RteComSignal'"!][!ENDVAR!]
[!IF "$configCount > 0"!]
#define DBG_TRACE_RTE_COM_SIGNAL             STD_ON
[!ELSE!][!//
#define DBG_TRACE_RTE_COM_SIGNAL             STD_OFF
[!ENDIF!]
[!VAR "configCount"!][!CALL "DBG_RES_PREDEF_DID_GROUP_COUNT","M_resGroupName"="'Dbg.PredefDid.Runnable'"!][!ENDVAR!]
[!IF "$configCount > 0"!]
#define DBG_TRACE_RUNNABLE                   STD_ON
[!ELSE!][!//
#define DBG_TRACE_RUNNABLE                   STD_OFF
[!ENDIF!]
[!VAR "configCount"!][!CALL "DBG_RES_PREDEF_DID_GROUP_COUNT","M_resGroupName"="'Dbg.PredefDid.Timestamp'"!][!ENDVAR!]
[!IF "$configCount > 0"!]
#define DBG_TRACE_TIMESTAMP                  STD_ON
[!ELSE!][!//
#define DBG_TRACE_TIMESTAMP                  STD_OFF
[!ENDIF!]
[!VAR "configCount"!][!CALL "DBG_RES_PREDEF_DID_GROUP_COUNT","M_resGroupName"="'Dbg.PredefDid.Func'"!][!ENDVAR!]
[!IF "$configCount > 0"!]
#define DBG_TRACE_FUNC                       STD_ON
[!ELSE!][!//
#define DBG_TRACE_FUNC                       STD_OFF
[!ENDIF!]
[!VAR "configCount"!][!CALL "DBG_RES_PREDEF_DID_GROUP_COUNT","M_resGroupName"="'Dbg.PredefDid.GenericFunc'"!][!ENDVAR!]
[!IF "$configCount > 0"!]
#define DBG_TRACE_GENERIC_FUNC               STD_ON
[!ELSE!][!//
#define DBG_TRACE_GENERIC_FUNC               STD_OFF
[!ENDIF!]
[!VAR "configCount"!][!CALL "DBG_RES_PREDEF_DID_GROUP_COUNT","M_resGroupName"="'Dbg.PredefDid.GenericState'"!][!ENDVAR!]
[!IF "$configCount > 0"!]
#define DBG_TRACE_GENERIC_STATE              STD_ON
[!ELSE!][!//
#define DBG_TRACE_GENERIC_STATE              STD_OFF
[!ENDIF!]

[!VAR "configState" = "'STD_OFF'"!]
[!VAR "sizeOfRingBuffer" = "0"!]
[!IF "node:exists(./DbgBuffering)"!]
  [!VAR "featureState" = "'STD_ON'"!]
  [!VAR "sizeOfRingBuffer"="./DbgBuffering/DbgBufferSize"!]
[!ENDIF!]
/* buffer configured */
#define DBG_CFG_BUFFER                       [!"$featureState"!]
/* size of ring buffer in bytes */
#define DBG_SIZE_OF_RING_BUFFER              [!"num:i($sizeOfRingBuffer)"!]U


/* - handle timestamp implemetation.
 * - says nothing about enable/disable at runtime, this will be done via
 *   defaultState, initState of Cfg_Internal
 */
[!IF "node:exists(./DbgTimestampConfiguration)"!]
#define DBG_CFG_TIMESTAMP                    STD_ON
  [!VAR "timestampChannel" = "''"!]
  [!IF "./DbgTimestampConfiguration/DbgOsTimestampTimer = 'true'"!]
#define DBG_TIMESTAMP_VIA_OS_TIMESTAMP       STD_ON
#define DBG_TIMESTAMP_VIA_OS                 STD_OFF
#define DBG_TIMESTAMP_VIA_GPT                STD_OFF
  [!ELSEIF "node:refexists(./DbgTimestampConfiguration/DbgOsCounterChannel)"!]
#define DBG_TIMESTAMP_VIA_OS_TIMESTAMP       STD_OFF
#define DBG_TIMESTAMP_VIA_OS                 STD_ON
#define DBG_TIMESTAMP_VIA_GPT                STD_OFF
    [!VAR "timestampChannel" = "node:name(as:ref(./DbgTimestampConfiguration/DbgOsCounterChannel))"!]
  [!ELSEIF "node:refexists(./DbgTimestampConfiguration/DbgGptChannel)"!]
#define DBG_TIMESTAMP_VIA_OS_TIMESTAMP       STD_OFF
#define DBG_TIMESTAMP_VIA_OS                 STD_OFF
#define DBG_TIMESTAMP_VIA_GPT                STD_ON
#define DBG_TIMESTAMP_GPT_MAX_VAL            [!"num:i(./DbgTimestampConfiguration/DbgGptChannelMaxValue)"!]
    [!VAR "timestampChannel" = "node:name(as:ref(./DbgTimestampConfiguration/DbgGptChannel))"!]
  [!ELSE!]
#define DBG_TIMESTAMP_VIA_OS_TIMESTAMP       STD_OFF
#define DBG_TIMESTAMP_VIA_OS                 STD_OFF
#define DBG_TIMESTAMP_VIA_GPT                STD_OFF
  [!ENDIF!]
  [!IF "./DbgTimestampConfiguration/DbgTimestampEmulation = 'true'"!]
#define DBG_TIMESTAMP_EMU                    STD_ON
  [!ELSE!]
#define DBG_TIMESTAMP_EMU                    STD_OFF
  [!ENDIF!]
#define DBG_TIMESTAMP_CHANNEL                [!"$timestampChannel"!]
[!ELSE!]
#define DBG_CFG_TIMESTAMP                    STD_OFF
#define DBG_TIMESTAMP_VIA_OS_TIMESTAMP       STD_OFF
#define DBG_TIMESTAMP_VIA_OS                 STD_OFF
#define DBG_TIMESTAMP_VIA_GPT                STD_OFF
#define DBG_TIMESTAMP_CHANNEL                           /* empty */
#define DBG_TIMESTAMP_EMU                    STD_OFF
[!ENDIF!]

[!IF "node:exists(./DbgDebugger)"!]
  [!IF "./DbgDebugger/DbgLauterbachSupport = 'Target'"!]
#define DBG_LAUTERBACH_TARGET_SUPPORT        STD_ON
#define DBG_LAUTERBACH_DIRECT_SUPPORT        STD_OFF
  [!ELSEIF "./DbgDebugger/DbgLauterbachSupport = 'Direct'"!]
#define DBG_LAUTERBACH_TARGET_SUPPORT        STD_OFF
    [!IF "node:exists(./DbgCommunication)"!]
#define DBG_LAUTERBACH_DIRECT_SUPPORT        STD_OFF
    [!ELSE!]
#define DBG_LAUTERBACH_DIRECT_SUPPORT        STD_ON
    [!ENDIF!]
  [!ELSE!]
#define DBG_LAUTERBACH_TARGET_SUPPORT        STD_OFF
#define DBG_LAUTERBACH_DIRECT_SUPPORT        STD_OFF
  [!ENDIF!]
[!ELSE!]
#define DBG_LAUTERBACH_TARGET_SUPPORT        STD_OFF
#define DBG_LAUTERBACH_DIRECT_SUPPORT        STD_OFF
[!ENDIF!]

[!IF "node:exists(./DbgCommunication)"!]
#define DBG_COM_VIA_TP_PDUR                  STD_ON
[!ELSE!]
#define DBG_COM_VIA_TP_PDUR                  STD_OFF
[!ENDIF!]

[!IF "./DbgGeneral/DbgProcessId = 'true'"!]
#define DBG_PROCESSID_SUPPORT                STD_ON
[!ELSE!]
#define DBG_PROCESSID_SUPPORT                STD_OFF
[!ENDIF!]

[!IF "./DbgGeneral/DbgMemoryProtection = 'true'"!]
#define DBG_MEMORY_PROTECTION_SUPPORT        STD_ON
[!ELSE!]
#define DBG_MEMORY_PROTECTION_SUPPORT        STD_OFF
[!ENDIF!]

/*******************[static DID name references]*****************************/
[!VAR "index"="0"!]
[!LOOP "./DbgMultipleConfigurationContainer/*[1]/DbgStaticDID/*"!]
  [!IF "./DbgStaticDIDNameRef != ''"!]
#define [!"./DbgStaticDIDNameRef"!] [!"num:i($index)"!]
  [!ENDIF!]
  [!VAR "index" = "$index+1"!]
[!ENDLOOP!]

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

[!ENDSELECT!]
[!ENDCODE!]
/** @} doxygen end group definition */
#endif /* if !defined( DBG_CFG_H ) */
/*==================[end of file]===========================================*/
