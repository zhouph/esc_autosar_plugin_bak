/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DBG_CFG_INTERNAL_H)
#define DBG_CFG_INTERNAL_H

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 19.7 (advisory)
 *  A function should be used in preference to a function-like macro.
 *
 *    Reason:
 *    Three reasons for the usage of function-like macros:
 *    - Performance
 *    - Readability
 *    - Possibility to switch Dbg functionality "completely off"
 *
*/

/*==================[includes]==============================================*/
[!CODE!]
[!AUTOSPACING!]
[!SELECT "as:modconf('Dbg')[1]"!]
#include <TSAutosar.h>              /* EB specific standard types */
#include <Std_Types.h>             /* AUTOSAR standard types (ON/OFF) */

[!LOOP "./DbgGeneral/DbgHeaderFileInclusion/*"!]
  [!INDENT "0"!]
  #include <[!"node:value(.)"!]>
  [!ENDINDENT!]
[!ENDLOOP!]

/*==================[version check]=========================================*/

/*==================[macros]================================================*/
#if (DBG_VALUE_TRACING==STD_OFF)
/* map generator used API to the AUTOSAR std API  -> last parameter not used */
/* Deviation MISRA-1 */
#define Dbg_InternalRTEVfbSignalSend(aU16,bU8,cU8,dU8,eU32) \
    Dbg_TraceRTEVfbSignalSend(aU16,bU8,cU8,dU8)
/* Deviation MISRA-1 */
#define Dbg_InternalRTEVfbSignalReceive(aU16,bU8,cU8,dU8,eU32) \
    Dbg_TraceRTEVfbSignalReceive(aU16,bU8,cU8,dU8)
#else
/* map generator used API to the EB API function  -> last parameter used */
/* Deviation MISRA-1 */
#define Dbg_InternalRTEVfbSignalSend(aU16,bU8,cU8,dU8,eU32) \
    Dbg_TraceRTEVfbSignalSendVal(aU16,bU8,cU8,dU8,eU32)
/* Deviation MISRA-1 */
#define Dbg_InternalRTEVfbSignalReceive(aU16,bU8,cU8,dU8,eU32) \
    Dbg_TraceRTEVfbSignalReceiveVal(aU16,bU8,cU8,dU8,eU32)
#endif

[!/* size in bytes for GetStatus */!]
[!VAR "numPredefinedDidConfigured" = "count(as:modconf('Dbg')[1]/DbgPredefinedDID/*)"!]
[!IF "$numPredefinedDidConfigured>0"!]
[!VAR "predefStatusSize" = "num:i(($numPredefinedDidConfigured+1) div 2)"!]
[!ELSE!]
[!VAR "predefStatusSize" = "num:i(1)"!]
[!ENDIF!]

[!VAR "directBufferSize" = "num:i(./DbgGeneral/DbgDirectBufferSize)"!]
[!/* predefStatusSize + 1 byte buffer info + 2 byte header + 1 byte ecu state */!]
[!IF "num:i($directBufferSize) <= num:i($predefStatusSize+4)"!]
[!VAR "directBufferSize" = "num:i($predefStatusSize+4)"!]
[!ENDIF!]
/* size of direct send buffer in bytes */
#define DBG_SIZE_OF_DIRECT_BUFFER  [!"num:i($directBufferSize)"!]U

/*==================[STATE CONFIG]==========================================*/
[!/* const flags */!][!//
[!VAR "cfgFlags" = "'DBG_CFG_FLAG_INIT'"!]
[!/* flags used after compiler startup, before Dbg_Init() */!][!//
[!VAR "defaultState" = "'DBG_STATE_INIT'"!]
[!/* flags used after Dbg_Init() */!][!//
[!VAR "initState" = "'DBG_STATE_INIT'"!]
[!VAR "timestampSize" = "0"!]
[!//
[!IF "node:exists(./DbgBuffering)"!]
  [!/* buffer enabled before and after Dbg_Init() */!][!//
  [!VAR "defaultState" = "concat($defaultState,'|DBG_STATE_BUFFERED')"!]
  [!VAR "initState" = "concat($initState,'|DBG_STATE_BUFFERED')"!]
  [!IF "./DbgBuffering/DbgBufferStrategy = 'OverwriteOldestEntries'"!]
    [!VAR "cfgFlags" ="concat($cfgFlags,'|DBG_CFG_FLAG_BUFFER_OVERWRITE')"!]
  [!ENDIF!]
  [!IF "./DbgBuffering/DbgBufferTransmission = 'StartOnHostRequest'"!]
    [!/* special handling in Dbg_Init() */!][!//
    [!VAR "cfgFlags" ="concat($cfgFlags,'|DBG_CFG_FLAG_TX_BUFFER_ON_REQ')"!]
  [!ENDIF!]
[!ELSE!]
  [!VAR "cfgFlags" ="concat($cfgFlags,'|DBG_CFG_FLAG_NO_BUFFER')"!]
[!ENDIF!]
[!//
[!/* no settings in defaultState because the timer are active after Dbg_Init() */!][!//
[!IF "node:exists(./DbgTimestampConfiguration)"!]
  [!IF "./DbgTimestampConfiguration/DbgGlobalTimestampActivation = 'TimestampOn'"!]
    [!VAR "initState" = "concat($initState,'|DBG_STATE_TIMESTAMP')"!]
  [!ELSE!]
    [!/* must be activated via host request*/!][!//
  [!ENDIF!]
  [!IF "./DbgTimestampConfiguration/DbgTimeStampSize = 'TimeStampSize_32Bit'"!]
     [!VAR "timestampSize" = "4"!]
  [!ELSE!]
     [!VAR "timestampSize" = "2"!]
  [!ENDIF!]
  [!IF "./DbgTimestampConfiguration/DbgTimestampEmulation = 'true'"!]
     [!VAR "cfgFlags" ="concat($cfgFlags,'|DBG_CFG_FLAG_EMULATE_TIME')"!]
     [!VAR "defaultState" = "concat($defaultState,'|DBG_STATE_TIMESTAMP')"!]
  [!ENDIF!]
[!ELSE!]
  [!VAR "cfgFlags" ="concat($cfgFlags,'|DBG_CFG_FLAG_NO_TIMESTAMP')"!]
[!ENDIF!]
[!//
/* init by compiler startup */
/* no real (Gpt or Os counter) timestamp possible */
#define DBG_DRV_STATE_CFG ([!"$defaultState"!]|DBG_STATE_ACTIVE)
/* init by Dbg_Init */
/* DBG_STATE_BUFFERED if configured
 * DBG_STATE_ACTIVE no DID's accepted */
#define DBG_DRV_DEFAULT_CFG \
    DBG_DRV_STATE_CFG, \
    ([!"$initState"!]|DBG_STATE_ACTIVE|DBG_STATE_DIRECT_TX), \
    ([!"$cfgFlags"!]), \
    [!"num:i($timestampSize)"!]U, /* size of timestamp */ \
    DBG_SIZE_OF_DIRECT_BUFFER-1, \
    DBG_SIZE_OF_RING_BUFFER-1

/*==================[DID CONFIG]==========================================*/
[!VAR "numStaticDid"="count(./DbgMultipleConfigurationContainer/*[1]/DbgStaticDID/*)"!]
#define DBG_NUM_STATIC_DID [!"num:i($numStaticDid)"!]U

[!VAR "numDynamicDid" = "0"!]
[!IF "node:exists(./DbgMultipleConfigurationContainer/*[1]/DbgMaxDynamicDID)"!]
[!VAR "numDynamicDid" = "node:value(./DbgMultipleConfigurationContainer/*[1]/DbgMaxDynamicDID)"!]
[!ENDIF!]
#define DBG_NUM_DYNAMIC_DID [!"num:i($numDynamicDid)"!]U

[!VAR "numPredefinedDid" = "count(ecu:list('Dbg.PredefDids'))"!]
#define DBG_NUM_PREDEF_DID [!"num:i($numPredefinedDid)"!]U

[!VAR "maxStandardDidSize" = "0"!]
[!VAR "i" = "1"!]
[!LOOP "./DbgMultipleConfigurationContainer/*[1]/DbgStaticDID/DbgStaticDIDData/*"!]
[!VAR "sumStandardDidSize" = "sum(./DbgAddressSizePair/*/DbgASSize)"!]
[!IF "$sumStandardDidSize > $maxStandardDidSize"!]
[!VAR "maxStandardDidSize" = "$sumStandardDidSize"!]
[!ENDIF!]
[!VAR "i" = "$i+1"!]
[!ENDLOOP!]
[!VAR "maxConfiguredStandardDidSize" = "./DbgMultipleConfigurationContainer/*[1]/DbgStandardDIDMaxSize"!]
[!IF "$maxStandardDidSize > $maxConfiguredStandardDidSize"!]
[!WARNING "DbgStandardDIDMaxSize is set to small, automatically increased size."!]
[!VAR "maxConfiguredStandardDidSize" = "$maxStandardDidSize"!]
[!ENDIF!]
[!IF "num:i($maxConfiguredStandardDidSize) = 0"!]
[!VAR "maxConfiguredStandardDidSize" = "1"!]
[!ENDIF!]
#define DBG_MAX_STD_DID_SIZE [!"num:i($maxConfiguredStandardDidSize)"!]U

#define DBG_PREDEF_DID_STATUS_SIZE ([!"$predefStatusSize"!]u)

[!VAR "numStaticAddrSizePairs" = "count(./DbgMultipleConfigurationContainer/*[1]/DbgStaticDID/*/DbgStaticDIDData/*[node:name(.)='DbgAddressSizePair'])"!]
#define DBG_NUM_STATIC_ADDR_SIZE_PAIRS [!"num:i($numStaticAddrSizePairs)"!]U

#define DBG_NUM_CONST_ADDR_SIZE_PAIRS DBG_NUM_STATIC_ADDR_SIZE_PAIRS+DBG_NUM_PREDEF_DID

[!VAR "ValueTracingSize" = "0"!]
[!IF "node:value(./DbgGeneral/DbgValueTracing)='true'"!]
[!VAR "ValueTracingSize" = "4"!]
[!ENDIF!]
#define DBG_VALUE_SIZE_RTE_VFB_RECEIVE ([!"num:i($ValueTracingSize)"!]u)
#define DBG_VALUE_SIZE_RTE_VFB_SEND    ([!"num:i($ValueTracingSize)"!]u)

[!VAR "MaxNrOfParams" = "0"!]
[!LOOP "as:modconf('Dbg')[1]/DbgPredefinedDID/*/DbgPredefinedDIDAddInfo/*/DbgFunctionGroup/*/DbgFunction/*"!]
  [!VAR "NrOfParams" = "count(./DbgParameter/*)"!]
  [!IF "$NrOfParams > $MaxNrOfParams"!]
    [!VAR "MaxNrOfParams" = "$NrOfParams"!]
  [!ENDIF!]
[!ENDLOOP!]
[!VAR "MaxLength" = "0"!]
[!FOR "i" = "1" TO "$MaxNrOfParams"!]
  [!VAR "MaxLength" = "$MaxLength+num:max(as:modconf('Dbg')[1]/DbgPredefinedDID/*/DbgPredefinedDIDAddInfo/*/DbgFunctionGroup/*/DbgFunction/*/DbgParameter/*[num:i($i)]/DbgParameterLength)"!]
[!ENDFOR!]

[!VAR "PredefFunctionLength" = "num:i($MaxLength)"!]
#define DBG_SIZE_GENERIC_FUNCTION ([!"$PredefFunctionLength"!]u)

[!VAR "PredefStateLength" = "num:i(0)"!]
[!IF "count(as:modconf('Dbg')[1]/DbgPredefinedDID/*/DbgPredefinedDIDAddInfo/*/DbgStateGroup/*)>0"!]
  [!VAR "PredefStateLength" = "num:i(2*num:max(as:modconf('Dbg')[1]/DbgPredefinedDID/*/DbgPredefinedDIDAddInfo/*/DbgStateGroup/*/DbgState/*/DbgStateLength))"!]
[!ENDIF!]
#define DBG_SIZE_GENERIC_STATE ([!"$PredefStateLength"!]u)

#define DBG_CONST_ADDR_SIZE_CFG \
{ \
[!INDENT "3"!]
    /* address, size pairs */ \
    [!LOOP "./DbgMultipleConfigurationContainer/*[1]/DbgStaticDID/*/DbgStaticDIDData/*[node:name(.)='DbgAddressSizePair']"!]
      [!VAR "absAddress" = "''"!]
      [!VAR "absSize" = "''"!]
      [!VAR "symbolName" = "''"!]
      [!IF "node:exists(./DbgASAbsoluteAddress)"!]
        [!VAR "absAddress" = "node:value(./DbgASAbsoluteAddress)"!]
      [!ENDIF!]
      [!IF "node:exists(./DbgASSize)"!]
        [!VAR "absSize" = "node:value(./DbgASSize)"!]
      [!ENDIF!]
      [!IF "node:exists(./DbgASNameRef)"!]
        [!VAR "symbolName" = "node:value(./DbgASNameRef)"!]
      [!ENDIF!]
      [!/* print one possible combination */!][!//
      [!IF "($absAddress != '') and ($absSize != '')"!]
    {(Dbg_DIDaddrType)[!"num:i($absAddress)"!],(Dbg_DIDsizeType)[!"num:i($absSize)"!]}, \
      [!ELSEIF "($symbolName != '') and ($absSize = '')"!]
    {(Dbg_DIDaddrType)&[!"$symbolName"!],(Dbg_DIDsizeType)DBG_SIZEOF([!"$symbolName"!])}, \
      [!ELSEIF "($symbolName != '') and ($absSize != '')"!]
    {(Dbg_DIDaddrType)&[!"$symbolName"!],(Dbg_DIDsizeType)[!"num:i($absSize)"!]}, \
      [!ELSE!]
        [!ERROR!]
          ERROR: Unsupported address size pair configuration for current node [!"node:name(.)"!]
        [!ENDERROR!]
      [!ENDIF!]
    [!ENDLOOP!]
    /* predefined DID: addr not used size=fix for predefined DID's*/\
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_STATUS)+DBG_PREDEF_DID_STATUS_SIZE)}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_RESERVED_236))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_GENERIC_FUNC)+DBG_SIZE_GENERIC_FUNCTION)}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_GENERIC_STATE)+DBG_SIZE_GENERIC_STATE)}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_POST_TASK_HOOK))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_PRE_TASK_HOOK))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_RUNNABLE_TERMINATE))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_RUNNABLE_START))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_RTE_CALL))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_RTE_COM_CALLBACK))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_RTE_VFB_RECEIVE)+DBG_VALUE_SIZE_RTE_VFB_RECEIVE)}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_RTE_VFB_SEND)+DBG_VALUE_SIZE_RTE_VFB_SEND)}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_RTE_COM_SIG_IV))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_RTE_COM_SIG_TX))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_RTE_COM_SIG_RX))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_DET_CALL))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_TIMESTAMP))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_FUNC_EXIT))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_FUNC_ENTRY))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_RESERVED_254))}, \
    {0u,(Dbg_DIDsizeType)(DBG_PREDEF_DID_SIZE(DBG_DID_RESERVED_255))}, \
[!ENDINDENT!]
}

#define DBG_NUM_DID DBG_NUM_STATIC_DID+DBG_NUM_DYNAMIC_DID+DBG_NUM_PREDEF_DID
#define DBG_DID_CFG \
{ \
    /* number of pairs, pointer to start pair */ \
    /* static DID's */ \
      [!VAR "currentIndex" = "0"!]
      [!LOOP "./DbgMultipleConfigurationContainer/*[1]/DbgStaticDID/*/DbgStaticDIDData"!]
        [!VAR "currentNumPairs" = "count(./*[node:name(.)='DbgAddressSizePair'])"!]
    {[!"num:i($currentNumPairs)"!],DBG_S_STATE_INIT,&Dbg_ConstDIDAddrSizePairs[[!"num:i($currentIndex)"!]]}, \
        [!VAR "currentIndex" = "$currentIndex+$currentNumPairs"!]
      [!ENDLOOP!]
    /* dynamic DID's */ \
      [!FOR "i" = "0" TO "$numDynamicDid - 1"!]
    {1u,DBG_S_STATE_INIT,&Dbg_VarDIDAddrSizePairs[[!"num:i($i)"!]]}, \
      [!ENDFOR!]
    /* dredef DID's */ \
    [!FOR "index_i"="1" TO "$numPredefinedDid"!]
      [!VAR "didTypeName" = "ecu:list('Dbg.PredefDids')[num:i($index_i)]"!]
      [!VAR "currentState" = "'DBG_S_STATE_INIT'"!]
      [!IF "count(./DbgPredefinedDID/*[DbgPredefinedDIDName=$didTypeName])>0"!]
        [!VAR "currentState" = "concat($currentState,'|DBG_S_STATE_CONFIGURED')"!]
      [!ENDIF!]
    {1u,[!"$currentState"!],&Dbg_ConstDIDAddrSizePairs[DBG_NUM_STATIC_ADDR_SIZE_PAIRS+[!"num:i($index_i - 1)"!]]}, \
    [!ENDFOR!]
}

/* DID state must be initialized by var init */
#define DBG_DID_STATE_CFG \
{ \
    /* static DID's */ \
    [!LOOP "./DbgMultipleConfigurationContainer/*[1]/DbgStaticDID/*"!]
        [!VAR "state" = "'DBG_STATE_INIT'"!]
        [!IF "./DbgStaticDIDActivation = 'true'"!]
          [!VAR "state" = "concat($state,'|DBG_STATE_ACTIVE')"!]
        [!ENDIF!]
        [!IF "./DbgStaticTimeStampActivation = 'true'"!]
          [!VAR "state" = "concat($state,'|DBG_STATE_TIMESTAMP')"!]
        [!ENDIF!]
        [!IF "./DbgStaticDIDBuffering = 'true'"!]
          [!VAR "state" = "concat($state,'|DBG_STATE_BUFFERED')"!]
        [!ENDIF!]
    ([!"$state"!]), \
      [!ENDLOOP!]
    /* dynamic DID's */ \
      [!FOR "i" = "0" TO "$numDynamicDid - 1"!]
    (DBG_STATE_INIT), /* dynamic DID's must be disabled at startup */ \
      [!ENDFOR!]
    /* predefined DID's */ \
    [!FOR "index_i"="1" TO "$numPredefinedDid"!]
      [!VAR "didTypeName" = "ecu:list('Dbg.PredefDids')[num:i($index_i)]"!]
      [!IF "count(./DbgPredefinedDID/*[DbgPredefinedDIDName=$didTypeName])>0"!]
        [!SELECT "./DbgPredefinedDID/*[DbgPredefinedDIDName=$didTypeName]"!]
        [!VAR "state" = "'DBG_STATE_INIT'"!]
        [!IF "./DbgPredefinedDIDActivation = 'true'"!]
          [!VAR "state" = "concat($state,'|DBG_STATE_ACTIVE')"!]
        [!ENDIF!]
        [!IF "./DbgPredefinedDIDTimeStampActivation = 'true'"!]
          [!VAR "state" = "concat($state,'|DBG_STATE_TIMESTAMP')"!]
        [!ENDIF!]
        [!IF "./DbgPredefinedDIDBuffering = 'true'"!]
          [!VAR "state" = "concat($state,'|DBG_STATE_BUFFERED')"!]
        [!ENDIF!]
        [!ENDSELECT!]
    ([!"$state"!]), /* [!"num:i(234 + $index_i)"!] [!"$didTypeName"!] */ \
      [!ELSE!]
    (DBG_STATE_INIT), /* [!"num:i(234 + $index_i)"!] [!"$didTypeName"!] */ \
      [!ENDIF!]
    [!ENDFOR!]
}

[!VAR "txBufferSize_ErrMsg" = "''"!]

[!/* Maximum size of constant predefined DIDs is 5 bytes + size of value tracing (Dbg_TraceRTEVfbSignalSend with value tracing) */!]
[!VAR "txBufferSize" = "num:i(5) + num:i($ValueTracingSize)"!]

[!IF "num:i($txBufferSize + num:i(2) + $timestampSize) > num:i(255)"!]
  [!VAR "txBufferSize_ErrMsg" = "'Maybe disabling ValueTracing or Timestamps will lower the buffer size.'"!]
[!ENDIF!]

[!/* Dbg_TraceGenericFunction size: 5 bytes + variable part */!]
[!IF "num:i($PredefFunctionLength + num:i(5)) > num:i($txBufferSize)"!]
  [!VAR "txBufferSize" = "num:i($PredefFunctionLength + num:i(5))"!]

  [!IF "num:i($txBufferSize + num:i(2) + $timestampSize) > num:i(255)"!]
    [!VAR "txBufferSize_ErrMsg" = "'Maybe reducing the amount of Os functions for tracing or removing some DbgParameter with a higher DbgParameterLength will lower the buffer size. Additionally reducing the DbgTimeStampSize can also lower the buffer size.'"!]
  [!ENDIF!]
[!ENDIF!]

[!/* Dbg_TraceGenericStatemachine size: 4 bytes + variable part */!]
[!IF "num:i($PredefStateLength + num:i(4)) > $txBufferSize"!]
  [!VAR "txBufferSize" = "num:i($PredefStateLength + num:i(4))"!]

  [!IF "num:i($txBufferSize + num:i(2) + $timestampSize) > num:i(255)"!]
    [!VAR "txBufferSize_ErrMsg" = "'Maybe reducing the amount of Os states for tracing or removing the DbgState with the highest DbgStateLength will lower the buffer size. Additionally reducing the DbgTimeStampSize can also lower the buffer size.'"!]
  [!ENDIF!]
[!ENDIF!]

[!IF "$directBufferSize > $txBufferSize"!]
  [!VAR "txBufferSize" = "$directBufferSize"!]

  [!IF "num:i($txBufferSize + num:i(2) + $timestampSize) > num:i(255)"!]
    [!VAR "txBufferSize_ErrMsg" = "'Maybe reducing the DbgDirectBufferSize or the DbgTimeStampSize will lower the buffer size.'"!]
  [!ENDIF!]
[!ENDIF!]

[!/* Add 2 bytes for header and size of timestamp */!]
[!VAR "txBufferSize" = "num:i($txBufferSize + num:i(2) + $timestampSize)"!]

[!/* Error message if the tx buffer size is larger than 255, because some variables of type uint8 will be set with that value */!]
[!IF "num:i($txBufferSize) > num:i(255)"!]
  [!ERROR!]ERROR: Transmit buffer size will be larger than 255 which is not supported. [!IF "$txBufferSize_ErrMsg != ''"!] [!"$txBufferSize_ErrMsg"!] [!ENDIF!][!ENDERROR!]
[!ENDIF!]
/* Transmit buffer size */
#define DBG_TX_BUFFER_SIZE [!"$txBufferSize"!]U

/*==================[TP PDUR CONFIG]========================================*/
[!IF "node:exists(./DbgCommunication)"!]

  [!IF "count(as:modconf('PduR')/PduRRoutingTables/*[1]/PduRRoutingTable/*/PduRRoutingPath/*/PduRSrcPdu[PduRSrcPduRef = node:current()/DbgCommunication/DbgTxPdu/DbgSendIPduRef]) != 1"!]
    [!ERROR!]ERROR: ./DbgCommunication/DbgTxPdu/DbgSendIPduRef not found in PduR configuration[!ENDERROR!]
  [!ELSE!]
#define DBG_COM_TX_CONFIRMATION_PDU_ID [!"num:i(./DbgCommunication/DbgTxPdu/DbgTxConfirmationPduId)"!]U
  [!VAR "txIPduRef" = "node:path(node:current()/DbgCommunication/DbgTxPdu/DbgSendIPduRef)"!]
  [!VAR "txIPduEcuCRef" = "node:ref($txIPduRef)"!]
  [!VAR "txPduLength" = "node:ref($txIPduEcuCRef)/PduLength"!]
#define DBG_COM_TX_PDU_LENGTH [!"$txPduLength"!]U
  [!ENDIF!]
  
  [!IF "count(as:modconf('PduR')/PduRRoutingTables/*[1]/PduRRoutingTable/*/PduRRoutingPath/*/PduRSrcPdu[PduRSrcPduRef = node:current()/DbgCommunication/DbgRxPdu/DbgReceiveIPduRef]) != 1"!]
    [!ERROR!]ERROR: ./DbgCommunication/DbgRxPdu/DbgReceiveIPduRef not found in PduR configuration[!ENDERROR!]
  [!ELSE!]
#define DBG_COM_RX_PDU_ID [!"num:i(./DbgCommunication/DbgRxPdu/DbgRxPduId)"!]U
  [!VAR "rxIPduRef" = "node:path(node:current()/DbgCommunication/DbgRxPdu/DbgReceiveIPduRef)"!]
  [!VAR "rxIPduEcuCRef" = "node:ref($rxIPduRef)"!]
  [!VAR "rxPduLength" = "node:ref($rxIPduEcuCRef)/PduLength"!]
#define DBG_COM_RX_PDU_LENGTH [!"$rxPduLength"!]U
  [!ENDIF!]

#define DBG_COM_TP_PDUR_CFG \
{ \
  DBG_COM_TX_CONFIRMATION_PDU_ID, /* txId */ \
  DBG_COM_RX_PDU_ID /* rxId */ \
}
[!ENDIF!]
/*==================[Process Id]============================================*/
[!VAR "numOfProcesses" = "num:i(count(as:modconf('Os')[1]/OsTask/*)+count(as:modconf('Os')[1]/OsIsr/*)+2)"!]
[!IF "$numOfProcesses > 65536"!]
#define DBG_PROCESSID_SIZE 4U
[!ELSEIF "$numOfProcesses > 256"!]
#define DBG_PROCESSID_SIZE 2U
[!ELSE!]
#define DBG_PROCESSID_SIZE 1U
[!ENDIF!]
/*==================[type definitions]======================================*/
[!IF "$numOfProcesses > 65536"!]
typedef uint32 Dbg_ProcessIdType;
[!ELSEIF "$numOfProcesses > 256"!]
typedef uint16 Dbg_ProcessIdType;
[!ELSE!]
typedef uint8 Dbg_ProcessIdType;
[!ENDIF!]

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

[!ENDSELECT!]
[!ENDCODE!]

#endif /* if !defined( DBG_CFG_INTERNAL_H ) */
/*==================[end of file]===========================================*/
