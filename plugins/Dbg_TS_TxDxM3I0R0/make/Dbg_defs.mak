# \file
#
# \brief AUTOSAR Dbg
#
# This file contains the implementation of the AUTOSAR
# module Dbg.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

ifeq ($(DBG_ROOT),)
  Dbg_CORE_PATH    = $(SSC_ROOT)\Dbg_$(Dbg_VARIANT)
else
  Dbg_CORE_PATH    = $(DBG_ROOT)
endif

Dbg_OUTPUT_PATH    := $(AUTOSAR_BASE_OUTPUT_PATH)

Dbg_GEN_FILES      := \
        $(Dbg_OUTPUT_PATH)\include\Dbg_Cfg.h \
        $(Dbg_OUTPUT_PATH)\include\Dbg_Cfg_Hw.h \
        $(Dbg_OUTPUT_PATH)\include\Dbg_Cfg_Internal.h \
        $(Dbg_OUTPUT_PATH)\make\Dbg_Cfg.mak \
        $(Dbg_OUTPUT_PATH)\src\Dbg_Rte_Hooks.c

TRESOS_GEN_FILES        += $(Dbg_GEN_FILES)

#################################################################
# REGISTRY
SSC_PLUGINS                += Dbg
Dbg_DEPENDENT_PLUGINS := base_make tresos
Dbg_VERSION           := 2.00.00
Dbg_DESCRIPTION       := Dbg Basic Software Makefile PlugIn for Autosar
CC_INCLUDE_PATH       += $(Dbg_CORE_PATH)\include \
                         $(Dbg_OUTPUT_PATH)\include
ASM_INCLUDE_PATH           +=

