/**
 * \file
 *
 * \brief AUTOSAR Xcp
 *
 * This file contains the implementation of the AUTOSAR
 * module Xcp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*==================[inclusions]============================================*/

#include <Xcp_Trace.h>
#include <Std_Types.h>         /* AUTOSAR standard types */
#include <TSAutosar.h>         /* EB specific standard types */
#include <SchM_Xcp.h>          /* Needed for exclusive area definition */

#include <Xcp.h>               /* Module public API */
#include <Xcp_Cbk.h>           /* Callbacks of Xcp */
#include <Xcp_Int.h>           /* Internal API */

/*==================[macros]================================================*/

#if (defined XCP_ACT_IGNORE)
# error XCP_ACT_IGNORE already defined
#endif
/** \brief Action indicates that the received package is ignored */
#define XCP_ACT_IGNORE                       0U

#if (defined XCP_ACT_DATA_STIM)
# error XCP_ACT_DATA_STIM already defined
#endif
/** \brief Action indicates that a STIM data package is received */
#define XCP_ACT_DATA_STIM                    1U

#if (defined XCP_ACT_Q_CMD)
# error XCP_ACT_Q_CMD already defined
#endif
/** \brief Action indicates that the received command package will be queued */
#define XCP_ACT_Q_CMD                2U

#if (defined XCP_ACT_UNQ_CMD_SYNC)
# error XCP_ACT_UNQ_CMD_SYNC already defined
#endif
/** \brief Action indicates the reception of sync command package. */
#define XCP_ACT_UNQ_CMD_SYNC         3U

#if (defined XCP_ACT_UNQ_CMD_DISCONNECT)
# error XCP_ACT_UNQ_CMD_DISCONNECT already defined
#endif
/** \brief Action indicates the reception of disconnect command package. */
#define XCP_ACT_UNQ_CMD_DISCONNECT   4U

#if (defined XCP_ACT_UNQ_CMD_ABORTBUSY)
# error XCP_ACT_UNQ_CMD_ABORTBUSY already defined
#endif
/** \brief Action indicates that the received command cannot be queued due to
 **        BUSY state of Xcp command processor. */
#define XCP_ACT_UNQ_CMD_ABORTBUSY   5U

#if (defined XCP_ACT_UNQ_CMD_SYNTAXERR)
# error XCP_ACT_UNQ_CMD_SYNTAXERR already defined
#endif
/** \brief Action indicates that the received command cannot be queued due to
 **        data inconsistency. */
#define XCP_ACT_UNQ_CMD_SYNTAXERR    6U

/*==================[type definitions]======================================*/

/** \brief Definition of Xcp received action type */
typedef uint8 Xcp_RcvActionType;

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

#define XCP_START_SEC_CODE
#include <MemMap.h>

/** \brief Function to extract the action from the received PDU.
 **
 ** The action extracted at this point is processed by:
 ** - Xcp_ProcessRcvCmdAction() for the case when a CTO is received;
 ** - Xcp_ProcessRcvDataAction() for the case when a DTO is received.
 **
 ** \param[in]   SduLength Data length of the received PDU.
 ** \param[in]   SduDataPtr Pointer to the received PDU data.
 ** \return      The action which is taken with the received package.
 ** \retval      XCP_ACT_IGNORE              Ignore PDU and don't set any action;
 ** \retval      XCP_ACT_DATA_STIM           Treat the PDU as a data STIM package;
 ** \retval      XCP_ACT_Q_CMD               Queue the command package;
 ** \retval      XCP_ACT_UNQ_CMD_SYNC        Treat the PDU as a SYNC command;
 ** \retval      XCP_ACT_UNQ_CMD_DISCONNECT  Treat the PDU as a DISCONNECT command;
 ** \retval      XCP_ACT_UNQ_CMD_ABORTBUSY   Ignore the package and set the action to
 **                                          abort the busy command;
 ** \retval      XCP_ACT_UNQ_CMD_SYNTAXERR   Ignore the package and set the action to
 **                                          send an ERR_CMD_SYNTAX response packet.
 */
STATIC FUNC(Xcp_RcvActionType, XCP_CODE) Xcp_GetRcvAction
(
  PduLengthType SduLength,
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) SduDataPtr
);

/** \brief Function to process the received CTO with the action returned by Xcp_GetRcvAction().
 **
 ** \param[in]   SduLength Data length of the received PDU.
 ** \param[in]   SduDataPtr Pointer to the received PDU data.
 ** \param[in]   RcvAction Action which has to be done with the received CTO.
 */
STATIC FUNC(void, XCP_CODE) Xcp_ProcessRcvCmdAction
(
  PduLengthType SduLength,
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) SduDataPtr,
  Xcp_RcvActionType RcvAction
);

#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
/** \brief Function to process the received DTO with the action returned by Xcp_GetRcvAction().
 **
 ** \param[in]   SduLength Data length of the received PDU.
 ** \param[in]   SduDataPtr Pointer to the received PDU data.
 ** \param[in]   RcvAction Action which has to be done with the received DTO.
 */
STATIC FUNC(void, XCP_CODE) Xcp_ProcessRcvDataAction
(
  PduLengthType SduLength,
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) SduDataPtr,
  Xcp_RcvActionType RcvAction
);
#endif /* XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK */

#if (XCP_ON_ETHERNET_ENABLED == STD_ON)
#if (XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_ON)
/** \brief Function which copies the remote address from the socket having the SrcSoConId to
 **        the socket having the DstSoConId.
 **
 ** The copy of the remote address is done with/without reseting the port value.
 **
 ** \param[in]   SrcSoConId The socket ID which is the source of the remote address.
 ** \param[in]   DstSoConId The socket ID which is the destination of the remote address.
 ** \param[in]   ClearPortID: - TRUE The port value is cleared before copying to the destination
 **                                  socket;
 **                           - FALSE The port value is left unchanged before copying to the
 **                                   destination socket.
 */
STATIC FUNC(void, XCP_CODE) Xcp_CopySoAdRemoteAddress
(
  SoAd_SoConIdType SrcSoConId,
  SoAd_SoConIdType DstSoConId,
  boolean ClearPortID
);
#endif /* XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_ON */
#endif /* XCP_ON_ETHERNET_ENABLED == STD_ON */

#if ((XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON) || \
     (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON))


/** \brief Function to return whether the command is allowed to be queued during a master block mode.
 ** If there is no ongoing master block mode, the function returns true if the queue permits it.
 ** Otherwise, the function checks if the command is related to the ongoing master block mode (i.e.
 ** PROGRAM_NEXT or DOWNLOAD next) and returns true only if so.
 **
 ** \return TRUE the command can be queued, FALSE otherwise
 **  */
STATIC FUNC(boolean, XCP_CODE) Xcp_MasterBlockModeAllowCmd(uint8 Pid);

#endif /* (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON) ||
          (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON) */

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

#define XCP_START_SEC_CODE
#include <MemMap.h>


FUNC(void, XCP_CODE) Xcp_RxIndication
(
  PduLengthType SduLength,
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) SduDataPtr
)
{

  /* Variable to determine the Xcp action response */
  Xcp_RcvActionType RcvAction;

  DBG_XCP_RXINDICATION_ENTRY(SduLength,SduDataPtr);

  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* determine what to do with the incoming message */
  RcvAction = Xcp_GetRcvAction(SduLength,SduDataPtr);
  /* process CTO incoming message */
  Xcp_ProcessRcvCmdAction(SduLength,SduDataPtr,RcvAction);

  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
  /* process DTO incoming message */
  Xcp_ProcessRcvDataAction(SduLength,SduDataPtr,RcvAction);
#endif /* XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK */

#if (XCP_ON_ETHERNET_ENABLED == STD_ON)
#if (XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_ON)
  /* Reconfigure the RX socket for each reception to have the remote port 0 (wild card - accept all
   * ports). Master can change its port but not the IP. */
  Xcp_CopySoAdRemoteAddress(XCP_SOAD_SOCKET_RX_ID,XCP_SOAD_SOCKET_RX_ID,TRUE);
#endif /* XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_ON */
#endif /* XCP_ON_ETHERNET_ENABLED == STD_ON */

  DBG_XCP_RXINDICATION_EXIT(SduLength,SduDataPtr);
}

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function definitions]=========================*/

#define XCP_START_SEC_CODE
#include <MemMap.h>

STATIC FUNC(Xcp_RcvActionType, XCP_CODE) Xcp_GetRcvAction
(
  PduLengthType SduLength,
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) SduDataPtr
)
{
  /* Local variable to hold the data packet PID (the first byte) */
  uint8 Pid = SduDataPtr[XCP_RES_PID_INDEX];
  /* Local variable to hold the Xcp_GetRcvAction() return value.
   * Initialize variable to ignore action. Change the value when a defined
   * type of action in certain conditions is received. */
  Xcp_RcvActionType RcvAction = XCP_ACT_IGNORE;

  DBG_XCP_GETRCVACTION_ENTRY(SduLength,SduDataPtr);

  /* If the PID corresponds to CMD packet */
  if (XCP_PID_IS_CMD(Pid))
  {
    /* If the command packets is a valid CTO */
    if (Xcp_CheckCommandLength(SduLength, SduDataPtr) == XCP_E_OK)
    {
      switch (Xcp_State)
      {
#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
      case XCP_STATE_RESUME: /* RESUME mode accepts the same commands like DISCONNECT mode, fall through*/
#endif /* XCP_STORE_DAQ_SUPPORTED == STD_ON */
      case XCP_STATE_DISCONNECTED:
      {
        /* accept only CONNECT or auto detection when the slave and master are disconnected */
        if( (XCP_PID_IS_CONNECT(Pid))
#if (STD_ON == XCP_ON_CAN_ENABLED)
          ||(XCP_PID_IS_TRANSPORT_LAYER(Pid))
#endif
          )
        {
          /* if the command queue is full, ignore the command, else queue the command */
          if (Xcp_CommandQueueIsFull() == FALSE)
          {
            /* Set action to queue command */
            RcvAction = XCP_ACT_Q_CMD;
          }
        }
        break;
      }
      case XCP_STATE_CONNECTED:
      {
        /* If the PID corresponds to SYNCH command packet */
        if (XCP_PID_IS_SYNCH(Pid))
        {
          /* Set action to process the SYNC command. */
          RcvAction = XCP_ACT_UNQ_CMD_SYNC;
        }
        /* If the PID corresponds to DISCONNECT command packet*/
        else if (XCP_PID_IS_DISCONNECT(Pid))
        {
          /* Set action to process the DISCONNECT command. */
          RcvAction = XCP_ACT_UNQ_CMD_DISCONNECT;
        }
        /* Normal command (not SYNCH or DISCONNECT) was received */
        else
        {
          /* if the command queue is full, ignore the command, else set the right command action */
          if (Xcp_CommandQueueIsFull() == FALSE)
          {
            /* If the state is BUSY, the command is ignored and the busy command is aborted */
            if (Xcp_GetCommandProcessorState() == XCP_CMDPROCESSOR_BUSY)
            {
              /* Set action to abort the BUSY command */
              RcvAction = XCP_ACT_UNQ_CMD_ABORTBUSY;
            }
            else
            {
#if ((XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON) || \
     (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON))
              /* Check that there is an ongoing master block transfer and if the command is
               * allowed during one */
              if (Xcp_MasterBlockModeAllowCmd(Pid))
              {
                /* Set action to queue command */
                RcvAction = XCP_ACT_Q_CMD;
              }
#else /* Master block mode is not enabled */
              /* Set action to queue command */
              RcvAction = XCP_ACT_Q_CMD;
#endif
            }
          }
        }
        break;
      }
      default:
        /* Ignore the packet */
        break;
      }
    }
    else
    {
      /* Set action to send an ERR_CMD_SYNTAX response package */
      RcvAction = XCP_ACT_UNQ_CMD_SYNTAXERR;
    }
  }
#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
  else /* This is a DTO packet */
  {
    /* Check the length of the DTO packet */
    if ( SduLength <= XCP_MAX_DTO )
    {
      /* Set action to STIM DATA direction */
      RcvAction = XCP_ACT_DATA_STIM;
    }
  }
#endif

  DBG_XCP_GETRCVACTION_EXIT(SduLength,SduDataPtr,RcvAction);

  /* Return action */
  return RcvAction;
}

STATIC FUNC(void, XCP_CODE) Xcp_ProcessRcvCmdAction
(
  PduLengthType SduLength,
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) SduDataPtr,
  Xcp_RcvActionType RcvAction
)
{
  /* Local variable to hold the data packet PID (the first byte) */
  uint8 Pid = SduDataPtr[XCP_RES_PID_INDEX];

  DBG_XCP_PROCESSRCVCMDACTION_ENTRY(SduLength,SduDataPtr,RcvAction);

  switch(RcvAction)
  {
  case XCP_ACT_Q_CMD:
#if (XCP_ON_ETHERNET_ENABLED == STD_ON)
#if (XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_ON)
    if (XCP_PID_IS_CONNECT(Pid))
    {
      /* If the command is CONNECT, we must configure the socket for transmission with the IP and
       * port received on the reception socket */
      Xcp_CopySoAdRemoteAddress(XCP_SOAD_SOCKET_RX_ID,XCP_SOAD_SOCKET_TX_ID,FALSE);
    }
#endif /* XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_ON */
#endif /* XCP_ON_ETHERNET_ENABLED == STD_ON */

    /* Queue the command */
    Xcp_InsertCommand(SduLength, SduDataPtr);

#if (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON)
    if (Pid == XCP_CMD_PROGRAM_PID)
    {
      /* if the command is PROGRAM we set the programming in block mode transfer.
       * From now on we accept only PROGRAM_NEXT commands in the command queue until the
       * queue is emptied */
      Xcp_SetProgrammingBlockMode();
    }
#endif /* XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON */

#if (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON)
    if (Pid == XCP_CMD_DOWNLOAD_PID)
    {
      /* if the command is DOWNLOAD we set the download in block mode flag.
       * From now on we accept only DOWNLOAD_NEXT commands in the command queue until the
       * queue is emptied */
      Xcp_SetDownloadInBlockMode();
    }
#endif /* XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON */
    break;
  case XCP_ACT_UNQ_CMD_SYNC:
    /* Set a flag to signal that SYNCH needs to be processed */
    Xcp_SetSynchCmdPending();
    break;
  case XCP_ACT_UNQ_CMD_DISCONNECT:
    /* Set a flag to signal that DISCONNECT needs to be processed */
    Xcp_SetDisconnectCmdPending();
    break;
  case XCP_ACT_UNQ_CMD_ABORTBUSY:
    /* Abort any BUSY command, set the command processor state to IDLE */
    Xcp_AbortBusyCommand();
    break;
  case XCP_ACT_UNQ_CMD_SYNTAXERR:
    /* The CTO data packet is not valid.
       Send an ERR_CMD_SYNTAX response packet */
    Xcp_SendErrorCmdSyntaxPacket(Pid);
    break;
  case XCP_ACT_IGNORE:
    /* Ignore the packet */
#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
  case XCP_ACT_DATA_STIM:
    /* Ignore the packet. This type of action is processed by Xcp_ProcessRcvDataAction() */
#endif /* XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK */
  default:
    /* Ignore the packet */
    break;
  }

  DBG_XCP_PROCESSRCVCMDACTION_EXIT(SduLength,SduDataPtr,RcvAction);
}

#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
STATIC FUNC(void, XCP_CODE) Xcp_ProcessRcvDataAction
(
  PduLengthType SduLength,
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) SduDataPtr,
  Xcp_RcvActionType RcvAction
)
{

  DBG_XCP_PROCESSRCVDATAACTION_ENTRY(SduLength,SduDataPtr,RcvAction);

  if(RcvAction == XCP_ACT_DATA_STIM)
  {
    /* Insert DTO packet into the STIM Buffer */
    Xcp_InsertSTIM(SduLength, SduDataPtr);
  }

  DBG_XCP_PROCESSRCVDATAACTION_EXIT(SduLength,SduDataPtr,RcvAction);
}
#endif /* XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK */

#if (XCP_ON_ETHERNET_ENABLED == STD_ON)
#if (XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_ON)
STATIC FUNC(void, XCP_CODE) Xcp_CopySoAdRemoteAddress
(
    SoAd_SoConIdType SrcSoConId,
    SoAd_SoConIdType DstSoConId,
    boolean ClearPortID
)
{
  /* Variable to hold the remote address received on the Rx socket */
  TcpIp_SockAddrType RemoteAddress;

  DBG_XCP_COPYSOADREMOTEADDRESS_ENTRY(SrcSoConId,DstSoConId,ClearPortID);

  /* Get the current address for the Rx socket */
  if (SoAd_GetRemoteAddr(SrcSoConId, &RemoteAddress) == E_OK)
  {
    /* We need a conversion to TcpIp_SockAddrInetType in order to gain access to the port stored at
     * the remote address  */
    if(ClearPortID)
    {
    ((P2VAR(TcpIp_SockAddrInetType, TYPEDEF, XCP_APPL_DATA))
                      (P2VAR(void, TYPEDEF, XCP_APPL_DATA))&RemoteAddress)->port = 0U;
    }

    /* Set the Rx socket with the modified port */
    if (SoAd_SetRemoteAddr(DstSoConId, &RemoteAddress) == E_OK)
    {
      /* E_OK */
    }
    /* CHECK: NOPARSE */
    else
    {
      /* SoAd_SetRemoteAddr() cannot return E_NOT_OK if the configuration is set correctly.
       * For details, check the Error handling view section from the design document. */
      XCP_UNREACHABLE_CODE_ASSERT(DstSoConId);
    }
    /* CHECK: PARSE */
  }
  /* CHECK: NOPARSE */
  else
  {
    /* SoAd_GetRemoteAddr() cannot return E_NOT_OK if the configuration is set correctly.
     * For details, check the Error handling view section from the design document. */
    XCP_UNREACHABLE_CODE_ASSERT(SrcSoConId);
  }

  DBG_XCP_COPYSOADREMOTEADDRESS_EXIT(SrcSoConId,DstSoConId,ClearPortID);
}
#endif /* XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_ON */
#endif /* XCP_ON_ETHERNET_ENABLED == STD_ON */

#if ((XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON) || \
     (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON))

STATIC FUNC(boolean, XCP_CODE)  Xcp_MasterBlockModeAllowCmd(uint8 Pid)
{
  /* The return value */
  boolean RetValue = FALSE;

  /* Check that we have a master block mode in progress */
  if (Xcp_IsMasterBlockModeInProgress() == FALSE)
  {
    /* No master block mode active, accept command if the queue allows it */
    if (Xcp_CommandQueueIsEmpty() == TRUE)
    {
      RetValue = TRUE;
    }
  }
  else
  {
#if (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON)
    if (Xcp_IsDownloadInBlockMode())
    {
      /* If there is a download sequence in progress accept only DOWNLOAD_NEXT commands */
      if (Pid == XCP_CMD_DOWNLOAD_NEXT_PID)
      {
        RetValue = TRUE;
      }
    }
#endif
#if (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON)
    if (Xcp_IsProgrammingBlockMode())
    {
      /* If there is a block mode programming in progress accept only PROGRAM_NEXT commands */
      if (Pid == XCP_CMD_PROGRAM_NEXT_PID)
      {
        RetValue = TRUE;
      }
    }
#endif
  }
  return RetValue;
}

#endif /* (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON) ||
          (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON) */

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

/*==================[end of file]===========================================*/
