/**
 * \file
 *
 * \brief AUTOSAR Xcp
 *
 * This file contains the implementation of the AUTOSAR
 * module Xcp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*  MISRA-C:2004 Deviation List
 *
 *  MISRA-1) Deviated Rule: 17.4 (required)
 *    "Array indexing shall be the only allowed form of pointer arithmetic."
 *
 *    Reason: In special cases pointer arithmetic is more efficient in run
 *    time and/or code size.
 *
 *  MISRA-2) Deviated Rule: 12.5 (required)
 *    "The operands of a logical '&&' or '||' shall be 'primary-expressions'."
 *
 *    Reason: There seems to be no issue. The operands are only
 *    primary-expressions.
 *
 *  MISRA-3) Deviated Rule: 18.4 (required)
 *      "Unions shall not be used."
 *
 *    Reason: Pointer is necessary to be used for both const and non-const positions to prevent
 *            compiler warnings. Usage is allowed like this according to the C standard as qualified
 *            and unqualified types have the same alignment and size restrictions, and the first
 *            element within a union starts at the same memory position.
 *
 *  MISRA-4) Deviated Rule: 15.5 (required)
 *      "Every switch clause shall have at least one case clause"
 *
 *    Reason: The troublesome switch is due to configuration options: if no mode is supported the
 *            default clause will fetch this and respond with a corresponding error message. The
 *            switch is used to improve readability.
 *
 *  MISRA-5) Deviated Rule: 11.3 (advisory)
 *      "A cast should not be performed between a pointer type and an integral type."
 *
 *    Reason: Cast is unavoidable - we construct the address of an ODT Entry based on input bytes.
 *
 *  MISRA-6) Deviated Rule: 11.4 (advisory)
 *      "A cast should not be performed between a pointer to object type and a different pointer to object type."
 *
 *    Reason: Dynamic DAQ lists are stored into a memory area of uint8 type with fixed size. We
 *    need to convert area sections into corresponding DAQ list, ODT and ODT entries types.
 *
 *  MISRA-7) Deviated Rule: 16.10 (advisory)
 *      16.10: If a function returns error information, then that error information shall be tested"
 *
 *    Reason: There is no meaningful way to react. The error was not ignored, but processed by the
 *    function which reported it.
 *
 */

/*==================[inclusions]============================================*/

#include <Xcp_Trace.h>
#include <Std_Types.h>          /* AUTOSAR standard types */
#include <TSAutosar.h>          /* EB specific standard types */
#include <TSMem.h>              /* EB memory functions */
#include <SchM_Xcp.h>           /* Needed for exclusive area definition */

#include <Xcp.h>                /* Module public API */
#include <Xcp_Int.h>            /* Internal defines and declarations */
#include <Xcp_Int_Cfg.h>        /* Internal configuration */
#include <Xcp_UserCallouts.h>   /* XCP user callout functions */
#include <Xcp_Cbk.h>            /* Callbacks and callouts of Xcp */

#if (XCP_TIMESTAMP_TYPE != XCP_NO_TIME_STAMP_TS_MASK)

#if (XCP_USER_TIMESTAMP_SUPPORTED == STD_OFF)
#include <os.h>                  /* Os counter is required */
#endif

#endif  /* (XCP_TIMESTAMP_TYPE != XCP_NO_TIME_STAMP_TS_MASK) */

#if (XCP_BUILD_CHECKSUM_SUPPORT == STD_ON)
#if ((XCP_BUILD_CHECKSUM_TYPE == XCP_CRC_16_CITT)||(XCP_BUILD_CHECKSUM_TYPE == XCP_CRC_32))
# include <Crc.h>               /* CRC is required for BUILD_CHECKSUM command */
#endif
#endif

#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
#include <NvM.h>                /* NvM API */
#endif

/*==================[macros]================================================*/

/** \brief Increment MTA according to a size in AG
 **
 ** This macro increments the address of the MTA according to the given
 ** parameter Size.
 **
 ** \param[in] Size Size to increment the MTA in AG
 */
#define XCP_INCREMENT_MTA_BY_SIZE_IN_AG(Size) \
  do \
  { \
    Xcp_MTA.Address.Write = &Xcp_MTA.Address.Write[((Size)* XCP_ADDRESS_GRANULARITY_SIZE)]; \
  } while( 0 )

#if (defined XCP_PGM_ABSOLUTE_ACCESS_MODE)
#error XCP_PGM_ABSOLUTE_ACCESS_MODE already defined
#endif
/** \brief Definition of value to indicate the absolute access mode as defined by ASAM */
#define XCP_PGM_ABSOLUTE_ACCESS_MODE                            0U

#if (defined XCP_PGM_ABS_ACCESS_MODE_MASK)
#error XCP_PGM_ABS_ACCESS_MODE_MASK already defined
#endif
/** \brief Definition of value to indicate that the ABSOLUTE_ACCESS_MODE is available for
 **        programming */
#define XCP_PGM_ABS_ACCESS_MODE_MASK                            0x1U

#if (defined XCP_PGM_FUNCTIONAL_ACCESS_MODE)
#error XCP_PGM_FUNCTIONAL_ACCESS_MODE already defined
#endif
/** \brief Definition of value to indicate the functional access mode as defined by ASAM */
#define XCP_PGM_FUNCTIONAL_ACCESS_MODE                          1U

#if (defined XCP_PGM_PROPERTIES_MASK)
#error XCP_PGM_PROPERTIES_MASK already defined
#endif
/** \brief Definition for PGM_PROPERTIES bit mask for command GET_PGM_PROCESSOR_INFO:
 **        - bit0..1: the programming mode bits, will be masked to only contain those modes,
 **                   supported by the Xcp.
 **        - bit2..7: will remain as given by the Xcp_ApplGetSectorInfo().
 **/
#define XCP_PGM_PROPERTIES_MASK                                 0xFCU

#if (defined XCP_GET_PGM_PROPERTIES)
/** \brief Macro used to mask available modes for programming */
#error XCP_GET_PGM_PROPERTIES already defined
#endif
#define XCP_GET_PGM_PROPERTIES(PgmProperties)  \
  (PgmProperties) &= ( XCP_PGM_PROPERTIES_MASK | XCP_PGM_ABS_ACCESS_MODE_MASK )

#if (defined XCP_ABORT_PROGRAMMING_MASK)
#error XCP_ABORT_PROGRAMMING_MASK already defined
#endif
/** \brief Definition of value to indicate that the flash programming was marked to be aborted
 * by an asynchronous event (e.g. the RX indication)*/
#define XCP_ABORT_PROGRAMMING_MASK                              0x1U

#if (defined XCP_CMD_SYNCH_PENDING_MASK)
#error XCP_CMD_SYNCH_PENDING_MASK already defined
#endif
/** \brief Definition of value to indicate a pending SYNCH command */
#define XCP_CMD_SYNCH_PENDING_MASK                              0x1U

#if (XCP_ON_CAN_ENABLED == STD_ON)
#if (defined XCP_RECEIVED_GET_SLAVE_ID_ECHO_MASK)
#error XCP_RECEIVED_GET_SLAVE_ID_ECHO_MASK already defined
#endif
/** \brief Definition of value to indicate reception of sub command GET_SLAVE_ID
 ** with mode = "identify by echo" */
#define XCP_RECEIVED_GET_SLAVE_ID_ECHO_MASK                     0x2U
#endif /* XCP_ON_CAN_ENABLED == STD_ON */

#if (defined XCP_CMD_DISCONNECT_PENDING_MASK)
#error XCP_CMD_DISCONNECT_PENDING_MASK already defined
#endif
/** \brief Definition of value to indicate a pending DISCONNECT command */
#define XCP_CMD_DISCONNECT_PENDING_MASK                         0x4U

#if (defined XCP_CMD_BUSY_ABORTED_MASK)
#error XCP_CMD_BUSY_ABORTED_MASK already defined
#endif
/** \brief Definition of value to indicate an aborted busy command */
#define XCP_CMD_BUSY_ABORTED_MASK                               0x8U

#if (defined XCP_CMD_DOWNLOAD_BLOCK_MODE_MASK)
#error XCP_CMD_DOWNLOAD_BLOCK_MODE_MASK already defined
#endif
/** \brief Definition of value to indicate a pending DOWNLOAD block mode transfer */
#define XCP_CMD_DOWNLOAD_BLOCK_MODE_MASK                        0x10U

#if (defined XCP_CMD_PROGRAMMING_STARTED_MASK)
#error XCP_CMD_PROGRAMMING_STARTED_MASK already defined
#endif
/** \brief Definition of value to indicate that PROGRAM_START command was received */
#define XCP_CMD_PROGRAMMING_STARTED_MASK                        0x20U

#if (defined XCP_CMD_PROGRAMMING_ACTIVE_MASK)
#error defined XCP_CMD_PROGRAMMING_ACTIVE_MASK already defined
#endif
/** \brief Definition of value to indicate that the programming of a segment is active */
#define XCP_CMD_PROGRAMMING_ACTIVE_MASK                         0x40U

#if (defined XCP_CMD_PGM_BLOCK_MODE_MASK)
#error defined XCP_CMD_PGM_BLOCK_MODE_MASK already defined
#endif
/** \brief Definition of value to indicate that the programming is currently in block mode */
#define XCP_CMD_PGM_BLOCK_MODE_MASK                             0x80U

/** \brief Response length value for commands that are not supported by the current
 * configuration/version */
#define XCP_UNSUPPORTED_CMD_RES_LENGTH                     255U

#if (defined XCP_IS_SYNCH_CMD_PENDING)
#error XCP_IS_SYNCH_CMD_PENDING already defined
#endif
/** \brief Returns the SYNCH command status. */
#define XCP_IS_SYNCH_CMD_PENDING() \
  ((XCP_CMD_SYNCH_PENDING_MASK == (Xcp_CmdReqStatus & XCP_CMD_SYNCH_PENDING_MASK))? TRUE:FALSE)

#if (defined XCP_IS_DISCONNECT_CMD_PENDING)
#error XCP_IS_DISCONNECT_CMD_PENDING already defined
#endif
/** \brief Returns the DISCONNECT command status. */
#define XCP_IS_DISCONNECT_CMD_PENDING() \
  ((XCP_CMD_DISCONNECT_PENDING_MASK == (Xcp_CmdReqStatus & XCP_CMD_DISCONNECT_PENDING_MASK))? TRUE:FALSE)

#if (defined XCP_IS_CMD_BUSY_ABORTED)
#error XCP_IS_CMD_BUSY_ABORTED already defined
#endif
/** \brief Returns whether the busy command was aborted or not */
#define XCP_IS_CMD_BUSY_ABORTED() \
  ((XCP_CMD_BUSY_ABORTED_MASK == (Xcp_CmdReqStatus & XCP_CMD_BUSY_ABORTED_MASK))? TRUE:FALSE)

#if (defined XCP_UNSUPPORTED_CMD_LENGTH)
#error XCP_UNSUPPORTED_CMD_LENGTH already defined
#endif
/** \brief Unsupported commands are handled as length 1. In this way their
 **        PID is evaluated and ERR_CMD_UNKNOWN is replied. */
#define XCP_UNSUPPORTED_CMD_LENGTH        1U

/** \brief Macro to initialize the Cyclic Event counter.
 **
 ** This macro is used for a Cyclic Event
 ** to initialize to 1U the TimeCycleCounter variable.
 **
 **/
#define XCP_CYCLIC_EVENTS_CNT_INI()                                      \
  do                                                                     \
  {                                                                      \
    /* Check if the event that has this Daq attached is a cyclic one */  \
    if( Xcp_EventInfo[DaqListPtr->EventId].TimeCycle > 0U )         \
    {                                                                    \
      /* This is a cyclic event */                                       \
      /* Set the cyclic event time cycle to the initial value 1U.        \
         This value allows the event to trigger for the first time when  \
         the Xcp_MainFunction is called */                               \
         Xcp_Event[DaqListPtr->EventId].TimeCycleCounter = 1U;      \
    }                                                                    \
  } while( 0 )

#if (XCP_RESOURCE_CAL_PAG == XCP_RESOURCE_CAL_PAG_MASK)
/** \brief Macro required only if Calibration/Paging Resource is supported */
#if (defined XCP_GET_SEGMENT_INFO_CMD_RES_LENGTH)
#error XCP_GET_SEGMENT_INFO_CMD_RES_LENGTH already defined
#endif
/** \brief GET_SEGMENT_INFO comand response maximum length */
#define XCP_GET_SEGMENT_INFO_CMD_RES_LENGTH  5U

#endif /* XCP_RESOURCE_CAL_PAG == XCP_RESOURCE_CAL_PAG_MASK */

#if (XCP_MODIFY_BITS_SUPPORTED == STD_ON)
/** \brief Macros required only if MODIFY_BITS command is supported */
#if (defined XCP_MODIFY_VALUE_SIZE)
#error XCP_MODIFY_VALUE_SIZE already defined
#endif
/** \brief Modify Bits value size (4 bytes) */
#define XCP_MODIFY_VALUE_SIZE                4U

#if (defined XCP_MODIFY_SHIFT_MAX)
#error XCP_MODIFY_SHIFT_MAX already defined
#endif
/** \brief Modify Bits Shift value max */
#define XCP_MODIFY_SHIFT_MAX                31U

#endif /* XCP_MODIFY_BITS_SUPPORTED == STD_ON */

/** \brief Identifier for calculating the length of the DOWNLOAD,
 **        DOWNLOAD_NEXT, UNLOCK, TRANSPORT_LAYER_CMD CTO, PROGRAM, PROGRAM_NEXT */
#define XCP_COMMAND_LENGTH_CALCULATED     240U

#if (defined XCP_RESP_CTO_QUEUE_LENGTH_SIZE)
#error XCP_RESP_CTO_QUEUE_LENGTH_SIZE already defined
#endif
/** \brief Response CTO message length size (number of bytes).
 * The length of a response CTO message is an 8-bit number (one byte) */
#define XCP_RESP_CTO_QUEUE_LENGTH_SIZE    1U

#if (defined XCP_RESP_CTO_QUEUE_FREE_RESERVED)
#error XCP_RESP_CTO_QUEUE_FREE_RESERVED already defined
#endif
/** \brief Removes a number of bytes that were previously reserved. */
#define XCP_RESP_CTO_QUEUE_FREE_RESERVED(PrevReservedBytes)  \
Xcp_CtoQueue.ReservedBytes -= ( (PrevReservedBytes) + XCP_RESP_CTO_QUEUE_LENGTH_SIZE )

#if (defined XCP_IS_COMMAND_EXECUTABLE)
#error XCP_IS_COMMAND_EXECUTABLE already defined
#endif
/** \brief Verify whether the command is executable.
 *
 * When XCP is connected all available commands can be executed.  When XCP is
 * disconnected only the CONNECT and GET_SLAVE_ID can be executed All commands
 * with ID > 0xC0 will be queued from rx indication function 0x00U - 0xBF are
 * queued as STIM. */
#define XCP_IS_COMMAND_EXECUTABLE(command) \
((((Xcp_State == XCP_STATE_CONNECTED) || \
  (((Xcp_State == XCP_STATE_DISCONNECTED ) || (Xcp_State == XCP_STATE_RESUME)) && \
   (((command) == XCP_CMD_CONNECT_PID) || \
    ((command) == XCP_CMD_TRANSPORT_LAYER_PID ) \
   )) \
 ) && (Xcp_Commands[(command) - XCP_CMD_PID_BASE].CommandHandlerPtr != NULL_PTR) \
)? TRUE:FALSE)

#if (defined XCP_CMD_PROCESSOR_STATE_TRANS)
#error XCP_CMD_PROCESSOR_STATE_TRANS already defined
#endif
/** \brief Change command processor state machine to specified state. */
#define XCP_CMD_PROCESSOR_STATE_TRANS(nextState) \
                                             (Xcp_CmdProcessorState = (nextState))

#if (defined XCP_IS_CTO_QUEUE_EMPTY)
#error XCP_IS_CTO_QUEUE_EMPTY already defined
#endif
/** \brief Check whether command response CTO Queue is empty. */
#define XCP_IS_CTO_QUEUE_EMPTY()\
                ((0U == Xcp_CtoQueue.NoOfBytes)? TRUE:FALSE)

#if (defined XCP_GET_NEXT_CTO_SIZE)
#error XCP_GET_NEXT_CTO_SIZE already defined
#endif
/** \brief Returns the size of next command response CTO in CTO queue */
#define XCP_GET_NEXT_CTO_SIZE() (Xcp_CtoQueue.Queue[Xcp_CtoQueue.Head])

#if (defined XCP_GET_CTOQUEUE_FREESPACE)
#error XCP_GET_CTOQUEUE_FREESPACE already defined
#endif
/** \brief Calculate free space available in CTO Queue.
 ** \param[in] RetType Cast type used to fix MISRA-C 10.1 violation */
#define XCP_GET_CTOQUEUE_FREESPACE(RetType) \
                            ((RetType)XCP_CTO_QUEUE_SIZE - (RetType)Xcp_CtoQueue.NoOfBytes)

#if (defined XCP_READ_CMDPACKET_BYTE)
#error XCP_READ_CMDPACKET_BYTE already defined
#endif
/** \brief Reads specified byte from  first command packet in command queue. */
#define XCP_READ_CMDPACKET_BYTE(ByteIndex) \
    (Xcp_CommandQueue.Queue[Xcp_CommandQueue.Head].CTO[(ByteIndex)])

#if (defined XCP_GET_NO_OF_COMMANDS)
#error XCP_GET_NO_OF_COMMANDS already defined
#endif
/** \brief Returns the number of commands. */
#define XCP_GET_NO_OF_COMMANDS()                          (Xcp_CommandQueue.NoOfCmds)

#if (defined XCP_IS_COMMAND_QUEUE_FULL)
#error XCP_IS_COMMAND_QUEUE_FULL already defined
#endif
/** \brief Checks whether commands queue is Full. */
#define XCP_IS_COMMAND_QUEUE_FULL() \
            (((uint8)XCP_CMD_PROCESSOR_QUEUE_SIZE == \
                                         Xcp_CommandQueue.NoOfCmds)? TRUE:FALSE)


#if (XCP_PGM_SUPPORTED == STD_ON)

#if (defined XCP_PGM_NO_OF_ELEMENTS_INDEX)
/** \brief The index in the PROGRAM and PROGRAM_NEXT commands parameters where how many elements
 * to flash is found */
#error XCP_PGM_NO_OF_ELEMENTS_INDEX already defined
#endif
#define XCP_PGM_NO_OF_ELEMENTS_INDEX     1U

#if (defined XCP_PGM_CLEAR_RANGE_INDEX)
/** \brief The index in the PROGRAM_CLEAR command input parameters where the clear range is found */
#error XCP_PGM_CLEAR_RANGE_INDEX already defined
#endif
#define XCP_PGM_CLEAR_RANGE_INDEX        4U

#endif /* XCP_PGM_SUPPORTED == STD_ON */

/*==================[type definitions]======================================*/

/** \brief  Definition of MTA type.
 *
 * MTA can be an address or a sequence counter. */
typedef enum {
  XCP_MTATYPE_ADDRESS, /* MTA is address of a memory location */
  XCP_MTATYPE_COUNTER  /* MTA is used as sequence counter */
} Xcp_MTAType_t;

/** \brief MTA information structure */
typedef struct {
  /* Deviation MISRA-3 <START> */
  union
  {
    P2VAR(uint8, TYPEDEF, XCP_VAR)   Write; /* Memory transfer address */
    P2CONST(uint8, TYPEDEF, XCP_VAR) Read;  /* Memory transfer address */
  } Address;
  /* Deviation MISRA-3 <STOP> */
  Xcp_MTAType_t Type; /* MTA type */
  boolean IsSet;      /* As NULL_PTR can be a valid address to be set, an additional flag indicating
                         whether MTA was set is needed*/
} Xcp_MTA_t;

/** \brief Command processor function pointer type */
typedef P2FUNC(Xcp_ErrorType, XCP_CONST, CommandHandlerPtr_t)(void);

/** \brief Structure containing each commands' execution handler and additional information regarding
 * the execution needs */
typedef struct
{
  CommandHandlerPtr_t CommandHandlerPtr;        /* Pointer to command execution handler */
  uint8               BaseLength;               /* Base Length */
  uint8               CommandMaxResponseLength; /* Maximum response length a command can send */
} Xcp_Commands_t;

/* If DAQ or STIM resource is supported */
#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))
/** \brief DAQ pointer information structure */
typedef struct {
  uint16 DAQList; /* DAQ List number */
  uint8 ODT;      /* Relative ODT number within the DAQ list */
  uint8 ODTEntry;  /* Relative ODT entry number within the ODT */
} Xcp_DAQPtr_t;
#endif

/* If Seed and Key mechanism is required */
#if (XCP_SEED_AND_KEY_ENABLED == STD_ON)
/** \brief  Definition of security access request type.
 *
 * Security request type can be a SEED request or Unlock / Compare KEY
 * request. */
typedef enum {
  /* Security access request is a Get Seed request with first part of seed */
  XCP_SEC_ACCESS_GETSEED_ZERO = 0,
  /* Security access request is a Get Seed request with remaining parts of
   * seed */
  XCP_SEC_ACCESS_GETSEED_ONE = 1,
  /* Security access request is an Unlock */
  XCP_SEC_ACCESS_UNLOCK = 2,
  /* Security access request is invalid(initial state) */
  XCP_SEC_ACCESS_INVALID = 3
} Xcp_SecAccessType_t;


/** \brief Security Access information structure */
typedef struct {
  Xcp_SecAccessType_t PrevType; /* PreviousSecurity Access type */
  uint8 Resource;           /* Last requested resource */
} Xcp_SecAccess_t;

#endif /* XCP_SEED_AND_KEY_ENABLED == STD_ON */

/** \brief Structure needed by PROGRAM, PROGRAM_NEXT, DOWNLOAD & DOWNLOAD_NEXT commands to write
 * in the memory (RAM or non-volatile) */
typedef struct
{
  uint8 ElementsLeftToWrite;          /* The remaining data elements that must be written in
                                         the memory */
  uint8 ElementsWrittenSoFar;         /* The data elements that were processed up to now and put in
                                         the write buffer. Used only for block mode transfer */
  uint8 Buffer[XCP_MAX_MEMORY_WRITE_BYTES]; /* The buffer where we keep the elements received from
                                               the master. When the writing (DOWNLOAD or PROGRAM
                                               sequence) is completed successfully, the buffer
                                               values will be copied at the MTA */
} Xcp_MemoryWriteType;


#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK)
/** \brief Runtime info regarding the configured Dynamic DAQ lists */
typedef struct
{
  uint32 AllocatedOdtsSoFar; /* Total number of ODTs allocated up to now */
  uint32 AllocatedOdtEntriesSoFar; /* Total number of ODT entries allocated up to now */
  P2VAR(Xcp_OdtType, TYPEDEF, XCP_VAR) FirstDynamicOdtPtr;           /* Address of the very first
                                                                        configured dynamic ODT */
  P2VAR(Xcp_OdtEntryType, TYPEDEF, XCP_VAR) FirstDynamicOdtEntryPtr; /* Address of the very
                                                                        first configured
                                                                        dynamic ODT entry*/
  Xcp_DaqIdType NoOfConfiguredDynamicDAQLists; /* Total number of dynamically configured DAQ Lists */
  uint8 LastDynamicDAQReq; /* Last serviced dynamic DAQ request */
} Xcp_DynamicDaqListsType;
#endif

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/
#define XCP_START_SEC_CODE
#include <MemMap.h>

/** \brief Function to reset the variables used to monitor a memory write
 **
 ** Use this function if the download/programming sequence finished (positively or with an error)
 ** or SYNCH command is executed or, if it's the download case, DISCONNECT command is executed.
 **  */
STATIC FUNC(void, XCP_CODE) Xcp_ResetMemoryWriteInfo(void);

/** \brief Function to enqueue CTO into CTO queue.
 **
 ** Use this function to enqueue one response CTO into CTO queue.
 ** CTO queue is implemented  as byte array. CTO length will be queued
 ** first and then the CTO data bytes will be queued.
 **
 ** \param[in] CtoPtr       Address of the CTO to be queued
 **
 */
STATIC FUNC(void, XCP_CODE) Xcp_InsertResponseCTO
(
  P2VAR(Xcp_CTOType, AUTOMATIC, XCP_APPL_DATA) CtoPtr
);

/** \brief Function to determine the number of bytes to be reserved in the CTO response queue.
 **
 **  The function will return the number of bytes to be reserved either statically, as the
 **  maximum response the command can yield, either dynamically, based on the command parameters.
 **
 ** \param[in]      CommandId  The command which awaits execution.
 **
 ** \return The number of bytes to be reserved.
 ** */
STATIC FUNC(uint8, XCP_CODE) Xcp_GetLengthToBeReserved
(
  uint8 CommandId
);

/** \brief Function to reserve bytes to the response CTO queue.
 **
 ** This function reserves a number of bytes (ReserveBytes + 1)
 ** to the response CTO queue.
 ** It does that way because the length of a message goes to
 ** the response CTO queue as well.
 ** This function must be called from an exclusive area (protected).
 **
 ** \param[in]      ReserveBytes  Number of bytes to be reserved.
 **
 ** \return Result of the operation
 ** \retval E_OK Bytes have been reserved in the queue.
 ** \retval E_NOT_OK Not enough space in the queue to reserve that many bytes.
 ** */
STATIC FUNC(Std_ReturnType, XCP_CODE) Xcp_ReserveBytesToRespCTOQueue
(
  Xcp_CtoNoOfBytesType ReserveBytes
);

/** \brief Function to terminate the current session
 **
 ** This function will flush command queue, CTO Queue, DTO Queue and all
 ** pending event packets.
 **
 ** This function must only be executed within a critical section.
 **
 **  */
STATIC FUNC(void, XCP_CODE) Xcp_TerminateSession(void);

/* If DAQ or STIM resource is supported */
#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))

/** \brief Function to disassociate DAQ list from an Event channel.
 **
 ** Use this function to disassociate specified DAQ list from specified
 ** Event channel. First the daq list will be removed and then the DAQ Lists
 ** positioned after the specified DAQ list will be moved one position up
 ** the list.
 **
 ** \param[in] DaqListNumber       DAQ List to be disassociated
 ** \param[in] EventChannelNumber  Event channel from which the DAQ List to be disassociated
 */


STATIC FUNC(void, XCP_CODE) Xcp_RemoveDAQListFromEventChannel
(
  Xcp_DaqIdType DaqListNumber,
  uint16 EventChannelNumber
);

/** \brief Function to associate DAQ list to an Event channel.
 **
 ** Use this function to associate specified DAQ list to specified
 ** Event channel. All DAQ List with priority less that the specified
 ** DAQ list will be moved one position down the list and then the new
 ** DAQ List will be inserted.
 **
 ** \param[in] DaqListNumber       DAQ List to be associated with the specified event channel
 ** \param[in] EventChannelNumber  Event cahnnel to which the DAQ List to be associated
 */
STATIC FUNC(void, XCP_CODE) Xcp_AssociateDAQListToEventChannel
(
  Xcp_DaqIdType DaqListNumber,
  uint16 EventChannelNumber
);

#endif /* ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) ||
        *  (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)) */

/** \brief Function to queue CTO bytes into CTO queue.
 **
 ** CTO Queue is a byte array. Use this function to enqueue one byte from the
 ** CTO into CTO queue
 **
 ** \param[in] DataByte  The data byte to be queued
 */
STATIC FUNC(void, XCP_CODE) Xcp_EnqueueByteInCtoQueue(uint8 DataByte);

/** \brief Function to Dequeue one command from Command Queue.
 **
 ** Use this function to flush one command from command queue.  Queue head
 ** will be updated to the next command in queue and rolled over if
 ** necessary. Number of commands in queue will also be updated. */
STATIC FUNC(void, XCP_CODE) Xcp_DeQueueCmdQueue(void);

/** \brief Function to Dequeue one CTO from the response CTO Queue.
 **
 ** Use this function to dequeue one CTO from the response CTO queue.  CTO
 ** data bytes will be copied byte by byte from queue head.  Queue head will
 ** be updated to the next CTO length information in the queue and rolled over
 ** if necessary.  Number of bytes in queue will also be updated.
 **
 ** \param[in] BufferPtr  Buffer to which CTO shall be dequeued */
STATIC FUNC(void, XCP_CODE) Xcp_DeQueueCtoQueue
(
  P2VAR(uint8, AUTOMATIC, XCP_APPL_DATA) BufferPtr
);

/** \brief Function to process one single command.
 **
 ** Use this function to execute the first command in command queue. The command will be checked for
 ** its executability. If the command is executable, a request will be made to invoke the
 ** corresponding command handler.
 **
 ** Depending on the command handler response, the following post-execution actions will be made:
 **
 ** - XCP_E_BUSY:
 ** Command processing can't be finished in this cycle. The command will be kept in
 ** the queue and the state of the command processor will change to busy.
 **
 ** - XCP_E_OK, XCP_E_NOT_OK:
 **  Command finished (successfully or not). The command shall be dequeued and state of the command
 **  processor will change to idle.
 **
 ** - XCP_E_INSUFFICIENT_MEMORY:
 ** Command didn't execute as there was not enough resources to do that (e.g. not enough space in
 ** the response queue). Keep the command in queue until needed resources are available.
 **
 **  */
STATIC FUNC(void, XCP_CODE) Xcp_ProcessCmd(void);

/** \brief Invoke processing function for the specified command.
 **
 **  The processing function for the specified command will be called only if the maximum response
 **  the command could yield can be queued in the CTO response queue.
 **
 ** \param[in] Command The command ID
 **
 ** \return The command execution response
 ** \retval XCP_E_OK      Processing done
 ** \retval XCP_E_NOT_OK  Processing can't be done
 ** \retval XCP_E_BUSY    Processing not complete
 ** \retval XCP_E_INSUFFICIENT_MEMORY  Processing failed due to insufficient memory
 **  */
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ExecuteCommand(uint8 Command);

/** \brief Function to Process CONNECT command
 **
 ** Use this function to execute a CONNECT command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdConnect(void);

/** \brief Function to Process DISCONNECT command request.
 **
 ** Use this function to execute a DISCONNECT command request.
 **
 ** \return E_OK if command succeeded and E_NOT_OK otherwise.
 ** */
STATIC FUNC(Std_ReturnType, XCP_CODE)  Xcp_ProcessCmdDisConnect(void);

/** \brief Function to Process GET_ID command request.
 **
 ** Use this function to execute a GET_ID command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetId(void);

/** \brief Function to Process GET_COMM_MODE_INFO command request.
 **
 ** Use this function to execute a GET_COMM_MODE_INFO command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetCommModeInfo(void);

#if (XCP_PGM_SUPPORTED == STD_ON)

/** \brief Function to process PROGRAM_MAX command request.
 **
 ** Use this function to execute a PROGRAM_MAX command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 ** \retval XCP_E_INSUFFICIENT_MEMORY Space not available to send response.
 */
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdProgramMax(void);

#if (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON)

/** \brief Function to process PROGRAM_NEXT command request.
 **
 ** Use this function to execute a PROGRAM_NEXT command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 */
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdProgramNext(void);
#endif /* XCP_MASTER_BLOCK_MODE_PGM == STD_ON */

/** \brief Function to process PROGRAM_RESET command request.
 **
 ** Use this function to execute a PROGRAM_RESET command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 */
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdProgramReset(void);

/** \brief Function to process PROGRAM command request.
 **
 ** Use this function to execute a PROGRAM command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 */
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdProgram(void);

/** \brief Function to process PROGRAM_CLEAR command request.
 **
 ** Use this function to execute a PROGRAM_CLEAR command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 */
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdProgramClear(void);

/** \brief Function to process PROGRAM_START command request.
 **
 ** Use this function to execute a PROGRAM_START command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 */
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdProgramStart(void);

/** \brief Function to process GET_SECTOR_INFO command request.
 **
 ** Use this function to execute a GET_SECTOR_INFO command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetSectorInfo(void);

/** \brief Function to process GET_PGM_PROCESSOR_INFO command request.
 **
 ** Use this function to execute a GET_PGM_PROCESSOR_INFO command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetPgmProcInfo(void);

/** \brief Helper function to process flash programming
 **
 ** This function is used by the commands PROGRAM, PROGRAM_MAX and PROGRAM_NEXT
 ** (as far as available) to perform the real programming.
 **
 ** \param[in] DataPtr Pointer to the data, that shall be programmed.
 ** \param[in] CallingCmdPid -  Which programming command called this function (PROGRAM,
 ** PROGRAM_NEXT or PROGRAM_MAX)
 **
 */
STATIC FUNC(void, XCP_CODE) Xcp_ProgramFlash
(
  P2CONST( uint8, AUTOMATIC, XCP_APPL_DATA ) DataPtr,
  uint8 CallingCmdPid
);
#endif /* XCP_PGM_SUPPORTED == STD_ON */

/** \brief Function to Process GET_STATUS command request.
 **
 ** Use this function to execute a GET_STATUS command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetStatus(void);
/* If DAQ or STIM resource is supported */
#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))
/** \brief Function to Process START_STOP_DAQ_LIST command request.
 **
 ** Use this function to execute a START_STOP_DAQ_LIST command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdStartStopDAQList(void);

/** \brief Function to Process START_STOP_SYNCH command request.
 **
 ** Use this function to execute a START_STOP_SYNCH command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdStartStopSynch(void);

/** \brief Function is sub function to Process the START STOP Sych command request.
 **
 ** Use this function to check error handling and to execute START STOP Sych command.
 **
 ** \param[out] Xcp_CTOType pointer to ResponseCTO data.
 **/
STATIC FUNC(void, XCP_CODE) Xcp_ProcessStartStopSynch
(
  P2VAR(Xcp_CTOType,AUTOMATIC,AUTOMATIC) ResponseCTOPtr
);

/** \brief Function to Process GET_DAQ_PROCESSOR_INFO command request.
 **
 ** Use this function to execute a GET_DAQ_PROCESSOR_INFO command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetDAQProcessorInfo(void);

/** \brief Function to Process GET_DAQ_LIST_MODE command request.
 **
 ** Use this function to execute a GET_DAQ_LIST_MODE command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetDAQListMode(void);

/** \brief Function to Process GET_DAQ_RESOLUTION_INFO command request.
 **
 ** Use this function to execute a GET_DAQ_RESOLUTION_INFO command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetDAQResolutionInfo(void);

/** \brief Function to Process GET_DAQ_EVENT_INFO command request.
 **
 ** Use this function to execute a GET_DAQ_EVENT_INFO command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetDAQEventInfo(void);

/** \brief Function is sub function to Process the Set DAQ list mode command request.
 **
 ** Use this function to check error handling for Set DAQ list mode command.
 **
 ** \param[out] Xcp_CTOType pointer to ResponseCTO data.
 **/
STATIC FUNC(void, XCP_CODE) Xcp_SubSetDAQListMode
(
  P2VAR(Xcp_CTOType,AUTOMATIC,AUTOMATIC) ResponseCTOPtr
);

/** \brief Function is sub function to Process the set DAQ list mode command request.
 **
 ** Use this function to execute Set DAQ list mode command.
 **/
STATIC FUNC(void, XCP_CODE) Xcp_ProcessSetDAQListMode
(
  void
);

/* This command is available only for static DAQ lists */
#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK)

/** \brief Function to Process GET_DAQ_LIST_INFO command request.
 **
 ** Use this function to execute a GET_DAQ_LIST_INFO command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetDAQListInfo(void);

/** \brief Function to Process CLEAR_DAQ_LIST command request.
 **
 ** Use this function to execute a CLEAR_DAQ_LIST command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdClearDAQList(void);

/** \brief Function to clear ODT Entries associated with STATIC DAQ lists.
 **
 ** This function clears pointer to data element, address extension, length
 ** information and bit offset.
 **
 ** \param[in] OdtEntryPtr     Address of first ODT Entry information to be cleared
 ** \param[in] NoOfODTEntries  Number of ODT Entry informations to be cleared
 */
STATIC FUNC(void, XCP_CODE) Xcp_ClearODTEntries
(
  P2VAR(Xcp_OdtEntryType, AUTOMATIC, XCP_APPL_DATA) ODTEntryPtr,
  uint8 NoOfODTEntries
);

/** \brief Function to clear ODTs associated with STATIC DAQ lists.
 **
 ** This function clears specified ODTs by invoking Xcp_ClearODTEntries().
 ** Clearing of an ODT is in fact is the clearing of all ODT entries
 ** associated with the ODT.
 **
 ** \param[in] OdtPtr     Address of first ODT to be cleared
 ** \param[in] NoOfODTs   Number of ODTs to be cleared */
STATIC FUNC(void, XCP_CODE) Xcp_ClearODTs
(
  P2VAR(Xcp_OdtType, AUTOMATIC, XCP_APPL_DATA) ODTPtr,
  uint8 NoOfODTs
);
#endif /* XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK */

/** \brief Function to Process SET_DAQ_LIST_MODE command request.
 **
 ** Use this function to execute a SET_DAQ_LIST_INFO command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdSetDAQListMode(void);
#endif/* ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) ||
       *  (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)) */

/** \brief Function to Process UPLOAD command request.
 **
 ** Use this function to execute a UPLOAD command request.  Present
 ** implementation of the UPLOAD command is to support a prior GET_ID command.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 ** \retval XCP_E_NOT_OK Command encountered an error.
 ** \retval XCP_E_BUSY Command needs more main function cycles in order to complete the task.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdUpload(void);

/** \brief Function to Process TRANSPORT_LAYER_CMD command request.
 **
 ** Use this function to execute transport layer specific commands.  The
 ** commands to be executed are defined in the Transport Layer specification.
 ** Present implementation supports XCP on CAN specific command GET_SLAVE_ID .
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdTLSpecific(void);

/* If XCP on CAN is Enabled */
#if (STD_ON == XCP_ON_CAN_ENABLED)

/** \brief Function to Process XCP on CAN specific sub command GET_SLAVE_ID.
 **
 ** Use this function to execute transport layer specific commands
 ** GET_SLAVE_ID.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessSubCmdGetSlaveID(void);

#endif

/* If DAQ or STIM resource is supported */
#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))

/* If XCP on CAN is Enabled */
#if (STD_ON == XCP_ON_CAN_ENABLED)

/** \brief Function to Process XCP on CAN specific sub command GET_DAQ_ID.
 **
 ** Use this function to execute transport layer specific commands
 ** GET_DAQ_ID.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessSubCmdGetDaqID(void);

#endif

/* If DaqList configuration type is Dynamic*/
#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK)

/** \brief Function to clear the dynamic info block during initialization or FREE_DAQ command */
STATIC FUNC(void, XCP_CODE) Xcp_ClearDynamicInfoBlock(void);

#if (XCP_MIN_DAQ != XCP_MAX_DAQ)
/** \brief Function used to arrange all dynamic ODTs in the dynamic area.
 ** It will basically set, for each DAQ list, the pointer to the first ODT it has so that
 ** the ODTs will form a contiguous array. It will also set the FIRST_PID field for each DAQ list
 ** if necessary. */
STATIC FUNC(void, XCP_CODE) Xcp_ArrangeDynamicODTs(void);
#endif

/** \brief Function used to arrange all dynamic ODT entries in the dynamic area.
 ** It will basically set, for each ODT, the pointer to the first ODT entry it has so that
 ** the ODT entries will form a contiguous array. */
STATIC FUNC(void, XCP_CODE) Xcp_ArrangeDynamicODTEntries(void);

/** \brief Function to Process ALLOC_ODT_ENTRY command request.
 **
 ** Use this function to execute a ALLOC_ODT_ENTRY command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdAllocODTEntry(void);

/** \brief Function to Process ALLOC_ODT command request.
 **
 ** Use this function to execute a ALLOC_ODT command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdAllocODT(void);

/** \brief Function to Process ALLOC_DAQ command request.
 **
 ** Use this function to execute a ALLOC_DAQ command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdAllocDAQ(void);

/** \brief Function to Process FREE_DAQ command request.
 **
 ** Use this function to execute a FREE_DAQ command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdFreeDAQ(void);

#endif /* (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK) */

/** \brief Function to Process SET_DAQ_PTR command request.
 **
 ** Use this function to execute a SET_DAQ_PTR command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdSetDAQPtr(void);

/** \brief Function to Process WRITE_DAQ command request.
 **
 ** Use this function to execute a WRITE_DAQ command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdWriteDAQ(void);
#endif /*((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) ||
        * (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))*/
#if (XCP_SEED_AND_KEY_ENABLED == STD_ON)

/** \brief Function to Process security access command GET_SEED.
 **
 ** Use this function to execute command GET_SEED.  This function use an
 ** external algorithm to generate seed.  External algorithm can be provided
 ** in file Xcp_Callouts.c.  If the requested resource is already unlocked the
 ** external algorithm wont be invoked.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetSeed(void);

/** \brief Function to Process security access command UNLOCK.
 **
 ** Use this function to execute command UNLOCK.  This function use an
 ** external algorithm to compare the received key.  External algorithm can be
 ** provided in file Xcp_Callouts.c.  If the key is valid the function will
 ** set the resource as unlocked.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdUnlock(void);

#endif /* XCP_SEED_AND_KEY_ENABLED == STD_ON */

/* If DAQ or STIM resource is supported */
#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))

/** \brief Function to Process GET_DAQ_CLOCK command request.
 **
 ** Use this function to execute a GET_DAQ_CLOCK command request.  This
 ** function reads the time stamp counter value using the OS function call
 ** GetCounterValue().
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetDAQClock(void);

#endif /*((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) ||
        * (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))*/

/* If DAQ or STIM resource is supported, or MODIFY_BITS command is supported */
#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK) || \
     (XCP_MODIFY_BITS_SUPPORTED == STD_ON))

/** \param[in] index index of start of uint16 value in command array
 ** \return extracted uint16 value */
STATIC FUNC(uint16, XCP_CODE) Xcp_Uint16FromCmd(uint16 index);

#endif  /* (XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) ||
           (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK) ||
           (XCP_MODIFY_BITS_SUPPORTED == STD_ON) */

#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK) || \
     (XCP_PGM_SUPPORTED == STD_ON) || \
     (XCP_BUILD_CHECKSUM_SUPPORT == STD_ON))

/** \brief Extract uint32 value from command message
 **
 ** \param[in] index index of start of uint32 value in command array
 ** \return extracted uint32 value */
STATIC FUNC(uint32, XCP_CODE) Xcp_Uint32FromCmd(uint16 index);

#endif /*(XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
         (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK) || \
         (XCP_PGM_SUPPORTED == STD_ON) || \
         (XCP_BUILD_CHECKSUM_SUPPORT == STD_ON)*/

/** \brief Function to Process DOWNLOAD command request.
 **
 ** Use this function to execute a DOWNLOAD command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdDownload(void);

#if (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON)
/** \brief Function to Process DOWNLOAD_NEXT command request.
 **
 ** Use this function to execute a DOWNLOAD_NEXT command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdDownloadNext(void);
#endif

/** \brief Function to Process one download request.
 **
 ** This function is used by DOWNLOAD and DOWNLOAD_NEXT commands and will fill the download buffer
 ** with incoming elements from the master. If the download sequence is completed, it will copy the
 ** download buffer into the slave's memory pointed by the MTA.
 **
 ** \param[out] ResponseCTO    Pointer to the response which may be filled by this function call if
 **                           it is the last download from the sequence */
STATIC FUNC(void, XCP_CODE) Xcp_ProcessOneDownload
(
  P2VAR(Xcp_CTOType, AUTOMATIC, XCP_APPL_DATA)  ResponseCTO
);

/** \brief Function to Process SHORT_UPLOAD command request.
 **
 ** Use this function to execute a SHORT_UPLOAD command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdShortUpload(void);

/** \brief Function to Process SET_MTA command request.
 **
 ** Use this function to execute a SET_MTA command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdSetMTA(void);

#if (XCP_RESOURCE_CAL_PAG == XCP_RESOURCE_CAL_PAG_MASK)
/** \brief Function to process COPY_CAL_PAGE command request.
 **
 ** Use this function to execute a COPY_CAL_PAGE command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdCopyCalPage(void);

/** \brief Function to process GET_PAG_PROCESSOR_INFO command request.
 **
 ** Use this function to execute a GET_PAG_PROCESSOR_INFO command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetPagProcInfo(void);

/** \brief Function to process GET_CAL_PAGE command request.
 **
 ** Use this function to execute a GET_CAL_PAGE command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetCalPage(void);

/** \brief Function to process SET_CAL_PAGE command request.
 **
 ** Use this function to execute a SET_CAL_PAGE command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdSetCalPage(void);

/** \brief Function to process GET_SEGMENT_MODE command request.
 **
 ** Use this function to execute a GET_SEGMENT_MODE command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetSegmentMode(void);

/** \brief Function to process SET_SEGMENT_MODE command request.
 **
 ** Use this function to execute a SET_SEGMENT_MODE command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdSetSegmentMode(void);

/** \brief Function to process GET_SEGMENT_INFO command request.
 **
 ** Use this function to execute a GET_SEGMENT_INFO command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetSegmentInfo(void);

/** \brief Function to process GET_PAGE_INFO command request.
 **
 ** Use this function to execute a GET_PAGE_INFO command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetPageInfo(void);

#endif /* XCP_RESOURCE_CAL_PAG == XCP_RESOURCE_CAL_PAG_MASK */

#if (XCP_BUILD_CHECKSUM_SUPPORT == STD_ON)
/** \brief Function to process BUILD_CHECKSUM command request.
 **
 ** Use this function to execute a BUILD_CHECKSUM command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdBuildChecksum(void);
#endif

#if (XCP_MODIFY_BITS_SUPPORTED == STD_ON)
/** \brief Function to Process MODIFY_BITS command request.
 **
 ** This function shall execute a MODIFY_BITS command request.
 **
 ** \return Command execution status.
 ** \retval E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdModifyBits(void);
#endif

#if ((XCP_STORE_DAQ_SUPPORTED == STD_ON) || \
     (XCP_CAL_PAG_STORE_SUPPORTED == STD_ON))
/** \brief Function to process SET_REQUEST command request.
 **
 ** Use this function to execute a SET_REQUEST command request.
 **
 ** \return Command execution status.
 ** \retval XCP_E_OK   Command executed successfully.
 **/
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdSetRequest(void);
#endif /* (XCP_STORE_DAQ_SUPPORTED == STD_ON || XCP_CAL_PAG_STORE_SUPPORTED == STD_ON) */

#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
/**
 ** \brief Function to handle a request to store the DAQ lists
 **  This function is used by SET_REQUEST command handler to perform the storing task.
 ** \param[in]  RequestedMode - the requested mode of how to store DAQ (Resume or no Resume)
 ** \param[out] ResponseCTO  -  Pointer to the response which will be filled upon the execution of
 **                             this function
 **
 **  */
STATIC FUNC(void, XCP_CODE) Xcp_StartStoreDaq
(
  uint8 RequestedMode,
  P2VAR(Xcp_CTOType, AUTOMATIC, XCP_APPL_DATA) ResponseCTO
);

/**
 ** \brief Function to handle a request to clear the DAQ lists
 **  This function is used by SET_REQUEST command handler to perform the clearing task.
 **
 ** \param[out] ResponseCTO  -  Pointer to the response which will be filled upon the execution of
 **                             this function
 **
 **  */
STATIC FUNC(void, XCP_CODE) Xcp_StartClearDaq
(
  P2VAR(Xcp_CTOType, AUTOMATIC, XCP_APPL_DATA) ResponseCTO
);

#endif /* XCP_STORE_DAQ_SUPPORTED == STD_ON */

#if (XCP_CAL_PAG_STORE_SUPPORTED == TRUE)
/**
 ** \brief Function to handle a request to store the calibration data in the non volatile memory
 **  This function is used by SET_REQUEST command handler to perform the storing task.
 **
 ** \param[out] ResponseCTO  -  Pointer to the response which will be filled upon the execution of
 **                             this function
 **
 **  */
STATIC FUNC(void, XCP_CODE) Xcp_StartStoreCal
(
  P2VAR(Xcp_CTOType, AUTOMATIC, XCP_APPL_DATA) ResponseCTO
);

#endif /* XCP_CAL_PAG_STORE_SUPPORTED == TRUE */


/**
 ** \brief Tries to send an error message and will immediately dequeue the command.
 **
 ** \param[in] ErrResponseCTOPtr  -  Pointer to the response which will be filled by this function
 **                               caller
 **
 **  */
STATIC FUNC(void, XCP_CODE) Xcp_AbortCommandWithError
(
  P2VAR(Xcp_CTOType, AUTOMATIC, XCP_APPL_DATA)  ErrResponseCTOPtr
);

/**
 ** \brief Function to check if a command is OK to be executed in the current context
 **
 ** \param[in] CommandId - The PID of the command to be validated
 ** \param[out] ErrResponseCTOPtr  -  Pointer to the response which will get filled if the validation
 ** fails. If the validation passes, the response will not be used by the calling function.
 **
 ** \return Whether the command can be executed
 ** \retval TRUE   Command can be executed
 ** \retval FALSE  Command cannot be executed
 **
 **  */
STATIC FUNC(boolean, XCP_CODE) Xcp_CommandCanBeExecuted
(
  uint8 CommandId,
  P2VAR(Xcp_CTOType, AUTOMATIC, XCP_APPL_DATA)  ErrResponseCTOPtr
);

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

#define XCP_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/** \brief The commands handlers and additional information regarding the command execution needs
 **   - Command function pointer
 **   - Base length (number of bytes)
 **     "Base length" means the packet length without any headers
 **     or tails that are transport layer or configuration specific.
 **     It is used for calculating the lengths of the packets received via FlexRay.
 **   - Command maximum response length
 **
 ** NOTE:
 ** 1) Some commands have common handling or need to calculate their length
 **    based on the message content. In these cases (e.g. DOWNLOAD)
 **    the XCP_COMMAND_LENGTH_CALCULATED ID is used to identify the special case
 **    and calculate the real length out of it.
 ** 2) Array uses magic numbers as they can directly be compared with the ASAM
 **    specification and they are not reused anywhere in the module which makes
 **    the overhead for constants questionable at least.
 */
STATIC CONST(Xcp_Commands_t, XCP_CONST) Xcp_Commands[XCP_NR_OF_XCP_COMMAND_CODES] =
{
  /* 0xC0 */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xC1 */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xC2 */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xC3 */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xC4 */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xC5 */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xC6 */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xC7 */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
#if (XCP_PGM_SUPPORTED == STD_ON)
  /* 0xC8 */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xC9 - PROGRAM_MAX */
  {
    &Xcp_ProcessCmdProgramMax,            /* Command handler pointer */
    XCP_MAX_CTO_PGM,                      /* Base Length */
    2U,                                   /* Command maximum response length */
  },
#if (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON)
  /* 0xCA - PROGRAM_NEXT */
  {
    &Xcp_ProcessCmdProgramNext,           /* Command handler pointer */
    XCP_COMMAND_LENGTH_CALCULATED,        /* Base Length */
    2U,                                   /* Command maximum response length */
  },
#else
  /* 0xCA  */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
#endif /* XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON */
  /* 0xCB */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xCC */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xCD - GET_SECTOR_INFO */
  {
    &Xcp_ProcessCmdGetSectorInfo,         /* Command handler pointer */
    3U,                                   /* Base Length */
    XCP_CMD_SECTOR_INFO_MAX_RES_LENGTH,   /* Command maximum response length */
  },
  /* 0xCE - GET_PGM_PROCESSOR_INFO */
  {
    &Xcp_ProcessCmdGetPgmProcInfo,        /* Command handler pointer */
    1U,                                   /* Base Length */
    XCP_CMD_PGM_PROC_INFO_RES_LENGTH,     /* Command maximum response length */
  },
  /* 0xCF - PROGRAM_RESET */
  {
    &Xcp_ProcessCmdProgramReset,          /* Command handler pointer */
    1U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
  /* 0xD0 - PROGRAM */
  {
    &Xcp_ProcessCmdProgram,                /* Command handler pointer */
    XCP_COMMAND_LENGTH_CALCULATED,        /* Base Length */
    2U,                                   /* Command maximum response length */
  },
  /* 0xD1 - PROGRAM_CLEAR */
  {
    &Xcp_ProcessCmdProgramClear,          /* Command handler pointer */
    8U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
  /* 0xD2 - PROGRAM_START */
  {
    &Xcp_ProcessCmdProgramStart,          /* Command handler pointer */
    1U,                                   /* Base Length */
    7U,                                   /* Command maximum response length */
  },
  #else /* no programming commands available */
  /* 0xC8 */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xC9 */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xCA */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xCB */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xCC */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xCD */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xCE */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xCF */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xD0 */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xD1 */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xD2 */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
#endif /* XCP_PGM_SUPPORTED == STD_ON */

/* If DAQ or STIM resource is supported */
#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))
#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK)
  /* 0xD3 - ALLOC_ODT_ENTRY */
  {
    &Xcp_ProcessCmdAllocODTEntry,         /* Command handler pointer */
    6U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
  /* 0xD4 - ALLOC_ODT */
  {
    &Xcp_ProcessCmdAllocODT,              /* Command handler pointer */
    5U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
  /* 0xD5 - ALLOC_DAQ */
  {
    &Xcp_ProcessCmdAllocDAQ,              /* Command handler pointer */
    4U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
  /* 0xD6 - FREE_DAQ */
  {
    &Xcp_ProcessCmdFreeDAQ,               /* Command handler pointer */
    1U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
#else /* Dynamic configuration is not supported */
  /*  0xD3 - ALLOC_ODT_ENTRY */
  {
    NULL_PTR,                             /* Command handler pointer */
    6U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xD4 - ALLOC_ODT */
  {
    NULL_PTR,                             /* Command handler pointer */
    5U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xD5 - ALLOC_DAQ */
  {
    NULL_PTR,                             /* Command handler pointer */
    4U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xD6 - FREE_DAQ */
  {
    NULL_PTR,                             /* Command handler pointer */
    1U,                                 /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
#endif /* XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK */
  /* 0xD7 - GET_DAQ_EVENT_INFO */
  {
    &Xcp_ProcessCmdGetDAQEventInfo,        /* Command handler pointer */
    4U,                                   /* Base Length */
    7U,                                   /* Command maximum response length */
  },
/* This command is available only for static DAQ lists */
#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK)
  /* 0xD8 - GET_DAQ_LIST_INFO */
  {
    &Xcp_ProcessCmdGetDAQListInfo,         /* Command handler pointer */
    4U,                                   /* Base Length */
    6U,                                   /* Command maximum response length */
  },
#else /* Static configuration is not supported */
  /* 0xD8 - GET_DAQ_LIST_INFO */
  {
    NULL_PTR,                             /* Command handler pointer */
    4U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
#endif /* XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK */
  /* 0xD9 - GET_DAQ_RESOLUTION_INFO */
  {
    &Xcp_ProcessCmdGetDAQResolutionInfo,   /* Command handler pointer */
    1U,                                   /* Base Length */
    8U,                                   /* Command maximum response length */
  },
  /* 0xDA - GET_DAQ_PROCESSOR_INFO */
  {
    &Xcp_ProcessCmdGetDAQProcessorInfo,    /* Command handler pointer */
    1U,                                   /* Base Length */
    8U,                                   /* Command maximum response length */
  },
  /* 0xDB - READ_DAQ  */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xDC - GET_DAQ_CLOCK */
  {
    &Xcp_ProcessCmdGetDAQClock,            /* Command handler pointer */
    1U,                                   /* Base Length */
    8U,                                   /* Command maximum response length */
  },
  /* 0xDD - START_STOP_SYNCH */
  {
    &Xcp_ProcessCmdStartStopSynch,         /* Command handler pointer */
    2U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
  /* 0xDE - START_STOP_DAQ_LIST */
  {
    &Xcp_ProcessCmdStartStopDAQList,       /* Command handler pointer */
    4U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
  /* 0xDF - GET_DAQ_LIST_MODE */
  {
    &Xcp_ProcessCmdGetDAQListMode,         /* Command handler pointer */
    4U,                                   /* Base Length */
    8U,                                   /* Command maximum response length */
  },
  /* 0xE0 - SET_DAQ_LIST_MODE */
  {
    &Xcp_ProcessCmdSetDAQListMode,         /* Command handler pointer */
    8U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
  /* 0xE1 - WRITE_DAQ */
  {
    &Xcp_ProcessCmdWriteDAQ,               /* Command handler pointer */
    8U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
  /* 0xE2 - SET_DAQ_PTR */
  {
    &Xcp_ProcessCmdSetDAQPtr,              /* Command handler pointer */
    6U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK)
  /* 0xE3 - CLEAR_DAQ_LIST */
  {
    &Xcp_ProcessCmdClearDAQList,           /* Command handler pointer */
    4U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
#else /* Static configuration is not supported */
  /* 0xE3 - CLEAR_DAQ_LIST */
  {
    NULL_PTR,                             /* Command handler pointer */
    4U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
#endif /* XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK */
#else /* ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) ||
          (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)) */
  /* 0xD3 - ALLOC_ODT_ENTRY */
  {
    NULL_PTR,                             /* Command handler pointer */
    6U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xD4 - ALLOC_ODT */
  {
    NULL_PTR,                             /* Command handler pointer */
    5U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xD5 - ALLOC_DAQ */
  {
    NULL_PTR,                             /* Command handler pointer */
    4U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xD6 - FREE_DAQ */
  {
    NULL_PTR,                             /* Command handler pointer */
    1U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xD7 - GET_DAQ_EVENT_INFO */
  {
    NULL_PTR,                             /* Command handler pointer */
    4U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xD8 - GET_DAQ_LIST_INFO */
  {
    NULL_PTR,                             /* Command handler pointer */
    4U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xD9 - GET_DAQ_RESOLUTION_INFO */
  {
    NULL_PTR,                             /* Command handler pointer */
    1U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xDA - GET_DAQ_PROCESSOR_INFO */
  {
    NULL_PTR,                             /* Command handler pointer */
    1U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xDB - READ_DAQ */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xDC - GET_DAQ_CLOCK */
  {
    NULL_PTR,                             /* Command handler pointer */
    1U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xDD - START_STOP_SYNCH */
  {
    NULL_PTR,                             /* Command handler pointer */
    2U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xDE - START_STOP_DAQ_LIST */
  {
    NULL_PTR,                             /* Command handler pointer */
    4U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xDF - GET_DAQ_LIST_MODE */
  {
    NULL_PTR,                             /* Command handler pointer */
    4U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xE0 - SET_DAQ_LIST_MODE */
  {
    NULL_PTR,                             /* Command handler pointer */
    8U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xE1 - WRITE_DAQ */
  {
    NULL_PTR,                             /* Command handler pointer */
    8U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xE2 - SET_DAQ_PTR */
  {
    NULL_PTR,                             /* Command handler pointer */
    6U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xE3 - CLEAR_DAQ_LIST */
  {
    NULL_PTR,                             /* Command handler pointer */
    4U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
#endif /* ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) ||
          (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)) */

#if (XCP_RESOURCE_CAL_PAG == XCP_RESOURCE_CAL_PAG_MASK)
  /* 0xE4 - COPY_CAL_PAGE */
  {
    &Xcp_ProcessCmdCopyCalPage,           /* Command handler pointer */
    5U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
  /* 0xE5 - GET_SEGMENT_MODE */
  {
    &Xcp_ProcessCmdGetSegmentMode,        /* Command handler pointer */
    3U,                                   /* Base Length */
    3U,                                   /* Command maximum response length */
  },
  /* 0xE6 - SET_SEGMENT_MODE */
  {
    &Xcp_ProcessCmdSetSegmentMode,        /* Command handler pointer */
    3U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
  /* 0xE7 - GET_PAGE_INFO */
  {
    &Xcp_ProcessCmdGetPageInfo,           /* Command handler pointer */
    4U,                                   /* Base Length */
    3U,                                   /* Command maximum response length */
  },
  /* 0xE8 - GET_SEGMENT_INFO */
  {
    &Xcp_ProcessCmdGetSegmentInfo,        /* Command handler pointer */
    5U,                                   /* Base Length */
    8U,                                   /* Command maximum response length */
  },
  /* 0xE9 - GET_PAG_PROCESSOR_INFO */
  {
    &Xcp_ProcessCmdGetPagProcInfo,        /* Command handler pointer */
    1U,                                   /* Base Length */
    3U,                                   /* Command maximum response length */
  },
  /* 0xEA - GET_CAL_PAGE */
  {
    &Xcp_ProcessCmdGetCalPage,            /* Command handler pointer */
    3U,                                   /* Base Length */
    4U,                                   /* Command maximum response length */
  },
  /* 0xEB - SET_CAL_PAGE */
  {
    &Xcp_ProcessCmdSetCalPage,            /* Command handler pointer */
    4U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
#else /* XCP_RESOURCE_CAL_PAG != XCP_RESOURCE_CAL_PAG_MASK */
  /* 0xE4 - COPY_CAL_PAGE */
  {
    NULL_PTR,                             /* Command handler pointer */
    5U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xE5 - GET_SEGMENT_MODE */
  {
    NULL_PTR,                             /* Command handler pointer */
    3U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xE6 - SET_SEGMENT_MODE */
  {
    NULL_PTR,                             /* Command handler pointer */
    3U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xE7 - GET_PAGE_INFO */
  {
    NULL_PTR,                             /* Command handler pointer */
    4U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xE8 - GET_SEGMENT_INFO */
  {
    NULL_PTR,                             /* Command handler pointer */
    5U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xE9 - GET_PAG_PROCESSOR_INFO */
  {
    NULL_PTR,                             /* Command handler pointer */
    1U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xEA - GET_CAL_PAGE */
  {
    NULL_PTR,                             /* Command handler pointer */
    3U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xEB - SET_CAL_PAGE */
  {
    NULL_PTR,                             /* Command handler pointer */
    4U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
#endif /* XCP_RESOURCE_CAL_PAG == XCP_RESOURCE_CAL_PAG_MASK */
#if (XCP_MODIFY_BITS_SUPPORTED == STD_ON)
  /* 0xEC - MODIFY_BITS */
  {
    &Xcp_ProcessCmdModifyBits,            /* Command handler pointer */
    6U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
#else
  /* 0xEC - MODIFY_BITS */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
#endif /* XCP_MODIFY_BITS_SUPPORTED == STD_ON */
  /* 0xED */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xEE */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_UNSUPPORTED_CMD_LENGTH,           /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
#if (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON)
  /* 0xEF - DOWNLOAD_NEXT */
  {
    &Xcp_ProcessCmdDownloadNext,          /* Command handler pointer */
    XCP_COMMAND_LENGTH_CALCULATED,        /* Base Length */
    3U,                                   /* Command maximum response length */
  },
#else /* Master block mode is not available */
  /* 0xEF - DOWNLOAD_NEXT */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_COMMAND_LENGTH_CALCULATED,        /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
#endif /* XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON */
  /* 0xF0 - DOWNLOAD */
  {
    &Xcp_ProcessCmdDownload,              /* Command handler pointer */
    XCP_COMMAND_LENGTH_CALCULATED,        /* Base Length */
    2U,                                   /* Command maximum response length */
  },
  /* 0xF1  */
  {
    NULL_PTR,                             /* Command handler pointer */
    1U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xF2 - TRANSPORT_LAYER_CMD */
  {
    &Xcp_ProcessCmdTLSpecific,            /* Command handler pointer */
    XCP_COMMAND_LENGTH_CALCULATED,        /* Base Length */
    8U,                                   /* Command maximum response length */
  },
#if (XCP_BUILD_CHECKSUM_SUPPORT == STD_ON)
  /* 0xF3 - BUILD_CHECKSUM */
  {
    &Xcp_ProcessCmdBuildChecksum,         /* Command handler pointer */
    8U,                                   /* Base Length */
    8U,                                   /* Command maximum response length */
  },
#else /* BUILD_CHECKSUM is not supported */
  /* 0xF3 - BUILD_CHECKSUM  */
  {
    NULL_PTR,                             /* Command handler pointer */
    8U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
#endif /* XCP_BUILD_CHECKSUM_SUPPORT == STD_ON */
  /*  0xF4 - SHORT_UPLOAD */
  {
    &Xcp_ProcessCmdShortUpload,           /* Command handler pointer */
    8U,                                   /* Base Length */
    XCP_MAX_CTO                           /* Command maximum response length */
  },
  /* 0xF5 - UPLOAD */
  {
    &Xcp_ProcessCmdUpload,                /* Command handler pointer */
    2U,                                   /* Base Length */
    XCP_MAX_CTO,                          /* Command maximum response length */
  },
  /* 0xF6 - SET_MTA */
  {
    &Xcp_ProcessCmdSetMTA,                /* Command handler pointer */
    8U,                                   /* Base Length */
    1U,                                   /* Command maximum response length */
  },
#if (XCP_SEED_AND_KEY_ENABLED == STD_ON)
  /* 0xF7 - UNLOCK */
  {
    &Xcp_ProcessCmdUnlock,                /* Command handler pointer */
    XCP_COMMAND_LENGTH_CALCULATED,        /* Base Length */
    2U,                                   /* Command maximum response length */
  },
  /* 0xF8 - GET_SEED */
  {
    &Xcp_ProcessCmdGetSeed,               /* Command handler pointer */
    3U,                                   /* Base Length */
    XCP_MAX_CTO,                          /* Command maximum response length */
  },
#else /* XCP_SEED_AND_KEY_ENABLED == STD_OFF */
  /* 0xF7 - UNLOCK */
  {
    NULL_PTR,                             /* Command handler pointer */
    XCP_COMMAND_LENGTH_CALCULATED,        /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /* 0xF8 - GET_SEED */
  {
    NULL_PTR,                             /* Command handler pointer */
    3U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
#endif /* XCP_SEED_AND_KEY_ENABLED == STD_ON */

#if ((XCP_STORE_DAQ_SUPPORTED == STD_ON) || \
     (XCP_CAL_PAG_STORE_SUPPORTED == STD_ON))
  /*  0xF9 - SET_REQUEST */
  {
    &Xcp_ProcessCmdSetRequest,            /* Command handler pointer */
    4U,                                   /* Base Length */
    2U,                                   /* Command maximum response length */
  },
#else
  /*  0xF9 - SET_REQUEST */
  {
    NULL_PTR,                             /* Command handler pointer */
    4U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
#endif
  /* 0xFA - GET_ID */
  {
    &Xcp_ProcessCmdGetId,                 /* Command handler pointer */
    2U,                                   /* Base Length */
#if ( (XCP_CMD_GETID_RES_LENGTH_MIN + XCP_IDENTIFICATION_TYPE1_LENGTH) < XCP_MAX_CTO )
                                          /* Command maximum response length */
    (XCP_CMD_GETID_RES_LENGTH_MIN + XCP_IDENTIFICATION_TYPE1_LENGTH),
#else
    XCP_MAX_CTO,                          /* Command maximum response length */
#endif
  },
  /* 0xFB - GET_COMM_MODE_INFO */
  {
    &Xcp_ProcessCmdGetCommModeInfo,       /* Command handler pointer */
    1U,                                   /* Base Length */
    8U,                                   /* Command maximum response length */
  },
  /*  0xFC - SYNCH Implemented without handling function */
  {
    NULL_PTR,                             /* Command handler pointer */
    1U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /*  0xFD - GET_STATUS */
  {
    &Xcp_ProcessCmdGetStatus,             /* Command handler pointer */
    1U,                                   /* Base Length */
    6U,                                   /* Command maximum response length */
  },
  /*  0xFE - DISCONNECT : Handled directly, with out inserting in to command queue */
  {
    NULL_PTR,                             /* Command handler pointer */
    1U,                                   /* Base Length */
    XCP_UNSUPPORTED_CMD_RES_LENGTH,       /* Command maximum response length */
  },
  /*  0xFF - CONNECT */
  {
    &Xcp_ProcessCmdConnect,               /* Command handler pointer */
    2U,                                   /* Base Length */
    8U,                                   /* Command maximum response length */
  },
};

#define XCP_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

#define XCP_START_SEC_CONST_8BIT
#include <MemMap.h>

#if (XCP_IDENTIFICATION_TYPE1_LENGTH > 0)
/** \brief ASAM-MC2 filename without path and extension.
 *
 * This is information for identification type 1: ASAM-MC2 filename without
 * path and extension. */
STATIC CONST(uint8, XCP_CONST)
  Xcp_IdentificationTypeOne[XCP_IDENTIFICATION_TYPE1_LENGTH]
  = XCP_IDENTIFICATION_TYPE_ONE;
#endif /* #if (XCP_IDENTIFICATION_TYPE1_LENGTH > 0) */

#define XCP_STOP_SEC_CONST_8BIT
#include <MemMap.h>

/*==================[external data]=========================================*/

#define XCP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include <MemMap.h>
/** \brief All current status information of the slave device.
 *
 * This includes the status of the resource protection, pending store requests
 * the general status of data acquisition and stimulation. */
VAR(Xcp_StatusType, XCP_VAR_NOINIT) Xcp_Status;

#define XCP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include <MemMap.h>


#define XCP_START_SEC_VAR_16BIT
#include <MemMap.h>

/* If event packet transmission enabled */
#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
/** \brief variable to hold the current index of Event CTO Queue */
VAR(uint16, XCP_VAR) Xcp_EventCTOIndex = 0U;
#endif

#define XCP_STOP_SEC_VAR_16BIT
#include <MemMap.h>

#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)

#define XCP_START_SEC_VAR_NOINIT_8BIT
#include <MemMap.h>

/** \brief The flag that determines whether we are in resume mode and we are waiting for the
 * event packet EV_RESUME_MODE to be transmitted. */
VAR(boolean, XCP_VAR_NOINIT) Xcp_EvResumeModeTransmitPending;

#define XCP_STOP_SEC_VAR_NOINIT_8BIT
#include <MemMap.h>

#endif /* XCP_STORE_DAQ_SUPPORTED == STD_ON */


/*==================[internal data]=========================================*/

#define XCP_START_SEC_VAR_16BIT
#include <MemMap.h>

#if (XCP_SLAVE_BLOCK_MODE == XCP_SLAVE_BLOCK_MODE_MASK)
/** \brief  Variable to hold the number of the data bytes we still need to send in a block mode
 * upload sequence */
STATIC VAR(uint16, XCP_VAR) Xcp_RemainingDataBytesToUpload = 0U;
#endif

#define XCP_STOP_SEC_VAR_16BIT
#include <MemMap.h>


#define XCP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include <MemMap.h>

/** \brief Memory writing related information */
STATIC VAR(Xcp_MemoryWriteType, XCP_VAR_NOINIT) Xcp_MemoryWrite;


#define XCP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include <MemMap.h>


#define XCP_START_SEC_VAR_NOINIT_8BIT
#include <MemMap.h>

/** \brief Command processor state machine state */
STATIC VAR(Xcp_CmdProcessorStateType, XCP_VAR_NOINIT) Xcp_CmdProcessorState;

/** \brief Definition of Command queue.
 *
 * The command queue is implemented as array of CTO structures. */
STATIC VAR(Xcp_CMDQueueType, XCP_VAR_NOINIT) Xcp_CommandQueue;

/** \brief Definition of CTO queue.
 *
 * The CTO queue is implemented as byte array. */
STATIC VAR(Xcp_CTOQueueType, XCP_VAR_NOINIT) Xcp_CtoQueue;

#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
/** \brief Definition of Event_CTO queue.
 *
 * The Event_CTO queue is implemented as byte array. */
VAR(Xcp_EventCTOType, XCP_VAR_NOINIT) Xcp_EventCTOQueue[XCP_EV_CTO_QUEUE_SIZE];
#endif

/** \brief Command status bit flags.
 *
 * Bit position 0:
 *         1 - SYNCH command received
 *         0 - No pending SYNCH command
 * Bit position 1:
 *         1 - Transport layer sub-command GET_SLAVE_ID with mode = "identify by echo" is received.
 *         0 - Transport layer sub-command GET_SLAVE_ID with mode = "identify by echo" not received.
 * Bit position 2:
 *         1 - DISCONNECT command received
 *         0 - No pending DISCONNECT command
 * Bit position 3:
 *         1 - BUSY command was aborted
 *         0 - BUSY command was not aborted
 * Bit position 4:
 *         1 - DOWNLOAD command pending
 *         0 - No pending DOWNLOAD command
 * Bit position 5:
 *         1 - PROGRAM_START command received
 *         0 - No PROGRAM_START command received or PROGRAM_RESET received in the meantime
 * Bit position 6:
 *         1 - First PROGRAM/_MAX command received, thus the programming of a segment is active
 *         0 - A PROGRAM with no. of elements = 0 was received, thus programming is inactive
 * Bit position 7:
 *         1 - Programming is in block mode transfer
 *         0 - Programming is not in block mode
 *
 */
STATIC VAR(uint8, XCP_VAR_NOINIT) Xcp_CmdReqStatus;

/** \brief Flags which were set by some asynchronous event.
 * These flags should be set by an event which interrupted the main function and must be checked and
 * handled by the main function.
 *
 * Right now only one flag is defined.
 *
 * Bit position 0:
 *         1 - Programming must be stopped
 *         0 - No action to be made regarding the flash programming
 */
STATIC VAR(uint8, XCP_VAR_NOINIT) Xcp_AsyncEventFlags;

#define XCP_STOP_SEC_VAR_NOINIT_8BIT
#include <MemMap.h>


#define XCP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include <MemMap.h>

/** \brief Memory transfer address information */
STATIC VAR(Xcp_MTA_t, XCP_VAR_NOINIT) Xcp_MTA;

/* If Seed and Key mechanism is required */
#if (XCP_SEED_AND_KEY_ENABLED == STD_ON)
/** \brief Security Access information */
STATIC VAR(Xcp_SecAccess_t, XCP_VAR_NOINIT) Xcp_SecAccess;
#endif


#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK)
/** \brief Runtime info regarding the configured Dynamic DAQ lists */
STATIC VAR(Xcp_DynamicDaqListsType, XCP_VAR_NOINIT) Xcp_DynamicInfo;
#endif

#define XCP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include <MemMap.h>

#define XCP_START_SEC_VAR_UNSPECIFIED
#include <MemMap.h>

/* If DAQ or STIM resource is supported */
#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))

/** \brief Global variable to hold DAQ List pointer
 *  This variable holds ODT entry pointer defined by the command SET_DAQ_PTR.
 *  This variable will be used to service commands WRITE_DAQ and READ_DAQ.
 */
STATIC VAR(Xcp_DAQPtr_t, XCP_VAR) Xcp_DAQPointer =
{
  XCP_MAX_DAQ,
  XCP_MAXIMUM_ODT,
  XCP_MAX_ENTRIES_PER_ODT
};
#endif

#define XCP_STOP_SEC_VAR_UNSPECIFIED
#include <MemMap.h>

/*==================[external function definitions]=========================*/

#define XCP_START_SEC_CODE
#include <MemMap.h>

/*-----------------------------[Xcp_HandleAsyncEventFlags]-----------------------------*/
FUNC(void, XCP_CODE) Xcp_HandleAsyncEventFlags( void )
{
  DBG_XCP_HANDLEASYNCEVENTFLAGS_ENTRY();

  /* Enter critical section.*/
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* If the flash programming was marked to be aborted */
  if ((Xcp_AsyncEventFlags & XCP_ABORT_PROGRAMMING_MASK) == XCP_ABORT_PROGRAMMING_MASK)
  {
    /* Clear the active programming flag */
    Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_PROGRAMMING_ACTIVE_MASK);

#if (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON)
    /* Clear the programming block mode flag */
    Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_PGM_BLOCK_MODE_MASK);
#endif

    /* Reset the globals used by any memory writing routine */
    Xcp_ResetMemoryWriteInfo();

    /* Clear the stored flag */
    Xcp_AsyncEventFlags &= (uint8)(~XCP_ABORT_PROGRAMMING_MASK);
  }

  /* Exit critical section.*/
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  DBG_XCP_HANDLEASYNCEVENTFLAGS_EXIT();
}

/*------------------[Xcp_InitCommandProcessor]------------------------------*/

FUNC(void, XCP_CODE) Xcp_InitCommandProcessor(void)
{
  DBG_XCP_INITCOMMANDPROCESSOR_ENTRY();

  /* Set Command processor state machine state to IDLE */
  Xcp_CmdProcessorState = XCP_CMDPROCESSOR_IDLE;
  /* Initialize command request status flags */
  Xcp_CmdReqStatus     = 0U;
  /* Initialize the variable holding the asynchronous flags */
  Xcp_AsyncEventFlags  = 0U;
  /* Initialize Memory transfer address information */
  Xcp_MTA.Type         = XCP_MTATYPE_ADDRESS;
  Xcp_MTA.Address.Read = NULL_PTR;
  Xcp_MTA.IsSet        = FALSE;

/* If Dynamic DAQ lists are supported */
#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK)
  /* Clear the dynamic info block */
  Xcp_ClearDynamicInfoBlock();
#endif

  /* Initialize All current status information of the slave device. This
   * includes the status of the resource protection, pending store requests
   * and the general status of data acquisition and stimulation. Protection
   * status: Present implementation supports SEED/UNLOCK and thus all
   * resources are initialized as Protected */
#if (XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK)
  Xcp_Status.NoOfRunningDaq  = 0U;
  Xcp_Status.NoOfSelectedDaq = 0U;
#endif
  Xcp_Status.SessionStatus   = 0U;
#if (XCP_SEED_AND_KEY_ENABLED == STD_ON)
  Xcp_Status.ProtectionStatus = XCP_RESOURCE_PGM_MASK |
  ( XCP_RESOURCE_STIM_MASK | ( XCP_RESOURCE_DAQ_MASK | XCP_RESOURCE_CAL_PAG_MASK ) );

  /* Initialize security access information */
  Xcp_SecAccess.PrevType = XCP_SEC_ACCESS_INVALID;
#else
  /* Seed and Key mechanism is not required and thus all resources are
   * unprotected */
  Xcp_Status.ProtectionStatus = 0U;
#endif

/* If event packet transmission enabled */
#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
  /* Initialize array Index */
  Xcp_EventCTOIndex          = 0U;
#endif

  /* Reset the globals used by any memory writing routine */
  Xcp_ResetMemoryWriteInfo();

  DBG_XCP_INITCOMMANDPROCESSOR_EXIT();
}

/*---------------------[Xcp_ProcessCommandQueue]----------------------------*/

FUNC(void, XCP_CODE) Xcp_ProcessCommandQueue(uint8 Context)
{
  DBG_XCP_PROCESSCOMMANDQUEUE_ENTRY(Context);

  /* Enter critical section.*/
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* If SYNCH command is pending */
  if (TRUE == XCP_IS_SYNCH_CMD_PENDING())
  {
    /* Local variable to prepare CTO */
    Xcp_CTOType ResponseCTO;


    /* Initialize command request status flags */
    Xcp_CmdReqStatus = 0U;
    /* Flush command queue */
    Xcp_FlushCMDQueue();
    /* Reset the globals used by any memory writing routine */
    Xcp_ResetMemoryWriteInfo();
    /* Flush CTO Queue */
    Xcp_FlushCTOQueue();
    /* Initialize transmit processor */
    Xcp_InitTransmitProcessor();

    /* Trigger state transition */
    XCP_CMD_PROCESSOR_STATE_TRANS(XCP_CMDPROCESSOR_IDLE);

    /* Prepare error CTO response */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_SYNCH;
    ResponseCTO.Length = XCP_ERR_CMD_SYNCH_LENGTH;

    /* XCP Insert CTO in response CTO queue  */
    Xcp_InsertResponseCTO(&ResponseCTO);
  }

  /* Exit critical section.*/
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* If DISCONNECT command is pending */
  if (TRUE == XCP_IS_DISCONNECT_CMD_PENDING())
  {
#if (XCP_PGM_SUPPORTED == STD_ON)
    /* Do not accept DISCONNECT command if a programming of a segment is currently active */
    if ((Xcp_CmdReqStatus & XCP_CMD_PROGRAMMING_ACTIVE_MASK) == XCP_CMD_PROGRAMMING_ACTIVE_MASK)
    {
      /* Enter critical section */
      SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

      /* Verify available space in CTO queue */
      if (XCP_GET_CTOQUEUE_FREESPACE(uint32) >= XCP_ERR_PGM_ACTIVE_LENGTH)
      {
        /* Variable to hold the error message */
        Xcp_CTOType ResponseCTO;
        /* Fill in the error ERR_PGM_ACTIVE */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_PGM_ACTIVE;
        ResponseCTO.Length = XCP_ERR_PGM_ACTIVE_LENGTH;

        /* XCP Insert CTO */
        Xcp_InsertResponseCTO(&ResponseCTO);
      }

      /* Clear pending status of DISCONNECT command */
      Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_DISCONNECT_PENDING_MASK);

      /* Exit critical section */
      SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
    }
    else
#endif
    {
      /* No meaningful reaction to the return value. The function already queued a message, either
       * error or positive */
      /* Deviation MISRA-7 <START> */
      (void)Xcp_ProcessCmdDisConnect();

      /* The disconnect pending flag is implicitly cleared by Xcp_ProcessCmdDisConnect() function */

      /* Deviation MISRA-7 <STOP> */
    }
  }

  /* Command processor state machine */
  switch (Xcp_CmdProcessorState)
  {
    case XCP_CMDPROCESSOR_IDLE:
    {
      /* Local variable to hold number of commands in command queue */
      uint8 NoOfCmds;
      /* iterator through the queued commands */
      uint8 i;

      /* Enter critical section.*/
      SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

      NoOfCmds = XCP_GET_NO_OF_COMMANDS();

      /* Exit critical section.*/
      SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

      /* Process queued commands (if any) */
      for (i=0U; i< NoOfCmds; i++)
      {
        Xcp_ProcessCmd();
      }
    }
    break;
    case XCP_CMDPROCESSOR_BUSY:
    {
      /* Execute the command only if it is called from a TX confirmation context */
      if (Context == XCP_RUN_FROM_TX_CONF_CONTEXT)
      {
        Xcp_ProcessCmd();
      }
    }
    break;
    default:
      /* Set DET */
    break;
  }

  DBG_XCP_PROCESSCOMMANDQUEUE_EXIT(Context);
}

/*------------------[Xcp_GetSduLength]-----------------------------------*/

FUNC(PduLengthType, XCP_CODE) Xcp_GetSduLength
(
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) XcpRxSduPtr
)
{
  /* command ID */
  const uint8 CommandId = XcpRxSduPtr[XCP_CMD_BYTE0_INDEX];
  /* result length read from array */
  PduLengthType Length = 0U;

  DBG_XCP_GETSDULENGTH_ENTRY(XcpRxSduPtr);

  /* get command base length */
  Length = Xcp_Commands[CommandId - XCP_CMD_PID_BASE].BaseLength;

  /* if the command has a dynamic length, calculate it from the data */
  if ( ( CommandId == XCP_CMD_DOWNLOAD_NEXT_PID ) ||
       ( CommandId == XCP_CMD_DOWNLOAD_PID      ) )
  {
    /* Check if additional fill bytes are needed due to alignment */
#if (XCP_DL_PGM_CMD_HEADER_LENGTH > XCP_ADDRESS_GRANULARITY_SIZE)
    Length = XCP_MIN( (
                         XCP_DL_PGM_CMD_HEADER_LENGTH +
                         (XcpRxSduPtr[XCP_CMD_BYTE1_INDEX] * XCP_ADDRESS_GRANULARITY_SIZE)
                       ),
                       XCP_MAX_CTO
                    );
#else
    Length = XCP_MIN( (
                          XCP_ADDRESS_GRANULARITY_SIZE +
                         (XcpRxSduPtr[XCP_CMD_BYTE1_INDEX] * XCP_ADDRESS_GRANULARITY_SIZE)
                       ),
                       XCP_MAX_CTO
                    );
#endif
  }
#if (XCP_PGM_SUPPORTED == STD_ON)
  /* if the command has a dynamic length, calculate it from the data */
  else  if ( ( CommandId == XCP_CMD_PROGRAM_PID ) ||
             ( CommandId == XCP_CMD_PROGRAM_NEXT_PID ))
  {
    /* Check if additional fill bytes are needed due to alignment */
#if (XCP_DL_PGM_CMD_HEADER_LENGTH > XCP_ADDRESS_GRANULARITY_SIZE)
    Length = XCP_MIN( (
                         XCP_DL_PGM_CMD_HEADER_LENGTH +
                         (XcpRxSduPtr[XCP_CMD_BYTE1_INDEX] * XCP_ADDRESS_GRANULARITY_SIZE)
                       ),
                       XCP_MAX_CTO_PGM
                    );
#else
    Length = XCP_MIN( (
                          XCP_ADDRESS_GRANULARITY_SIZE +
                         (XcpRxSduPtr[XCP_CMD_BYTE1_INDEX] * XCP_ADDRESS_GRANULARITY_SIZE)
                       ),
                       XCP_MAX_CTO_PGM
                    );
#endif
  }
#endif /* XCP_PGM_SUPPORTED == STD_ON */
  else if ( CommandId == XCP_CMD_UNLOCK_PID )
  {
    /* UNLOCK */
    Length = XCP_MIN( XCP_UNLOCK_CMD_HEADER_LENGTH + XcpRxSduPtr[XCP_CMD_BYTE1_INDEX],
                      XCP_MAX_CTO
                    );
  }
  else if ( CommandId == XCP_CMD_TRANSPORT_LAYER_PID )
  {
    /* TRANSPORT_LAYER_CMD */
    /* No sub commands with parameters are supported,
       therefore just evaluate the sub command code;
       Xcp.TPCMD.NegativeResponse001 */
    Length = 2U;
  }
  else
  {
    /* Length already correct in the lookup table; nothing to do */
  }


  DBG_XCP_GETSDULENGTH_EXIT(Length,XcpRxSduPtr);
  return Length;
}

/*----------------------------[Xcp_CheckCommandLength]---------------------------*/

FUNC(Std_ReturnType, XCP_CODE) Xcp_CheckCommandLength
(
  PduLengthType SduLength,
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) SduDataPtr
)
{
  /* Local variable to hold return value */
  Std_ReturnType RetVal = E_NOT_OK;

#if (XCP_PGM_SUPPORTED == STD_ON)
  /* command ID */
  const uint8 CommandId = SduDataPtr[XCP_CMD_BYTE0_INDEX];
#endif

  /* Local variable to hold the command minimum length */
  PduLengthType LengthMin = Xcp_GetSduLength( SduDataPtr );

  DBG_XCP_CHECKCOMMANDLENGTH_ENTRY(SduLength,SduDataPtr);

  /* The command length must be:
     - greater or equal than the command minimum allowed length AND
     - smaller or equal than MAX_CTO or MAX_CTO_PGM */
  if ( SduLength >= LengthMin)
  {
#if (XCP_PGM_SUPPORTED == STD_ON)
    /* If PID corresponds to a programming command */
    if ( ( CommandId >= XCP_CMD_FIRST_PROGRAMMING_PID ) &&
         ( CommandId <= XCP_CMD_LAST_PROGRAMMING_PID )
       )
    {
      if  ( SduLength <= XCP_MAX_CTO_PGM )
      {
        /* Command length is valid */
        RetVal = E_OK;
      }
      else
      {
        /* Bad command length, check for active programming status */
        if ((Xcp_CmdReqStatus & XCP_CMD_PROGRAMMING_ACTIVE_MASK) != 0U)
        {
          /* There is an error, programming of a segment is not active anymore. The user can retry
           * the whole sequence. The programming sequence will be aborted in the next main function
           * cycle */
          Xcp_AsyncEventFlags |= XCP_ABORT_PROGRAMMING_MASK;
        }
      }
    }
    else
#endif
    if ( SduLength <= XCP_MAX_CTO )
    {
      /* Command length is valid */
      RetVal = E_OK;
    }
    else
    {
      /* Command length is not valid (redundant assignment but required by MISRA on if - else if) */
      RetVal = E_NOT_OK;
    }
  }

  DBG_XCP_CHECKCOMMANDLENGTH_EXIT(RetVal,SduLength,SduDataPtr);
  return RetVal;
}

/*----------------------------[Xcp_SendErrorCmdSyntaxPacket]---------------------------*/

FUNC(void, XCP_CODE) Xcp_SendErrorCmdSyntaxPacket
(
  uint8 Pid
)
{
  DBG_XCP_SENDERRORCMDSYNTAXPACKET_ENTRY(Pid);

  /* Check space available in response CTO queue */
  if ( XCP_ERR_CMD_SYNTAX_LENGTH <= XCP_GET_CTOQUEUE_FREESPACE(uint32) )
  { /* There is enough space for this message */
    /* Check the Pid and the Xcp module state */
    if ( ( Xcp_State == XCP_STATE_CONNECTED ) ||
         ( ( Pid == XCP_CMD_CONNECT_PID ) ||
           ( Pid == XCP_CMD_TRANSPORT_LAYER_PID ) ) )
    {
      /* If Pid is CONNECT or Pid is TRANSPORT_LAYER_CMD or
         Xcp module state is XCP_STATE_CONNECTED
         send XCP_ERR_CMD_SYNTAX response */

      /* Local variable to prepare response */
      Xcp_CTOType ResponseCTO;

      /* Prepare error CTO with negative response XCP_ERR_CMD_SYNTAX */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_SYNTAX;
      ResponseCTO.Length = XCP_ERR_CMD_SYNTAX_LENGTH;

      /* Enqueue response packet */
      Xcp_InsertResponseCTO(&ResponseCTO);
    }

  }
  else
  {
    /* Not enough space in the response CTO queue.
       Do not enqueue response packet */
  } /* ( XCP_ERR_CMD_SYNTAX_LENGTH <= XCP_GET_CTOQUEUE_FREESPACE() ) */


  DBG_XCP_SENDERRORCMDSYNTAXPACKET_EXIT(Pid);
}

/*----------------------------[Xcp_InsertCommand]---------------------------*/

FUNC(void, XCP_CODE) Xcp_InsertCommand
(
  PduLengthType SduLength,
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) SduDataPtr
)
{
  DBG_XCP_INSERTCOMMAND_ENTRY(SduLength,SduDataPtr);

  /* Enqueue command CTO to the command queue */
  TS_MemCpy(Xcp_CommandQueue.Queue[Xcp_CommandQueue.Tail].CTO, SduDataPtr, SduLength);

  /* Update length of the command packet */
  Xcp_CommandQueue.Queue[Xcp_CommandQueue.Tail].Length = (uint8)SduLength;
  /* Update Queue tail */
  Xcp_CommandQueue.Tail++;
  Xcp_CommandQueue.Tail %= XCP_CMD_PROCESSOR_QUEUE_SIZE;

  /* Update number of commands */
  Xcp_CommandQueue.NoOfCmds++;

  DBG_XCP_INSERTCOMMAND_EXIT(SduLength,SduDataPtr);
}

/*-----------------------[Xcp_SetSynchCmdPending]--------------------------*/

FUNC(void, XCP_CODE) Xcp_SetSynchCmdPending(void)
{
  DBG_XCP_SETSYNCHCMDPENDING_ENTRY();

  /* Set SYNCH command as pending */
  Xcp_CmdReqStatus |= XCP_CMD_SYNCH_PENDING_MASK;

  DBG_XCP_SETSYNCHCMDPENDING_EXIT();
}

/*-----------------------[Xcp_SetDisconnectCmdPending]--------------------------*/

FUNC(void, XCP_CODE) Xcp_SetDisconnectCmdPending(void)
{
  DBG_XCP_SETDISCONNECTCMDPENDING_ENTRY();

  /* Set DISCONNECT command as pending */
  Xcp_CmdReqStatus |= XCP_CMD_DISCONNECT_PENDING_MASK;

  DBG_XCP_SETDISCONNECTCMDPENDING_EXIT();
}


#if ((XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON) || \
     (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON))

/*-----------------------[Xcp_IsMasterBlockModeInProgress]--------------------------*/
FUNC(boolean, XCP_CODE) Xcp_IsMasterBlockModeInProgress(void)
{
  boolean RetValue = FALSE;

  DBG_XCP_ISMASTERBLOCKMODEINPROGRESS_ENTRY();

#if (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON)
  RetValue = Xcp_IsDownloadInBlockMode();
#endif

#if (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON)
  if (RetValue == FALSE)
  {
    RetValue = Xcp_IsProgrammingBlockMode();
  }
#endif

  DBG_XCP_ISMASTERBLOCKMODEINPROGRESS_EXIT(RetValue);
  return RetValue;
}

#endif /* (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON) ||
          (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON) */


#if (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON)

/*-----------------------[Xcp_IsProgrammingBlockMode]--------------------------*/
FUNC(boolean, XCP_CODE) Xcp_IsProgrammingBlockMode(void)
{
  boolean RetValue;

  DBG_XCP_ISPROGRAMMINGBLOCKMODE_ENTRY();

  RetValue = (boolean)(((Xcp_CmdReqStatus & XCP_CMD_PGM_BLOCK_MODE_MASK) != 0U)? TRUE:FALSE);

  DBG_XCP_ISPROGRAMMINGBLOCKMODE_EXIT(RetValue);
  return RetValue;
}

/*-----------------------[Xcp_SetProgrammingBlockMode]--------------------------*/
FUNC(void, XCP_CODE) Xcp_SetProgrammingBlockMode(void)
{
  /* Set the programming transfer in block mode  */
  Xcp_CmdReqStatus |= XCP_CMD_PGM_BLOCK_MODE_MASK;
}

#endif /* XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON */


#if (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON)

/*-----------------------[Xcp_IsDownloadInBlockMode]--------------------------*/
FUNC(boolean, XCP_CODE) Xcp_IsDownloadInBlockMode(void)
{
  boolean RetValue;
  DBG_XCP_ISDOWNLOADINBLOCKMODE_ENTRY();
  RetValue = (boolean)(((Xcp_CmdReqStatus & XCP_CMD_DOWNLOAD_BLOCK_MODE_MASK) != 0U)? TRUE:FALSE);
  DBG_XCP_ISDOWNLOADINBLOCKMODE_EXIT(RetValue);
  return RetValue;
}

/*-----------------------[Xcp_SetDownloadInBlockMode]--------------------------*/
FUNC(void, XCP_CODE) Xcp_SetDownloadInBlockMode(void)
{
  DBG_XCP_SETDOWNLOADINBLOCKMODE_ENTRY();

  /* Set the DOWNLOAD in block mode */
  Xcp_CmdReqStatus |= XCP_CMD_DOWNLOAD_BLOCK_MODE_MASK;

  DBG_XCP_SETDOWNLOADINBLOCKMODE_EXIT();
}

#endif /* XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON */

/*-----------------------[Xcp_CommandQueueIsEmpty]--------------------------*/

FUNC(boolean, XCP_CODE) Xcp_CommandQueueIsEmpty(void)
{
  boolean RetValue;
  DBG_XCP_COMMANDQUEUEISEMPTY_ENTRY();
  RetValue = (boolean)((Xcp_CommandQueue.NoOfCmds == 0U) ? TRUE : FALSE);
  DBG_XCP_COMMANDQUEUEISEMPTY_EXIT(RetValue);
  return RetValue;
}


/*-----------------------[Xcp_CommandQueueIsFull]--------------------------*/

FUNC(boolean, XCP_CODE) Xcp_CommandQueueIsFull(void)
{
  boolean RetValue;
  DBG_XCP_COMMANDQUEUEISFULL_ENTRY();
  RetValue = (boolean)((Xcp_CommandQueue.NoOfCmds == XCP_CMD_PROCESSOR_QUEUE_SIZE ) ? TRUE : FALSE);
  DBG_XCP_COMMANDQUEUEISFULL_EXIT(RetValue);
  return RetValue;
}

/*----------------------------[Xcp_GetEventCTO]----------------------------*/

/* If event packet transmission enabled */
#if (XCP_EVENT_PACKET_ENABLED == STD_ON)

FUNC(Xcp_ErrorType, XCP_CODE) Xcp_GetEventCTO
(
  P2VAR(uint8, AUTOMATIC, XCP_APPL_DATA)TxBufferPtr,
  P2VAR(PduLengthType, AUTOMATIC, XCP_APPL_DATA)TxLengthPtr,
  const PduLengthType SpaceAvailable
)
{
  /* Local variable to hold return value */
  Xcp_ErrorType RetVal = XCP_E_NOT_OK;
  uint8 TempIndex;

  DBG_XCP_GETEVENTCTO_ENTRY(TxBufferPtr,TxLengthPtr,SpaceAvailable);

  /* Enter critical section.*/
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Check whether any event CTO is available */
  if (FALSE == XCP_IS_EVENT_CTO_QUEUE_EMPTY())
  {
    /* Get Length of next response Event CTO */
    PduLengthType LengthOfCTO = XCP_GET_NEXT_EVENT_CTO_SIZE();
    /* Check whether next Event CTO fits in the available space */
    if (SpaceAvailable >= LengthOfCTO)
    {
      /* Dequeue Event CTO from Event CTO queue to transmit buffer */
      TS_MemCpy(TxBufferPtr , &(Xcp_EventCTOQueue[0U].EVENT_CTO[0U]) , LengthOfCTO);
      /* Update transmit data length */
      *TxLengthPtr = LengthOfCTO;
      Xcp_EventCTOIndex--;
      for(TempIndex = 0U ; TempIndex < Xcp_EventCTOIndex ; TempIndex++)
      {
         Xcp_EventCTOQueue[TempIndex] = Xcp_EventCTOQueue[TempIndex + 1U];
      }
      /* Return Ok as space is available to copy response Event CTO packet and
       * Event Packets are available */
      RetVal = XCP_E_OK;
    }
  }

  /* Exit critical section.*/
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_GETEVENTCTO_EXIT(RetVal,TxBufferPtr,TxLengthPtr,SpaceAvailable);
  return RetVal;
}
#endif


/*----------------------------[Xcp_GetCTO]----------------------------*/

FUNC(Xcp_ErrorType, XCP_CODE) Xcp_GetCTO
(
  P2VAR(uint8, AUTOMATIC, XCP_APPL_DATA)TxBufferPtr,
  P2VAR(PduLengthType, AUTOMATIC, XCP_APPL_DATA)TxLengthPtr,
  const PduLengthType SpaceAvailable
)
{
  /* Local variable to hold return value */
  Xcp_ErrorType RetVal = XCP_E_NOT_OK;

  DBG_XCP_GETCTO_ENTRY(TxBufferPtr,TxLengthPtr,SpaceAvailable);

  /* Enter critical section.*/
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Check whether any command response CTO is available */
  if (FALSE == XCP_IS_CTO_QUEUE_EMPTY())
  {
    /* Get Length of next response CTO */
    PduLengthType LengthOfCTO = XCP_GET_NEXT_CTO_SIZE();
    /* Check whether next CTO fits in the available space */
    if (SpaceAvailable >= LengthOfCTO)
    {
      /* Dequeue response CTO from CTO queue to transmit buffer */
      Xcp_DeQueueCtoQueue(TxBufferPtr);
      /* Update transmit data length */
      *TxLengthPtr += LengthOfCTO;

      RetVal = XCP_E_OK;
    }
    else
    {
      /* Space not available to copy response CTO packet */
    }
  }

  /* Exit critical section.*/
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_GETCTO_EXIT(RetVal,TxBufferPtr,TxLengthPtr,SpaceAvailable);
  return RetVal;
}

/*----------------------------[Xcp_FlushCMDQueue]----------------------------*/

FUNC(void, XCP_CODE) Xcp_FlushCMDQueue(void)
{
  DBG_XCP_FLUSHCMDQUEUE_ENTRY();

  /* Reset queue head */
  Xcp_CommandQueue.Head     =  0U;
  /* Reset queue tail */
  Xcp_CommandQueue.Tail     =  0U;
  /* Reset number of commands in queue */
  Xcp_CommandQueue.NoOfCmds =  0U;

  DBG_XCP_FLUSHCMDQUEUE_EXIT();
}

/*----------------------------[Xcp_FlushCTOQueue]---------------------------*/

FUNC(void, XCP_CODE) Xcp_FlushCTOQueue(void)
{
  DBG_XCP_FLUSHCTOQUEUE_ENTRY();

  /* Reset queue head */
  Xcp_CtoQueue.Head      =  0U;
  /* Reset queue tail */
  Xcp_CtoQueue.Tail      =  0U;
  /* Reset number of bytes in queue */
  Xcp_CtoQueue.NoOfBytes =  0U;

  DBG_XCP_FLUSHCTOQUEUE_EXIT();
}

/*------------------------[Xcp_FlushEventCTOQueue]--------------------------*/
/* If event packet transmission enabled */
#if (XCP_EVENT_PACKET_ENABLED == STD_ON)

FUNC(void, XCP_CODE) Xcp_FlushEventCTOQueue(void)
{
  DBG_XCP_FLUSHEVENTCTOQUEUE_ENTRY();

  /* Clear all pending events to be transmitted by resetting the index */
  Xcp_EventCTOIndex = 0;

  DBG_XCP_FLUSHEVENTCTOQUEUE_EXIT();
}

#endif

/*--------------------[Xcp_WriteDwordToByteArray]---------------------------*/

FUNC(void, XCP_CODE) Xcp_WriteDwordToByteArray
(
  P2VAR(uint8, AUTOMATIC, XCP_APPL_DATA) DesPtr,
  uint32                                 Dword
)
{
  DBG_XCP_WRITEDWORDTOBYTEARRAY_ENTRY(DesPtr,Dword);

  DesPtr[XCP_UINT32_SB0_IDX] = (uint8)Dword;
  DesPtr[XCP_UINT32_SB1_IDX] = (uint8)(Dword >> 8U);
  DesPtr[XCP_UINT32_SB2_IDX] = (uint8)(Dword >> 16U);
  DesPtr[XCP_UINT32_SB3_IDX] = (uint8)(Dword >> 24U);

  DBG_XCP_WRITEDWORDTOBYTEARRAY_EXIT(DesPtr,Dword);
}

/*---------------------[Xcp_WriteWordToByteArray]---------------------------*/

FUNC(void, XCP_CODE) Xcp_WriteWordToByteArray
(
  P2VAR(uint8, AUTOMATIC, XCP_APPL_DATA) DesPtr,
  uint16                                 Word
)
{
  DBG_XCP_WRITEWORDTOBYTEARRAY_ENTRY(DesPtr,Word);

  DesPtr[XCP_UINT16_LSB_IDX] = (uint8)Word;
  DesPtr[XCP_UINT16_MSB_IDX] = (uint8)(Word >> 8U);

  DBG_XCP_WRITEWORDTOBYTEARRAY_EXIT(DesPtr,Word);
}

/*--------------------[Xcp_GetCommandProcessorState]---------------------------*/

FUNC(Xcp_CmdProcessorStateType, XCP_CODE) Xcp_GetCommandProcessorState(void)
{
  DBG_XCP_GETCOMMANDPROCESSORSTATE_ENTRY();
  DBG_XCP_GETCOMMANDPROCESSORSTATE_EXIT(Xcp_CmdProcessorState);
  return Xcp_CmdProcessorState;
}

/*--------------------[Xcp_AbortBusyCommand]---------------------------*/

FUNC(void, XCP_CODE) Xcp_AbortBusyCommand(void)
{
  DBG_XCP_ABORTBUSYCOMMAND_ENTRY();

  /* Reset the Command processor state machine state to IDLE */
  Xcp_CmdProcessorState = XCP_CMDPROCESSOR_IDLE;
  /* Set the busy command as aborted */
  Xcp_CmdReqStatus |= XCP_CMD_BUSY_ABORTED_MASK;

  DBG_XCP_ABORTBUSYCOMMAND_EXIT();
}

#if (XCP_CAL_PAG_STORE_SUPPORTED == STD_ON)

/*--------------------[Xcp_SetReqStoreCalReqCbk]---------------------------*/
FUNC(Std_ReturnType, XCP_APPL_CODE) Xcp_SetReqStoreCalReqCbk
(
  uint8 ServiceId,
  NvM_RequestResultType JobResult
)
{
  DBG_XCP_SETREQSTORECALREQCBK_ENTRY(ServiceId,JobResult);

  /* Check SessionStatus: Request to store calibration data */
  if( ( (Xcp_Status.SessionStatus & XCP_MASK_STORE_CAL_REQ) != 0U ) &&
      (ServiceId == XCP_NVM_WRITE_BLOCK_API_ID) )
  {
    /* Clear Xcp_Status.SessionStatus - STORE_CAL_REQ bit */
    XCP_CLEAR_STORE_CAL_REQ();

#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
    /* Check ServiceId and JobResult */
    if (JobResult == NVM_REQ_OK)
    {
      /* Enter critical area */
      SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

      /* Check if we have room to queue the EV_STORE_CAL event */
      if(Xcp_EventCTOIndex < XCP_EV_CTO_QUEUE_SIZE)
      {
        /* Queue the Event Packet EV_STORE_CAL */
        Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[0U] = XCP_EV_PID;
        Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[1U] = XCP_EV_STORE_CAL;
        Xcp_EventCTOQueue[Xcp_EventCTOIndex].Length = XCP_CALPAG_EV_STORE_CAL_LENGTH;
        Xcp_EventCTOIndex++;
      }

      /* Exit critical area */
      SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
    }  /* (JobResult == NVM_REQ_OK) */
#else
    TS_PARAM_UNUSED(JobResult);
#endif /* (XCP_EVENT_PACKET_ENABLED == STD_ON) */

  }  /* ( ( (Xcp_Status.SessionStatus & XCP_MASK_STORE_CAL_REQ) != 0U ) &&
          ( ServiceId == XCP_NVM_WRITE_BLOCK_API_ID) )*/


  DBG_XCP_SETREQSTORECALREQCBK_EXIT(E_OK,ServiceId,JobResult);
  return E_OK;
}
#endif /* (XCP_CAL_PAG_STORE_SUPPORTED == STD_ON) */

#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)

/*--------------------[Xcp_NvmStoreDaqSingleCbk]---------------------------*/

FUNC(Std_ReturnType, XCP_CODE) Xcp_NvmStoreDaqSingleCbk
(
  uint8 ServiceId,
  NvM_RequestResultType JobResult
)
{
  DBG_XCP_NVMSTOREDAQSINGLECBK_ENTRY(ServiceId,JobResult);

  if (ServiceId != XCP_NVM_WRITE_BLOCK_API_ID)
  {
    /* Ignore callback, it was wrongfully sent to us - we expect only the NvM_WriteBlock service */
  }
  else
  {
    /* Find out for which request the callback is received  */
    if ((Xcp_Status.SessionStatus & XCP_MASK_CLEAR_DAQ_REQ) != 0U)
    {
      /* Clear the CLEAR_DAQ_REQ flag regardless of the block request status */
      Xcp_Status.SessionStatus &= (uint8)(~XCP_MASK_CLEAR_DAQ_REQ);

#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
      if (JobResult == NVM_REQ_OK)
      {
        /* Enter critical area */
        SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

        /* If we have room to queue our event CTO */
        if(Xcp_EventCTOIndex < XCP_EV_CTO_QUEUE_SIZE)
        {
          /* Queue the Event Packet XCP_EV_CLEAR_DAQ */
          Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[0U] = XCP_EV_PID;
          Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[1U] = XCP_EV_CLEAR_DAQ;
          Xcp_EventCTOQueue[Xcp_EventCTOIndex].Length = 0x02U;
          Xcp_EventCTOIndex++;
        }

        /* Exit critical area */
        SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
      }
#else
      TS_PARAM_UNUSED(JobResult);
#endif
    }
    if ((Xcp_Status.SessionStatus & XCP_MASK_STORE_DAQ_REQ) != 0U)
    {
      /* clear the STORE_DAQ_REQ flag*/
      Xcp_Status.SessionStatus &= (uint8)(~XCP_MASK_STORE_DAQ_REQ);

#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
      if (JobResult == NVM_REQ_OK)
      {
        /* Enter critical area */
        SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

        /* If we have room to queue our event CTO */
        if(Xcp_EventCTOIndex < XCP_EV_CTO_QUEUE_SIZE)
        {
          /* Queue the Event Packet XCP_EV_STORE_DAQ */
          Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[0U] = XCP_EV_PID;
          Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[1U] = XCP_EV_STORE_DAQ;
          Xcp_EventCTOQueue[Xcp_EventCTOIndex].Length = 0x02U;
          Xcp_EventCTOIndex++;
        }

        /* Exit critical area */
        SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
      }
#else
      TS_PARAM_UNUSED(JobResult);
#endif
    }
  }

  DBG_XCP_NVMSTOREDAQSINGLECBK_EXIT(E_OK,ServiceId,JobResult);
  return E_OK;
}

/*--------------------------[Xcp_PrepareStoredDaqLists]-----------------------------*/
FUNC(void, XCP_CODE) Xcp_PrepareStoredDaqLists(void)
{
  /* Local variable to hold address of the requested DAQ List's parameters */
  P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr;
  /* Iterator through all DAQ lists */
  Xcp_DaqIdType DaqListNumber;

  DBG_XCP_PREPARESTOREDDAQLISTS_ENTRY();

#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK)
  /* Set the number of configured dynamic DAQ lists */
  Xcp_DynamicInfo.NoOfConfiguredDynamicDAQLists = Xcp_DaqLists.Xcp_StoreDaq.NoOfConfiguredDynamicDaqLists;
#endif

  /* Loop through all configured DAQ Lists to check which ones are selected and which ones are not */
  for (DaqListNumber = 0U; (DaqListNumber < XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(Xcp_DaqIdType)); 
       DaqListNumber++)
  {
    /* The byte location of this DAQ list in the selected DAQ array */
    uint16 ByteLocation = ( (uint16)DaqListNumber ) / XCP_NUMBER_OF_BITS_IN_BYTE;
    /* The bit corresponding to this DAQ list in the ByteLocation byte */
    uint8 BitOffset = (uint8)(DaqListNumber%XCP_NUMBER_OF_BITS_IN_BYTE);
    /* Retrieve DAQ List parameter */
    DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListNumber);

    /* If a DAQ List is selected to to be stored */
    if ((Xcp_DaqLists.Xcp_StoreDaq.SelectedDAQ[ByteLocation] & (uint8)(1U << BitOffset)) != 0U )
    {
      /* If selected DAQ lists have events associated queue all their events */
      if (DaqListPtr->EventId != XCP_INVALID_EVENT_CHANNEL)
      {
        /* Non predefined lists lost their associated events during the reset. The connection
         * must be made once again. */
        if (DaqListNumber >= XCP_MIN_DAQ)
        {
          Xcp_AssociateDAQListToEventChannel(DaqListNumber,DaqListPtr->EventId);
        }
        /* If there is a request to start in RESUME mode, also set the DAQ lists as running */
        if ((Xcp_DaqLists.Xcp_StoreDaq.Mode & XCP_MASK_STORE_DAQ_REQ_RESUME) != 0U)
        {
          /* Set DAQ list as running */
          XCP_SET_DAQLIST_RUNNING(DaqListPtr);
          /* Update number of DAQ Lists running */
          Xcp_Status.NoOfRunningDaq++;
        }
      }
    }
    else /* the DAQ list was not selected to be stored*/
    {
      /* Restore it with default values */
      Xcp_ResetDaqListData(DaqListNumber);
    }
  }

  DBG_XCP_PREPARESTOREDDAQLISTS_EXIT();
}

/*--------------------------[Xcp_PrepareStoredDaqListsResume]-----------------------------*/
FUNC(void, XCP_CODE) Xcp_PrepareStoredDaqListsResume(void)
{
  /* We save the state of the XCP so we can restore it if something goes wrong */
  Xcp_StateType SavedState  = Xcp_State;

  DBG_XCP_PREPARESTOREDDAQLISTSRESUME_ENTRY();

  /* Set the status of XCP as RESUME */
  Xcp_Status.SessionStatus |= XCP_MASK_RESUME_MODE;
  /* Set the state of XCP as RESUME */
  Xcp_State = XCP_STATE_RESUME;
  /* Set XCP slave status as DAQ running */
  Xcp_Status.SessionStatus |= XCP_MASK_DAQ_RUNNING;

  /* Prepare the stored DAQ lists which are selected */
  Xcp_PrepareStoredDaqLists();

  /* If any of the DAQ list is running */
  if (Xcp_Status.NoOfRunningDaq > 0U )
  {
    /* Try to send EV_RESUME_MODE event */
#if (XCP_EVENT_PACKET_ENABLED == STD_ON)

#if (XCP_TIMESTAMP_SUPPORTED == XCP_TIMESTAMP_MASK)
#if (XCP_USER_TIMESTAMP_SUPPORTED == STD_OFF)
    /* Local variable to hold the counter value */
    TickType CounterValue;
#endif  /* XCP_USER_TIMESTAMP_SUPPORTED == STD_OFF */

    /* Local variable to hold the timestamp */
    Xcp_TimestampType TimeStamp = 0U;

#if (XCP_USER_TIMESTAMP_SUPPORTED == STD_OFF)
    /* Retrieve present time stamp from the OS counter */
    (void) GetCounterValue(XCP_OS_COUNTER, &CounterValue);

    /* Convert the CounterVAlue to the Xcp_TimestampType and copy the value to the TimpeStamp variable */
    TimeStamp = (Xcp_TimestampType)CounterValue;
#else
    /* Get the timestamp value from the user provided function */
    TimeStamp = Xcp_ApplGetTimestamp();
#endif  /* (XCP_USER_TIMESTAMP_SUPPORTED == STD_OFF) */
#endif  /* XCP_TIMESTAMP_SUPPORTED == XCP_TIMESTAMP_MASK */

    /* Enter critical section */
    SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* If we have room to queue our event CTO */
    if(Xcp_EventCTOIndex < XCP_EV_CTO_QUEUE_SIZE)
    {
      /* Set the global flag indicating that the EV_RESUME_MODE is pending for transmission */
      Xcp_EvResumeModeTransmitPending = TRUE;

      /* Prepare the event XCP_EV_RESUME_MODE */
      Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[0U] = XCP_EV_PID;
      Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[1U] = XCP_EV_RESUME_MODE;
      Xcp_WriteWordToByteArray(&(Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[2U]),
                                 XCP_GET_SESSION_CONFIGID());
#if (XCP_TIMESTAMP_SUPPORTED == XCP_TIMESTAMP_MASK)
      /* Copy time stamp to response packet */
      Xcp_WriteDwordToByteArray( &(Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[4U]), TimeStamp );
      Xcp_EventCTOQueue[Xcp_EventCTOIndex].Length = 8U;
#else /* Timestamp is disabled */
      Xcp_EventCTOQueue[Xcp_EventCTOIndex].Length = 4U;
#endif /* XCP_TIMESTAMP_SUPPORTED == XCP_TIMESTAMP_MASK */
      Xcp_EventCTOIndex++;
    }

    /* Exit critical section.*/
    SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
#endif /* XCP_EVENT_PACKET_ENABLED == STD_ON */
  }
  else
  {
    /* Clear the status of XCP as it is no longer in RESUME */
    Xcp_Status.SessionStatus &= (uint8)(~XCP_MASK_RESUME_MODE);
    /* Restore the state of XCP */
    Xcp_State = SavedState;
    /* Clear the XCP slave status as DAQ running */
    Xcp_Status.SessionStatus &= (uint8)(~XCP_MASK_DAQ_RUNNING);
  }
  DBG_XCP_PREPARESTOREDDAQLISTSRESUME_EXIT();
}

#endif /* XCP_STORE_DAQ_SUPPORTED == STD_ON */

#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK)

FUNC(P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR), XCP_CODE) Xcp_GetDaqListPtr(Xcp_DaqIdType DaqListNumber)
{
  /* The queried pointer to a DAQ list */
  P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) RetValue;

  /* Deviation MISRA-6 <START> */
  P2VAR(Xcp_DaqType, TYPEDEF, XCP_VAR) AddressOfTheFirstDynamicDaq =
      (Xcp_DaqType *)(&(Xcp_DaqLists.Xcp_DynamicArea[0U]));
  /* Deviation MISRA-6 <STOP> */

  DBG_XCP_GETDAQLISTPTR_ENTRY(DaqListNumber);

#if (XCP_MIN_DAQ > 0)
  RetValue = (DaqListNumber<XCP_MIN_DAQ)?
      &(Xcp_DaqLists.Xcp_NonDynamicDaqLists.Xcp_Daq[DaqListNumber]):
      &(AddressOfTheFirstDynamicDaq[DaqListNumber - XCP_MIN_DAQ]);
#else
  RetValue = &(AddressOfTheFirstDynamicDaq[DaqListNumber - XCP_MIN_DAQ]);
#endif

  DBG_XCP_GETDAQLISTPTR_EXIT(RetValue, DaqListNumber);
  return RetValue;
}

FUNC(Xcp_DaqIdType, XCP_CODE) Xcp_GetNoOfDynamicDaqLists( void )
{
  Xcp_DaqIdType RetValue = Xcp_DynamicInfo.NoOfConfiguredDynamicDAQLists;

  DBG_XCP_GETNOOFDYNAMICDAQLISTS_ENTRY();

  DBG_XCP_GETNOOFDYNAMICDAQLISTS_EXIT(RetValue);
  return RetValue;
}

#endif /* XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK */

/*==================[internal function definitions]=========================*/

/*-----------------------------[Xcp_GetLengthToBeReserved]-----------------------------*/
STATIC FUNC(uint8, XCP_CODE) Xcp_GetLengthToBeReserved
(
  uint8 CommandId
)
{
  /* The calculated length to be reserved in the CTO response queue */
  uint8 ReserveLength;

  DBG_XCP_GETLENGTHTOBERESERVED_ENTRY(CommandId);

  /* If command is UPLOAD or SHORT_UPLOAD the length depends on the command parameters */
  if ((CommandId == XCP_CMD_UPLOAD_PID) || (CommandId == XCP_CMD_SHORT_UPLOAD_PID))
  {
    ReserveLength = XCP_MIN(
                            ((XCP_ADDRESS_GRANULARITY_SIZE * XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX))
                             + XCP_ADDRESS_GRANULARITY_SIZE),
                             XCP_MAX_CTO
                           );
  }
  else
  {
    /* Get the length statically */
    ReserveLength = Xcp_Commands[CommandId - XCP_CMD_PID_BASE].CommandMaxResponseLength;
  }

  DBG_XCP_GETLENGTHTOBERESERVED_EXIT(ReserveLength,CommandId);
  return ReserveLength;
}

/*--------------------------[Xcp_Disconnect]--------------------------------*/

FUNC(void, XCP_CODE) Xcp_Disconnect(void)
{
  DBG_XCP_DISCONNECT_ENTRY();

  /* Flush command queue */
  Xcp_FlushCMDQueue();
  /* Terminate session */
  Xcp_TerminateSession();
  /* Initialize command processor */
  Xcp_InitCommandProcessor();
  /* If DAQ/STIM resource is supported */
#if (XCP_MAX_DAQ != 0U)
  /* Reset DAQList runtime information */
  Xcp_ResetEventProcessor();
#endif
  /* Initialize transmit processor */
  Xcp_InitTransmitProcessor();

  DBG_XCP_DISCONNECT_EXIT();
}

/*==================[internal function definitions]=========================*/

/*-----------------------------[Xcp_ReserveBytesToRespCTOQueue]-------------*/

STATIC FUNC(Std_ReturnType, XCP_CODE) Xcp_ReserveBytesToRespCTOQueue
(
  Xcp_CtoNoOfBytesType ReserveBytes
)
{
  /* Local variable to hold the number of bytes that need to be reserved plus the length (1 byte) */
  Xcp_CtoNoOfBytesType ReserveBytesPlusLength = ReserveBytes + XCP_RESP_CTO_QUEUE_LENGTH_SIZE;
  /* Local variable to hold return value */
  Std_ReturnType RetVal = E_NOT_OK;

  DBG_XCP_RESERVEBYTESTORESPCTOQUEUE_ENTRY(ReserveBytes);

  /* Check if there is enough free space into the Response CTO queue */
  if ( ReserveBytesPlusLength <= XCP_GET_CTOQUEUE_FREESPACE(uint32) )
  {
    /* Enough free space: set the ReservedBytes variable */
    Xcp_CtoQueue.ReservedBytes += ReserveBytesPlusLength;
    /* Set the return value to E_OK */
    RetVal = E_OK;
  }

  /* Return E_OK or E_NOT_OK */

  DBG_XCP_RESERVEBYTESTORESPCTOQUEUE_EXIT(RetVal,ReserveBytes);
  return RetVal;
}

#if (((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
      (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)) && \
     (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK))

/*------------------------[Xcp_ClearODTEntries]-----------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_ClearODTEntries
(
  P2VAR(Xcp_OdtEntryType, AUTOMATIC, XCP_APPL_DATA) ODTEntryPtr,
  uint8                                             NoOfODTEntries
)
{
  /* Local variable to keep track of the cleared ODT entries */
  uint8 NoOfClearedODTEntries;

  DBG_XCP_CLEARODTENTRIES_ENTRY(ODTEntryPtr,NoOfODTEntries);

  /* Clear all ODT Entries */
  for (NoOfClearedODTEntries = 0U;
       NoOfClearedODTEntries < NoOfODTEntries;
       ++NoOfClearedODTEntries)
  {
    /* Clear the pointer to the data element as per ASAM specifications
     *
     * Req ID. Xcp.ASAM.CLEARDAQLIST003  */
    ODTEntryPtr[NoOfClearedODTEntries].Address   = NULL_PTR;
    /* Clear bit offset */
    ODTEntryPtr[NoOfClearedODTEntries].BitOffset = 0xFFU;
    /* Clear address extension */
    ODTEntryPtr[NoOfClearedODTEntries].AddrExtn  = 0U;
    /* Reset data element length */
    ODTEntryPtr[NoOfClearedODTEntries].Length    = 0U;
  }

  DBG_XCP_CLEARODTENTRIES_EXIT(ODTEntryPtr,NoOfODTEntries);
}

/*----------------------------[Xcp_ClearODTs]--------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_ClearODTs
(
  P2VAR(Xcp_OdtType, AUTOMATIC, XCP_APPL_DATA) ODTPtr,
  uint8                                        NoOfODTs
)
{
  /* Local variable to keep track of the cleared ODTs */
  uint8 NoOfClearedODTs;

  DBG_XCP_CLEARODTS_ENTRY(ODTPtr,NoOfODTs);

  /* Clear all ODTs */
  for (NoOfClearedODTs = 0; NoOfClearedODTs < NoOfODTs; ++NoOfClearedODTs)
  {
    /* Clear all ODT Entries */
    Xcp_ClearODTEntries(
      ODTPtr[NoOfClearedODTs].OdtEntry,
      ODTPtr[NoOfClearedODTs].NrOfOdtEntries);
  }

  DBG_XCP_CLEARODTS_EXIT(ODTPtr,NoOfODTs);
}

#endif

/*-----------------------------[Xcp_InsertResponseCTO]-----------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_InsertResponseCTO
(
  P2VAR(Xcp_CTOType, AUTOMATIC, XCP_APPL_DATA) CtoPtr
)
{
  /* Local variable index CTO bytes */
  uint8 CtoByteIndex;

  DBG_XCP_INSERTRESPONSECTO_ENTRY(CtoPtr);

  /* Enqueue CTO length */
  Xcp_EnqueueByteInCtoQueue(CtoPtr->Length);
  /* Enqueue each byte in CTO into CTO Queue */
  for (CtoByteIndex = 0U; CtoByteIndex < CtoPtr->Length; CtoByteIndex++)
  {
    Xcp_EnqueueByteInCtoQueue(CtoPtr->CTO[CtoByteIndex]);
  }

  DBG_XCP_INSERTRESPONSECTO_EXIT(CtoPtr);
}

/*--------------------------[Xcp_TerminateSession]---------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_TerminateSession(void)
{
  DBG_XCP_TERMINATESESSION_ENTRY();

  /* Flush CTO Queue */
  Xcp_FlushCTOQueue();
  /* If event packet transmission enabled */
#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
  /* Flush XCP events CTOs */
  Xcp_FlushEventCTOQueue();
#endif

/* If DAQ or STIM resource is supported */
#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))

  /* Reset event channels runtime information */
  Xcp_ResetEventChannels();

/* If DAQ resource is supported */
#if (XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK)
  /* Flush DTO Queue */
  Xcp_FlushDTOQueue();
#endif /* (XCP_MAX_DAQ != 0U) */


/* If STIM resource is supported */
#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
  /* Flush STIM Buffer */
  Xcp_FlushStimBuffer();
#endif /* (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK) */
#endif /* ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) */

  /* Set connection mode */
  Xcp_State = XCP_STATE_DISCONNECTED;

  DBG_XCP_TERMINATESESSION_EXIT();
}

/*----------------[Xcp_RemoveDAQListFromEventChannel]------------------------*/
/* If DAQ/STIM resource is supported */
#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))

STATIC FUNC(void, XCP_CODE) Xcp_RemoveDAQListFromEventChannel
(
  Xcp_DaqIdType DaqListNumber,
  uint16 EventChannelNumber
)
{
  /* Local variable to hold daq list index with respect to other DAQ lists
   * associated with the event */
  uint16 DaqListIndex = 0U;
  /* Local variable to hold address of the requested Event channel
   * parameters */
  Xcp_EventType *EventParamPtr = &Xcp_Event[EventChannelNumber];

  DBG_XCP_REMOVEDAQLISTFROMEVENTCHANNEL_ENTRY(DaqListNumber,EventChannelNumber);

  /* Find DAQ List index from DAQ lists associated with the event. */

  while ( (DaqListNumber != EventParamPtr->DaqIdList[DaqListIndex]) &&
          (DaqListIndex != EventParamPtr->DaqIdListCount)
        )
  {
    /* Verify next */
    DaqListIndex++;
  }
  /* Dis-associate DAQList from the event channel */
  /* Decrement number of lists associated with the event channel */
  if (0U < XCP_GET_EVENT_NO_OF_DAQLIST(EventChannelNumber))
  {
    XCP_GET_EVENT_NO_OF_DAQLIST(EventChannelNumber)--;
  }
  /* Move the DAQ list with same or less priority up.  Note: The list is
   * already sorted */
  while (DaqListIndex <
          XCP_GET_EVENT_NO_OF_DAQLIST(EventChannelNumber))
  {
    /* Move the lists up */
    EventParamPtr->DaqIdList[DaqListIndex] =
       EventParamPtr->DaqIdList[DaqListIndex + 0x1U];
    /* Move to  next */
    DaqListIndex++;
  }

  DBG_XCP_REMOVEDAQLISTFROMEVENTCHANNEL_EXIT(DaqListNumber,EventChannelNumber);
}

/*----------------[Xcp_AssociateDAQListToEventChannel]-----------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_AssociateDAQListToEventChannel
(
  Xcp_DaqIdType DaqListNumber,
  uint16 EventChannelNumber
)
{
  /* Local variable to hold insert location index */
  uint8 InsertDaqListIndex = XCP_GET_EVENT_NO_OF_DAQLIST(EventChannelNumber);
  /* Local variable to hold address of the requested Event channel
   * parameters */
  Xcp_EventType *EventParamPtr = &Xcp_Event[EventChannelNumber];

  DBG_XCP_ASSOCIATEDAQLISTTOEVENTCHANNEL_ENTRY(DaqListNumber,EventChannelNumber);

  /* Check whether there is any DAQ List associated with the event */
  if (XCP_GET_EVENT_NO_OF_DAQLIST(EventChannelNumber) > 0U)
  {
    /* Local variable to hold address of the requested DAQ List's parameters */
    P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListToBeAssociatedPtr = XCP_GET_DAQ_LIST_PTR(DaqListNumber);

    /* Insert the DAQ list number in the event's list of associated DAQ lists based on its priority.
     * We assume that the list is already sorted in a decreasing order. */
    for (; InsertDaqListIndex > 0U; InsertDaqListIndex--)
    {
      /* Get the nearest left DAQ list from the current index */
      P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr = XCP_GET_DAQ_LIST_PTR(EventParamPtr->DaqIdList[InsertDaqListIndex-1U]);
      if (DaqListToBeAssociatedPtr->Priority <= DaqListPtr->Priority)
      {
        /* DAQ list to be associated is at the right position to be inserted. */
        break;
      }
      else
      {
        /* Move the queried DAQ list to the right as the priority is lower than the one the new
         * DAQ list has */
        EventParamPtr->DaqIdList[InsertDaqListIndex] = EventParamPtr->DaqIdList[InsertDaqListIndex-1U];
      }
    }
  }

  /* Insert the DAQ List  */
  EventParamPtr->DaqIdList[InsertDaqListIndex] = DaqListNumber;
  /* Update the number of lists associated with the event */
  XCP_GET_EVENT_NO_OF_DAQLIST(EventChannelNumber)++;

  DBG_XCP_ASSOCIATEDAQLISTTOEVENTCHANNEL_EXIT(DaqListNumber,EventChannelNumber);
}

#endif /* (XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) */
/*-----------------------[Xcp_EnqueueByteInCtoQueue]----------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_EnqueueByteInCtoQueue(uint8 DataByte)
{
  DBG_XCP_ENQUEUEBYTEINCTOQUEUE_ENTRY(DataByte);

  /* Enqueue the byte*/
  Xcp_CtoQueue.Queue[Xcp_CtoQueue.Tail] = DataByte;
  /* Update the tail */
  Xcp_CtoQueue.Tail++;
  Xcp_CtoQueue.Tail %= XCP_CTO_QUEUE_SIZE;
  /* Update number of bytes in CTO Queue */
  Xcp_CtoQueue.NoOfBytes++;

  DBG_XCP_ENQUEUEBYTEINCTOQUEUE_EXIT(DataByte);
}

/*----------------------------[Xcp_DeQueueCmdQueue]--------------------------*/

/* DeQueue first command from command queue. */
STATIC FUNC(void, XCP_CODE) Xcp_DeQueueCmdQueue(void)
{
  DBG_XCP_DEQUEUECMDQUEUE_ENTRY();

  /* If queue is not empty */
  if (0U != XCP_GET_NO_OF_COMMANDS())
  {
    /* Update Queue Head */
    Xcp_CommandQueue.Head++;
    Xcp_CommandQueue.Head %= XCP_CMD_PROCESSOR_QUEUE_SIZE;
    /* Update number of commands */
    Xcp_CommandQueue.NoOfCmds--;
  }

  DBG_XCP_DEQUEUECMDQUEUE_EXIT();
}

/*---------------------------[Xcp_DeQueueCtoQueue]---------------------------*/

/* DeQueue first CTO from CTO queue. */
STATIC FUNC(void, XCP_CODE) Xcp_DeQueueCtoQueue
(
  P2VAR(uint8, AUTOMATIC, XCP_APPL_DATA) BufferPtr
)
{
  /* Local variable to hold number of bytes occupied by the first CTO in
   * queue */
  uint8 CtoSizeInQueue = XCP_GET_NEXT_CTO_SIZE();
  /* Local variable to hold data index */
  PduLengthType DataIndex = 0;

  DBG_XCP_DEQUEUECTOQUEUE_ENTRY(BufferPtr);

  /* Check whether queue head needs to be rolled over */
  ++Xcp_CtoQueue.Head;
  Xcp_CtoQueue.Head %= XCP_CTO_QUEUE_SIZE;
  /* Update total number of bytes in CTO queue */
  Xcp_CtoQueue.NoOfBytes--;
  /* Copy CTO bytes until all bytes are copied */
  while (0U != CtoSizeInQueue)
  {
    /* Copy CTO bytes to user buffer */
    BufferPtr[DataIndex] = Xcp_CtoQueue.Queue[Xcp_CtoQueue.Head];
    /* Update data index */
    DataIndex++;
    /* Update remaining bytes of CTO available in queue */
    CtoSizeInQueue--;
    /* Update total number of bytes in CTO queue */
    Xcp_CtoQueue.NoOfBytes--;
    /* Check whether queue head needs to be rolled over */
    ++Xcp_CtoQueue.Head;
    Xcp_CtoQueue.Head %= XCP_CTO_QUEUE_SIZE;
  }

  DBG_XCP_DEQUEUECTOQUEUE_EXIT(BufferPtr);
}

/*---------------------------[Xcp_ProcessCmd]-------------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_ProcessCmd(void)
{
  /* Local variable to hold command ID */
  const uint8 Command = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE0_INDEX);
  /* Variable to hold the error message, if any */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMD_ENTRY();

  /* Initialize the response */
  ResponseCTO.Length = 0U;
  ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;

  if (Xcp_CommandCanBeExecuted(Command, &ResponseCTO))
  {
    /* Execute the command */
    const Xcp_ErrorType cmdProcError = Xcp_ExecuteCommand(Command);
    /* Enter critical section.*/
    SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
    /* Check the return value of the command execution */
    switch (cmdProcError)
    {
      case XCP_E_BUSY:
        /* Command processing can't be finished in this cycle, trigger state change */
        XCP_CMD_PROCESSOR_STATE_TRANS(XCP_CMDPROCESSOR_BUSY);
        break;
      case XCP_E_OK:
      case XCP_E_NOT_OK:
        /* Command processing finished, dequeue the command from command queue */
        Xcp_DeQueueCmdQueue();
        /* Trigger state transition */
        XCP_CMD_PROCESSOR_STATE_TRANS(XCP_CMDPROCESSOR_IDLE);
        break;
      case XCP_E_INSUFFICIENT_MEMORY:
        /* Do nothing, keep the command in queue until the CTO response queue has enough space
         * to queue the response. */
        break;
      /* CHECK: NOPARSE */
      default:
        /* all possible cases have been explicitly covered in 'case's above, the 'default'
         * cannot be reached */
        break;
      /* CHECK: PARSE */
    }
    /* Exit critical section.*/
    SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
  }
  else
  {
    /* If we have a response to send */
    if (ResponseCTO.Length > 0U)
    {
      /* The command didn't passed the validation, abort with error */
      Xcp_AbortCommandWithError(&ResponseCTO);

#if ((XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON) || \
     (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON))
      /* Enter critical section.*/
      SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

#if (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON)
      if (Command == XCP_CMD_DOWNLOAD_PID)
      {
        /* The block mode was put as active by the receive processor but the DOWNLOAD command
         * is not allowed to execute, therefore we must clear the block mode flag */
        Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_DOWNLOAD_BLOCK_MODE_MASK);
      }
#endif /* (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON) */

#if (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON)
      if (Command == XCP_CMD_PROGRAM_PID)
      {
        /* The block mode was put as active by the receive processor but the PROGRAM command
         * is not allowed to execute, therefore we must clear the block mode flag */
        Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_PGM_BLOCK_MODE_MASK);
      }
#endif /* (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON) */

      /* Exit critical section.*/
      SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
#endif /* ((XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON) || \
        *  (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON)) */
    }
  }

  DBG_XCP_PROCESSCMD_EXIT();
}

/*-----------------------[Xcp_ExecuteCommand]--------------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ExecuteCommand(uint8 Command)
{
  /* Function return value */
  Xcp_ErrorType RetValue;
  /* The result of byte reservation function */
  Std_ReturnType ReservationResult;

  /* Determine the number of bytes to be reserved in the CTO response queue for this command */
  uint8 ReserveLength = Xcp_GetLengthToBeReserved(Command);

  DBG_XCP_EXECUTECOMMAND_ENTRY(Command);

  /* Enter critical area */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Reserve bytes to the response CTO Queue */
  ReservationResult = Xcp_ReserveBytesToRespCTOQueue( ReserveLength );

  /* Exit critical area */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  /* Check if the response reservation has been successful */
  if ( ReservationResult == XCP_OK )
  { /* Response CTO bytes have been reserved into the response CTO queue */
    /* Execute the command handler */
    RetValue = (Xcp_Commands[Command - XCP_CMD_PID_BASE].CommandHandlerPtr)();

    /* Enter critical area */
    SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* Remove the reserved bytes from the response CTO queue. The cast is safe as it will be
     * cast to the same type or greater. */
    XCP_RESP_CTO_QUEUE_FREE_RESERVED( (Xcp_CtoNoOfBytesType) ReserveLength );

    /* Exit critical area */
    SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
  }
  else
  {
    /* Command was not executed, wait for the CTO response queue to get additional free bytes */
    RetValue = XCP_E_INSUFFICIENT_MEMORY;
  }

  DBG_XCP_EXECUTECOMMAND_EXIT(RetValue,Command);
  return RetValue;
}

/*--------------------------[Xcp_ProcessCmdConnect]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdConnect(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDCONNECT_ENTRY();

  /* Check whether mode parameter is valid */
  switch (XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX))
  {
    case XCP_CONNECT_MODE_NORMAL:
      /* Prepare CTO with positive response */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
      ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_RESOURCE_SUPPORTED;
      ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_COMM_MODE_BASIC;
      ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = XCP_MAX_CTO;
      Xcp_WriteWordToByteArray(&ResponseCTO.CTO[XCP_RES_BYTE4_INDEX], XCP_MAX_DTO);
      ResponseCTO.CTO[XCP_RES_BYTE6_INDEX] = XCP_PROTOCOL_LAYER_MAJOR_VN;
      ResponseCTO.CTO[XCP_RES_BYTE7_INDEX] = XCP_TRANSPORT_LAYER_MAJOR_VN;
      ResponseCTO.Length = XCP_CMD_CONNECT_RES_LENGTH;
      /* Set connection mode */
      Xcp_State = XCP_STATE_CONNECTED;
    break;
    case XCP_CONNECT_MODE_USERDEFINED:
      /* Shall be implemented later */
    default:
      /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
      ResponseCTO.Length = XCP_ERR_CMD_SYNTAX_LENGTH;
    break;
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDCONNECT_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------------[Xcp_ProcessCmdDisConnect]-----------------------*/

STATIC FUNC(Std_ReturnType, XCP_CODE)  Xcp_ProcessCmdDisConnect(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  /* Standard return value */
  Std_ReturnType RetValue = E_OK;

  DBG_XCP_PROCESSCMDDISCONNECT_ENTRY();

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* find out if there is a pending storage/clearing of DAQ lists in the NV memory */
  if (((Xcp_Status.SessionStatus & XCP_MASK_CLEAR_DAQ_REQ) != 0U ) ||
      ((Xcp_Status.SessionStatus & XCP_MASK_STORE_DAQ_REQ) != 0U ))
  {
    /* Prepare CTO with negative response ERR_CMD_BUSY */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_BUSY;
    ResponseCTO.Length = XCP_ERR_CMD_BUSY_LENGTH;

    /* Clear pending status of DISCONNECT command */
    Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_DISCONNECT_PENDING_MASK);

    RetValue = E_NOT_OK;
  }
  else
  {
    /* Perform an XCP disconnect */
    Xcp_Disconnect();

    /* Prepare CTO with positive response */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
    ResponseCTO.Length = XCP_CMD_DISCONNECT_RES_LENGTH;
  }

  /* Queue the response */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  DBG_XCP_PROCESSCMDDISCONNECT_EXIT(RetValue);

  return RetValue;
}

/*--------------------------[Xcp_ProcessCmdGetId]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetId(void)
{
  /* Retrieve requested Identification Type  */
  uint8 IdentificationType = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETID_ENTRY();

  switch(IdentificationType)
  {
    /* ASAM-MC2 filename without path and extension */
    case XCP_IDENTIFICATION_TYPE1:
    {
      /* Prepare CTO with positive response */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
      ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_IDENTIFICATION_TRANSFER_MASK;
      ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_RESERVED_BYTE;
      ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = XCP_RESERVED_BYTE;
      /* Fill the identification type length */
      Xcp_WriteDwordToByteArray(
        &(ResponseCTO.CTO[XCP_RES_BYTE4_INDEX]),
        XCP_IDENTIFICATION_TYPE1_LENGTH);

#if (XCP_IDENTIFICATION_TYPE1_LENGTH > 0)
#if ((XCP_CMD_GETID_RES_LENGTH_MIN + XCP_IDENTIFICATION_TYPE1_LENGTH) > XCP_MAX_CTO)
      /* If response length is greater than a single CTO size */
      /* Set MTA */
      Xcp_MTA.Type         = XCP_MTATYPE_ADDRESS;
      Xcp_MTA.Address.Read = Xcp_IdentificationTypeOne;
      Xcp_MTA.IsSet        = TRUE;
      /* Set response data length to MAX_CTO */
      ResponseCTO.Length   = XCP_CMD_GETID_RES_LENGTH_MIN;
#else
      /* If response length is less than a single CTO size */
      /* Copy identification field bytes to response CTO */
      TS_MemCpy(
        &(ResponseCTO.CTO[XCP_RES_BYTE8_INDEX]),
        Xcp_IdentificationTypeOne,
        XCP_IDENTIFICATION_TYPE1_LENGTH);
      /* Set response length */
      ResponseCTO.Length = (uint8)(XCP_CMD_GETID_RES_LENGTH_MIN + XCP_IDENTIFICATION_TYPE1_LENGTH);
#endif /* #if((XCP_CMD_GETID_RES_LENGTH_MIN + XCP_IDENTIFICATION_TYPE1_LENGTH) > XCP_MAX_CTO) */
#else
      /* Set response length */
      ResponseCTO.Length = (uint8)(XCP_CMD_GETID_RES_LENGTH_MIN + XCP_IDENTIFICATION_TYPE1_LENGTH);
#endif /* #if (XCP_IDENTIFICATION_TYPE1_LENGTH > 0) */
    }
    break;
    /* ASCII text */
    case XCP_IDENTIFICATION_TYPE0:
    /* ASAM-MC2 filename with path and extension */
    case XCP_IDENTIFICATION_TYPE2:
    /* URL where the ASAM-MC2 file can be found */
    case XCP_IDENTIFICATION_TYPE3:
    /* ASAM-MC2 file to upload */
    case XCP_IDENTIFICATION_TYPE4:
    /* User defined */
    default: /* Prepare CTO with positive response */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
      ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_IDENTIFICATION_TRANSFER_MASK;
      ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_RESERVED_BYTE;
      ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = XCP_RESERVED_BYTE;
      /* Since requested identification type is not supported return length
       * 0(zero)*/
      Xcp_WriteDwordToByteArray(
        &(ResponseCTO.CTO[XCP_RES_BYTE4_INDEX]), 0U);
      /* Set response length */
      ResponseCTO.Length = XCP_CMD_GETID_RES_LENGTH_MIN;
    break;
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETID_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------------[Xcp_ProcessCmdGetCommModeInfo]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetCommModeInfo(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETCOMMMODEINFO_ENTRY();

  /* Prepare CTO with positive response */
  ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
  ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_RESERVED_BYTE;
  ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_COMM_MODE_OPTIONAL;
  ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = XCP_RESERVED_BYTE;
  ResponseCTO.CTO[XCP_RES_BYTE4_INDEX] = XCP_MAX_BS;
  ResponseCTO.CTO[XCP_RES_BYTE5_INDEX] = XCP_MIN_ST;
  /* Replace the reserved value with XCP_CMD_PROCESSOR_QUEUE_SIZE when interleaved communication
   * is implemented */
  ResponseCTO.CTO[XCP_RES_BYTE6_INDEX] = XCP_RESERVED_BYTE;
  ResponseCTO.CTO[XCP_RES_BYTE7_INDEX] = XCP_DRIVER_VERSION;
  /* Set response length */
  ResponseCTO.Length = XCP_CMD_GET_COMM_INFO_RES_LENGTH;

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETCOMMMODEINFO_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------------[Xcp_ProcessCmdGetStatus]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetStatus(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETSTATUS_ENTRY();

  /* Prepare CTO with positive response */
  ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
  ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_GET_SESSION_STATUS();
  ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_GET_RESOURCE_PROTECTION_STATUS();
  ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = XCP_RESERVED_BYTE;
  Xcp_WriteWordToByteArray(&(ResponseCTO.CTO[XCP_RES_BYTE4_INDEX]),
                           XCP_GET_SESSION_CONFIGID());
  ResponseCTO.Length = XCP_CMD_GETSTATUS_RES_LENGTH;

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETSTATUS_EXIT(XCP_E_OK);
  return XCP_E_OK;
}
#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))
/*----------------------[Xcp_ProcessCmdStartStopDAQList]---------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdStartStopDAQList(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDSTARTSTOPDAQLIST_ENTRY();

  /* Verify protection status for both DAQ and STIM*/
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* BOTH Daq and STIM is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Retrieve requested DAQ List number */
    const uint16 DaqListNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);

    /* If DAQ List number is invalid */
    if (DaqListNumber >= XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(uint16))
    {
      /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
      ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
    }
    else /* DAQ List number is valid */
    {
      /* Local variable to hold address of the requested DAQ List's parameters */
      P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListNumber);

        /* Verify DAQ protection status */
      if(((XCP_MASK_DAQLIST_DIRECTION_DAQ ==
          (DaqListPtr->Mode & XCP_MASK_DAQLIST_DIRECTION)) &&
          ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
           (uint8)XCP_RESOURCE_DAQ_MASK)
         )||
         /* Verify STIM protection status */
         ((XCP_MASK_DAQLIST_DIRECTION_STIM ==
           (DaqListPtr->Mode & XCP_MASK_DAQLIST_DIRECTION)) &&
           ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
            (uint8)XCP_RESOURCE_STIM_MASK)
         )
        )
      {
        /* Either Daq or STIM is protected return ERR_ACCESS_LOCKED */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
        ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
      }
      /* To check if direction is not set or event is not configured */
      else if (XCP_IS_DAQLIST_FULLY_CONFIGURED(DaqListPtr) == FALSE)
      {
        /* Prepare error CTO with negative response ERR_DAQ_CONFIG when DAQ Direction is not clear
         * and event not configured */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_DAQ_CONFIG;
        ResponseCTO.Length = XCP_ERR_DAQ_CONFIG_LENGTH;
      }
      else
      {
        /* Retrieve requested mode */
        uint8 DaqMode = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);

        /* Handle each requested mode */
        switch(DaqMode)
        {
          /* Mode = 00 = Stop */
          case XCP_SSDL_MODE_STOP:
              /* Check whether DAQ List is running */
              if(XCP_IS_DAQ_LIST_RUNNING(DaqListPtr) == TRUE)
              {
                /*Stop the DAQ List*/
                XCP_SET_DAQLIST_STOPPED(DaqListPtr);
                /* Update the number of DAQ Lists running */
                XCP_GET_NO_OF_DAQLISTS_RUNNING()--;
                /* If all DAQ lists got stopped */
                if (XCP_GET_NO_OF_DAQLISTS_RUNNING() == 0U)
                {
                  /* Clear Xcp DAQ running status */
                  XCP_CLEAR_XCP_STATUS_DAQ_RUNNING();
                }
              }

              /* Remove stopped DTOs from DTO Queue */
              Xcp_ClearStoppedDtos();

              /* Prepare response CTO */
              ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
              /* If DAQ Packet Identification is of type "Absolute PID" */
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK)
              ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_GET_FIRST_PID(DaqListPtr);
#else
              /* The value is not significant when Packet Identification is of
               * type is not "Absolute PID" */
              ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_RESERVED_BYTE;
#endif
              ResponseCTO.Length =XCP_CMD_SSDL_RES_LENGTH;
          break;
          /* Mode = 01 = Start */
          case XCP_SSDL_MODE_START:
              /* Check whether DAQ List is already running */
              if (XCP_IS_DAQ_LIST_RUNNING(DaqListPtr) == FALSE)
              {
                /* Set DAQ list as running */
                XCP_SET_DAQLIST_RUNNING(DaqListPtr);
                /* Update number of DAQ Lists running */
                XCP_GET_NO_OF_DAQLISTS_RUNNING()++;
              }
              /* Set XCP slave status as DAQ running */
              XCP_SET_XCP_STATUS_DAQ_RUNNING();
              /* Prepare response CTO */
              ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
              /* If DAQ Packet Identification is of type "Absolute PID" */
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK)
              ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_GET_FIRST_PID(DaqListPtr);
#else
              /* The value is not significant when Packet Identification is of
               * type is not "Absolute PID" */
              ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_RESERVED_BYTE;
#endif
              ResponseCTO.Length = XCP_CMD_SSDL_RES_LENGTH;

              /* If this is a cyclic event
                 set the cyclic event time cycle to the initial value 1U.
                 This value allows the event to trigger for the first time when
                 the Xcp_MainFunction is called */
              XCP_CYCLIC_EVENTS_CNT_INI( );

          break;
          /* Mode = 02 = Select */
          case XCP_SSDL_MODE_SELECT:
              /* Set the DAQ List as selected */
              XCP_SET_DAQLIST_SELECTED(DaqListPtr);
              /* Update number of selected DAQ Lists */
              XCP_GET_NO_OF_DAQLISTS_SELECTED()++;
              /* Prepare response CTO */
              ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
              /* If DAQ Packet Identification is of type "Absolute PID" */
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK)
              ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_GET_FIRST_PID(DaqListPtr);
#else
              /* The value is not significant when Packet Identification is of
               * type is not "Absolute PID" */
              ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_RESERVED_BYTE;
#endif
              ResponseCTO.Length =XCP_CMD_SSDL_RES_LENGTH;
          break;
          /* Undefined mode parameter */
          default:
            /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
            ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
            ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
            ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
          break;
        }
      }
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDSTARTSTOPDAQLIST_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*----------------------[Xcp_ProcessCmdStartStopSynch]---------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdStartStopSynch(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDSTARTSTOPSYNCH_ENTRY();

  /* If both DAQ and STIM resources are protected */
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* Both Daq and STIM is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Process the START STOP Sych command request */
    Xcp_ProcessStartStopSynch(&ResponseCTO);
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDSTARTSTOPSYNCH_EXIT(XCP_E_OK);
  return XCP_E_OK;
}
/*----------------------[Xcp_ProcessStartStopSynch]---------------------*/
STATIC FUNC(void, XCP_CODE) Xcp_ProcessStartStopSynch
(
  P2VAR(Xcp_CTOType,AUTOMATIC,AUTOMATIC) ResponseCTOPtr
)
{
  /* Local variable to hold DAQ List number */
  Xcp_DaqIdType DaqListNumber;
  /* Local variable to hold address of the requested DAQ List's parameters */
  P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr;

  DBG_XCP_PROCESSSTARTSTOPSYNCH_ENTRY(ResponseCTOPtr);

  /* Handle each requested mode */
  switch (XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX))
  {
    /* Mode = 00 = Stop All */
    case XCP_SS_SYNCH_MODE_STOP_ALL:
      /* Loop through all DAQ Lists until the last DAQ List */
      for (DaqListNumber = 0U; DaqListNumber < XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(Xcp_DaqIdType);
           ++DaqListNumber)
      {
        /* Retrieve DAQ List parameter */
        DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListNumber);

        /* Check whether DAQ List is already running */
        if (XCP_IS_DAQ_LIST_RUNNING(DaqListPtr) == TRUE)
        {
          /* Stop the DAQ List */
          XCP_SET_DAQLIST_STOPPED(DaqListPtr);
          /* Update the number of DAQ Lists running */
          XCP_GET_NO_OF_DAQLISTS_RUNNING()--;
        }
      }
      /* Clear Xcp DAQ running status */
      XCP_CLEAR_XCP_STATUS_DAQ_RUNNING();

      /* Flush DTO Queue to avoid transmission of any pending DTOs */
      Xcp_FlushDTOQueue();

      /* Prepare CTO with positive response */
      ResponseCTOPtr->CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
      ResponseCTOPtr->Length = XCP_CMD_SS_SYNCH_RES_LENGTH;
    break;
    /* Mode = 01 = Start Selected */
    case XCP_SS_SYNCH_MODE_START_SELECTED:
      /* Loop through all DAQ Lists until the last DAQ List or until all
       * selected DAQ Lists are covered */
      for (DaqListNumber = 0U;
         ((DaqListNumber < XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(Xcp_DaqIdType)) &&
          (0U != XCP_GET_NO_OF_DAQLISTS_SELECTED()));
         ++DaqListNumber)
      {
        /* Retrieve DAQ List parameter */
        DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListNumber);
        /* If the DAQ List is selected */
        if (XCP_IS_DAQ_LIST_SELECTED(DaqListPtr) == TRUE)
        {
          /* Check whether DAQ List is already running */
          if (FALSE == XCP_IS_DAQ_LIST_RUNNING(DaqListPtr))
          {
            /* Set DAQ list as running */
            XCP_SET_DAQLIST_RUNNING(DaqListPtr);
            /* Update number of DAQ Lists running */
            XCP_GET_NO_OF_DAQLISTS_RUNNING()++;
          }

          /* If this is a cyclic event
             set the cyclic event time cycle to the initial value 1U.
             This value allows the event to trigger for the first time when
             the Xcp_MainFunction is called */
          XCP_CYCLIC_EVENTS_CNT_INI( );

          /* Update number of DAQ Lists selected as the selected DAQ list is
           * running */
          XCP_GET_NO_OF_DAQLISTS_SELECTED()--;
          /* Clear selected status of the DAQ List */
          XCP_CLEAR_DAQLIST_SELECTED(DaqListPtr);
        }
      }
      /* If any of the DAQ list is running */
      if (0U != XCP_GET_NO_OF_DAQLISTS_RUNNING())
      {
        /* Set XCP slave status as DAQ running */
        XCP_SET_XCP_STATUS_DAQ_RUNNING();
      }
      /* Prepare CTO with positive response */
      ResponseCTOPtr->CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
      ResponseCTOPtr->Length = XCP_CMD_SS_SYNCH_RES_LENGTH;
    break;
    /* Mode = 02 = Stop Selected*/
    case XCP_SS_SYNCH_MODE_STOP_SELECTED:
      /* Loop through all DAQ Lists until the last DAQ List or until all
       * selected DAQ Lists are covered */
      for (DaqListNumber = 0U;
         ((DaqListNumber < XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(Xcp_DaqIdType))
          && (0U != XCP_GET_NO_OF_DAQLISTS_SELECTED()));
         ++DaqListNumber)
      {
        /* Retrieve DAQ List parameter */
        DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListNumber);
        /* If the DAQ List is selected */
        if (XCP_IS_DAQ_LIST_SELECTED(DaqListPtr) == TRUE)
        {
          /* Check whether DAQ List is already running */
          if (XCP_IS_DAQ_LIST_RUNNING(DaqListPtr) == TRUE)
          {
            /* Stop the DAQ List */
            XCP_SET_DAQLIST_STOPPED(DaqListPtr);
            /* Update the number of DAQ Lists running */
            XCP_GET_NO_OF_DAQLISTS_RUNNING()--;
          }
          /* Update number of DAQ Lists selected as the selected DAQ list is stopped */
          XCP_GET_NO_OF_DAQLISTS_SELECTED()--;
          /* Clear selected status of the DAQ List */
          XCP_CLEAR_DAQLIST_SELECTED(DaqListPtr);
        }
      }
      /* If all DAQ lists got stopped */
      if (XCP_GET_NO_OF_DAQLISTS_RUNNING() == 0U)
      {
        /* Clear Xcp DAQ running status */
        XCP_CLEAR_XCP_STATUS_DAQ_RUNNING();
      }

      /* Remove stopped DTOs from DTO Queue */
      Xcp_ClearStoppedDtos();

      /* Prepare CTO with positive response */
      ResponseCTOPtr->CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
      ResponseCTOPtr->Length = XCP_CMD_SS_SYNCH_RES_LENGTH;
    break;
    /* Undefined mode parameter */
    default:
      /* Prepare error CTO with negative response ERR_MODE_NOT_VALID */
      ResponseCTOPtr->CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTOPtr->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_MODE_NOT_VALID;
      ResponseCTOPtr->Length = XCP_ERR_MODE_NOT_VALID_LENGTH;
    break;
  }

  DBG_XCP_PROCESSSTARTSTOPSYNCH_EXIT(ResponseCTOPtr);
}

/*-------------------[Xcp_ProcessCmdGetDAQProcessorInfo]---------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetDAQProcessorInfo(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETDAQPROCESSORINFO_ENTRY();

  /* If both DAQ and STIM resources are protected */
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* Daq is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Prepare CTO with positive response */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
    ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_DAQ_PROPERTIES;
    Xcp_WriteWordToByteArray(
      &(ResponseCTO.CTO[XCP_RES_BYTE2_INDEX]),
      XCP_MAX_DAQ);
    Xcp_WriteWordToByteArray(
      &(ResponseCTO.CTO[XCP_RES_BYTE4_INDEX]),
      XCP_MAX_EVENT_CHANNEL);
    ResponseCTO.CTO[XCP_RES_BYTE6_INDEX] = XCP_MIN_DAQ;
    ResponseCTO.CTO[XCP_RES_BYTE7_INDEX] =  XCP_DAQ_KEY_BYTE;
    ResponseCTO.Length = XCP_CMD_GET_DAQPI_RES_LENGTH;
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETDAQPROCESSORINFO_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*----------------------[Xcp_ProcessCmdGetDAQListMode]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetDAQListMode(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETDAQLISTMODE_ENTRY();

  /* Verify protection status for both DAQ and STIM*/
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* BOTH Daq and STIM is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Retrieve requested DAQ List number */
    const uint16 DaqListNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);

    /* If DAQ List number is invalid (not yet dynamically allocated) */
    if ( DaqListNumber >= XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(uint16) )
    {
      /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
      ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
    }

    else /* DAQ List number is valid */
    {
      /* Local variable to hold address of the requested DAQ List's parameters */
      P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListNumber);

      /* If the DAQ list was not fully configured via a prior SET_DAQ_LIST_MODE command */
      if ( XCP_IS_DAQLIST_FULLY_CONFIGURED(DaqListPtr) == FALSE )
      {
        /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
        ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
      }
              /* Verify DAQ protection status */
      else if (((XCP_MASK_DAQLIST_DIRECTION_DAQ ==
                (DaqListPtr->Mode & XCP_MASK_DAQLIST_DIRECTION)) &&
               ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
                   (uint8)XCP_RESOURCE_DAQ_MASK)
              )||
              /* Verify STIM protection status */
              ((XCP_MASK_DAQLIST_DIRECTION_STIM ==
                (DaqListPtr->Mode & XCP_MASK_DAQLIST_DIRECTION)) &&
               ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
                   (uint8)XCP_RESOURCE_STIM_MASK)
              )
             )
      {
        /* Either Daq or STIM is protected return ERR_ACCESS_LOCKED */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
        ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
      }
      else
      {
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_GET_DAQLIST_MODE(DaqListPtr);
        /* "Selected" flag is conflicting with the "alternating" flag as the "selected" is required
         * to be the bit 0 according to GET_DAQ_LIST_MODE command, while the "alternating" flag is
         * required to be also the bit 0 according to the SET_DAQ_LIST_MODE command. Therefore,
         * we put the "selected" flag to bit 2 position */
        if ((DaqListPtr->Mode & XCP_MASK_DAQLIST_SELECTED) != 0U)
        {
          /* Set the selected bit at the GET_DAQ_LIST_MODE required position*/
          ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] |= XCP_MASK_DAQLIST_SELECTED_GDLM;
          /* Clear the selected bit which was set when the entire mode was copied */
          ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] &= (uint8)(~XCP_MASK_DAQLIST_SELECTED);
        }
        else
        {
          /* Clear the selected bit from the GET_DAQ_LIST_MODE required position */
          ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] &= (uint8)(~XCP_MASK_DAQLIST_SELECTED_GDLM);
        }
        ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_RESERVED_BYTE;
        ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = XCP_RESERVED_BYTE;
        Xcp_WriteWordToByteArray(
          &(ResponseCTO.CTO[XCP_RES_BYTE4_INDEX]),
          XCP_GET_DAQLIST_EVENT_CHANNEL(DaqListPtr));
        ResponseCTO.CTO[XCP_RES_BYTE6_INDEX] = XCP_GET_DAQLIST_PRESCALER(DaqListPtr);
        ResponseCTO.CTO[XCP_RES_BYTE7_INDEX] = XCP_GET_DAQLIST_PRIORITY(DaqListPtr);
        ResponseCTO.Length = XCP_CMD_GET_DAQLM_RES_LENGTH;
      }
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETDAQLISTMODE_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*-------------------[Xcp_ProcessCmdGetDAQResolutionInfo]--------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetDAQResolutionInfo(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETDAQRESOLUTIONINFO_ENTRY();

  /* If both DAQ and STIM resources are protected */
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* Both Daq and STIM is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Prepare CTO with positive response */
    ResponseCTO.CTO[XCP_RES_PID_INDEX]   = XCP_RES_PID;
    ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_GRANULARITY_ODT_ENTRY_SIZE_DAQ;
    ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_MAX_ODT_ENTRY_SIZE_DAQ;
    ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = XCP_GRANULARITY_ODT_ENTRY_SIZE_STIM;
    ResponseCTO.CTO[XCP_RES_BYTE4_INDEX] = XCP_MAX_ODT_ENTRY_SIZE_STIM;
    ResponseCTO.CTO[XCP_RES_BYTE5_INDEX] = XCP_TIMESTAMP_MODE;
    Xcp_WriteWordToByteArray(
      &(ResponseCTO.CTO[XCP_RES_BYTE6_INDEX]),
      XCP_TIMESTAMP_TICKS);
    ResponseCTO.Length = XCP_CMD_GET_DAQRI_RES_LENGTH;
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETDAQRESOLUTIONINFO_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdGetDAQEventInfo]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetDAQEventInfo(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETDAQEVENTINFO_ENTRY();

  /* If both DAQ and STIM resources are protected */
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* Both Daq and STIM is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Retrieve requested Event Channel number */
    const uint16 EventChannelNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);

    /* Check whether event channel is valid */
    if (XCP_MAX_EVENT_CHANNEL <= EventChannelNumber)
    {
      /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
      ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
    }
    else
    {
      /* Local variable to hold address of event configuration information */
      const Xcp_EventInfoType *EventCfgPtr = &Xcp_EventInfo[EventChannelNumber];
      /* Prepare CTO with positive response */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
      ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_GET_EVENT_PROPERTIES(EventCfgPtr);
      ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_GET_EVENT_MAX_DAQ(EventCfgPtr);
      ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = EventCfgPtr->EventNameLength;
      ResponseCTO.CTO[XCP_RES_BYTE4_INDEX] = EventCfgPtr->TimeCycle;
      ResponseCTO.CTO[XCP_RES_BYTE5_INDEX] = EventCfgPtr->TimeUnit;
      ResponseCTO.CTO[XCP_RES_BYTE6_INDEX] = XCP_GET_EVENT_PRIORITY(EventCfgPtr);
      ResponseCTO.Length = XCP_CMD_GET_DAQEI_RES_LENGTH;

      /* Set MTA */
      Xcp_MTA.Type         = XCP_MTATYPE_ADDRESS;
      /* Set the address location from which the name to be uploaded */
      Xcp_MTA.Address.Read = EventCfgPtr->EventName;
      /* Mark the MTA as being set */
      Xcp_MTA.IsSet        = TRUE;
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETDAQEVENTINFO_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*----------------------[Xcp_ProcessCmdGetDAQListInfo]-----------------------*/
/* This command is available only for static DAQ lists */
#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK)

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetDAQListInfo(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETDAQLISTINFO_ENTRY();

  /* Verify protection status for both DAQ and STIM*/
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* BOTH Daq and STIM is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Retrieve requested DAQ List number */
    const uint16 DaqListNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);

    /* If DAQ List number is invalid */
    if (DaqListNumber >= XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(uint16))
    {
      /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
      ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
    }
    else /* DAQ List number is valid */
    {
      /* Local variable to hold address of the requested DAQ List's parameters */
      P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListNumber);

            /* Verify DAQ protection status */
      if (((XCP_MASK_DAQLIST_DIRECTION_DAQ ==
            (DaqListPtr->Mode & XCP_MASK_DAQLIST_DIRECTION)) &&
           ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
               (uint8)XCP_RESOURCE_DAQ_MASK)
          )||
          /* Verify STIM protection status */
          ((XCP_MASK_DAQLIST_DIRECTION_STIM ==
            (DaqListPtr->Mode & XCP_MASK_DAQLIST_DIRECTION)) &&
           ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
               (uint8)XCP_RESOURCE_STIM_MASK)
          )
         )
      {
        /* Either Daq or STIM is protected return ERR_ACCESS_LOCKED */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
        ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
      }
      else
      {
        /* Local variable to hold address of the requested DAQ List */
        const P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DAQListConfigPtr = &Xcp_DaqListsDefault.Xcp_Daq[DaqListNumber];

        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_GET_DAQLIST_PROPERTIES(DAQListConfigPtr);
        ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_GET_DAQLIST_MAX_ODT(DAQListConfigPtr);
        ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = XCP_GET_DAQLIST_MAX_ODT_ENTRIES(DAQListConfigPtr);
        Xcp_WriteWordToByteArray(
            &(ResponseCTO.CTO[XCP_RES_BYTE4_INDEX]),
          XCP_GET_DAQLIST_FIXED_EVENT_CHANNEL(DAQListConfigPtr));
        ResponseCTO.Length = XCP_CMD_GET_DAQLI_RES_LENGTH;
      }
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETDAQLISTINFO_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdClearDAQList]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdClearDAQList(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;
  /* Local variable to hold Event Channel number */
  uint16 EventChannel;
  /* Local variable to hold Daq Id Index */
  uint16 Daqindex;

  DBG_XCP_PROCESSCMDCLEARDAQLIST_ENTRY();

  /* Verify protection status for both DAQ and STIM*/
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* BOTH Daq and STIM is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Retrieve requested DAQ List number. */
    const uint16 DaqListNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);

    /* If DAQ List number is invalid */
    if (DaqListNumber >= XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(uint16))
    {
      /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
      ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
    }
    else /* DAQ List number valid */
    {
      /* Local variable to hold address of the requested DAQ List's parameters */
      P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListNumber);

         /* Verify DAQ protection status */
      if (((XCP_MASK_DAQLIST_DIRECTION_DAQ ==
              (DaqListPtr->Mode & XCP_MASK_DAQLIST_DIRECTION)) &&
             ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
                 (uint8)XCP_RESOURCE_DAQ_MASK)
            )||
            /* Verify STIM protection status */
            ((XCP_MASK_DAQLIST_DIRECTION_STIM ==
              (DaqListPtr->Mode & XCP_MASK_DAQLIST_DIRECTION)) &&
             ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
                 (uint8)XCP_RESOURCE_STIM_MASK)
            )
           )
      {
        /* Either Daq or STIM is protected return ERR_ACCESS_LOCKED */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
        ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
      }
      else
      {
#if (XCP_MIN_DAQ != 0)
        /* Check whether the DAQ List is predefined and configurable */
        if (XCP_MIN_DAQ <= DaqListNumber)
#endif
        {
          /* Clear all ODT Entries associated with every ODT in the DAQ List */
          Xcp_ClearODTs(DaqListPtr->OdtList, DaqListPtr->MaxOdt);
        }

        /* Reset DAQ List states and running Data.
           The cast is OK as, if Xcp_DaqIdType is an 8 bit type and the DAQ ID is
           a 16 bit value, the previous check ( DaqListNumber < XCP_MAX_DAQ )
           ensured that the DAQ list ID is no bigger than 255 at this point. */
        Xcp_ResetDaqListData( (Xcp_DaqIdType)DaqListNumber );

        /* If any event is associated with the DAQList, this needs to be
         * removed */
        for (EventChannel = 0U; EventChannel < XCP_MAX_EVENT_CHANNEL; ++EventChannel)
        {
          for (Daqindex = 0U; Daqindex < Xcp_Event[EventChannel].DaqIdListCount; ++Daqindex)
          {
            if (DaqListNumber == Xcp_Event[EventChannel].DaqIdList[Daqindex])
            {
              /* Disassociate DAQ list from present event channel.
                 The cast is OK as, if Xcp_DaqIdType is an 8 bit type and the DAQ ID is
                 a 16 bit value, the previous check ( DaqListNumber < XCP_MAX_DAQ )
                 ensured that the DAQ list ID is no bigger than 255 at this point. */
              Xcp_RemoveDAQListFromEventChannel( (Xcp_DaqIdType)DaqListNumber, EventChannel );
            }
          }
        }

#if (XCP_MIN_DAQ != 0U)
        /* Initialize Preconfigured DaqList with configured values */
        if (DaqListNumber < XCP_MIN_DAQ)
        {
          /* For PREDEFINED DAQ lists, reset the associated events */
          for (EventChannel = 0U; EventChannel < XCP_MAX_EVENT_CHANNEL; ++EventChannel)
          {
            for (Daqindex = 0U; Daqindex < Xcp_EventInfo[EventChannel].DaqIdListCount; ++Daqindex)
            {
              if (DaqListNumber == Xcp_EventInfo[EventChannel].DaqIdList[Daqindex])
              {
                /* Associate DAQ list to present event channel.
                   The DaqListNumber variable is casted to Xcp_DaqIdType type
                   a 16 bit value, the previous check ( DaqListNumber < XCP_MAX_DAQ )
                   ensured that the DAQ list ID is no bigger than 255 at this point. */
                Xcp_AssociateDAQListToEventChannel( (Xcp_DaqIdType)DaqListNumber, EventChannel );
              }
            }
          }
        }
#endif
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO.Length = XCP_CMD_CLEAR_DAQ_RES_LENGTH;
      }
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDCLEARDAQLIST_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

#endif /*(XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK)*/

/*----------------------[Xcp_ProcessCmdSetDAQListMode]----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdSetDAQListMode(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDSETDAQLISTMODE_ENTRY();

  /* Verify protection status for both DAQ and STIM */
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* Both Daq and STIM is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
#if (XCP_PRESCALER_SUPPORTED == XCP_PRESCALER_MASK)
    /* Local variable to hold requested Prescaler */
    uint8 Prescaler;
#endif
    /* Local variable to hold requested Mode */
    uint8 Mode;

    /* Retrieve requested DAQ List number.
       This DaqListNumber is a variable holding the DAQ list number from the command
       sent by the Master.
       The DAQ list number validity is checked below. */
    const uint16 DaqListNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);

    /* Retrieve requested event channel number */
    const uint16 EventChannelNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE4_INDEX);

#if (XCP_PRESCALER_SUPPORTED == XCP_PRESCALER_MASK)
    /* Retrieve requested Prescaler */
    Prescaler = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE6_INDEX);
#endif
    /* Retrieve requested Mode */
    Mode = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);

    /* Verify command parameters, Verify whether DAQ List number, event
     * channel number and prescaler are valid */
    if (((DaqListNumber >= XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(uint16))
       ||
       (EventChannelNumber >= XCP_MAX_EVENT_CHANNEL))
       /* If prescaler is supported */
#if (XCP_PRESCALER_SUPPORTED == XCP_PRESCALER_MASK)
       ||
       ((XCP_MASK_DAQLIST_DIRECTION_DAQ == (Mode & XCP_MASK_DAQLIST_DIRECTION)) &&
        (XCP_PRESCALER_MIN > Prescaler)
       )
#endif
      )
    {
      /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
      ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
    }
            /* Verify DAQ protection status */
    else if (((XCP_MASK_DAQLIST_DIRECTION_DAQ == (Mode & XCP_MASK_DAQLIST_DIRECTION)) &&
             ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
                 (uint8)XCP_RESOURCE_DAQ_MASK)
            )||
            /* Verify STIM protection status */
            ((XCP_MASK_DAQLIST_DIRECTION_STIM == (Mode & XCP_MASK_DAQLIST_DIRECTION)) &&
             ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
                 (uint8)XCP_RESOURCE_STIM_MASK)
            )
           )
    {
      /* Either Daq or STIM is protected return ERR_ACCESS_LOCKED */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
      ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
    }
    /* If alternating mode and timestamped mode are requested together or if
     * timestamped mode is requested and the requested event has ODT
     * consistency or if alternating mode is requested along with direction
     * STIM */
    else if ((((XCP_MASK_DAQLIST_ALTERNATING_MODE ==
               (Mode & XCP_MASK_DAQLIST_ALTERNATING_MODE)
              ) ||
              (XCP_EVENT_CHANNEL_CONSISTENCY_ODT ==
               XCP_GET_EVENT_CHANNEL_CONSISTENCY(EventChannelNumber)
              )
             ) &&
             (XCP_MASK_DAQLIST_TIMESTAMPED_MODE ==
              (Mode & XCP_MASK_DAQLIST_TIMESTAMPED_MODE)
             )
            ) ||
            ((XCP_MASK_DAQLIST_ALTERNATING_MODE ==
              (Mode & XCP_MASK_DAQLIST_ALTERNATING_MODE)) &&
              (XCP_MASK_DAQLIST_DIRECTION_STIM ==
              (Mode & XCP_MASK_DAQLIST_DIRECTION))
            )
           )
    {
      /* Prepare error CTO with negative response ERR_MODE_NOT_VALID */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_MODE_NOT_VALID;
      ResponseCTO.Length = XCP_ERR_MODE_NOT_VALID_LENGTH;
    }
    /* If timestamp can't be switched off */
#if (XCP_TIMESTAMP_FIXED == XCP_TIMESTAMP_FIXED_MASK)
    /* If master requested to switched off timestamp*/
    else if ( 0U == (Mode & XCP_MASK_DAQLIST_TIMESTAMPED_MODE))
    {
      /* Prepare error CTO with negative response ERR_CMD_SYNTAX */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_SYNTAX;
      ResponseCTO.Length = XCP_ERR_CMD_SYNTAX_LENGTH;
    }
#endif
    /* If PID is not absolute ODT number */
#if (XCP_IDENTIFICATION_FIELD_TYPE != XCP_ABSOLUTE_IF_MASK)
    /* If master requested to switch off PID */
    else if (XCP_MASK_DAQLIST_PID_OFF_MODE == (Mode & XCP_MASK_DAQLIST_PID_OFF_MODE))
    {
      /* Prepare error CTO with negative response ERR_CMD_SYNTAX */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_SYNTAX;
      ResponseCTO.Length = XCP_ERR_CMD_SYNTAX_LENGTH;
    }
#endif
#if (XCP_RESOURCE_STIM != XCP_RESOURCE_STIM_MASK)
    /* If the requested direction is STIM but STIM direction is not supported */
    else if ((Mode & XCP_MASK_DAQLIST_DIRECTION) == XCP_MASK_DAQLIST_DIRECTION_STIM )
    {
      /* Prepare error CTO with negative response ERR_CMD_SYNTAX */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_SYNTAX;
      ResponseCTO.Length = XCP_ERR_CMD_SYNTAX_LENGTH;
    }
#endif
    else
    {
      /* Process SET_DAQ_LIST_MODE command request */
      Xcp_SubSetDAQListMode(&ResponseCTO);
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDSETDAQLISTMODE_EXIT(XCP_E_OK);
  return XCP_E_OK;
}
/*----------------------[Xcp_SubSetDAQListMode]----------------------*/
STATIC FUNC(void, XCP_CODE) Xcp_SubSetDAQListMode
(
  P2VAR(Xcp_CTOType,AUTOMATIC,AUTOMATIC) ResponseCTOPtr
)
{
  /* Retrieve requested DAQ List number */
  const uint16 DaqListNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);
  /* Retrieve requested Event channel number */
  const uint16 EventChannelNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE4_INDEX);
  /* Local variable to hold address of the requested DAQ List's parameters */
  P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr;
  /* Local variable to hold requested Mode */
  uint8 Mode;
  /* Local variable to hold address of event configuration information */
  const Xcp_EventInfoType *EventCfgPtr = &Xcp_EventInfo[EventChannelNumber];

#if (XCP_MIN_DAQ != 0U)
  P2CONST(Xcp_DaqType, AUTOMATIC, XCP_CONST) DaqListCfgPtr = NULL_PTR;
  DBG_XCP_SUBSETDAQLISTMODE_ENTRY(ResponseCTOPtr);
  if (DaqListNumber < XCP_MIN_DAQ)
  {
    /* Local variable to hold address of DAQ configuration information */
    DaqListCfgPtr= &(Xcp_DaqListsDefault.Xcp_Daq[DaqListNumber]);
  }
#else
  DBG_XCP_SUBSETDAQLISTMODE_ENTRY(ResponseCTOPtr);
#endif


  /* Get the required DAQ list */
  DaqListPtr = XCP_GET_DAQ_LIST_PTR((Xcp_DaqIdType)DaqListNumber);

  Mode = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);

  /* If requested event channel or associated event channel to the
   * requested DAQ List is in queue. */
  if ((Xcp_Event[EventChannelNumber].EventIsSet == TRUE) ||
      ( (DaqListPtr->EventId != XCP_INVALID_EVENT_CHANNEL) &&
        (Xcp_Event[DaqListPtr->EventId].EventIsSet == TRUE)\
      )
    )
  {
    /* Prepare error CTO with negative response ERR_DAQ_ACTIVE */
    ResponseCTOPtr->CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTOPtr->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_DAQ_ACTIVE;
    ResponseCTOPtr->Length = XCP_ERR_DAQ_ACTIVE_LENGTH;
  }
  /* If the event channel is not available or DAQ list is not already
   * associated with the event channel and the specified event channel
   * already has the maximum number of DAQ list associated with it or if
   * the specified DAQ List direction is not supported by the specified
   * Event channel. */
  else if (
    /* If the event channel is not available to be associated with any DAQ
     * List */
      (FALSE == XCP_IS_EVENT_CHANNEL_AVAILABLE(EventCfgPtr))||
     /* If the request DAQ list is not already associated with the event
      * channel and the requested event channel already has the maximum
      * number of DAQ list  */
     (((EventChannelNumber != XCP_GET_DAQLIST_EVENT_CHANNEL(DaqListPtr)) &&
      (XCP_GET_EVENT_MAX_DAQ(EventCfgPtr) == XCP_GET_EVENT_NO_OF_DAQLIST(EventChannelNumber))
     ) ||
     /* If the requested DAQ List is of direction STIM and the requested
      * Event channel doesn't support DAQ Lists with direction STIM  */
     (((XCP_MASK_DAQLIST_DIRECTION_STIM == (Mode & XCP_MASK_DAQLIST_DIRECTION)) &&
       (FALSE == XCP_IS_EVENT_DIRECTION(EventCfgPtr,XCP_MASK_EVENT_DIRECTION_STIM))
     )
     ||
     /* If the requested DAQ List is of direction DAQ and the requested
      * Event channel doesn't support DAQ Lists with direction DAQ  */
     ((XCP_MASK_DAQLIST_DIRECTION_DAQ == (Mode & XCP_MASK_DAQLIST_DIRECTION)) &&
      (FALSE == XCP_IS_EVENT_DIRECTION(EventCfgPtr,XCP_MASK_EVENT_DIRECTION_DAQ))
     )))
#if (XCP_MIN_DAQ != 0U)
     /* If the requested DAQ List is predefined we must not allow the configured direction to be
      * changed if it is not DAQ_STIM */
     ||
     (
       /* Check whether the DAQ List is predefined */
       (DaqListNumber < XCP_MIN_DAQ) &&
      /* If the requested DAQ List is of direction STIM and the DAQ is
       * configured not to support DAQ Lists with direction STIM  */
      ((((Mode & XCP_MASK_DAQLIST_DIRECTION) == XCP_MASK_DAQLIST_DIRECTION_STIM) &&
        (XCP_IS_DAQ_DIRECTION(DaqListNumber,XCP_MASK_DAQ_DIRECTION_STIM) == FALSE)
      )
      ||
      /* If the requested DAQ List is of direction DAQ and the DAQ is
       * configured not to support DAQ Lists with direction DAQ  */
      (((Mode & XCP_MASK_DAQLIST_DIRECTION) == XCP_MASK_DAQLIST_DIRECTION_DAQ) &&
       (XCP_IS_DAQ_DIRECTION(DaqListNumber,XCP_MASK_DAQ_DIRECTION_DAQ) == FALSE)
      ))
     )
#endif
    /* Deviation MISRA-2 */
    )
  {
    /* Prepare error CTO with negative response ERR_CMD_SYNTAX */
    ResponseCTOPtr->CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTOPtr->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_SYNTAX;
    ResponseCTOPtr->Length = XCP_ERR_CMD_SYNTAX_LENGTH;
  }
#if (XCP_MIN_DAQ != 0U)
  /* If event channel is fixed and requested event channel is not the
   * configured fixed event channel  */
  else if ( (DaqListNumber < XCP_MIN_DAQ) &&
            ((XCP_IS_DAQLIST_EVENT_CHANNEL_FIXED(DaqListCfgPtr) == TRUE ) &&
             (XCP_GET_DAQLIST_FIXED_EVENT_CHANNEL(DaqListCfgPtr) != EventChannelNumber)) )
  {
    /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
    ResponseCTOPtr->CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTOPtr->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
    ResponseCTOPtr->Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
  }
#endif
  /* Check that the intention is to configure a dynamically configurable DAQ list with the direction
   * STIM and if this is allowed according to the ODT PID limitation for the STIM case
   * (the PID shouldn't be higher than 0xBF)
   * We only perform the check if the configuration is DYNAMIC (as the check for STATIC is performed
   * at configuration time) and the DAQ list will then have an ODT with the PID greater than 0xBF*/
#if ((XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK) && (XCP_MAX_ODT > XCP_MAX_ODT_STIM))
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK)
  /* If direction is STIM and the number of configured ODTs for the current DAQ list plus the number
   * of allowed ODTs for previous DAQ lists is greater than the maximum allowed */
  else if (((Mode & XCP_MASK_DAQLIST_DIRECTION) == XCP_MASK_DAQLIST_DIRECTION_STIM) &&
           ((DaqListPtr->FirstPID + DaqListPtr->MaxOdt)
                 > XCP_MAX_ODT_STIM))
#else /* XCP_IDENTIFICATION_FIELD_TYPE != XCP_ABSOLUTE_IF_MASK */
  /* If direction is STIM and the number of configured ODTs for the current DAQ list is greater than
   * the maximum allowed  */
  else if (((Mode & XCP_MASK_DAQLIST_DIRECTION) == XCP_MASK_DAQLIST_DIRECTION_STIM) &&
           (DaqListPtr->MaxOdt > XCP_MAX_ODT_STIM))
#endif /* XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK */
  {
    /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
    ResponseCTOPtr->CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTOPtr->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
    ResponseCTOPtr->Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
  }
#endif /*(XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK) && (XCP_MAX_ODT > XCP_MAX_ODT_STIM) */
  else
  {
    /* Process the set DAQ list mode command request */
    Xcp_ProcessSetDAQListMode();
    /* Prepare CTO with positive response */
    ResponseCTOPtr->CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
    ResponseCTOPtr->Length = XCP_CMD_SET_DAQLM_RES_LENGTH;
  }

  DBG_XCP_SUBSETDAQLISTMODE_EXIT(ResponseCTOPtr);
}
/*----------------------[Xcp_ProcessSetDAQListMode]----------------------*/
STATIC FUNC(void, XCP_CODE) Xcp_ProcessSetDAQListMode
(
  void
)
{
  /* Retrieve requested DAQ List number */
  const uint16 DaqListNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);
  /* Retrieve requested Event channel number */
  const uint16 EventChannelNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE4_INDEX);
  /* Local variable to hold address of the requested DAQ List's
   * parameters */
  P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr;
  /* Local variable to hold requested Prescaler */
  uint8 Prescaler;
  /* Local variable to hold requested Mode */
  uint8 Mode;
  /* Local variable to hold requested Mode */
  uint8 Priority;

  DBG_XCP_PROCESSSETDAQLISTMODE_ENTRY();

  /* Get the requested DAQ list */
  DaqListPtr = XCP_GET_DAQ_LIST_PTR((Xcp_DaqIdType)DaqListNumber);
  /* Retrieve requested Prescaler */
  Prescaler = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE6_INDEX);
  /* Retrieve requested Mode */
  Mode = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);
  /* Retrieve requested Priority */
  Priority = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE7_INDEX);
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
  /* Set PID_OFF mode */
  XCP_SET_DAQLIST_PIDOFF_MODE(DaqListPtr, (uint8)(Mode & XCP_MASK_DAQLIST_PID_OFF_MODE));
  /* Set Timestamp mode  */
  XCP_SET_DAQLIST_TIMESTAMPED_MODE(DaqListPtr, (uint8)(Mode & XCP_MASK_DAQLIST_TIMESTAMPED_MODE));
  /* Set DAQ List direction  */
  XCP_SET_DAQLIST_DIRECTION(DaqListPtr, (uint8)(Mode & XCP_MASK_DAQLIST_DIRECTION));
  /* Set DAQ List alternating mode status  */
  XCP_SET_DAQLIST_ALTERNATING_MODE(DaqListPtr, (uint8)(Mode & XCP_MASK_DAQLIST_ALTERNATING_MODE));
  /* Set DAQ List Prescaler */
  XCP_SET_DAQLIST_PRESCALER(DaqListPtr, Prescaler);
  /* Set DAQ List Prescaler count */
  XCP_SET_DAQLIST_PRESCALERCNT(DaqListPtr, Prescaler);
  /* Set DAQ List priority */
  XCP_SET_DAQLIST_PRIORITY(DaqListPtr, Priority);

  if (XCP_INVALID_EVENT_CHANNEL != DaqListPtr->EventId)
  {
    /* Disassociate DAQ list from present event channel.
       The cast is OK as, if Xcp_DaqIdType is an 8 bit type and the DAQ ID is
       a 16 bit value, the previous check in Xcp_ProcessCmdSetDAQListMode() function
       ( DaqListNumber < XCP_MAX_DAQ ) ensured that the DAQ list ID is no bigger
       than 255 at this point. */
    Xcp_RemoveDAQListFromEventChannel( (Xcp_DaqIdType)DaqListNumber, DaqListPtr->EventId );
  }
  /* Associate DAQ List to the specified event channel */
  XCP_SET_DAQLIST_EVENT_CHANNEL(DaqListPtr, EventChannelNumber);
  /* Sort DAQ Lists associated with the event channel
     This cast is safe because for the configuration with Identification Field Type
     Absolute ODT Number there are maximum 255 DAQ Lists only. */
  Xcp_AssociateDAQListToEventChannel( (Xcp_DaqIdType)DaqListNumber, EventChannelNumber );
  /* Set DAQ List as configured */
  XCP_SET_DAQLIST_CONFIGURED(DaqListPtr);

  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  DBG_XCP_PROCESSSETDAQLISTMODE_EXIT();
}
#endif /*((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) ||
        * (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)) */

/*-------------------------[Xcp_ProcessCmdSetMTA]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdSetMTA(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDSETMTA_ENTRY();

  /* If address granularity is 4 bytes */
  Xcp_MTA.Type   = XCP_MTATYPE_ADDRESS;
  TS_MemCpy(
    &Xcp_MTA.Address.Write,
    &(Xcp_CommandQueue.Queue[Xcp_CommandQueue.Head].CTO[4U]),
    XCP_MTA_ADDRESS_LENGTH);

  /* Mark the MTA as being set */
  Xcp_MTA.IsSet = TRUE;

  /* Prepare CTO with positive response */
  ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
  ResponseCTO.Length                 = XCP_CMD_SET_MTA_RES_LENGTH;

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDSETMTA_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*-------------------------[Xcp_ProcessCmdShortUpload]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdShortUpload(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;
  /* Local variable to hold requested number of data bytes */
  const uint16 NoOfDataBytes
    = (uint16)( XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX) ) * XCP_ADDRESS_GRANULARITY_SIZE;

  DBG_XCP_PROCESSCMDSHORTUPLOAD_ENTRY();

  /* Verify whether requested length is valid */
  if (NoOfDataBytes > (XCP_MAX_CTO - XCP_ADDRESS_GRANULARITY_SIZE))
  {
    /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
    ResponseCTO.CTO[XCP_RES_PID_INDEX]     = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
    ResponseCTO.Length                     = XCP_ERR_OUT_OF_RANGE_LENGTH;
  }
  else
  {
    Xcp_MTA.Type   = XCP_MTATYPE_ADDRESS;
    TS_MemCpy(
      &Xcp_MTA.Address.Write,
      &(Xcp_CommandQueue.Queue[Xcp_CommandQueue.Head].CTO[4U]),
      XCP_MTA_ADDRESS_LENGTH);

    Xcp_MTA.IsSet = TRUE;

    /* Prepare CTO with positive response */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
    TS_MemCpy(
      &(ResponseCTO.CTO[XCP_ADDRESS_GRANULARITY_SIZE]),
      XCP_APPL_GET_ADDRESS( 0U, (P2VAR(void, AUTOMATIC, XCP_APPL_DATA))Xcp_MTA.Address.Write ),
      NoOfDataBytes);
    /* Safe to set the cast to uint8 here as NoOfDataBytes + XCP_ADDRESS_GRANULARITY_SIZE is lower
     * than XCP_MAX_CTO, which can be maximum 255 */
    ResponseCTO.Length = (uint8)(NoOfDataBytes + XCP_ADDRESS_GRANULARITY_SIZE);

    /* Reset MTA */
    Xcp_MTA.Address.Write = &Xcp_MTA.Address.Write[NoOfDataBytes];
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDSHORTUPLOAD_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*-------------------------[Xcp_ProcessCmdUpload]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdUpload(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;
  /* Local variable to hold return value */
  Xcp_ErrorType RetVal = XCP_E_OK;
  /* Flag signaling whether this command execution must output a response to the master */
  boolean ProvideResponse = FALSE;
  /* Variable holding the number of bytes this command execution should upload */
  uint8 NoOfDataBytesToUploadNow = 0U;

  DBG_XCP_PROCESSCMDUPLOAD_ENTRY();

  /* Perform initial checks and assignments */
  if (Xcp_CmdProcessorState == XCP_CMDPROCESSOR_IDLE)
  {
    /* Local variable to hold requested number of data bytes to be uploaded*/
    const uint16 NoOfDataBytesRequested
      = (uint16)( XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX) ) * XCP_ADDRESS_GRANULARITY_SIZE;


    /* Check whether MTA indicates address */
    if (( Xcp_MTA.Type == XCP_MTATYPE_COUNTER) ||
        ( Xcp_MTA.IsSet == FALSE))
    {
      /* Prepare error CTO with negative response XCP_ERR_ACCESS_DENIED */
      ResponseCTO.CTO[XCP_RES_PID_INDEX]     = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_DENIED;
      ResponseCTO.Length                     = XCP_ERR_ACCESS_DENIED_LENGTH;
      RetVal = XCP_E_NOT_OK;
      ProvideResponse = TRUE;
    }
#if (XCP_SLAVE_BLOCK_MODE == XCP_SLAVE_BLOCK_MODE_MASK)
    /* Verify whether requested length is valid. */
    else if (NoOfDataBytesRequested == 0U)
#else /* Block mode is not supported */
    /* Verify whether requested length is valid. */
    else if (
              (NoOfDataBytesRequested > (XCP_MAX_CTO - XCP_ADDRESS_GRANULARITY_SIZE)) ||
              (NoOfDataBytesRequested == 0U)
            )
#endif /* XCP_SLAVE_BLOCK_MODE == XCP_SLAVE_BLOCK_MODE_MASK */
    {
      /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
      ResponseCTO.CTO[XCP_RES_PID_INDEX]     = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
      ResponseCTO.Length                     = XCP_ERR_OUT_OF_RANGE_LENGTH;
      RetVal = XCP_E_NOT_OK;
      ProvideResponse = TRUE;
    }
    else
    {
#if (XCP_SLAVE_BLOCK_MODE != XCP_SLAVE_BLOCK_MODE_MASK)
      /* Only one upload will be made. Directly initialize the number of bytes to upload now */
      NoOfDataBytesToUploadNow = (uint8)NoOfDataBytesRequested;
#else
      /* Initialize the remaining data bytes to upload. Safe to cast to uint8 as
       NoOfDataBytesRequested + XCP_ADDRESS_GRANULARITY_SIZE is lower than XCP_MAX_CTO,
       which can be maximum 255 */
      Xcp_RemainingDataBytesToUpload = (uint8)NoOfDataBytesRequested;
#endif /* XCP_SLAVE_BLOCK_MODE != XCP_SLAVE_BLOCK_MODE_MASK */
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* If checks were OK, or there was no need for checking (in the busy case)*/
  if (RetVal == XCP_E_OK)
  {
#if (XCP_SLAVE_BLOCK_MODE == XCP_SLAVE_BLOCK_MODE_MASK)
    /* if the number of remaining data bytes to upload are higher than the maximum allowed data
     * bytes to be uploaded in one message  */
    if (Xcp_RemainingDataBytesToUpload > (uint8)XCP_MAX_UPLOAD_BYTES_ONE_CTO)
    {
      /* Upload maximum bytes allowed */
      NoOfDataBytesToUploadNow = XCP_MAX_UPLOAD_BYTES_ONE_CTO;
    }
    else
    {
      /* Upload the remaining bytes. Safe to cast to uint8. */
      NoOfDataBytesToUploadNow = (uint8)Xcp_RemainingDataBytesToUpload;
    }
#endif /* XCP_SLAVE_BLOCK_MODE != XCP_SLAVE_BLOCK_MODE_MASK */

    /* It might be that the busy command was aborted in the meantime
     * (SYNCH or another command during BUSY state). If so, we exit the command processing */
    if (XCP_IS_CMD_BUSY_ABORTED() == TRUE)
    {
      /* Processing will not continue */
      RetVal = XCP_E_NOT_OK;
      /* Clear the aborted flag */
      Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_BUSY_ABORTED_MASK);
      /* Do not provide a response */
      ProvideResponse = FALSE;
    }
    else
    {
      ProvideResponse = TRUE;
      /* Prepare CTO with positive response */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
      TS_MemCpy(
        &(ResponseCTO.CTO[XCP_ADDRESS_GRANULARITY_SIZE]),
        XCP_APPL_GET_ADDRESS( 0U, (P2VAR(void, AUTOMATIC, XCP_APPL_DATA))Xcp_MTA.Address.Write ),
        NoOfDataBytesToUploadNow);
      ResponseCTO.Length = NoOfDataBytesToUploadNow + XCP_ADDRESS_GRANULARITY_SIZE;

      /* Post-increment MTA */
      Xcp_MTA.Address.Write = &Xcp_MTA.Address.Write[NoOfDataBytesToUploadNow];

#if (XCP_SLAVE_BLOCK_MODE == XCP_SLAVE_BLOCK_MODE_MASK)
      /* update the number of remaining data bytes*/
      Xcp_RemainingDataBytesToUpload -= NoOfDataBytesToUploadNow;

      /* if we still need to upload */
      if (Xcp_RemainingDataBytesToUpload > 0U)
      {
        /* the command has not finished, emit busy state. It will be continued next time the
         * command processing is called */
        RetVal = XCP_E_BUSY;
      }
#endif
    }
  }

  /* If a response is needed to be queued */
  if (ProvideResponse == TRUE)
  {
    /* Enqueue response packet. */
    Xcp_InsertResponseCTO(&ResponseCTO);
  }

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDUPLOAD_EXIT(RetVal);
  return RetVal;
}

/*-------------------------[Xcp_ProcessCmdDownload]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdDownload(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;
    /* Local variable to hold requested number of data bytes */
  const uint8 NoOfDataElementsRequested = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);

  DBG_XCP_PROCESSCMDDOWNLOAD_ENTRY();
  /* Initialize the ResponseCTO length */
  ResponseCTO.Length = 0U;

  /* Used to avoid warning when comparing uint8 with 255
   * (only when XCP_MAX_DOWNLOAD_ELEMENTS is 255 )*/
#if (XCP_MAX_DOWNLOAD_ELEMENTS <  XCP_ABS_MAX_DOWNLOAD_ELEMENTS)
  if ((NoOfDataElementsRequested == 0U) ||
      (NoOfDataElementsRequested > XCP_MAX_DOWNLOAD_ELEMENTS))
#else
  if (NoOfDataElementsRequested == 0U)
#endif
  {
    /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
    ResponseCTO.CTO[XCP_RES_PID_INDEX]     = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
    ResponseCTO.Length                     = XCP_ERR_OUT_OF_RANGE_LENGTH;
  }
  else
  {
    if((XCP_GET_RESOURCE_PROTECTION_STATUS() & XCP_RESOURCE_CAL_PAG_MASK) ==
                   XCP_RESOURCE_CAL_PAG_MASK)
    {
      /* Calibration commands are protected; return ERR_ACCESS_LOCKED */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
      ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
    }
    else
    {
      if (( Xcp_MTA.Type == XCP_MTATYPE_COUNTER) || ( Xcp_MTA.IsSet == FALSE))
      {
        /* Prepare error CTO with negative response XCP_ERR_ACCESS_DENIED */
        ResponseCTO.CTO[XCP_RES_PID_INDEX]     = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_DENIED;
        ResponseCTO.Length                     = XCP_ERR_ACCESS_DENIED_LENGTH;
      }
      else
      {
        /* Set the variable holding the number of elements to download with the requested number found
         * in the command */
        Xcp_MemoryWrite.ElementsLeftToWrite = NoOfDataElementsRequested;
        /* Process the elements for download */
        Xcp_ProcessOneDownload(&ResponseCTO);
      }
    }
  }

  /* Check if we need to send a response  */
  if (ResponseCTO.Length > 0U)
  {
    /* Enter critical section */
    SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

#if (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON)
    /* Download has finished - clear the flag */
    Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_DOWNLOAD_BLOCK_MODE_MASK);
#endif

    /* Queue the response */
    Xcp_InsertResponseCTO(&ResponseCTO);

    /* Exit critical section */
    SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* Reset the globals used by any memory writing routine */
    Xcp_ResetMemoryWriteInfo();
  }


  DBG_XCP_PROCESSCMDDOWNLOAD_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*-------------------------[Xcp_ProcessCmdDownloadNext]-----------------------*/
#if (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON)
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdDownloadNext(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDDOWNLOADNEXT_ENTRY();
  /* Initialize the ResponseCTO length */
  ResponseCTO.Length = 0U;

  /* Check that the number of elements left to download match the expected */
  if (Xcp_MemoryWrite.ElementsLeftToWrite != XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX))
  {
    /* If the number of data elements does not match the expected value, the error code
     * ERR_SEQUENCE will be returned.
     * The negative response will contain the expected number of data elements. */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEQUENCE;
    ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = Xcp_MemoryWrite.ElementsLeftToWrite;
    ResponseCTO.Length = XCP_ERR_WRITE_SEQUENCE_LENGTH;
  }
  else
  {
    /* Process the elements for download */
    Xcp_ProcessOneDownload(&ResponseCTO);
  }

  /* Check if we need to send a response  */
  if (ResponseCTO.Length > 0U)
  {
    /* Enter critical section */
    SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* Download has finished - clear the flag */
    Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_DOWNLOAD_BLOCK_MODE_MASK);

    /* Queue the response */
    Xcp_InsertResponseCTO(&ResponseCTO);

    /* Exit critical section */
    SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* Reset the globals used by any memory writing routine */
    Xcp_ResetMemoryWriteInfo();
  }

  DBG_XCP_PROCESSCMDDOWNLOADNEXT_EXIT(XCP_E_OK);
  return XCP_E_OK;
}
#endif /* XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON */

STATIC FUNC(void, XCP_CODE) Xcp_ProcessOneDownload
(
  P2VAR(Xcp_CTOType, AUTOMATIC, XCP_APPL_DATA)  ResponseCTO
)
{
  /* the number of elements that will be processed */
  uint8 NumberOfElementsToProcessNow;

  DBG_XCP_PROCESSONEDOWNLOAD_ENTRY(ResponseCTO);

  /* calculate number of elements to be processed now */
  if (Xcp_MemoryWrite.ElementsLeftToWrite >= XCP_MAX_DOWNLOAD_ELEMNTS_ONE_CMD)
  {
    /* process the maximum number of elements possible in this step */
    NumberOfElementsToProcessNow = XCP_MAX_DOWNLOAD_ELEMNTS_ONE_CMD;
  }
  else
  {
    /* process the last remaining elements or a single download (no DOWNLOAD_NEXT afterwards)
     * with less then maximum elements that can be processed in one cycle */
    NumberOfElementsToProcessNow = Xcp_MemoryWrite.ElementsLeftToWrite;
  }

  /* copy the elements in the internal buffer */
  TS_MemCpy
  (
    (&Xcp_MemoryWrite.Buffer[Xcp_MemoryWrite.ElementsWrittenSoFar * XCP_ADDRESS_GRANULARITY_SIZE]),
    &(Xcp_CommandQueue.Queue[Xcp_CommandQueue.Head].CTO[XCP_GET_START_DATA_INDEX_FROM_CTO()]),
    ( (uint32)NumberOfElementsToProcessNow * XCP_ADDRESS_GRANULARITY_SIZE )
  );

  /* increment the number of elements processed so far */
  Xcp_MemoryWrite.ElementsWrittenSoFar += (NumberOfElementsToProcessNow * XCP_ADDRESS_GRANULARITY_SIZE);


  /* decrement the number of elements remaining to be processed with the number of elements
     * processed in this step */
  Xcp_MemoryWrite.ElementsLeftToWrite -= NumberOfElementsToProcessNow;

  /* if download has finished */
  if (Xcp_MemoryWrite.ElementsLeftToWrite == 0U )
  {
    /*  we execute the download if the CTO response queue has room to enqueue the response */
    if (XCP_GET_CTOQUEUE_FREESPACE(uint32) >= XCP_DOWNLOAD_POSITIVE_LENGTH)
    {
      /* calculate the number of bytes to copy from the buffer to the intended memory location */
      uint16 NumberOfBytesToDownload = ( (uint16)Xcp_MemoryWrite.ElementsWrittenSoFar *
                                         XCP_ADDRESS_GRANULARITY_SIZE );
      /* reset the counter holding the number of elements downloaded in this download sequence */
      Xcp_MemoryWrite.ElementsWrittenSoFar = 0U;

      SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
      /* copy all the data from the buffer to the intended memory location pointed by the MTA */
      TS_MemCpy(XCP_APPL_GET_ADDRESS( 0U, Xcp_MTA.Address.Write ),
                &(Xcp_MemoryWrite.Buffer[0U]),
                NumberOfBytesToDownload);
      SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

      /* post-increment MTA address by the number of elements processed in this download instance */
      Xcp_MTA.Address.Write = &Xcp_MTA.Address.Write[NumberOfBytesToDownload];


      /* send a positive response, since the download has finished */
      ResponseCTO->CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
      ResponseCTO->Length                 = XCP_DOWNLOAD_POSITIVE_LENGTH;
    }
    else
    {
#if (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON)
      /* Enter critical section */
      SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

      /* Download has finished - clear the block mode flag */
      Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_DOWNLOAD_BLOCK_MODE_MASK);

      /* Exit critical section */
      SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
#endif

      /* Download has finished - reset the monitors and the pending flag because the command
       * did not execute due to not enough free space in the CTO response queue */
      Xcp_ResetMemoryWriteInfo();
    }
  }

  DBG_XCP_PROCESSONEDOWNLOAD_EXIT(ResponseCTO);
}

/*-------------------------[Xcp_ProcessCmdTLSpecific]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdTLSpecific(void)
{
  /* Local variable to hold return value */
  Xcp_ErrorType RetVal = XCP_E_OK;

  DBG_XCP_PROCESSCMDTLSPECIFIC_ENTRY();

#if (XCP_ON_CAN_ENABLED == STD_ON)
  /* Verify whether sub command requested is GET_SLAVE_ID */
  if (XCP_SUBCMD_GET_SLAVE_ID_PID
      == XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX))
  {
    RetVal = Xcp_ProcessSubCmdGetSlaveID();
  }
#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))
  /* Verify whether sub command requested is GET_DAQ_ID */
  else if (XCP_SUBCMD_GET_DAQ_ID_PID
      == XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX))
  {
    RetVal = Xcp_ProcessSubCmdGetDaqID();
  }
#endif /* (XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
          (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK) */
  else
#endif /* XCP_ON_CAN_ENABLED == STD_ON */
  {
    /* The sub command requested is not supported.  Send negative response if
     * XCP is connected. */
    if (XCP_STATE_CONNECTED == Xcp_State)
    {
      /* Local variable to prepare response */
      Xcp_CTOType ResponseCTO;

      /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
      ResponseCTO.CTO[XCP_RES_PID_INDEX]     = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
      ResponseCTO.Length                     = XCP_ERR_OUT_OF_RANGE_LENGTH;

      /* Enter critical section */
      SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

      /* Enqueue response packet */
      Xcp_InsertResponseCTO(&ResponseCTO);

      /* Exit critical section */
      SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
    }
    else
    {
      /* Ignore request silently */
    }
  }


  DBG_XCP_PROCESSCMDTLSPECIFIC_EXIT(RetVal);
  return RetVal;
}

/*----------------------[Xcp_ProcessSubCmdGetSlaveID]-----------------------*/

#if (STD_ON == XCP_ON_CAN_ENABLED)

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessSubCmdGetSlaveID(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;
  /* Local variable to hold command parameter start address */
  uint8 *ParamterPtr = &XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE2_INDEX);
  /* Local variable to hold requested mode */
  uint8 Mode = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE5_INDEX);

  DBG_XCP_PROCESSSUBCMDGETSLAVEID_ENTRY();

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Verify whether command parameter bytes 2,3 and 4 reads "XCP" */
  if ( ((XCP_ASCII_X != ParamterPtr[XCP_GET_SLAVE_ID_OFFSET_X])  ||
        (XCP_ASCII_C != ParamterPtr[XCP_GET_SLAVE_ID_OFFSET_C])) ||
        (XCP_ASCII_P != ParamterPtr[XCP_GET_SLAVE_ID_OFFSET_P])
     )
  {
    /* Prepare error CTO with negative response ERR_CMD_SYNTAX */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_SYNTAX;
    ResponseCTO.Length = XCP_ERR_CMD_SYNTAX_LENGTH;
  }
  /* Verify requested mode parameter */
  else if (XCP_GET_SLAVE_ID_BY_INVERSE_ECHO < Mode)
  {
    /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
    ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
  }
  else
  {
    /* If mode = identify by echo */
    if (XCP_GET_SLAVE_ID_BY_ECHO == Mode)
    {
      /* Prepare CTO with positive response */
      ResponseCTO.CTO[XCP_RES_PID_INDEX]   = XCP_RES_PID;
      ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_ASCII_X;
      ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_ASCII_C;
      ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = XCP_ASCII_P;
      Xcp_WriteDwordToByteArray(
        &(ResponseCTO.CTO[XCP_RES_BYTE4_INDEX]),
        XCP_CMD_STIM_CAN_ID);
      ResponseCTO.Length = XCP_SUBCMD_GET_SLAVE_ID_RES_LENGTH;

      /* Set GET_SLAVE_ID request receive status */
      Xcp_CmdReqStatus |= XCP_RECEIVED_GET_SLAVE_ID_ECHO_MASK;
    }
    /* If mode = identify by inverse echo */
    else
    {
      /* If there was a prior request with mode = identify by echo */
      if (XCP_RECEIVED_GET_SLAVE_ID_ECHO_MASK
          == (Xcp_CmdReqStatus & XCP_RECEIVED_GET_SLAVE_ID_ECHO_MASK))
      {
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_ASCII_INVERSE_X;
        ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_ASCII_INVERSE_C;
        ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = XCP_ASCII_INVERSE_P;
        Xcp_WriteDwordToByteArray(&(ResponseCTO.CTO[XCP_RES_BYTE4_INDEX]),
                                  (uint32)XCP_CMD_STIM_CAN_ID
                                 );
        ResponseCTO.Length = XCP_SUBCMD_GET_SLAVE_ID_RES_LENGTH;

        /* Clear GET_SLAVE_ID request receive status */
        Xcp_CmdReqStatus &= (uint8)(~XCP_RECEIVED_GET_SLAVE_ID_ECHO_MASK);
      }
      else
      {
        /* Ignore request silently */
        ResponseCTO.Length = 0U;
      }
    }
  }
  /* If there is a response to send */
  if (0U != ResponseCTO.Length)
  {
    /* Enqueue response packet */
    Xcp_InsertResponseCTO(&ResponseCTO);
  }

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  DBG_XCP_PROCESSSUBCMDGETSLAVEID_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

#endif

#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))

/*----------------------[Xcp_ProcessSubCmdGetDaqID]-----------------------*/

#if (STD_ON == XCP_ON_CAN_ENABLED)

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessSubCmdGetDaqID(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;
  /* Retrieve requested DAQ List number */
  const uint16 DaqListNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);

  DBG_XCP_PROCESSSUBCMDGETDAQID_ENTRY();

  /* If DAQ List number is invalid */
  if (DaqListNumber >= XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(uint16))
  {
    /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
    ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
  }
  else
  {
    /* Get the DAQ list based on the DAQ list number */
    P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListNumber);

    /* If requested DAQ list's direction is STIM  */
    if (XCP_MASK_DAQLIST_DIRECTION_STIM ==
       (DaqListPtr->Mode & XCP_MASK_DAQLIST_DIRECTION))
    {
      ResponseCTO.CTO[XCP_RES_PID_INDEX]   = XCP_RES_PID;
      ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_CAN_ID_FIXED;
      ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_RESERVED_BYTE;
      ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = XCP_RESERVED_BYTE;
      Xcp_WriteDwordToByteArray(
        &(ResponseCTO.CTO[XCP_RES_BYTE4_INDEX]),
        (uint32)XCP_CMD_STIM_CAN_ID);
      ResponseCTO.Length = XCP_SUBCMD_GET_DAQ_ID_RES_LENGTH;
    }
    /* Requested DAQ list's direction is DAQ */
    else
    {
      ResponseCTO.CTO[XCP_RES_PID_INDEX]   = XCP_RES_PID;
      ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_CAN_ID_FIXED;
      ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_RESERVED_BYTE;
      ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = XCP_RESERVED_BYTE;
      Xcp_WriteDwordToByteArray(
        &(ResponseCTO.CTO[XCP_RES_BYTE4_INDEX]),
        (uint32)XCP_CMD_DAQ_CAN_ID);
      ResponseCTO.Length = XCP_SUBCMD_GET_DAQ_ID_RES_LENGTH;
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSSUBCMDGETDAQID_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

#endif

#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK)

/*--------------------[Xcp_ProcessCmdFreeDAQ]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdFreeDAQ(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDFREEDAQ_ENTRY();

  /* If both DAQ and STIM resources are protected */
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* Daq and STIM is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Local variable to hold Dynamic DAQ List  */
    Xcp_DaqIdType DAQListNumber;

    /* Enter the critical section as the DAQ list can be read during Xcp_SetEvent(), if
     * associated to the event */
    SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* Clear dynamically allocated DAQ Lists */
    for ( DAQListNumber = XCP_MIN_DAQ;
          DAQListNumber < XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(Xcp_DaqIdType);
          DAQListNumber++
        )
    {
      /* Get the DAQ list based on the DAQ list number */
      P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DAQListNumber);

      /* If any event is associated with the DAQList, this needs to be removed */
      if (DaqListPtr->EventId < XCP_MAX_EVENT_CHANNEL)
      {
        Xcp_RemoveDAQListFromEventChannel(DAQListNumber, DaqListPtr->EventId);
      }
    }

    /* Exit the critical section */
    SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* Clear the dynamic info block */
    Xcp_ClearDynamicInfoBlock();

    /* Update last serviced dynamic DAQ request */
    Xcp_DynamicInfo.LastDynamicDAQReq = XCP_CMD_FREE_DAQ_PID;

    /* Reset the entire dynamic memory area to 0 */
    TS_MemBZero(Xcp_DaqLists.Xcp_DynamicArea, XCP_DYNAMIC_AREA_SIZE);

    /* Prepare CTO with positive response */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
    ResponseCTO.Length = XCP_CMD_FREE_DAQ_RES_LENGTH;
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDFREEDAQ_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdAllocDAQ]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdAllocDAQ(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDALLOCDAQ_ENTRY();

  /* If both DAQ and STIM resources are protected */
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* Both Daq and STIM is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Local variable to hold number of available DAQ Lists to be configured */
    uint16 NumberOfConfigurableDaqLists;

    /* Get requested number of DAQ Lists */
    const uint16 RequestedNoOfDAQs = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);

    /* How many bytes are being consumed already by dynamically allocated DAQ lists (via a
     * previous ALLOC_DAQ command) */
    uint32 SpaceConsumedByDaqLists =  Xcp_DynamicInfo.NoOfConfiguredDynamicDAQLists * sizeof(Xcp_DaqType);

    /* Identify number of available DAQ Lists to be configured in the pre-allocated area */
    NumberOfConfigurableDaqLists =
        (uint16)((XCP_DYNAMIC_AREA_SIZE - SpaceConsumedByDaqLists)/(sizeof(Xcp_DaqType)));

    /* Check if the calling sequence is correct: ALLOC_DAQ must be called after a FREE_DAQ and
     * before any ALLOC_ODT and ALLOC_ODT_ENTRY to be called */
    if (Xcp_DynamicInfo.LastDynamicDAQReq < XCP_CMD_ALLOC_DAQ_PID)
    {
      /* Sequence is not correct, send error */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEQUENCE;
      ResponseCTO.Length = XCP_ERR_SEQUENCE_LENGTH;
    }
    /* Verify whether the requested number of DAQ Lists is valid and within range */
    else if ((RequestedNoOfDAQs == 0U) ||
             (RequestedNoOfDAQs > (XCP_DAQ_COUNT - (uint16)Xcp_DynamicInfo.NoOfConfiguredDynamicDAQLists))
            )

    {
      /* Invalid number of DAQ Lists  */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
      ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
    }
    /* Verify whether there is enough space available to allocate requested number of DAQ Lists */
    else if (RequestedNoOfDAQs > NumberOfConfigurableDaqLists)
    {
      /* Reset the entire configuration */
      Xcp_ClearDynamicInfoBlock();

      /* Not enough space available to allocate new DAQ lists - send error */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_MEMORY_OVERFLOW;
      ResponseCTO.Length = XCP_ERR_MEMORY_OVERFLOW_LENGTH;
    }
    else
    {
      /* Iterator */
      Xcp_DaqIdType i;
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK)
      /* FIRST_PID */
      uint8 FirstPid = 0U;
#if (XCP_MIN_DAQ > 0)
      /* Get the FIRST_PID of the last predefined DAQ list */
      FirstPid = Xcp_DaqListsDefault.Xcp_Daq[XCP_MIN_DAQ-1U].FirstPID +
                 Xcp_DaqListsDefault.Xcp_Daq[XCP_MIN_DAQ-1U].MaxOdt;
#endif
#endif /* XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK */

      /* Save the number of dynamically configured DAQ Lists. Cast is safe due to previous condition
       * that checks that the requested DAQ number is within range. */
      Xcp_DynamicInfo.NoOfConfiguredDynamicDAQLists += (Xcp_DaqIdType)RequestedNoOfDAQs;

      /* For all dynamically allocated DAQ lists */
      for (i=XCP_MIN_DAQ; i< (XCP_MIN_DAQ + Xcp_DynamicInfo.NoOfConfiguredDynamicDAQLists); i++)
      {
        /* Get the DAQ list */
        P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr = XCP_GET_DAQ_LIST_PTR(i);

        /* Set the associated event channel as invalid */
        DaqListPtr->EventId = XCP_INVALID_EVENT_CHANNEL;

#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK)
        /* Set a dummy FIRST_PID for the dynamic DAQ lists. This value should be overwritten by
         * follow up ALLOC_ODT commands.
         * The cast to uint8 is safe because the VSMD checks make sure
         * the right-hand number cannot be greater than 255 */
        DaqListPtr->FirstPID = (uint8)(FirstPid + ((i-XCP_MIN_DAQ) * XCP_ODT_COUNT));
#endif /* XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK */
      }

      /* Update last serviced dynamic DAQ request */
      Xcp_DynamicInfo.LastDynamicDAQReq = XCP_CMD_ALLOC_DAQ_PID;
      /* Prepare CTO with positive response */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
      ResponseCTO.Length = XCP_CMD_ALLOC_DAQ_RES_LENGTH;
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDALLOCDAQ_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdAllocODT]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdAllocODT(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDALLOCODT_ENTRY();

  /* This is needed to prevent MISRA violations when the only DAQ lists allowed are predefined ones */
#if (XCP_MIN_DAQ == XCP_MAX_DAQ)
  /* There is no available dynamic DAQ list to be configured */
  ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
  ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
  ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
#else

  /* Verify protection status */
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* Both Daq and STIM is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Calculate next DAQ List to be allocated */
    const uint16 DAQListTobeAllocated = XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(uint16);
    /* Retrieve the requested DAQ List number */
    const uint16 RequestedDAQNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);
    /* Retrieve the requested number of ODTs */
    const uint8 RequestedNoOfODTs = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE4_INDEX);
    /* Get the DAQ list based on the DAQ list number */
    P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) RequestedDaqListPtr = XCP_GET_DAQ_LIST_PTR(RequestedDAQNumber);

    /* ALLOC_ODT command must be called only after an ALLOC_DAQ or ALLOC_ODT command */
    if ((Xcp_DynamicInfo.LastDynamicDAQReq != XCP_CMD_ALLOC_DAQ_PID) &&
        (Xcp_DynamicInfo.LastDynamicDAQReq != XCP_CMD_ALLOC_ODT_PID)
       )
    {
      /* Violated dynamic allocation sequence:
         ALLOC_ODT shall not follow a FREE_DAQ or an ALLOC_ODT_ENTRY command */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEQUENCE;
      ResponseCTO.Length = XCP_ERR_SEQUENCE_LENGTH;
    }
    /* Check whether the requested DAQ list is available:
       Check whether the DAQ List is a dynamic DAQ list and is already allocated.
       Check whether number of ODTs requested is valid and within range */


#if (XCP_MIN_DAQ != 0)
    else if ((RequestedDAQNumber < XCP_MIN_DAQ ) ||
             ((RequestedDAQNumber >= XCP_MAX_DAQ) ||
              ((RequestedDAQNumber >= DAQListTobeAllocated ) ||
               ((RequestedNoOfODTs == 0U) ||
                (RequestedNoOfODTs > (XCP_ODT_COUNT - RequestedDaqListPtr->MaxOdt))
               )
              )
             )
            )
#else
    else if ((RequestedDAQNumber >= XCP_MAX_DAQ) ||
              ((RequestedDAQNumber >= DAQListTobeAllocated ) ||
               ((RequestedNoOfODTs == 0U) ||
                (RequestedNoOfODTs > (XCP_ODT_COUNT - RequestedDaqListPtr->MaxOdt))
               )
              )
            )
#endif
    {
      /* The DAQ List is not allocated or is not dynamic or the number of ODTs
       * requested is invalid */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
      ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
    }
    else /* DAQ List number is valid */
    {
      /* Do not allow allocation to an already configured (using SET_DAQ_LIST_MODE) DAQ List */
      if (XCP_IS_DAQLIST_FULLY_CONFIGURED(RequestedDaqListPtr) == TRUE)
      {
        /* DAQ List is not available for further allocation of associated ODTS */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_RES_TEMP_NOT_A;
        ResponseCTO.Length = XCP_ERR_RES_TEMP_NOT_A_LENGTH;
      }
      else
      {
        /* Local variable to hold number of available free space for ODT */
        uint16 FreeODTSpace;

        /* How many bytes are being consumed by the DAQ lists */
        uint32 SpaceConsumedByDaqLists =  Xcp_DynamicInfo.NoOfConfiguredDynamicDAQLists * sizeof(Xcp_DaqType);

        /* Identify available free space in the allocated memory area */
        FreeODTSpace = (uint16)(((XCP_DYNAMIC_AREA_SIZE - SpaceConsumedByDaqLists)/
                                 ((uint32)sizeof(Xcp_OdtType))
                                ) -
                                Xcp_DynamicInfo.AllocatedOdtsSoFar
                               );

        /* Check whether there is enough space available to allocate the
         * requested ODTs */
        if (RequestedNoOfODTs > FreeODTSpace)
        {
          /* Reset the entire configuration */
          Xcp_ClearDynamicInfoBlock();

          /* Not enough space available to allocate new ODTs */
          ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
          ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_MEMORY_OVERFLOW;
          ResponseCTO.Length = XCP_ERR_MEMORY_OVERFLOW_LENGTH;
        }
        else
        {
          /* Address of the very first DAQ list - the beginning of the dynamic area */
          /* Deviation MISRA-6 <START> */
          P2VAR(Xcp_DaqType, TYPEDEF, XCP_VAR) AddressOfTheFirstDynamicDaq =
          (Xcp_DaqType *)(&(Xcp_DaqLists.Xcp_DynamicArea[0U]));
          /* Deviation MISRA-6 <STOP> */

          /* If this is the first ALLOC_ODT command, we must set the address of the first ODT */
          if (Xcp_DynamicInfo.LastDynamicDAQReq == XCP_CMD_ALLOC_DAQ_PID)
          {
            /* Save the address of the very first ODT */
            /* Deviation MISRA-6 <START> */
            Xcp_DynamicInfo.FirstDynamicOdtPtr =
            (Xcp_OdtType *)(&AddressOfTheFirstDynamicDaq[Xcp_DynamicInfo.NoOfConfiguredDynamicDAQLists]);
            /* Deviation MISRA-6 <STOP> */
          }

          /* Allocate ODTs in the space set aside for ODTs associated with the specified DAQ */
          RequestedDaqListPtr->MaxOdt += RequestedNoOfODTs;

          /* Update how many ODTs have been allocated so far */
          Xcp_DynamicInfo.AllocatedOdtsSoFar+=RequestedNoOfODTs;

          /* Update last serviced dynamic DAQ request */
          Xcp_DynamicInfo.LastDynamicDAQReq = XCP_CMD_ALLOC_ODT_PID;
          /* Prepare CTO with positive response */
          ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
          ResponseCTO.Length = XCP_CMD_ALLOC_DAQ_RES_LENGTH;
        }
      }
    }
  }
#endif /* XCP_MIN_DAQ == XCP_MAX_DAQ */

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  DBG_XCP_PROCESSCMDALLOCODT_EXIT(XCP_E_OK);

  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdAllocODTEntry]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdAllocODTEntry(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDALLOCODTENTRY_ENTRY();

  /* This is needed to prevent MISRA violations when the only DAQ lists allowed are predefined ones */
#if (XCP_MIN_DAQ == XCP_MAX_DAQ)
  /* There is no available dynamic DAQ list to be configured */
  ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
  ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
  ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
#else

  /* Verify protection status */
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* Both Daq and STIM is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  /* ALLOC_ODT_ENTRY command must be called only after an ALLOC_ODT or ALLOC_ODT_ENTRY command */
  else if ((Xcp_DynamicInfo.LastDynamicDAQReq != XCP_CMD_ALLOC_ODT_PID) &&
      (Xcp_DynamicInfo.LastDynamicDAQReq != XCP_CMD_ALLOC_ODT_ENTRY_PID)
     )
  {
    /* Violated dynamic allocation sequence:
       ALLOC_ODT_ENTRY shall not follow a FREE_DAQ or an ALLOC_DAQ command */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEQUENCE;
    ResponseCTO.Length = XCP_ERR_SEQUENCE_LENGTH;
  }
  else
  {
    /* Calculate next DAQ List to be allocated */
    const uint16 DAQListTobeAllocated = XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(uint16);
    /* Retrieve the requested DAQ List number */
    const uint16 RequestedDAQNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);

    /* Check whether the requested DAQ list is available */
#if (XCP_MIN_DAQ != 0)
    if ( (RequestedDAQNumber < XCP_MIN_DAQ ) ||
         ((RequestedDAQNumber >= XCP_MAX_DAQ) ||
          (RequestedDAQNumber >= DAQListTobeAllocated)
         )
       )
#else
    if ((RequestedDAQNumber >= XCP_MAX_DAQ) ||
        (RequestedDAQNumber >= DAQListTobeAllocated )
       )
#endif
    {
      /* The DAQ List is not allocated or is not dynamic */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
      ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
    }
    else
    {
      /* Retrieve the requested ODT number */
      const uint8 RequestedODTNumber = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE4_INDEX);
      /* Get the DAQ list based on the number */
      P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) RequestedDaqListPtr = XCP_GET_DAQ_LIST_PTR(RequestedDAQNumber);

      /* If this is the first ALLOC_ODT_ENTRY command we must arrange all allocated ODTs in the
       * dynamic memory area */
      if (Xcp_DynamicInfo.LastDynamicDAQReq == XCP_CMD_ALLOC_ODT_PID)
      {
        /* We need to re-assign the start position of where the first ODT is for each DAQ list */
        Xcp_ArrangeDynamicODTs();
      }

      /* Check that requested ODT number is valid */
      if (RequestedODTNumber >= RequestedDaqListPtr->MaxOdt)
      {
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
        ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
      }
      else
      {
        /* Retrieve the requested number of ODT Entries */
        const uint8 ReqNoOfODTEntries = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE5_INDEX);

        /* Get the number of ODT entries which can be allocated in the ODT based on the ASAM limits
         * (ODT_ENTRY_COUNT) */
        uint8 OdtEntriesThatCanBeAllocated =
            XCP_MAX_ENTRIES_PER_ODT_DYN - RequestedDaqListPtr->OdtList[RequestedODTNumber].NrOfOdtEntries;

#if (XCP_TIMESTAMP_SUPPORTED == XCP_TIMESTAMP_MASK)
       /* If timestamp is enabled and the requested ODT is zero calculate the free ODT entry space
        * taking into consideration the timestamp limitation */
        if( RequestedODTNumber == 0U )
        {
          OdtEntriesThatCanBeAllocated =
              XCP_MAX_ENTRIES_PER_ODT_DYN_TS - RequestedDaqListPtr->OdtList[RequestedODTNumber].NrOfOdtEntries;
        }
#endif

        /* Check whether number of ODT Entries requested is valid or the requested ODT is valid */
        if ((ReqNoOfODTEntries == 0U) ||
            (ReqNoOfODTEntries > OdtEntriesThatCanBeAllocated)
           )
        {
          /* The DAQ List is not allocated or is not dynamic */
          ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
          ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
          ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
        }
        /* Do not allow allocation to an already configured (using SET_DAQ_LIST_MODE) DAQ List */
        else if (XCP_IS_DAQLIST_FULLY_CONFIGURED(RequestedDaqListPtr) == TRUE)
        {
          /* DAQ List is not available for further allocation of associated ODTs */
          ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
          ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_RES_TEMP_NOT_A;
          ResponseCTO.Length = XCP_ERR_RES_TEMP_NOT_A_LENGTH;
        }
        else
        {
          /* Local variable to hold number of available free space for ODT Entries */
          uint16 FreeODTEntrySpace;

          /* How many bytes are being consumed by DAQ lists and ODTs */
          uint32 SpaceConsumedByDaqListsAndOdts =
              ((uint32)Xcp_DynamicInfo.NoOfConfiguredDynamicDAQLists * (uint32)sizeof(Xcp_DaqType)) +
              (Xcp_DynamicInfo.AllocatedOdtsSoFar * (uint32)sizeof(Xcp_OdtType));

          /* Identify available free space for ODT entries in the allocated memory */
          FreeODTEntrySpace = (uint16)(((XCP_DYNAMIC_AREA_SIZE - SpaceConsumedByDaqListsAndOdts)/
                                        (sizeof(Xcp_OdtEntryType))
                                       ) -
                                       Xcp_DynamicInfo.AllocatedOdtEntriesSoFar
                                      );

          /* Check whether there is enough space available to allocate the requested ODT Entries */
          if (ReqNoOfODTEntries > FreeODTEntrySpace)
          {
            /* Reset the entire configuration */
            Xcp_ClearDynamicInfoBlock();

            /* Not enough space available to allocate new ODT entries - send error */
            ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
            ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_MEMORY_OVERFLOW;
            ResponseCTO.Length = XCP_ERR_MEMORY_OVERFLOW_LENGTH;
          }
          else
          {
            /* If this is the first ALLOC_ODT_ENTRY command, we must set the address of the first ODT
             * entry */
            if (Xcp_DynamicInfo.LastDynamicDAQReq == XCP_CMD_ALLOC_ODT_PID)
            {
              /* Save the address of the absolute first ODT Entry */
              /* Deviation MISRA-6 <START> */
              Xcp_DynamicInfo.FirstDynamicOdtEntryPtr =
                  (Xcp_OdtEntryType *)(&(Xcp_DynamicInfo.FirstDynamicOdtPtr[Xcp_DynamicInfo.AllocatedOdtsSoFar]));
              /* Deviation MISRA-6 <STOP> */
            }

            /* Allocate ODTEntries in the space set aside for ODT Entries
             * associated with the specified ODT */
            RequestedDaqListPtr->OdtList[RequestedODTNumber].NrOfOdtEntries += ReqNoOfODTEntries;

            /* Update how many ODT entries have been allocated so far */
            Xcp_DynamicInfo.AllocatedOdtEntriesSoFar+=ReqNoOfODTEntries;

            /* Update last serviced dynamic DAQ request */
            Xcp_DynamicInfo.LastDynamicDAQReq = XCP_CMD_ALLOC_ODT_ENTRY_PID;
            /* Prepare CTO with positive response */
            ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
            ResponseCTO.Length = XCP_CMD_ALLOC_DAQ_RES_LENGTH;
          }
        }
      }
    }
  }
#endif /* XCP_MIN_DAQ == XCP_MAX_DAQ */

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDALLOCODTENTRY_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

STATIC FUNC(void, XCP_CODE) Xcp_ClearDynamicInfoBlock(void)
{
  DBG_XCP_CLEARDYNAMICINFOBLOCK_ENTRY();

  /* Clear number of dynamically allocated DAQ lists */
  Xcp_DynamicInfo.NoOfConfiguredDynamicDAQLists = 0U;
  /* Clear number of allocated ODTs */
  Xcp_DynamicInfo.AllocatedOdtsSoFar = 0U;
  /* Clear number of allocated ODT entries */
  Xcp_DynamicInfo.AllocatedOdtEntriesSoFar = 0U;
  /* Reset the address of the first dynamic ODT */
  Xcp_DynamicInfo.FirstDynamicOdtPtr = NULL_PTR;
  /* Reset the address of the first dynamic ODT Entry */
  Xcp_DynamicInfo.FirstDynamicOdtEntryPtr = NULL_PTR;
  /* Reset the last dynamic configuration command */
  Xcp_DynamicInfo.LastDynamicDAQReq = XCP_CMD_FREE_DAQ_NOT_RECEIVED;

  DBG_XCP_CLEARDYNAMICINFOBLOCK_EXIT();
}

#if (XCP_MIN_DAQ != XCP_MAX_DAQ) /* this condition is put to avoid compiler warnings when there are
no dynamic DAQ lists configurable as, then, this function would not be called by anybody */
STATIC FUNC(void, XCP_CODE) Xcp_ArrangeDynamicODTs(void)
{
  /* Iterator through dynamic DAQ lists */
  Xcp_DaqIdType DaqListNumber;

  /* The index in the dynamic area where the first ODT for the required DAQ list resides */
  uint32 IndexOfTheFirstOdtInDAQ = 0U;

  for (DaqListNumber=XCP_MIN_DAQ;
         DaqListNumber < XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(Xcp_DaqIdType);
         DaqListNumber++)
  {
    /* Get the DAQ list */
    P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DynamicDaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListNumber);

    if (DynamicDaqListPtr->MaxOdt > 0U)
    {
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK)
      /* Variable to calculate FIRST_PID */
      uint8 FirstPid = 0U;
#endif
      /* Assign the address of the first ODT */
      DynamicDaqListPtr->OdtList = &(Xcp_DynamicInfo.FirstDynamicOdtPtr[IndexOfTheFirstOdtInDAQ]);
      /* Move the position to the next first ODT */
      IndexOfTheFirstOdtInDAQ += DynamicDaqListPtr->MaxOdt;

/* We must re-calculate the FIRST_PID */
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK)
      if (DaqListNumber > 0U)
      {
        CONSTP2CONST(Xcp_DaqType, AUTOMATIC, AUTOMATIC) PreviousDaqListPtr =
            XCP_GET_DAQ_LIST_PTR(DaqListNumber - 1U);
        FirstPid = PreviousDaqListPtr->FirstPID + PreviousDaqListPtr->MaxOdt;
      }
#if (XCP_MIN_DAQ > 0)
      else
      {
        /* Get the FIRST_PID of the last predefined DAQ list */
        FirstPid = Xcp_DaqListsDefault.Xcp_Daq[XCP_MIN_DAQ-1U].FirstPID +
                   Xcp_DaqListsDefault.Xcp_Daq[XCP_MIN_DAQ-1U].MaxOdt;
      }
#endif
      /* Set the FIRST_PID of the containing DAQ list */
      DynamicDaqListPtr->FirstPID = FirstPid;
#endif  /* XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK */
    }
  }
}
#endif /* XCP_MIN_DAQ != XCP_MAX_DAQ */

STATIC FUNC(void, XCP_CODE) Xcp_ArrangeDynamicODTEntries(void)
{
  /* Iterator through dynamic DAQ lists */
  Xcp_DaqIdType DaqListNumber;
  /* Iterator through dynamic ODTs */
  uint8 ODTNumber;
  /* The index in the dynamic area where the first ODT entry for the required ODT is */
  uint32 IndexOfTheFirstOdtEntryInOdt = 0U;

  for (DaqListNumber=XCP_MIN_DAQ;
         DaqListNumber < XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(Xcp_DaqIdType);
         DaqListNumber++)
  {
    /* Get the DAQ list */
    P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DynamicDaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListNumber);

    for (ODTNumber=0U; ODTNumber < DynamicDaqListPtr->MaxOdt; ODTNumber++)
    {
      /* Get the ODT */
      P2VAR(Xcp_OdtType, TYPEDEF, XCP_VAR) DynamicOdt = &DynamicDaqListPtr->OdtList[ODTNumber];

      if (DynamicOdt->NrOfOdtEntries > 0U)
      {
        /* Set the pointer to the first ODT Entry in the ODT */
        DynamicOdt->OdtEntry = &(Xcp_DynamicInfo.FirstDynamicOdtEntryPtr[IndexOfTheFirstOdtEntryInOdt]);
        /* Move the position to the next first ODT */
        IndexOfTheFirstOdtEntryInOdt += DynamicOdt->NrOfOdtEntries;
      }
    }
  }
}

#endif /* (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK) */

/*--------------------[Xcp_ProcessCmdSetDAQPtr]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdSetDAQPtr(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDSETDAQPTR_ENTRY();

  /* Verify protection status for both DAQ and STIM*/
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* BOTH Daq and STIM is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Retrieve the requested DAQ List number */
    const uint16 RequestedDAQListNumber = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);

    /* Verify requested DAQ List */
    if (RequestedDAQListNumber >= XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(uint16))
    {
      /* The requested DAQ List is invalid */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
      ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
    }
    else
    {
      /* Local variable to hold requested ODT number */
      uint8 RequestedODTNumber;
      /* Get the DAQ list based on the DAQ list number */
      P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) RequestedDaqList = XCP_GET_DAQ_LIST_PTR(RequestedDAQListNumber);

      /* Retrieve the requested ODT number */
      RequestedODTNumber = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE4_INDEX);

#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK)
      /* We need to re-assign the start position of where the first ODT entry is for each ODT,
       * if the last command was ALLOC_ODT_ENTRY */
      if (Xcp_DynamicInfo.LastDynamicDAQReq == XCP_CMD_ALLOC_ODT_ENTRY_PID)
      {
        Xcp_ArrangeDynamicODTEntries();
        /* Update last serviced dynamic DAQ request */
        Xcp_DynamicInfo.LastDynamicDAQReq = XCP_CMD_SET_DAQ_PTR_PID;
      }
#endif

      /* Verify requested ODT */
      if (RequestedDaqList->MaxOdt <= RequestedODTNumber)
      {
        /* The requested ODT is invalid */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
        ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
      }
      else
      {
        /* Retrieve the requested ODT Entry number */
        uint8 RequestedODTEntryNumber = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE5_INDEX);

        /* Verify requested ODT Entry */
        if (RequestedDaqList->OdtList[RequestedODTNumber].NrOfOdtEntries <= RequestedODTEntryNumber)
        {
          /* The requested ODT Entry is invalid */
          ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
          ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
          ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
        }
        else
        {
          /* Set DAQ pointer */
          Xcp_DAQPointer.DAQList = RequestedDAQListNumber;
          Xcp_DAQPointer.ODT = RequestedODTNumber;
          Xcp_DAQPointer.ODTEntry = RequestedODTEntryNumber;
          /* Prepare CTO with positive response */
          ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
          ResponseCTO.Length = XCP_CMD_SET_DAQ_PTR_RES_LENGTH;
        }
      }
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDSETDAQPTR_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdWriteDAQ]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdWriteDAQ(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDWRITEDAQ_ENTRY();

  /* Verify protection status */
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* Daq is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Check whether the DAQ pointer points to a valid DAQ */
    if (Xcp_DAQPointer.DAQList >= XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(uint16))
    {
      /* Memory location is not accessible / invalid */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_DENIED;
      ResponseCTO.Length = XCP_ERR_ACCESS_DENIED_LENGTH;
    }
#if (XCP_MIN_DAQ != 0)
    /* Check whether the DAQ List is writable */
    else if (Xcp_DAQPointer.DAQList < XCP_MIN_DAQ)
    {
      /* DAQ List is not writable */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_WRITE_PROTECTED;
      ResponseCTO.Length = XCP_ERR_WRITE_PROTECTED_LENGTH;
    }
#endif /* XCP_MIN_DAQ != 0 */
    else
    {
      /* Local variable to hold requested size of DAQ Element */
      /* Retrieve requested size of DAQ Element */
      uint8 ReqDAQElementSize = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE2_INDEX);
      /* Get the DAQ list based on the information provided by previous SET_DAQ_PTR command */
      P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) PointedDaqListPtr = XCP_GET_DAQ_LIST_PTR(Xcp_DAQPointer.DAQList);

      /* Check whether the DAQ pointer points to a valid ODT */
      if (PointedDaqListPtr->MaxOdt <= Xcp_DAQPointer.ODT)
      {
        /* Memory location is not accessible / invalid */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_DENIED;
        ResponseCTO.Length = XCP_ERR_ACCESS_DENIED_LENGTH;
      }
      /* Check whether the DAQ pointer points to a valid ODTEntry */
      else if (PointedDaqListPtr->OdtList[Xcp_DAQPointer.ODT].NrOfOdtEntries <= Xcp_DAQPointer.ODTEntry)
      {
        /* Memory location is not accessible / invalid */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_DENIED;
        ResponseCTO.Length = XCP_ERR_ACCESS_DENIED_LENGTH;
      }
      /* Preprocessor check is needed to avoid compiler warnings when MAX size is 255 => condition
       * will be always true as ReqDAQElementSize is an 8bit value */
  #if (XCP_MAX_ODT_ENTRY_SIZE_ABS < 255)
      /* Check if the requested size is within limits */
      else if (ReqDAQElementSize > XCP_MAX_ODT_ENTRY_SIZE_ABS )
      {
        /* Invalid ODT entry length */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
        ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
      }
  #endif /* MAX_ODT_ENTRY_SIZE_ABS < 255 */
      /* If the DAQ already started */
      else if (XCP_IS_DAQ_LIST_RUNNING(PointedDaqListPtr) == TRUE)
      {
        /* Prepare error CTO with negative response ERR_DAQ_ACTIVE */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_DAQ_ACTIVE;
        ResponseCTO.Length = XCP_ERR_DAQ_ACTIVE_LENGTH;
      }
      else
      {
        /* Local variable to hold ODT size */
        uint16 ODTSize = 0U;
        /* Local variable to hold number of ODT entries in the ODT */
        uint8 NoOfODTEntries;
        /* Local variable to hold header size of Xcp message */
        uint8 HeaderSize;
        /* Local variable to hold ODT pointer */
        P2VAR(Xcp_OdtType, AUTOMATIC, XCP_APPL_DATA) OdtPtr;

        /* Get ODT pointer */
        OdtPtr = &(PointedDaqListPtr->OdtList[Xcp_DAQPointer.ODT]);
        /* Get number of entries in the target ODT */
        NoOfODTEntries = OdtPtr->NrOfOdtEntries;
        /* Calculate size of elements represented by the ODT */
        while (NoOfODTEntries > 0U)
        {
          NoOfODTEntries--;
          /* Get the ODT entry size in AG */
          ODTSize += OdtPtr->OdtEntry[NoOfODTEntries].Length;
        }
        /* Calculate resultant size of ODT after update with the requested
         * data */
        ODTSize -= OdtPtr->OdtEntry[Xcp_DAQPointer.ODTEntry].Length;
        ODTSize += ReqDAQElementSize;

        /* Calculate the header size (Length of Identifier field and Timestamp field) */
        /* The time stamp field will be present only for the first ODT of the DAQ list */
        if (Xcp_DAQPointer.ODT == 0U)
        {
          HeaderSize = XCP_PID_LENGTH + XCP_TS_LENGTH;
        }
        else
        {
          HeaderSize = XCP_PID_LENGTH;
        }

        /* Check whether the resultant size is within limits */
        if (XCP_MAX_DTO < (ODTSize + HeaderSize))
        {
          /* Resultant size of ODT is invalid or size of ODT entry is invalid */
          ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
          ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
          ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
        }
        else
        {
          /* Write ODT entry */

          /* Retrieve address of element.*/
          const uint32 addr = Xcp_Uint32FromCmd(XCP_CMD_BYTE4_INDEX);
          /* Deviation MISRA-5 <START> */
          OdtPtr->OdtEntry[Xcp_DAQPointer.ODTEntry].Address
            = (P2VAR(uint8, XCP_VAR, XCP_APPL_DATA))addr;
          /* Deviation MISRA-5 <STOP> */
          OdtPtr->OdtEntry[Xcp_DAQPointer.ODTEntry].Length = ReqDAQElementSize;

          /* Increment DAQ pointer to next ODT entry */
          Xcp_DAQPointer.ODTEntry++;
          /* Prepare CTO with positive response */
          ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
          ResponseCTO.Length = XCP_CMD_WRITE_DAQ_RES_LENGTH;
        }
      }
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDWRITEDAQ_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

#endif /*((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) ||
        * (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))*/

#if (XCP_SEED_AND_KEY_ENABLED == STD_ON)

/*--------------------------[Xcp_ProcessCmdGetSeed]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetSeed(void)
{
  /* Local variable to hold requested resource type */
  uint8 ResourceIdentifier;
  /* Local variable to hold requested mode */
  uint8 Mode;
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETSEED_ENTRY();

  /* Retrieve requested mode */
  Mode = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);
  /* Retrieve requested Resource Type */
  ResourceIdentifier = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE2_INDEX);

  /* Verify requested resource type
     Present implementation doesn't support Mode = 1 */
  if ((Mode != XCP_SEC_ACCESS_GETSEED_ZERO) ||
          (((((ResourceIdentifier != (uint8)XCP_RESOURCE_PGM_MASK) &&
          (ResourceIdentifier != (uint8)XCP_RESOURCE_STIM_MASK)) &&
          (ResourceIdentifier != (uint8)XCP_RESOURCE_DAQ_MASK)) &&
          (ResourceIdentifier != (uint8)XCP_RESOURCE_CAL_PAG_MASK))
         )
        )
  {
    /* Requested mode is invalid or
       Requested resource type is out of range */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
    ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
  }
  /* Verify whether the request follows the sequence rules.
     1) There shall be no previous request and the current request shall be XCP_SEC_ACCESS_GETSEED_ONE
     2) The previous request shall be XCP_SEC_ACCESS_GETSEED_ZERO and the current request
        shall be XCP_SEC_ACCESS_GETSEED_ONE
     3) The previous request shall be XCP_SEC_ACCESS_GETSEED_ONE and the current request
        shall be XCP_SEC_ACCESS_GETSEED_ONE
     4) The previous request shall be XCP_SEC_ACCESS_UNLOCK and the current request shall be
        XCP_SEC_ACCESS_GETSEED_ZERO
   */
  else if (
          /* There was no previous request or previous request was UNLOCK */
          ((Xcp_SecAccess.PrevType > XCP_SEC_ACCESS_GETSEED_ONE) &&
           (Mode != XCP_SEC_ACCESS_GETSEED_ZERO)
          ) ||
          /* Previous request was GET_SEED mode zero(0) */
          (((XCP_SEC_ACCESS_GETSEED_ZERO == Xcp_SecAccess.PrevType) &&
           (Mode != XCP_SEC_ACCESS_GETSEED_ONE)
          ) ||
          /* Previous request was GET_SEED mode One(1) */
          ((XCP_SEC_ACCESS_GETSEED_ONE == Xcp_SecAccess.PrevType) &&
           (Mode != XCP_SEC_ACCESS_GETSEED_ONE)
          ))
         )
  {
    /* Requested sequence is invalid */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEQUENCE;
    ResponseCTO.Length = XCP_ERR_SEQUENCE_LENGTH;
  }
  else
  {
    /* Local variable to hold protection status of the requested resource */
    uint8  ProtectionStatus =  (uint8)(Xcp_Status.ProtectionStatus & ResourceIdentifier);
    /* Check whether the resource is already unlocked */
    if (XCP_RESOURCE_UNLOCKED == ProtectionStatus)
    {
      /* Resource is already unlocked.
         Send response with seed length information = zero(0) */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
      /* Set seed length information */
      ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_RESOURCE_UNLOCKED;
      /* Set response length */
      ResponseCTO.Length = XCP_CMD_GETSEED_RES_LENGTH_MIN;
      /* Set security request status */
      Xcp_SecAccess.PrevType = (Xcp_SecAccessType_t)Mode;
      Xcp_SecAccess.Resource = ResourceIdentifier;
    }
    else
    {
      /* Local variable to hold generated seed length */
      uint8 SeedLength;
      /* Local variable to hold Seed request return value*/
      Std_ReturnType errorCode;

      /* Calculate seed */
      errorCode = Xcp_ApplGetSeed(ResourceIdentifier, &(ResponseCTO.CTO[XCP_RES_BYTE2_INDEX]), &SeedLength);
      /* Check the validity of seed
         Present implementation doesn't support Seed length greater than MAX_CTO - 2 */
      if ((E_OK != errorCode) ||
         (SeedLength > XCP_SEED_KEY_MAX_LENGTH)
        )
      {
        /* Algorithm failed or generated seed cannot be send in one CTO */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
        ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
      }
      else
      {
        /* Set positive response ID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        /* Set seed length */
        ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = SeedLength;
        /* Set length of the response with seed */
        ResponseCTO.Length = SeedLength + XCP_CMD_GETSEED_RES_LENGTH_MIN;
        /* Set security request status */
        Xcp_SecAccess.PrevType = (Xcp_SecAccessType_t)Mode;
        Xcp_SecAccess.Resource = ResourceIdentifier;
      }
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETSEED_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------------[Xcp_ProcessCmdUnlock]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdUnlock(void)
{
  /* Local variable to hold key length information */
  uint8 KeyLength;
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDUNLOCK_ENTRY();

  /* Retrieve key length information */
  KeyLength = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);

  /* Verify whether the request follows the sequence rules.
     UNLOCK request shall be accepted only if there is a prior GET_SEED request. */
  if (XCP_SEC_ACCESS_GETSEED_ZERO != Xcp_SecAccess.PrevType)
  {
    /* Requested sequence is invalid */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEQUENCE;
    ResponseCTO.Length = XCP_ERR_SEQUENCE_LENGTH;
  }
  else
  {
    /* Local variable to hold compare key request return value*/
    Std_ReturnType errorCode;

    /* Compare key */
    errorCode = Xcp_ApplCompareKey(Xcp_SecAccess.Resource,
                                     &(XCP_READ_CMDPACKET_BYTE(XCP_RES_BYTE2_INDEX)),
                                     KeyLength
                                    );
    /* Check the validity of key */
    if (E_OK != errorCode)
    {
      /* Algorithm failed */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
      ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;

      /* Enter critical section */
      SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

      /** Flush queues:
       ** 1. Command Queue is of size one(1) and thus there won't be any new commands to flush
       ** 2. Flush XCP Event CTO Queue
       ** 3. Reset DAQ event channels runtime information
       ** 4. Flush DTO queue
       ** 5. Do not flush CTO response queue as we need to send response to this command
       */
/* If event packet transmission enabled */
#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
      /* Flush XCP events CTOs */
      Xcp_FlushEventCTOQueue();
#endif

/* If DAQ or STIM resource is supported */
#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
         (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))

      /* Reset event channels runtime information */
      Xcp_ResetEventChannels();

/* If DAQ resource is supported */
#if (XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK)
      /* Flush DTO Queue */
      Xcp_FlushDTOQueue();
#endif /* (XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) */

/* If STIM resource is supported */
#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
      /* Flush STIM Buffer */
      Xcp_FlushStimBuffer();
#endif /* (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK) */

#endif /* ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) */

      /* Go to disconnected state */
      Xcp_State = XCP_STATE_DISCONNECTED;

      /* Exit critical section.*/
      SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
    }
    else
    {
      /* Unlock the resource */
      Xcp_Status.ProtectionStatus &= (uint8)(0xFFU ^ Xcp_SecAccess.Resource);
      /* Prepare CTO with positive response */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
      ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_GET_RESOURCE_PROTECTION_STATUS();
      ResponseCTO.Length = XCP_CMD_UNLOCK_RES_LENGTH;
      /* Set security request status */
      Xcp_SecAccess.PrevType = XCP_SEC_ACCESS_UNLOCK;
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDUNLOCK_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

#endif /* XCP_SEED_AND_KEY_ENABLED == STD_ON */

#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))

/*--------------------------[Xcp_ProcessCmdGetDAQClock]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE)  Xcp_ProcessCmdGetDAQClock(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETDAQCLOCK_ENTRY();

  /* If both DAQ and STIM resources are protected */
  if(((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_DAQ_MASK) ==
              (uint8)XCP_RESOURCE_DAQ_MASK) &&
     ((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_STIM_MASK) ==
              (uint8)XCP_RESOURCE_STIM_MASK))
  {
    /* Daq is protected return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {

/* Check if time stamp is supported */
#if (XCP_TIMESTAMP_SUPPORTED == XCP_TIMESTAMP_MASK)
#if (XCP_USER_TIMESTAMP_SUPPORTED == STD_OFF)
      /* Local variable to hold the counter value */
    TickType CounterValue;
#endif  /* (XCP_USER_TIMESTAMP_SUPPORTED == STD_OFF) */

    /* Local variable to hold the timestamp */
    Xcp_TimestampType TimeStamp = 0U;

    /* Prepare CTO with positive response */
    ResponseCTO.CTO[XCP_RES_PID_INDEX]   = XCP_RES_PID;
    ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_RESERVED_BYTE;
    ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_RESERVED_BYTE;
    ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = XCP_RESERVED_BYTE;

#if (XCP_USER_TIMESTAMP_SUPPORTED == STD_OFF)
    /* Retrieve present time stamp from the OS counter */
    (void) GetCounterValue(XCP_OS_COUNTER, &CounterValue);

    /* Convert the CounterVAlue to the Xcp_TimestampType and copy the value to the TimpeStamp variable */
    TimeStamp = (Xcp_TimestampType)CounterValue;
#else
    /* Get the Timestamp value from the user provided function */
    TimeStamp = Xcp_ApplGetTimestamp();
#endif  /* (XCP_USER_TIMESTAMP_SUPPORTED == STD_OFF) */

    /* Copy time stamp to response packet */
    Xcp_WriteDwordToByteArray( &(ResponseCTO.CTO[XCP_RES_BYTE4_INDEX]), TimeStamp );
    /* Set the response length */
    ResponseCTO.Length = XCP_CMD_GET_DAQ_CLOCK_RES_LENGTH;
#else
    /* If time stamp is not supported */
    /* Prepare error CTO with negative response ERR_CMD_UNKNOWN*/
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
    ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
#endif /* (XCP_TIMESTAMP_SUPPORTED == XCP_TIMESTAMP_MASK) */
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETDAQCLOCK_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

#endif  /* (XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) ||
           (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)   */

#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK) || \
     (XCP_MODIFY_BITS_SUPPORTED == STD_ON))

/*--------------------------[Xcp_Uint16FromCmd]-----------------------------*/

STATIC FUNC(uint16, XCP_CODE) Xcp_Uint16FromCmd(uint16 index)
{
  /* pointer to first byte of uint16 value in command array */
  CONSTP2VAR(uint8, AUTOMATIC, XCP_VAR) ptr = &XCP_READ_CMDPACKET_BYTE(index);

  /* The assembled value */
  uint16 RetValue;

  DBG_XCP_UINT16FROMCMD_ENTRY(index);

  /* assemble and return uint16 value respecting assuming that the protocol
   * byte order matches the CPU architecture byte order */
  /* The result of right shift operation is casted with uint16 in order to avoid misra violation
    * Rule 12.7: Btwise operators shall not be applied to operands whose underlying type is signed */
  RetValue = (uint16)
        ((uint16)((uint16)ptr[XCP_UINT16_MSB_IDX] << 8U) | (uint16)ptr[XCP_UINT16_LSB_IDX]);

  DBG_XCP_UINT16FROMCMD_EXIT(RetValue,index);
  return RetValue;
}

#endif  /* (XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) ||
           (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK) ||
           (XCP_MODIFY_BITS_SUPPORTED == STD_ON) */
/*--------------------------[Xcp_Uint32FromCmd]-----------------------------*/

#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK)   || \
     (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK) || \
     (XCP_PGM_SUPPORTED == STD_ON) || \
     (XCP_BUILD_CHECKSUM_SUPPORT == STD_ON))

STATIC FUNC(uint32, XCP_CODE) Xcp_Uint32FromCmd(uint16 index)
{
  /* pointer to first byte of uint32 value in command array */
  CONSTP2VAR(uint8, AUTOMATIC, XCP_VAR) ptr = &XCP_READ_CMDPACKET_BYTE(index);

  /* The assembled value */
  uint32 RetValue;

  DBG_XCP_UINT32FROMCMD_ENTRY(index);
  /* assemble and return uint32 value respecting assuming that the protocol
   * byte order matches the CPU architecture byte order */

  RetValue = (uint32)
            (((uint32)ptr[XCP_UINT32_SB3_IDX] << 24U)
           | ((uint32)ptr[XCP_UINT32_SB2_IDX] << 16U)
           | ((uint32)ptr[XCP_UINT32_SB1_IDX] << 8U)
           | (uint32)ptr[XCP_UINT32_SB0_IDX]);

  DBG_XCP_UINT32FROMCMD_EXIT(RetValue,index);
  return RetValue;
}

#endif /*(XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK)   || \
         (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK) || \
         (XCP_PGM_SUPPORTED == STD_ON) || \
         (XCP_BUILD_CHECKSUM_SUPPORT == STD_ON)*/

#if (XCP_RESOURCE_CAL_PAG == XCP_RESOURCE_CAL_PAG_MASK)
/*--------------------[Xcp_ProcessCmdCopyCalPage]------------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdCopyCalPage(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDCOPYCALPAGE_ENTRY();

  /* Check if the Calibration/Paging Resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & XCP_RESOURCE_CAL_PAG_MASK) ==
                 XCP_RESOURCE_CAL_PAG_MASK)
  {
    /* Calibration commands are protected; return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Callout function return value */
    Xcp_ApplReturnType CalloutRes;
    /* Local variable to hold the Source Segment */
    const uint8 SrcSegment = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);
    /* Local variable to hold the Source Page */
    const uint8 SrcPage = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE2_INDEX);
    /* Local variable to hold the Destination Segment */
    const uint8 DestSegment = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE3_INDEX);
    /* Local variable to hold the Destination Page */
    const uint8 DestPage = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE4_INDEX);

    /* Call Xcp_ApplGetPagProcessorInfo() callout function */
    CalloutRes = Xcp_ApplCopyCalPage( SrcSegment, SrcPage, DestSegment, DestPage );

    /* Check the callout function return */
    switch( CalloutRes )
    {
      case XCP_APPL_OK:
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO.Length = XCP_CMD_COPY_CAL_PAGE_RES_LENGTH;
        break;
      case XCP_APPL_ERR_WRITE_PROTECTED:
        /* Prepare error CTO with negative response ERR_WRITE_PROTECTED */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_WRITE_PROTECTED;
        ResponseCTO.Length = XCP_ERR_WRITE_PROTECTED_LENGTH;
        break;
      case XCP_APPL_ERR_PAGE_NOT_VALID:
        /* Prepare error CTO with negative response ERR_PAGE_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_PAGE_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_PAGE_NOT_VALID_LENGTH;
        break;
      case XCP_APPL_ERR_SEGMENT_NOT_VALID:
        /* Prepare error CTO with negative response ERR_SEGMENT_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEGMENT_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_SEGMENT_NOT_VALID_LENGTH;
        break;
      default:
        /* EB specific handling: */
        /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
        ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
        break;
    }

  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDCOPYCALPAGE_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdGetPagProcInfo]----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetPagProcInfo(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  /* Callout function return value */
  Xcp_ApplReturnType CalloutRes;
  /* Total number of available segments */
  uint8 MaxSegment;
  /* General properties for paging (Freeze Supported) */
  uint8 PagProperties;

  DBG_XCP_PROCESSCMDGETPAGPROCINFO_ENTRY();

  /* Check if the Calibration/Paging Resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & XCP_RESOURCE_CAL_PAG_MASK) ==
                 XCP_RESOURCE_CAL_PAG_MASK)
  {
    /* Calibration commands are protected; return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Call Xcp_ApplGetPagProcessorInfo() callout function */
    CalloutRes = Xcp_ApplGetPagProcessorInfo( &MaxSegment, &PagProperties );

    /* Check the callout function return */
    switch( CalloutRes )
    {
      case XCP_APPL_OK:
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO.CTO[1U] = MaxSegment; /* maximum number of segments */
        ResponseCTO.CTO[2U] = PagProperties; /* freeze mode support */
        ResponseCTO.Length = XCP_CMD_GET_PAG_PROC_INFO_RES_LENGTH;
        break;
      default:
        /* EB specific handling: */
        /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
        ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
        break;
    } /* switch( CalloutRes ) */
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETPAGPROCINFO_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdGetCalPage]-------------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetCalPage(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETCALPAGE_ENTRY();

  /* Check if the Calibration/Paging Resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & XCP_RESOURCE_CAL_PAG_MASK) ==
                 XCP_RESOURCE_CAL_PAG_MASK)
  {
    /* Calibration commands are protected; return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Callout function return value */
    Xcp_ApplReturnType CalloutRes;
    /* Local variable where the Page number returned by the callout function is stored */
    uint8 Page = 0U;
    /* GET_CAL_PAGE command: Mode parameter */
    const uint8 Mode = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);
    /* Segment number */
    const uint8 Segment = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE2_INDEX);

    /* Call Xcp_ApplGetCalPage() callout function */
    CalloutRes = Xcp_ApplGetCalPage( Segment, Mode, &Page );

    switch( CalloutRes )
    {
      case XCP_APPL_OK:
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO.CTO[1U] = XCP_RESERVED_BYTE;
        ResponseCTO.CTO[2U] = XCP_RESERVED_BYTE;
        ResponseCTO.CTO[3U] = Page;
        ResponseCTO.Length = XCP_CMD_GET_CAL_PAGE_RES_LENGTH;
        break;
      case XCP_APPL_ERR_MODE_NOT_VALID:
        /* Prepare error CTO with negative response ERR_MODE_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_MODE_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_MODE_NOT_VALID_LENGTH;
        break;
      case XCP_APPL_ERR_SEGMENT_NOT_VALID:
        /* Prepare error CTO with negative response ERR_SEGMENT_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEGMENT_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_SEGMENT_NOT_VALID_LENGTH;
        break;
      case XCP_APPL_ERR_PAGE_NOT_VALID:
        /* Prepare error CTO with negative response ERR_PAGE_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_PAGE_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_PAGE_NOT_VALID_LENGTH;
        break;
      default:
        /* EB specific handling: */
        /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
        ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
        break;
    } /* switch( CalloutRes ) */
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETCALPAGE_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdSetCalPage]-------------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdSetCalPage(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDSETCALPAGE_ENTRY();

  /* Check if the Calibration/Paging Resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & XCP_RESOURCE_CAL_PAG_MASK) ==
                 XCP_RESOURCE_CAL_PAG_MASK)
  {
    /* Calibration commands are protected; return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Callout function return value */
    Xcp_ApplReturnType CalloutRes;
    /* SET_CAL_PAGE command: Mode parameter */
    const uint8 Mode = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);
    /* Segment Number */
    const uint8 Segment = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE2_INDEX);
    /* Page Number */
    const uint8 Page = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE3_INDEX);

    /* Call Xcp_ApplSetCalPage() callout function */
    CalloutRes = Xcp_ApplSetCalPage( Segment, Page, Mode );

    switch( CalloutRes )
    {
      case XCP_APPL_OK:
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO.Length = XCP_CMD_SET_CAL_PAGE_RES_LENGTH;
        break;
      case XCP_APPL_ERR_PAGE_NOT_VALID:
        /* Prepare error CTO with negative response ERR_PAGE_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_PAGE_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_PAGE_NOT_VALID_LENGTH;
        break;
      case XCP_APPL_ERR_MODE_NOT_VALID:
        /* Prepare error CTO with negative response ERR_MODE_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_MODE_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_MODE_NOT_VALID_LENGTH;
        break;
      case XCP_APPL_ERR_SEGMENT_NOT_VALID:
        /* Prepare error CTO with negative response ERR_SEGMENT_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEGMENT_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_SEGMENT_NOT_VALID_LENGTH;
        break;
      default:
        /* EB specific handling: */
        /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
        ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
        break;
    } /* switch( CalloutRes ) */
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDSETCALPAGE_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdGetSegmentMode]-------------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetSegmentMode(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETSEGMENTMODE_ENTRY();

  /* Check if the Calibration/Paging Resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & XCP_RESOURCE_CAL_PAG_MASK) ==
                 XCP_RESOURCE_CAL_PAG_MASK)
  {
    /* Calibration commands are protected; return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Callout function return value */
    Xcp_ApplReturnType CalloutRes;
    /* Local variable where the Mode that is returned by the callout function is stored */
    uint8 Mode = 0U;
    /* Segment number */
    const uint8 Segment = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE2_INDEX);

    /* Call Xcp_ApplGetSegmentMode() callout function */
    CalloutRes = Xcp_ApplGetSegmentMode( Segment, &Mode );

    switch( CalloutRes )
    {
      case XCP_APPL_OK:
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO.CTO[1U] = XCP_RESERVED_BYTE;
        ResponseCTO.CTO[2U] = Mode;
        ResponseCTO.Length = XCP_CMD_GET_SEGMENT_MODE_RES_LENGTH;
        break;
      case XCP_APPL_ERR_OUT_OF_RANGE:
        /* Prepare error CTO with negative response XCP_APPL_ERR_OUT_OF_RANGE */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_APPL_ERR_OUT_OF_RANGE;
        ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
        break;
      case XCP_APPL_ERR_SEGMENT_NOT_VALID:
        /* Prepare error CTO with negative response XCP_APPL_ERR_SEGMENT_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_APPL_ERR_SEGMENT_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_SEGMENT_NOT_VALID_LENGTH;
        break;
      default:
        /* EB specific handling: */
        /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
        ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
        break;
    } /* switch( CalloutRes ) */
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETSEGMENTMODE_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdSetSegmentMode]-------------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdSetSegmentMode(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDSETSEGMENTMODE_ENTRY();

  /* Check if the Calibration/Paging Resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & XCP_RESOURCE_CAL_PAG_MASK) ==
                 XCP_RESOURCE_CAL_PAG_MASK)
  {
    /* Calibration commands are protected; return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Callout function return value */
    Xcp_ApplReturnType CalloutRes;
    /* SET_SEGMENT_MODE command: Mode parameter */
    const uint8 Mode = ( XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX) & /* Take Bit 0 only */
                         XCP_CMD_SET_GET_SEGMENT_MODE_FREEZE_MASK );
    /* Segment number */
    const uint8 Segment = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE2_INDEX);

    /* Call Xcp_ApplSetSegmentMode() callout function */
    CalloutRes = Xcp_ApplSetSegmentMode( Segment, Mode );

    switch( CalloutRes )
    {
      case XCP_APPL_OK:
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO.Length = XCP_CMD_SET_SEGMENT_MODE_RES_LENGTH;
        break;
      case XCP_APPL_ERR_OUT_OF_RANGE:
        /* Prepare error CTO with negative response XCP_APPL_ERR_OUT_OF_RANGE */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_APPL_ERR_OUT_OF_RANGE;
        ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
        break;
      case XCP_APPL_ERR_SEGMENT_NOT_VALID:
        /* Prepare error CTO with negative response XCP_APPL_ERR_SEGMENT_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_APPL_ERR_SEGMENT_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_SEGMENT_NOT_VALID_LENGTH;
        break;
      case XCP_APPL_ERR_MODE_NOT_VALID:
        /* Prepare error CTO with negative response XCP_APPL_ERR_MODE_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_APPL_ERR_MODE_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_MODE_NOT_VALID_LENGTH;
        break;
      default:
        /* EB specific handling: */
        /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
        ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
        break;
    } /* switch( CalloutRes ) */
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDSETSEGMENTMODE_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdGetSegmentInfo]-------------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetSegmentInfo(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETSEGMENTINFO_ENTRY();

  /* Check if the Calibration/Paging Resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & XCP_RESOURCE_CAL_PAG_MASK) ==
                 XCP_RESOURCE_CAL_PAG_MASK)
  {
    /* Calibration commands are protected; return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Callout function return value */
    Xcp_ApplReturnType CalloutRes;
    /* GET_SEGMENT_INFO: Mode */
    const uint8 Mode = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);
    /* Segment Number */
    const uint8 Segment = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE2_INDEX);
    /* GET_SEGMENT_INFO: Segment Info command parameter */
    const uint8 SegmentInfo = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE3_INDEX);
    /* GET_SEGMENT_INFO: Mapping Index command parameter */
    const uint8 MappingIndex = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE4_INDEX);
    /* Array where the callout function return information is stored */
    uint8 GetSegmentInfoRespData[XCP_GET_SEGMENT_INFO_CMD_RES_LENGTH];

    /* Call Xcp_ApplGetSegmentInfo() callout function */
    CalloutRes = Xcp_ApplGetSegmentInfo( Mode, Segment, SegmentInfo,
                                         MappingIndex, &GetSegmentInfoRespData[0] );

    switch( CalloutRes )
    {
      case XCP_APPL_OK:
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        /* Check Mode parameter */
        if( ( Mode == 0U ) || ( Mode == 2U ) )
        { /* Mode = 0 or Mode = 2 */
          ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_RESERVED_BYTE; /* Reserved */
          ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = XCP_RESERVED_BYTE; /* Reserved */
          ResponseCTO.CTO[XCP_RES_BYTE3_INDEX] = XCP_RESERVED_BYTE; /* Reserved */
          /* Copy 4-byte source/destination address or length parameter */
          TS_MemCpy( &ResponseCTO.CTO[XCP_RES_BYTE4_INDEX],
                     GetSegmentInfoRespData,
                     XCP_CALPAG_ADDRESS_LENGTH_SIZE );
          ResponseCTO.Length = XCP_CMD_GET_SEGMENT_INFO_RES_LENGTH_0;
        }
        else
        { /* Mode = 1 */
          /* Copy standard info for this segment */
          TS_MemCpy( &ResponseCTO.CTO[XCP_RES_BYTE1_INDEX],
                     GetSegmentInfoRespData,
                     XCP_CALPAG_GETSEGMENTINFO_STANDARD_SIZE );
          ResponseCTO.Length = XCP_CMD_GET_SEGMENT_INFO_RES_LENGTH_1;
        }
        break;
      case XCP_APPL_ERR_MODE_NOT_VALID:
        /* Prepare error CTO with negative response ERR_MODE_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_MODE_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_MODE_NOT_VALID_LENGTH;
        break;
      case XCP_APPL_ERR_SEGMENT_NOT_VALID:
        /* Prepare error CTO with negative response ERR_SEGMENT_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEGMENT_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_SEGMENT_NOT_VALID_LENGTH;
        break;
      case XCP_APPL_ERR_OUT_OF_RANGE:
        /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
        ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
        break;
      default:
        /* EB specific handling: */
        /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
        ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
        break;
    } /* switch( CalloutRes ) */
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETSEGMENTINFO_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdGetPageInfo]-------------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetPageInfo(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETPAGEINFO_ENTRY();

  /* Check if the Calibration/Paging Resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & XCP_RESOURCE_CAL_PAG_MASK) ==
                 XCP_RESOURCE_CAL_PAG_MASK)
  {
    /* Calibration commands are protected; return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Callout function return value */
    Xcp_ApplReturnType CalloutRes;
    /* Segment Number */
    const uint8 Segment = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE2_INDEX);
    /* Page Number*/
    const uint8 Page = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE3_INDEX);
    /* Local variable where PageProperties is stored */
    uint8 PageProperties = 0U;
    /* Local variable where the INIT_SEGMENT is stored */
    uint8 InitSegment = 0U;

    /* Call Xcp_ApplGetPageInfo() callout function */
    CalloutRes = Xcp_ApplGetPageInfo( Segment, Page, &PageProperties, &InitSegment );

    switch( CalloutRes )
    {
      case XCP_APPL_OK:
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO.CTO[1U] = PageProperties;
        ResponseCTO.CTO[2U] = InitSegment;
        ResponseCTO.Length = XCP_CMD_GET_PAGE_INFO_RES_LENGTH;
        break;
      case XCP_APPL_ERR_SEGMENT_NOT_VALID:
        /* Prepare error CTO with negative response ERR_SEGMENT_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEGMENT_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_SEGMENT_NOT_VALID_LENGTH;
        break;
      case XCP_APPL_ERR_PAGE_NOT_VALID:
        /* Prepare error CTO with negative response ERR_PAGE_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_PAGE_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_PAGE_NOT_VALID_LENGTH;
        break;
      case XCP_APPL_ERR_OUT_OF_RANGE:
        /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
        ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
        break;
      default:
        /* EB specific handling: */
        /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
        ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
        break;
    } /* switch( CalloutRes ) */
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETPAGEINFO_EXIT(XCP_E_OK);
  return XCP_E_OK;
}

#endif /* XCP_RESOURCE_CAL_PAG == XCP_RESOURCE_CAL_PAG_MASK */

#if (XCP_BUILD_CHECKSUM_SUPPORT == STD_ON)
/*--------------------[Xcp_ProcessCmdBuildChecksum]----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdBuildChecksum(void)
{
  /* Local variable to prepare the response */
  Xcp_CTOType ResponseCTO;
  /* The checksum type defined by ASAM specs*/
  uint8 ChecksumType = 0U;
  /* The result can be either the calculated checksum
   * or the maximum block size if there is a ERR_OUT_OF_RANGE detected */
  uint32 ChecksumResult = 0U;
  /* Get a double word from the command sent by the master containing the block size */
  const uint32 BlockSize = Xcp_Uint32FromCmd(4U);

  DBG_XCP_PROCESSCMDBUILDCHECKSUM_ENTRY();

  if (( Xcp_MTA.Type == XCP_MTATYPE_COUNTER) || ( Xcp_MTA.IsSet == FALSE))
  {
    /* Prepare error CTO with negative response XCP_ERR_ACCESS_DENIED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX]     = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_DENIED;
    ResponseCTO.Length                     = XCP_ERR_ACCESS_DENIED_LENGTH;
  }
  else
  {
#if (XCP_BUILD_CHECKSUM_TYPE == XCP_CRC_USER_CALLOUT)
    Xcp_ApplReturnType CalloutRes = XCP_APPL_OK;

    CalloutRes = Xcp_ApplBuildChecksum( XCP_APPL_GET_ADDRESS( 0U, Xcp_MTA.Address.Write ),
                                        BlockSize,
                                        &ChecksumResult,
                                        &ChecksumType
                                      );
    switch( CalloutRes )
    {
      case XCP_APPL_OK:
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO.CTO[1U] = ChecksumType;
        ResponseCTO.CTO[2U] = XCP_RESERVED_BYTE;
        ResponseCTO.CTO[3U] = XCP_RESERVED_BYTE;
        Xcp_WriteDwordToByteArray( &(ResponseCTO.CTO[4U]), ChecksumResult );
        ResponseCTO.Length = XCP_CMD_BUILD_CHECKSUM_RES_LENGTH;
        /* Post-increment MTA according to command specification */
        XCP_INCREMENT_MTA_BY_SIZE_IN_AG( BlockSize );
        break;
      case XCP_APPL_ERR_OUT_OF_RANGE:
        /* Prepare error CTO with negative response ERR_OUT_OF_RANGE (special
           format for this command). */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
        ResponseCTO.CTO[2U] = XCP_RESERVED_BYTE;
        ResponseCTO.CTO[3U] = XCP_RESERVED_BYTE;
        Xcp_WriteDwordToByteArray( &(ResponseCTO.CTO[4U]), ChecksumResult );
        ResponseCTO.Length = 8U;
        break;
      case XCP_APPL_ERR_ACCESS_DENIED:
        /* Prepare error CTO with negative response ERR_ACCESS_DENIED */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_DENIED;
        ResponseCTO.Length = XCP_ERR_ACCESS_DENIED_LENGTH;
        break;
      default:
        /* EB specific handling: */
        /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
        ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
        break;
    }
#else /* XCP_BUILD_CHECKSUM_TYPE == XCP_CRC_USER_CALLOUT */
#if (XCP_BUILD_CHECKSUM_TYPE == XCP_CRC_16_CITT)
      ChecksumResult = Crc_CalculateCRC16( XCP_APPL_GET_ADDRESS( 0U, Xcp_MTA.Address.Write ),
                                          (BlockSize * XCP_ADDRESS_GRANULARITY_SIZE),
                                           XCP_CRC_16_CITT_STARTVALUE,
                                           TRUE
                                         );
      ChecksumType = 0x08U;
#else /* then XCP_BUILD_CHECKSUM_TYPE == XCP_CRC_32 */
      ChecksumResult = Crc_CalculateCRC32( XCP_APPL_GET_ADDRESS( 0U, Xcp_MTA.Address.Write ),
                                          (BlockSize * XCP_ADDRESS_GRANULARITY_SIZE),
                                           XCP_CRC_32_STARTVALUE,
                                           TRUE
                                          );
      ChecksumType = 0x09U;
#endif /* XCP_BUILD_CHECKSUM_TYPE == XCP_CRC_16_CITT */
      /* Prepare CTO with positive response */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
      ResponseCTO.CTO[1U] = ChecksumType;
      ResponseCTO.CTO[2U] = XCP_RESERVED_BYTE;
      ResponseCTO.CTO[3U] = XCP_RESERVED_BYTE;
      Xcp_WriteDwordToByteArray( &(ResponseCTO.CTO[4U]), ChecksumResult );
      ResponseCTO.Length = XCP_CMD_BUILD_CHECKSUM_RES_LENGTH;
      /* Post-increment MTA according to command specification */
      XCP_INCREMENT_MTA_BY_SIZE_IN_AG( BlockSize );

#endif /* XCP_BUILD_CHECKSUM_TYPE == XCP_CRC_USER_CALLOUT */
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet. */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDBUILDCHECKSUM_EXIT(XCP_E_OK);
  return XCP_E_OK;
}
#endif /* XCP_BUILD_CHECKSUM_SUPPORT == STD_ON */

#if (XCP_MODIFY_BITS_SUPPORTED == STD_ON)
/*-------------------------[Xcp_ProcessCmdModifyBits]------------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdModifyBits(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;
  /* Local variable to hold request parameter Shift Value (S) */
  const uint8 Shift_Value = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);
  /* Local variable to hold request parameter AND Mask (MA) */
  const uint16 And_Mask = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);
  /* Local variable to hold request parameter XOR Mask (MX) */
  const uint16 Xor_Mask = Xcp_Uint16FromCmd(XCP_CMD_BYTE4_INDEX);

  DBG_XCP_PROCESSCMDMODIFYBITS_ENTRY();

  /* Verify protection status for calibration commands */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & XCP_RESOURCE_CAL_PAG_MASK) ==
             XCP_RESOURCE_CAL_PAG_MASK)
  {
    /* Calibration commands are protected; return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  /* Check whether MTA address is set and MTA address has the correct type */
  else if ( ( Xcp_MTA.IsSet == FALSE) || ( Xcp_MTA.Type == XCP_MTATYPE_COUNTER) )
  {
    /* Prepare error CTO with negative response XCP_ERR_ACCESS_DENIED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_DENIED;
    ResponseCTO.Length = XCP_ERR_ACCESS_DENIED_LENGTH;
  }
  /* Verify whether command parameter Shift Value (S) is smaller than 32 */
  else if (Shift_Value > XCP_MODIFY_SHIFT_MAX)
  {
    /* Prepare error CTO with negative response ERR_CMD_OUT_OF_RANGE */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
    ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
  }
  else
  {
    /* Local variable holding the value to be modified */
    uint32 A_Value;
    /* Local temporary variables used to calculate the result of the MODIFY_BITS command.
       This is because of Misra rule 10.3 and 10.5 contradiction. See below. */
    uint32 Temp_Value_1, Temp_Value_2;

    /* Read 4 bytes from memory location pointed to by MTA */
    TS_MemCpy(&A_Value, Xcp_MTA.Address.Read, XCP_MODIFY_VALUE_SIZE);

    /* Modify bits */
    /* This line has been split due to Misra rules 10.3 and 10.5 contradiction:
       A = ( ( A ) & ( (~((uint32)(((uint16)~And_Mask) << Shift_Value))) ) ^
                     ( (uint32)(Xor_Mask << Shift_Value) ) )
       Two temporary variables have been added for this purpose. */

    Temp_Value_1 = (uint32)( (uint16)(~And_Mask) ) << Shift_Value;
    Temp_Value_2 = (uint32)Xor_Mask << Shift_Value;

    /* Calculate A_Value */
    A_Value = ( A_Value & (uint32)( ~Temp_Value_1 ) ) ^ Temp_Value_2;

    /* Write DWORD back to memory location pointed to by MTA */
    TS_MemCpy(Xcp_MTA.Address.Write, &A_Value, XCP_MODIFY_VALUE_SIZE);

    /* Prepare CTO with positive response */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
    ResponseCTO.Length = XCP_CMD_MODIFY_BITS_RES_LENGTH;
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet. */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDMODIFYBITS_EXIT(XCP_E_OK);
  return XCP_E_OK;
}
#endif /* XCP_MODIFY_BITS_SUPPORTED == STD_ON */

#if (XCP_PGM_SUPPORTED == STD_ON)

/*--------------------[Xcp_ProcessCmdProgramMax]-------------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdProgramMax(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDPROGRAMMAX_ENTRY();

  /* Initialize the ResponseCTO length */
  ResponseCTO.Length = 0U;

  /* If PGM resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_PGM_MASK) ==
             (uint8)XCP_RESOURCE_PGM_MASK
    )
  {
    /* PGM is protected, return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* If PROGRAM_START wasn't called before */
    if ((Xcp_CmdReqStatus & XCP_CMD_PROGRAMMING_STARTED_MASK) == 0U)
    {
      /* Fill in ERR_SEQUENCE message */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEQUENCE;
      ResponseCTO.Length = XCP_ERR_SEQUENCE_LENGTH;
    }
    else
    {
      /* Check whether MTA address is set and MTA address has the correct type */
      if ( ( Xcp_MTA.IsSet == FALSE) || ( Xcp_MTA.Type == XCP_MTATYPE_COUNTER) )
      {
       /* Prepare error CTO with negative response XCP_ERR_ACCESS_DENIED */
       ResponseCTO.CTO[XCP_RES_PID_INDEX]     = XCP_ERR_PID;
       ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_DENIED;
       ResponseCTO.Length                     = XCP_ERR_ACCESS_DENIED_LENGTH;
      }
      else
      {
        /*  Get the data to be programmed  */
        P2VAR( uint8, AUTOMATIC, XCP_APPL_DATA ) DataPtr = &XCP_READ_CMDPACKET_BYTE(XCP_ADDRESS_GRANULARITY_SIZE);

        /* How many elements to program, i.e. maximum possible */
        Xcp_MemoryWrite.ElementsLeftToWrite = XCP_MAX_CTO_PGM - XCP_ADDRESS_GRANULARITY_SIZE;

        /* If the active programming flag is not yet set */
        if ((Xcp_CmdReqStatus & XCP_CMD_PROGRAMMING_ACTIVE_MASK) == 0U)
        {
          /* Enter critical section */
          SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

          /* Set the active programming of a segment */
          Xcp_CmdReqStatus |= XCP_CMD_PROGRAMMING_ACTIVE_MASK;

          /* Exit critical section */
          SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
        }

        /* Call the common programming function */
        Xcp_ProgramFlash( DataPtr, XCP_CMD_PROGRAM_MAX_PID );
      }
    }
  }

  /* Check if we need to send a response  */
  if (ResponseCTO.Length > 0U)
  {
    /* Enter critical section */
    SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* We got an error, programming of a segment is not active anymore. The user can retry
     * the whole sequence. */
    Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_PROGRAMMING_ACTIVE_MASK);

    /* Queue the response */
    Xcp_InsertResponseCTO(&ResponseCTO);

    /* Exit critical section */
    SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* Reset the globals used by any memory writing routine */
    Xcp_ResetMemoryWriteInfo();
  }

  DBG_XCP_PROCESSCMDPROGRAMMAX_EXIT(XCP_E_OK);

  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdProgramReset]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdProgramReset(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  /* Initialize response length */
  ResponseCTO.Length = 0U;

  DBG_XCP_PROCESSCMDPROGRAMRESET_ENTRY();

  /* If PGM resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_PGM_MASK) ==
             (uint8)XCP_RESOURCE_PGM_MASK
    )
  {
    /* PGM is protected, return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* If PROGRAM_START wasn't called before */
    if ((Xcp_CmdReqStatus & XCP_CMD_PROGRAMMING_STARTED_MASK) == 0U)
    {
      /* Fill in ERR_SEQUENCE message */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEQUENCE;
      ResponseCTO.Length = XCP_ERR_SEQUENCE_LENGTH;
    }
    else
    {
      /* PROGRAM_RESET behaves more or less equal to DISCONNECT */
      if (Xcp_ProcessCmdDisConnect() == E_OK)
      {
        /* Call callout function to trigger any application specific effects */
        Xcp_ApplProgramReset();
      }
    }
  }

  if (ResponseCTO.Length > 0U)
  {
    /* Enter critical section */
    SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* Enqueue response packet */
    Xcp_InsertResponseCTO(&ResponseCTO);

    /* Exit critical section */
    SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
  }

  DBG_XCP_PROCESSCMDPROGRAMRESET_EXIT(XCP_E_OK);

  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdProgram]----------------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdProgram(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDPROGRAM_ENTRY();

  /* Initialize the ResponseCTO length */
  ResponseCTO.Length = 0U;

  /* If PGM resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_PGM_MASK) ==
             (uint8)XCP_RESOURCE_PGM_MASK
    )
  {
    /* PGM is protected, return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* If PROGRAM_START wasn't called before */
    if ((Xcp_CmdReqStatus & XCP_CMD_PROGRAMMING_STARTED_MASK) == 0U)
    {
      /* Fill in ERR_SEQUENCE message */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEQUENCE;
      ResponseCTO.Length = XCP_ERR_SEQUENCE_LENGTH;
    }
    else
    {
      /* Check whether MTA address is set and MTA address has the correct type */
      if ( ( Xcp_MTA.IsSet == FALSE) || ( Xcp_MTA.Type == XCP_MTATYPE_COUNTER) )
      {
       /* Prepare error CTO with negative response XCP_ERR_ACCESS_DENIED */
       ResponseCTO.CTO[XCP_RES_PID_INDEX]     = XCP_ERR_PID;
       ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_DENIED;
       ResponseCTO.Length                     = XCP_ERR_ACCESS_DENIED_LENGTH;
      }
      else
      {
        /* Fetch the number of elements to be programmed */
        const uint8 NoOfDataElementsRequested = XCP_READ_CMDPACKET_BYTE(XCP_PGM_NO_OF_ELEMENTS_INDEX);

        /* If XCP_MAX_PROGRAM_ELEMENTS = 255, the requested number of data elements is always valid */
        #if (XCP_MAX_PROGRAM_ELEMENTS <  XCP_ABS_MAX_PROGRAM_ELEMENTS)
        /* Check that the requested number of data elements is valid */
        if (NoOfDataElementsRequested > XCP_MAX_PROGRAM_ELEMENTS)
        {

          /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
          ResponseCTO.CTO[XCP_RES_PID_INDEX]     = XCP_ERR_PID;
          ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
          ResponseCTO.Length                     = XCP_ERR_OUT_OF_RANGE_LENGTH;
        }
        else
        #endif
        {
          /* Set the pointer to where the data to be programmed is located */
          CONSTP2VAR( uint8, AUTOMATIC, XCP_APPL_DATA ) DataPtr =
              &XCP_READ_CMDPACKET_BYTE(XCP_DL_PGM_CMD_HEADER_LENGTH);

          /* Set the variable holding the number of elements to program with the requested number
           * found in the command */
          Xcp_MemoryWrite.ElementsLeftToWrite = NoOfDataElementsRequested;

          /* Enter critical section */
          SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

          /* Check that this is the PROGRAM with 0 as number of data elements */
          if (NoOfDataElementsRequested == 0U)
          {
            /* This marks the end of an active programming segment */
            Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_PROGRAMMING_ACTIVE_MASK);
          }
          else
          {
            /* If the active programming flag is not yet set */
            if ((Xcp_CmdReqStatus & XCP_CMD_PROGRAMMING_ACTIVE_MASK) == 0U)
            {
              /* Set the beginning of an active programming of a segment */
              Xcp_CmdReqStatus |= XCP_CMD_PROGRAMMING_ACTIVE_MASK;
            }
          }

          /* Exit critical section */
          SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

          /* Call the common programming function */
          Xcp_ProgramFlash( DataPtr, XCP_CMD_PROGRAM_PID );
        }
      }
    }
  }

  /* Check if we need to send a response  */
  if (ResponseCTO.Length > 0U)
  {
    /* Enter critical section */
    SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* Queue the response */
    Xcp_InsertResponseCTO(&ResponseCTO);

    /* We got an error, programming of a segment is not active anymore. The user can retry
     * the whole sequence. */
    Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_PROGRAMMING_ACTIVE_MASK);

#if (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON)
    /* This is the end of the block mode transfer */
    Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_PGM_BLOCK_MODE_MASK);
#endif

    /* Exit critical section */
    SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
  }

  DBG_XCP_PROCESSCMDPROGRAM_EXIT(XCP_E_OK);

  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdProgramNext]------------------------*/

#if (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON)
STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdProgramNext(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDPROGRAMNEXT_ENTRY();

  /* Initialize the length */
  ResponseCTO.Length = 0U;

  /* Check that a PROGRAM was issued beforehand */
  if ((Xcp_CmdReqStatus & XCP_CMD_PROGRAMMING_ACTIVE_MASK) == 0U)
  {
    /* PROGRAM_NEXT needs a PROGRAM to be called before it, so ERR_SEQUENCE will be returned. */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEQUENCE;
    ResponseCTO.Length = XCP_ERR_SEQUENCE_LENGTH;
  }
  else
  {
    /* Fetch the number of elements to be programmed */
    const uint8 NoOfDataElementsRequested = XCP_READ_CMDPACKET_BYTE(XCP_PGM_NO_OF_ELEMENTS_INDEX);

    /* Check that the number of elements left to program match the expected */
    if (Xcp_MemoryWrite.ElementsLeftToWrite != NoOfDataElementsRequested)
    {
      /* If the number of data elements does not match the expected value, the error code
       * ERR_SEQUENCE will be returned.
       * The negative response will contain the expected number of data elements. */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEQUENCE;
      ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = Xcp_MemoryWrite.ElementsLeftToWrite;
      ResponseCTO.Length = XCP_ERR_WRITE_SEQUENCE_LENGTH;
    }
    else
    {
      /* Set the pointer to where the data to be programmed is located */
      CONSTP2VAR( uint8, AUTOMATIC, XCP_APPL_DATA ) DataPtr =
          &XCP_READ_CMDPACKET_BYTE(XCP_DL_PGM_CMD_HEADER_LENGTH);

      /* Call the common programming function */
      Xcp_ProgramFlash(DataPtr , XCP_CMD_PROGRAM_NEXT_PID);
    }
  }

  /* If we have an error response registered */
  if (ResponseCTO.Length > 0U)
  {
    /* Enter critical section */
    SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* We got an error, programming of a segment is not active anymore. The user can retry
     * the whole sequence. */
    Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_PROGRAMMING_ACTIVE_MASK);

    /* This is the end of the block mode transfer */
    Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_PGM_BLOCK_MODE_MASK);

    /* Queue the response */
    Xcp_InsertResponseCTO(&ResponseCTO);

    /* Exit critical section */
    SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* Reset the globals used by any memory writing routine */
    Xcp_ResetMemoryWriteInfo();
  }

  DBG_XCP_PROCESSCMDPROGRAMNEXT_EXIT(XCP_E_OK);
  return XCP_E_OK;
}
#endif /* XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON */

/*--------------------[Xcp_ProgramFlash]---------------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_ProgramFlash
(
  P2CONST( uint8, AUTOMATIC, XCP_APPL_DATA ) DataPtr,
  uint8 CallingCmdPid
)
{
  /* The response the command which calls this function will send to the master */
  Xcp_CTOType ResponseCTO;

  /* the number of elements that will be processed */
  uint8 NumberOfElementsToProcessNow;

  /* The message returned by the callout function */
  Xcp_ApplReturnType CalloutRes = XCP_APPL_ERR_CMD_UNKNOWN;

  DBG_XCP_PROGRAMFLASH_ENTRY(DataPtr, CallingCmdPid);

  /* Initialize the length of our response with 0 as, in block mode, we musn't reply positively
   * unless this is the last programming command. */
  ResponseCTO.Length = 0U;

  if (CallingCmdPid == XCP_CMD_PROGRAM_MAX_PID)
  {
    /* With PROGRAM_MAX, the maximum possible number of elements are programmed. The maximum
     * for this command differs from the maximum possible of PROGRAM/_NEXT commands. */
    NumberOfElementsToProcessNow =  Xcp_MemoryWrite.ElementsLeftToWrite;
  }
  else
  {
    /* calculate number of elements to be processed now */
    if (Xcp_MemoryWrite.ElementsLeftToWrite >= XCP_MAX_PROGRAM_ELEMNTS_ONE_CMD)
    {
      /* process the maximum number of elements possible in this step */
      NumberOfElementsToProcessNow = XCP_MAX_PROGRAM_ELEMNTS_ONE_CMD;
    }
    else
    {
      /* process the last remaining elements or a single PROGRAM (no PROGRAM_NEXT afterwards)
       * with less then maximum elements that can be processed in one cycle */
      NumberOfElementsToProcessNow = Xcp_MemoryWrite.ElementsLeftToWrite;
    }
  }

  /* copy the elements in the internal buffer */
  TS_MemCpy
  (
    (&Xcp_MemoryWrite.Buffer[Xcp_MemoryWrite.ElementsWrittenSoFar * XCP_ADDRESS_GRANULARITY_SIZE]),
    DataPtr,
    ( (uint32)NumberOfElementsToProcessNow * XCP_ADDRESS_GRANULARITY_SIZE )
  );

  /* increment the number of elements processed so far */
  Xcp_MemoryWrite.ElementsWrittenSoFar += NumberOfElementsToProcessNow;

  /* decrement the number of elements remaining to be processed with the number of elements
   * processed in this step */
  Xcp_MemoryWrite.ElementsLeftToWrite -= NumberOfElementsToProcessNow;

  if (Xcp_MemoryWrite.ElementsLeftToWrite == 0U)
  {
    /* calculate the number of bytes to copy from the buffer to the intended memory location */
    uint16 NumberOfBytesToProgram = ( (uint16)Xcp_MemoryWrite.ElementsWrittenSoFar *
                                       XCP_ADDRESS_GRANULARITY_SIZE );

    /* reset the counter holding the number of elements programmed in this sequence */
    Xcp_MemoryWrite.ElementsWrittenSoFar = 0U;

    /* Call the callout function to handle the actual programming */
    CalloutRes = Xcp_ApplProgram( XCP_APPL_GET_ADDRESS(0U, Xcp_MTA.Address.Write),
                                  &(Xcp_MemoryWrite.Buffer[0U]),
                                  NumberOfBytesToProgram
                                );

    /* post-increment MTA address by the number of elements processed in this programming instance */
    Xcp_MTA.Address.Write = &Xcp_MTA.Address.Write[NumberOfBytesToProgram];

    /* Determine our response based on the callout response */
    switch( CalloutRes )
    {
      case XCP_APPL_OK:
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO.Length = XCP_CMD_PGM_RES_LENGTH;
        break;
      case XCP_APPL_ERR_OUT_OF_RANGE:
        /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
        ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
        break;
      case XCP_APPL_ERR_ACCESS_DENIED:
        /* Prepare error CTO with negative response ERR_ACCESS_DENIED */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_DENIED;
        ResponseCTO.Length = XCP_ERR_ACCESS_DENIED_LENGTH;
        break;
      case XCP_APPL_ERR_MEMORY_OVERFLOW:
        /* Prepare error CTO with negative response ERR_MEMORY_OVERFLOW */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_MEMORY_OVERFLOW;
        ResponseCTO.Length = XCP_ERR_MEMORY_OVERFLOW_LENGTH;
        break;
      default:
        /* EB specific handling: */
        /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
        ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
        break;
    }
  }

   /* Check if we need to send a response */
  if (ResponseCTO.Length > 0U)
  {
    /* Enter critical section */
    SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* Enqueue response packet */
    Xcp_InsertResponseCTO(&ResponseCTO);

    if (CalloutRes != XCP_APPL_OK)
    {
      /* We got an error, programming of a segment is not active anymore. The user can retry
       * the whole sequence. */
      Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_PROGRAMMING_ACTIVE_MASK);
    }

#if (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON)
    /* This is the end of the block mode transfer */
    Xcp_CmdReqStatus &= (uint8)(~XCP_CMD_PGM_BLOCK_MODE_MASK);
#endif

    /* Exit critical section */
    SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* Reset the globals used by any memory writing routine */
    Xcp_ResetMemoryWriteInfo();
  }

  DBG_XCP_PROGRAMFLASH_EXIT();
}

/*--------------------[Xcp_ProcessCmdProgramClear]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdProgramClear(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDPROGRAMCLEAR_ENTRY();

  /* If PGM resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_PGM_MASK) ==
             (uint8)XCP_RESOURCE_PGM_MASK
    )
  {
    /* PGM is protected, return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* If PROGRAM_START wasn't called before */
    if ((Xcp_CmdReqStatus & XCP_CMD_PROGRAMMING_STARTED_MASK) == 0U)
    {
      /* Fill in ERR_SEQUENCE message */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEQUENCE;
      ResponseCTO.Length = XCP_ERR_SEQUENCE_LENGTH;
    }
    else
    {
      /* Check whether MTA address is set and MTA address has the correct type */
      if ( ( Xcp_MTA.IsSet == FALSE) || ( Xcp_MTA.Type == XCP_MTATYPE_COUNTER) )
      {
        /* Prepare error CTO with negative response XCP_ERR_ACCESS_DENIED */
        ResponseCTO.CTO[XCP_RES_PID_INDEX]     = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_DENIED;
        ResponseCTO.Length                     = XCP_ERR_ACCESS_DENIED_LENGTH;
      }
      else
      {
        const uint8 Mode = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);

        if (Mode != XCP_PGM_ABSOLUTE_ACCESS_MODE)
        {
          /* We support only absolute access mode, prepare error CTO with negative response
           * ERR_OUT_OF_RANGE */
          ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
          ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
          ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
        }
        else
        {

          /* The message returned by the callout function */
          Xcp_ApplReturnType CalloutRes;

          /* The length of the memory part to be cleared */
          const uint32 ClearRange = Xcp_Uint32FromCmd(XCP_PGM_CLEAR_RANGE_INDEX);

          /* Call the callout function to do the actual work */
          CalloutRes = Xcp_ApplProgramClear( XCP_APPL_GET_ADDRESS( 0U, Xcp_MTA.Address.Write),
                                             ClearRange
                                           );

          /* Check the callout function's response and construct our message upon it */
          switch( CalloutRes )
          {
            case XCP_APPL_OK:
              /* Prepare CTO with positive response */
              ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
              ResponseCTO.Length = XCP_CMD_PGM_CLEAR_RES_LENGTH;
              break;
            case XCP_APPL_ERR_OUT_OF_RANGE:
              /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
              ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
              ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
              ResponseCTO.Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
              break;
            case XCP_APPL_ERR_ACCESS_DENIED:
              /* Prepare error CTO with negative response ERR_ACCESS_DENIED */
              ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
              ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_DENIED;
              ResponseCTO.Length = XCP_ERR_ACCESS_DENIED_LENGTH;
              break;
            default:
              /* EB specific handling: */
              /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
              ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
              ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
              ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
              break;
          }
        }
      }
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  DBG_XCP_PROCESSCMDPROGRAMCLEAR_EXIT(XCP_E_OK);

  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdProgramStart]-----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdProgramStart(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDPROGRAMSTART_ENTRY();

  /* If PGM resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & (uint8)XCP_RESOURCE_PGM_MASK) ==
             (uint8)XCP_RESOURCE_PGM_MASK
    )
  {
    /* PGM is protected, return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* If we have at least one DAQ list running, we must reject the programming start */
    if ((Xcp_Status.SessionStatus & XCP_MASK_DAQ_RUNNING ) != 0U )
    {
      /* Fill in ERR_DAQ_ACTIVE message */
      ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_DAQ_ACTIVE;
      ResponseCTO.Length = XCP_ERR_DAQ_ACTIVE_LENGTH;
    }
    else
    {
      /* The message returned by the callout function */
      Xcp_ApplReturnType CalloutRes;

      /* Variable to pass on the callout function to write the error code */
      uint16 ErrorCode = 0U;

      /* Call the callout function to do the actual work */
      CalloutRes = Xcp_ApplProgramStart( &ErrorCode );

      /* Check the callout function's response and construct our message upon it */
      switch( CalloutRes )
      {
        case XCP_APPL_OK:
          /* Prepare CTO with positive response */
          ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
          ResponseCTO.CTO[1U] = XCP_RESERVED_BYTE;
          ResponseCTO.CTO[2U] = XCP_COMM_MODE_PGM;
          ResponseCTO.CTO[3U] = XCP_MAX_CTO_PGM;
          ResponseCTO.CTO[4U] = XCP_MAX_BS_PGM;
          ResponseCTO.CTO[5U] = XCP_MIN_ST_PGM;
          ResponseCTO.CTO[6U] = 0U; /* No interleaved mode support */
          ResponseCTO.Length = XCP_CMD_PGM_START_RES_LENGTH;

          /* Enter critical section */
          SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

          /* Mark the programming in progress */
          Xcp_CmdReqStatus |= XCP_CMD_PROGRAMMING_STARTED_MASK;

          /* Exit critical section */
          SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

          break;
        case XCP_APPL_ERR_GENERIC:
          /* Prepare error CTO with negative response ERR_GENERIC */
          ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
          ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_GENERIC;
          Xcp_WriteWordToByteArray(&ResponseCTO.CTO[XCP_RES_ERRGENERIC_ADDERR_INDEX], ErrorCode);
          ResponseCTO.Length = XCP_ERR_GENERIC_LENGTH;
          break;
        default:
          /* EB specific handling: */
          /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
          ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
          ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
          ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
          break;
      }
    }
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  DBG_XCP_PROCESSCMDPROGRAMSTART_EXIT(XCP_E_OK);

  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdGetSectorInfo]-------------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetSectorInfo(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETSECTORINFO_ENTRY();

  /* Check if the Programming Resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & XCP_RESOURCE_PGM_MASK) ==
                 XCP_RESOURCE_PGM_MASK)
  {
    /* Programming commands are protected; return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Callout function return value */
    Xcp_ApplReturnType CalloutRes;
    /* GET_SECTOR_INFO: Mode */
    const uint8 Mode = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);
    /* GET_SECTOR_INFO: Sector Number */
    const uint8 Sector = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE2_INDEX);
    /* Array where the callout function return sector's name address */
    uint8 GetSectorInfoRespMta[XCP_MTA_ADDRESS_LENGTH];
    /* Array where the callout function return sector's information */
    uint8 GetSectorInfoRespData[XCP_CMD_SECTOR_INFO_MAX_RES_LENGTH - 1U];

    /* Call Xcp_ApplGetSectorInfo() callout function */
    CalloutRes = Xcp_ApplGetSectorInfo( Mode, Sector,
                                        GetSectorInfoRespMta,
                                        GetSectorInfoRespData );

    switch( CalloutRes )
    {
      case XCP_APPL_OK:
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        /* Check Mode parameter */
        if( ( Mode == 0U ) || ( Mode == 1U ) )
        { /* Mode = 0 or Mode = 1 */
          /* Copy 7-byte data containing sector information */
          TS_MemCpy( &ResponseCTO.CTO[XCP_RES_BYTE1_INDEX],
                     GetSectorInfoRespData,
                     (XCP_CMD_SECTOR_INFO_MAX_RES_LENGTH - 1U) );
          ResponseCTO.Length = XCP_CMD_SECTOR_INFO_MAX_RES_LENGTH;
        }
        else
        { /* Mode = 2 */
          /* Copy the sector name length information */
          ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] =
              GetSectorInfoRespData[XCP_SECTOR_INFO_NAME_BYTE_POS];
          ResponseCTO.Length = XCP_CMD_SECTOR_INFO_MIN_RES_LENGTH;

          /* set MTA to the Sector's name address, if available */
          if(GetSectorInfoRespData[XCP_SECTOR_INFO_NAME_BYTE_POS] > 0U)
          {
            /* If address granularity is 4 bytes */
            Xcp_MTA.Type = XCP_MTATYPE_ADDRESS;

            /* set MTA to the Sector's name address */
            TS_MemCpy( &Xcp_MTA.Address.Read,
                       GetSectorInfoRespMta,
                       XCP_MTA_ADDRESS_LENGTH);

            /* Mark the MTA as being set */
            Xcp_MTA.IsSet = TRUE;
          }
        }
        break;
      case XCP_APPL_ERR_MODE_NOT_VALID:
        /* Prepare error CTO with negative response ERR_MODE_NOT_VALID */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_MODE_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_MODE_NOT_VALID_LENGTH;
        break;
      case XCP_APPL_ERR_SEGMENT_NOT_VALID:
        /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEGMENT_NOT_VALID;
        ResponseCTO.Length = XCP_ERR_SEGMENT_NOT_VALID_LENGTH;
        break;
      default:
        /* EB specific handling: */
        /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
        ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
        break;
    } /* switch( CalloutRes ) */
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETSECTORINFO_EXIT(XCP_E_OK);

  return XCP_E_OK;
}

/*--------------------[Xcp_ProcessCmdGetPgmProcInfo]----------------------*/

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdGetPgmProcInfo(void)
{
  /* Local variable to prepare response */
  Xcp_CTOType ResponseCTO;

  DBG_XCP_PROCESSCMDGETPGMPROCINFO_ENTRY();

  /* Check if the Programming Resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & XCP_RESOURCE_PGM_MASK) ==
                 XCP_RESOURCE_PGM_MASK)
  {
    /* Programming commands are protected; return ERR_ACCESS_LOCKED */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO.Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Callout function return value */
    Xcp_ApplReturnType CalloutRes;
    /* Total number of available sectors */
    uint8 MaxSector;
    /* General properties for programming */
    uint8 PgmProperties;

    /* Call Xcp_ApplGetPgmProcessorInfo() callout function */
    CalloutRes = Xcp_ApplGetPgmProcessorInfo( &MaxSector, &PgmProperties );

    /* Check the callout function return */
    switch( CalloutRes )
    {
      case XCP_APPL_OK:
        /* Prepare CTO with positive response */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO.CTO[XCP_RES_BYTE1_INDEX] = XCP_GET_PGM_PROPERTIES(PgmProperties);
        ResponseCTO.CTO[XCP_RES_BYTE2_INDEX] = MaxSector;
        ResponseCTO.Length = XCP_CMD_PGM_PROC_INFO_RES_LENGTH;
        break;
      default:
        /* EB specific handling: */
        /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
        ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
        ResponseCTO.Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
        break;
    } /* switch( CalloutRes ) */
  }

  /* Enter critical section */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Enqueue response packet */
  Xcp_InsertResponseCTO(&ResponseCTO);

  /* Exit critical section */
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_PROCESSCMDGETPGMPROCINFO_EXIT(XCP_E_OK);

  return XCP_E_OK;
}


#endif /* XCP_PGM_SUPPORTED == STD_ON */

/*--------------------[Xcp_ProcessCmdSetRequest]----------------------*/
#if ((XCP_STORE_DAQ_SUPPORTED == STD_ON) || \
     (XCP_CAL_PAG_STORE_SUPPORTED == STD_ON))

STATIC FUNC(Xcp_ErrorType, XCP_CODE) Xcp_ProcessCmdSetRequest(void)
{
  /* Local variable to prepare the response */
  Xcp_CTOType ResponseCTO;
  /* Local variable to hold requested mode */
  uint8 RequestedMode;

  DBG_XCP_PROCESSCMDSETREQUEST_ENTRY();
  /* Initialize response CTO length */
  ResponseCTO.Length = 0U;

  /* Retrieve the requested mode */
  RequestedMode = XCP_READ_CMDPACKET_BYTE(XCP_CMD_BYTE1_INDEX);

  /* Check if a NvM storage is currently pending */
  if ((Xcp_Status.SessionStatus &
      ((XCP_MASK_CLEAR_DAQ_REQ | (XCP_MASK_STORE_DAQ_REQ | XCP_MASK_STORE_CAL_REQ)))) != 0U)
  {
    /* The master must wait for the current request to finish, issue ERR_RES_TEMP_NOT_A. */
    ResponseCTO.CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_RES_TEMP_NOT_A;
    ResponseCTO.Length = XCP_ERR_RES_TEMP_NOT_A_LENGTH;
  }
  else
  {
    switch (RequestedMode)
    {
#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
      case XCP_MASK_STORE_DAQ_REQ_RESUME:
      case XCP_MASK_STORE_DAQ_REQ_NORESUME:
        /* Request for write the DAQ lists */
        Xcp_StartStoreDaq(RequestedMode, &ResponseCTO);
        break;
      case XCP_MASK_CLEAR_DAQ_REQ:
        /* Request for write/clear the DAQ lists */
        Xcp_StartClearDaq(&ResponseCTO);
        break;
#endif /* XCP_STORE_DAQ_SUPPORTED == STD_ON */
#if ( XCP_CAL_PAG_STORE_SUPPORTED == TRUE)
      case XCP_MASK_STORE_CAL_REQ:
        /* Request for write the calibration/paging data */
        Xcp_StartStoreCal(&ResponseCTO);
        break;
#endif /* XCP_CAL_PAG_STORE_SUPPORTED == TRUE */
      default:
        /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
        ResponseCTO.CTO[XCP_RES_PID_INDEX]     = XCP_ERR_PID;
        ResponseCTO.CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
        ResponseCTO.Length                     = XCP_ERR_OUT_OF_RANGE_LENGTH;
        break;
    }
    /* Deviation MISRA-4 */
  }
  /* If we have an answer to send */
  if (ResponseCTO.Length > 0U)
  {
    /* Enter critical section */
    SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    /* Enqueue response packet. */
    Xcp_InsertResponseCTO(&ResponseCTO);

    /* Exit critical section */
    SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
  }

  DBG_XCP_PROCESSCMDSETREQUEST_EXIT(XCP_E_OK);
  return XCP_E_OK;
}
#endif /* (XCP_STORE_DAQ_SUPPORTED == STD_ON || XCP_CAL_PAG_STORE_SUPPORTED == STD_ON) */

/*--------------------------[Xcp_StartStoreDaq]-----------------------------*/

#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
STATIC FUNC(void, XCP_CODE) Xcp_StartStoreDaq
(
  uint8 RequestedMode,
  P2VAR(Xcp_CTOType, AUTOMATIC, XCP_APPL_DATA) ResponseCTO
)
{
  /* The Session Configuration Id from the SET_REQUEST command */
  uint16 SessionConfigId = Xcp_Uint16FromCmd(XCP_CMD_BYTE2_INDEX);

  DBG_XCP_STARTSTOREDAQ_ENTRY(RequestedMode,ResponseCTO);
  /* If the Session Configuration ID is 0, reject the store request */
  if (SessionConfigId == 0U)
  {
    /* Fill in ERR_CMD_SYNTAX message */
    ResponseCTO->CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_SYNTAX;
    ResponseCTO->Length = XCP_ERR_CMD_SYNTAX_LENGTH;
  }
  else
  {
    /* If we have at least a DAQ list running, we must reject the store request */
    if ((Xcp_Status.SessionStatus & XCP_MASK_DAQ_RUNNING ) != 0U )
    {
      /* Fill in ERR_DAQ_ACTIVE message */
      ResponseCTO->CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_DAQ_ACTIVE;
      ResponseCTO->Length = XCP_ERR_DAQ_ACTIVE_LENGTH;
    }
    else
    {
      /* Iterator */
      uint16 i;
      /* Check that at least one DAQ list was selected */
      boolean AtLeastOneDaqListIsSelected = FALSE;

      /* Clear any previous selected DAQ lists from the ADMIN array */
      for (i = 0U; i< XCP_NUMBER_OF_BYTES_SELECTED_DAQ; i++)
      {
        Xcp_DaqLists.Xcp_StoreDaq.SelectedDAQ[i] = 0U;
      }

      /* The mode must be also saved in the NV memory in order to determine if this is the RESUME
       * mode or not upon slave restart. */
      Xcp_DaqLists.Xcp_StoreDaq.Mode = RequestedMode;

#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK)
      /* Save the number of dynamically configured DAQ lists */
      Xcp_DaqLists.Xcp_StoreDaq.NoOfConfiguredDynamicDaqLists = Xcp_DynamicInfo.NoOfConfiguredDynamicDAQLists;
#endif

      /* For all selected DAQ lists - set the appropriate selected flag in the store DAQ admin
       * structure while clearing the one in the DAQ list's mode (retrieved by GET_DAQ_LIST_MODE) */
      for (i = 0U; i< XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(uint16); i++)
      {
        /* Get the DAQ list */
        P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr = XCP_GET_DAQ_LIST_PTR(i);
        /* If the DAQ List is selected */
        if ((DaqListPtr->Mode & XCP_MASK_DAQLIST_SELECTED) != 0U)
        {
          /* The byte location of this DAQ list in the selected DAQ array */
          uint16 ByteLocation = i/XCP_NUMBER_OF_BITS_IN_BYTE;
          /* The bit corresponding to this DAQ list in the ByteLocation byte */
          uint8 BitOffset = (uint8)(i%XCP_NUMBER_OF_BITS_IN_BYTE);

          /* Save the selected flag in the NV memory via the store DAQ admin block */
          Xcp_DaqLists.Xcp_StoreDaq.SelectedDAQ[ByteLocation] |= (uint8)(1U<<BitOffset);

          /* Clear the selected flag from the DAQ's mode */
          DaqListPtr->Mode &= (uint8)(~XCP_MASK_DAQLIST_SELECTED);

          if (RequestedMode == XCP_MASK_STORE_DAQ_REQ_RESUME)
          {
            /* Set the resume flag for the DAQ list */
            DaqListPtr->Mode |= XCP_MASK_DAQLIST_RESUME;
          }
          AtLeastOneDaqListIsSelected = TRUE;
        }
      }
      /* If not a single DAQ list was selected, reject with ERR_SEQUENCE message */
      if (AtLeastOneDaqListIsSelected == FALSE)
      {
        /* Fill in ERR_SEQUENCE message */
        ResponseCTO->CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_SEQUENCE;
        ResponseCTO->Length = XCP_ERR_SEQUENCE_LENGTH;
      }
      else
      {
        /* Proceed if the write request is accepted by the NvM. We give a NULL_PTR as the source pointer
         * as the block should have a permanent RAM address assigned to it  */
        if (NvM_WriteBlock(XCP_STORE_DAQ_NVM_BLOCK_ID, NULL_PTR) == E_OK)
        {
          /* Set the Store DAQ pending flag in the session status*/
          Xcp_Status.SessionStatus |= XCP_MASK_STORE_DAQ_REQ;
          /* Save the Configuration Session ID */
          Xcp_DaqLists.Xcp_StoreDaq.ConfigurationSessionID = SessionConfigId;

          /* Fill in positive response */
          ResponseCTO->CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
          ResponseCTO->Length = XCP_SET_REQUEST_POSITIVE_LENGTH;
        }
        else
        {
          /* NvM has rejected the request : send error message RESOURCE_TEMPORARY_NOT_ACCESSIBLE */
          ResponseCTO->CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
          ResponseCTO->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_RES_TEMP_NOT_A;
          ResponseCTO->Length = XCP_ERR_RES_TEMP_NOT_A_LENGTH;

          /* We must restore the selected status and the resume mode (if needed) for the selected
           * DAQ lists */
          for (i = 0U; i< XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(uint16); i++)
          {
            /* Get the DAQ list */
            P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr = XCP_GET_DAQ_LIST_PTR(i);
            /* The byte location of this DAQ list in the selected DAQ array */
            uint16 ByteLocation = i/XCP_NUMBER_OF_BITS_IN_BYTE;
            /* The bit corresponding to this DAQ list in the ByteLocation byte */
            uint8 BitOffset = (uint8)(i%XCP_NUMBER_OF_BITS_IN_BYTE);

            /* If a DAQ List is selected to be stored mode */
            if ((Xcp_DaqLists.Xcp_StoreDaq.SelectedDAQ[ByteLocation] & (uint8)(1U << BitOffset)) != 0U )
            {
              /* Set the selected flag from the DAQ's mode */
              DaqListPtr->Mode |= XCP_MASK_DAQLIST_SELECTED;

              if (RequestedMode == XCP_MASK_STORE_DAQ_REQ_RESUME)
              {
                /* Clear the resume flag for the DAQ list */
                DaqListPtr->Mode &= (uint8)(~XCP_MASK_DAQLIST_RESUME);
              }
            }
          }
        }
      }
    }
  }

  DBG_XCP_STARTSTOREDAQ_EXIT(RequestedMode,ResponseCTO);
}

/*--------------------------[Xcp_StartClearDaq]-----------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_StartClearDaq
(
  P2VAR(Xcp_CTOType, AUTOMATIC, XCP_APPL_DATA) ResponseCTO
)
{
  DBG_XCP_STARTCLEARDAQ_ENTRY(ResponseCTO);

  /* If we have at least a DAQ list running, we must reject the clear request */
  if ((Xcp_Status.SessionStatus & XCP_MASK_DAQ_RUNNING ) != 0U )
  {
    /* Fill in ERR_DAQ_ACTIVE message */
    ResponseCTO->CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_DAQ_ACTIVE;
    ResponseCTO->Length = XCP_ERR_DAQ_ACTIVE_LENGTH;
  }
  else
  {
    /* The Session Configuration Id is saved if restore is needed later on */
    uint16 SavedSessionConfigId = Xcp_DaqLists.Xcp_StoreDaq.ConfigurationSessionID;
    /* Clear the Session Configuration ID */
    Xcp_DaqLists.Xcp_StoreDaq.ConfigurationSessionID = 0U;
    /* The mode must be also saved in the NV memory */
    Xcp_DaqLists.Xcp_StoreDaq.Mode = XCP_MASK_CLEAR_DAQ_REQ;

    /* Register the write request */
    if (NvM_WriteBlock(XCP_STORE_DAQ_NVM_BLOCK_ID, NULL_PTR) == E_OK)
    {
      /* Set the clear DAQ pending flag in the session status bit field as this */
      Xcp_Status.SessionStatus |= XCP_MASK_CLEAR_DAQ_REQ;

      /* Fill in positive response */
      ResponseCTO->CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
      ResponseCTO->Length = XCP_SET_REQUEST_POSITIVE_LENGTH;
    }
    else
    {
      /* Restore the Session Configuration Id */
      Xcp_DaqLists.Xcp_StoreDaq.ConfigurationSessionID = SavedSessionConfigId;
      /* Restore the Clear mode */
      Xcp_DaqLists.Xcp_StoreDaq.Mode &= (uint8)(~XCP_MASK_CLEAR_DAQ_REQ);

      /* NvM has rejected the request : send error message */
      ResponseCTO->CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
      ResponseCTO->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_RES_TEMP_NOT_A;
      ResponseCTO->Length = XCP_ERR_RES_TEMP_NOT_A_LENGTH;
    }
  }

  DBG_XCP_STARTCLEARDAQ_EXIT(ResponseCTO);
}

#endif /* XCP_STORE_DAQ_SUPPORTED == STD_ON */

#if ( XCP_CAL_PAG_STORE_SUPPORTED == TRUE)
/*--------------------------[Xcp_StartStoreCal]-----------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_StartStoreCal
(
  P2VAR(Xcp_CTOType, AUTOMATIC, XCP_APPL_DATA) ResponseCTO
)
{
  DBG_XCP_STARTSTORECAL_ENTRY(ResponseCTO);

  /* Check if the Calibration/Paging Resource is protected */
  if((XCP_GET_RESOURCE_PROTECTION_STATUS() & XCP_RESOURCE_CAL_PAG_MASK) ==
                 XCP_RESOURCE_CAL_PAG_MASK)
  {
    /* Calibration commands are protected; return ERR_ACCESS_LOCKED */
    ResponseCTO->CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
    ResponseCTO->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_ACCESS_LOCKED;
    ResponseCTO->Length = XCP_ERR_ACCESS_LOCKED_LENGTH;
  }
  else
  {
    /* Callout function return value */
    Xcp_ApplReturnType CalloutRes;
    /* Call Xcp_ApplSetReqStoreCalReq() callout function */
    CalloutRes = Xcp_ApplSetReqStoreCalReq( );

    switch( CalloutRes )
    {
      case XCP_APPL_OK:
        /* Set STORE_CAL_REQ mode parameter */
        XCP_SET_STORE_CAL_REQ();
        /* Prepare CTO with positive response */
        ResponseCTO->CTO[XCP_RES_PID_INDEX] = XCP_RES_PID;
        ResponseCTO->Length = XCP_CMD_SET_REQUEST_RES_LENGTH;
        break;
      case XCP_APPL_ERR_OUT_OF_RANGE:
        /* Prepare error CTO with negative response ERR_OUT_OF_RANGE */
        ResponseCTO->CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_OUT_OF_RANGE;
        ResponseCTO->Length = XCP_ERR_OUT_OF_RANGE_LENGTH;
        break;
      default:
        /* EB specific handling: */
        /* Prepare error CTO with negative response ERR_CMD_UNKNOWN */
        ResponseCTO->CTO[XCP_RES_PID_INDEX] = XCP_ERR_PID;
        ResponseCTO->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
        ResponseCTO->Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
        break;
    } /* switch( CalloutRes ) */
  }

  DBG_XCP_STARTSTORECAL_EXIT(ResponseCTO);
}

#endif /* XCP_CAL_PAG_STORE_SUPPORTED == TRUE */

/*--------------------------[Xcp_AbortCommandWithError]-----------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_AbortCommandWithError
(
   P2VAR(Xcp_CTOType, AUTOMATIC, XCP_APPL_DATA)  ErrResponseCTOPtr
)
{
  DBG_XCP_ABORTCOMMANDWITHERROR_ENTRY(ErrorId, ErrorLength);

  /* Enter critical section.*/
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  if (Xcp_State == XCP_STATE_CONNECTED)
  {
    /* Local variable read available space in CTO Queue */
    uint16 SpaceAvailable = XCP_GET_CTOQUEUE_FREESPACE(uint16);

    /* verify available space in CTO queue */
    if (SpaceAvailable >= ErrResponseCTOPtr->Length)
    {
      /* XCP Insert CTO */
      Xcp_InsertResponseCTO(ErrResponseCTOPtr);
    }
  }
  /* CHECK: NOPARSE */
  else
  {
    /* Unreachable code due to previous conditions */
    XCP_UNREACHABLE_CODE_ASSERT(XCP_SID_MAIN_FUNCTION);
  }
  /* CHECK: PARSE */

  /* Dequeue the command */
  Xcp_DeQueueCmdQueue();
  /* Trigger state transition */
  XCP_CMD_PROCESSOR_STATE_TRANS(XCP_CMDPROCESSOR_IDLE);

  /* Exit critical section.*/
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  DBG_XCP_ABORTCOMMANDWITHERROR_EXIT();
}

/*--------------------------[Xcp_CommandCanBeExecuted]-----------------------------*/

STATIC FUNC(boolean, XCP_CODE) Xcp_CommandCanBeExecuted
(
  uint8 CommandId,
  P2VAR(Xcp_CTOType, AUTOMATIC, XCP_APPL_DATA)  ErrResponseCTOPtr
)
{
  boolean CommandIsExecutable = TRUE;

  DBG_XCP_COMMANDCANBEEXECUTED_ENTRY(CommandId, ErrorMessage);

  /* Check whether command is available */
  if (XCP_IS_COMMAND_EXECUTABLE(CommandId) == FALSE)
  {
    /* Set the error message */
    ErrResponseCTOPtr->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_UNKNOWN;
    ErrResponseCTOPtr->Length = XCP_ERR_CMD_UNKNOWN_LENGTH;
    /* The command must not be executed */
    CommandIsExecutable = FALSE;
  }
  else
  {
#if (XCP_PGM_SUPPORTED == STD_ON)
     /* During an active programming session or the programming of a segment is active, execute only
      * allowed commands */
     if (((COMMAND_CAN_EXECUTE_PGM_START(CommandId) == FALSE) &&
          ((Xcp_CmdReqStatus & XCP_CMD_PROGRAMMING_STARTED_MASK) == XCP_CMD_PROGRAMMING_STARTED_MASK)) ||
         ((COMMAND_CAN_EXECUTE_PGM_ACTIVE(CommandId) == FALSE) &&
          ((Xcp_CmdReqStatus & XCP_CMD_PROGRAMMING_ACTIVE_MASK) == XCP_CMD_PROGRAMMING_ACTIVE_MASK))
         )
     {
       /* Set the error message */
       ErrResponseCTOPtr->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_PGM_ACTIVE;
       ErrResponseCTOPtr->Length = XCP_ERR_PGM_ACTIVE_LENGTH;
       /* The command must not be executed */
       CommandIsExecutable = FALSE;
     }
 #endif
 #if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
     if (CommandIsExecutable)
     {
       /* Deviation MISRA-2 <START> */
       /* If the command can modify a DAQ list runtime information (anything from Xcp_DaqLists) and
        *  currently a request to STORE/CLEAR the DAQ lists in NV memory is pending, the commands must
        *  be replied with XCP_ERR_CMD_BUSY message and the command shall not be executed. */
       if ( (COMMAND_CAN_MODIFY_DAQ_LIST(CommandId) == TRUE) &&
            (((Xcp_Status.SessionStatus & XCP_MASK_CLEAR_DAQ_REQ) != 0U ) ||
            ((Xcp_Status.SessionStatus & XCP_MASK_STORE_DAQ_REQ) != 0U )))
       /* Deviation MISRA-2 <STOP> */
       {
         /* Set the error message */
         ErrResponseCTOPtr->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_BUSY;
         ErrResponseCTOPtr->Length = XCP_ERR_CMD_BUSY_LENGTH;
         /* The command must not be executed */
         CommandIsExecutable = FALSE;
       }
       /* Reject the flash programming start if we are currently writing in the NV memory. */
       if ( (CommandId == XCP_CMD_PROGRAM_START_PID) &&
            (((Xcp_Status.SessionStatus & XCP_MASK_CLEAR_DAQ_REQ) != 0U ) ||
            ((Xcp_Status.SessionStatus & XCP_MASK_STORE_DAQ_REQ) != 0U )))
       {
         /* Set the error message */
         ErrResponseCTOPtr->CTO[XCP_RES_ERRCODE_INDEX] = XCP_ERR_CMD_BUSY;
         ErrResponseCTOPtr->Length = XCP_ERR_CMD_BUSY_LENGTH;
         /* The command must not be executed */
         CommandIsExecutable = FALSE;
       }
     }
 #endif /* XCP_STORE_DAQ_SUPPORTED == STD_ON */
  }

  DBG_XCP_COMMANDCANBEEXECUTED_EXIT(CommandId, ErrorMessage, CommandIsExecutable);
  return CommandIsExecutable;
}

/*-----------------------[Xcp_ResetMemoryWriteInfo]--------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_ResetMemoryWriteInfo(void)
{
  DBG_XCP_RESETMEMORYWRITEINFO_ENTRY();

  Xcp_MemoryWrite.ElementsLeftToWrite = 0U;
  Xcp_MemoryWrite.ElementsWrittenSoFar = 0U;

  DBG_XCP_RESETMEMORYWRITEINFO_EXIT();
}

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

/*==================[end of file]===========================================*/
