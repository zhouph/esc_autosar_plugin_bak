/**
 * \file
 *
 * \brief AUTOSAR BswM
 *
 * This file contains the implementation of the AUTOSAR
 * module BswM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined BSWM_LINIF_H)
#define BSWM_LINIF_H

/* !LINKSTO     BswM0026,1
 * !description Definition of the BswM_LinIf.h for the LinIf.
 */

#include <BswM_Cfg.h>    /* Needed for macro resolution */

#if(BSWM_LINTP_API_ENABLED == STD_ON)
/*==================[includes]===============================================*/

#include <BswM.h>   /* Module public API         */
#include <LinIf.h>  /* LinTp API                 */

/*==================[macros]=================================================*/

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

#define BSWM_START_SEC_CODE
#include <MemMap.h>

/** \brief Requests a mode for LIN channel (Called by LinTp).
 **
 ** This function is called by LinTp to request a mode for the corresponding
 ** LIN channel. The LinTp_Mode mainly correlates to the LIN schedule table
 ** that should be used .
 **
 ** Precondition: None
 **
 ** \param[in]    Network            - The LIN channel that the LinTp mode
                                       request relates to.
 **               LinTpRequestedMode - The requested LIN TP mode.
 ** \param[inout] None.
 ** \param[out]   None.
 **
 ** \ServiceID{0x0B}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous} */

extern FUNC(void, BSWM_CODE) BswM_LinTp_RequestMode
(
  NetworkHandleType Network,
  LinTp_Mode LinTpRequestedMode
);

#define BSWM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/
#endif /* (BSWM_LINTP_API_ENABLED == STD_ON) */

#endif /* if !defined( BSWM_LINIF_H ) */
/*==================[end of file]============================================*/
