/**
 * \file
 *
 * \brief AUTOSAR BswM
 *
 * This file contains the implementation of the AUTOSAR
 * module BswM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined BSWM_ETHSM_H)
#define BSWM_ETHSM_H

/* !LINKSTO     BswM0026,1
 * !description Definition of the BswM_EthSM.h for the EthSM.
 */

#include <BswM_Cfg.h>   /* Needed for macro resolution */

#if (BSWM_ETHSM_API_ENABLED == STD_ON)

/*==================[includes]===============================================*/

#include <BswM.h>              /* Module public API         */
#include <EthSM_Types.h>       /* EthSM API                 */
/*==================[macros]=================================================*/

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/


#define BSWM_START_SEC_CODE
#include <MemMap.h>


/** \brief Indicates current state (Called by EthSM).
 **
 ** This function is called by EthSM to indicate its current state.
 **
 ** Precondition: None
 **
 ** \param[in]    Network      - The Ethernet channel that the indicated state
 **                              corresponds to.
 **               CurrentState - The current state of the Ethernet channel.
 ** \param[inout] None.
 ** \param[out]   None.
 **
 ** \ServiceID{0x0D}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous} */

extern FUNC(void, BSWM_CODE) BswM_EthSM_CurrentState
(
  NetworkHandleType Network,
  EthSM_NetworkModeStateType CurrentState
);

#define BSWM_STOP_SEC_CODE
#include <MemMap.h>


/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/
#endif /* (BSWM_ETHSM_API_ENABLED == STD_ON) */

#endif /* if !defined( BSWM_ETHSM_H ) */
/*==================[end of file]============================================*/
