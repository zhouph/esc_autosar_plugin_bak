/**
 * \file
 *
 * \brief AUTOSAR BswM
 *
 * This file contains the implementation of the AUTOSAR
 * module BswM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined BSWM_ECUM_H)
#define BSWM_ECUM_H

/* !LINKSTO     BswM0026,1
 * !description Definition of the BswM_EcuM.h for the EcuM.
 */

 /*==================[includes]===============================================*/

#include <BswM_Cfg.h>   /* Needed for macro resolution */

#if (BSWM_ECUM_API_ENABLED)

#include <EcuM.h>   /* EcuM types */
#include <BswM.h>   /* Module public API         */

/*==================[macros]=================================================*/

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

#define BSWM_START_SEC_CODE
#include <MemMap.h>


/** \brief Indicates the current ECU Operation Mode (Called by EcuM).
 **
 ** This function is called by EcuM to indicate the current ECU Operation Mode.
 **
 ** Precondition: None
 **
 ** \param[in]    CurrentState - The requested ECU Operation Mode.
 ** \param[inout] None.
 ** \param[out]   None.
 **
 ** \ServiceID{0x0F}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous} */
extern FUNC(void, BSWM_CODE) BswM_EcuM_CurrentState
(
  EcuM_StateType CurrentState
);

/** \brief Indicates the current state of a wakeup source (Called by EcuM).
 **
 ** This function is called by EcuM to indicate the current state of a wakeup source.
 **
 ** Precondition: None
 **
 ** \param[in]    source - Wakeup source(s) that changed state.
 **               state  - The new state of the wakeup source(s) .
 ** \param[inout] None.
 ** \param[out]   None.
 **
 ** \ServiceID{0x10}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous} */
extern FUNC(void, BSWM_CODE) BswM_EcuM_CurrentWakeup
(
  EcuM_WakeupSourceType source,
  EcuM_WakeupStatusType state
);


#define BSWM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

#endif /* (BSWM_ECUM_API_ENABLED) */

#endif /* if !defined( BSWM_ECUM_H ) */
/*==================[end of file]============================================*/
