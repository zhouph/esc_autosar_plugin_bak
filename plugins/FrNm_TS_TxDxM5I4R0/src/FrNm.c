/**
 * \file
 *
 * \brief AUTOSAR FrNm
 *
 * This file contains the implementation of the AUTOSAR
 * module FrNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
/* !LINKSTO FRNM064_Refine,1 */

/*  MISRA-C:2004 Deviation List
 *
 *  MISRA-1) Deviated Rule: 16.7 (advisory)
 *  A pointer parameter in a function prototype should be declared as pointer to const
 *  if the pointer is not used to modify the addressed object.
 *
 *   Reason:
 *   There is no need to change the function argument because it's part of
 *   an public API and we will loose the consistency with the same API 
 *   generated when FRNM_PASSIVE_MODE_ENABLED == STD_ON.
 */

/*==================[includes]===========================================*/
#include <FrNm_Trace.h>
#include <Std_Types.h>         /* AUTOSAR standard types */
/* !LINKSTO FRNM065,1 */
#include <FrNm.h>              /* FrNm API definitions (own interface) */
#include <FrNm_Int.h>          /* FrNm internal interface */
/* !LINKSTO FRNM065,1 */
#include <FrNm_Cbk.h>          /* FRNM callback API (own interface) */
#include <FrNm_Hsm.h>          /* public API of FrNm_Hsm.c */
#include <FrNm_HsmFrNm.h>      /* public statechart model definitions, */
/* !LINKSTO FRNM066_SchM,1 */
#include <SchM_FrNm.h>         /* SchM-header for FrNm */
/* !LINKSTO FRNM066_Nm_Cbk,1 */
#include <Nm_Cbk.h>            /* Nm callback API */
/* !LINKSTO FRNM066_FrIf,1 */
#include <FrIf.h>              /* FrIf API */
#include <TSMem.h>

#if (FRNM_DEV_ERROR_DETECT == STD_ON)
/* Development Error Tracer (FRNM_DET_REPORT_ERROR() macros) */
/* !LINKSTO FRNM066_Det,1 */
#include <Det.h>
#endif

#if ((FRNM_COM_USER_DATA_SUPPORT == STD_ON) || (FRNM_PN_EIRA_CALC_ENABLED == STD_ON))
/* !LINKSTO FRNM066_PduR,1 */
#include <PduR_FrNm.h>         /* PduR API for FrNm */
#endif

/*==================[macro definitions]==================================*/
/* !LINKSTO FrNm.Version.Check,1 */
/*------------------[AUTOSAR vendor identification check]-------------------*/

#if (!defined FRNM_VENDOR_ID) /* configuration check */
#error FRNM_VENDOR_ID must be defined
#endif

#if (FRNM_VENDOR_ID != 1U) /* vendor check */
#error FRNM_VENDOR_ID has wrong vendor id
#endif

/*------------------[AUTOSAR release version identification check]----------*/

#if (!defined FRNM_AR_RELEASE_MAJOR_VERSION) /* configuration check */
#error FRNM_AR_RELEASE_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (FRNM_AR_RELEASE_MAJOR_VERSION != 4U)
#error FRNM_AR_RELEASE_MAJOR_VERSION wrong (!= 4U)
#endif

#if (!defined FRNM_AR_RELEASE_MINOR_VERSION) /* configuration check */
#error FRNM_AR_RELEASE_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (FRNM_AR_RELEASE_MINOR_VERSION != 0U )
#error FRNM_AR_RELEASE_MINOR_VERSION wrong (!= 0U)
#endif

#if (!defined FRNM_AR_RELEASE_REVISION_VERSION) /* configuration check */
#error FRNM_AR_RELEASE_REVISION_VERSION must be defined
#endif

/* revision version check */
#if (FRNM_AR_RELEASE_REVISION_VERSION != 3U )
#error FRNM_AR_RELEASE_REVISION_VERSION wrong (!= 3U)
#endif

/*------------------[AUTOSAR module version identification check]-----------*/

#if (!defined FRNM_SW_MAJOR_VERSION) /* configuration check */
#error FRNM_SW_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (FRNM_SW_MAJOR_VERSION != 5U)
#error FRNM_SW_MAJOR_VERSION wrong (!= 5U)
#endif

#if (!defined FRNM_SW_MINOR_VERSION) /* configuration check */
#error FRNM_SW_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (FRNM_SW_MINOR_VERSION < 4U)
#error FRNM_SW_MINOR_VERSION wrong (< 4U)
#endif

#if (!defined FRNM_SW_PATCH_VERSION) /* configuration check */
#error FRNM_SW_PATCH_VERSION must be defined
#endif

/* patch version check */
#if (FRNM_SW_PATCH_VERSION < 12U)
#error FRNM_SW_PATCH_VERSION wrong (< 12U)
#endif

/*------------------[module internal macros]-----------------------------*/

#if (FRNM_DEV_ERROR_DETECT == STD_ON)
/** \brief MACRO for DET reporting
 */
#define FRNM_DET_REPORT_ERROR(InstanceId, ServiceId, ErrorCode)          \
   (void)Det_ReportError(FRNM_MODULE_ID, (InstanceId), (ServiceId), (ErrorCode))
#else
#define FRNM_DET_REPORT_ERROR(InstanceId, ServiceId, ErrorCode)
#endif

/* Macro for better readability since event emitting is done at many places */
#define FRNM_EMIT(a,b)                                 \
        FRNM_HSMEMITINST(&FrNm_HsmScFrNm, a, b)
/*==================[type definitions]===================================*/

/*==================[external function declarations]=====================*/

/*==================[internal function declarations]=====================*/

#define FRNM_START_SEC_CODE
/* !LINKSTO FRNM066_MemMap,1 */
#include <MemMap.h>

/** \brief Handle timers respective for each clusters.
 ** \param instIdx ClusterIndex received from MainFunction
 */
STATIC FUNC(void, FRNM_CODE) FrNm_HandleTimers
(
  FRNM_PDL_SF(const uint8 instIdx)
);

#if ((FRNM_REPEAT_MSG_BIT_ENABLED == STD_ON) && (FRNM_NODE_DETECTION_ENABLED == STD_ON))
/** \brief Handle control bit vector received respective for each clusters.
 ** \param instIdx Index to the channel clusters.
 */
STATIC FUNC(void, FRNM_CODE) FrNm_HandleControlBitVector
(
  FRNM_PDL_SF(const uint8 instIdx)
);
#endif

#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
/** \brief Copy data to the NM transmit buffer.
 ** \param instIdx Index to the channel clusters.
 ** \param PduInfoPtr Address of Pdu which contains the transmit buffer and length information.
 */
STATIC FUNC(void, FRNM_CODE) FrNm_CopyPduData
(
  const uint8 instIdx,
  P2VAR(PduInfoType, AUTOMATIC, FRNM_APPL_DATA) PduInfoPtr
);
#endif

#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_ON) || \
     (FRNM_DEV_ERROR_DETECT == STD_ON)       || \
     (FRNM_COM_USER_DATA_SUPPORT == STD_ON)     \
    )
/** \brief Get channel/cluster array index.
 ** \param NetworkHandle ChannelId received from ComM/Nm
 */
STATIC FUNC(uint8, FRNM_CODE) FrNm_GetClusterIndex
(
  CONST(NetworkHandleType, AUTOMATIC) NetworkHandle
);
#endif


#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
/** \brief Get Tx pdu array index.
 ** \param PduId TxPduId received from FrIf
 */
STATIC FUNC(uint8, FRNM_CODE) FrNm_GetPduIndex
(
  CONST(PduIdType, AUTOMATIC) PduId
);
#endif

#if (FRNM_ENABLE_NM_RXVOTE == STD_ON)
/** \brief Handle received vote PDU.
 ** \param instIdx ClusterId
 ** \param SduDataPtr Received Nm message
 */
STATIC FUNC(void, FRNM_CODE) FrNm_HandleRxVotePdu
(
  const uint8 instIdx,
  CONSTP2CONST(uint8, AUTOMATIC, FRNM_APPL_CONST) SduDataPtr
);
#endif

#if (FRNM_ENABLE_NM_RXDATA == STD_ON)
/** \brief Handle received data PDU.
 ** \param instIdx ClusterId
 ** \param SduDataPtr Received Nm message
 */
STATIC FUNC(void, FRNM_CODE) FrNm_HandleRxDataPdu
(
  const uint8 instIdx,
  CONSTP2CONST(uint8, AUTOMATIC, FRNM_APPL_CONST) SduDataPtr
);
#endif

#if (FRNM_ENABLE_NM_RXMIXED == STD_ON)
/** \brief Handle received vote/data PDU.
 ** \param instIdx ClusterId
 ** \param SduDataPtr Received Nm message
 */
STATIC FUNC(void, FRNM_CODE) FrNm_HandleRxMixedPdu
(
  const uint8 instIdx,
  CONSTP2CONST(uint8, AUTOMATIC, FRNM_APPL_CONST) SduDataPtr
);
#endif

#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
/** \brief Handle Partial netorking info, EIRA.
 */
STATIC FUNC(void, FRNM_CODE) FrNm_HandlePn
(
  void
);

/** \brief Handle EIRA for Rx or Tx buffers.
 ** \param MessageBuffer Received/Transmitted Nm message
 **
 ** \return boolean
 ** \retval TRUE EIRA has been changed
 ** \retval FALSE EIRA has not been changed
 */
STATIC FUNC(boolean, FRNM_CODE) FrNm_HandlePnEira
(
  CONSTP2CONST(uint8, AUTOMATIC, FRNM_APPL_CONST) MessageBuffer
);

/** \brief Aggregate external and internal requests.
 ** \param instIdx ClusterId
 ** \param PnInfo Received Nm message
 */
STATIC FUNC(boolean, FRNM_CODE) FrNm_AggregateEira
(
  const uint8 instIdx,
  CONSTP2CONST(uint8, AUTOMATIC, FRNM_APPL_CONST) PnInfo
);

#endif

#define FRNM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[external constants]=================================*/

/*==================[internal constants]=================================*/

/*==================[external data]======================================*/

#define FRNM_START_SEC_VAR_FAST_NO_INIT_UNSPECIFIED
#include <MemMap.h>

VAR(FrNm_ChanStatusType, FRNM_VAR_FAST) FrNm_ChanStatus[FRNM_NUMBER_OF_CHANNELS];

/** \brief Partial netoworking info: EiraValue and EiraTimer */
#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
VAR(FrNm_PnStatusType, FRNM_VAR_FAST) FrNm_PnStatus;
#endif

#define FRNM_STOP_SEC_VAR_FAST_NO_INIT_UNSPECIFIED
#include <MemMap.h>

/*==================[internal data]======================================*/

#define FRNM_START_SEC_VAR_INIT_8
#include <MemMap.h>

/** \brief Intialization information of FrNm module
 */
/* !LINKSTO FRNM071,1 */
VAR(uint8, FRNM_VAR) FrNm_InitStatus = FRNM_UNINIT;

#define FRNM_STOP_SEC_VAR_INIT_8
#include <MemMap.h>

#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
#define FRNM_START_SEC_VAR_NO_INIT_8
#include <MemMap.h>
/** \brief Buffer to hold data to be sent to FrIf.
  * Only one buffer is reserved for all channels and FrIf has to copy data in FrIf_Transmit.
  * Also execution  of main function for one channel is not expected to be preempted for
  * execution of main function for another channel.
  */
STATIC VAR(uint8, FRNM_APPL_DATA) FrNm_PduBuffer[FRNM_TX_BUFFER_SIZE];
#define FRNM_STOP_SEC_VAR_NO_INIT_8
#include <MemMap.h>

#define FRNM_START_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>
/** \brief Variable to hold data and length to be sent to FrIf.
  * Only one variable is reserved for all channels and FrIf has to copy data in FrIf_Transmit.
  * Also execution  of main function for one channel is not expected to be preempted for
  * execution of main function for another channel.
  */
STATIC VAR(PduInfoType, FRNM_APPL_DATA) FrNm_PduInfo;

#define FRNM_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>
#endif /*(FRNM_PASSIVE_MODE_ENABLED == STD_OFF)*/
/*==================[external functions definitions]=====================*/

#define FRNM_START_SEC_CODE
#include <MemMap.h>

/*------------------[FrNm_Init]------------------------------------------*/

FUNC(void, FRNM_CODE) FrNm_Init
(
  CONSTP2CONST(FrNm_ConfigType, AUTOMATIC, FRNM_APPL_CONST) nmConfigPtr
)
{
  uint8 instIdx;

  /* FRNM395 => Rejected, do not support post build configuration */
  TS_PARAM_UNUSED(nmConfigPtr);

  DBG_FRNM_INIT_ENTRY(nmConfigPtr);

  for (instIdx = 0U; instIdx < FRNM_NUMBER_OF_CHANNELS; ++instIdx)
  {
    FRNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_BUS_SLEEP;
    FRNM_CHANNEL_STATUS(instIdx).UniversalTimer = 0U;
    FRNM_CHANNEL_STATUS(instIdx).TimeoutTimer = 0U;
    FRNM_CHANNEL_STATUS(instIdx).ReadySleepTimer = 0U;
    FRNM_CHANNEL_STATUS(instIdx).ActCycle = 0U;
    FRNM_CHANNEL_STATUS(instIdx).ChanStatus  = 0U;
    FRNM_CHANNEL_STATUS(instIdx).ComEnabled = TRUE;
#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
    TS_MemSet(FRNM_CHANNEL_STATUS(instIdx).PnInfoEira, 0U, FRNM_PN_INFO_LENGTH);
#endif
  }

  /* FRNM307 */

  /* Init of channel specific data structures: */
  if (FrNm_InitStatus == FRNM_INIT)
  {
    for (instIdx = 0U; instIdx < FRNM_NUMBER_OF_CHANNELS; instIdx++)
    {
      /* Emit event to indicate reinitialisation and to enter into bus sleep state */
      FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_FAILSAFE_SLEEP_MODE);
    }
  }
  else
  {
    /* Initialize FrNm state machine, perform initial transitions */
    FrNm_HsmInit(&FrNm_HsmScFrNm);
  }

  /* Init of non channel specific data structures (valid for all scenarios): */
#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
  /* !LINKSTO FRNM421,1 */
  TS_MemSet(FrNm_PnStatus.EiraValue, 0x00U, FRNM_PN_INFO_LENGTH);
  TS_MemSet(FrNm_PnStatus.EiraTimer, 0x00U, FRNM_EIRA_TIMER_SIZE);
#endif

  /* FRNM073 */
  FrNm_InitStatus = FRNM_INIT;

  DBG_FRNM_INIT_EXIT(nmConfigPtr);
}


FUNC(void, FRNM_CODE) FrNm_MainFunction
(
  const uint8 instIdx
)
{
  DBG_FRNM_MAINFUNCTION_ENTRY(instIdx);

  /* Do not execute main function until module is initialized, this may be the
   * case during ECU startup. */

  if (FrNm_InitStatus == FRNM_UNINIT)
  {
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
    FRNM_DET_REPORT_ERROR(instIdx, (FRNM_MAINFUNCTION_ID + instIdx), FRNM_E_UNINIT);
#endif /* FRNM_DEV_ERROR_DETECT == STD_ON */
  }
  else
  {
    uint16 FlexRayTickDummy;
    Std_ReturnType FlexRayStatus;

#if (FRNM_HW_VOTE_ENABLE == STD_ON)
    uint8 NmVector;
    uint8 NmChannelProperty;
#endif

#if (FRNM_HW_VOTE_ENABLE == STD_ON)

    /* Note from SWS: If the NM-Vote is transmitted in the static segment of the
     * FlexRay schedule (alternative 1, 3, 4 and 7), the usage of the HW
     * NM-Vector support is possible.
     * Schedule variant 3 is not supported and hence need not be checked here. */

    NmChannelProperty = FRNM_CHANNEL_CONFIG(instIdx).ChannelIdentifiers.ChannelProperty;

    if (((NmChannelProperty & FRNM_PDU_SCHEDULE_VARIANT_MASK) == FRNM_PDU_SCHEDULE_VARIANT_1) ||
        (((NmChannelProperty & FRNM_PDU_SCHEDULE_VARIANT_MASK) == FRNM_PDU_SCHEDULE_VARIANT_4) ||
        ((NmChannelProperty & FRNM_PDU_SCHEDULE_VARIANT_MASK) == FRNM_PDU_SCHEDULE_VARIANT_7))
       )
    {
      if (FrIf_GetNmVector(FrNm_CtrlIdx[instIdx], &NmVector) == E_OK)
      {
        /* Handle the vote bit */
        if ((NmVector & UINT8_C(0x80)) != 0U)
        {
          /* Emit event to restart remote sleep timer if module is in ready
           * sleep state. */
          FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_VOTE_RECEIVED);
        }

#if ((FRNM_REPEAT_MSG_BIT_ENABLED == STD_ON) && (FRNM_NODE_DETECTION_ENABLED == STD_ON))
        if (((NmChannelProperty & FRNM_CONTROL_BIT_VECTOR) == FRNM_CONTROL_BIT_VECTOR) &&
            ((NmChannelProperty & FRNM_REPEAT_MESSAGE_BIT) == FRNM_REPEAT_MESSAGE_BIT)
           )
        {
          /* Handle the repeat message bit since the received vote is for the
           * channel having schedule variant 7. */
          if ((NmVector & FRNM_PDU_REPEAT_MSG_BIT) != 0U)
          {
            /* Enter critical section to protect from concurrent access
             * to different bits in 'ChanStatus' in different APIs.
             */
            SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
            /* Set channel status with request info so as to start
             * repeat message transmission in the next repetition cycle. */
            FRNM_CHANNEL_STATUS(instIdx).ChanStatus |= FRNM_REPEAT_MSG_REQUESTED;
            SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
          }
        }
#endif
      }
    }
#endif /* FRNM_HW_VOTE_ENABLE */

    /* Handle timers */
    FrNm_HandleTimers(FRNM_PL_SF(instIdx));

    /* Get the module cycle time */
    FlexRayStatus = FrIf_GetGlobalTime(FrNm_CtrlIdx[instIdx], &FRNM_CHANNEL_STATUS(instIdx).ActCycle,
                                        &FlexRayTickDummy);

    /* Handle problems getting the actual cycle */
    if (FlexRayStatus != E_OK)
    {
      /* Obviously the synchronization is lost. */
      FrNm_HandleSynchError(FRNM_PL_SF(instIdx));
    }
    else
    {
#if (FRNM_MAIN_ACROSS_FR_CYCLE == STD_OFF)
      /* This FrNm_MainFunction is scheduled before the cycle start.
       * i.e transmission of the first vote shall take place only in the next
       *  cycle. Hence increment cycle count to evaluate FrNm channel timings. */
      if (FRNM_CHANNEL_STATUS(instIdx).ActCycle == UINT8_C(63))
      {
         FRNM_CHANNEL_STATUS(instIdx).ActCycle = UINT8_C(0);
      }
      else
      {
         FRNM_CHANNEL_STATUS(instIdx).ActCycle++;
      }
#endif

      /* Set the Repetition Cycle counter event */
      if (0U == (FRNM_CHANNEL_STATUS(instIdx).ActCycle %
                 FRNM_CHANNELTIME_CONFIG(instIdx).RepititionCycle
                )
         )
      {
        /* Emit event so as to perform transition from the current state if the
         * guard conditions are met. */
        FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_REP_CYCLE_COMPLETED);

        /* Call Nm_SynchronizationPoint if FrNm is in Network Mode, FRNM322 */
#if (FRNM_SYNCHRONIZATIONPOINT_ENABLED == STD_ON)
        if ((FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_NORMAL_OPERATION) ||
            ((FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_READY_SLEEP) ||
            (FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_REPEAT_MESSAGE))
           )
        {
          Nm_SynchronizationPoint(FRNM_CHANNELID_CONFIG(instIdx).ChannelId);
        }
#endif
      }

      if (0U == (FRNM_CHANNEL_STATUS(instIdx).ActCycle %
                 FRNM_CHANNELTIME_CONFIG(instIdx).VotingCycle
                )
         )
      {
          /* Emit event to repeat transmission of vote  */
          FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_VOTE_CYCLE_TIMESLOT);
      }

      if (0U == (FRNM_CHANNEL_STATUS(instIdx).ActCycle %
                 FRNM_CHANNELTIME_CONFIG(instIdx).DataCycle
                )
         )
      {
        /* Emit event to repeat transmission of data */
        FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_DATA_CYCLE_TIMESLOT);
      }
    }

    /* Perform state transitions and actions according to the events queued */
    while (TRUE == FRNM_HSMMAININST(&FrNm_HsmScFrNm, instIdx))
    {
      /* Execute state machine as long as there is a transition in the last
       * state machine function call. */
    }

#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
    FrNm_HandlePn();
#endif
  }

  DBG_FRNM_MAINFUNCTION_EXIT(instIdx);
  return;
}



/*------------------[FrNm_PassiveStartUp]--------------------------------*/

FUNC(Std_ReturnType, FRNM_CODE) FrNm_PassiveStartUp
(
   const NetworkHandleType NetworkHandle
)
{
  Std_ReturnType retVal = E_NOT_OK;

#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) && (FRNM_DEV_ERROR_DETECT == STD_OFF))
  TS_PARAM_UNUSED(NetworkHandle);
#else
  const uint8 instIdx = FrNm_GetClusterIndex(NetworkHandle);
#endif

  DBG_FRNM_PASSIVESTARTUP_ENTRY(NetworkHandle);

#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_PASSIVESTARTUP, FRNM_E_UNINIT);
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_PASSIVESTARTUP, FRNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {

    if (FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_BUS_SLEEP)
    {
      /* Enter critical section to protect from concurrent access
       * to different bits in 'ChanStatus' in different APIs.
       */
      SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
      /* Set Passive Start status flag */
      FRNM_CHANNEL_STATUS(instIdx).ChanStatus |= FRNM_NETWORK_PASSIVE_START;
      SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
      /* FRNM138, FRNM317, Emit event for transition to Synchronise mode */
      FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_NETWORK_START);
      /* FRNM237 */
      retVal = E_OK;
    }
    else
    {
      /* FRNM260: */
      retVal = E_NOT_OK;
    }
  }

  DBG_FRNM_PASSIVESTARTUP_EXIT(retVal,NetworkHandle);
  return retVal;
}

/*------------------[FrNm_NetworkRequest]--------------------------------*/

#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
FUNC(Std_ReturnType, FRNM_CODE) FrNm_NetworkRequest
(
   const NetworkHandleType NetworkHandle
)
{
  Std_ReturnType retVal = E_NOT_OK;

#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) && (FRNM_DEV_ERROR_DETECT == STD_OFF))
  TS_PARAM_UNUSED(NetworkHandle);
#else
  const uint8 instIdx = FrNm_GetClusterIndex(NetworkHandle);
#endif

  DBG_FRNM_NETWORKREQUEST_ENTRY(NetworkHandle);

#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_NETWORKREQUEST, FRNM_E_UNINIT);
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_NETWORKREQUEST, FRNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {
    /* Enter critical section to protect from concurrent access
     * to different bits in 'ChanStatus' in different APIs.
     */
    SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    /* Set Network Requested status flag */
    FRNM_CHANNEL_STATUS(instIdx).ChanStatus |= FRNM_NETWORK_REQUESTED;
    SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

    if (FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_BUS_SLEEP)
    {
      /* FRNM316, Emit event for transition to Synchronise mode */
      FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_NETWORK_START);
    }

    retVal = E_OK;
  }

  DBG_FRNM_NETWORKREQUEST_EXIT(retVal,NetworkHandle);
  return retVal;
}

/*------------------[FrNm_NetworkRelease]--------------------------------*/

FUNC(Std_ReturnType, FRNM_CODE) FrNm_NetworkRelease
(
   const NetworkHandleType NetworkHandle
)
{
  Std_ReturnType retVal = E_NOT_OK;

#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) && (FRNM_DEV_ERROR_DETECT == STD_OFF))
  TS_PARAM_UNUSED(NetworkHandle);
#else
  const uint8 instIdx = FrNm_GetClusterIndex(NetworkHandle);
#endif

  DBG_FRNM_NETWORKRELEASE_ENTRY(NetworkHandle);

#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_NETWORKRELEASE, FRNM_E_UNINIT);
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_NETWORKRELEASE, FRNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {
    /* Enter critical section to protect from concurrent access
     * to different bits in 'ChanStatus' in different APIs.
     */
    SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    /* Clear Network Requested status flag */
    FRNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~FRNM_NETWORK_REQUESTED);
    SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    retVal = E_OK;
  }


  DBG_FRNM_NETWORKRELEASE_EXIT(retVal,NetworkHandle);
  return retVal;
}
#endif /* (FRNM_PASSIVE_MODE_ENABLED == STD_OFF) */


/*------------------[FrNm_SetUserData]-----------------------------------*/

#if ((FRNM_USER_DATA_ENABLED == STD_ON)   \
  && ((FRNM_ENABLE_NMDATA == STD_ON) || (FRNM_ENABLE_NMMIXED == STD_ON)) \
  && (FRNM_COM_USER_DATA_SUPPORT == STD_OFF))
FUNC(Std_ReturnType, FRNM_CODE) FrNm_SetUserData
(
   const NetworkHandleType NetworkHandle,
   CONSTP2CONST(uint8, AUTOMATIC, FRNM_APPL_CONST) nmUserDataPtr
)
{
  Std_ReturnType retVal = E_NOT_OK;

#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) && (FRNM_DEV_ERROR_DETECT == STD_OFF))
  TS_PARAM_UNUSED(NetworkHandle);
#else
  const uint8 instIdx = FrNm_GetClusterIndex(NetworkHandle);
#endif

  DBG_FRNM_SETUSERDATA_ENTRY(NetworkHandle,nmUserDataPtr);

  /* Perform DET checks */
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_SETUSERDATA, FRNM_E_UNINIT);
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_SETUSERDATA, FRNM_E_INVALID_CHANNEL);
  }
  /* Check if the channel has a pdu configured to send data (FrNmTxPduContainsData = True) */
  else if ((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_NM_DATA_ENABLED) !=
            FRNM_NM_DATA_ENABLED)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_SETUSERDATA, FRNM_E_INVALID_CHANNEL);
  }
  else if (nmUserDataPtr == NULL_PTR)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_SETUSERDATA, FRNM_E_INVALID_POINTER);
  }
  else if (FRNM_CHANNELID_CONFIG(instIdx).PduLength <= FRNM_RESERVED_BYTES)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_SETUSERDATA, FRNM_E_INVALID_FUNCTION_ARG);
  }
  else
#endif
  {
    /* Protect data buffer from being read for the transmission */
    SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    /* Reserved bytes in the data buffer are being filled just before actual data transmission and
     * not at this place.
     */
    TS_MemCpy(
      &FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[FRNM_RESERVED_BYTES], nmUserDataPtr,
      (uint16)(FRNM_CHANNELID_CONFIG(instIdx).PduLength - FRNM_RESERVED_BYTES));
    SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    retVal = E_OK;
  }

  DBG_FRNM_SETUSERDATA_EXIT(retVal,NetworkHandle,nmUserDataPtr);
  return retVal;
}
#endif /* (FRNM_USER_DATA_ENABLED == STD_ON) && .. */

/*------------------[FrNm_GetUserData]-----------------------------------*/
#if (FRNM_USER_DATA_ENABLED == STD_ON)
FUNC(Std_ReturnType, FRNM_CODE) FrNm_GetUserData
(
   const NetworkHandleType NetworkHandle,
   CONSTP2VAR(uint8, AUTOMATIC, FRNM_APPL_DATA) nmUserDataPtr
)
{
  Std_ReturnType retVal = E_NOT_OK;

#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) && \
     (FRNM_DEV_ERROR_DETECT == STD_OFF)       && \
     (FRNM_COM_USER_DATA_SUPPORT == STD_OFF)     \
    )
  TS_PARAM_UNUSED(NetworkHandle);
#else
  const uint8 instIdx = FrNm_GetClusterIndex(NetworkHandle);
#endif

  DBG_FRNM_GETUSERDATA_ENTRY(NetworkHandle,nmUserDataPtr);

  /* Perform DET checks */
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_GETUSERDATA, FRNM_E_UNINIT);
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_GETUSERDATA, FRNM_E_INVALID_CHANNEL);
  }
  else if (nmUserDataPtr == NULL_PTR)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_GETUSERDATA, FRNM_E_INVALID_POINTER);
  }
  else if (FRNM_CHANNELID_CONFIG(instIdx).PduLength <= FRNM_RESERVED_BYTES)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_GETUSERDATA, FRNM_E_INVALID_FUNCTION_ARG);
  }
  else
#endif
  {
#if (FRNM_COM_USER_DATA_SUPPORT == STD_ON)
    if ((FrNm_ComUserDataInfo[instIdx].ComUserRxPduEnabled == FALSE) ||
        (FrNm_ComUserDataInfo[instIdx].ComUserTxPduEnabled == FALSE))
    {
      FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_GETUSERDATA, FRNM_E_INVALID_FUNCTION_ARG);
    }
    else
#endif
    {
      SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

      /* Copy user data */
      TS_MemCpy(
        nmUserDataPtr,
        &FRNM_CHANNEL_CONFIG(instIdx).RxPduPtr[FRNM_RESERVED_BYTES],
        (uint16)(FRNM_CHANNELID_CONFIG(instIdx).PduLength - FRNM_RESERVED_BYTES));

      SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

      retVal = E_OK;
    }
  }

  DBG_FRNM_GETUSERDATA_EXIT(retVal,NetworkHandle,nmUserDataPtr);
  return retVal;
}

#endif /* (FRNM_USER_DATA_ENABLED == STD_ON) */


/*------------------[FrNm_GetPduData]------------------------------------*/

#if ((FRNM_CONTROL_BIT_VECTOR_ENABLED == STD_ON)   \
   || (FRNM_SOURCE_NODE_IDENTIFIER_ENABLED == STD_ON) \
   || (FRNM_USER_DATA_ENABLED == STD_ON))
FUNC(Std_ReturnType, FRNM_CODE) FrNm_GetPduData
(
   const NetworkHandleType NetworkHandle,
   CONSTP2VAR(uint8, AUTOMATIC, FRNM_APPL_DATA) nmPduData
)
{
  Std_ReturnType retVal = E_NOT_OK;

#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) && (FRNM_DEV_ERROR_DETECT == STD_OFF))
  TS_PARAM_UNUSED(NetworkHandle);
#else
  const uint8 instIdx = FrNm_GetClusterIndex(NetworkHandle);
#endif

  DBG_FRNM_GETPDUDATA_ENTRY(NetworkHandle,nmPduData);

  /* Perform DET checks */
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_GETPDUDATA, FRNM_E_UNINIT);
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_GETPDUDATA, FRNM_E_INVALID_CHANNEL);
  }
  else if (nmPduData == NULL_PTR)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_GETPDUDATA, FRNM_E_INVALID_POINTER);
  }
  else
#endif
  {
    SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

    /* Copy the whole NM PDU data */
    TS_MemCpy(
      nmPduData,
      FRNM_CHANNEL_CONFIG(instIdx).RxPduPtr,
      FRNM_CHANNELID_CONFIG(instIdx).PduLength);

    SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

    retVal = E_OK;
  }

  DBG_FRNM_GETPDUDATA_EXIT(retVal,NetworkHandle,nmPduData);
  return retVal;
}
#endif /* ((FRNM_CONTROL_BIT_VECTOR_ENABLED == STD_ON)   \
        *|| (FRNM_SOURCE_NODE_IDENTIFIER_ENABLED == STD_ON) ||   \
        *|| (FRNM_USER_DATA_ENABLED == STD_ON)) */

/*------------------[FrNm_RepeatMessageRequest]--------------------------*/

#if (FRNM_NODE_DETECTION_ENABLED == STD_ON)
FUNC(Std_ReturnType, FRNM_CODE) FrNm_RepeatMessageRequest
(
   const NetworkHandleType NetworkHandle
)
{
  /* Implicit requirement : If not in network mode, this function returns E_NOT_OK */
  Std_ReturnType retVal = E_NOT_OK;

#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) && (FRNM_DEV_ERROR_DETECT == STD_OFF))
  TS_PARAM_UNUSED(NetworkHandle);
#else
  const uint8 instIdx = FrNm_GetClusterIndex(NetworkHandle);
#endif

  DBG_FRNM_REPEATMESSAGEREQUEST_ENTRY(NetworkHandle);

  /* Perform DET checks */
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_REPEATMESSAGEREQUEST, FRNM_E_UNINIT);
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_REPEATMESSAGEREQUEST, FRNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {
    /*Repeat message request is accepted only in Network mode */
    if ((FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_NORMAL_OPERATION) ||
        ((FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_READY_SLEEP) ||
        (FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_REPEAT_MESSAGE))
       )
    {
      /* Enter critical section to protect from concurrent access
       * to different bits in 'ChanStatus' in different APIs.
       */
      SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
      /* FRNM111, FRNM226 */
      /* Set Repeat Message Requestd flag */
      FRNM_CHANNEL_STATUS(instIdx).ChanStatus |= FRNM_REPEAT_MSG_REQUESTED;
      SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

      /* Set repeat message bit in the transmit buffer */

#if (FRNM_REPEAT_MSG_BIT_ENABLED == STD_ON)
      if (((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_CONTROL_BIT_VECTOR) != 0U) &&
          ((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_REPEAT_MESSAGE_BIT) != 0U)
         )
      {
          /* Make sure that transmission not happens while setting bit */
          SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
          /* Set repeat message bit */
          FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] |= FRNM_PDU_REPEAT_MSG_BIT;
          SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
      }
#endif
      retVal = E_OK;
    }
  }

  DBG_FRNM_REPEATMESSAGEREQUEST_EXIT(retVal,NetworkHandle);
  return retVal;
}
#endif /* (FRNM_NODE_DETECTION_ENABLED == STD_ON) */

/*------------------[FrNm_GetNodeIdentifier]-----------------------------*/

#if (FRNM_NODE_DETECTION_ENABLED == STD_ON) && (FRNM_SOURCE_NODE_IDENTIFIER_ENABLED == STD_ON)
FUNC(Std_ReturnType, FRNM_CODE) FrNm_GetNodeIdentifier
(
  const NetworkHandleType NetworkHandle,
  CONSTP2VAR(uint8, AUTOMATIC, FRNM_APPL_DATA) nmNodeIdPtr
)
{
  Std_ReturnType retVal = E_NOT_OK;

#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) && (FRNM_DEV_ERROR_DETECT == STD_OFF))
  TS_PARAM_UNUSED(NetworkHandle);
#else
  const uint8 instIdx = FrNm_GetClusterIndex(NetworkHandle);
#endif

  DBG_FRNM_GETNODEIDENTIFIER_ENTRY(NetworkHandle,nmNodeIdPtr);

  /* Perform checks, get index */
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_GETNODEIDENTIFIER, FRNM_E_UNINIT);
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_GETNODEIDENTIFIER, FRNM_E_INVALID_CHANNEL);
  }
  else if (nmNodeIdPtr == NULL_PTR)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_GETNODEIDENTIFIER, FRNM_E_INVALID_POINTER);
  }
  else
#endif
  {
    /* Make sure that RxIndication not happens while copying data */
     SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    /* Copy Node Identifier */
    *nmNodeIdPtr = FRNM_CHANNEL_CONFIG(instIdx).RxPduPtr[FRNM_PDU_BYTE_1];
    SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    retVal = E_OK;
  }

  DBG_FRNM_GETNODEIDENTIFIER_EXIT(retVal,NetworkHandle,nmNodeIdPtr);
  return retVal;
}

/*------------------------[FrNm_GetLocalNodeIdentifier]---------------------*/

FUNC(Std_ReturnType, FRNM_CODE) FrNm_GetLocalNodeIdentifier
(
  const NetworkHandleType NetworkHandle,
  CONSTP2VAR(uint8, AUTOMATIC, FRNM_APPL_DATA) nmNodeIdPtr
)
{
  Std_ReturnType retVal = E_NOT_OK;

#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) && (FRNM_DEV_ERROR_DETECT == STD_OFF))
  TS_PARAM_UNUSED(NetworkHandle);
#else
  const uint8 instIdx = FrNm_GetClusterIndex(NetworkHandle);
#endif

  DBG_FRNM_GETLOCALNODEIDENTIFIER_ENTRY(NetworkHandle,nmNodeIdPtr);

#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_GETLOCALNODEIDENTIFIER, FRNM_E_UNINIT);
  }

  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_GETLOCALNODEIDENTIFIER,
      FRNM_E_INVALID_CHANNEL);
  }

  else if (nmNodeIdPtr == NULL_PTR)
  {
    FRNM_DET_REPORT_ERROR(instIdx,FRNM_SERVID_GETLOCALNODEIDENTIFIER,
      FRNM_E_INVALID_POINTER);
  }

  else
#endif
  {
    /* Copy Node Id */
    *nmNodeIdPtr = FRNM_CHANNEL_CONFIG(instIdx).ChannelIdentifiers.NodeId;
    retVal = E_OK;
  }

  DBG_FRNM_GETLOCALNODEIDENTIFIER_EXIT(retVal,NetworkHandle,nmNodeIdPtr);
  return retVal;
}
#endif /*(FRNM_NODE_DETECTION_ENABLED == STD_ON)&&(FRNM_SOURCE_NODE_IDENTIFIER_ENABLED == STD_ON) */

/*------------------[FrNm_RequestBusSynchronization]---------------------*/

#if (FRNM_BUS_SYNCHRONIZATION_ENABLED == STD_ON)
FUNC(Std_ReturnType, FRNM_CODE) FrNm_RequestBusSynchronization
(
   const NetworkHandleType NetworkHandle
)
{
  /* FRNM174: The FrNm module shall support the function FrNm_RequestBusSynchronization
   * by returning E_OK without executing any functionality.
   */
  Std_ReturnType retVal = E_OK;
  /* Though, this function has no functionality, DET checks are performed */
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  const uint8 instIdx = FrNm_GetClusterIndex(NetworkHandle);
#else
  TS_PARAM_UNUSED(NetworkHandle);
#endif

  DBG_FRNM_REQUESTBUSSYNCHRONIZATION_ENTRY(NetworkHandle);

#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_REQUESTBUSSYNCHRONIZATION, FRNM_E_UNINIT);
    retVal = E_NOT_OK;
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_REQUESTBUSSYNCHRONIZATION,
      FRNM_E_INVALID_CHANNEL);
    retVal = E_NOT_OK;
  }
  else
  {
    /* Do nothing, FRNM174 */
  }
#endif

  DBG_FRNM_REQUESTBUSSYNCHRONIZATION_EXIT(retVal,NetworkHandle);
  return retVal;
}
#endif /* (FRNM_BUS_SYNCHRONIZATION_ENABLED == STD_ON) */


/*------------------[FrNm_CheckRemoteSleepIndication]--------------------*/

#if (FRNM_REMOTE_SLEEP_INDICATION_ENABLED == STD_ON)
FUNC(Std_ReturnType, FRNM_CODE) FrNm_CheckRemoteSleepIndication
(
   const NetworkHandleType NetworkHandle,
   CONSTP2VAR(boolean, AUTOMATIC, FRNM_APPL_DATA) nmRemoteSleepIndPtr
)
{
  Std_ReturnType retVal = E_NOT_OK;

#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) && (FRNM_DEV_ERROR_DETECT == STD_OFF))
  TS_PARAM_UNUSED(NetworkHandle);
#else
  const uint8 instIdx = FrNm_GetClusterIndex(NetworkHandle);
#endif

  DBG_FRNM_CHECKREMOTESLEEPINDICATION_ENTRY(NetworkHandle,nmRemoteSleepIndPtr);

  /* Perform DET checks */
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_CHECKREMOTESLEEPINDICATION, FRNM_E_UNINIT);
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_CHECKREMOTESLEEPINDICATION, FRNM_E_INVALID_CHANNEL);
  }
  else if (nmRemoteSleepIndPtr == NULL_PTR)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_CHECKREMOTESLEEPINDICATION, FRNM_E_INVALID_POINTER);
  }
  else
#endif
  {
    /* It is always the case that remote sleep indication bit is cleared when FrNm is in Repeat
     * Message State.Though the below check includes Repeat Message State in order to check FRNM186
     */

    if ((FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_NORMAL_OPERATION) ||
        ((FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_READY_SLEEP) ||
        (FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_REPEAT_MESSAGE))
       )
    {
      /* FRNM185 */
      if ((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_REMOTE_SLEEP_INDICATION) != 0U)
      {
        *nmRemoteSleepIndPtr = TRUE;
      }
      else
      {
        *nmRemoteSleepIndPtr = FALSE;
      }
      retVal = E_OK;
    }
    else
    {
      /* FRNM186 */
      retVal = E_NOT_OK;
    }
  }

  DBG_FRNM_CHECKREMOTESLEEPINDICATION_EXIT(retVal,NetworkHandle,nmRemoteSleepIndPtr);
  return retVal;
}
#endif /* (FRNM_REMOTE_SLEEP_INDICATION_ENABLED == STD_ON) */

/*------------------[FrNm_GetState]--------------------------------------*/

FUNC(Std_ReturnType, FRNM_CODE) FrNm_GetState
(
   const NetworkHandleType NetworkHandle,
   CONSTP2VAR(Nm_StateType, AUTOMATIC, FRNM_APPL_DATA) nmStatePtr,
   CONSTP2VAR(Nm_ModeType, AUTOMATIC, FRNM_APPL_DATA) nmModePtr
)
{
  Std_ReturnType retVal = E_NOT_OK;

#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) && (FRNM_DEV_ERROR_DETECT == STD_OFF))
  TS_PARAM_UNUSED(NetworkHandle);
#else
  const uint8 instIdx = FrNm_GetClusterIndex(NetworkHandle);
#endif

  DBG_FRNM_GETSTATE_ENTRY(NetworkHandle,nmStatePtr,nmModePtr);

  /* Perform checks, get index */
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_GETSTATE, FRNM_E_UNINIT);
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_GETSTATE, FRNM_E_INVALID_CHANNEL);
  }
  else if (nmStatePtr == NULL_PTR)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_GETSTATE, FRNM_E_INVALID_POINTER);
  }
  else if (nmModePtr == NULL_PTR)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_GETSTATE, FRNM_E_INVALID_POINTER);
  }
  else
#endif
  {
    /* Note from SWS : Consistency between the provided values and the current values
       of the state and mode should be ensured.*/

    /* Above consistency can not be guaranteed by FrNm since the state changes are driven
     * by external events. Even if the remaining code snippet of this function is protected
     * by a critical section, it could still happen a state transition happens before a
     * calling module evaluates these values. Hence, in order to have a consistency of these values
     * the calling module shall consider this aspect and call this function inside critical section.
     */

    *nmStatePtr = FRNM_CHANNEL_STATUS(instIdx).CurState;

    switch (*nmStatePtr)
    {
      case NM_STATE_NORMAL_OPERATION: /** fall through */
      case NM_STATE_REPEAT_MESSAGE: /** fall through */
      case NM_STATE_READY_SLEEP:
        *nmModePtr = NM_MODE_NETWORK;
        retVal = E_OK;
        break;
      case NM_STATE_BUS_SLEEP:
        *nmModePtr = NM_MODE_BUS_SLEEP;
        retVal = E_OK;
        break;
      case NM_STATE_SYNCHRONIZE:
        *nmModePtr = NM_MODE_SYNCHRONIZE;
        retVal = E_OK;
        break;
      default:
        /* place for defensive programming */
        break;
    }
  }

  DBG_FRNM_GETSTATE_EXIT(retVal,NetworkHandle,nmStatePtr,nmModePtr);
  return retVal;
}

/*------------------[FrNm_GetVersionInfo]--------------------------------*/

#if (FRNM_VERSION_INFO_API == STD_ON)
FUNC(void, FRNM_CODE) FrNm_GetVersionInfo
(
   P2VAR(Std_VersionInfoType, AUTOMATIC, FRNM_APPL_DATA) NmVerInfoPtr
)
{
  DBG_FRNM_GETVERSIONINFO_ENTRY(NmVerInfoPtr);
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
   if (NULL_PTR == NmVerInfoPtr)
   {
     FRNM_DET_REPORT_ERROR(
     FRNM_INSTANCE_ID, FRNM_SERVID_GETVERSIONINFO, FRNM_E_INVALID_POINTER);
   }
   else
#endif
   {
     NmVerInfoPtr->vendorID         = FRNM_VENDOR_ID;
     NmVerInfoPtr->moduleID         = FRNM_MODULE_ID;
     NmVerInfoPtr->sw_major_version = FRNM_SW_MAJOR_VERSION;
     NmVerInfoPtr->sw_minor_version = FRNM_SW_MINOR_VERSION;
     NmVerInfoPtr->sw_patch_version = FRNM_SW_PATCH_VERSION;
   }

   DBG_FRNM_GETVERSIONINFO_EXIT(NmVerInfoPtr);
}
#endif /* (FRNM_VERSION_INFO_API == STD_ON) */

/*------------------[FrNm_StartupError]-----------------------------------*/

FUNC(void, FRNM_CODE) FrNm_StartupError
(
   const NetworkHandleType NetworkHandle
)
{
#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) && (FRNM_DEV_ERROR_DETECT == STD_OFF))
  TS_PARAM_UNUSED(NetworkHandle);
#else
  const uint8 instIdx = FrNm_GetClusterIndex(NetworkHandle);
#endif

  DBG_FRNM_STARTUPERROR_ENTRY(NetworkHandle);

  /* Perform checks, get index */
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_STARTUPERROR, FRNM_E_UNINIT);
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_STARTUPERROR, FRNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {
    /* Disable transmission. We here do it to prevent transmission immediately.*/
    FRNM_CHANNEL_STATUS(instIdx).ComEnabled = FALSE;

    /* Emit event to indicate startup error */
    FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_STARTUP_ERROR);

    /* FRNM336: The FlexRay Network Management must react (execute) synchronously on reception
     * of the indication FrNm_StartupError from the FlexRay State Manager even when the FrNM
     * Mainfunction is no longer executing.
     * But we do not perform state transitions and actions here.
     * Rationale: We strongly assume that the FrNm will also be scheduled if the synchronization is
     * lost because normally FrSM and FrNm are scheduled from the same entity and FrSM needs to be
     * scheduled even if synchronization is lost because it is the task of FrSM to organize the
     * re-synchronization of the FR.
     */
  }

  DBG_FRNM_STARTUPERROR_EXIT(NetworkHandle);
}



/*------------------[FrNm_Transmit]--------------------------------------*/

FUNC(Std_ReturnType, FRNM_CODE) FrNm_Transmit
(
  PduIdType FrNmTxPduId,
  P2CONST(PduInfoType, AUTOMATIC, FRNM_APPL_CONST) PduInfoPtr
)
{
  TS_PARAM_UNUSED(FrNmTxPduId);
  TS_PARAM_UNUSED(PduInfoPtr);

  DBG_FRNM_TRANSMIT_ENTRY(FrNmTxPduId,PduInfoPtr);

  /* FRNM366 : The FrNm implementation shall provide an API FrNm_Transmit (see FRNM359).
   * This API shall never be called by PduR as the FrNm will always query the data by
   * means of PduR_FrNmTriggerTransmit. FrNm_Transmit is an empty function returning
   * E_NOT_OK at any time. This requirement is relevant to avoid linker errors as PduR
   * expects this API to be provided.
   */

  DBG_FRNM_TRANSMIT_EXIT((E_NOT_OK),FrNmTxPduId,PduInfoPtr);
  return (E_NOT_OK);
}


/*------------------[FrNm_EnableCommunication]---------------------------*/

FUNC(Std_ReturnType, FRNM_CODE) FrNm_EnableCommunication
(
   const NetworkHandleType nmChannelHandle
)
{
  Std_ReturnType retVal = E_NOT_OK;
#if ((FRNM_DEV_ERROR_DETECT == STD_ON) \
  || ((FRNM_PASSIVE_MODE_ENABLED == STD_OFF) \
  &&  (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)))
  const uint8 instIdx = FrNm_GetClusterIndex(nmChannelHandle);
#else
  TS_PARAM_UNUSED(nmChannelHandle);
#endif

  DBG_FRNM_ENABLECOMMUNICATION_ENTRY(nmChannelHandle);
  /* Perform DET checks */
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_ENABLECOMMUNICATION, FRNM_E_UNINIT);
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_ENABLECOMMUNICATION, FRNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {
#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
    /* Enable communication request is accepted only in Network mode with passive mode
     * disabled, FRNM388.
     */
    if (
        (FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_NORMAL_OPERATION) ||
        ((FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_READY_SLEEP) ||
        (FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_REPEAT_MESSAGE))
       )
    {
      /* Enable transmission. No event being emitted */
      FRNM_CHANNEL_STATUS(instIdx).ComEnabled = TRUE;
      retVal = E_OK;
    }
    else
#endif /* (FRNM_PASSIVE_MODE_ENABLED == STD_OFF) */
    {
      /* return E_NOT_OK if module is not in network mode, FRNM388.
       * or if module operates with passive mode enabled, FRNM389.
       */
      retVal = E_NOT_OK;
    }
  }

  DBG_FRNM_ENABLECOMMUNICATION_EXIT(retVal,nmChannelHandle);
  return retVal;
}



/*------------------[FrNm_DisableCommunication]--------------------------*/

FUNC(Std_ReturnType, FRNM_CODE) FrNm_DisableCommunication
(
   const NetworkHandleType nmChannelHandle
)
{
  Std_ReturnType retVal = E_NOT_OK;
#if ((FRNM_DEV_ERROR_DETECT == STD_ON) \
  || ((FRNM_PASSIVE_MODE_ENABLED == STD_OFF) \
  && (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)))
  const uint8 instIdx = FrNm_GetClusterIndex(nmChannelHandle);
#else
  TS_PARAM_UNUSED(nmChannelHandle);
#endif

  DBG_FRNM_ENABLECOMMUNICATION_ENTRY(nmChannelHandle);
  /* Perform DET checks */
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_DISABLECOMMUNICATION, FRNM_E_UNINIT);
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(instIdx, FRNM_SERVID_DISABLECOMMUNICATION, FRNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {
#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
    /* Disable communication request is accepted only in Network mode with passive mode
     * disabled, FRNM391.
     */
    if (
        (FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_NORMAL_OPERATION) ||
        ((FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_READY_SLEEP) ||
         (FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_REPEAT_MESSAGE)
        )
       )
    {
      /* Disable transmission. No event being emitted */
      FRNM_CHANNEL_STATUS(instIdx).ComEnabled = FALSE;
      retVal = E_OK;
    }
    else
#endif /* (FRNM_PASSIVE_MODE_ENABLED == STD_OFF) */
    {
      /* return E_NOT_OK if module is not in network mode, FRNM391
       * or if module operates with passive mode enabled, FRNM392
       */
      retVal = E_NOT_OK;
    }
  }

  DBG_FRNM_DISABLECOMMUNICATION_EXIT(retVal,nmChannelHandle);
  return retVal;
}



/*------------------[FrNm_RxIndication]----------------------------------*/

FUNC(void, FRNM_CODE) FrNm_RxIndication
(
   PduIdType RxPduId,
   P2VAR(PduInfoType, AUTOMATIC, FRNM_APPL_DATA) PduInfoPtr
)
{
#if (FRNM_COM_USER_DATA_SUPPORT == STD_ON)
  VAR(PduInfoType, FRNM_APPL_DATA) UserPdu;
#endif /* (FRNM_COM_USER_DATA_SUPPORT == STD_ON) */

  DBG_FRNM_RXINDICATION_ENTRY(RxPduId,PduInfoPtr);

  /* Perform DET checks */
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (RxPduId >= FRNM_NUMBER_OF_RX_PDUS)
  {
    /* In case the incoming PduId is invalid, the associated instance can not be
     * determined, therefore instance 0 is reported to DET.
     */
    FRNM_DET_REPORT_ERROR(0U, FRNM_SERVID_RXINDICATION, FRNM_E_PDU_ID_INVALID);
  }
  else if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(FrNm_RxPduMap[RxPduId].ChannelHandle, FRNM_SERVID_RXINDICATION,
                           FRNM_E_UNINIT);
  }
  else if (
           (PduInfoPtr == NULL_PTR) ||
           (PduInfoPtr->SduDataPtr == NULL_PTR)
          )
  {
    /* Report FRNM_E_INVALID_POINTER.*/
    FRNM_DET_REPORT_ERROR(FrNm_RxPduMap[RxPduId].ChannelHandle, FRNM_SERVID_RXINDICATION,
                          FRNM_E_INVALID_POINTER);
  }
  else if (PduInfoPtr->SduLength != FRNM_CHANNELID_CONFIG(FrNm_RxPduMap[RxPduId].ChannelHandle).
                                                                                 PduLength)
  {
    /* Det error shall be raised in case any of the remote node send data with size different
     * than expected because this would mix up data between different nodes.
     */
    FRNM_DET_REPORT_ERROR(FrNm_RxPduMap[RxPduId].ChannelHandle, FRNM_SERVID_RXINDICATION,
                          FRNM_E_INVALID_FUNCTION_ARG);
  }
  else
#endif
  {
    const uint8 instIdx = FrNm_RxPduMap[RxPduId].ChannelHandle;
#if (FRNM_COORDINATOR_SYNC_SUPPORT == STD_ON)
    if ((PduInfoPtr->SduDataPtr[0U] & FRNM_CBV_SLEEPREADYBIT_MASK) != 0U)
    {
      /* FRNM396 */
      Nm_CoordReadyToSleepIndication(instIdx);
    }
#endif /* (FRNM_COORDINATOR_SYNC_SUPPORT == STD_ON) */
#if (FRNM_ENABLE_NM_RXVOTE == STD_ON)
    if (FrNm_RxPduMap[RxPduId].PduType == FRNM_VOTE)
    {
      FrNm_HandleRxVotePdu(instIdx, PduInfoPtr->SduDataPtr);
      /* Emit event to indicate RxIndication to the upper layer if in network mode */
      FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_RX_INDICATION);
    }
    else
#endif /* (FRNM_ENABLE_NM_RXVOTE == STD_ON) */
    {
#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
      /* FRNM407 */
      boolean PnMessage = TRUE;
      SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
      /* FrNm_RxIndication could be called more than once within the interval of 2 main function
       * calls. Therefore, the external requests to be aggregated.
       */
      /* FrNm_AggregateEira shall be called only when CRI bit is set in the received NM-PDU */
      /* !LINKSTO FRNM404,1 */
      if ((PduInfoPtr->SduDataPtr[0U] & FRNM_CBV_PNINFOBIT_MASK) != 0U)
      {
        PnMessage = FrNm_AggregateEira(instIdx, &PduInfoPtr->SduDataPtr[FRNM_PN_INFO_OFFSET]);
      }
      SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
#endif

#if (FRNM_PN_ENABLED == STD_ON)
#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
      if ((PnMessage == TRUE) ||
          /* FRNM409 : Partial networking is not supported */
          (FRNM_CHANNELID_CONFIG(instIdx).PnEnabled == FALSE)
         )
#else
      /* FRNM409 : Partial networking is not supported */
      if (FRNM_CHANNELID_CONFIG(instIdx).PnEnabled == FALSE)
#endif
#endif /*(FRNM_PN_ENABLED == STD_ON)*/
      {
#if (FRNM_ENABLE_NM_RXDATA == STD_ON)
        if (FrNm_RxPduMap[RxPduId].PduType == FRNM_DATA)
        {
          FrNm_HandleRxDataPdu(instIdx, PduInfoPtr->SduDataPtr);
        }
#endif /* (FRNM_ENABLE_NM_RXDATA == STD_ON) */
#if (FRNM_ENABLE_NM_RXMIXED == STD_ON)
        if (FrNm_RxPduMap[RxPduId].PduType == FRNM_MIXED)
        {
          FrNm_HandleRxMixedPdu(instIdx, PduInfoPtr->SduDataPtr);
        }
#endif /* (FRNM_ENABLE_NM_RXMIXED == STD_ON) */
         /* Emit event to indicate RxIndication to the upper layer if in network mode */
        FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_RX_INDICATION);
      }
      SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

      /* Copy the NM bytes from the second byte position since first byte
       * has already been copied.
       */
      TS_MemCpy(
        &FRNM_CHANNEL_CONFIG(instIdx).RxPduPtr[1U],
        &PduInfoPtr->SduDataPtr[1U],
        (uint16)(PduInfoPtr->SduLength - 1U));
       SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

#if (FRNM_COM_USER_DATA_SUPPORT == STD_ON)
    if (FrNm_ComUserDataInfo[instIdx].ComUserRxPduEnabled == TRUE)
    {
      UserPdu.SduLength = PduInfoPtr->SduLength - FRNM_RESERVED_BYTES;
      UserPdu.SduDataPtr = &FRNM_CHANNEL_CONFIG(instIdx).RxPduPtr[FRNM_RESERVED_BYTES];

      PduR_FrNmRxIndication(FrNm_ComUserDataInfo[instIdx].ComUserRxPduId, &UserPdu);
    }
#endif /* (FRNM_COM_USER_DATA_SUPPORT == STD_ON) */

    }
  }

  DBG_FRNM_RXINDICATION_EXIT(RxPduId,PduInfoPtr);
}

#if (FRNM_ENABLE_NM_RXVOTE == STD_ON)
STATIC FUNC(void, FRNM_CODE) FrNm_HandleRxVotePdu
(
  const uint8 instIdx,
  CONSTP2CONST(uint8, AUTOMATIC, FRNM_APPL_CONST) SduDataPtr
)
{
#if (FRNM_HSM_INST_MULTI_ENABLED == STD_OFF)
  TS_PARAM_UNUSED(instIdx);
#endif

  DBG_FRNM_HANDLERXVOTEPDU_ENTRY(instIdx,SduDataPtr);

  /* Received Pdu contains the Vote and may be Control Bit Vector,too.
   * The first byte can not be copied to internal buffer if schedule variant is not 7
   * as it might corrupt the Control Bit Vector bit already stored. Therefore, vote bit
   * need to be extracted and stored in the internal buffer if schedule variant is not 7.
   */

  /* Position of vote is last bit in the first byte.*/
  if ((SduDataPtr[0U] & 0x80U) == 0x80U)
  {
    /* Store this bit info in the internal buffer.*/
    FRNM_CHANNEL_CONFIG(instIdx).RxPduPtr[0U] |= 0x80U;
    /* Emit event to indicate a vote received to keep the cluster awake */
    FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_VOTE_RECEIVED);
  }
  else
  {
    /* Clear this bit info in the internal buffer.*/
    FRNM_CHANNEL_CONFIG(instIdx).RxPduPtr[0U] &= 0x7FU;
  }
  if ((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_PDU_SCHEDULE_VARIANT_MASK) ==
       FRNM_PDU_SCHEDULE_VARIANT_7)
  {
    /*Schedule variant is 7, hence first byte can be copied, Vote + CBV */
    FRNM_CHANNEL_CONFIG(instIdx).RxPduPtr[0U] = SduDataPtr[0U];
#if ((FRNM_REPEAT_MSG_BIT_ENABLED == STD_ON) && (FRNM_NODE_DETECTION_ENABLED == STD_ON))
     FrNm_HandleControlBitVector(FRNM_PL_SF(instIdx));
#endif
  }

  DBG_FRNM_HANDLERXVOTEPDU_EXIT(instIdx,SduDataPtr);
}
#endif

#if (FRNM_ENABLE_NM_RXDATA == STD_ON)
STATIC FUNC(void, FRNM_CODE) FrNm_HandleRxDataPdu
(
  const uint8 instIdx,
  CONSTP2CONST(uint8, AUTOMATIC, FRNM_APPL_CONST) SduDataPtr
)
{
#if (FRNM_HSM_INST_MULTI_ENABLED == STD_OFF)
  TS_PARAM_UNUSED(instIdx);
#endif

  DBG_FRNM_HANDLERXDATAPDU_ENTRY(instIdx,SduDataPtr);

  /* Received Pdu contains the Data and may be Control Bit Vector,too.
   * The first byte can not be copied to internal buffer as it might corrupt
   * the vote bit already stored. Also, if the schedule variant is 7,
   * then control bit vector is invalid with this data pdu. Therefore,
   * control bit vector need to be extracted and stored in the internal buffer
   * if schedule variant is not 7.
   */
   if ((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty &
        FRNM_PDU_SCHEDULE_VARIANT_MASK) != FRNM_PDU_SCHEDULE_VARIANT_7)
   {
     /* Position of control bit vector is first bit in the first byte.*/
     if ((SduDataPtr[0U] & 0x01U) == 0x01U)
     {
      /* Store this bit info in the internal buffer.*/
      FRNM_CHANNEL_CONFIG(instIdx).RxPduPtr[0U] |= 0x01U;
#if ((FRNM_REPEAT_MSG_BIT_ENABLED == STD_ON) && (FRNM_NODE_DETECTION_ENABLED == STD_ON))
      FrNm_HandleControlBitVector(FRNM_PL_SF(instIdx));
#endif
     }
     else
     {
      /* Clear this bit info in the internal buffer.*/
      FRNM_CHANNEL_CONFIG(instIdx).RxPduPtr[0U] &= 0xFEU;
     }
   }

   DBG_FRNM_HANDLERXDATAPDU_EXIT(instIdx,SduDataPtr);
}
#endif

#if (FRNM_ENABLE_NM_RXMIXED == STD_ON)
STATIC FUNC(void, FRNM_CODE) FrNm_HandleRxMixedPdu
(
  const uint8 instIdx,
  CONSTP2CONST(uint8, AUTOMATIC, FRNM_APPL_CONST) SduDataPtr
)
{
#if (FRNM_HSM_INST_MULTI_ENABLED == STD_OFF)
  TS_PARAM_UNUSED(instIdx);
#endif

  DBG_FRNM_HANDLERXMIXEDPDU_ENTRY(instIdx,SduDataPtr);

  /* Received Pdu contains Vote, Data and may be Control Bit Vector,too.*/

  /* Refer SWS 7.9 Schedule details, point 2, when both data and vote is transmitted
   * using same Pdu in dynamic segment (schedule variant 2), the presence (or non-presence)
   * of the PDU corresponds to the NM-Vote.
   * Therfore event to be emitted if the received pdu contains +ve vote OR if the pdu belongs
   * to schedule variant 2.
   */

  if (((SduDataPtr[0U] & 0x80U) == 0x80U) ||
      ((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty &
        FRNM_PDU_SCHEDULE_VARIANT_MASK) == FRNM_PDU_SCHEDULE_VARIANT_2)
     )
  {
    /* Emit event to indicate a vote received to keep the cluster awake */
    FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_VOTE_RECEIVED);
  }
  /* First byte can be copied as it won't harm anything since both data and vote
   * belongs to same pdu.
   */
  FRNM_CHANNEL_CONFIG(instIdx).RxPduPtr[0U] = SduDataPtr[0U];

  /* Check if repeat message request feature is supported.*/
#if ((FRNM_REPEAT_MSG_BIT_ENABLED == STD_ON) && (FRNM_NODE_DETECTION_ENABLED == STD_ON))
  FrNm_HandleControlBitVector(FRNM_PL_SF(instIdx));
#endif

  DBG_FRNM_HANDLERXMIXEDPDU_EXIT(instIdx,SduDataPtr);
}
#endif

/*------------------[FrNm_TriggerTransmit]-------------------------------*/
#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
FUNC(Std_ReturnType, FRNM_CODE) FrNm_TriggerTransmit
(
   PduIdType TxPduId,
   P2VAR(PduInfoType, AUTOMATIC, FRNM_APPL_DATA) PduInfoPtr
)
{
  Std_ReturnType retVal = E_NOT_OK;

  DBG_FRNM_TRIGGERTRANSMIT_ENTRY(TxPduId,PduInfoPtr);

  /* Excerpts from AUTOSAR_TPS_ECUConfiguration.pdf V3.2.0 (R4.0 Rev3)
   * section "4.4.1.2 Definition of Handle IDs":
   * Handle IDs are defined by the module providing the API and used by
   * the module calling the API. Handle IDs that are used in
   * callback functions (e.g. Tx Confirmation functions or Trigger
   * Transmit functions) shall be defined by the upper layer module.
   * Hence, TxPduId for 'FrNm_TriggerTransmit' shall be defined by
   * the parameter 'FrNmTxConfirmationPduId'.
   */

  /* Perform DET checks */
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (TxPduId >= FRNM_NUMBER_OF_TX_PDUS)
  {
    /* In case the incoming PduId is invalid, the associated instance can not be
     * determined, therefore instance 0 is reported to DET.
     */
    FRNM_DET_REPORT_ERROR(0U, FRNM_SERVID_TRIGGERTRANSMIT, FRNM_E_PDU_ID_INVALID);
  }
  else if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(FrNm_TxPduMap[TxPduId].ChannelHandle, FRNM_SERVID_TRIGGERTRANSMIT,
                           FRNM_E_UNINIT);
  }
  else if (
           (PduInfoPtr == NULL_PTR) ||
           (PduInfoPtr->SduDataPtr == NULL_PTR)
          )
  {
    /* Report FRNM_E_INVALID_POINTER.*/
    FRNM_DET_REPORT_ERROR(FrNm_TxPduMap[TxPduId].ChannelHandle,
      FRNM_SERVID_TRIGGERTRANSMIT, FRNM_E_INVALID_POINTER);
  }
  else
#endif
  {
     const uint8 instIdx = FrNm_TxPduMap[TxPduId].ChannelHandle;
     PduLengthType PduLength;

#if (FRNM_ENABLE_NMVOTE == STD_ON)
    if (FrNm_TxPduMap[TxPduId].PduType == FRNM_VOTE)
    {
      /* The Pdu is of type vote and hence only the first byte
       * need to be copied. But the first byte can be copied
       * only if the schedule variant is 7. If the schedule variant
       * is not 7 then the control bit vector (if supported) need to be send
       * with Pdu of type data or pdu of type 'data and vote'.
       */

      if ((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_PDU_SCHEDULE_VARIANT_MASK) !=
           FRNM_PDU_SCHEDULE_VARIANT_7)
      {
        /* Position of vote is last bit in the first byte.*/
        if ((FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] & 0x80U) == 0x80U)
        {
          /* Store this bit info in the transmit buffer.*/
          PduInfoPtr->SduDataPtr[0U] |= 0x80U;
        }
        else
        {
          /* Clear this bit info in the transmit buffer.*/
          PduInfoPtr->SduDataPtr[0U] &= 0x7FU;
        }
      }
      else
      {
        /*schedule variant is 7, hence the first can be completely copied */
        PduInfoPtr->SduDataPtr[0U] |= FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] ;
      }
      PduInfoPtr->SduLength = 1U;
    }
    else
#endif /* (FRNM_ENABLE_NMVOTE == STD_ON) */
    {
      /* Store data pointer temporarily */
      P2VAR(uint8, AUTOMATIC, AUTOSAR_COMSTACKDATA) DataPtr = PduInfoPtr->SduDataPtr;
      /* Increment pointer to data byte location to copy data bytes from PduR or from
       * internal transmit buffer.Here pointer address is changing tempororily to
       * avoid additional buffer and mem copy since PduR interface also has PduInfoType pointer.
       */

#if (FRNM_COM_USER_DATA_SUPPORT == STD_ON)
      if (FrNm_ComUserDataInfo[instIdx].ComUserTxPduEnabled == TRUE)
      {
        /* PduLength in com user data holds only length corresponds to user data */
        PduLength = FrNm_ComUserDataInfo[instIdx].PduLength;
      }
      else
#endif
      {
        /* Com user data is not supported for this channel */
        /* PduLength in FrNm_TxPduMap holds length corresponds to user data and FrNm header bytes
         * (Vote,SID) */
        PduLength = FrNm_TxPduMap[TxPduId].PduLength - FRNM_RESERVED_BYTES;
      }

      PduInfoPtr->SduDataPtr = &DataPtr[FRNM_RESERVED_BYTES];
      /* PduLength is ensured to be greater than or equal to FRNM_RESERVED_BYTES through Xpath checks */
      PduInfoPtr->SduLength = PduLength;
      /* Copy Pdu Data bytes */
      FrNm_CopyPduData(instIdx,PduInfoPtr);

      /* Relocate SduDataPtr and length to store vote/cbv and source node id */
      PduInfoPtr->SduDataPtr = DataPtr;
      PduInfoPtr->SduLength = PduLength + FRNM_RESERVED_BYTES;

#if (FRNM_ENABLE_NMDATA == STD_ON)

      if (FrNm_TxPduMap[TxPduId].PduType == FRNM_DATA)
      {
        /* Pdu corresponds to  the Data and may be Control Bit Vector,too.
         * if schedule variant is not 7, then control bit vector also need to be sent with
         * data pdu. The first byte can not be copied from internal buffer as it might also
         * have the vote bit already stored. Therefore, control bit vector need to be
         * extracted and stored in the transmit buffer.
         */
        if ((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty &
             FRNM_PDU_SCHEDULE_VARIANT_MASK) != FRNM_PDU_SCHEDULE_VARIANT_7)
        {
          /* Clear vote/Cbv byte */
          PduInfoPtr->SduDataPtr[0U] = 0U;
          /* Position of control bit vector is first bit in the first byte.*/
          PduInfoPtr->SduDataPtr[0U] |= FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] &
                                        FRNM_PDU_REPEAT_MSG_BIT;
#if (FRNM_PN_ENABLED == STD_ON)
          /* Copy CRI bit too */
          PduInfoPtr->SduDataPtr[0U] |= FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] &
                                        FRNM_CBV_PNINFOBIT_MASK;
#endif
        }
      }
#endif /* (FRNM_ENABLE_NMDATA == STD_ON) */
#if (FRNM_ENABLE_NMMIXED == STD_ON)
      if (FrNm_TxPduMap[TxPduId].PduType == FRNM_MIXED)
      {
        /* Pdu correspnds to Vote, Data and may be Control Bit Vector,too.
         */
        /* Copy the first byte as it contains the vote and control bit vector info.
         */
        PduInfoPtr->SduDataPtr[0U] = FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U];
      }
#endif /* (FRNM_ENABLE_NMMIXED == STD_ON) */

#if (FRNM_SOURCE_NODE_IDENTIFIER_ENABLED == STD_ON)
   PduInfoPtr->SduDataPtr[1U]=FRNM_CHANNELID_CONFIG(instIdx).NodeId;
#endif
    }
    retVal  = E_OK;
  }

  DBG_FRNM_TRIGGERTRANSMIT_EXIT(retVal,TxPduId,PduInfoPtr);
  return retVal;
}
#else /* (FRNM_PASSIVE_MODE_ENABLED == STD_ON) */
FUNC(Std_ReturnType, FRNM_CODE) FrNm_TriggerTransmit
(
   PduIdType TxPduId,
  /* Deviation MISRA-1 */
   P2VAR(PduInfoType, AUTOMATIC, FRNM_APPL_DATA) PduInfoPtr
)
{
  TS_PARAM_UNUSED(TxPduId);
  TS_PARAM_UNUSED(PduInfoPtr);
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  FRNM_DET_REPORT_ERROR(FRNM_INSTANCE_ID, FRNM_SERVID_TRIGGERTRANSMIT, FRNM_E_PDU_ID_INVALID);
#endif
  return E_NOT_OK;
}
#endif /* (FRNM_PASSIVE_MODE_ENABLED == STD_OFF) */
/*------------------[FrNm_TxConfirmation]--------------------------------*/
#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
FUNC(void, AUTOMATIC) FrNm_TxConfirmation
(
   PduIdType TxPduId
)
{
#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) && \
     (FRNM_DEV_ERROR_DETECT == STD_OFF) && \
     (FRNM_COM_USER_DATA_SUPPORT == STD_OFF))
  TS_PARAM_UNUSED(TxPduId);
#endif

  DBG_FRNM_TXCONFIRMATION_ENTRY(TxPduId);

  /* Perform Det checks */
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (TxPduId >= FRNM_NUMBER_OF_TX_PDUS)
  {
    /* In case the incoming PduId is invalid, the associated instance can not be
     * determined, therefore instance 0 is reported to DET.
     */
    FRNM_DET_REPORT_ERROR(0U, FRNM_SERVID_TXCONFIRMATION, FRNM_E_PDU_ID_INVALID);
  }
  else if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(FrNm_TxPduMap[TxPduId].ChannelHandle, FRNM_SERVID_TXCONFIRMATION,
                           FRNM_E_UNINIT);
  }
  else
#endif
  {
#if (FRNM_COM_USER_DATA_SUPPORT == STD_ON)
    /* If the PDU belongs to NM-Vote type, it was not triggerd by PduR */
    if (FrNm_TxPduMap[TxPduId].PduType != FRNM_VOTE)
    {
      const uint8 instIdx = FrNm_TxPduMap[TxPduId].ChannelHandle;
      if (FrNm_ComUserDataInfo[instIdx].ComUserTxPduEnabled == TRUE)
      {
        /* Forward Tx confirmation with PDU Id defined by PduR */
        PduR_FrNmTxConfirmation(FrNm_ComUserDataInfo[instIdx].ComUserTxConfPduId);
      }
    }
#endif /* (FRNM_COM_USER_DATA_SUPPORT == STD_ON) */
    /* Emit event to reset transmission error detection timer */
    FRNM_EMIT(FrNm_TxPduMap[TxPduId].ChannelHandle, FRNM_HSM_FRNM_EV_TX_CONFIRMATION);
  }

  DBG_FRNM_TXCONFIRMATION_EXIT(TxPduId);
}
#else /* (FRNM_PASSIVE_MODE_ENABLED == STD_ON) */
FUNC(void, AUTOMATIC) FrNm_TxConfirmation
(
   PduIdType TxPduId
)
{
    TS_PARAM_UNUSED(TxPduId);
#if (FRNM_DEV_ERROR_DETECT == STD_ON)
    FRNM_DET_REPORT_ERROR(FRNM_INSTANCE_ID, FRNM_SERVID_TXCONFIRMATION, FRNM_E_PDU_ID_INVALID);
#endif	
}
#endif /* (FRNM_PASSIVE_MODE_ENABLED == STD_OFF) */

/*------------------[FrNm_SetSleepReadyBit]--------------------------------*/
#if (FRNM_COORDINATOR_SYNC_SUPPORT == STD_ON)
FUNC(Std_ReturnType, FRNM_CODE) FrNm_SetSleepReadyBit
(
  const NetworkHandleType nmChannelHandle,
  const boolean nmSleepReadyBit
)
{
  Std_ReturnType retVal = E_NOT_OK;

#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) && (FRNM_DEV_ERROR_DETECT == STD_OFF))
  TS_PARAM_UNUSED(nmChannelHandle);
#else
  const uint8 instIdx = FrNm_GetClusterIndex(nmChannelHandle);
#endif

  DBG_FRNM_SETSLEEPREADYBIT_ENTRY(nmChannelHandle,nmSleepReadyBit);

#if (FRNM_DEV_ERROR_DETECT == STD_ON)
  if (FrNm_InitStatus == FRNM_UNINIT)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_SETSLEEPREADYBIT, FRNM_E_UNINIT);
  }
  else if (instIdx >= FRNM_NUMBER_OF_CHANNELS)
  {
    FRNM_DET_REPORT_ERROR(
      instIdx, FRNM_SERVID_SETSLEEPREADYBIT, FRNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {
    /* FRNM398 */
    if (nmSleepReadyBit == TRUE)
    {
      FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] |= FRNM_CBV_SLEEPREADYBIT_MASK;
    }
    else
    {
      FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] &= (uint8) (~FRNM_CBV_SLEEPREADYBIT_MASK);
    }
    retVal = E_OK;
  }

  DBG_FRNM_SETSLEEPREADYBIT_EXIT(retVal,nmChannelHandle,nmSleepReadyBit);
  return retVal;
}
#endif /* (FRNM_COORDINATOR_SYNC_SUPPORT == STD_ON) */
/*==================[internal functions definitions]=====================*/

/************************** [FrNm_HandleVoteCycle]*************************************/
#if ((FRNM_PASSIVE_MODE_ENABLED == STD_OFF) \
    && ((FRNM_ENABLE_NMVOTE == STD_ON) \
     || (FRNM_ENABLE_NMMIXED == STD_ON)))
FUNC(void, FRNM_CODE) FrNm_HandleVoteCycle
(
  FRNM_PDL_SF(const uint8 instIdx)
)
{
  PduLengthType PduLength;
  uint8 PduIndex;
#if (FRNM_HSM_INST_MULTI_ENABLED != STD_ON)
  uint8 instIdx = 0U;
#endif
  DBG_FRNM_HANDLEVOTECYCLE_ENTRY(FRNM_PDL_SF);

  /* Vote: Only positive votes are transmitted in the voting cycle.
   * When in ReadySleepState, the vote are cleared and is transmitted
   * through FrNm_TriggerTransmit. That is, there is no explicit
   * transmission for 'negative' vote.
   */
  /* Data: If the pdu schedule variant is 1 or 2, then both data and vote
   * are transmitted in the same pdu. In this case pdus are sent only
   * on voting cycle and not in data cycle.
   */
   if (((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_PDU_SCHEDULE_VARIANT_MASK) ==
         FRNM_PDU_SCHEDULE_VARIANT_1) ||
       ((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_PDU_SCHEDULE_VARIANT_MASK) ==
         FRNM_PDU_SCHEDULE_VARIANT_2)
      )
  {
    /* Assign the data byte offset position to copy data bytes either from
     * Com using PduR or from the internal transmit buffer being filled with
     * FrNm_SetUserData().
     */

#if (FRNM_COM_USER_DATA_SUPPORT == STD_ON)
      if (FrNm_ComUserDataInfo[instIdx].ComUserTxPduEnabled == TRUE)
      {
        /* PduLength in com user data holds only length corresponds to user data */
        PduLength = FrNm_ComUserDataInfo[instIdx].PduLength;
      }
      else
#endif
      {
        /* Com user data is not supported for this channel*/
        /* If dual channels are configured, length of corresponding PDUs must be equal.
         * Therefore, use first TxPduId to derive the PduLength.
         */
        PduIndex = FrNm_GetPduIndex(FRNM_CHANNELID_CONFIG(instIdx).TxVotePduId[0U]);
        /* PduLength in FrNm_TxPduMap holds length corresponds to user data and FrNm header bytes
         * (Vote,SID) */
        PduLength = FrNm_TxPduMap[PduIndex].PduLength - FRNM_RESERVED_BYTES;
      }

    FrNm_PduInfo.SduDataPtr = &FrNm_PduBuffer[FRNM_RESERVED_BYTES];
    FrNm_PduInfo.SduLength = PduLength;

    /* Copy Pdu Data bytes */
    FrNm_CopyPduData(instIdx,&FrNm_PduInfo);

#if (FRNM_SOURCE_NODE_IDENTIFIER_ENABLED == STD_ON)
    FrNm_PduBuffer[1U]=FRNM_CHANNELID_CONFIG(instIdx).NodeId;
#endif

    /* Copy vote and cbv byte*/
    FrNm_PduBuffer[0U] = FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U];

    /* SduDataPtr and length need to be relocated before passing to FrIf*/
    FrNm_PduInfo.SduDataPtr = FrNm_PduBuffer;
    FrNm_PduInfo.SduLength = PduLength + FRNM_RESERVED_BYTES;
  }
  /* If Schedule variant 7 copy first byte as it contains the control bit vector,too.*/
  else if ((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_PDU_SCHEDULE_VARIANT_MASK) ==
            FRNM_PDU_SCHEDULE_VARIANT_7)
  {
    FrNm_PduBuffer[0U] = FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U];
    FrNm_PduInfo.SduDataPtr = FrNm_PduBuffer;
    FrNm_PduInfo.SduLength = 1U;
  }
  else
  {
    /* Copy only the vote bit, here vote bit can be directly set to 0x80, too. */
    FrNm_PduBuffer[0U] |= (FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0] & 0x80U);
    FrNm_PduInfo.SduDataPtr = FrNm_PduBuffer;
    FrNm_PduInfo.SduLength = 1U;
  }

  /* If there are double channels configured, then more than one vote pdu might be configured
   * for same channel and in this case transmission to be done for each pdu.
   */
#if (FRNM_DUAL_CHANNEL_PDU_ENABLE == STD_ON)
  for(PduIndex = 0U; PduIndex < FRNM_CHANNELID_CONFIG(instIdx).NoOfTxVotePdus; PduIndex++)
  {
    /* The behaviour if transmit function returns NOT_OK is not described in SWS.
     * Though, transmission error is detected by checking whether confirmation is
     * is received or not.
     */
    (void)FrIf_Transmit(FRNM_CHANNELID_CONFIG(instIdx).TxVotePduId[PduIndex], &FrNm_PduInfo);
  }
#else
  (void)FrIf_Transmit(FRNM_CHANNELID_CONFIG(instIdx).TxVotePduId[0U], &FrNm_PduInfo);
#endif

  DBG_FRNM_HANDLEVOTECYCLE_EXIT(FRNM_PDL_SF);
}
#endif /* ((FRNM_PASSIVE_MODE_ENABLED == STD_OFF) && ........))*/

/************************** [FrNm_HandleDataCycle]*************************************/
#if ((FRNM_PASSIVE_MODE_ENABLED == STD_OFF) && (FRNM_ENABLE_NMDATA == STD_ON))
FUNC(void, FRNM_CODE) FrNm_HandleDataCycle
(
  FRNM_PDL_SF(const uint8 instIdx)
)
{
  PduLengthType PduLength;
  uint8 PduIndex;
#if (FRNM_HSM_INST_MULTI_ENABLED != STD_ON)
  uint8 instIdx = 0U;
#endif
  DBG_FRNM_HANDLEDATACYCLE_ENTRY(FRNM_PDL_SF);
  /* If the pdu schedule variant is 1 or 2, then both data and vote
   * are transmitted only in the vote cycle.
   */
   if (((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_PDU_SCHEDULE_VARIANT_MASK) !=
         FRNM_PDU_SCHEDULE_VARIANT_1) &&
       ((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_PDU_SCHEDULE_VARIANT_MASK) !=
         FRNM_PDU_SCHEDULE_VARIANT_2)
      )
  {
    /* Assign the data byte offset position to copy data bytes either from
     * Com using PduR or from the internal transmit buffer being filled with
     * FrNm_SetUserData().
     */
#if (FRNM_COM_USER_DATA_SUPPORT == STD_ON)
      if (FrNm_ComUserDataInfo[instIdx].ComUserTxPduEnabled == TRUE)
      {
        /* PduLength in com user data holds only length corresponds to user data */
        PduLength = FrNm_ComUserDataInfo[instIdx].PduLength;
      }
      else
#endif
      {
        /* Com user data is not supported for this channel*/
        /* PduLength in FrNm_TxPduMap holds length corresponds to user data and FrNm header bytes
         * (Vote,SID) */
        /* If dual channels are configured, length of corresponding PDUs must be equal.
         * Therefore, use first TxPduId to derive the PduLength.
         */
        PduIndex = FrNm_GetPduIndex(FRNM_CHANNELID_CONFIG(instIdx).TxDataPduId[0U]);
        PduLength = FrNm_TxPduMap[PduIndex].PduLength - FRNM_RESERVED_BYTES;
      }
    FrNm_PduInfo.SduDataPtr = &FrNm_PduBuffer[FRNM_RESERVED_BYTES];
    FrNm_PduInfo.SduLength = PduLength;

    /* Copy Pdu Data bytes */
    FrNm_CopyPduData(instIdx,&FrNm_PduInfo);

#if (FRNM_SOURCE_NODE_IDENTIFIER_ENABLED == STD_ON)
    FrNm_PduBuffer[1U]=FRNM_CHANNELID_CONFIG(instIdx).NodeId;
#endif

    /* If pdu schedule variant is not 1 or 2 or 7, then control bit vector
     * to be copied in to the transmit buffer.
     */
    /* clear vote/cbv byte*/
    FrNm_PduBuffer[0U] = 0U;
    /* CRI bit shall not be cleared if PN feature is enabled */
#if (FRNM_PN_ENABLED == STD_ON)
    FrNm_PduBuffer[0] |= (FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0] & FRNM_CBV_PNINFOBIT_MASK);
#endif
    if ((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_PDU_SCHEDULE_VARIANT_MASK) !=
            FRNM_PDU_SCHEDULE_VARIANT_7)
    {
      FrNm_PduBuffer[0] |= (FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0] & 0x01U);
    }
    /* SduDataPtr and length need to be relocated before passing to FrIf*/
    FrNm_PduInfo.SduDataPtr = FrNm_PduBuffer;
    FrNm_PduInfo.SduLength = PduLength + FRNM_RESERVED_BYTES;

    /* If there are double channels configured, then more than one vote pdu might be configured
     * for same channel and in this case transmission to be done for each pdu.
     */
#if (FRNM_DUAL_CHANNEL_PDU_ENABLE == STD_ON)
      for(PduIndex = 0U; PduIndex < FRNM_CHANNELID_CONFIG(instIdx).NoOfTxDataPdus; PduIndex++)
    {
      /* The behaviour if transmit function returns NOT_OK is not described in SWS.
       * Though, transmission error is detected by checking whether confirmation is
       * is received or not.
       */
      (void)FrIf_Transmit(FRNM_CHANNELID_CONFIG(instIdx).TxDataPduId[PduIndex], &FrNm_PduInfo);
    }
#else
    (void)FrIf_Transmit(FRNM_CHANNELID_CONFIG(instIdx).TxDataPduId[0U], &FrNm_PduInfo);
#endif
  }

  DBG_FRNM_HANDLEDATACYCLE_EXIT(FRNM_PDL_SF);
}
#endif /*((FRNM_PASSIVE_MODE_ENABLED == STD_OFF) && (FRNM_ENABLE_NMDATA == STD_ON))*/

/************************** [FrNm_CopyPduData]*************************************/
#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
STATIC FUNC(void, FRNM_CODE) FrNm_CopyPduData
(
  const uint8 instIdx,
  P2VAR(PduInfoType, AUTOMATIC, FRNM_APPL_DATA) PduInfoPtr
)
{
#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_OFF) || (FRNM_COM_USER_DATA_SUPPORT == STD_ON))
TS_PARAM_UNUSED(instIdx);
#endif

  DBG_FRNM_COPYPDUDATA_ENTRY(instIdx,PduInfoPtr);

#if (FRNM_COM_USER_DATA_SUPPORT == STD_ON)

  if (FrNm_ComUserDataInfo[instIdx].ComUserTxPduEnabled == TRUE)
  {
    if (E_OK == PduR_FrNmTriggerTransmit(FrNm_ComUserDataInfo[instIdx].ComUserTxConfPduId,
                                         PduInfoPtr
                                        )
       )
    {
      /* Now, data has been copied to the FrNm_PduBuffer or to the buffer
       * provided by FrIf through FrNm_TriggerTransmit().
       * Here, FrNm_PduBuffer or buffer from FrIf is used to receive data
       * from Com instead of directly using TxPduPtr. This is to avoid call of
       * PduR_FrNmTriggerTransmit() within a critical section as TxPduPtr
       * is accessed at more than one place.
       */
      SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
      /* Copy the NM data bytes received from Com to the internal buffer.*/
      TS_MemCpy(
            &FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[FRNM_RESERVED_BYTES],
            &PduInfoPtr->SduDataPtr[0U],
            PduInfoPtr->SduLength);
      SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    }
  }
  else
#endif /* (FRNM_COM_USER_DATA_SUPPORT == STD_ON) */
  {
    SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

    /* Copy the NM data bytes from the internal buffer */
    TS_MemCpy(
         &PduInfoPtr->SduDataPtr[0U],
          &FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[FRNM_RESERVED_BYTES],
          PduInfoPtr->SduLength);
    SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  }
#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
  if (FRNM_CHANNELID_CONFIG(instIdx).PnEnabled == TRUE)
  {
    uint8 PnIndex;
    /* Eira is accessed in main function and RxIndication too, hence protect the copying */
    SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    for (PnIndex=0U; PnIndex < FRNM_PN_INFO_LENGTH; PnIndex++)
    {
      /* Aggregate external and internal requests */
      FRNM_CHANNEL_STATUS(instIdx).PnInfoEira[PnIndex] |=
                 FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[FRNM_PN_INFO_OFFSET + PnIndex];
    }
    SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  }
#endif

  DBG_FRNM_COPYPDUDATA_EXIT(instIdx,PduInfoPtr);
}
#endif

/************************** [FrNm_HandleSynchError]*************************************/
FUNC(void, FRNM_CODE) FrNm_HandleSynchError
(
  FRNM_PDL_SF(const uint8 instIdx)
)
{
  /* The behaviour of start up error is not clearly described in SWS.
   * Refer http://www.autosar.org/bugzilla/show_bug.cgi?id=51436.
   * Following cases are considered as special cases.
   *
   * Case 1. Repeat message state (and when cycle counter emulation is ON):
   * The reaction upon synchronous error in network mode (when cycle counter emulation is OFF) is
   * to enter synchronous mode.
   * The reaction upon synchronous error in Repeat message state (when cycle counter emulation is
   * ON) is to enter bus sleep mode.
   *
   * Case 2. Synchronous state:
   * The reaction upon synchronous error in synchronous state (if network is not in requested
   * state) is to enter bus sleep mode. But, synchronous state is the target state if error occures
   * in network mode.
   *
   * Notes:
   * SWS doesn't say reaction upon error while reading global time when FrNm is in the synchronize
   * state. Therefore, synchronize state must ignore error of global time and consider only
   * FrNm_StartUpError().
   *
   * The reaction upon synchronous error in ready sleep state is not clear, therefore we consider
   * it same as of the behaviour when cycle counter emulation is OFF. i,e state will change to
   * synchronous mode.
   *
   * Because of the above mentioned points it is assumed that synchronous error is a kind of event
   * for which action to be taken immediately and the error status should  not be kept for
   * evaluating at a later point of time.
   *
   * The rationale behind the rules mentioned in SWS are not clear and expect these rules would
   * change in future autosar releases.
   */

  /* To implement the special cases mentioned above, we check the current state here to emit
   * relevant events.
   */

  DBG_FRNM_HANDLESYNCHERROR_ENTRY(FRNM_PDL_SF);
#if (FRNM_CYCLE_COUNTER_EMULATION == STD_ON)
  if (FRNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_REPEAT_MESSAGE)
  {
    /* Emit event to indicate startup/synch error to enter into bus sleep state */
    FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_FAILSAFE_SLEEP_MODE);
  }
  else
#endif
  {
    /* Emit event to indicate startup/synch error to enter into synchronize state */
    FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_FAILSAFE_SYNCH_MODE);
  }

  /* Stop data or vote transmission.
   * This bit is used by Enable/Disable communication service as well.
   * This bit is cleared upon entry of network mode and doesn't conflict with above services as
   * the enable/disable services are allowed to set/clear this bit only in the network mode.
   */

  FRNM_CHANNEL_STATUS(instIdx).ComEnabled = FALSE;

  /* Stop all timers irrespective of whether they are runing or not */
  FRNM_CHANNEL_STATUS(instIdx).UniversalTimer = 0U;
  FRNM_CHANNEL_STATUS(instIdx).TimeoutTimer = 0U;
  FRNM_CHANNEL_STATUS(instIdx).ReadySleepTimer = 0U;

  DBG_FRNM_HANDLESYNCHERROR_EXIT(FRNM_PDL_SF);
}
/************************** [FrNm_HandleControlBitVector]*************************************/
#if ((FRNM_REPEAT_MSG_BIT_ENABLED == STD_ON) && (FRNM_NODE_DETECTION_ENABLED == STD_ON))
STATIC FUNC(void, FRNM_CODE) FrNm_HandleControlBitVector
(
  FRNM_PDL_SF(const uint8 instIdx)
)
{
  /* Check if repeat message request feature is supported.*/

  DBG_FRNM_HANDLECONTROLBITVECTOR_ENTRY(FRNM_PDL_SF);
  if (((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_REPEAT_MESSAGE_BIT) ==
        FRNM_REPEAT_MESSAGE_BIT) &&
      ((FRNM_CHANNEL_CONFIG(instIdx).RxPduPtr[0U] & 0x01U) == 0x01U)
     )
  {
    /* Enter critical section to protect from concurrent access
     * to different bits in 'ChanStatus' in different APIs.
     */
    SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    /* Set the channel status. No event being emitted */
    FRNM_CHANNEL_STATUS(instIdx).ChanStatus |= FRNM_REPEAT_MSG_REQUESTED;
    SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  }

  DBG_FRNM_HANDLECONTROLBITVECTOR_EXIT(FRNM_PDL_SF);
}
#endif /*((FRNM_REPEAT_MSG_BIT_ENABLED == STD_ON) && (FRNM_NODE_DETECTION_ENABLED == STD_ON))*/
/************************** [FrNm_HandleTimers   ]*************************************/

STATIC FUNC(void, FRNM_CODE) FrNm_HandleTimers
(
  FRNM_PDL_SF(const uint8 instIdx)
)
{

  DBG_FRNM_HANDLETIMERS_ENTRY(FRNM_PDL_SF);
  if (FRNM_CHANNEL_STATUS(instIdx).UniversalTimer > 0U)
  {
    --FRNM_CHANNEL_STATUS(instIdx).UniversalTimer;
    if (FRNM_CHANNEL_STATUS(instIdx).UniversalTimer == 0U)
    {
      FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_UNI_TIMEOUT);
    }
  }

#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
  if (FRNM_CHANNEL_STATUS(instIdx).TimeoutTimer > 0U)
  {
    --FRNM_CHANNEL_STATUS(instIdx).TimeoutTimer;
    if (FRNM_CHANNEL_STATUS(instIdx).TimeoutTimer == 0U)
    {
      FRNM_EMIT(instIdx, FRNM_HSM_FRNM_EV_TX_TIMEOUT);
    }
  }
#endif
  if (0U == (FRNM_CHANNEL_STATUS(instIdx).ActCycle %
             FRNM_CHANNELTIME_CONFIG(instIdx).RepititionCycle
            )
     )
  {
    if (FRNM_CHANNEL_STATUS(instIdx).ReadySleepTimer > 0U)
    {
      /* Note from SWS:
       * As all transitions regarding the FrNmReadySleepCnt are guarded and the order of evaluation
       * is implicitly defined by the guards - the FrNmReadySleepCnt is only decremented if no Repeat
       * Message Request is active (FrNm_RepeatMessage is set to FALSE), the network has been released
       * (FrNm_NetworkRequested is set to FALSE) and if no Cluster awake vote has been received on
       * the bus.
       */

      /* Note from SWS :
       * A local request to keep the network awake (i.e., Network Request) will be Ignored in the
       * Ready Sleep State when the FrNmReadySleepCnt has already reached 0.
       */

      /* We ignore local network requests, be it NetworkRequest or RepeatMessageRequest
       * at the time when FrNmReadySleepCnt reach 0.
       */
      --FRNM_CHANNEL_STATUS(instIdx).ReadySleepTimer;
    }
  }
  /* FrNmNodeDetectionLock timer shall be considered later while implementing relevant feature. */

  DBG_FRNM_HANDLETIMERS_EXIT(FRNM_PDL_SF);
}

/************************** [FrNm_GetClusterIndex   ]*************************************/

#if ((FRNM_HSM_INST_MULTI_ENABLED == STD_ON) || \
     (FRNM_DEV_ERROR_DETECT == STD_ON)       || \
     (FRNM_COM_USER_DATA_SUPPORT == STD_ON)     \
    )
STATIC FUNC(uint8, FRNM_CODE) FrNm_GetClusterIndex
(
  CONST(NetworkHandleType, AUTOMATIC) NetworkHandle
)

{
  uint8_least instIdx;

  DBG_FRNM_GETCLUSTERINDEX_ENTRY(NetworkHandle);

  for (instIdx = 0U; instIdx < FRNM_NUMBER_OF_CHANNELS; instIdx++)
  {
    if (FRNM_CHANNEL_CONFIG(instIdx).ChannelIdentifiers.ChannelId == NetworkHandle)
    {
      break;
    }
  }

  DBG_FRNM_GETCLUSTERINDEX_EXIT((uint8)instIdx,NetworkHandle);
  return (uint8)instIdx;
}
#endif /*((FRNM_HSM_INST_MULTI_ENABLED == STD_ON) || (FRNM_DEV_ERROR_DETECT == STD_ON))*/

/************************** [FrNm_GetPduIndex]*************************************/
#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
STATIC FUNC(uint8, FRNM_CODE) FrNm_GetPduIndex
(
  CONST(PduIdType, AUTOMATIC) PduId
)
{
  uint8_least instIdx = 0U;

  DBG_FRNM_GETPDUINDEX_ENTRY(PduId);

  while ((FrNm_TxPduMap[instIdx].TxPduId != PduId)&&
         (instIdx < FRNM_NUMBER_OF_TX_PDUS)
        )
  {
    instIdx++;
  }

  DBG_FRNM_GETPDUINDEX_EXIT((uint8)instIdx,PduId);
  return (uint8)instIdx;
}
#endif

#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
STATIC FUNC(void, FRNM_CODE) FrNm_HandlePn
(
  void
)
{
  uint8 Index;
  boolean PnEiraChanged;

  DBG_FRNM_HANDLEPN_ENTRY();

 /* Process Rx and Tx buffers for all channels for calculating EIRA */
  for (Index = 0U; Index < FRNM_NUMBER_OF_CHANNELS; Index++)
  {
    VAR (uint8, AUTOMATIC) PnInfoEira[FRNM_PN_INFO_LENGTH];

    /* Protect FrNm_PnStatus from concurrent access in different
     * Channel specific main functions.
     */
    SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    /* copy and clear external and internal requests */
    TS_MemCpy(PnInfoEira, FRNM_CHANNEL_STATUS(Index).PnInfoEira, FRNM_PN_INFO_LENGTH);
    TS_MemSet(FRNM_CHANNEL_STATUS(Index).PnInfoEira, 0U, FRNM_PN_INFO_LENGTH);
    PnEiraChanged = FrNm_HandlePnEira(PnInfoEira);
    SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

    if (PnEiraChanged == TRUE)
   {
      PduInfoType pduInfo;

      pduInfo.SduDataPtr = FrNm_PnStatus.EiraValue;
      pduInfo.SduLength = FRNM_PN_INFO_LENGTH;
      PduR_FrNmRxIndication(FRNM_PN_EIRA_PDUID, &pduInfo);
    }
  }

  DBG_FRNM_HANDLEPN_EXIT();
}

STATIC FUNC(boolean, FRNM_CODE) FrNm_HandlePnEira
(
  CONSTP2CONST(uint8, AUTOMATIC, FRNM_APPL_CONST) MessageBuffer
)
{
  uint8 PnIndex;
  boolean EiraChanged = FALSE;

  DBG_FRNM_HANDLEPNEIRA_ENTRY(MessageBuffer);
  for (PnIndex = 0U; PnIndex < FRNM_PN_INFO_LENGTH; PnIndex++)
  {
    if ((MessageBuffer[PnIndex] & FrNm_PnFilterMask[PnIndex]) != 0U)
    {
      uint8 PnBitPos;

      for (PnBitPos = 0U; PnBitPos < 8U; PnBitPos++)
      {
        uint8 PnBitMask = 1U << PnBitPos;

        if ((MessageBuffer[PnIndex] & PnBitMask) != 0U)
        {
          if ((FrNm_PnStatus.EiraValue[PnIndex] & PnBitMask) == 0U)
          {
            FrNm_PnStatus.EiraValue[PnIndex] |= PnBitMask;
            EiraChanged = TRUE;
          }
          /* load timer value by mapping through FrNm_EiraTimerMap*/
          if ((FrNm_EiraTimerMap[(PnIndex * 8U) + PnBitPos]) != FRNM_EIRA_INVALID_INDEX)
          {
            FrNm_PnStatus.EiraTimer[FrNm_EiraTimerMap[(PnIndex * 8U) + PnBitPos]] =
              FRNM_PN_RESET_TIME;
          }
        }
      }
    }
  }

  DBG_FRNM_HANDLEPNEIRA_EXIT(EiraChanged, MessageBuffer);

  return EiraChanged;
}

STATIC FUNC(boolean, FRNM_CODE) FrNm_AggregateEira
(
  const uint8 instIdx,
  CONSTP2CONST(uint8, AUTOMATIC, FRNM_APPL_CONST) PnInfo
)
{
  uint8 PnIndex;
  uint8 PnInfoEira;
  boolean ValidMessage = FALSE;

#if (FRNM_HSM_INST_MULTI_ENABLED == STD_OFF)
TS_PARAM_UNUSED(instIdx);
#endif

  DBG_FRNM_AGGREGATEEIRA_ENTRY(instIdx,PnInfo);

  for (PnIndex = 0U; PnIndex < FRNM_PN_INFO_LENGTH; PnIndex++)
  {
    PnInfoEira = PnInfo[PnIndex] & FrNm_PnFilterMask[PnIndex];
    /* Aggregate external and internal requests */
    FRNM_CHANNEL_STATUS(instIdx).PnInfoEira[PnIndex] |= PnInfoEira;

    if (PnInfoEira != 0U)
    {
      ValidMessage = TRUE;
    }
  }

  DBG_FRNM_AGGREGATEEIRA_EXIT((ValidMessage),instIdx,PnInfo);
  return(ValidMessage);
}

FUNC(void, FRNM_CODE) FrNm_HandlePnTimers
(
  void
)
{
  uint8 PnIndex;
  boolean EiraChanged = FALSE;

  DBG_FRNM_HANDLEPNTIMERS_ENTRY();

  for (PnIndex = 0U; PnIndex < FRNM_PN_EIRA_TIMER_SIZE; PnIndex++)
  {
    uint8 EiraIndex = FrNm_EiraTimerMap[PnIndex];

    if (EiraIndex != FRNM_EIRA_INVALID_INDEX)
    {
      /* Valid index; The Eira Timer can be accessed using this index */
      if (FrNm_PnStatus.EiraTimer[EiraIndex] > 0U)
      {
        /* Protect FrNm_PnStatus from concurrent access in different
         * Channel specific main functions.
         */
        SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

        FrNm_PnStatus.EiraTimer[EiraIndex]--;

        if (FrNm_PnStatus.EiraTimer[EiraIndex] == 0U)
        {
          /* Decrement PN timer.
           * Timers in EiraTimer holds values corresponding to each PN (each bit) by
           * mapping through FrNm_EiraTimerMap.
           * For example EiraTimer[0] is the timer for PN0 (Bit 0 of EiraValue[0]) and
           * EiraTimer[9] is the timer for PN9 (Bit 1 of EiraValue[1]).
           */
          FrNm_PnStatus.EiraValue[PnIndex/8U] &= (uint8)~((uint8)(1U << (PnIndex % 8U)));
          EiraChanged = TRUE;
        }

        SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
      }
    }
  }
  if (EiraChanged == TRUE)
  {
    PduInfoType pduInfo;
    pduInfo.SduDataPtr = FrNm_PnStatus.EiraValue;
    pduInfo.SduLength = FRNM_PN_INFO_LENGTH;
    PduR_FrNmRxIndication(FRNM_PN_EIRA_PDUID,&pduInfo);
  }

  DBG_FRNM_HANDLEPNTIMERS_EXIT();
}

#endif

#define FRNM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[end of file]========================================*/
