/**
 * \file
 *
 * \brief AUTOSAR FrNm
 *
 * This file contains the implementation of the AUTOSAR
 * module FrNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* Implementation of the state handler functions of the state machine
 * FrNm.
 *
 * This file contains the implementation of the state functions.  It
 * is generated but must be edited to implement the real actions.  If the
 * state machine model is updated and the code generator regenerates the
 * files the user must manually merge the changes with any existing hand
 * written code.
 */
/*  MISRA-C:2004 Deviation List
 *
 *  MISRA-1) Deviated Rule: 19.10 (required)
 *    "Parameter instance shall be enclosed in parentheses."
 *
 *  Reason:
 *  It is used in function parameter declarations and definitions
 *  or as structure member.
 */
/*==================[inclusions]============================================*/

#include <Std_Types.h>  /* AUTOSAR standard types */

#include <FrNm_Hsm.h>  /* state machine driver interface */
#include <FrNm_HsmFrNm.h> /* public statechart model definitions */
#include <FrNm_HsmFrNmInt.h> /* internal statechart model definitions */
#include <FrNm_Int.h>         /* Module intenal definitions */
#include <SchM_FrNm.h>        /* SchM-header for FrNm */
#include <Nm_Cbk.h>           /* Nm call back API */
#include <TSMem.h>

/*==================[macros]================================================*/

#if (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)
/* parameter list for internal functions */
/* Deviation MISRA-1 */
#define FRNM_PL_TIMER(a, b)          a, b
#else
/* Deviation MISRA-1 */
#define FRNM_PL_TIMER(a, b)          b
#endif
/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

#define FRNM_START_SEC_CODE
#include <MemMap.h>

/** \brief Initialize Internal variables of respective channel.
 ** \param instIdx index of state machine instance to work on */
STATIC FUNC(void, FRNM_CODE) FrNm_InitInternalVar(
  FRNM_PDL_SF(const uint8 instIdx));

STATIC FUNC(void, FRNM_CODE) FrNm_ReadySleepTimerStart(
  FRNM_PL_TIMER(const uint8 instIdx, FrNm_TimeType RsTimer));

STATIC FUNC(void, FRNM_CODE) FrNm_ReadySleepTimerStop(
  FRNM_PDL_SF(const uint8 instIdx));

STATIC FUNC(void, FRNM_CODE) FrNm_UniTimerStart(
  FRNM_PL_TIMER(const uint8 instIdx, FrNm_TimeType UniTimer));

STATIC FUNC(void, FRNM_CODE) FrNm_UniTimerStop(
  FRNM_PDL_SF(const uint8 instIdx));

STATIC FUNC(boolean, FRNM_CODE) FrNm_IsTransitionRmsAllowed(
  FRNM_PDL_SF(const uint8 instIdx));
STATIC FUNC(boolean, FRNM_CODE) FrNm_IsTransitionNoOpAllowed(
  FRNM_PDL_SF(const uint8 instIdx));

STATIC FUNC(boolean, FRNM_CODE) FrNm_IsTransitionAllowed(
  FRNM_PDL_SF(const uint8 instIdx));

#define FRNM_STOP_SEC_CODE
#include <MemMap.h>
/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/
#define FRNM_START_SEC_VAR_INIT_BOOLEAN
#include <MemMap.h>

VAR(boolean, FRNM_VAR_FAST) FrNm_SetActiveWakeUpFlag;

#define FRNM_STOP_SEC_VAR_INIT_BOOLEAN
#include <MemMap.h>
/*==================[external function definitions]=========================*/

#define FRNM_START_SEC_CODE
#include <MemMap.h>

/* ************************ state functions ******************************* */

/* ************************************************************************
 * State: TOP
 * Parent state: none
 * Init substate: BusSleepMode
 * Transitions originating from this state:
 * 1) RX_INDICATION[]/#if (FRNM_PDU_RX_INDIATION_ENABLED == STD_ON) Nm_PduRxIndication(); #endif =>FRNM189 FRNM190
 */

FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfTOPEntry(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* entry action 'UniTimerStop();
   * /InitInternalVar();/CurState = NM_STATE_UNINIT;' */
  FrNm_UniTimerStop(FRNM_PL_SF(instIdx));
  FRNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_UNINIT;
  FrNm_InitInternalVar(FRNM_PL_SF(instIdx));
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfTOPAction1(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action '#if (FRNM_PDU_RX_INDIATION_ENABLED == STD_ON) Nm_PduRxIndication();
   * #endif =>FRNM189 FRNM190'
   * for RX_INDICATION[]/...
   * internal transition */
#if (FRNM_PDU_RX_INDICATION_ENABLED == STD_ON)
  Nm_PduRxIndication(FRNM_CHANNELID_CONFIG(instIdx).ChannelId);
#elif (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
}

/* ************************************************************************
 * State: BusSleepMode
 * Parent state: TOP
 * Init substate: none, this is a leaf state
 * Transitions originating from this state:
 * 1) VOTE_RECEIVED[]/Nm_NetworkStartIndication(); => FRNM175
 * 2) BusSleepMode -> SynchronizeMode: NETWORK_START[]/
 *    if(FRNM_NETWORK_REQUEST_BIT) SetActiveWakeUpFlag == true
 */

FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfBusSleepModeEntry(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* entry action 'ChanStatus&=~REPEAT_MSG_REQUESTED; => FRNM320 */
  const Nm_StateType PreviousState = FRNM_CHANNEL_STATUS(instIdx).CurState;
  /* Enter critical section to protect from concurrent access
   * to different bits in 'ChanStatus' in different APIs.
   */
  SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  /* !LINKSTO FRNM320,1 */
  FRNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~FRNM_REPEAT_MSG_REQUESTED);
  FRNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_BUS_SLEEP;
  SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

  if(PreviousState != NM_STATE_UNINIT)
  {
#if (FRNM_STATE_CHANGE_INDICATION_ENABLED == STD_ON)
    Nm_StateChangeNotification(FRNM_CHANNELID_CONFIG(instIdx).ChannelId,
    PreviousState,
    NM_STATE_BUS_SLEEP);
#endif
    Nm_BusSleepMode(FRNM_CHANNELID_CONFIG(instIdx).ChannelId);
  }

}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfBusSleepModeAction1(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'Nm_NetworkStartIndication(); => FRNM175'
   * for VOTE_RECEIVED[]/...
   * internal transition */
  Nm_NetworkStartIndication(FRNM_CHANNELID_CONFIG(instIdx).ChannelId);
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfBusSleepModeAction2(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'if(ChanStatus & NETWORK_REQUESTED) SetActiveWakeUpFlag = true'
   * for NETWORK_START[]/...
   * external transition to state SynchronizeMode */
  if((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_NETWORK_REQUESTED) != 0U)
  {
    FrNm_SetActiveWakeUpFlag = TRUE;
  }
}

/* ************************************************************************
 * State: NetworkMode
 * Parent state: TOP
 * Init substate: SendingSubMode
 * Transitions originating from this state:
 * 1) STARTUP_ERROR[]/FrNm_HandleSynchError();
 * 2) NetworkMode -> BusSleepMode: FAILSAFE_SLEEP_MODE[]/InitInternalVar();
 * 3) NetworkMode -> SynchronizeMode:
 *     FAILSAFE_SYNCH_MODE[]/ClearActiveWakeupBit()
 */

FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfNetworkModeEntry(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* entry action 'ChanStatus |= FRNM_REPEAT_MSG_REQUESTED; => FRNM109 ,
   * ChanStatus |=FRNM_COM_ENABLED;Nm_NetworkMode(); => FRNM110
   * if(SetActiveWakeFlag == true) SetActiveWakeupBit();
   *  SetActiveWakeFlag = false;'
   */
  /* Enter critical section to protect from concurrent access
   * to different bits in 'ChanStatus' in different APIs.
   */
  SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  FRNM_CHANNEL_STATUS(instIdx).ChanStatus |= FRNM_REPEAT_MSG_REQUESTED;
  /* Enable data or vote transmission */
  FRNM_CHANNEL_STATUS(instIdx).ComEnabled = TRUE;
  if(FrNm_SetActiveWakeUpFlag == TRUE)
  {
#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
    FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0] |= (uint8)(FRNM_CBV_ACTIVEWAKEUPBIT);
#endif
    FrNm_SetActiveWakeUpFlag = FALSE;
  }
  SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  Nm_NetworkMode(FRNM_CHANNELID_CONFIG(instIdx).ChannelId);
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfNetworkModeAction1(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'FrNm_HandleSynchError();'
   * for STARTUP_ERROR[]/...
   * internal transition */
  FrNm_HandleSynchError(FRNM_PL_SF(instIdx));
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfNetworkModeAction2(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'InitInternalVar();'
   * for FAILSAFE_SLEEP_MODE[]/...
   * external transition to state BusSleepMode */
  FrNm_InitInternalVar(FRNM_PL_SF(instIdx));
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfNetworkModeAction3(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'ClearActiveWakeupBit()'
   * for FAILSAFE_SYNCH_MODE[]/...
   * external transition to state SynchronizeMode */
#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
  FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0] &= (uint8)(~FRNM_CBV_ACTIVEWAKEUPBIT);
#elif (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
}

/* ************************************************************************
 * State: ReadySleepState
 * Parent state: NetworkMode
 * Init substate: ReadySleepRemoteActivity
 * Transitions originating from this state:
 * 1) ReadySleepState -> BusSleepMode:
 *    REP_CYCLE_COMPLETED[ReadySleepTimer == 0]/ClearActiveWakeupBit()
 */

FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfReadySleepStateEntry(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* entry action '#ifdef (SCI == STD_ON)
   * Nm_StateChangeNotification(LastState, ActState) ;
   * #endif CurState = ReadySleepState;'
   */

#if (FRNM_STATE_CHANGE_INDICATION_ENABLED == STD_ON)
  const Nm_StateType PreviousState = FRNM_CHANNEL_STATUS(instIdx).CurState;
  FRNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_READY_SLEEP;
  Nm_StateChangeNotification(FRNM_CHANNELID_CONFIG(instIdx).ChannelId,
    PreviousState,
    NM_STATE_READY_SLEEP);
#else
  FRNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_READY_SLEEP;
#endif

}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfReadySleepStateExit(
   FRNM_PDL_SF(const uint8 instIdx))
{
  /* exit action 'ReadySleepTimerStop();' */
  FrNm_ReadySleepTimerStop(FRNM_PL_SF(instIdx));
}
FUNC(boolean, FRNM_CODE) FrNm_HsmFrNmSfReadySleepStateGuard1(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* guard condition 'ReadySleepTimer == 0'
   * for REP_CYCLE_COMPLETED[...]/ClearActiveWakeupBit()
   * external transition to state BusSleepMode */
  return (boolean)((FRNM_CHANNEL_STATUS(instIdx).ReadySleepTimer == 0U) ? TRUE : FALSE);
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfReadySleepStateAction1(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'ClearActiveWakeupBit()'
   * for REP_CYCLE_COMPLETED[ReadySleepTimer == 0]/...
   * external transition to state BusSleepMode */
#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
  FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0] &= (uint8)(~FRNM_CBV_ACTIVEWAKEUPBIT);
#elif (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
}

/* ************************************************************************
 * State: ReadySleepRemoteActivity
 * Parent state: ReadySleepState
 * Init substate: none, this is a leaf state
 * Transitions originating from this state:
 * 1) VOTE_RECEIVED[]/ReadySleepTimerStart(READY_SLEEP_COUNT);
 * 2) ReadySleepRemoteActivity -> RepeatMessageState:
 *    REP_CYCLE_COMPLETED[IsTransitionRmsAllowed() == true]/
 * 3) ReadySleepRemoteActivity -> NormalOperationRemoteActivity:
 *    REP_CYCLE_COMPLETED[IsTransitionNoOpAllowed() == true]/
 */

FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfReadySleepRemoteActivityEntry(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* entry action 'ReadySleepTimerStart(READY_SLEEP_COUNT)' */
  FrNm_ReadySleepTimerStart(
    FRNM_PL_TIMER(instIdx, FRNM_CHANNELTIME_CONFIG(instIdx).ReadySleepCnt));
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfReadySleepRemoteActivityAction1(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'ReadySleepTimerStart(READY_SLEEP_COUNT);'
   * for VOTE_RECEIVED[]/...
   * internal transition */
  FrNm_ReadySleepTimerStart(
    FRNM_PL_TIMER(instIdx, FRNM_CHANNELTIME_CONFIG(instIdx).ReadySleepCnt));
}
FUNC(boolean, FRNM_CODE) FrNm_HsmFrNmSfReadySleepRemoteActivityGuard2(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* guard condition 'IsTransitionRmsAllowed() == true'
   * for REP_CYCLE_COMPLETED[...]/
   * external transition to state RepeatMessageState */
  return (boolean)((FrNm_IsTransitionRmsAllowed(FRNM_PL_SF(instIdx)) == TRUE) ? TRUE : FALSE);
}
FUNC(boolean, FRNM_CODE) FrNm_HsmFrNmSfReadySleepRemoteActivityGuard3(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* guard condition 'IsTransitionNoOpAllowed() == true'
   * for REP_CYCLE_COMPLETED[...]/
   * external transition to state NormalOperationRemoteActivity */
  return (boolean)((FrNm_IsTransitionNoOpAllowed(FRNM_PL_SF(instIdx)) == TRUE) ? TRUE : FALSE);
}

/* ************************************************************************
 * State: ReadySleepRemoteSleep
 * Parent state: ReadySleepState
 * Init substate: none, this is a leaf state
 * Transitions originating from this state:
 * 1) ReadySleepRemoteSleep -> RepeatMessageState:
 *    REP_CYCLE_COMPLETED[IsTransitionRmsAllowed() == true]/
 *    Nm_RemoteSleepCancellation();
 * 2) ReadySleepRemoteSleep -> NormalOperationRemoteSleep:
 *    REP_CYCLE_COMPLETED[IsTransitionNoOpAllowed() == true]/
 * 3) ReadySleepRemoteSleep -> ReadySleepRemoteActivity:
 *    VOTE_RECEIVED[]/Nm_RemoteSleepCancellation();
 *    ChanStatus &= ~FRNM_REMOTE_SLEEP_INDICATION;
 */

FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfReadySleepRemoteSleepEntry(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* entry action 'ReadySleepTimerStart(READY_SLEEP_COUNT)' */
  FrNm_ReadySleepTimerStart(
    FRNM_PL_TIMER(instIdx, FRNM_CHANNELTIME_CONFIG(instIdx).ReadySleepCnt));
}
FUNC(boolean, FRNM_CODE) FrNm_HsmFrNmSfReadySleepRemoteSleepGuard1(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* guard condition 'IsTransitionRmsAllowed() == true'
   * for REP_CYCLE_COMPLETED[...]/Nm_RemoteSleepCancellation();
   * external transition to state RepeatMessageState */
  return (boolean) ((FrNm_IsTransitionRmsAllowed(FRNM_PL_SF(instIdx)) == TRUE) ? TRUE : FALSE);
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfReadySleepRemoteSleepAction1(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'Nm_RemoteSleepCancellation();'
   * for REP_CYCLE_COMPLETED[IsTransitionRmsAllowed() == true]/...
   * external transition to state RepeatMessageState */
  Nm_RemoteSleepCancellation(FRNM_CHANNELID_CONFIG(instIdx).ChannelId);
}
FUNC(boolean, FRNM_CODE) FrNm_HsmFrNmSfReadySleepRemoteSleepGuard2(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* guard condition 'IsTransitionNoOpAllowed() == true'
   * for REP_CYCLE_COMPLETED[...]/
   * external transition to state NormalOperationRemoteSleep */
  return (boolean)((FrNm_IsTransitionNoOpAllowed(FRNM_PL_SF(instIdx)) == TRUE) ? TRUE : FALSE);
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfReadySleepRemoteSleepAction3(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'Nm_RemoteSleepCancellation();
   * ChanStatus &= ~FRNM_REMOTE_SLEEP_INDICATION;'
   * for VOTE_RECEIVED[]/...
   * external transition to state ReadySleepRemoteActivity */
  Nm_RemoteSleepCancellation(FRNM_CHANNELID_CONFIG(instIdx).ChannelId);
  /* Enter critical section to protect from concurrent access
   * to different bits in 'ChanStatus' in different APIs.
   */
  SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  FRNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~FRNM_REMOTE_SLEEP_INDICATION);
  SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
}

/* ************************************************************************
 * State: SendingSubMode
 * Parent state: NetworkMode
 * Init substate: RepeatMessageState
 * Transitions originating from this state:
 * 1) DATA_CYCLE_TIMESLOT[]/#if ((PASSIVE_MODE == STD_OFF))
 *    if ((ChanStatus == COM_ENABLED) && (ChanProperty == NM_DATA_ENABLED))
 *    {HandleDataCycle();}#endif
 * 2) VOTE_CYCLE_TIMESLOT[]/#if ((PASSIVE_MODE == STD_OFF))
 *    if ((ChanStatus == FRNM_COM_ENABLED) {HandleVoteCycle();}#endif
 * 3) TX_TIMEOUT[]/Nm_TxTimeoutException();
 * 4) TX_CONFIRMATION[]/Restart TimeoutTimer;
 */

FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfSendingSubModeEntry(
  FRNM_PDL_SF(const uint8 instIdx))
{
 /* entry action 'SetVoteBit(); Start TimeoutTimer;' */
#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
#if (FRNM_SCHEDULE_VARIANT_2_FORCE_VOTE_BIT == STD_ON)
  if((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_PDU_SCHEDULE_VARIANT_MASK) == FRNM_PDU_SCHEDULE_VARIANT_2)
  {
    /* !LINKSTO FrNm.EB.VoteBitValueScheduleVariant2,1 */
    FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] |= FRNM_SCHEDULE_VARIANT_2_VOTE_BIT_VALUE;
  }
  else
  {
    FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] |= FRNM_PDU_VOTE_BIT;
  }
#else
  FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] |= FRNM_PDU_VOTE_BIT;
#endif  
  /* Start timeout timer to detect any transmission error */
  FRNM_CHANNEL_STATUS(instIdx).TimeoutTimer = FRNM_CHANNELTIME_CONFIG(instIdx).MsgTimeoutTime;
#elif (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfSendingSubModeExit(
   FRNM_PDL_SF(const uint8 instIdx))
{
  /* exit action 'clearVoteBit(); Stop TimeoutTimer;' */
#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
#if (FRNM_SCHEDULE_VARIANT_2_FORCE_VOTE_BIT == STD_OFF)
  FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] &= (uint8)(~FRNM_PDU_VOTE_BIT);
#else
  /* !LINKSTO FrNm.EB.VoteBitValueScheduleVariant2,1 */
  /* In case the Schedule Variant is 2 the vote bit does not need to be changed since 
   * it's set in the SendingSubmodeEntry */
  if((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_PDU_SCHEDULE_VARIANT_MASK) != FRNM_PDU_SCHEDULE_VARIANT_2)
  {
    FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] &= (uint8)(~FRNM_PDU_VOTE_BIT);
  }
#endif
  /* Stop timeout timer */
  FRNM_CHANNEL_STATUS(instIdx).TimeoutTimer = 0U;
#elif (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif

}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfSendingSubModeAction1(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action '#if ((PASSIVE_MODE == STD_OFF))
   * if ((ChanStatus == COM_ENABLED) && (ChanProperty == NM_DATA_ENABLED))
   * {HandleDataCycle();}
   * #endif' for DATA_CYCLE_TIMESLOT[]/...
   * internal transition */
#if ((FRNM_PASSIVE_MODE_ENABLED == STD_OFF) \
     && (FRNM_ENABLE_NMDATA == STD_ON))
    if ((FRNM_CHANNEL_STATUS(instIdx).ComEnabled == TRUE) &&
        ((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_NM_DATA_ENABLED) != 0U))
   {
    FrNm_HandleDataCycle(FRNM_PL_SF(instIdx));
   }
#elif (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfSendingSubModeAction2(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action '#if ((PASSIVE_MODE == STD_OFF))
   * if (ChanStatus == FRNM_COM_ENABLED) {HandleVoteCycle();}#endif'
   * for VOTE_CYCLE_TIMESLOT[]/...
   * internal transition */
#if ((FRNM_PASSIVE_MODE_ENABLED == STD_OFF) \
   && ((FRNM_ENABLE_NMVOTE == STD_ON) || (FRNM_ENABLE_NMMIXED == STD_ON)))

  if (FRNM_CHANNEL_STATUS(instIdx).ComEnabled == TRUE)
  {
    FrNm_HandleVoteCycle(FRNM_PL_SF(instIdx));
  }
#elif (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfSendingSubModeAction3(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'Nm_TxTimeoutException();'
   * for TX_TIMEOUT[]/...
   * internal transition */
  Nm_TxTimeoutException(FRNM_CHANNELID_CONFIG(instIdx).ChannelId);
  /* Restart timeout timer */
  FRNM_CHANNEL_STATUS(instIdx).TimeoutTimer = FRNM_CHANNELTIME_CONFIG(instIdx).MsgTimeoutTime;
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfSendingSubModeAction4(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'Restart TimeoutTimer;'
   * for TX_CONFIRMATION[]/...
   * internal transition */
   /* Restart timeout timer */
  FRNM_CHANNEL_STATUS(instIdx).TimeoutTimer = FRNM_CHANNELTIME_CONFIG(instIdx).MsgTimeoutTime;
}
/* ************************************************************************
 * State: NormalOperationState
 * Parent state: SendingSubMode
 * Init substate: NormalOperationRemoteActivity
 * Transitions originating from this state:
 */

FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfNormalOperationStateEntry(
  FRNM_PDL_SF(const uint8 instIdx))
{

  /* entry action '#ifdef (SCI == STD_ON)
   * Nm_StateChangeNotification(LastState, ActState); #endif
   * CurState = NormalOperationState;'
   */

#if (FRNM_STATE_CHANGE_INDICATION_ENABLED == STD_ON)
  const Nm_StateType PreviousState = FRNM_CHANNEL_STATUS(instIdx).CurState;
  FRNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_NORMAL_OPERATION;
  Nm_StateChangeNotification(FRNM_CHANNELID_CONFIG(instIdx).ChannelId,
    PreviousState,
    NM_STATE_NORMAL_OPERATION);
#else
  FRNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_NORMAL_OPERATION;
#endif

}

/* ************************************************************************
 * State: NormalOperationRemoteActivity
 * Parent state: NormalOperationState
 * Init substate: none, this is a leaf state
 * Transitions originating from this state:
 * 1) VOTE_RECEIVED[]/#if (RSI == STD_ON)
 *    UniTimerStart(FRNM_REMOTE_SLEEP_IND_TIME); #endif
 * 2) NormalOperationRemoteActivity -> RepeatMessageState:
 *    REP_CYCLE_COMPLETED[ChanStatus & REPEAT_MESSAGE_REQUESTED]/
 * 3) NormalOperationRemoteActivity -> NormalOperationRemoteSleep:
 *    REP_CYCLE_COMPLETED[(ChanStatus.UniversalTimer == 0) &&
 *    ChanConfig.RemoteSleepIndTime != 0)]/
 * 4) NormalOperationRemoteActivity -> ReadySleepRemoteActivity:
 *    REP_CYCLE_COMPLETED[(! ChanStatus & NETWORK_REQUESTED) &&
 *    (! ChanStatus & REPEAT_MSG_REQUESTED)]/
 */

FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfNormalOperationRemoteActivityEntry(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* entry action '#if (RSI == STD_ON) if (ChanStatus & R_S_I)
   * {Nm_RemoteSleepCancellation();
   * ChanStatus &= ~R_S_I;} #endif#if (RSI == STD_ON)
   * UniTimerStart(FRNM_REMOTE_SLEEP_IND_TIME); #endif'
   */
#if (FRNM_REMOTE_SLEEP_IND_ENABLED == STD_ON)
  /* clear the internal status flag, before starting the universal timer */
  /* Enter critical section to protect from concurrent access
   * to different bits in 'ChanStatus' in different APIs.
   */
  SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  FRNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~ FRNM_UNI_TIMEOUT_PASSED);
  SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  FrNm_UniTimerStart(
    FRNM_PL_TIMER(instIdx, FRNM_CHANNELTIME_CONFIG(instIdx).RemoteSleepIndTime));

  if ((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_REMOTE_SLEEP_INDICATION) != 0U)
  {
    Nm_RemoteSleepCancellation(FRNM_CHANNELID_CONFIG(instIdx).ChannelId);
    /* Enter critical section to protect from concurrent access
     * to different bits in 'ChanStatus' in different APIs.
     */
    SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    FRNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~FRNM_REMOTE_SLEEP_INDICATION);
    SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  }
#elif (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfNormalOperationRemoteActivityAction1(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action '#if (RSI == STD_ON) UniTimerStart(FRNM_REMOTE_SLEEP_IND_TIME);
   * #endif'
   * for VOTE_RECEIVED[]/...
   * internal transition */
#if(FRNM_REMOTE_SLEEP_IND_ENABLED == STD_ON)
  /* clear the internal status flag, before starting the universal timer */
  /* Enter critical section to protect from concurrent access
   * to different bits in 'ChanStatus' in different APIs.
   */
  SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  FRNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~ FRNM_UNI_TIMEOUT_PASSED);
  SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  FrNm_UniTimerStart(
    FRNM_PL_TIMER(instIdx, FRNM_CHANNELTIME_CONFIG(instIdx).RemoteSleepIndTime));
#elif (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
}
FUNC(boolean, FRNM_CODE) FrNm_HsmFrNmSfNormalOperationRemoteActivityGuard2(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* guard condition 'ChanStatus & REPEAT_MESSAGE_REQUESTED'
   * for REP_CYCLE_COMPLETED[...]/
   * external transition to state RepeatMessageState */
  return (boolean)
    (((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_REPEAT_MSG_REQUESTED)!= 0U) ? TRUE : FALSE);
}
FUNC(boolean, FRNM_CODE) FrNm_HsmFrNmSfNormalOperationRemoteActivityGuard3(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* guard condition '(ChanStatus.UniversalTimer == 0) &&
   *  ChanConfig.RemoteSleepIndTime != 0)'
   * for REP_CYCLE_COMPLETED[...]/
   * external transition to state NormalOperationRemoteSleep */
  return (boolean)(((FRNM_CHANNEL_STATUS(instIdx).UniversalTimer == 0U) &&
   (FRNM_CHANNELTIME_CONFIG(instIdx).RemoteSleepIndTime != 0U)) ? TRUE : FALSE);
}
FUNC(boolean, FRNM_CODE) FrNm_HsmFrNmSfNormalOperationRemoteActivityGuard4(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* guard condition '(! ChanStatus & NETWORK_REQUESTED) &&
   * (! ChanStatus & REPEAT_MSG_REQUESTED)'
   * for REP_CYCLE_COMPLETED[...]/
   * external transition to state ReadySleepRemoteActivity */
  return (boolean)
    ((((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_NETWORK_REQUESTED) == 0U) &&
     ((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_REPEAT_MSG_REQUESTED) == 0U)) ? TRUE : FALSE);
}

/* ************************************************************************
 * State: NormalOperationRemoteSleep
 * Parent state: NormalOperationState
 * Init substate: none, this is a leaf state
 * Transitions originating from this state:
 * 1) NormalOperationRemoteSleep -> RepeatMessageState:
 *    REP_CYCLE_COMPLETED[ChanStatus & REPEAT_MSG_REQUESTED]/
 *    Nm_RemoteSleepCancellation(); => FRNM230
 * 2) NormalOperationRemoteSleep -> ReadySleepRemoteSleep:
 *    REP_CYCLE_COMPLETED[(! ChanStatus & NETWORK_REQUESTED) &&
 *    (! ChanStatus & REPEAT_MSG_REQUESTED)]/
 * 3) NormalOperationRemoteSleep -> NormalOperationRemoteActivity:
 *    VOTE_RECEIVED[]/Uni timer (RSI) is started by entry action
 */

FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfNormalOperationRemoteSleepEntry(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* entry action 'ChanStatus |= FRNM_REMOTE_SLEEP_INDICATION;
   * Nm_RemoteSleepIndication();' */
  /* Enter critical section to protect from concurrent access
   * to different bits in 'ChanStatus' in different APIs.
   */
  SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  FRNM_CHANNEL_STATUS(instIdx).ChanStatus |= FRNM_REMOTE_SLEEP_INDICATION;
  SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  Nm_RemoteSleepIndication(FRNM_CHANNELID_CONFIG(instIdx).ChannelId);
}
FUNC(boolean, FRNM_CODE) FrNm_HsmFrNmSfNormalOperationRemoteSleepGuard1(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* guard condition 'ChanStatus & REPEAT_MSG_REQUESTED'
   * for REP_CYCLE_COMPLETED[...]/Nm_RemoteSleepCancellation(); => FRNM230
   * external transition to state RepeatMessageState */
  return (boolean)
    (((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_REPEAT_MSG_REQUESTED) != 0U) ? TRUE : FALSE);
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfNormalOperationRemoteSleepAction1(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'Nm_RemoteSleepCancellation(); => FRNM230 '
   * for REP_CYCLE_COMPLETED[ChanStatus & REPEAT_MSG_REQUESTED]/...
   * external transition to state RepeatMessageState */
  Nm_RemoteSleepCancellation(FRNM_CHANNELID_CONFIG(instIdx).ChannelId);
}
FUNC(boolean, FRNM_CODE) FrNm_HsmFrNmSfNormalOperationRemoteSleepGuard2(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* guard condition '(! ChanStatus & NETWORK_REQUESTED) &&
   * (! ChanStatus & REPEAT_MSG_REQUESTED)'
   * for REP_CYCLE_COMPLETED[...]/
   * external transition to state ReadySleepRemoteSleep */
  return (boolean)
    ((((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_NETWORK_REQUESTED) == 0U) &&
     ((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_REPEAT_MSG_REQUESTED) == 0U)) ? TRUE : FALSE);
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfNormalOperationRemoteSleepAction3(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'Uni timer (RSI) is started by entry action'
   * for VOTE_RECEIVED[]/...
   * external transition to state NormalOperationRemoteActivity */
  /* Enter critical section to protect from concurrent access
   * to different bits in 'ChanStatus' in different APIs.
   */
  SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  /* clear the internal status flag, before starting the universal timer */
  FRNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~ FRNM_UNI_TIMEOUT_PASSED);
  SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
}

/* ************************************************************************
 * State: RepeatMessageState
 * Parent state: SendingSubMode
 * Init substate: none, this is a leaf state
 * Transitions originating from this state:
 * 1) UNI_TIMEOUT[]/ChanStatus |= UNI_TIMEOUT_PASSED;
 * 2) RepeatMessageState -> NormalOperationState:
 *    REP_CYCLE_COMPLETED[(ChanStatus & NETWORK_REQUESTED) &&
 *    (ChanStatus & UNI_TIMEOUT_PASSED)]/
 * 3) RepeatMessageState -> ReadySleepState:
 *    REP_CYCLE_COMPLETED[( !(ChanStatus & NETWORK_REQUESTED) &&
 *    (ChanStatus & UNI_TIMEOUT_PASSED) )]/
 */

FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfRepeatMessageStateEntry(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* entry action '#if (RSI == STD_ON) ChanStatus &= REMOTE_SLEEP_INDICATION;
   * #endif
   * #if (SCI == STD_ON) Nm_StateChangeNotification(LastState, ActState); #endif
   * CurState = RepeatMessageState;HandleRepeatMessageStateEntry();
   * UniTimerStart(FRNM_REPEAT_MSG_TIME); => FRNM117 '
   */
#if (FRNM_STATE_CHANGE_INDICATION_ENABLED == STD_ON)
   const Nm_StateType PreviousState = FRNM_CHANNEL_STATUS(instIdx).CurState;
#endif
  /* Refer FrNm0030_Conf, timeout for Repeat Message State with value  "0"
   * denotes that no Repeat Message State is configured, which means that
   * Repeat Message State is transient and implies that it is left immediately
   * after entry and consequently no startup stability is guaranteed and no
   * node detection procedure is possible.
   */

  if (FRNM_CHANNELTIME_CONFIG(instIdx).RepeatMessageTime != 0U)
  {
    /* Enter critical section to protect from concurrent access
     * to different bits in 'ChanStatus' in different APIs.
     */
    SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
#if (FRNM_REMOTE_SLEEP_IND_ENABLED == STD_ON)
    FRNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~FRNM_REMOTE_SLEEP_INDICATION);
#endif
    FRNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_REPEAT_MESSAGE;
    SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
#if (FRNM_STATE_CHANGE_INDICATION_ENABLED == STD_ON)
    Nm_StateChangeNotification(FRNM_CHANNELID_CONFIG(instIdx).ChannelId,
      PreviousState, NM_STATE_REPEAT_MESSAGE);
#endif
    /* Enter critical section to protect from concurrent access
     * to different bits in 'ChanStatus' in different APIs.
     */
    SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    /* clear the internal status flag, before starting the universal timer */
    FRNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~ FRNM_UNI_TIMEOUT_PASSED);
    SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    /* -->FRNM117; */
    FrNm_UniTimerStart(
      FRNM_PL_TIMER(instIdx, FRNM_CHANNELTIME_CONFIG(instIdx).RepeatMessageTime));
  }
  else
  {
    /* Repeat message time is 0.
     * This condition is handled in the do action 'HandleRepeatMessageTimer()'.
     */
  }
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfRepeatMessageStateExit(
   FRNM_PDL_SF(const uint8 instIdx))
{
  /* exit action 'HandleRepeatMessageStateExit();' */
  /* It is ensured that repeat message bit and node detection feature can be
   * active only when passive mode is disabled.
   */

  /* clear the internal status flag, repeat message request */

  /* Enter critical section to protect from concurrent access
   * to different bits in 'ChanStatus' and 'TxPduPtr' in different APIs.
   */
  SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

  FRNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~FRNM_REPEAT_MSG_REQUESTED);

#if ((FRNM_REPEAT_MSG_BIT_ENABLED == STD_ON) && (FRNM_NODE_DETECTION_ENABLED == STD_ON))
  /* Clear repeat message bit in the transmit buffer */
  FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] &= (uint8)(~FRNM_PDU_REPEAT_MSG_BIT);
#endif

  SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfRepeatMessageStateDo(
   FRNM_PDL_SF(const uint8 instIdx))
{
  /* do action 'HandleRepeatMessageTimer();' */

  /* Refer FrNm0030_Conf, timeout for Repeat Message State with value  "0"
   * denotes that no Repeat Message State is configured, which means that
   * Repeat Message State is transient and implies that it is left immediately
   * after entry and consequently no startup stability is guaranteed and no
   * node detection procedure is possible.
   */

    /* In the entry action of 'RepeatMessageState', the 'CurState' variable
     * is updated only if 'FrNmRepeatMessageTime > 0'.
     * So, check the value of 'CurState' to ensure that we're dealing with
     * the case 'FrNmRepeatMessageTime = 0' only.
     */
  if ((NM_STATE_REPEAT_MESSAGE != FRNM_CHANNEL_STATUS(instIdx).CurState) &&
      (FRNM_CHANNELTIME_CONFIG(instIdx).RepeatMessageTime == 0U)
     )
  {
    /* The configured Repeat message time is 0.
     * Emit repetition cycle event in order to exit from 'repeat message state'
     * immediately.
     * Set channel status with repeat message time out bit set, to satisfy the
     * guard condition to exit from repeat message state.
     */
    FRNM_HSMEMITINST(&FrNm_HsmScFrNm, instIdx, FRNM_HSM_FRNM_EV_REP_CYCLE_COMPLETED);
    /* Enter critical section to protect from concurrent access
     * to different bits in 'ChanStatus' in different APIs.
     */
    SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    FRNM_CHANNEL_STATUS(instIdx).ChanStatus |= FRNM_UNI_TIMEOUT_PASSED;
    SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  }
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfRepeatMessageStateAction1(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'ChanStatus |= UNI_TIMEOUT_PASSED;'
   * for UNI_TIMEOUT[]/...
   * internal transition */
  /* Enter critical section to protect from concurrent access
   * to different bits in 'ChanStatus' in different APIs.
   */
  SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  FRNM_CHANNEL_STATUS(instIdx).ChanStatus |= FRNM_UNI_TIMEOUT_PASSED;
  SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
}
FUNC(boolean, FRNM_CODE) FrNm_HsmFrNmSfRepeatMessageStateGuard2(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* guard condition '(ChanStatus & NETWORK_REQUESTED) &&
   * (ChanStatus & UNI_TIMEOUT_PASSED)'
   * for REP_CYCLE_COMPLETED[...]/
   * external transition to state NormalOperationState */
  return (boolean)
    ((((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_NETWORK_REQUESTED) != 0U) &&
     ((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_UNI_TIMEOUT_PASSED) != 0U)) ? TRUE : FALSE);
}
FUNC(boolean, FRNM_CODE) FrNm_HsmFrNmSfRepeatMessageStateGuard3(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* guard condition '( !(ChanStatus & NETWORK_REQUESTED) &&
   * (ChanStatus & UNI_TIMEOUT_PASSED) )'
   * for REP_CYCLE_COMPLETED[...]/
   * external transition to state ReadySleepState */
  return (boolean)
    ((((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_NETWORK_REQUESTED) == 0U) &&
     ((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_UNI_TIMEOUT_PASSED) != 0U)) ? TRUE : FALSE);
}

/* ************************************************************************
 * State: SynchronizeMode
 * Parent state: TOP
 * Init substate: none, this is a leaf state
 * Transitions originating from this state:
 * 1) SynchronizeMode -> BusSleepMode:
      FAILSAFE_SLEEP_MODE[]/FrNm_InitInternalVar();
 * 2) SynchronizeMode -> NetworkMode:
 *    REP_CYCLE_COMPLETED[(ChanStatus & NETWORK_STARTED) &&
 *    !(ChanStatus & FRNM_STARTUP_ERROR)]/
 * 3) SynchronizeMode -> BusSleepMode:
 *    STARTUP_ERROR[ ! (ChanStatus & NETWORK_REQUESTED)]/
 */

FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfSynchronizeModeEntry(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* entry action '#if (SCI == STD_ON)
   * Nm_StateChangeNotification(LastState, ActState);
   * => FRNM191 FRNM192 CurState = SynchronizeState;
   * ChanStatus &=~STARTUP_ERROR;'
   */

#if (FRNM_STATE_CHANGE_INDICATION_ENABLED == STD_ON)
  const Nm_StateType PreviousState = FRNM_CHANNEL_STATUS(instIdx).CurState;
  FRNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_SYNCHRONIZE;
  Nm_StateChangeNotification(FRNM_CHANNELID_CONFIG(instIdx).ChannelId,
    PreviousState, NM_STATE_SYNCHRONIZE);
#else
  FRNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_SYNCHRONIZE;
#endif
  /* Enter critical section to protect from concurrent access
   * to different bits in 'ChanStatus' in different APIs.
   */
  SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  FRNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~FRNM_STARTUP_ERROR);
  SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
}
FUNC(void, FRNM_CODE) FrNm_HsmFrNmSfSynchronizeModeAction1(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* action 'InitInternalVar();'
   * for FAILSAFE_SLEEP_MODE[]/...
   * external transition to state BusSleepMode */
  FrNm_InitInternalVar(FRNM_PL_SF(instIdx));
}

FUNC(boolean, FRNM_CODE) FrNm_HsmFrNmSfSynchronizeModeGuard2(

  FRNM_PDL_SF(const uint8 instIdx))
{
  /* guard condition '(ChanStatus & NETWORK_STARTED)'
   * for REP_CYCLE_COMPLETED[...]/
   * external transition to state NetworkMode */

  return (boolean)
    (((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_NETWORK_STARTED) != 0U) ? TRUE : FALSE);

}
FUNC(boolean, FRNM_CODE) FrNm_HsmFrNmSfSynchronizeModeGuard3(

  FRNM_PDL_SF(const uint8 instIdx))
{
  /* guard condition '! (ChanStatus & NETWORK_REQUESTED)'
   * for STARTUP_ERROR[...]/
   * external transition to state BusSleepMode */

  return (boolean)
    (((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_NETWORK_REQUESTED) == 0U) ? TRUE : FALSE);
}
/*==================[internal function definitions]=========================*/
STATIC FUNC(void, FRNM_CODE) FrNm_InitInternalVar(
  FRNM_PDL_SF(const uint8 instIdx))
{
  FrNm_SetActiveWakeUpFlag = FALSE;
  FRNM_CHANNEL_STATUS(instIdx).ChanStatus  = 0U;
  FRNM_CHANNEL_STATUS(instIdx).ActCycle = 0U;
  FRNM_CHANNEL_STATUS(instIdx).ReadySleepTimer = 0U;
  FRNM_CHANNEL_STATUS(instIdx).ComEnabled = TRUE;
#if (FRNM_PASSIVE_MODE_ENABLED == STD_OFF)
  FRNM_CHANNEL_STATUS(instIdx).TimeoutTimer = 0U;
  FRNM_CHANNEL_STATUS(instIdx).UniversalTimer = 0U;
  /* !LINKSTO FRNM042,1 */
  FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] = 0U;

#if (FRNM_SOURCE_NODE_IDENTIFIER_ENABLED == STD_ON)
  FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[1U] = FRNM_CHANNELID_CONFIG(instIdx).NodeId;
#endif

#if ((FRNM_USER_DATA_ENABLED  == STD_ON) || (FRNM_COM_USER_DATA_SUPPORT == STD_ON))
  /* FRNM045 */
  TS_MemSet(
      &FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[FRNM_RESERVED_BYTES],
      0xFFU, (uint16)(FRNM_CHANNELID_CONFIG(instIdx).PduLength - FRNM_RESERVED_BYTES));
#endif /*((FRNM_USER_DATA_ENABLED  == STD_ON) || ......*/
#if (FRNM_PN_ENABLED == STD_ON)
  if(FRNM_CHANNELID_CONFIG(instIdx).PnEnabled == TRUE)
  {
    /* FRNM409, Set CRI bit */
    FRNM_CHANNEL_CONFIG(instIdx).TxPduPtr[0U] |=  FRNM_CBV_PNINFOBIT_MASK;
  }
#endif
#endif /*(FRNM_PASSIVE_MODE_ENABLED == STD_OFF)*/
#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
  /* Initialise external and internal requests */
  TS_MemSet(FRNM_CHANNEL_STATUS(instIdx).PnInfoEira, 0U, FRNM_PN_INFO_LENGTH);
#endif

}

STATIC FUNC(void, FRNM_CODE) FrNm_ReadySleepTimerStart(
  FRNM_PL_TIMER(const uint8 instIdx, FrNm_TimeType RsTimer))
{
  /* load timer value */
  FRNM_CHANNEL_STATUS(instIdx).ReadySleepTimer = RsTimer;
}

STATIC FUNC(void, FRNM_CODE) FrNm_ReadySleepTimerStop(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* Stop the Universal Timer */
  FRNM_CHANNEL_STATUS(instIdx).ReadySleepTimer = 0U;
}

STATIC FUNC(void, FRNM_CODE) FrNm_UniTimerStart(
  FRNM_PL_TIMER(const uint8 instIdx, FrNm_TimeType UniTimer))
{
  /* load timer value */
  FRNM_CHANNEL_STATUS(instIdx).UniversalTimer = UniTimer;
}

STATIC FUNC(void, FRNM_CODE) FrNm_UniTimerStop(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* Stop the Universal Timer */
  FRNM_CHANNEL_STATUS(instIdx).UniversalTimer = 0U;
}

STATIC FUNC(boolean, FRNM_CODE) FrNm_IsTransitionRmsAllowed(
  FRNM_PDL_SF(const uint8 instIdx))
{
  return (boolean)
    (((FrNm_IsTransitionAllowed(FRNM_PL_SF(instIdx)) == TRUE) &&
    ((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_REPEAT_MSG_REQUESTED) != 0U)) ? TRUE : FALSE);
}
STATIC FUNC(boolean, FRNM_CODE) FrNm_IsTransitionNoOpAllowed(
  FRNM_PDL_SF(const uint8 instIdx))
{
  /* Note: Extra braces are used to get rid of misra waring.
   * Rule 12.5(required): The operands of a logical && or || shall be
   * primar-expressions.
   */
 return (boolean) (((FrNm_IsTransitionAllowed(FRNM_PL_SF(instIdx)) == TRUE) &&
   (((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_NETWORK_REQUESTED) != 0U) &&
   ((FRNM_CHANNEL_STATUS(instIdx).ChanStatus & FRNM_REPEAT_MSG_REQUESTED) == 0U))) ? TRUE : FALSE);
}

STATIC FUNC(boolean, FRNM_CODE) FrNm_IsTransitionAllowed(
  FRNM_PDL_SF(const uint8 instIdx))
{
  boolean retVal = FALSE;
#if ( FRNM_DUAL_CHANNEL_PDU_ENABLE == STD_ON)
  if ((FRNM_CHANNELID_CONFIG(instIdx).ChannelProperty & FRNM_VOTE_INHIBITION_ENABLED) != 0U)
  {
    /* FRNM357 */
    if (FRNM_CHANNEL_STATUS(instIdx).ReadySleepTimer > 1U)
    {
     retVal = TRUE;
    }
  }
else
#endif
  {
    if (FRNM_CHANNEL_STATUS(instIdx).ReadySleepTimer > 0U)
    {
      retVal = TRUE;
    }
  }
  return retVal;
}


#define FRNM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[end of file]===========================================*/
