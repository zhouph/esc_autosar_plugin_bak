/**
 * \file
 *
 * \brief AUTOSAR FrNm
 *
 * This file contains the implementation of the AUTOSAR
 * module FrNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined FRNM_TRACE_H)
#define FRNM_TRACE_H
/*==================[inclusions]============================================*/

[!IF "node:exists(as:modconf('Dbg'))"!]
#include <Dbg.h>
[!ENDIF!]

/*==================[macros]================================================*/

#include <TSAutosar.h>

#ifndef DBG_FRNM_GENERIC_GRP
/** \brief Gegeric state change */
#define DBG_FRNM_GENERIC_GRP(a,b,c,d) TS_PARAM_UNUSED(c)
#endif

#ifndef DBG_FRNM_INIT_ENTRY
/** \brief Entry point of function FrNm_Init() */
#define DBG_FRNM_INIT_ENTRY(a)
#endif

#ifndef DBG_FRNM_INIT_EXIT
/** \brief Exit point of function FrNm_Init() */
#define DBG_FRNM_INIT_EXIT(a)
#endif

#ifndef DBG_FRNM_MAINFUNCTION_ENTRY
/** \brief Entry point of function FrNm_MainFunction() */
#define DBG_FRNM_MAINFUNCTION_ENTRY(a)
#endif

#ifndef DBG_FRNM_MAINFUNCTION_EXIT
/** \brief Exit point of function FrNm_MainFunction() */
#define DBG_FRNM_MAINFUNCTION_EXIT(a)
#endif

#ifndef DBG_FRNM_PASSIVESTARTUP_ENTRY
/** \brief Entry point of function FrNm_PassiveStartUp() */
#define DBG_FRNM_PASSIVESTARTUP_ENTRY(a)
#endif

#ifndef DBG_FRNM_PASSIVESTARTUP_EXIT
/** \brief Exit point of function FrNm_PassiveStartUp() */
#define DBG_FRNM_PASSIVESTARTUP_EXIT(a,b)
#endif

#ifndef DBG_FRNM_NETWORKREQUEST_ENTRY
/** \brief Entry point of function FrNm_NetworkRequest() */
#define DBG_FRNM_NETWORKREQUEST_ENTRY(a)
#endif

#ifndef DBG_FRNM_NETWORKREQUEST_EXIT
/** \brief Exit point of function FrNm_NetworkRequest() */
#define DBG_FRNM_NETWORKREQUEST_EXIT(a,b)
#endif

#ifndef DBG_FRNM_NETWORKRELEASE_ENTRY
/** \brief Entry point of function FrNm_NetworkRelease() */
#define DBG_FRNM_NETWORKRELEASE_ENTRY(a)
#endif

#ifndef DBG_FRNM_NETWORKRELEASE_EXIT
/** \brief Exit point of function FrNm_NetworkRelease() */
#define DBG_FRNM_NETWORKRELEASE_EXIT(a,b)
#endif

#ifndef DBG_FRNM_SETUSERDATA_ENTRY
/** \brief Entry point of function FrNm_SetUserData() */
#define DBG_FRNM_SETUSERDATA_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_SETUSERDATA_EXIT
/** \brief Exit point of function FrNm_SetUserData() */
#define DBG_FRNM_SETUSERDATA_EXIT(a,b,c)
#endif

#ifndef DBG_FRNM_GETUSERDATA_ENTRY
/** \brief Entry point of function FrNm_GetUserData() */
#define DBG_FRNM_GETUSERDATA_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_GETUSERDATA_EXIT
/** \brief Exit point of function FrNm_GetUserData() */
#define DBG_FRNM_GETUSERDATA_EXIT(a,b,c)
#endif

#ifndef DBG_FRNM_GETPDUDATA_ENTRY
/** \brief Entry point of function FrNm_GetPduData() */
#define DBG_FRNM_GETPDUDATA_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_GETPDUDATA_EXIT
/** \brief Exit point of function FrNm_GetPduData() */
#define DBG_FRNM_GETPDUDATA_EXIT(a,b,c)
#endif

#ifndef DBG_FRNM_REPEATMESSAGEREQUEST_ENTRY
/** \brief Entry point of function FrNm_RepeatMessageRequest() */
#define DBG_FRNM_REPEATMESSAGEREQUEST_ENTRY(a)
#endif

#ifndef DBG_FRNM_REPEATMESSAGEREQUEST_EXIT
/** \brief Exit point of function FrNm_RepeatMessageRequest() */
#define DBG_FRNM_REPEATMESSAGEREQUEST_EXIT(a,b)
#endif

#ifndef DBG_FRNM_GETNODEIDENTIFIER_ENTRY
/** \brief Entry point of function FrNm_GetNodeIdentifier() */
#define DBG_FRNM_GETNODEIDENTIFIER_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_GETNODEIDENTIFIER_EXIT
/** \brief Exit point of function FrNm_GetNodeIdentifier() */
#define DBG_FRNM_GETNODEIDENTIFIER_EXIT(a,b,c)
#endif

#ifndef DBG_FRNM_GETLOCALNODEIDENTIFIER_ENTRY
/** \brief Entry point of function FrNm_GetLocalNodeIdentifier() */
#define DBG_FRNM_GETLOCALNODEIDENTIFIER_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_GETLOCALNODEIDENTIFIER_EXIT
/** \brief Exit point of function FrNm_GetLocalNodeIdentifier() */
#define DBG_FRNM_GETLOCALNODEIDENTIFIER_EXIT(a,b,c)
#endif

#ifndef DBG_FRNM_REQUESTBUSSYNCHRONIZATION_ENTRY
/** \brief Entry point of function FrNm_RequestBusSynchronization() */
#define DBG_FRNM_REQUESTBUSSYNCHRONIZATION_ENTRY(a)
#endif

#ifndef DBG_FRNM_REQUESTBUSSYNCHRONIZATION_EXIT
/** \brief Exit point of function FrNm_RequestBusSynchronization() */
#define DBG_FRNM_REQUESTBUSSYNCHRONIZATION_EXIT(a,b)
#endif

#ifndef DBG_FRNM_CHECKREMOTESLEEPINDICATION_ENTRY
/** \brief Entry point of function FrNm_CheckRemoteSleepIndication() */
#define DBG_FRNM_CHECKREMOTESLEEPINDICATION_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_CHECKREMOTESLEEPINDICATION_EXIT
/** \brief Exit point of function FrNm_CheckRemoteSleepIndication() */
#define DBG_FRNM_CHECKREMOTESLEEPINDICATION_EXIT(a,b,c)
#endif

#ifndef DBG_FRNM_GETSTATE_ENTRY
/** \brief Entry point of function FrNm_GetState() */
#define DBG_FRNM_GETSTATE_ENTRY(a,b,c)
#endif

#ifndef DBG_FRNM_GETSTATE_EXIT
/** \brief Exit point of function FrNm_GetState() */
#define DBG_FRNM_GETSTATE_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRNM_GETVERSIONINFO_ENTRY
/** \brief Entry point of function FrNm_GetVersionInfo() */
#define DBG_FRNM_GETVERSIONINFO_ENTRY(a)
#endif

#ifndef DBG_FRNM_GETVERSIONINFO_EXIT
/** \brief Exit point of function FrNm_GetVersionInfo() */
#define DBG_FRNM_GETVERSIONINFO_EXIT(a)
#endif

#ifndef DBG_FRNM_STARTUPERROR_ENTRY
/** \brief Entry point of function FrNm_StartupError() */
#define DBG_FRNM_STARTUPERROR_ENTRY(a)
#endif

#ifndef DBG_FRNM_STARTUPERROR_EXIT
/** \brief Exit point of function FrNm_StartupError() */
#define DBG_FRNM_STARTUPERROR_EXIT(a)
#endif

#ifndef DBG_FRNM_TRANSMIT_ENTRY
/** \brief Entry point of function FrNm_Transmit() */
#define DBG_FRNM_TRANSMIT_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_TRANSMIT_EXIT
/** \brief Exit point of function FrNm_Transmit() */
#define DBG_FRNM_TRANSMIT_EXIT(a,b,c)
#endif

#ifndef DBG_FRNM_ENABLECOMMUNICATION_ENTRY
/** \brief Entry point of function FrNm_EnableCommunication() */
#define DBG_FRNM_ENABLECOMMUNICATION_ENTRY(a)
#endif

#ifndef DBG_FRNM_ENABLECOMMUNICATION_EXIT
/** \brief Exit point of function FrNm_EnableCommunication() */
#define DBG_FRNM_ENABLECOMMUNICATION_EXIT(a,b)
#endif

#ifndef DBG_FRNM_DISABLECOMMUNICATION_ENTRY
/** \brief Entry point of function FrNm_DisableCommunication() */
#define DBG_FRNM_DISABLECOMMUNICATION_ENTRY(a)
#endif

#ifndef DBG_FRNM_DISABLECOMMUNICATION_EXIT
/** \brief Exit point of function FrNm_DisableCommunication() */
#define DBG_FRNM_DISABLECOMMUNICATION_EXIT(a,b)
#endif

#ifndef DBG_FRNM_RXINDICATION_ENTRY
/** \brief Entry point of function FrNm_RxIndication() */
#define DBG_FRNM_RXINDICATION_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_RXINDICATION_EXIT
/** \brief Exit point of function FrNm_RxIndication() */
#define DBG_FRNM_RXINDICATION_EXIT(a,b)
#endif

#ifndef DBG_FRNM_HANDLERXVOTEPDU_ENTRY
/** \brief Entry point of function FrNm_HandleRxVotePdu() */
#define DBG_FRNM_HANDLERXVOTEPDU_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_HANDLERXVOTEPDU_EXIT
/** \brief Exit point of function FrNm_HandleRxVotePdu() */
#define DBG_FRNM_HANDLERXVOTEPDU_EXIT(a,b)
#endif

#ifndef DBG_FRNM_HANDLERXDATAPDU_ENTRY
/** \brief Entry point of function FrNm_HandleRxDataPdu() */
#define DBG_FRNM_HANDLERXDATAPDU_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_HANDLERXDATAPDU_EXIT
/** \brief Exit point of function FrNm_HandleRxDataPdu() */
#define DBG_FRNM_HANDLERXDATAPDU_EXIT(a,b)
#endif

#ifndef DBG_FRNM_HANDLERXMIXEDPDU_ENTRY
/** \brief Entry point of function FrNm_HandleRxMixedPdu() */
#define DBG_FRNM_HANDLERXMIXEDPDU_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_HANDLERXMIXEDPDU_EXIT
/** \brief Exit point of function FrNm_HandleRxMixedPdu() */
#define DBG_FRNM_HANDLERXMIXEDPDU_EXIT(a,b)
#endif

#ifndef DBG_FRNM_TRIGGERTRANSMIT_ENTRY
/** \brief Entry point of function FrNm_TriggerTransmit() */
#define DBG_FRNM_TRIGGERTRANSMIT_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_TRIGGERTRANSMIT_EXIT
/** \brief Exit point of function FrNm_TriggerTransmit() */
#define DBG_FRNM_TRIGGERTRANSMIT_EXIT(a,b,c)
#endif

#ifndef DBG_FRNM_TXCONFIRMATION_ENTRY
/** \brief Entry point of function FrNm_TxConfirmation() */
#define DBG_FRNM_TXCONFIRMATION_ENTRY(a)
#endif

#ifndef DBG_FRNM_TXCONFIRMATION_EXIT
/** \brief Exit point of function FrNm_TxConfirmation() */
#define DBG_FRNM_TXCONFIRMATION_EXIT(a)
#endif

#ifndef DBG_FRNM_SETSLEEPREADYBIT_ENTRY
/** \brief Entry point of function FrNm_SetSleepReadyBit() */
#define DBG_FRNM_SETSLEEPREADYBIT_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_SETSLEEPREADYBIT_EXIT
/** \brief Exit point of function FrNm_SetSleepReadyBit() */
#define DBG_FRNM_SETSLEEPREADYBIT_EXIT(a,b,c)
#endif

#ifndef DBG_FRNM_HANDLEVOTECYCLE_ENTRY
/** \brief Entry point of function FrNm_HandleVoteCycle() */
#define DBG_FRNM_HANDLEVOTECYCLE_ENTRY(a)
#endif

#ifndef DBG_FRNM_HANDLEVOTECYCLE_EXIT
/** \brief Exit point of function FrNm_HandleVoteCycle() */
#define DBG_FRNM_HANDLEVOTECYCLE_EXIT(a)
#endif

#ifndef DBG_FRNM_HANDLEDATACYCLE_ENTRY
/** \brief Entry point of function FrNm_HandleDataCycle() */
#define DBG_FRNM_HANDLEDATACYCLE_ENTRY(a)
#endif

#ifndef DBG_FRNM_HANDLEDATACYCLE_EXIT
/** \brief Exit point of function FrNm_HandleDataCycle() */
#define DBG_FRNM_HANDLEDATACYCLE_EXIT(a)
#endif

#ifndef DBG_FRNM_COPYPDUDATA_ENTRY
/** \brief Entry point of function FrNm_CopyPduData() */
#define DBG_FRNM_COPYPDUDATA_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_COPYPDUDATA_EXIT
/** \brief Exit point of function FrNm_CopyPduData() */
#define DBG_FRNM_COPYPDUDATA_EXIT(a,b)
#endif

#ifndef DBG_FRNM_HANDLESYNCHERROR_ENTRY
/** \brief Entry point of function FrNm_HandleSynchError() */
#define DBG_FRNM_HANDLESYNCHERROR_ENTRY(a)
#endif

#ifndef DBG_FRNM_HANDLESYNCHERROR_EXIT
/** \brief Exit point of function FrNm_HandleSynchError() */
#define DBG_FRNM_HANDLESYNCHERROR_EXIT(a)
#endif

#ifndef DBG_FRNM_HANDLECONTROLBITVECTOR_ENTRY
/** \brief Entry point of function FrNm_HandleControlBitVector() */
#define DBG_FRNM_HANDLECONTROLBITVECTOR_ENTRY(a)
#endif

#ifndef DBG_FRNM_HANDLECONTROLBITVECTOR_EXIT
/** \brief Exit point of function FrNm_HandleControlBitVector() */
#define DBG_FRNM_HANDLECONTROLBITVECTOR_EXIT(a)
#endif

#ifndef DBG_FRNM_HANDLETIMERS_ENTRY
/** \brief Entry point of function FrNm_HandleTimers() */
#define DBG_FRNM_HANDLETIMERS_ENTRY(a)
#endif

#ifndef DBG_FRNM_HANDLETIMERS_EXIT
/** \brief Exit point of function FrNm_HandleTimers() */
#define DBG_FRNM_HANDLETIMERS_EXIT(a)
#endif

#ifndef DBG_FRNM_GETCLUSTERINDEX_ENTRY
/** \brief Entry point of function FrNm_GetClusterIndex() */
#define DBG_FRNM_GETCLUSTERINDEX_ENTRY(a)
#endif

#ifndef DBG_FRNM_GETCLUSTERINDEX_EXIT
/** \brief Exit point of function FrNm_GetClusterIndex() */
#define DBG_FRNM_GETCLUSTERINDEX_EXIT(a,b)
#endif

#ifndef DBG_FRNM_GETPDUINDEX_ENTRY
/** \brief Entry point of function FrNm_GetPduIndex() */
#define DBG_FRNM_GETPDUINDEX_ENTRY(a)
#endif

#ifndef DBG_FRNM_GETPDUINDEX_EXIT
/** \brief Exit point of function FrNm_GetPduIndex() */
#define DBG_FRNM_GETPDUINDEX_EXIT(a,b)
#endif

#ifndef DBG_FRNM_HANDLEPN_ENTRY
/** \brief Entry point of function FrNm_HandlePn() */
#define DBG_FRNM_HANDLEPN_ENTRY()
#endif

#ifndef DBG_FRNM_HANDLEPN_EXIT
/** \brief Exit point of function FrNm_HandlePn() */
#define DBG_FRNM_HANDLEPN_EXIT()
#endif

#ifndef DBG_FRNM_HANDLEPNEIRA_ENTRY
/** \brief Entry point of function FrNm_HandlePnEira() */
#define DBG_FRNM_HANDLEPNEIRA_ENTRY(a)
#endif

#ifndef DBG_FRNM_HANDLEPNEIRA_EXIT
/** \brief Exit point of function FrNm_HandlePnEira() */
#define DBG_FRNM_HANDLEPNEIRA_EXIT(a,b)
#endif

#ifndef DBG_FRNM_AGGREGATEEIRA_ENTRY
/** \brief Entry point of function FrNm_AggregateEira() */
#define DBG_FRNM_AGGREGATEEIRA_ENTRY(a,b)
#endif

#ifndef DBG_FRNM_AGGREGATEEIRA_EXIT
/** \brief Exit point of function FrNm_AggregateEira() */
#define DBG_FRNM_AGGREGATEEIRA_EXIT(a,b,c)
#endif

#ifndef DBG_FRNM_HANDLEPNTIMERS_ENTRY
/** \brief Entry point of function FrNm_HandlePnTimers() */
#define DBG_FRNM_HANDLEPNTIMERS_ENTRY()
#endif

#ifndef DBG_FRNM_HANDLEPNTIMERS_EXIT
/** \brief Exit point of function FrNm_HandlePnTimers() */
#define DBG_FRNM_HANDLEPNTIMERS_EXIT()
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[external data]=========================================*/

#endif /* (!defined FRNM_TRACE_H) */
/*==================[end of file]===========================================*/
