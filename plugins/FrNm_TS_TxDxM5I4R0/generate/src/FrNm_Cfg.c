/**
 * \file
 *
 * \brief AUTOSAR FrNm
 *
 * This file contains the implementation of the AUTOSAR
 * module FrNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
/* !LINKSTO FRNM064_Refine,1 */
[!AUTOSPACING!][!//
/*==================[inclusions]============================================*/
#include <Std_Types.h>        /* AUTOSAR standard types */
#include <FrNm_Types.h>
#include <FrNm_Int.h>        /* FrNm internal dependent information */
/*==================[macros]================================================*/
[!INCLUDE "../include/FrNm_Checks.m"!][!//
[!NOCODE!][!//
[!// Get the TxPduId defined by FrIf.
[!MACRO "FrNm_GetFrIfTxPduId", "TxPduRef"!][!//
    [!"as:modconf('FrIf')[1]/FrIfConfig/*/FrIfPdu/*/FrIfPduDirection[FrIfTxPduRef = $TxPduRef]/FrIfTxPduId"!][!//
[!ENDMACRO!][!//
[!ENDNOCODE!][!//

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[internal data]=========================================*/
#define FRNM_START_SEC_VAR_INIT_8
#include <MemMap.h>

[!LOOP "node:order(FrNmChannelConfig/*/FrNmChannel/*, 'node:value(node:ref(FrNmChannelIdentifiers/FrNmChannelHandle)/FrIfClstIdx)')"!][!//
/* Buffer to store Pdu data be received. One buffer is used for one channel with maximum length of
 * the associated Pdus.
 */
STATIC VAR(uint8, AUTOSAR_COMSTACKDATA) FrNm_RxPdu_[!"name(.)"!]_Data[[!"num:max(node:refs(FrNmChannelIdentifiers/FrNmRxPdu/*/FrNmRxPduRef)/PduLength)"!]U];
[!IF "num:i(count(FrNmChannelIdentifiers/FrNmTxPdu/*)) !=0"!]
/* Buffer to store Pdu data be sent. One buffer is used for one channel with maximum length of
 * the associated Pdus.
 */
STATIC VAR(uint8, AUTOSAR_COMSTACKDATA) FrNm_TxPdu_[!"name(.)"!]_Data[[!"num:max(node:refs(FrNmChannelIdentifiers/FrNmTxPdu/*/FrNmTxPduRef)/PduLength)"!]U];
[!ENDIF!]
[!ENDLOOP!][!//

#define FRNM_STOP_SEC_VAR_INIT_8
#include <MemMap.h>
/*==================[external data]=========================================*/
#define  FRNM_START_SEC_CONST_8
#include <MemMap.h>

[!IF "count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers[FrNmPnEnabled ='true']) > 0"!]
[!SELECT "FrNmGlobalConfig/FrNmGlobalFeatures/FrNmPnInfo"!][!//
CONST(uint8, FRNM_CONST) FrNm_PnFilterMask[[!"FrNmPnInfoLength"!]U] =
{
[!FOR "bytePos"="0" TO "FrNmPnInfoLength - 1"!]
  [!"num:inttohex(FrNmPnFilterMaskByte/*[FrNmPnFilterMaskByteIndex = $bytePos]/FrNmPnFilterMaskByteValue)"!]U,[!//
[!ENDFOR!]
};
[!ENDSELECT!][!//
[!ENDIF!]

#define  FRNM_STOP_SEC_CONST_8
#include <MemMap.h>


#define FRNM_START_SEC_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

[!IF "as:modconf('FrNm')[1]/FrNmGlobalConfig/FrNmGlobalFeatures/FrNmComUserDataSupport = 'true'"!]
CONST(FrNm_UserDataConfigType, FRNM_CONST)
  FrNm_ComUserDataInfo[FRNM_NUMBER_OF_CHANNELS]=
{
[!LOOP "node:order(FrNmChannelConfig/*/FrNmChannel/*, 'node:value(node:ref(FrNmChannelIdentifiers/FrNmChannelHandle)/FrIfClstIdx)')"!]
  /* Configuration of FR NM channel [!"name(.)"!] : */
  {
   [!/* Userdata length is calculated from the minimum of PduLength of the Tx and Rx containers */!][!//
   [!VAR "TxPduLength" = "255"!][!//
   [!VAR "RxPduLength" = "255"!][!//
[!IF "node:exists(FrNmChannelIdentifiers/FrNmUserDataTxPdu)"!][!//
    [!"FrNmChannelIdentifiers/FrNmUserDataTxPdu/FrNmTxUserDataPduId"!]U, /* User Data Tx PduId */[!/*
    Get User Data TxConf PduId by EcuC reference from PduR;
    existence is already checked in FrNm_Checks.m.
    */!]
    [!"as:modconf('PduR')[1]/PduRRoutingTables/*/PduRRoutingTable/*/PduRRoutingPath/*/
       PduRDestPdu/*[PduRDestPduRef=node:current()/FrNmChannelIdentifiers/FrNmUserDataTxPdu/FrNmTxUserDataPduRef]/
       PduRDestPduHandleId"!]U, /* User Data Tx Confirmation PduId */
    [!VAR "TxPduLength" = "num:i(as:ref(FrNmChannelIdentifiers/FrNmUserDataTxPdu/FrNmTxUserDataPduRef)/PduLength)"!]
[!ELSE!][!//
    0U, /* User Data Tx PduId */
    0U, /* User Data Tx Confirmation PduId */
[!ENDIF!][!//
[!IF "node:exists(FrNmChannelIdentifiers/FrNmUserDataRxPdu)"!][!//
    [!"as:modconf('PduR')[1]/PduRRoutingTables/*/PduRRoutingTable/*/PduRRoutingPath/*/PduRSrcPdu[PduRSrcPduRef=node:current()/FrNmChannelIdentifiers/FrNmUserDataRxPdu/FrNmRxUserDataPduRef]/PduRSourcePduHandleId"!]U, /* UserRxPduRPduId */
    [!VAR "RxPduLength" = "num:i(as:ref(FrNmChannelIdentifiers/FrNmUserDataRxPdu/FrNmRxUserDataPduRef)/PduLength)"!]
[!ELSE!][!//
    0U, /* UserRxPduPduId */
[!ENDIF!][!//
[!IF "$TxPduLength != 255"!]
  [!"$TxPduLength"!]U, /* PduLength */
[!ELSE!][!//
  0U, /* PduLength */
[!ENDIF!][!//
[!IF "node:exists(FrNmChannelIdentifiers/FrNmUserDataTxPdu)"!][!//
    TRUE, /* ComUserTxPduEnabled */
[!ELSE!][!//
    FALSE, /* ComUserTxPduEnabled */
[!ENDIF!][!//
[!IF "node:exists(FrNmChannelIdentifiers/FrNmUserDataRxPdu)"!][!//
    TRUE /* ComUserRxPduEnabled */
[!ELSE!][!//
    FALSE /* ComUserRxPduEnabled */
[!ENDIF!][!//
  },
[!ENDLOOP!]
};
[!ENDIF!]

[!LOOP "node:order(FrNmChannelConfig/*/FrNmChannel/*, 'node:value(node:ref(FrNmChannelIdentifiers/FrNmChannelHandle)/FrIfClstIdx)')"!][!//
[!IF "num:i(count(FrNmChannelIdentifiers/FrNmTxPdu/*[FrNmTxPduContainsVote = 'true'])) !=0"!][!//
/* Arrays for each channel holding Pdu Ids used to send Nm vote with or without Nm data. In normal
 * case, only one Pdu is required to send vote for one channel, but there can be upto 2 Pdus to
 * send vote when dual channel is used.
 */
CONST(PduIdType, FRNM_CONST) FrNm_VoteTxPdu_[!"name(.)"!][[!"num:i(count(FrNmChannelIdentifiers/FrNmTxPdu/*[FrNmTxPduContainsVote = 'true']))"!]] =
{
[!LOOP "FrNmChannelIdentifiers/FrNmTxPdu/*[FrNmTxPduContainsVote = 'true']"!][!//
[!CALL "FrNm_GetFrIfTxPduId","TxPduRef" = "FrNmTxPduRef"!],
[!ENDLOOP!][!//
};[!//
[!ENDIF!][!//
[!ENDLOOP!][!//

[!LOOP "node:order(FrNmChannelConfig/*/FrNmChannel/*, 'node:value(node:ref(FrNmChannelIdentifiers/FrNmChannelHandle)/FrIfClstIdx)')"!][!//
[!IF "num:i(count(FrNmChannelIdentifiers/FrNmTxPdu/*[FrNmTxPduContainsData='true' and FrNmTxPduContainsVote='false'])) !=0"!][!//
/* Arrays for each channel holding Pdu Ids used to send Nm data. */
/* Pdu Ids used to send vote and data together are not considered */
CONST(PduIdType, FRNM_CONST) FrNm_TxPdu_[!"name(.)"!][[!"num:i(count(FrNmChannelIdentifiers/FrNmTxPdu/*[FrNmTxPduContainsData='true' and FrNmTxPduContainsVote='false']))"!]] =
{
[!LOOP "FrNmChannelIdentifiers/FrNmTxPdu/*[FrNmTxPduContainsData='true' and FrNmTxPduContainsVote='false']"!][!//
[!CALL "FrNm_GetFrIfTxPduId","TxPduRef" = "FrNmTxPduRef"!],
[!ENDLOOP!][!//
};[!//
[!ENDIF!][!//
[!ENDLOOP!][!//

[!VAR "GlobalTxPduCount" = "num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmTxPdu/*[FrNmTxPduContainsVote = 'true']))"!][!//
[!VAR "GlobalDataTxPduCount" = "num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmTxPdu/*[FrNmTxPduContainsData = 'true' and FrNmTxPduContainsVote='false']))"!][!//

/* Array with the first controller index for each cluster */
CONST(uint8, FRNM_CONST) FrNm_CtrlIdx[FRNM_NUMBER_OF_CHANNELS]=
{
[!LOOP "node:order(FrNmChannelConfig/*/FrNmChannel/*, 'node:value(node:ref(FrNmChannelIdentifiers/FrNmChannelHandle)/FrIfClstIdx)')"!]
  [!"num:i(node:value(node:ref(FrNmChannelIdentifiers/FrNmChannelHandle)/FrIfController/*[1]/FrIfCtrlIdx))"!]U,
[!ENDLOOP!]
};

/* Configuration data structure for one channel or cluster */
CONST(FrNm_ChnConfigType, FRNM_CONST) FrNm_ChanConfig[FRNM_NUMBER_OF_CHANNELS] =
{
  [!LOOP "node:order(FrNmChannelConfig/*/FrNmChannel/*, 'node:value(node:ref(FrNmChannelIdentifiers/FrNmChannelHandle)/FrIfClstIdx)')"!][!//
  {
    /* Receive buffer */
    FrNm_RxPdu_[!"name(.)"!]_Data,
    [!IF "num:i(count(FrNmChannelIdentifiers/FrNmTxPdu/*)) !=0"!]
    /* Send buffer */
    FrNm_TxPdu_[!"name(.)"!]_Data,
    [!ELSE!][!//
    NULL_PTR,
    [!ENDIF!]
    [!SELECT "FrNmChannelIdentifiers"!]
    {
      [!VAR "VoteTxPduCount" = "num:i(count(FrNmTxPdu/*[FrNmTxPduContainsVote = 'true']))"!][!//
      [!IF "$VoteTxPduCount != 0"!][!//
      FrNm_VoteTxPdu_[!"name(..)"!],/* Array of Pdu Ids use to send Nm Vote with or without Nm Data */
      [!ENDIF!][!//
      [!VAR "DataTxPduCount" = "num:i(count(FrNmTxPdu/*[FrNmTxPduContainsData = 'true' and FrNmTxPduContainsVote='false']))"!][!//
      [!IF "$DataTxPduCount != 0"!][!//
      FrNm_TxPdu_[!"name(..)"!], /* Array holding Pdu Ids use to send Nm Data only. */
      [!ELSE!][!//
        [!IF "$GlobalDataTxPduCount != 0"!][!//
        NULL_PTR,
        [!ENDIF!][!//
      [!ENDIF!][!//
      [!VAR "RxPduLength" = "num:max(node:refs(FrNmRxPdu/*/FrNmRxPduRef)/PduLength)"!][!// /* Length of the PduId to read or write data to the buffers.*/
      [!VAR "TxPduLength" = "0"!][!//
      [!IF "num:i(count(FrNmTxPdu/*)) !=0"!][!//
      [!VAR "TxPduLength" = "num:max(node:refs(FrNmTxPdu/*/FrNmTxPduRef)/PduLength)"!][!//
      [!ENDIF!][!//
      [!IF "$RxPduLength > $TxPduLength"!][!//
      [!"$RxPduLength"!]U[!//
      [!ELSE!][!//
      [!"$TxPduLength"!]U[!//
      [!ENDIF!], /* PduLength */
      [!"FrNmNodeId"!]U, /* NodeId */
      [!"num:i(as:ref(FrNmComMNetworkHandleRef)/ComMChannelId)"!]U, /* Nm channel Id */
      /* ChannelProperty */
      [!"FrNmPduScheduleVariant"!] [!/*
      */!][!IF "FrNmControlBitVectorActive = 'true'"!][!/*
      */!]| FRNM_CONTROL_BIT_VECTOR [!//
      [!ENDIF!][!//
      [!IF "FrNmRepeatMessageBitActive = 'true'"!][!/*
      */!]| FRNM_REPEAT_MESSAGE_BIT [!//
      [!ENDIF!][!//
      [!IF "FrNmSynchronizationPointEnabled = 'true'"!][!/*
      */!]| FRNM_SYNCH_POINT_ENABLED [!//
      [!ENDIF!][!//
      [!IF "../FrNmChannelTiming/FrNmVoteInhibitionEnabled = 'true'"!][!/*
      */!]| FRNM_VOTE_INHIBITION_ENABLED [!//
      [!ENDIF!][!//
      [!IF "num:i(count(FrNmTxPdu/*[FrNmTxPduContainsData = 'true'])) != 0"!][!/*
      */!]| FRNM_NM_DATA_ENABLED
      [!ENDIF!],
      [!IF "$VoteTxPduCount != 0"!][!//
        [!"$VoteTxPduCount"!]U,  /* NoOfTxVotePdus */
      [!ENDIF!][!//
      [!IF "$DataTxPduCount != 0"!][!//
        [!"$DataTxPduCount"!]U,  /* NoOfTxDataPdus */
      [!ELSE!][!//
        [!IF "$GlobalDataTxPduCount != 0"!][!//
          0U,  /* NoOfTxDataPdus */
        [!ENDIF!][!//
      [!ENDIF!][!//
      [!IF "FrNmPnEnabled = 'true'"!][!//
      TRUE /* PnEnabled */
      [!ELSE!][!//
      FALSE /* PnEnabled */
      [!ENDIF!][!//
    },
    [!ENDSELECT!]
    [!SELECT "FrNmChannelTiming"!]
    {
      [!"num:i((FrNmMsgTimeoutTime * 1000) div (FrNmMainFunctionPeriod * 1000))"!]U, /* Msg Timeout Time */
      [!"FrNmReadySleepCnt"!], /* Number of repetitions in the ready sleep state */
      [!"num:i((FrNmRemoteSleepIndTime * 1000) div (FrNmMainFunctionPeriod * 1000))"!]U, /* Remote sleep indication Time */
      [!"num:i((FrNmRepeatMessageTime * 1000) div (FrNmMainFunctionPeriod * 1000))"!]U, /* Timeout for repeat message state */
      [!"FrNmDataCycle"!], /* Number of FlexRay schedule cycles for Nm data be transmitted*/
      [!"FrNmRepetitionCycle"!], /* Number of FlexRay schedule cycles for Nm vote be changed*/
      [!"FrNmVotingCycle"!], /* Number of FlexRay schedule cycles for Nm vote be transmitted*/
    },
    [!ENDSELECT!]
  },
[!ENDLOOP!][!//
};
/* Mapping between RxPdus to PduType and Cluster */
CONST (FrNm_PduType, FRNM_CONST) FrNm_RxPduMap[FRNM_NUMBER_OF_RX_PDUS] =
{
  [!LOOP "node:order(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmRxPdu/*, 'FrNmRxPduId')"!][!//
  {
    [!IF "FrNmRxPduContainsData = 'true'"!][!//
      [!IF "FrNmRxPduContainsVote = 'true'"!][!//
    FRNM_MIXED,
      [!ELSE!][!//
    FRNM_DATA,
      [!ENDIF!][!//
    [!ELSE!][!//
    FRNM_VOTE,
    [!ENDIF!][!//
    [!"num:i(as:ref(../../FrNmChannelHandle)/FrIfClstIdx)"!]U,
  },
  [!ENDLOOP!]
};

[!IF "num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmTxPdu/*)) !=0"!]
/* Mapping between TxPdus to PduType and Cluster */
CONST (FrNm_TxPduType, FRNM_CONST) FrNm_TxPduMap[FRNM_NUMBER_OF_TX_PDUS] =
{
  [!LOOP "node:order(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmTxPdu/*, 'FrNmTxConfirmationPduId')"!][!//
  {
    [!CALL "FrNm_GetFrIfTxPduId","TxPduRef" = "FrNmTxPduRef"!]U,   /* Tx Pdu Id */
    [!"FrNmTxConfirmationPduId"!]U,   /* Tx Confirmation PduId */
    [!"num:i(as:ref(FrNmTxPduRef)/PduLength)"!]U,   /* Pdu length */
    [!IF "FrNmTxPduContainsData = 'true'"!][!//
      [!IF "FrNmTxPduContainsVote = 'true'"!][!//
    FRNM_MIXED,     /* Pdu Type */
      [!ELSE!][!//
    FRNM_DATA,      /* Pdu Type */
      [!ENDIF!][!//
    [!ELSE!][!//
    FRNM_VOTE,      /* Pdu Type */
    [!ENDIF!][!//
    [!"num:i(as:ref(../../FrNmChannelHandle)/FrIfClstIdx)"!]U,  /* Cluster Id */
  },
  [!ENDLOOP!]
};
[!ENDIF!]

#define FRNM_STOP_SEC_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

#define FRNM_START_SEC_CONFIG_DATA_8
#include <MemMap.h>

#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
[!VAR "Index" = "0"!]
/* Mapping between PN Index and EIRA timer array */
CONST(uint8, FRNM_CONST) FrNm_EiraTimerMap[FRNM_PN_EIRA_TIMER_SIZE] =
{
[!LOOP "FrNmGlobalConfig/FrNmGlobalFeatures/FrNmPnInfo/FrNmPnFilterMaskByte/*"!][!//
  [!FOR "BitPos" = "0" TO "7"!][!//
    [!IF "bit:getbit(node:value(./FrNmPnFilterMaskByteValue), $BitPos) = 'true' "!][!//
  [!"num:i($Index)"!]U,
      [!VAR "Index" = "$Index + 1"!][!//
    [!ELSE!][!//
  FRNM_EIRA_INVALID_INDEX,
    [!ENDIF!][!//
  [!ENDFOR!][!//
[!ENDLOOP!]
};
#endif
#define FRNM_STOP_SEC_CONFIG_DATA_8
#include <MemMap.h>

/*==================[external function definitions]=========================*/
#define FRNM_START_SEC_CODE
#include <MemMap.h>
[!VAR "LeastMainFnPeriodChId" = "0"!][!//
[!VAR "LeastMainFnPeriodChId" = "node:value(node:ref(FrNmChannelConfig/*/FrNmChannel/*[FrNmChannelTiming/FrNmMainFunctionPeriod = num:min(../*/FrNmChannelTiming/FrNmMainFunctionPeriod)]/FrNmChannelIdentifiers/FrNmChannelHandle)/FrIfClstIdx)"!]

[!FOR "I"="0" TO "num:i(FrNmGlobalConfig/FrNmGlobalConstants/FrNmNumberOfClusters)-1"!]
  FUNC(void, FRNM_CODE) FrNm_MainFunction_[!"num:i($I)"!](void)
  {
[!IF "$I = $LeastMainFnPeriodChId"!]
#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
  if (FrNm_InitStatus == FRNM_INIT)
  {
    FrNm_HandlePnTimers();
  }
#endif
[!ENDIF!]
    FrNm_MainFunction([!"num:i($I)"!]U);
  }
[!ENDFOR!]

#define FRNM_STOP_SEC_CODE
#include <MemMap.h>
/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
