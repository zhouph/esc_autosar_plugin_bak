/**
 * \file
 *
 * \brief AUTOSAR IpduM
 *
 * This file contains the implementation of the AUTOSAR
 * module IpduM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 * MISRA-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 14.7 (required)
 * A function shall have a single point of exit at the end of the function.
 *
 * Reason:
 * To reduce indentation-level of code and make it more comprehensible.
 *
 * MISRA-2) Deviated Rule: 11.4 (advisory)
 * A cast should not be performed between a pointer to object type and a
 * different pointer to object type
 *
 * Reason:
 * For post build configuration this type cast is needed.
 */


/*==================[inclusions]============================================*/
#include <IpduM_Trace.h>
#include <ComStack_Types.h>
#include <IpduM_Lcfg_Static.h>
#include <PduR_IpduM.h>         /* !LINKSTO IPDUM.ASR40.IPDUM148_3,1 */
#include <IpduM_Cbk.h>          /* !LINKSTO IPDUM.ASR40.IPDUM148_2,1 */
#include <IpduM_Api.h>          /* !LINKSTO IPDUM.EB.IPDUM148,1 */
#include <TSAtomic_Assign.h>
#include <IpduM_Int.h>

#if (IPDUM_DEV_ERROR_DETECT==STD_ON)
#include <Det.h>               /* !LINKSTO IPDUM.ASR40.IPDUM148_5,1 */
#endif

/*==================[macros]================================================*/

/*------------------[AUTOSAR vendor identification check]-------------------*/

#if (!defined IPDUM_VENDOR_ID) /* configuration check */
#error IPDUM_VENDOR_ID must be defined
#endif

#if (IPDUM_VENDOR_ID != 1U) /* vendor check */
#error IPDUM_VENDOR_ID has wrong vendor id
#endif

/*------------------[AUTOSAR release version identification check]----------*/

#if (!defined IPDUM_AR_RELEASE_MAJOR_VERSION) /* configuration check */
#error IPDUM_AR_RELEASE_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (IPDUM_AR_RELEASE_MAJOR_VERSION != 4U)
#error IPDUM_AR_RELEASE_MAJOR_VERSION wrong (!= 4U)
#endif

#if (!defined IPDUM_AR_RELEASE_MINOR_VERSION) /* configuration check */
#error IPDUM_AR_RELEASE_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (IPDUM_AR_RELEASE_MINOR_VERSION != 0U )
#error IPDUM_AR_RELEASE_MINOR_VERSION wrong (!= 0U)
#endif

#if (!defined IPDUM_AR_RELEASE_REVISION_VERSION) /* configuration check */
#error IPDUM_AR_RELEASE_REVISION_VERSION must be defined
#endif

/* revision version check */
#if (IPDUM_AR_RELEASE_REVISION_VERSION != 3U )
#error IPDUM_AR_RELEASE_REVISION_VERSION wrong (!= 3U)
#endif

/*------------------[AUTOSAR module version identification check]-----------*/

#if (!defined IPDUM_SW_MAJOR_VERSION) /* configuration check */
#error IPDUM_SW_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (IPDUM_SW_MAJOR_VERSION != 3U)
#error IPDUM_SW_MAJOR_VERSION wrong (!= 3U)
#endif

#if (!defined IPDUM_SW_MINOR_VERSION) /* configuration check */
#error IPDUM_SW_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (IPDUM_SW_MINOR_VERSION < 2U)
#error IPDUM_SW_MINOR_VERSION wrong (< 2U)
#endif

#if (!defined IPDUM_SW_PATCH_VERSION) /* configuration check */
#error IPDUM_SW_PATCH_VERSION must be defined
#endif

/* patch version check */
#if (IPDUM_SW_PATCH_VERSION < 5U)
#error IPDUM_SW_PATCH_VERSION wrong (< 5U)
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/
#define IPDUM_START_SEC_CODE
#include <MemMap.h>

#if (IPDUM_ZERO_COPY==STD_OFF)
IPDUM_STATIC FUNC(PduLengthType, IPDUM_CODE) IpduM_BitCopy
(
  P2VAR(uint8, AUTOMATIC, IPDUM_APPL_DATA) Dest,
  P2CONST(uint8, AUTOMATIC, IPDUM_APPL_DATA) Src,
  P2CONST(IpduM_CopyBitFieldType, AUTOMATIC, IPDUM_APPL_CONST) CopyBitField,
  IpduM_BitfieldArraySizeType Size
);

IPDUM_STATIC FUNC(void, IPDUM_CODE) IpduM_PreparePdu
(
  P2CONST(IpduM_TxPathWayType, AUTOMATIC, IPDUM_APPL_CONST) TxPathWay
);
#endif

#if (IPDUM_DYNAMIC_PART_QUEUE==STD_ON)
IPDUM_STATIC FUNC(Std_ReturnType, IPDUM_CODE) IpduM_InsertQueue
(
  PduIdType PdumTxPduId,
  PduIdType PduRTxConfirmationPduId,
  P2CONST(PduInfoType, AUTOMATIC, IPDUM_APPL_DATA) PduInfoPtr,
  uint16 DynamicPriority
);

IPDUM_STATIC FUNC(P2VAR(IpduM_QueueEntryType, AUTOMATIC, IPDUM_VAR_NOINIT), IPDUM_CODE) IpduM_RemoveQueue
(
    PduIdType TxConfirmationPduId
);

#if (IPDUM_ZERO_COPY==STD_OFF)
IPDUM_STATIC FUNC(void,IPDUM_CODE) IpduM_SetDynamicPduReadyToSend(PduIdType PdumTxPduId);
#endif

IPDUM_STATIC FUNC(uint16, IPDUM_CODE) IpduM_GetMaxPriorityQueue(PduIdType PdumTxPduId);
#endif

#define IPDUM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/
#define IPDUM_START_SEC_VAR_FAST_INIT_8
#include <MemMap.h>

/** \brief IpduM_InitStatus
 * global variable for the actual initialization status of the IpduM */
/* !LINKSTO IPDUM.ASR40.IPDUM075,1 */
IPDUM_STATIC VAR(IpduM_StatusType, IPDUM_VAR_FAST) IpduM_InitStatus = IPDUM_UNINIT;

#define IPDUM_STOP_SEC_VAR_FAST_INIT_8
#include <MemMap.h>

#define IPDUM_START_SEC_VAR_FAST_INIT_UNSPECIFIED
#include <MemMap.h>

/** \brief IpduM_ConfigPtr
 * global variable for the config ptr of the IpduM */
/* !LINKSTO IPDUM.ASR40.IPDUM075,1 */
IPDUM_STATIC P2CONST(IpduM_ConfigType, IPDUM_VAR, IPDUM_APPL_CONST) IpduM_ConfigPtr = NULL_PTR;

#define IPDUM_STOP_SEC_VAR_FAST_INIT_UNSPECIFIED
#include <MemMap.h>

/*==================[external function definitions]=========================*/

#define IPDUM_START_SEC_CODE
#include <MemMap.h>

/*--------------------------------------------------------------------------*/
/* Deviation MISRA-1 */
FUNC(void, IPDUM_CODE) IpduM_Init
(
  P2CONST(IpduM_ConfigType, AUTOMATIC, IPDUM_APPL_CONST) Config
)
{
  P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT) TxData;
  P2CONST(IpduM_TxPathWayType, AUTOMATIC, IPDUM_APPL_CONST) TxPathWayI;
  uint16 i;
#if (IPDUM_DEV_ERROR_DETECT==STD_ON)
  boolean InitFlag = TRUE;
#endif

  DBG_IPDUM_INIT_ENTRY(Config);

#if (IPDUM_DEV_ERROR_DETECT==STD_ON)

  if (NULL_PTR == Config)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_INIT, IPDUM_E_PARAM_POINTER);
    InitFlag = FALSE;
  }
  else
  {
    /* Check if IpduMDataMemSize is big enough for the current configuration */
    if (NULL_PTR != Config->TxPathWay)
    {
#if (IPDUM_ZERO_COPY==STD_OFF)
      uint16 ByteSize = IPDUM_GET_BYTE_SIZE(Config->TxPathWay[Config->TxPathWayLen-1U].Size);

      if (((uint32)(Config->TxPathWay[Config->TxPathWayLen-1U].BufferOffset)+ ByteSize) >
            IpduM_gDataMemSize
         )
#elif (IPDUM_DYNAMIC_PART_QUEUE==STD_ON)
      if (((uint32)(Config->TxPathWay[Config->TxPathWayLen-1U].QueueOffset +
            (Config->TxPathWay[Config->TxPathWayLen-1U].QueueSize * sizeof(IpduM_QueueEntryType)))) >
            IpduM_gDataMemSize
         )
#else
      if ((uint32)(Config->TxPathWayLen*sizeof(IpduM_TxDataType)) > IpduM_gDataMemSize )
#endif
      {
        IPDUM_DET_REPORT_ERROR(IPDUM_SID_INIT, IPDUM_E_PARAM);
        InitFlag = FALSE;
      }
    }
  }
  if (InitFlag == TRUE)
#endif

  {
    IpduM_ConfigPtr = Config;

    if (NULL_PTR != Config->TxPathWay)
    {
      /* Deviation MISRA-2 */
      TxData = (P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT))(&IpduM_gDataMemPtr[IPDUM_TX_DATA_OFFSET]);

      for (i=0; i<IPDUM_TX_PATHWAY_LEN; i++)
      {
        TxPathWayI = &Config->TxPathWay[i];

        if (TxPathWayI->Size > 0)
        {
#if (IPDUM_ZERO_COPY==STD_OFF)
          /* Initialize selector and initial values */
          IpduM_PreparePdu(TxPathWayI);
#endif
          /* Initialize timeout monitor */
          TxData[i].ConfirmationTimeoutCounter = 0U;

          /* Set confirmation ID to invalid value */
          TxData[i].PduRTxConfirmationPduId = IPDUM_RESERVED_PDUID;

#if (IPDUM_DYNAMIC_PART_QUEUE == STD_ON)
          /* Initialize queue position */
          TxData[i].QueuePos = 0U;
#endif
        }
      }
    }

    /* tag module as initialized */

    DBG_IPDUM_INITSTATUS((IpduM_InitStatus),(IPDUM_INIT));
    IpduM_InitStatus = IPDUM_INIT;
  }

  DBG_IPDUM_INIT_EXIT(Config);
}
/*--------------------------------------------------------------------------*/
/* Deviation MISRA-1 */
FUNC(void, IPDUM_CODE) IpduM_RxIndication
(
  PduIdType RxPduId,
  P2VAR(PduInfoType, AUTOMATIC, IPDUM_APPL_DATA) PduInfoPtr
)
{
  uint16 i;
  IpduM_SelectorType Selector;
#if (IPDUM_ZERO_COPY==STD_OFF)
  uint8 Buffer[IPDUM_RECEIVE_STACK_SIZE];
  PduInfoType PduBufferInfo = {NULL_PTR, 0U};
#else
  PduInfoType LocalPduInfo;
  IpduM_BitfieldType Offset;
  IpduM_BitfieldType EndByte;
#endif
  P2CONST(IpduM_RxPathWayType, AUTOMATIC, IPDUM_APPL_CONST) RxPathWayRI;
  P2CONST(IpduM_RxPartType, AUTOMATIC, IPDUM_APPL_CONST) RxDynamicPart = NULL_PTR;

  DBG_IPDUM_RXINDICATION_ENTRY(RxPduId,PduInfoPtr);

#if (IPDUM_DEV_ERROR_DETECT==STD_ON)
  if (IPDUM_INIT != IpduM_InitStatus)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_RX_INDICATION, IPDUM_E_UNINIT);
  }
  else if (RxPduId >= IPDUM_RX_PATHWAY_LEN)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_RX_INDICATION, IPDUM_E_PARAM);
  }
  else if (NULL_PTR == PduInfoPtr)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_RX_INDICATION, IPDUM_E_PARAM_POINTER);
  }
  else if (NULL_PTR == PduInfoPtr->SduDataPtr)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_RX_INDICATION, IPDUM_E_PARAM_POINTER);
  }
  else
#endif

  {
    /* Get RxPathWay container */
    RxPathWayRI = &IpduM_ConfigPtr->RxPathWay[RxPduId];

    /* Extract selector */
    Selector = IPDUM_GET_SELECTOR(PduInfoPtr->SduDataPtr, RxPathWayRI->StartBit, RxPathWayRI->EndBit);

    /* Search for dynamic part */
    i = 0U;
    while(i < RxPathWayRI->DynamicPduArraySize)
    {
      if (RxPathWayRI->DynamicPart[i].SelectorValue == Selector)
      {
        /* Found, set RxDynamicPart */
        RxDynamicPart = &RxPathWayRI->DynamicPart[i];
        break;
      }
      i++;
    }

#if (IPDUM_ZERO_COPY==STD_ON)

    /* Static part exists? */
#if (IPDUM_STATIC_PART_EXISTS==STD_ON)
    if (NULL_PTR != RxPathWayRI->StaticPart)
    {
      /* Calculate offset */
      Offset = IPDUM_GET_BYTE_OFFSET(RxPathWayRI->StaticPart->CopyBitField[0U].StartBit);
      EndByte = IPDUM_GET_BYTE_OFFSET(RxPathWayRI->StaticPart->CopyBitField[0U].EndBit);

      /* Call rx indication for static part */
      LocalPduInfo.SduLength = (EndByte - Offset) + 1U;
      LocalPduInfo.SduDataPtr = &PduInfoPtr->SduDataPtr[Offset];
      PduR_IpduMRxIndication(RxPathWayRI->StaticPart->OutgoingHandleId, &LocalPduInfo);
    }
#endif

    /* Always ensure that the selector is valid */
    if ( NULL_PTR != RxDynamicPart )
    {
      /* Calculate offset */
      Offset = IPDUM_GET_BYTE_OFFSET(RxDynamicPart->CopyBitField[0].StartBit);
      EndByte = IPDUM_GET_BYTE_OFFSET(RxDynamicPart->CopyBitField[0].EndBit);

      /* Call rx indication for dynamic part */
      LocalPduInfo.SduLength = (EndByte - Offset) + 1U;
      LocalPduInfo.SduDataPtr = &PduInfoPtr->SduDataPtr[Offset];
      PduR_IpduMRxIndication(RxDynamicPart->OutgoingHandleId, &LocalPduInfo);
    }
#else /* IPDUM_ZERO_COPY==STD_ON) */
#if (IPDUM_STATIC_PART_EXISTS==STD_ON)
    if (NULL_PTR != RxPathWayRI->StaticPart)
    {
      PduBufferInfo.SduDataPtr = &Buffer[0];

      /* Assemble fragmented static part into temporary buffer */
      PduBufferInfo.SduLength = IpduM_BitCopy(&Buffer[0],
        PduInfoPtr->SduDataPtr,
        RxPathWayRI->StaticPart->CopyBitField,
        RxPathWayRI->StaticPart->CopyBitFieldArraySize);

      /* Call rx indication for static part */
      PduR_IpduMRxIndication(RxPathWayRI->StaticPart->OutgoingHandleId, &PduBufferInfo);
    }
#endif

    /* Always ensure that the selector is valid */
    if ( NULL_PTR != RxDynamicPart )
    {
      PduBufferInfo.SduDataPtr = &Buffer[0];

      /* Assemble fragmented dynamic part into temporary buffer */
      PduBufferInfo.SduLength = IpduM_BitCopy(&Buffer[0],
        PduInfoPtr->SduDataPtr,
        RxDynamicPart->CopyBitField,
        RxDynamicPart->CopyBitFieldArraySize);

      /* Call rx indication for dynamic part */
      PduR_IpduMRxIndication(RxDynamicPart->OutgoingHandleId, &PduBufferInfo);
    }
#endif
  }

  DBG_IPDUM_RXINDICATION_EXIT(RxPduId,PduInfoPtr);
}
/*--------------------------------------------------------------------------*/
/* Deviation MISRA-1 */
FUNC(Std_ReturnType, IPDUM_CODE) IpduM_Transmit
(
  PduIdType PdumTxPduId,
  P2CONST(PduInfoType, AUTOMATIC, IPDUM_APPL_DATA) PduInfoPtr
)
{
  P2CONST(IpduM_TxPartType, AUTOMATIC, IPDUM_APPL_CONST) TxPartT;
  P2CONST(IpduM_TxPathWayType, AUTOMATIC, IPDUM_APPL_CONST) TxPathWayT;
  P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT) TxData;
  PduIdType PdumDynamicTxPduId = IPDUM_RESERVED_PDUID; /* local storage of PDU ID of dynamic part */
  PduIdType PduRTxConfirmationPduId = IPDUM_RESERVED_PDUID; /* local storage of response PDU ID of dynamic part */
#if (IPDUM_DYNAMIC_PART_QUEUE == STD_ON)
  uint16 DynPriority;
  boolean ImmediatelySend = FALSE;
#if (IPDUM_ZERO_COPY==STD_OFF)
  P2VAR(IpduM_QueueEntryType, AUTOMATIC, IPDUM_VAR_NOINIT) QueueEntry;
#endif
#endif

  PduInfoType PduTxInfo = { NULL_PTR, 0U };

  Std_ReturnType RetValue = E_NOT_OK;

  DBG_IPDUM_TRANSMIT_ENTRY(PdumTxPduId,PduInfoPtr);

#if (IPDUM_DEV_ERROR_DETECT==STD_ON)
  if (IPDUM_INIT != IpduM_InitStatus)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_TRANSMIT, IPDUM_E_UNINIT);
  }
  else if (PdumTxPduId >= IpduM_ConfigPtr->TxPartLen)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_TRANSMIT, IPDUM_E_PARAM);
  }
  else if (NULL_PTR == PduInfoPtr)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_TRANSMIT, IPDUM_E_PARAM_POINTER);
  }
  else if (NULL_PTR == PduInfoPtr->SduDataPtr)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_TRANSMIT, IPDUM_E_PARAM_POINTER);
  }
#if (IPDUM_DYNAMIC_PART_QUEUE==STD_ON)
  else if (PduInfoPtr->SduLength > IPDUM_TX_SDU_SIZE)
  {
    /* if queuing is enabled, check that data fits into the queue */
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_TRANSMIT, IPDUM_E_PARAM);
  }
#endif
  else
#endif

  {
    RetValue = E_OK;

    /* Get TxPartT */
    TxPartT = &IpduM_ConfigPtr->TxPart[PdumTxPduId];

    /* Deviation MISRA-2 */
    TxData = (P2VAR(IpduM_TxDataType, IPDUM_VAR, IPDUM_VAR))(&IpduM_gDataMemPtr[IPDUM_TX_DATA_OFFSET]);

    TxPathWayT = &IpduM_ConfigPtr->TxPathWay[TxPartT->TxConfirmationPduId];

    /* Ignore if the Size of the Path way is zero */
    if (TxPathWayT->Size > 0U)
    {
      boolean TxFlag = FALSE;

      SchM_Enter_IpduM_SCHM_IPDUM_EXCLUSIVE_AREA_0();

      /* Check if a transmission is ongoing */
      if (0 == TxData[TxPartT->TxConfirmationPduId].ConfirmationTimeoutCounter)
      {
        /* No request pending */

#if (IPDUM_DYNAMIC_PART_QUEUE == STD_ON)
        /* Static or dynamic part */
#if (IPDUM_ZERO_COPY==STD_OFF)
        if (PdumTxPduId < IpduM_ConfigPtr->TxDynamicPartOffset)
        {
          /* Static */
          if ((TxPathWayT->TriggerMode & IPDUM_TRIGGER_STATIC) == IPDUM_TRIGGER_STATIC)
          {
            /* Trigger condition fulfilled, increase priority for pending dynamic PDUs */
            IpduM_SetDynamicPduReadyToSend(PdumTxPduId);

            /* Remove entry with highest priority */
            QueueEntry = IpduM_RemoveQueue(TxPartT->TxConfirmationPduId);

            /* Copy only if a dynamic part exists */
            if (NULL_PTR != QueueEntry)
            {
              /* Get pointer to static buffer */
              PduTxInfo.SduDataPtr = &IpduM_gDataMemPtr[TxPathWayT->BufferOffset];

              /* Assemble dynamic PDU */
              PduTxInfo.SduLength = IpduM_BitCopy(PduTxInfo.SduDataPtr,
              QueueEntry->SduData,
              IpduM_ConfigPtr->TxPart[QueueEntry->PdumTxPduId].CopyBitField,
              IpduM_ConfigPtr->TxPart[QueueEntry->PdumTxPduId].CopyBitFieldArraySize);

              /* Store PDU IDs for dynamic part (currently retrieved from queue) */
              PdumDynamicTxPduId = QueueEntry->PdumTxPduId;
              PduRTxConfirmationPduId = QueueEntry->PduRTxConfirmationPduId;
            }

            /* And mark for immediately sending */
            ImmediatelySend = TRUE;
          }
        }
        else
#endif /* (IPDUM_ZERO_COPY==STD_OFF) */
        {
          /* dynamic part (when IPDUM_ZERO_COPY==STD_ON the must not be a static part) */

          /* Set priority */
          DynPriority = TxPartT->DynamicPriority;

          /* Dynamic triggering condition fulfilled? */
          if ((TxPathWayT->TriggerMode & IPDUM_TRIGGER_DYNAMIC) != IPDUM_TRIGGER_DYNAMIC)
          {
            /* Trigger condition not fulfilled, mark as not to send */
            DynPriority |= IPDUM_PRIO_NOT_READY_TO_SEND;
          }

          /* Check if priority is higher than highest in queue and triggered */
          if ((DynPriority < IpduM_GetMaxPriorityQueue(PdumTxPduId)) &&
              (DynPriority < IPDUM_PRIO_NOT_READY_TO_SEND))
          {
            /* Priority is higher than in queue, send immediately */
            ImmediatelySend = TRUE;

            /* Store PDU IDs for dynamic part (currently passed via API) */
            PdumDynamicTxPduId = PdumTxPduId;
            PduRTxConfirmationPduId = TxPartT->PduRTxConfirmationPduId;
          }
          else
          {
            /* Priority is lesser than in queue, put into queue */
            RetValue = IpduM_InsertQueue(PdumTxPduId,
                    TxPartT->PduRTxConfirmationPduId,
                    PduInfoPtr,
                    DynPriority);
          }
        }
#else /* (IPDUM_DYNAMIC_PART_QUEUE == STD_ON) */
#if (IPDUM_ZERO_COPY==STD_OFF)
        /* dynamic part ? */
        if (PdumTxPduId >= IpduM_ConfigPtr->TxDynamicPartOffset)
#endif /* (IPDUM_ZERO_COPY==STD_OFF)*/
        /* dynamic part (when IPDUM_ZERO_COPY==STD_ON the must not be a static part) */
        {
          /* Store PDU IDs for dynamic part (currently passed via API) */
          PdumDynamicTxPduId = PdumTxPduId;
          PduRTxConfirmationPduId = TxPartT->PduRTxConfirmationPduId;
        }
#endif /* (IPDUM_DYNAMIC_PART_QUEUE == STD_ON) */

#if (IPDUM_ZERO_COPY==STD_OFF)
        /* Get pointer to static buffer */
        PduTxInfo.SduDataPtr = &IpduM_gDataMemPtr[TxPathWayT->BufferOffset];

        /* Assemble PDU */
        PduTxInfo.SduLength = IpduM_BitCopy(PduTxInfo.SduDataPtr,
        PduInfoPtr->SduDataPtr,
        TxPartT->CopyBitField,
        TxPartT->CopyBitFieldArraySize);
#endif /* (IPDUM_ZERO_COPY==STD_OFF) */

        /* In case we have updated the dynamic part, store the PDU ID for the dynamic PDU */
        if (PdumDynamicTxPduId != IPDUM_RESERVED_PDUID)
        {
          TxData[TxPartT->TxConfirmationPduId].PduRTxConfirmationPduId = PduRTxConfirmationPduId;
        }

#if (IPDUM_DYNAMIC_PART_QUEUE == STD_ON)
        if (TRUE == ImmediatelySend)
#else /* (IPDUM_DYNAMIC_PART_QUEUE == STD_ON) */
        /* Trigger condition fulfilled? */
        if ( ((PdumTxPduId < IpduM_ConfigPtr->TxDynamicPartOffset) &&
             ((TxPathWayT->TriggerMode & IPDUM_TRIGGER_STATIC) == IPDUM_TRIGGER_STATIC) ) ||
             ((PdumTxPduId >= IpduM_ConfigPtr->TxDynamicPartOffset) &&
             ((TxPathWayT->TriggerMode & IPDUM_TRIGGER_DYNAMIC) == IPDUM_TRIGGER_DYNAMIC) )
           )
#endif /* (IPDUM_DYNAMIC_PART_QUEUE == STD_ON) */
        {
          /* First store length */
          PduTxInfo.SduLength = IPDUM_GET_BYTE_SIZE(TxPathWayT->Size);

          /* set timeout counter to special value for ongoing calls to
           * PduR_IpduMTransmit() - will be set to correct timeout value after
           * PduR_IpduMTransmit() has returned */
          TxData[TxPartT->TxConfirmationPduId].ConfirmationTimeoutCounter = IPDUM_TRANSMIT_ONGOING;

          /* Set PDU Info */
#if (IPDUM_ZERO_COPY==STD_OFF)

          /* Automatically set selector value */
#if (IPDUM_AUTOMATIC_SELECTOR==STD_ON)
          /* do we have an updated dynamic part? */
          if (PdumDynamicTxPduId != IPDUM_RESERVED_PDUID)
          {
            CONSTP2CONST(IpduM_TxPartType, AUTOMATIC, IPDUM_APPL_CONST) TxDynamicPart = &IpduM_ConfigPtr->TxPart[PdumDynamicTxPduId];

            /* set the selector value based on the TxPart configuration (which is derived based on the PdumTxPduId */
            IPDUM_SET_SELECTOR(PduTxInfo.SduDataPtr,
              TxDynamicPart->SelectorValue,
              TxPathWayT->StartBit,
              TxPathWayT->EndBit);
          }
#endif /* (IPDUM_AUTOMATIC_SELECTOR==STD_ON) */
#else /* (IPDUM_ZERO_COPY==STD_OFF) */

          /* zero copy - just adjust the data pointer according to the start bit
           * (in the zero copy case only a single CopyBitField is allowed) */
          PduTxInfo.SduDataPtr = &PduInfoPtr->SduDataPtr[IPDUM_GET_BYTE_OFFSET(TxPartT->CopyBitField[0].StartBit)];
#endif /* (IPDUM_ZERO_COPY==STD_OFF) */

          TxFlag = TRUE;
        }
        else
        {
          /* return E_OK */
        }
      }

      else /* Transmission ongoing */
      {
#if (IPDUM_DYNAMIC_PART_QUEUE == STD_ON)
        if (PdumTxPduId < IpduM_ConfigPtr->TxDynamicPartOffset)
        {
          /* Static part, reject */
          RetValue = E_NOT_OK;
        }
        else
        {
          /* Set priority */
          DynPriority = TxPartT->DynamicPriority;

          /* Queue dynamic part */
          if ((TxPathWayT->TriggerMode & IPDUM_TRIGGER_DYNAMIC) != IPDUM_TRIGGER_DYNAMIC)
          {
            /* Trigger condition not fulfilled, mark as not to send */
            DynPriority |= IPDUM_PRIO_NOT_READY_TO_SEND;
          }

          /* !LINKSTO IPDUM.EB.IPDUM552_Conf, 1 */
          RetValue = IpduM_InsertQueue(PdumTxPduId,
                  TxPartT->PduRTxConfirmationPduId,
                  PduInfoPtr,
                  DynPriority);
        }
#else /* (IPDUM_DYNAMIC_PART_QUEUE == STD_ON) */
        RetValue = E_NOT_OK;
#endif /* (IPDUM_DYNAMIC_PART_QUEUE == STD_ON) */
      }

      SchM_Exit_IpduM_SCHM_IPDUM_EXCLUSIVE_AREA_0();

      /* Check whether the frame is to be transmitted */
      if (TxFlag == TRUE)
      {
        Std_ReturnType TransmitRetVal = E_NOT_OK;

        /* Transmit frame */
        TransmitRetVal = PduR_IpduMTransmit(TxPartT->TxOutgoingPduId, &PduTxInfo);

        SchM_Enter_IpduM_SCHM_IPDUM_EXCLUSIVE_AREA_0();

        if (E_NOT_OK == TransmitRetVal)
        {
           /* Reset confirmation timer so that a new frame can be transmitted */
           TxData[TxPartT->TxConfirmationPduId].ConfirmationTimeoutCounter = 0U;

          /* Transmit failed, return with error */
          RetValue = E_NOT_OK;
        }
        else
        {
          switch (TxData[TxPartT->TxConfirmationPduId].ConfirmationTimeoutCounter)
          {
            case IPDUM_TRANSMIT_ONGOING:
              /* Start timer for timeout monitoring */
              TxData[TxPartT->TxConfirmationPduId].ConfirmationTimeoutCounter = TxPathWayT->ConfirmationTimeout;
              break;

            case IPDUM_TRANSMIT_ONGOING_CONFIRMED:
              /* Do not start the time out counting when the transmit of the PDU
               * was confirmed before PduR_IpduMTransmit() has completely finished */
               TxData[TxPartT->TxConfirmationPduId].ConfirmationTimeoutCounter = 0U;
               break;

            /* all possible cases have been explicitly covered in 'case's above, the 'default'
             * cannot be reached */

            /* CHECK: NOPARSE */
            default:
              IPDUM_UNREACHABLE_CODE_ASSERT(IPDUM_SID_TRANSMIT);
              break;
            /* CHECK: PARSE */
          }
        }
        SchM_Exit_IpduM_SCHM_IPDUM_EXCLUSIVE_AREA_0() ;
      }
    }
    else
    {
      RetValue = E_OK;
    }
  }

  DBG_IPDUM_TRANSMIT_EXIT(RetValue,PdumTxPduId,PduInfoPtr);
  return RetValue;
}
/*--------------------------------------------------------------------------*/
#if (IPDUM_ZERO_COPY==STD_OFF)
/* Deviation MISRA-1 */
FUNC(Std_ReturnType , IPDUM_CODE) IpduM_TriggerTransmit
(
  PduIdType TxPduId,
  P2VAR(PduInfoType, AUTOMATIC, IPDUM_APPL_DATA) PduInfoPtr
)
{
  P2CONST(IpduM_TxPathWayType, AUTOMATIC, IPDUM_APPL_CONST) TxPathWay;
  P2VAR(uint8, AUTOMATIC, IPDUM_VAR_NOINIT) Buffer;
  Std_ReturnType RetValue = E_NOT_OK;

  DBG_IPDUM_TRIGGERTRANSMIT_ENTRY(TxPduId,PduInfoPtr);

#if (IPDUM_DEV_ERROR_DETECT==STD_ON)
  if (IPDUM_INIT != IpduM_InitStatus)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_TRIGGER_TRANSMIT, IPDUM_E_UNINIT);
  }
  else if (NULL_PTR==PduInfoPtr)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_TRIGGER_TRANSMIT, IPDUM_E_PARAM_POINTER);
  }
  else if (NULL_PTR==PduInfoPtr->SduDataPtr)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_TRIGGER_TRANSMIT, IPDUM_E_PARAM_POINTER);
  }
  else if (TxPduId >= (IpduM_ConfigPtr->TxPathWayLen))
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_TRIGGER_TRANSMIT, IPDUM_E_PARAM);
  }
  /* CHECK: NOPARSE */
  else if ((uint8)IPDUM_TRIGGER_INVALID ==
      IpduM_ConfigPtr->TxPathWay[TxPduId].TriggerMode)
  {
    /* unexpected error condition - should never be reached */
    IPDUM_UNREACHABLE_CODE_ASSERT(IPDUM_SID_TRIGGER_TRANSMIT);
  }
  /* CHECK: PARSE */
  else
#endif

  {
     /* Ignore if the Size of the Path way is zero */
    if (IpduM_ConfigPtr->TxPathWay[TxPduId].Size > 0U)
    {
      TxPathWay = &IpduM_ConfigPtr->TxPathWay[TxPduId];

      /* Get pointer to internal buffer */
      Buffer = &IpduM_gDataMemPtr[TxPathWay->BufferOffset];

      SchM_Enter_IpduM_SCHM_IPDUM_EXCLUSIVE_AREA_0();

      /* Copy data to output buffer */
      PduInfoPtr->SduLength = IPDUM_GET_BYTE_SIZE(TxPathWay->Size);
      IPDUM_COPY(PduInfoPtr->SduDataPtr, Buffer, PduInfoPtr->SduLength);

      SchM_Exit_IpduM_SCHM_IPDUM_EXCLUSIVE_AREA_0();
      RetValue = E_OK;
    }
  }

  DBG_IPDUM_TRIGGERTRANSMIT_EXIT(RetValue,TxPduId,PduInfoPtr);
  return RetValue;
}
#endif
/*--------------------------------------------------------------------------*/
/* Deviation MISRA-1 */
FUNC(void, IPDUM_CODE) IpduM_TxConfirmation (PduIdType TxPduId)
{
  P2CONST(IpduM_TxPathWayType, AUTOMATIC, IPDUM_APPL_CONST) TxPathWayC;
  P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT) TxData;
  PduIdType TxDynConfirmHandleId = IPDUM_RESERVED_PDUID;

  DBG_IPDUM_TXCONFIRMATION_ENTRY(TxPduId);

#if (IPDUM_DEV_ERROR_DETECT==STD_ON)
  if (IPDUM_INIT != IpduM_InitStatus)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_TX_CONFIRMATION, IPDUM_E_UNINIT);
  }
  else if (TxPduId >= IpduM_ConfigPtr->TxPathWayLen)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_TX_CONFIRMATION, IPDUM_E_PARAM);
  }
  /* CHECK: NOPARSE */
  else if ((uint8)IPDUM_TRIGGER_INVALID ==
      IpduM_ConfigPtr->TxPathWay[TxPduId].TriggerMode)
  {
    /* unexpected error condition - should never be reached */
    IPDUM_UNREACHABLE_CODE_ASSERT(IPDUM_SID_TRIGGER_TRANSMIT);
  }
  /* CHECK: PARSE */
  else
#endif

  {
    /* Ignore if the Size of the Path way is zero */
    if (IpduM_ConfigPtr->TxPathWay[TxPduId].Size > 0U)
    {
      boolean TxConfirmFlag = FALSE;

      /* Deviation MISRA-2 */
      TxData = (P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT))(&IpduM_gDataMemPtr[IPDUM_TX_DATA_OFFSET]);

      TxPathWayC = &IpduM_ConfigPtr->TxPathWay[TxPduId];

      SchM_Enter_IpduM_SCHM_IPDUM_EXCLUSIVE_AREA_0();

      /* Check if we are waiting for a confirmation */
      if ((TxData[TxPduId].ConfirmationTimeoutCounter > 0U) || (TxPathWayC->ConfirmationTimeout == 0U))
      {
        if(TxData[TxPduId].ConfirmationTimeoutCounter == IPDUM_TRANSMIT_ONGOING)
        {
          /* Signal not to start the time out counter, because the PDU is confirmed before the function
           * IpduM_Transmit() has finished */
          TxData[TxPduId].ConfirmationTimeoutCounter = IPDUM_TRANSMIT_ONGOING_CONFIRMED;
        }
        else
        {
          /* Stop timeout counter */
          TxData[TxPduId].ConfirmationTimeoutCounter = 0U;
        }

        /* In the case there is a dynamic part get the dynamic confirmation ID to call the PduR with */
        if((IPDUM_RESERVED_PDUID != TxData[TxPduId].PduRTxConfirmationPduId) &&
                (TxPathWayC->DynamicConfirmArraySize != 0U))
        {
          /* Store handle ID for TxConfirmation of dynamic part for later use */
          TxDynConfirmHandleId = TxData[TxPduId].PduRTxConfirmationPduId;

          /* Reset stored ID for confirmation of dynamic part */
          TxData[TxPduId].PduRTxConfirmationPduId = IPDUM_RESERVED_PDUID;
        }

        /* Call the confirmation outside the exclusive area */
        TxConfirmFlag = TRUE;
      }
      SchM_Exit_IpduM_SCHM_IPDUM_EXCLUSIVE_AREA_0();

      if (TxConfirmFlag == TRUE)
      {

#if (IPDUM_STATIC_PART_EXISTS==STD_ON)
        /* Call PduR_IpduM_TxConfirmation for the static part */
        if (IPDUM_RESERVED_PDUID != TxPathWayC->TxStaticConfirmHandleId)
        {
          PduR_IpduMTxConfirmation(TxPathWayC->TxStaticConfirmHandleId);
        }
#endif

        /* do PduR_IpduMTxConfirmation() if we have found a matching dynamic part */
        if (IPDUM_RESERVED_PDUID != TxDynConfirmHandleId)
        {
          /* Call PduR_IpduM_TxConfirmation for the dynamic part */
          PduR_IpduMTxConfirmation(TxDynConfirmHandleId);
        }
      }
    }
  }

  DBG_IPDUM_TXCONFIRMATION_EXIT(TxPduId);
}

/*--------------------------------------------------------------------------*/
/* Deviation MISRA-1 */
FUNC(void, IPDUM_CODE) IpduM_MainFunction (void)
{
  uint16 index;
  P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT) TxData;
#if (IPDUM_DYNAMIC_PART_QUEUE==STD_ON)
  P2CONST(IpduM_TxPathWayType, AUTOMATIC, IPDUM_APPL_CONST) TxPathWayMain;
  P2CONST(IpduM_TxPartType, AUTOMATIC, IPDUM_APPL_CONST) TxPartMain;
#if (IPDUM_ZERO_COPY==STD_OFF)
  P2VAR(uint8, AUTOMATIC, IPDUM_VAR_NOINIT) TxBuffer;

#endif
#endif

  DBG_IPDUM_MAINFUNCTION_ENTRY();

  if (IPDUM_INIT == IpduM_InitStatus)
  {
    for (index=0U; index<IPDUM_TX_PATHWAY_LEN; index++)
    {
      /* Check for valid PduIds */
      if (IpduM_ConfigPtr->TxPathWay[index].Size > 0U)
      {
#if (IPDUM_DYNAMIC_PART_QUEUE==STD_ON)
        PduInfoType PduInfo;
        boolean TxFlag = FALSE;
        PduIdType TxPduId = 0U;
#endif

        /* Deviation MISRA-2 */
        TxData = (P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT))(&IpduM_gDataMemPtr[IPDUM_TX_DATA_OFFSET]);

        SchM_Enter_IpduM_SCHM_IPDUM_EXCLUSIVE_AREA_0();

        /* Check for pending requests */
        if (TxData[index].ConfirmationTimeoutCounter>=IPDUM_TRANSMIT_ONGOING_CONFIRMED)
        {
          /* the main function just preempted a call to PduR_IpduMTransmit() =>
           * do nothing */
         }
        else if (TxData[index].ConfirmationTimeoutCounter>0U)
        {
          /* a call to PduR_IpduMTransmit() has completed and we're waiting
           * for the corresponding confirmation */

          /* decrease timer */
          TxData[index].ConfirmationTimeoutCounter--;
        }
#if (IPDUM_DYNAMIC_PART_QUEUE!=STD_ON)
        else
        {
        }
#else /* (IPDUM_DYNAMIC_PART_QUEUE!=STD_ON) */
        else
        {
          P2VAR(IpduM_QueueEntryType, AUTOMATIC, IPDUM_VAR_NOINIT) QueueEntry;

          /* Get entry with highest priority */
          QueueEntry = IpduM_RemoveQueue(index);

          if (NULL_PTR != QueueEntry)
          {
#if (IPDUM_ZERO_COPY==STD_ON)
            uint8 SduDataBuffer[IPDUM_TX_SDU_SIZE];
#endif
            TxPduId = QueueEntry->PdumTxPduId;

            /* Get pointer of TxPathWay */
            TxPathWayMain = &IpduM_ConfigPtr->TxPathWay[index];

            /* set timeout counter to special value for ongoing calls to
             * PduR_IpduMTransmit() - will be set to correct timeout value after
             * PduR_IpduMTransmit() has returned */
            TxData[index].ConfirmationTimeoutCounter = IPDUM_TRANSMIT_ONGOING;

            /* Set PDU Info */
            PduInfo.SduLength = IPDUM_GET_BYTE_SIZE(TxPathWayMain->Size);

            /* TxPathWay is the same, but TxPartMain may have been changed */
            TxPartMain = &IpduM_ConfigPtr->TxPart[TxPduId];

#if (IPDUM_ZERO_COPY==STD_OFF)
            /* Get pointer to static buffer */
            TxBuffer = &IpduM_gDataMemPtr[TxPathWayMain->BufferOffset];

            /* Assemble PDU */
            (void)IpduM_BitCopy(TxBuffer,
              QueueEntry->SduData,
              TxPartMain->CopyBitField,
              TxPartMain->CopyBitFieldArraySize);

            /* Set PDU Info */
            PduInfo.SduDataPtr = &TxBuffer[0];

            /* Automatically set selector value */
#if (IPDUM_AUTOMATIC_SELECTOR==STD_ON)
            IPDUM_SET_SELECTOR(PduInfo.SduDataPtr,
              TxPartMain->SelectorValue,
              TxPathWayMain->StartBit,
              TxPathWayMain->EndBit);
#endif
#else /* (IPDUM_ZERO_COPY==STD_OFF) */

            /* copy data from queue element to local buffer, since after we've
             * left the critical section another IpduM_Transmit() or the
             * IpduM_MainFunction() might preempt us and modify the queue
             * adjust the source data pointer according to the start bit
             * (in the zero copy case only a single CopyBitField is allowed) */
            IPDUM_COPY(SduDataBuffer, &QueueEntry->SduData[IPDUM_GET_BYTE_OFFSET(TxPartMain->CopyBitField[0].StartBit)], PduInfo.SduLength);

            /* Set PDU Info */
            PduInfo.SduDataPtr = SduDataBuffer;
#endif /* (IPDUM_ZERO_COPY==STD_OFF) */

            TxData[TxPartMain->TxConfirmationPduId].PduRTxConfirmationPduId =
                    QueueEntry->PduRTxConfirmationPduId;

            TxFlag = TRUE;
          }
        }
#endif /* (IPDUM_DYNAMIC_PART_QUEUE!=STD_ON) */

        SchM_Exit_IpduM_SCHM_IPDUM_EXCLUSIVE_AREA_0();

#if (IPDUM_DYNAMIC_PART_QUEUE==STD_ON)
        if (TxFlag == TRUE)
        {
          Std_ReturnType RetValue;

          /* Get pointer of TxPathWay */
          TxPathWayMain = &IpduM_ConfigPtr->TxPathWay[index];

          /* Get TxPartMain */
          TxPartMain = &IpduM_ConfigPtr->TxPart[TxPduId];

          /* Transmit frame */
          RetValue = PduR_IpduMTransmit(
            TxPartMain->TxOutgoingPduId,
            &PduInfo
            );

          if (E_NOT_OK == RetValue)
          {
            /* The transmit failed, reset confirmation timer so that other frames
             * can be transmitted */
             TS_AtomicAssign16(TxData[index].ConfirmationTimeoutCounter, 0);
          }
          else
          {
            /* Start timer for timeout surveillance */
            TS_AtomicAssign16(TxData[index].ConfirmationTimeoutCounter, TxPathWayMain->ConfirmationTimeout);
          }
        }
#endif /* (IPDUM_DYNAMIC_PART_QUEUE==STD_ON) */
      }
    }
  }

  DBG_IPDUM_MAINFUNCTION_EXIT();
}
/*--------------------------------------------------------------------------*/
#if (IPDUM_VERSION_INFO_API==STD_ON)
/* Deviation MISRA-1 */
FUNC(void, IPDUM_CODE) IpduM_GetVersionInfo
(
  CONSTP2VAR(Std_VersionInfoType, AUTOMATIC, IPDUM_APPL_DATA) versioninfo
)
{
  DBG_IPDUM_GETVERSIONINFO_ENTRY(versioninfo);
#if (IPDUM_DEV_ERROR_DETECT==STD_ON)

  if (NULL_PTR==versioninfo)
  {
    IPDUM_DET_REPORT_ERROR(IPDUM_SID_GET_VERSION_INFO, IPDUM_E_PARAM_POINTER);
  }
  else
#endif

  {
    versioninfo->vendorID         = IPDUM_VENDOR_ID;
    versioninfo->moduleID         = IPDUM_MODULE_ID;
    versioninfo->sw_major_version = IPDUM_SW_MAJOR_VERSION;
    versioninfo->sw_minor_version = IPDUM_SW_MINOR_VERSION;
    versioninfo->sw_patch_version = IPDUM_SW_PATCH_VERSION;
  }

  DBG_IPDUM_GETVERSIONINFO_EXIT(versioninfo);
}
#endif

/*--------------------------------------------------------------------------*/

#define IPDUM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function definitions]=========================*/

#define IPDUM_START_SEC_CODE
#include <MemMap.h>

#if (IPDUM_ZERO_COPY==STD_OFF)
IPDUM_STATIC FUNC(PduLengthType, IPDUM_CODE) IpduM_BitCopy
(
  P2VAR(uint8, AUTOMATIC, IPDUM_APPL_DATA) Dest,
  P2CONST(uint8, AUTOMATIC, IPDUM_APPL_DATA) Src,
  P2CONST(IpduM_CopyBitFieldType, AUTOMATIC, IPDUM_APPL_CONST) CopyBitField,
  IpduM_BitfieldArraySizeType Size
)
{
  IpduM_BitfieldArraySizeType i;
  uint8 SrcOffset;
  uint8 DstOffset = 0U;
  PduLengthType LastByte = 0U;

  DBG_IPDUM_BITCOPY_ENTRY(Dest,Src,CopyBitField,Size);

  for (i=0U; i<Size; i++)
  {
    /* !LINKSTO IPDUM.EB.IPDUM550_Conf, 1 */
#if (IPDUM_BYTE_COPY==STD_ON)
    uint8 Length = (uint8) IPDUM_GET_BYTE_SIZE(CopyBitField[i].EndBit-CopyBitField[i].StartBit);
    SrcOffset = (uint8) IPDUM_GET_BYTE_OFFSET(CopyBitField[i].StartBit);
    DstOffset = (uint8) IPDUM_GET_BYTE_OFFSET(CopyBitField[i].DestinationBit);

    /* Copy data bytewise */
    IPDUM_COPY(&Dest[DstOffset], &Src[SrcOffset], Length);

    if (((uint16) DstOffset + (uint16) Length) > LastByte)
    {
        LastByte = ((uint16) DstOffset + (uint16) Length);
    }
#else
    uint16 j;
    uint16 Length = CopyBitField[i].EndBit-CopyBitField[i].StartBit;

    /* Copy data bitwise */
    for (j=0U; j<=Length; j++)
    {
      /* Src and Dst offset in bytes */
      SrcOffset = (uint8)((uint16)(CopyBitField[i].StartBit+j)>>3U);
      DstOffset = (uint8)((uint16)(CopyBitField[i].DestinationBit+j)>>3U);

      /* First clear bit, because destination can be initialized with any value */
      Dest[DstOffset] &= (uint8)(~(uint8)(1U<<
                          (IPDUM_MODULO_8((uint16)(CopyBitField[i].DestinationBit+j)))));

      /* Set bit */
      Dest[DstOffset] |= (uint8)(((uint8)(Src[SrcOffset]>>
                          (IPDUM_MODULO_8((uint16)(CopyBitField[i].StartBit+j))))&1U) <<
                          (IPDUM_MODULO_8((uint16)(CopyBitField[i].DestinationBit+j))));

    }
    if ( DstOffset > LastByte)
    {
        LastByte = DstOffset;
    }
#endif
  }

  DBG_IPDUM_BITCOPY_EXIT(LastByte + 1U,Dest,Src,CopyBitField,Size);
  return LastByte + 1U;
}
#endif

/*--------------------------------------------------------------------------*/
#if (IPDUM_ZERO_COPY==STD_OFF)
IPDUM_STATIC FUNC(void, IPDUM_CODE) IpduM_PreparePdu
(
  P2CONST(IpduM_TxPathWayType, AUTOMATIC, IPDUM_APPL_CONST) TxPathWay
)
{
  uint8 i;
  uint8 Size;
  P2VAR(uint8, AUTOMATIC, IPDUM_VAR_NOINIT) Buffer;

  DBG_IPDUM_PREPAREPDU_ENTRY(TxPathWay);

  Buffer = &IpduM_gDataMemPtr[TxPathWay->BufferOffset];

  /* Convert from bits to bytes */
  Size = (uint8) IPDUM_GET_BYTE_SIZE(TxPathWay->Size);

  /* set default values */
  for (i=0U; i<Size; i++)
  {
    Buffer[i] = TxPathWay->UnusedAreasDefault;
  }

  DBG_IPDUM_PREPAREPDU_EXIT(TxPathWay);
}
#endif

#if (IPDUM_DYNAMIC_PART_QUEUE==STD_ON)
/*--------------------------------------------------------------------------*/
IPDUM_STATIC FUNC(Std_ReturnType, IPDUM_CODE) IpduM_InsertQueue
(
  PduIdType PdumTxPduId,
  PduIdType PduRTxConfirmationPduId,
  P2CONST(PduInfoType, AUTOMATIC, IPDUM_APPL_DATA) PduInfoPtr,
  uint16 DynamicPriority
)
{
  uint8 i;
  IpduM_QueueEntryType key;
  P2VAR(IpduM_QueueEntryType, AUTOMATIC, IPDUM_VAR_NOINIT) Queue;
  P2VAR(uint8, AUTOMATIC, IPDUM_VAR_NOINIT) QueuePosition;
  P2CONST(IpduM_TxPathWayType, AUTOMATIC, IPDUM_APPL_CONST) TxPathWayIQ;
  P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT) TxData;
  Std_ReturnType RetValue = E_NOT_OK;

  DBG_IPDUM_INSERTQUEUE_ENTRY(PdumTxPduId,PduRTxConfirmationPduId,PduInfoPtr,DynamicPriority);

  /* Get TxPathWay */
  TxPathWayIQ = &IpduM_ConfigPtr->TxPathWay[IpduM_ConfigPtr->TxPart[PdumTxPduId].TxConfirmationPduId];

  /* Insert entry if there is a queue, otherwise return E_NOT_OK to indicate that the PDU can not
   * be transmitted while a transmission is ongoing */
  if(TxPathWayIQ->QueueSize > 0U)
  {
    RetValue = E_OK;

    /* Get TxData */
    /* Deviation MISRA-2 */
    TxData = (P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT))(&IpduM_gDataMemPtr[IPDUM_TX_DATA_OFFSET]);

    /* Get start address of queue for this PDU */
    /* Deviation MISRA-2 */
    Queue = (P2VAR(IpduM_QueueEntryType, AUTOMATIC, IPDUM_VAR_NOINIT))(&IpduM_gDataMemPtr[TxPathWayIQ->QueueOffset]);

    /* Get Queue position */
    QueuePosition = &TxData[IpduM_ConfigPtr->TxPart[PdumTxPduId].TxConfirmationPduId].QueuePos;

    /* First check if PDU is already queued */
    for (i=0U; i<(*QueuePosition); i++)
    {
      if (Queue[i].PdumTxPduId == PdumTxPduId)
      {
        RetValue = E_NOT_OK;
      }
    }

    if (E_OK == RetValue)
    {
      if ((*QueuePosition) >= TxPathWayIQ->QueueSize)
      {
        /* Queue full, check if priority is higher than job with lowest priority */
        if ( DynamicPriority < Queue[0].DynamicPriority )
        {
          /* Priority is higher, overwrite lowest priority job */
          Queue[0].PdumTxPduId = PdumTxPduId;
          Queue[0].PduRTxConfirmationPduId = PduRTxConfirmationPduId;
          IPDUM_COPY(Queue[0].SduData, PduInfoPtr->SduDataPtr, PduInfoPtr->SduLength);
          Queue[0].DynamicPriority = DynamicPriority;

          /* Sort queue */
          i=0U;
          while(((i+1U)<TxPathWayIQ->QueueSize) && (Queue[i].DynamicPriority<Queue[i+1U].DynamicPriority))
          {
            /* Swap entries */
            key = Queue[i];
            Queue[i] = Queue[i+1U];
            Queue[i+1U] = key;
            i++;
          }
        }
        else
        {
          /* Priority is lower, job cannot be queued */
          RetValue = E_NOT_OK;
        }
      }
      else
      {
        /* Queue not full, insert entry */
        Queue[(*QueuePosition)].PdumTxPduId = PdumTxPduId;
        Queue[(*QueuePosition)].PduRTxConfirmationPduId = PduRTxConfirmationPduId;
        IPDUM_COPY(Queue[(*QueuePosition)].SduData, PduInfoPtr->SduDataPtr, PduInfoPtr->SduLength);
        Queue[(*QueuePosition)].DynamicPriority = DynamicPriority;

        /* Sort queue according to priority */
        i=(*QueuePosition);
        while((i>0U) && (Queue[i].DynamicPriority>=Queue[i-1U].DynamicPriority))
        {
          /* Swap entries */
          key = Queue[i];
          Queue[i] = Queue[i-1U];
          Queue[i-1U] = key;
          i--;
        }
        /* Set position to next free element */
        (*QueuePosition)++;
      }
    }
  }

  DBG_IPDUM_INSERTQUEUE_EXIT(RetValue,PdumTxPduId,PduRTxConfirmationPduId,PduInfoPtr,DynamicPriority);
  return RetValue;
}

/*--------------------------------------------------------------------------*/
IPDUM_STATIC FUNC(P2VAR(IpduM_QueueEntryType, AUTOMATIC, IPDUM_VAR_NOINIT), IPDUM_CODE) IpduM_RemoveQueue
(
    PduIdType TxConfirmationPduId
)
{
  P2VAR(IpduM_QueueEntryType, AUTOMATIC, IPDUM_VAR_NOINIT) Queue;
  P2VAR(IpduM_QueueEntryType, AUTOMATIC, IPDUM_VAR_NOINIT) NextEntry;
  P2VAR(uint8, AUTOMATIC, IPDUM_VAR_NOINIT) QueuePosition;
  P2CONST(IpduM_TxPathWayType, AUTOMATIC, IPDUM_APPL_CONST) TxPathWayRQ;
  P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT) TxData;

  DBG_IPDUM_REMOVEQUEUE_ENTRY(TxConfirmationPduId);

  /* Get TxPathWay */
  TxPathWayRQ = &IpduM_ConfigPtr->TxPathWay[TxConfirmationPduId];

  /* Get TxData */
  /* Deviation MISRA-2 */
  TxData = (P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT))(&IpduM_gDataMemPtr[IPDUM_TX_DATA_OFFSET]);

  /* Get start address of queue for this PDU */
  /* Deviation MISRA-2 */
  Queue = (P2VAR(IpduM_QueueEntryType, AUTOMATIC, IPDUM_VAR_NOINIT))(&IpduM_gDataMemPtr[TxPathWayRQ->QueueOffset]);

  /* Get start address of queue for this PDU */
  QueuePosition = &TxData[TxConfirmationPduId].QueuePos;

  if ( (*(QueuePosition) > 0U) &&
       ((Queue[(*QueuePosition)-1U].DynamicPriority&IPDUM_PRIO_NOT_READY_TO_SEND)!=IPDUM_PRIO_NOT_READY_TO_SEND)
     )
  {
    /* Return job with highest priority */
    NextEntry = &Queue[(*QueuePosition)-1U];

    /* Remove element */
    (*QueuePosition)--;
  }
  else
  {
    NextEntry = NULL_PTR;
  }

  DBG_IPDUM_REMOVEQUEUE_EXIT(NextEntry,TxConfirmationPduId);
  return NextEntry;
}

/*--------------------------------------------------------------------------*/
#if (IPDUM_ZERO_COPY==STD_OFF)
IPDUM_STATIC FUNC(void, IPDUM_CODE) IpduM_SetDynamicPduReadyToSend(PduIdType PdumTxPduId)
{
  uint8 i;
  P2VAR(IpduM_QueueEntryType, AUTOMATIC, IPDUM_VAR_NOINIT) Queue;
  P2VAR(uint8, AUTOMATIC, IPDUM_VAR_NOINIT) QueuePos;
  P2CONST(IpduM_TxPathWayType, AUTOMATIC, IPDUM_APPL_CONST) TxPathWay;
  P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT) TxData;

  DBG_IPDUM_SETDYNAMICPDUREADYTOSEND_ENTRY(PdumTxPduId);

  /* Get TxPathWay */
  TxPathWay = &IpduM_ConfigPtr->TxPathWay[IpduM_ConfigPtr->TxPart[PdumTxPduId].TxConfirmationPduId];

  /* Get TxData */
  /* Deviation MISRA-2 */
  TxData = (P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT))(&IpduM_gDataMemPtr[IPDUM_TX_DATA_OFFSET]);

  /* Get start address of queue for this PDU */
  /* Deviation MISRA-2 */
  Queue = (P2VAR(IpduM_QueueEntryType, AUTOMATIC, IPDUM_VAR_NOINIT))(&IpduM_gDataMemPtr[TxPathWay->QueueOffset]);

  /* Get Queue position */
  QueuePos = &TxData[IpduM_ConfigPtr->TxPart[PdumTxPduId].TxConfirmationPduId].QueuePos;

  /* Set PDU to ready for send */
  for(i=0; i<(*QueuePos);i++)
  {
    Queue[i].DynamicPriority &= (uint16)(~IPDUM_PRIO_NOT_READY_TO_SEND);
  }

  DBG_IPDUM_SETDYNAMICPDUREADYTOSEND_EXIT(PdumTxPduId);
}
#endif
/*--------------------------------------------------------------------------*/
IPDUM_STATIC FUNC(uint16, IPDUM_CODE) IpduM_GetMaxPriorityQueue(PduIdType PdumTxPduId)
{
  uint8 QueuePosition;
  P2VAR(IpduM_QueueEntryType, AUTOMATIC, IPDUM_VAR_NOINIT) Queue;
  P2CONST(IpduM_TxPathWayType, AUTOMATIC, IPDUM_APPL_CONST) TxPathWayGM;
  P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT) TxData;
  uint16 Priority=IPDUM_PRIO_LOWEST_POSSIBLE;

  DBG_IPDUM_GETMAXPRIORITYQUEUE_ENTRY(PdumTxPduId);

  /* Get TxPathWay */
  TxPathWayGM = &IpduM_ConfigPtr->TxPathWay[IpduM_ConfigPtr->TxPart[PdumTxPduId].TxConfirmationPduId];

  /* Get TxData */
  /* Deviation MISRA-2 */
  TxData = (P2VAR(IpduM_TxDataType, AUTOMATIC, IPDUM_VAR_NOINIT))(&IpduM_gDataMemPtr[IPDUM_TX_DATA_OFFSET]);

  /* Get start address of queue for this PDU */
  /* Deviation MISRA-2 */
  Queue = (P2VAR(IpduM_QueueEntryType, AUTOMATIC, IPDUM_VAR_NOINIT))(&IpduM_gDataMemPtr[TxPathWayGM->QueueOffset]);

  /* Get Queue position */
  QueuePosition = TxData[IpduM_ConfigPtr->TxPart[PdumTxPduId].TxConfirmationPduId].QueuePos;

  if (QueuePosition > 0U)
  {
      Priority = Queue[QueuePosition - 1U].DynamicPriority;
  }

  DBG_IPDUM_GETMAXPRIORITYQUEUE_EXIT(Priority,PdumTxPduId);
  return Priority;
}

#endif

#define IPDUM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[end of file]===========================================*/
