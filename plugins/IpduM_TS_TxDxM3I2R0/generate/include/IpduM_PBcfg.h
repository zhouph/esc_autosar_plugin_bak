/**
 * \file
 *
 * \brief AUTOSAR IpduM
 *
 * This file contains the implementation of the AUTOSAR
 * module IpduM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined IPDUM_PBCFG_H)
#define IPDUM_PBCFG_H

/* This file contains all target independent public configuration declarations
 * for the AUTOSAR module IpduM. */

[!AUTOSPACING!]
[!//
[!VAR "Spaces31" = "'                               '"!]
[!//

/*==================[inclusions]============================================*/
#include <IpduM_Cfg.h>
#include <IpduM_Types.h>
#include <IpduM_SymbolicNames_PBcfg.h>

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/
#define IPDUM_START_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

/** \brief IpduM configuration
 **
 ** This is the IpduM configuration that can be given to
 ** IpduM_Init as configuration parameter.
 ** Please note that the name of this element is configuration dependent.
 */
[!SELECT "IpduMConfig/*[1]"!]
extern CONST(IpduM_ConfigType, IPDUM_APPL_CONST) [!"name(.)"!];
[!ENDSELECT!]

#define IPDUM_STOP_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( IPDUM_PBCFG_H ) */
/*==================[end of file]===========================================*/
