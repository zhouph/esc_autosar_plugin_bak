[!/**
 * \file
 *
 * \brief AUTOSAR Fee
 *
 * This file contains the implementation of the AUTOSAR
 * module Fee.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//
[!IF "not(var:defined('FEE_CHECKS.M'))"!][!//
[!VAR "FEE_CHECKS.M" = "'true'"!][!//
[!//
[!VAR "VirPageSize" = "FeeGeneral/FeeVirtualPageSize"!][!//
[!VAR "Headerfieldsize" = "num:integer(2 + ($VirPageSize - (2 mod $VirPageSize)) mod $VirPageSize)"!][!//
[!VAR "Headersize" = "num:integer(3 * $Headerfieldsize)"!][!//
[!VAR "AlignedBlockInfoSize" = "num:integer(6 + ($VirPageSize - (6 mod $VirPageSize)) mod $VirPageSize)"!][!//
[!VAR "TotalFlashSize" = "0"!][!//
[!LOOP "as:modconf('Fls')[1]/FlsConfigSet/*[1]/FlsSectorList/FlsSector/*"!][!//
  [!VAR "TotalFlashSize" = "$TotalFlashSize + (FlsNumberOfSectors * FlsSectorSize)"!][!//
[!ENDLOOP!][!//
[!//
[!/***********************Calculation of Space required for switch operation ********************/!][!//
[!VAR "SwitchSectionSpace" = "0"!][!//
[!VAR "TotalBlocksSize" = "0"!][!//
[!VAR "LargestBlockSize" = "0"!][!//
[!LOOP "FeeBlockConfiguration/*"!][!//
  [!VAR "TotalBlocksSize" = "$TotalBlocksSize + (FeeBlockSize + 2 + ($VirPageSize - ((FeeBlockSize + 2) mod $VirPageSize)) mod $VirPageSize)"!][!//
  [!IF "FeeBlockSize > $LargestBlockSize"!][!//
    [!VAR "LargestBlockSize" = "FeeBlockSize"!][!//
  [!ENDIF!][!//
[!ENDLOOP!][!//
[!VAR "TotalBlocksSize" = "$TotalBlocksSize + (count(FeeBlockConfiguration/*) * $AlignedBlockInfoSize)"!][!//
[!VAR "LargestBlockSize" = "($LargestBlockSize + 2 + ($VirPageSize - (($LargestBlockSize + 2) mod $VirPageSize)) mod $VirPageSize)"!][!//
[!VAR "SwitchSectionSpace" = "$TotalBlocksSize + $LargestBlockSize + $AlignedBlockInfoSize"!][!//
[!//
[!//
[!/****** Check to ensure that memory available is sufficient for all blocks and their copies *******/!][!//
[!VAR "RequiredFlashSize" = "0"!][!//
[!LOOP "FeeBlockConfiguration/*"!][!//
  [!VAR "AlignedBlockDataSize" = "num:integer(FeeBlockSize + 2 + ($VirPageSize - ((FeeBlockSize + 2) mod $VirPageSize)) mod $VirPageSize)"!][!//
  [!VAR "RequiredFlashSize" = "$RequiredFlashSize + ($AlignedBlockDataSize + $AlignedBlockInfoSize) * FeeNumberOfWriteCycles"!][!//
[!ENDLOOP!][!//
[!//
[!SELECT "as:modconf('Fls')[1]/FlsPublishedInformation"!][!//
  [!VAR "RequiredFlashSize" = "$RequiredFlashSize div FlsSpecifiedEraseCycles"!][!//
[!ENDSELECT!][!//
[!//
[!VAR "RequiredFlashSize" = "($RequiredFlashSize + (2 * ($SwitchSectionSpace + $Headersize)))"!][!//
[!//
[!/* This check is done in code template because if it is moved to .xdm, all the calculations and checks need to be executed after
each block is configured. Since this is difficult and makes the configuration tool slow, these checks are done in code template */!][!//
[!IF "$RequiredFlashSize > $TotalFlashSize"!][!//
  [!ERROR "Configured flash space is not enough for the blocks and its copies"!][!//
[!ENDIF!][!//
[!//
[!//
[!/********************** Calculation of section 0 and section 1 size ***************************/!][!//
[!VAR "Section0size" = "0"!][!//
[!VAR "Flag" = "0"!][!//
[!VAR "TotalSectors" = "sum(as:modconf('Fls')[1]/FlsConfigSet/*[1]/FlsSectorList/FlsSector/*/FlsNumberOfSectors)"!][!//
[!VAR "SectorIndex" = "0"!][!//
[!LOOP "as:modconf('Fls')[1]/FlsConfigSet/*[1]/FlsSectorList/FlsSector/*"!][!//
  [!FOR "i" = "1" TO "FlsNumberOfSectors"!][!//
    [!VAR "SectorIndex" = "$SectorIndex + 1"!][!//
    [!IF "($SectorIndex = $TotalSectors) or ($Section0size >= ($TotalFlashSize div 2))"!][!//
      [!VAR "Flag" = "1"!][!//
      [!BREAK!][!//
    [!ENDIF!][!//
    [!VAR "Section0size" = "$Section0size + FlsSectorSize"!][!//
  [!ENDFOR!][!//
  [!IF "$Flag = 1"!][!//
    [!BREAK!][!//
  [!ENDIF!][!//
[!ENDLOOP!] [!//
[!VAR "Section1size" = "$TotalFlashSize - $Section0size"!][!//
[!//
[!//
[!/********************** Checks for section 0 and section 1 size *******************************/!][!//
[!/* This checks are done in code template because if it is moved to .xdm, all the calculations and checks need to be executed after
each block is configured. Since this is difficult and makes the configuration tool slow, these checks are done in code template */!][!//
[!//
[!//
[!IF "$Section0size < ($SwitchSectionSpace + $Headersize)"!][!//
  [!ERROR "Section 0 is less than Space required for switch operation"!][!//
[!ENDIF!][!//
[!IF "$Section1size < ($SwitchSectionSpace + $Headersize)"!][!//
  [!ERROR "Section 1 is less than Space required for switch operation"!][!//
[!ENDIF!][!//
[!IF "$Section0size < ($SwitchSectionSpace + $TotalBlocksSize + $Headersize)"!][!//
  [!WARNING "For better performance of the module during shutdown, Section 0 shall be configured with more space"!][!//
[!ENDIF!][!//
[!IF "$Section1size < ($SwitchSectionSpace + $TotalBlocksSize + $Headersize)"!][!//
  [!WARNING "For better performance of the module during shutdown, Section 1 shall be configured with more space"!][!//
[!ENDIF!][!//
[!//
[!ENDIF!]
