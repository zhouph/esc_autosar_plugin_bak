/**
 * \file
 *
 * \brief AUTOSAR Det
 *
 * This file contains the implementation of the AUTOSAR
 * module Det.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if !defined(DET_INT_H)
#define DET_INT_H

/*==================[inclusions]============================================*/

#include <Det_Int_Cfg.h>            /* Det generated internal configuration */

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#if (DET_LOGMODE == DET_LOGMODE_INTERNAL)

/** \brief Proxy function for SchM_Enter_Det_SCHM_DET_EXCLUSIVE_AREA_0
 **
 ** This function was introduced to avoid RTE includes
 ** within compilation unit Det.c
 **
 ** \return No return value. */
FUNC(void, COMM_CODE) Det_Enter_SCHM_DET_EXCLUSIVE_AREA_0 (void);

/** \brief Proxy function for SchM_Exit_Det_SCHM_DET_EXCLUSIVE_AREA_0
 **
 ** This function was introduced to avoid RTE includes
 ** within compilation unit Det.c
 **
 ** \return No return value. */
FUNC(void, COMM_CODE) Det_Exit_SCHM_DET_EXCLUSIVE_AREA_0 (void);

#endif

/*==================[end of file]===========================================*/
#endif /* if !defined(DET_INT_H) */
