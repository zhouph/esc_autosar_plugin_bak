/**
 * \file
 *
 * \brief AUTOSAR FrTp
 *
 * This file contains the implementation of the AUTOSAR
 * module FrTp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 12.5 (required)
 * The operands of a logical '&&' or '||' shall be 'primary-expressions'.
 * TestSequences
 *
 * Reason:
 * Omitted additional parenthesis to improve readability.
 *
 *
 * MISRA-2) Deviated Rule: 15.2 (required)
 * An unconditional 'break' statement shall terminate every non-empty switch clause.
 *
 * Reason:
 * Omitted 'break' statement to simplify code.
 *
 */
/******************************************************************************
 **                      Include Section                                     **
 *****************************************************************************/

#include <FrTp_Trace.h>
#include <FrIf.h>       /* FrIf interface file */
#include <PduR_FrTp.h>  /* get PduR interface for FrTp */
#include <FrTp_Priv.h>  /* data types */
#include <TSMem.h>      /* Memory copy operations */

/******************************************************************************
 **                      Local Macros                                        **
 *****************************************************************************/


/******************************************************************************
**                       Local Function prototypes                           **
******************************************************************************/

/*
 * Start code section declaration
 */
#define FRTP_START_SEC_CODE
#include <MemMap.h>

STATIC FUNC(void,FRTP_CODE) FrTp_RxSm_RxData_HandleStartFrame
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    VAR(uint8,AUTOMATIC) SN,
    P2CONST(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) pPduInfo
);

STATIC FUNC(void,FRTP_CODE) FrTp_RxSm_RxData_HandleSFBufOk
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    VAR(uint8,AUTOMATIC) SN,
    P2CONST(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) pPduInfo
);

STATIC FUNC(void,FRTP_CODE) FrTp_RxSm_RxData_HandleSFBufBusy
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    VAR(uint8,AUTOMATIC) SN,
    P2CONST(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) pPduInfo
);

STATIC FUNC(void,FRTP_CODE) FrTp_RxSm_RxData_HandleSFBufNOk
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    VAR(uint8,AUTOMATIC) SN,
    P2CONST(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) pPduInfo,
    VAR(BufReq_ReturnType,AUTOMATIC) RetCode,
    VAR(boolean,AUTOMATIC)STFRetransmitFlag
);

STATIC FUNC(void,FRTP_CODE) FrTp_RxSm_RxData_HandleConFrame
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    VAR(uint8,AUTOMATIC) PCI,
    VAR(uint8,AUTOMATIC) SN,
    P2CONST(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) pPduInfo
);

STATIC FUNC(void,FRTP_CODE) FrTp_RxSm_RxData_HandleLastFrame
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    P2CONST(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) pPduInfo
);

/** \brief Indicate that reception can be continued
 *
 * Send CTS/WAIT or ACK (acknowledged) or just finish silently (unacknowledged)
 * */
STATIC FUNC(void,FRTP_CODE) FrTp_SendFc_Continue
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    uint8 SN,
    uint8 fpl,
    uint16 ml
);


/** \brief Indicate that PduR returned overflow
 *
 * Send OVFL or abort silently (unsegmented, unacknowledged)
 * */
STATIC FUNC(void,FRTP_CODE) FrTp_SendFc_Ovflw
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    uint8 SN,
    uint8 fpl,
    uint16 ml
);


/** \brief Indicate that an error occurred which cannot be recovered
 *
 * Send ABORT or abort silently (unsegmented, unacknowledged)
 * */
STATIC FUNC(void,FRTP_CODE) FrTp_SendFc_Abort
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    uint8 SN,
    uint8 fpl,
    uint16 ml,
    boolean IndicatePduR
);


#if (FRTP_COPY_STF_LOCALBUFFER == STD_ON)
/** \brief Copy data in STF to the local buffer */
STATIC FUNC(void,FRTP_CODE) FrTp_CopyToLocalBuffer_STF
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    uint8 fpl,
    CONSTP2CONST(uint8,AUTOMATIC,FRTP_APPL_DATA) pData
);

STATIC FUNC(BufReq_ReturnType,FRTP_CODE) FrTp_RxSm_PduRCopyData
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel
);
#else

/** \brief Indicate that data reception was unsuccessful and request retry if supported
 *
 * Send RETRY (acknowledged) or abort with OVFLW or silently (unacknowledged)
 * */
STATIC FUNC(void,FRTP_CODE) FrTp_SendFc_Retry
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    uint8 SN,
    uint8 fpl,
    uint16 ml
);
#endif

/*
 * Stop code section declaration
 */
#define FRTP_STOP_SEC_CODE
#include <MemMap.h>

/*
 * Start code section declaration
 */
#define FRTP_START_SEC_CODE
#include <MemMap.h>

/******************************************************************************
**                      Function Definitions                                 **
******************************************************************************/

FUNC(boolean,FRTP_CODE) FrTp_RxSm_TriggerTransmit
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    P2VAR(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) pPduInfo
)
{
    /* local variable for atomic access */
    VAR(uint8,AUTOMATIC) RxState;
    /* flag indicating transmission pending */
    VAR(boolean,AUTOMATIC) IsTxPending;
    boolean ReturnVal = FALSE;

    /* get Rx-statemachine full state into local variable */
    VAR(uint8,AUTOMATIC) FullState;

    DBG_FRTP_RXSM_TRIGGERTRANSMIT_ENTRY(pChannel,pPduInfo);
    TS_AtomicAssign8(FullState, pChannel->Rx_State);

    /* get Rx-statemachine state into local variable */
    RxState = FrTp_Chnl_GetRxState(FullState);

    /* Has the Rx statemachine FC transmission pending ? */
    IsTxPending = FrTp_Chnl_IsTxPending(FullState);

    /* only if connection is allocated transmission is pending */
    if((pChannel->ConnectionIdx != FRTP_CONNECTION_INVALID) && (IsTxPending != FALSE))
    {
        /* get pointer to associated connection object */
        CONSTP2CONST(FrTp_ConnectionType,AUTOMATIC,FRTP_APPL_CONST) pConnection =
                FRTP_CFG_GET_PTR_TO_CONNECTION(pChannel->ConnectionIdx);
        ReturnVal = TRUE;

        /* set address information into tx-buffer */
        FrTp_SetAI(pPduInfo->SduDataPtr,pConnection->RemoteAddress, pConnection->LocalAddress);

        switch(RxState)
        {
            case FRTP_CHNL_RX_ACK:
            {
                FrTp_SetPCI_ACK(pPduInfo->SduDataPtr);
                pPduInfo->SduLength = FrTp_Size_Header_FC_ACK_RET;
                break;
            }

            case FRTP_CHNL_RX_RETRY:
            {
                FrTp_SetPCI_RET(pPduInfo->SduDataPtr,pChannel->Rx_CtrBlockReceived);
                pPduInfo->SduLength = FrTp_Size_Header_FC_ACK_RET;
                break;
            }
            case FRTP_CHNL_RX_CTS:
            {
                FrTp_SetPCI_CTS(pPduInfo->SduDataPtr,pConnection->BC,pChannel->Rx_BfS);
                pPduInfo->SduLength = FrTp_Size_Header_FC_CTS;
                break;
            }
            case FRTP_CHNL_RX_OVFL:
            {
                FrTp_SetPCI_OVFLW(pPduInfo->SduDataPtr);
                pPduInfo->SduLength = FrTp_Size_Header_FC;
                break;
            }
            case FRTP_CHNL_RX_ABORT:
            {
                FrTp_SetPCI_ABT(pPduInfo->SduDataPtr);
                pPduInfo->SduLength = FrTp_Size_Header_FC;
                break;
            }
            case FRTP_CHNL_RX_WAIT:
            {
                FrTp_SetPCI_WAIT(pPduInfo->SduDataPtr);
                pPduInfo->SduLength = FrTp_Size_Header_FC;
                break;
            }
            /* CHECK: NOPARSE */
            case FRTP_CHNL_RX_DATA: /* fall through */
            case FRTP_CHNL_RX_IDLE: /* fall through */
            default:
            {
                /* The module will never go through these states in this context, since the channel
                   associated with TxPdu will be a NULL_PTR, this function will not be called. */
                FRTP_UNREACHABLE_CODE_ASSERT(FRTP_TRIGGERTRANSMIT_SERVICE_ID);
                break;
            }
            /* CHECK: PARSE */
        }
    }
    /* CHECK: NOPARSE */
    else
    { 
      /* This function will not be called if a PDU is already confirmed or not sent, so this
         condition will always be true. */
      FRTP_UNREACHABLE_CODE_ASSERT(FRTP_TRIGGERTRANSMIT_SERVICE_ID);
    }
    /* CHECK: PARSE */

    DBG_FRTP_RXSM_TRIGGERTRANSMIT_EXIT(ReturnVal,pChannel,pPduInfo);
    return ReturnVal;
}

FUNC(void,FRTP_CODE) FrTp_RxSm_TransmitConfirmation
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    VAR(uint8,AUTOMATIC) TxPduIdx
)
{
    /* local variable for atomic access */
    VAR(uint8,AUTOMATIC) RxState;
    /* flag indicating transmission pending */
    VAR(boolean,AUTOMATIC) IsTxPending;
    /* get Rx-statemachine full state into local variable */
    VAR(uint8,AUTOMATIC) FullState;

    DBG_FRTP_RXSM_TRANSMITCONFIRMATION_ENTRY(pChannel,TxPduIdx);
    TS_AtomicAssign8(FullState, pChannel->Rx_State);

    /* get Rx-statemachine state into local variable */
    RxState = FrTp_Chnl_GetRxSubState(FullState);

    /* Has the Rx statemachine FC transmission pending ? */
    IsTxPending = FrTp_Chnl_IsTxPending(FullState);

    /* only if connection is allocated transmission is pending */
    if((pChannel->ConnectionIdx != FRTP_CONNECTION_INVALID) && (IsTxPending != FALSE) )
    {
        /* get pointer to associated connection object */
        CONSTP2CONST(FrTp_ConnectionType,AUTOMATIC,FRTP_APPL_CONST) pConnection =
                FRTP_CFG_GET_PTR_TO_CONNECTION(pChannel->ConnectionIdx);

        FrTp_StopTimer(pChannel->Rx_Timer1);

        switch(RxState)
        {
            case FRTP_CHNL_RX_ACK:
            {
                /* received complete frame */
                PduR_FrTpRxIndication(pConnection->RxSduIdx,NTFRSLT_OK);

                /* free channel for new receptions */
                FrTp_ResetRxChannel(pChannel);

                break;
            }
            case FRTP_CHNL_RX_CTS: /* fall through */
            case FRTP_CHNL_RX_RETRY:
            {
                /* start timer CR */
                FrTp_StartTimer(pChannel->Rx_Timer2,pConnection->TimeroutCR);

                /* process further received data */
                TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_DATA);

                break;
            }
            case FRTP_CHNL_RX_WAIT:
            {
                TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_CTS);

                break;
            }
            case FRTP_CHNL_RX_ABORT_WRONG_SN:
            {
                /* received complete frame */
                PduR_FrTpRxIndication(pConnection->RxSduIdx,NTFRSLT_E_WRONG_SN);

                /* free channel for new receptions */
                FrTp_ResetRxChannel(pChannel);

                break;
            }
            case FRTP_CHNL_RX_ABORT_ML_MISMATCH:
            {
                /* received complete frame */
                PduR_FrTpRxIndication(pConnection->RxSduIdx,NTFRSLT_E_FR_ML_MISMATCH);

                /* free channel for new receptions */
                FrTp_ResetRxChannel(pChannel);

                break;
            }
            case FRTP_CHNL_RX_ABORT_NO_BUFFER:
            {
                /* received complete frame */
                PduR_FrTpRxIndication(pConnection->RxSduIdx,NTFRSLT_E_NOT_OK);

                /* free channel for new receptions */
                FrTp_ResetRxChannel(pChannel);

                break;
            }
            case FRTP_CHNL_RX_ABORT: /* fall through */
            case FRTP_CHNL_RX_OVFL: /* fall through */
            {
                /* free channel for new receptions */
                FrTp_ResetRxChannel(pChannel);

                break;
            }
            /* CHECK: NOPARSE */
            case FRTP_CHNL_RX_DATA: /* fall through */
            case FRTP_CHNL_RX_IDLE: /* fall through */
                /* Module will not go through these states in this context, so this function will not
                   be called in these states. */
            default:
                FRTP_UNREACHABLE_CODE_ASSERT(FRTP_TXCONFIRMATION_SERVICE_ID);
            break;
            /* CHECK: PARSE */
        }
    }
    /* CHECK: NOPARSE */
    else
    {
        /* This function will not be called for a PDU already confirmed or not sent. */
        FRTP_UNREACHABLE_CODE_ASSERT(FRTP_TXCONFIRMATION_SERVICE_ID);
    }
    /* CHECK: PARSE */

    /* perform Tx-Pdu deallocation */
    FrTp_RuntimeTxPdu[TxPduIdx].pChannel = NULL_PTR;

    DBG_FRTP_RXSM_TRANSMITCONFIRMATION_EXIT(pChannel,TxPduIdx);
}



/** \brief Performs FrIf_Transmit() call for transmission of a FC frame
 *
 * Send RETRY (acknowledged) or abort with OVFLW or silently (unacknowledged)
 * */
STATIC FUNC(void,FRTP_CODE) FrTp_RxSm_FC_Transmit
(    
  P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
  CONSTP2CONST(FrTp_ConnectionType,AUTOMATIC,FRTP_APPL_CONST) pConnection,
  P2VAR(PduInfoType,AUTOMATIC,AUTOMATIC) pPduInfo
);

STATIC FUNC(void,FRTP_CODE) FrTp_RxSm_FC_Transmit
(    
  P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
  CONSTP2CONST(FrTp_ConnectionType,AUTOMATIC,FRTP_APPL_CONST) pConnection,
  P2VAR(PduInfoType,AUTOMATIC,AUTOMATIC) pPduInfo
)
{
    const uint8 TxPduIdx = FrTp_GetFreeTxPdu(pChannel,pPduInfo->SduLength);
        
    if(TxPduIdx != FRTP_TXPDU_INVALID)
    {
        /* get TxPdu information */
        CONSTP2CONST(FrTp_TxPduConfigType,AUTOMATIC,FRTP_APPL_CONST) pTxPdu =
                FRTP_CFG_GET_PTR_TO_TXPDU(TxPduIdx);

        const Std_ReturnType retCode = FrIf_Transmit(pTxPdu->PduId, pPduInfo);

        if(retCode == E_OK)
        {

            /* perform Tx-Pdu allocation */
            FrTp_RuntimeTxPdu[TxPduIdx].pChannel = pChannel;

            /* this PDU is used for data transmission */
            FrTp_RuntimeTxPdu[TxPduIdx].PduType = FRTP_TXPDUTYPE_FC;

            /* start timer AR */
            FrTp_StartTimer(pChannel->Rx_Timer1,pConnection->TimeroutAR);

            /* enter exclusive area */
            SchM_Enter_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

            /* mark a transmission as pending */
            pChannel->Rx_State |= FRTP_CHNL_RX_WAITTX_MASK;

            /* exit exclusive area */
            SchM_Exit_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

            /* reset FrIf retry counter */
            pChannel->TxFc_FrIfRetry = 0U;
        }
        else
        {
            pChannel->TxFc_FrIfRetry++;
            if(pChannel->TxFc_FrIfRetry == pConnection->MaxFrIfRetry)
            {
              /* reception is already finished on for this connection */
              PduR_FrTpRxIndication(pConnection->RxSduIdx, NTFRSLT_E_NOT_OK);
              /* free channel */
              FrTp_ResetRxChannel(pChannel);
            }
        }
    }
}

FUNC(void,FRTP_CODE) FrTp_RxSm_TransmitSyncPoint
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel
)
{
    /* local variable for atomic access */
    VAR(uint8,AUTOMATIC) RxState;
    /* flag indicating transmission pending */
    VAR(boolean,AUTOMATIC) IsTxPending;
    boolean RxSyncFlag = TRUE;

    /* get Rx-statemachine full state into local variable */
    VAR(uint8,AUTOMATIC) FullState;

    DBG_FRTP_RXSM_TRANSMITSYNCPOINT_ENTRY(pChannel);
    TS_AtomicAssign8(FullState, pChannel->Rx_State);

    /* get Rx-statemachine state into local variable */
    RxState = FrTp_Chnl_GetRxState(FullState);

    /* Has the Rx statemachine FC transmission pending ? */
    IsTxPending = FrTp_Chnl_IsTxPending(FullState);

    /* only if connection is allocated and no FC transmission is in progress */
    /* Deviation MISRA-1 <START> */
    if((pChannel->ConnectionIdx != FRTP_CONNECTION_INVALID) &&
       (RxState != FRTP_CHNL_RX_IDLE) &&
       (IsTxPending == FALSE)
      )
    /* Deviation MISRA-1 <STOP> */
    {

        /* get pointer to associated connection object */
        CONSTP2CONST(FrTp_ConnectionType,AUTOMATIC,FRTP_APPL_CONST) pConnection =
                FRTP_CFG_GET_PTR_TO_CONNECTION(pChannel->ConnectionIdx);

        PduInfoType PduInfo;
        PduInfo.SduLength = 0U;
        PduInfo.SduDataPtr = NULL_PTR;

        switch(RxState)
        {
            case FRTP_CHNL_RX_STF_RETRY:
            {
              /* Start TimeBR and wait to send the FC RETRY. */
              FrTp_StartTimer(pChannel->Rx_Timer3,pConnection->TimeBR);
              TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_RETRY);
              RxSyncFlag = FALSE;
              break;
            }
            case FRTP_CHNL_RX_RETRY:
            {
                /* invert CF type */
                pChannel->Rx_PCI ^= FRTP_PCITYPE_CFI_MASK;

                /* reset to 15, since it will be incremented before reception */
                pChannel->Rx_PCI |= FRTP_PCITYPE_SN_MASK;

                if((pChannel->Rx_CtrDataAck == 0U) && (pChannel->Rx_Timer3 != 0U))
                {
                    RxSyncFlag = FALSE;
                }
                else if(pChannel->Rx_CtrRetry < pConnection->MaxRetry)
                {
                    pChannel->Rx_CtrRetry++;
                    PduInfo.SduLength = FrTp_Size_Header_FC_ACK_RET;
                }
                else
                {
                    TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_ABORT_WRONG_SN);
                    PduInfo.SduLength = FrTp_Size_Header_FC;
                }

                break;
            }
            /* Deviation MISRA-2 <START> */
            case FRTP_CHNL_RX_CTS:
            {
                if((pChannel->Rx_BfS != 0U) && (pChannel->Rx_CtrDataAck != 0U))
                {
                    /* Previously received data was copied and Rx buffer is available - send CTS */
                    PduInfo.SduLength = FrTp_Size_Header_FC_CTS;
                    break;
                }
                else
                {
                    /* No rx buffer is available and previously received STF has not been copied -
                       perform WAIT procedure */
                    FrTp_StartTimer(pChannel->Rx_Timer3,pConnection->TimeBR);
                    TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_WAIT);
                }
                /* intentionally no break here (else path = fall through) */
            }
            case FRTP_CHNL_RX_WAIT:
            /* Deviation MISRA-2 <STOP> */
            {
                PduLengthType Bfs;       /* variable stores the buffer size */
                BufReq_ReturnType retcode;
#if (FRTP_COPY_STF_LOCALBUFFER == STD_ON)
                const boolean isUnsegmented = (pChannel->Rx_ML == pChannel->STFCopied);
#endif

                /* initialize info structure */
                PduInfo.SduLength = 0U;
                PduInfo.SduDataPtr = NULL_PTR;

                /* an error should not happen here, since no data is copied */
                /* we try to re-request a new rx buffer */
                retcode = PduR_FrTpCopyRxData(pConnection->RxSduIdx, &PduInfo, &Bfs);

                PduInfo.SduLength = FrTp_Size_Header_FC;

#if (FRTP_COPY_STF_LOCALBUFFER == STD_ON)
                /* Check if the PduR returned buffersize sufficient for the data copied to the
                   local buffer. */
                if((retcode == BUFREQ_OK) && ((Bfs != 0U) && (Bfs >= pChannel->STFCopied)))
#else
                if((retcode == BUFREQ_OK) && (Bfs != 0U))
#endif
                {
#if ( TS_SIZE_PduLengthType > TS_SIZE_uint16 )
                    pChannel->Rx_BfS = FrTp_Limit_uint16(Bfs); /* limit Bfs to uint16 maximum */
#else
                    pChannel->Rx_BfS = Bfs;
#endif
#if (FRTP_COPY_STF_LOCALBUFFER == STD_ON)
                    retcode = FrTp_RxSm_PduRCopyData(pChannel);

                    if(retcode != BUFREQ_OK)
                    {
                        RxSyncFlag = FALSE;
                    }
                    else
                    {
                        /* Unsegmented Acknowledged reception - prepare to send ACK. */
                        if((pChannel->RxHasAck != 0U) &&
                           (pChannel->Rx_ML == pChannel->Rx_CtrDataAck)
                          )
                        {
                            PduInfo.SduLength = FrTp_Size_Header_FC_ACK_RET;
                            TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_ACK);
                        }
                        else if((pChannel->RxHasAck == 0U) &&(isUnsegmented))
                        {
                            /* Unsegmented Unacknowledged reception is successful */
                            PduR_FrTpRxIndication(pConnection->RxSduIdx,NTFRSLT_OK);
                            FrTp_ResetRxChannel(pChannel);
                            RxSyncFlag = FALSE;
                        }
                       else
#else
                        {
#endif
                        /* Rx buffer is now available for Segmented reception - prepare to send
                           CTS. */
                            {
                                PduInfo.SduLength = FrTp_Size_Header_FC_CTS;
                                TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_CTS);
                            }
                            FrTp_StopTimer(pChannel->Rx_Timer3);
                        }
                }
                else if(pChannel->Rx_Timer3 == 0U)
                {
#if (FRTP_COPY_STF_LOCALBUFFER == STD_ON)
                    if((pChannel->RxHasAck == 0U) &&
                     (pChannel->Rx_ML == pChannel->STFCopied)
                    )
                    {
                      /* Data copy request still not successful */
                      PduR_FrTpRxIndication(pConnection->RxSduIdx,NTFRSLT_E_NOT_OK);
                      /* free channel for new receptions */
                      FrTp_ResetRxChannel(pChannel);

                      RxSyncFlag = FALSE;
                    }
                    else
#endif
                    if((pConnection->MaxWait != 0U) &&
                       (pChannel->Rx_CtrWait < pConnection->MaxWait)
                       )
                    {
                      pChannel->Rx_CtrWait++;
                    }
                    else
                    {
                      /* Data copy request still not successful */
                      PduR_FrTpRxIndication(pConnection->RxSduIdx,NTFRSLT_E_WFT_OVRN);
                      /* free channel for new receptions */
                      FrTp_ResetRxChannel(pChannel);

                      RxSyncFlag = FALSE;
                    }
                }
                else
                {
                    RxSyncFlag = FALSE;
                }
                break;
            }
            case FRTP_CHNL_RX_ACK:
            {
                PduInfo.SduLength = FrTp_Size_Header_FC_ACK_RET;
                break;
            }

            case FRTP_CHNL_RX_OVFL: /* fall through */
            case FRTP_CHNL_RX_ABORT:
            {
                PduInfo.SduLength = FrTp_Size_Header_FC;
                break;
            }

            case FRTP_CHNL_RX_DATA: /* fall through */
            case FRTP_CHNL_RX_IDLE: /* fall through */
            default:
            {
                RxSyncFlag = FALSE;
                break;
            }
        }
        /* Checking RxSyncFlag for transmission of FC */
        if (RxSyncFlag == TRUE)
        {
            FrTp_RxSm_FC_Transmit(pChannel, pConnection, &PduInfo);
        }
    }

    DBG_FRTP_RXSM_TRANSMITSYNCPOINT_EXIT(pChannel);
}



FUNC(void,FRTP_CODE) FrTp_RxSm_IndicateAndAbort
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    VAR(NotifResultType,AUTOMATIC) result
)
{
    /* get pointer to associated connection object */
    CONSTP2CONST(FrTp_ConnectionType,AUTOMATIC,FRTP_APPL_CONST) pConnection =
            FRTP_CFG_GET_PTR_TO_CONNECTION(pChannel->ConnectionIdx);

    DBG_FRTP_RXSM_INDICATEANDABORT_ENTRY(pChannel,result);

    /* received complete frame */
    PduR_FrTpRxIndication(pConnection->RxSduIdx,result);

    /* free channel for new receptions */
    FrTp_ResetRxChannel(pChannel);

    DBG_FRTP_RXSM_INDICATEANDABORT_EXIT(pChannel,result);
}


STATIC FUNC(void,FRTP_CODE) FrTp_RxSm_RxData_HandleStartFrame
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    VAR(uint8,AUTOMATIC) SN,
    P2CONST(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) pPduInfo
)
{
    /* flag for distinguishing initial STF frame and STF frame as response to RETRY frame */
    boolean stf_retransmission_flag = FALSE;
    /* local variable for frame payload length */
    VAR(uint8,AUTOMATIC) fpl;
    /* local variable for message length */
    VAR(uint16,AUTOMATIC) ml;
    /* local variable for atomic comparison */
    VAR(uint8,AUTOMATIC) RxState;

    /* get local copy of data pointer */
    CONSTP2CONST(uint8,AUTOMATIC,FRTP_APPL_DATA) pData =
        pPduInfo->SduDataPtr;

    /* get pointer to associated connection object */
    CONSTP2CONST(FrTp_ConnectionType,AUTOMATIC,FRTP_APPL_CONST) pConnection =
            FRTP_CFG_GET_PTR_TO_CONNECTION(pChannel->ConnectionIdx);

    DBG_FRTP_RXSM_RXDATA_HANDLESTARTFRAME_ENTRY(pChannel,SN,pPduInfo);

    /* get the frame payload length value */
    fpl = FrTp_GetFPL(pData);

    /* get Rx-statemachine state into local variable */
    TS_AtomicAssign8(RxState, pChannel->Rx_State);

    /* get the message length field */
    ml = FrTp_GetML(pData);

    /* check for an already ongoing reception */
    if (FrTp_Chnl_GetRxState(RxState) == FRTP_CHNL_RX_IDLE)
    {
        /* STF received in idle state
         * -> this is the start of a new reception */
        pChannel->Rx_CtrRetry = 0U;      /* reset the retry counter */
    }
    else if (pChannel->Rx_CtrDataAck == 0U)
    {
        /* STF received in reception process but no CTS yet received
         * -> this is a STF which is retransmitting data */
        stf_retransmission_flag = TRUE;
    }
    else
    {
        /* STF frame was received in reception process, but it's not a
         * retransmission of a start frame
         * -> abort the current reception - report an unknown Pdu received */
        PduR_FrTpRxIndication(pConnection->RxSduIdx,NTFRSLT_E_UNEXP_PDU);
    }

    pChannel->Rx_CtrWait = 0U;           /* reset wait counter */
    pChannel->Rx_PCI = 0U;               /* reset CF type */
    pChannel->Rx_CtrDataAck = 0U;        /* reset number of data received */
    pChannel->Rx_CtrBlockReceived = 0U;  /* reset num of data received within current block */
    pChannel->Rx_ML = ml;                /* store total message length to expect */
    pChannel->RxHasAck = SN;             /* acknowledge needed? Note that of STF the SN
                                          * parameter contain the information whether an
                                          * acknowlegde is needed (1) or not (0).
                                          */
    pChannel->TxFc_FrIfRetry = 0U;       /* reset FrIf retry counter */
    /* stop the timers (if still running) */
    FrTp_StopTimer(pChannel->Rx_Timer3);
    FrTp_StopTimer(pChannel->Rx_Timer2);
    FrTp_StopTimer(pChannel->Rx_Timer1);

    /* ignore inconsistent frames */
    if( (fpl > ml) || (fpl > FrTp_Max_FPL_STF))
    {
        /* ignore invalid frame */
        FrTp_ResetRxChannel(pChannel);
    }
    else if(((pConnection->TpOptions & FRTP_CONNECTION_ISBROADCAST) != 0U) && 
               ((ml != fpl) || (SN != 0U))
           )
    {
        /* ignore segmented or acknowledged broadcast connection */
        FrTp_ResetRxChannel(pChannel);
    }
    else
    {
        /* FPL is consistent */

        PduLengthType Bfs;  /* variable stores the buffer size */

        /* indicate the start of a  new reception (and obtain the existing buffer size */
        BufReq_ReturnType RetCode;

        if (stf_retransmission_flag == FALSE)
        {
            /* just a regular STF -> start reception */
            RetCode = PduR_FrTpStartOfReception(pConnection->RxSduIdx,pChannel->Rx_ML,&Bfs);
        }
        else
        {
            /* retransmission of STF -> just try to copy data */
            PduInfoType pduInfo = {NULL_PTR, 0U};
            RetCode = PduR_FrTpCopyRxData(pConnection->RxSduIdx, &pduInfo, &Bfs);
        }

        /* we got a buffer reserved */
        if ((RetCode == BUFREQ_OK) && (Bfs>=fpl))
        {
            /* PduR has an available buffer and buffer size is large enough to
             * copy the start frame */
            FrTp_RxSm_RxData_HandleSFBufOk(pChannel, SN, pPduInfo);
        }
        else if ( ((RetCode == BUFREQ_OK) && (Bfs<fpl))
                ||(RetCode == BUFREQ_E_BUSY )
                )
        {
            /* PduR does not have a sufficiently large buffer*/
            FrTp_RxSm_RxData_HandleSFBufBusy(pChannel, SN, pPduInfo);
        }
        else
        {
            /* PduR returned NOT_OK or OVFLW */
            FrTp_RxSm_RxData_HandleSFBufNOk(pChannel, SN, pPduInfo, RetCode,
                                            stf_retransmission_flag
                                           );
        }
    }

    DBG_FRTP_RXSM_RXDATA_HANDLESTARTFRAME_EXIT(pChannel,SN,pPduInfo);
}

STATIC FUNC(void,FRTP_CODE) FrTp_RxSm_RxData_HandleSFBufOk
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    VAR(uint8,AUTOMATIC) SN,
    P2CONST(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) pPduInfo
)
{
    PduLengthType Bfs;
    /* get local copy of data pointer */
    CONSTP2CONST(uint8,AUTOMATIC,FRTP_APPL_DATA) pData =
        pPduInfo->SduDataPtr;

    /* get the frame payload length value */
    const uint8 fpl = FrTp_GetFPL(pData);

    /* get the message length field */
    const uint16 ml = FrTp_GetML(pData);

    /* get pointer to associated connection object */
    CONSTP2CONST(FrTp_ConnectionType,AUTOMATIC,FRTP_APPL_CONST) pConnection =
            FRTP_CFG_GET_PTR_TO_CONNECTION(pChannel->ConnectionIdx);

    /* buffer is large enough for the startframe */

    PduInfoType PduInfo;    /* temporary for passing parameters */
    BufReq_ReturnType RetCode;

    DBG_FRTP_RXSM_RXDATA_HANDLESFBUFOK_ENTRY(pChannel,SN,pPduInfo);

    /* create a Pdu info structure for calling the upper layer */
    PduInfo.SduLength = (PduLengthType)fpl;
    PduInfo.SduDataPtr = FrTp_GetPayload_STF(pPduInfo->SduDataPtr);

    /* copy the payload into the receive buffer */
    /* an error should not happen here, since the buffer was previously guaranteed */
    RetCode = PduR_FrTpCopyRxData(pConnection->RxSduIdx, &PduInfo, &Bfs);

    if(RetCode == BUFREQ_OK)
    {
        /* set number of received data bytes */
        pChannel->Rx_CtrBlockReceived = (uint16)fpl;
        pChannel->Rx_CtrDataAck = (uint16)fpl;

        /* save actual buffersize available */
#if ( TS_SIZE_PduLengthType > TS_SIZE_uint16 )
        pChannel->Rx_BfS = FrTp_Limit_uint16(Bfs);
#else
        pChannel->Rx_BfS = Bfs;
#endif
        /* copy succeeded -> unsegmented reception is finished now,
         * segmented reception is continued with CTS or WAIT frame */
        FrTp_SendFc_Continue(pChannel, SN, fpl, ml);
    }
    else if(RetCode == BUFREQ_E_BUSY)
    {
#if (FRTP_COPY_STF_LOCALBUFFER == STD_ON)
        /* Copy the STF to local buffer and retry for PduR. */
        FrTp_CopyToLocalBuffer_STF(pChannel, fpl, pData);
#else
        /* should not happen: PduR granted this buffer just before and is now
         * returning busy -> same behavior as StartOfReception returning BUSY */
        FrTp_SendFc_Retry(pChannel, SN, fpl, ml);
#endif
    }
    else if (RetCode == BUFREQ_E_NOT_OK)
    {
        FrTp_SendFc_Abort(pChannel, SN, fpl, ml, TRUE);
    }
    /*  CHECK: NOPARSE */
    else
    {
        /* unreachable */
        FRTP_UNREACHABLE_CODE_ASSERT(FRTP_RXINDICATION_SERVICE_ID);
    }
    /* CHECK: PARSE */

    DBG_FRTP_RXSM_RXDATA_HANDLESFBUFOK_EXIT(pChannel,SN,pPduInfo);
}

STATIC FUNC(void,FRTP_CODE) FrTp_RxSm_RxData_HandleSFBufBusy
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    VAR(uint8,AUTOMATIC) SN,
    P2CONST(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) pPduInfo
)
{
    /* get local copy of data pointer */
    CONSTP2CONST(uint8,AUTOMATIC,FRTP_APPL_DATA) pData =
        pPduInfo->SduDataPtr;

    /* get the frame payload length value */
    const uint8 fpl = FrTp_GetFPL(pData);

#if (FRTP_COPY_STF_LOCALBUFFER == STD_OFF)
    /* get the message length field */
    const uint16 ml = FrTp_GetML(pData);
#else
    TS_PARAM_UNUSED(SN);
#endif

    DBG_FRTP_RXSM_RXDATA_HANDLESFBUFBUSY_ENTRY(pChannel,SN,pPduInfo);

#if (FRTP_COPY_STF_LOCALBUFFER == STD_ON)
    /* Copy to local buffer and retry for PduR.*/
    FrTp_CopyToLocalBuffer_STF(pChannel, fpl, pData);
#else
    /* Prepare to send a FC RETRY, copy of STF to local buffer is not supported. */
    FrTp_SendFc_Retry(pChannel, SN, fpl, ml);
#endif

    DBG_FRTP_RXSM_RXDATA_HANDLESFBUFBUSY_EXIT(pChannel,SN,pPduInfo);
}


STATIC FUNC(void,FRTP_CODE) FrTp_RxSm_RxData_HandleSFBufNOk
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    VAR(uint8,AUTOMATIC) SN,
    P2CONST(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) pPduInfo,
    VAR(BufReq_ReturnType,AUTOMATIC) RetCode,
    VAR(boolean,AUTOMATIC)STFRetransmitFlag
)
{
    /* get local copy of data pointer */
    CONSTP2CONST(uint8,AUTOMATIC,FRTP_APPL_DATA) pData =
        pPduInfo->SduDataPtr;

    /* get the frame payload length value */
    const uint8 fpl = FrTp_GetFPL(pData);

    /* get the message length field */
    const uint16 ml = FrTp_GetML(pData);

    DBG_FRTP_RXSM_RXDATA_HANDLESFBUFNOK_ENTRY(pChannel,SN,pPduInfo,RetCode,STFRetransmitFlag);

    if (RetCode == BUFREQ_E_OVFL)
    {
        FrTp_SendFc_Ovflw(pChannel, SN, fpl, ml);
    }
    else if (RetCode == BUFREQ_E_NOT_OK)
    {
        FrTp_SendFc_Abort(pChannel, SN, fpl, ml, STFRetransmitFlag);
    }
    /* CHECK: NOPARSE */
    else
    {
        /* unreachable */
        FRTP_UNREACHABLE_CODE_ASSERT(FRTP_RXINDICATION_SERVICE_ID);
    }
    /* CHECK: PARSE */

    DBG_FRTP_RXSM_RXDATA_HANDLESFBUFNOK_EXIT(pChannel,SN,pPduInfo,RetCode,STFRetransmitFlag);
}

STATIC FUNC(void,FRTP_CODE) FrTp_SendFc_Continue
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    uint8 SN,
    uint8 fpl,
    uint16 ml
)
{
    DBG_FRTP_SENDFC_CONTINUE_ENTRY(pChannel,SN,pData);
    if(fpl == ml)
    {
        /* handle unsegmented transfer */
        if(SN != 0U)
        {
            /* if acknowledge required, request ACK transmission */
            TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_ACK);
        }
        else
        {
              /* get pointer to associated connection object */
              CONSTP2CONST(FrTp_ConnectionType,AUTOMATIC,FRTP_APPL_CONST) pConnection =
                      FRTP_CFG_GET_PTR_TO_CONNECTION(pChannel->ConnectionIdx);

              /* if unacknowledged indicate full reception */
              PduR_FrTpRxIndication(pConnection->RxSduIdx,NTFRSLT_OK);

              /* free channel for new receptions */
              FrTp_ResetRxChannel(pChannel);
        }
    }
    else
    {
        /* Segmented transfer, perform CTS handling */
        TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_CTS);
    }

    DBG_FRTP_SENDFC_CONTINUE_EXIT(pChannel,SN,pData);
}


STATIC FUNC(void,FRTP_CODE) FrTp_SendFc_Ovflw
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    uint8 SN,
    uint8 fpl,
    uint16 ml
)
{
    /* acknowledged or segmented transmission */

    DBG_FRTP_SENDFC_OVFLW_ENTRY(pChannel,SN,fpl,ml);
    if( (SN != 0U) || (fpl != ml))
    {
        /* if acknowledged or segmented transmission, then indicate an overflow */
        TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_OVFL);
    }
    else
    {
        /* otherwise abort reception */
        FrTp_ResetRxChannel(pChannel);
    }


    DBG_FRTP_SENDFC_OVFLW_EXIT(pChannel,SN,fpl,ml);
}

STATIC FUNC(void,FRTP_CODE) FrTp_SendFc_Abort
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    uint8 SN,
    uint8 fpl,
    uint16 ml,
    boolean IndicatePduR
)
{
    /* acknowledged or segmented transmission */

    DBG_FRTP_SENDFC_ABORT_ENTRY(pChannel,SN,fpl,ml,IndicatePduR);
    if( (SN != 0U) || (fpl != ml))
    {
        /* if acknowledged or segmented transmission, then abort */
        if(IndicatePduR == TRUE)
        {
            TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_ABORT_NO_BUFFER);
        }
        else
        {
            TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_ABORT);
        }
    }
    else
    {
        /* otherwise abort reception */
        FrTp_ResetRxChannel(pChannel);
    }

    DBG_FRTP_SENDFC_ABORT_EXIT(pChannel,SN,fpl,ml,IndicatePduR);
}

#if (FRTP_COPY_STF_LOCALBUFFER == STD_ON)
STATIC FUNC(void,FRTP_CODE) FrTp_CopyToLocalBuffer_STF
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    uint8 fpl,
    CONSTP2CONST(uint8,AUTOMATIC,FRTP_APPL_DATA) pData
)
{

    DBG_FRTP_COPYTOLOCALBUFFER_STF_ENTRY(pChannel,fpl,pData);

    /* Copy STF payload to the local buffer and prepare to retry for PduR. */
    TS_MemCpy(pChannel->LocalSTFBuffer, &pData[FrTp_Size_Header_STF], (uint16)fpl);
    pChannel->STFCopied = fpl;


    TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_CTS);

    DBG_FRTP_COPYTOLOCALBUFFER_STF_EXIT(pChannel,fpl,pData);
}

STATIC FUNC(BufReq_ReturnType,FRTP_CODE) FrTp_RxSm_PduRCopyData
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel
)
{
    BufReq_ReturnType retval = BUFREQ_OK;
    PduInfoType PduInfo;
    PduLengthType Bfs;
    CONSTP2CONST(FrTp_ConnectionType,AUTOMATIC,FRTP_APPL_CONST) pConnection =
            FRTP_CFG_GET_PTR_TO_CONNECTION(pChannel->ConnectionIdx);

    /* Check if STF is copied to the local buffer, pass it to PduR. */
    if(pChannel->STFCopied != 0U)
    {
        PduInfo.SduLength = pChannel->STFCopied;
        PduInfo.SduDataPtr = pChannel->LocalSTFBuffer;

        /* Error should not happen here, buffer was just granted. */
        retval = PduR_FrTpCopyRxData(pConnection->RxSduIdx, &PduInfo, &Bfs);

        if(retval == BUFREQ_OK)
        {
            pChannel->Rx_CtrBlockReceived = pChannel->STFCopied;
            pChannel->Rx_CtrDataAck = pChannel->STFCopied;
            pChannel->Rx_BfS = Bfs;
            pChannel->STFCopied = 0U;
        }
    }
    return retval;
}
#else

STATIC FUNC(void,FRTP_CODE) FrTp_SendFc_Retry
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    uint8 SN,
    uint8 fpl,
    uint16 ml
)
{
    /* get pointer to associated connection object */
    CONSTP2CONST(FrTp_ConnectionType,AUTOMATIC,FRTP_APPL_CONST) pConnection =
            FRTP_CFG_GET_PTR_TO_CONNECTION(pChannel->ConnectionIdx);

    DBG_FRTP_SENDFC_RETRY_ENTRY(pChannel,SN,fpl,ml);

    if(SN != 0U)
    {
        /* if acknowledged transmission (segmented or unsegmented),
         * then request a retry */
        TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_STF_RETRY);
    }
    else if(fpl != ml)
    {
        /* if segmented (unacknowledged) transmission, send abort frame and indicate to PduR */
        TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_ABORT_NO_BUFFER);
    }
    else
    {
        /* otherwise (unacknowledged, unsegmented) just
         * - ignore frame
         * - free channel for new receptions
         * - indicate to PduR */
        FrTp_ResetRxChannel(pChannel);
        PduR_FrTpRxIndication(pConnection->RxSduIdx, NTFRSLT_E_NOT_OK);
    }

    DBG_FRTP_SENDFC_RETRY_EXIT(pChannel,SN,fpl,ml);
}
#endif

STATIC FUNC(void,FRTP_CODE) FrTp_RxSm_RxData_HandleConFrame
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    VAR(uint8,AUTOMATIC) PCI,
    VAR(uint8,AUTOMATIC) SN,
    P2CONST(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) pPduInfo
)
{

    /* get local copy of data pointer */
    CONSTP2CONST(uint8,AUTOMATIC,FRTP_APPL_DATA) pData =
        pPduInfo->SduDataPtr;

    /* local variable to store return value of PduR call */
    BufReq_ReturnType retval;

    /* get the frame payload length value */
    const uint8 fpl = FrTp_GetFPL(pData);

    /* local variable for atomic comparison */
    VAR(uint8,AUTOMATIC) RxState;

    /* get pointer to associated connection object */
    CONSTP2CONST(FrTp_ConnectionType,AUTOMATIC,FRTP_APPL_CONST) pConnection =
            FRTP_CFG_GET_PTR_TO_CONNECTION(pChannel->ConnectionIdx);

    DBG_FRTP_RXSM_RXDATA_HANDLECONFRAME_ENTRY(pChannel,PCI,SN,pPduInfo);

    /* get Rx-statemachine state into local variable */
    TS_AtomicAssign8(RxState, pChannel->Rx_State);

    /* check if data is currently expected */
    if(FrTp_Chnl_GetRxState(RxState) == FRTP_CHNL_RX_DATA)
    {
        /* is this an expected type of CF ? */
        if( ( (PCI&FRTP_PCITYPE_CF_MASK) ==
              (FRTP_PCITYPE_CF1 + (pChannel->Rx_PCI&FRTP_PCITYPE_CFI_MASK))
            )
          ||(PCI == FRTP_PCITYPE_EOB)
          )
        {
            /* increment sequence number */
            if((pChannel->Rx_PCI&FRTP_PCITYPE_SN_MASK) < FRTP_PCITYPE_SN_MASK)
            {
                pChannel->Rx_PCI++;
            }
            else
            {
                pChannel->Rx_PCI &= (uint8)(~FRTP_PCITYPE_SN_MASK);
            }

            /* check for valid sequence number */
            if((pChannel->Rx_PCI&FRTP_PCITYPE_SN_MASK) != SN)
            {
                /* sequence number is invalid */
                if( pChannel->RxHasAck != 0U)
                {
                    /* if acknowlegde is enabled, send a retry message */
                    TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_RETRY);
                }
                else
                {
                    /* if no acknowlegde is enabled, send an abort message */
                    TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_ABORT_WRONG_SN);
                }
            }
            else
            {
                /* sequence number is valid */

                PduInfoType PduInfo;     /* temporary for passing parameters */
                PduLengthType Bfs;       /* variable stores the buffer size */

                /* initialize info structure */
                PduInfo.SduLength = (PduLengthType)fpl;
                PduInfo.SduDataPtr = FrTp_GetPayload_CF(pPduInfo->SduDataPtr);

                /* copy the payload into the receive buffer */
                /* an error should not happen here, since the buffer was previosuly guranteed */
                retval = PduR_FrTpCopyRxData(pConnection->RxSduIdx, &PduInfo, &Bfs);

                if(retval != BUFREQ_OK)
                {
                    TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_ABORT_NO_BUFFER);
                }
                else
                {
#if ( TS_SIZE_PduLengthType > TS_SIZE_uint16 )
                    pChannel->Rx_BfS = FrTp_Limit_uint16(Bfs); /* limit Bfs to uint16 maximum */
#else
                    pChannel->Rx_BfS = Bfs;
#endif

                    pChannel->Rx_CtrDataAck += (uint16)fpl;         /* summ up total data received */
                    pChannel->Rx_CtrBlockReceived += (uint16)fpl;    /* summ up block data received */

                    /* at end of block request CTS transmission */
                    if(PCI == FRTP_PCITYPE_EOB)
                    {
                        /* EOB CF received */

                        /* stop the CR timer */
                        FrTp_StopTimer(pChannel->Rx_Timer2);

                        /* request a CTS transmission */
                        TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_CTS);

                        /* reset block data counter */
                        pChannel->Rx_CtrBlockReceived = 0U;

                        /* reset wait and retry counter */
                        pChannel->Rx_CtrRetry = 0U;
                        pChannel->Rx_CtrWait = 0U;
                    }
                    else
                    {
                        /* normal CF frame received */

                        /* restart CR timer */
                        FrTp_StartTimer(pChannel->Rx_Timer2,pConnection->TimeroutCR);
                    }
                }
            }
        }
    }

    DBG_FRTP_RXSM_RXDATA_HANDLECONFRAME_EXIT(pChannel,PCI,SN,pPduInfo);
}



STATIC FUNC(void,FRTP_CODE) FrTp_RxSm_RxData_HandleLastFrame
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    P2CONST(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) pPduInfo
)
{
    /* local variabel for atomic comparison */
    VAR(uint8,AUTOMATIC) RxState;

    /* local variable to store return value of PduR call */
    BufReq_ReturnType retval;

    /* get local copy of data pointer */
    CONSTP2CONST(uint8,AUTOMATIC,FRTP_APPL_DATA) pData =
        pPduInfo->SduDataPtr;

    /* get the frame payload length value */
    const uint8 fpl = FrTp_GetFPL(pData);

    /* get pointer to associated connection object */
    CONSTP2CONST(FrTp_ConnectionType,AUTOMATIC,FRTP_APPL_CONST) pConnection =
            FRTP_CFG_GET_PTR_TO_CONNECTION(pChannel->ConnectionIdx);

    DBG_FRTP_RXSM_RXDATA_HANDLELASTFRAME_ENTRY(pChannel,pPduInfo);

    /* get Rx-statemachine state into local variable */
    TS_AtomicAssign8(RxState, pChannel->Rx_State);

    /* check if data is currently expected */
    if(FrTp_Chnl_GetRxState(RxState) == FRTP_CHNL_RX_DATA)
    {

        /* get the message length field */
        const uint16 ml = FrTp_GetML(pData);

        pChannel->Rx_CtrDataAck += (uint16)fpl; /* sum up received bytes */

        /* check message length consistency */
        if((pChannel->Rx_CtrDataAck != ml) || (pChannel->Rx_CtrDataAck != pChannel->Rx_ML))
        {
            if(pChannel->RxHasAck != 0U)
            {
                /* if acknowlegde is enabled, send an abort message */
                TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_ABORT_ML_MISMATCH);
            }
            else
            {
                /* indicate an invalid sequence number */
                PduR_FrTpRxIndication(pConnection->RxSduIdx,NTFRSLT_E_FR_ML_MISMATCH);

                /* free channel for new receptions */
                FrTp_ResetRxChannel(pChannel);
            }
        }
        else
        {
            PduInfoType PduInfo;     /* temporary for passing parameters */
            PduLengthType BfsUnused; /* variable stores the buffer size */

            /* initialize info structure */
            PduInfo.SduLength = (PduLengthType)fpl;
            PduInfo.SduDataPtr = FrTp_GetPayload_LF(pPduInfo->SduDataPtr);

            /* copy the payload into the receive buffer */
            /* an error should not happen here, since the buffer was previosuly guranteed */
            retval = PduR_FrTpCopyRxData(pConnection->RxSduIdx, &PduInfo, &BfsUnused);

            if(retval != BUFREQ_OK)
            {
                TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_ABORT_NO_BUFFER);
            }
            else
            {
                /* stop the CR timer */
                FrTp_StopTimer(pChannel->Rx_Timer2);

                if(pChannel->RxHasAck != 0U)
                {
                    /* acknowledge is requested */
                    TS_AtomicAssign8(pChannel->Rx_State, FRTP_CHNL_RX_ACK);
                }
                else
                {
                    /* no acknowledge is requested */

                    /* received complete frame */
                    PduR_FrTpRxIndication(pConnection->RxSduIdx,NTFRSLT_OK);

                    /* free channel for new receptions */
                    FrTp_ResetRxChannel(pChannel);
                }
            }
        }
    }

    DBG_FRTP_RXSM_RXDATA_HANDLELASTFRAME_EXIT(pChannel,pPduInfo);
}


FUNC(void,FRTP_CODE) FrTp_RxSm_RxData
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    VAR(uint8,AUTOMATIC) PCI,
    VAR(uint8,AUTOMATIC) SN,
    P2CONST(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) pPduInfo
)
{

#if (FRTP_FULLDUPLEX_ENABLE == STD_OFF)

    /* get Rx-statemachine state into local variable */
    VAR(uint8,AUTOMATIC) TxState;

    DBG_FRTP_RXSM_RXDATA_ENTRY(pChannel,PCI,SN,pPduInfo);
    TS_AtomicAssign8(TxState, pChannel->Tx_State);

    /* no transmission in progress? */
    if(FrTp_Chnl_GetTxState(TxState) == FRTP_CHNL_TX_IDLE)
#endif
    {
        switch(PCI)
        {
            /* received a startframe */
            case FRTP_PCITYPE_STFU:
            {
                FrTp_RxSm_RxData_HandleStartFrame(pChannel,SN,pPduInfo);
                break;
            }

            /* any of the consecutive frames */
            case FRTP_PCITYPE_CF1: /* fall through */
            case FRTP_PCITYPE_CF2: /* fall through */
            case FRTP_PCITYPE_EOB:
            {
                FrTp_RxSm_RxData_HandleConFrame(pChannel,PCI,SN,pPduInfo);
                break;
            }

            case FRTP_PCITYPE_LF:
            {
                /* check lower bytes are not used */
                if( 0U == SN )
                {
                  FrTp_RxSm_RxData_HandleLastFrame(pChannel,pPduInfo);
                }
                break;
            }
            /* CHECK: NOPARSE */
            default:
            {
                /* unreachable */
                FRTP_UNREACHABLE_CODE_ASSERT(FRTP_RXINDICATION_SERVICE_ID);
                break;
            }
            /* CHECK: PARSE */
        }
    }

    DBG_FRTP_RXSM_RXDATA_EXIT(pChannel,PCI,SN,pPduInfo);
}

/**
 * \brief Resets all state variables for the given RX channel.
 */
FUNC(void,FRTP_CODE) FrTp_ResetRxChannel
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel
)
{
    /* enter exclusive area */

    DBG_FRTP_RESETRXCHANNEL_ENTRY(pChannel);
    SchM_Enter_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

    FrTp_StopTimer((pChannel)->Rx_Timer1);
    FrTp_StopTimer((pChannel)->Rx_Timer2);
    FrTp_StopTimer((pChannel)->Rx_Timer3);
    (pChannel)->Rx_State = FRTP_CHNL_RX_IDLE;
#if (FRTP_FULLDUPLEX_ENABLE == STD_ON)
    if((pChannel)->Tx_State == FRTP_CHNL_TX_IDLE)
#endif
    {
        (pChannel)->ConnectionIdx = FRTP_CONNECTION_INVALID;
    }

    /* exit exclusive area */
    SchM_Exit_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

    DBG_FRTP_RESETRXCHANNEL_EXIT(pChannel);
}

/*
 * Stop code section declaration
 */
#define FRTP_STOP_SEC_CODE
#include <MemMap.h>


