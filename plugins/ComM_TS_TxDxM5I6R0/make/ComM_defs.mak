# \file
#
# \brief AUTOSAR ComM
#
# This file contains the implementation of the AUTOSAR
# module ComM.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

ComM_CORE_PATH      := $(SSC_ROOT)\ComM_$(ComM_VARIANT)

ComM_OUTPUT_PATH    := $(AUTOSAR_BASE_OUTPUT_PATH)

#################################################################
# REGISTRY
SSC_PLUGINS            += ComM
ComM_DEPENDENT_PLUGINS := base_make tresos
ComM_VERSION           := 2.00.00
ComM_DESCRIPTION       := ComM Basic Software Makefile PlugIn for Autosar
CC_INCLUDE_PATH        += \
   $(ComM_CORE_PATH)\include \
   $(ComM_OUTPUT_PATH)\include
