/**
 * \file
 *
 * \brief AUTOSAR FiM
 *
 * This file contains the implementation of the AUTOSAR
 * module FiM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined FIM_CFG_H)
#define FIM_CFG_H

/* This file contains the generated FiM module configuration. */

[!AUTOSPACING!]
[!//
[!VAR "Spaces31" = "'                               '"!]
[!//
[!/* verify all FIDs not using more than 255 Dem events */!][!//
[!/* this relates to FiM_FidConfig[].numEventCfg and to FiM_FidInhibitCounter[] */!][!//
[!INDENT "0"!]
  [!LOOP "FiMConfigSet/*/FiMFID/*"!]
    [!/* count single Dem events of this FID */!][!//
    [!VAR "EventPerFidCount" = "count(../../FiMInhibitionConfiguration/*[name(as:ref(FiMInhFunctionIdRef)) = name(node:current()) and FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceDemRef'])"!]
    [!/* count individual Dem events of all event summaries of this FID */!][!//
    [!LOOP "../../FiMInhibitionConfiguration/*[name(as:ref(FiMInhFunctionIdRef)) = name(node:current()) and FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceSumRef']"!]
      [!VAR "EventPerFidCount" = "$EventPerFidCount + count(../../FiMEventSummary/*[FiMOutputSumEventRef = node:current()/FiMInhEventId/FiMInhRefChoice/FiMInhSumRef/*[1]])"!]
    [!ENDLOOP!]
    [!IF "$EventPerFidCount > 255"!]
      [!ERROR!]
        More than 255 events (single Dem events and individual Dem events from event summaries) are assigned to the FID '[!"name(.)"!]'. Please reduce the number of assigned Dem events.
      [!ENDERROR!]
    [!ENDIF!]
  [!ENDLOOP!]
[!ENDINDENT!]
[!//
/*==================[includes]===============================================*/

#include <Std_Types.h>  /* AUTOSAR standard types */

/*==================[macros]=================================================*/

/*------------------[API configuration options]------------------------------*/

/* !LINKSTO FIM033,1 */
#if (defined FIM_VERSION_INFO_API)
#error FIM_VERSION_INFO_API already defined
#endif
/** \brief Switch, indicating if Version Info is activated for FiM */
[!IF "FiMGeneral/FiMVersionInfoApi = 'true'"!]
  [!WS "0"!]#define FIM_VERSION_INFO_API         STD_ON
[!ELSE!][!//
  [!WS "0"!]#define FIM_VERSION_INFO_API         STD_OFF
[!ENDIF!]

#if (defined FIM_DEV_ERROR_DETECT)
#error FIM_DEV_ERROR_DETECT already defined
#endif
/** \brief Switch, indicating if the detection of development errors is
 ** activated or deactivated for FiM */
[!IF "FiMGeneral/FiMDevErrorDetect = 'true'"!]
  [!WS "0"!]#define FIM_DEV_ERROR_DETECT         STD_ON
[!ELSE!][!//
  [!WS "0"!]#define FIM_DEV_ERROR_DETECT         STD_OFF
[!ENDIF!]

#if (defined FIM_EVENT_UPDATE_TRIGGERED_BY_DEM)
#error FIM_EVENT_UPDATE_TRIGGERED_BY_DEM already defined
#endif
/** \brief Switch, indicating if the Event update triggered by Dem is activated or
 ** deactivated for FiM */
[!IF "FiMGeneral/FiMEventUpdateTriggeredByDem = 'true'"!]
  [!WS "0"!]#define FIM_EVENT_UPDATE_TRIGGERED_BY_DEM STD_ON
[!ELSE!][!//
  [!WS "0"!]#define FIM_EVENT_UPDATE_TRIGGERED_BY_DEM  STD_OFF
[!ENDIF!]

#if (defined FIM_DATA_FIXED)
#error FIM_DATA_FIXED already defined
#endif
/** \brief Switch, indicating if the data is fixed */
#define FIM_DATA_FIXED               STD_ON

#if (defined FIM_INCLUDE_RTE)
#error FIM_INCLUDE_RTE already defined
#endif
/** \brief Switch, indicating if RTE is activated or deactivated for FiM */
[!IF "FiMGeneral/FiMRteUsage = 'true'"!]
  [!WS "0"!]#define FIM_INCLUDE_RTE              STD_ON
[!ELSE!][!//
  [!WS "0"!]#define FIM_INCLUDE_RTE              STD_OFF
[!ENDIF!]

#if (defined FIM_MAXIMUM_EVENT_FID_LINKS)
#error FIM_MAXIMUM_EVENT_FID_LINKS already defined
#endif
/** \brief This configuration parameter specifies the total maximum number of
 ** links between EventIds and FIDs. */
#define FIM_MAXIMUM_EVENT_FID_LINKS  [!"num:integer(FiMGeneral/FiMMaxEventFidLinks)"!]U

#if (defined FIM_MAXIMUM_FIDS_PER_EVENT)
#error FIM_MAXIMUM_FIDS_PER_EVENT already defined
#endif
/** \brief This configuration parameter specifies the maximum number of FIDs
 ** that can be linked to a single event. */
#define FIM_MAXIMUM_FIDS_PER_EVENT   [!"num:integer(FiMGeneral/FiMMaxFidsPerEvent)"!]U

#if (defined FIM_MAXIMUM_EVENTS_PER_FID)
#error FIM_MAXIMUM_EVENTS_PER_FID already defined
#endif
/** \brief This configuration parameter specifies the maximum number of
 ** EventIds that can be linked to a single FID. */
#define FIM_MAXIMUM_EVENTS_PER_FID   [!"num:integer(FiMGeneral/FiMMaxEventsPerFid)"!]U

#if (defined FIM_MAXIMUM_SUMMARY_EVENTS)
#error FIM_MAXIMUM_SUMMARY_EVENTS already defined
#endif
/** \brief This configuration parameter specifies the maximum number of
 ** summarized events that can be configured. */
#define FIM_MAXIMUM_SUMMARY_EVENTS   [!"num:integer(FiMGeneral/FiMMaxSummaryEvents)"!]U

#if (defined FIM_MAXIMUM_SUMMARY_LINKS)
#error FIM_MAXIMUM_SUMMARY_LINKS already defined
#endif
/** \brief This configuration parameter specifies the total maximum number of
 ** links between EventIds and summarized events. */
#define FIM_MAXIMUM_SUMMARY_LINKS    [!"num:integer(FiMGeneral/FiMMaxSummaryLinks)"!]U

#if (defined FIM_MAXIMUM_TOTAL_LINKS)
#error FIM_MAXIMUM_TOTAL_LINKS already defined
#endif
/** \brief This configuration parameter specifies the total maximum number of
 ** links between EventIds and FIDs plus the number of links between EventIds
 ** and summarized events. */
#define FIM_MAXIMUM_TOTAL_LINKS      [!"num:integer(FiMGeneral/FiMMaxTotalLinks)"!]U

/*------------------[FIDs configuration]-------------------------------------*/

#if (defined FIM_FID_NUM)
#error FIM_FID_NUM already defined
#endif
/** \brief This configuration parameter specifies the total number of FIDs.
 **
 ** Calculated by number of configured FIDs including the invalid FID 0. */
#define FIM_FID_NUM    [!"num:i(count(FiMConfigSet/*/FiMFID/*) + 1)"!]U

/* Symbolic names of configured Function IDs */
[!INDENT "0"!]
  [!LOOP "node:order(FiMConfigSet/*/FiMFID/*, 'FiMFunctionId')"!]
    [!VAR "Indention" = "''"!]
    [!IF "string-length($Spaces31) > string-length(name(.))"!]
      [!VAR "Indention" = "substring($Spaces31, string-length(name(.)))"!]
    [!ENDIF!]

    #if (defined FiMConf_FiMFID_[!"name(.)"!])
    #error FiMConf_FiMFID_[!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value */
    #define FiMConf_FiMFID_[!"name(.)"!][!"$Indention"!] [!"FiMFunctionId"!]U

    #if (!defined FIM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
    #if (defined [!"name(.)"!])
    #error [!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
    #define [!"name(.)"!][!"$Indention"!] [!"FiMFunctionId"!]U

    #if (defined FiM_[!"name(.)"!])
    #error FiM_[!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value with module abbreviation as prefix
     [!WS!]** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
    #define FiM_[!"name(.)"!][!"$Indention"!] [!"FiMFunctionId"!]U
    #endif /* !defined FIM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
  [!ENDLOOP!]

  [!IF "FiMGeneral/FiMEventUpdateTriggeredByDem = 'true'"!]
    [!VAR "EventCount" = "0"!]
    [!/* loop over all Dem event parameter to search associated FIDs */!][!//
    [!LOOP "node:order(as:modconf('Dem')[1]/DemConfigSet/*/DemEventParameter/*, 'DemEventId')"!]
      [!VAR "FIDCount" = "0"!]
      [!VAR "eventName" = "name(.)"!]
      [!LOOP "as:modconf('FiM')[1]/FiMConfigSet/*/FiMInhibitionConfiguration/*"!]
        [!/* single FID associated to Dem event */!][!//
        [!IF "FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceDemRef' and name(as:ref(FiMInhEventId/FiMInhRefChoice/FiMInhEventRef)) = $eventName"!]
          [!VAR "FIDCount" = "$FIDCount + 1"!]
        [!ELSEIF "FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceSumRef'"!][!//
          [!VAR "eventSumName" = "name(as:ref(FiMInhEventId/FiMInhRefChoice/FiMInhSumRef/*[1]))"!]
          [!VAR "FIDCount" = "$FIDCount + count(../../FiMEventSummary/*[name(as:ref(FiMInputSumEventRef/*[1])) = $eventName and name(as:ref(FiMOutputSumEventRef)) = $eventSumName])"!]
        [!ENDIF!]
      [!ENDLOOP!]
      [!IF "num:i($FIDCount) > 0"!]
        [!VAR "EventCount" = "$EventCount + 1"!]
      [!ENDIF!]
    [!ENDLOOP!]

    #if (defined FIM_EVENT_NUM)
    #error FIM_EVENT_NUM already defined
    #endif
    /** \brief This configuration parameter specifies the total number of events.
     [!WS!]** Calculated by number of configured FIDs. */
    #define FIM_EVENT_NUM         [!"num:i($EventCount)"!]U
  [!ENDIF!][!/* endif of FiMEventUpdateTriggeredByDem */!][!//

  #if (defined FIM_MAX_DEM_EVENT_ID)
  #error FIM_MAX_DEM_EVENT_ID already defined
  #endif
  /** \brief This configuration parameter specifies the largest event Id
   [!WS!]** configured in Dem. */
  #define FIM_MAX_DEM_EVENT_ID  [!"num:max(as:modconf('Dem')[1]/DemConfigSet/*/DemEventParameter/*/DemEventId)"!]U
[!ENDINDENT!]

/*------------------[Event summaries configuration]----------------------------*/

/* Symbolic names of configured Event Summary IDs - not relevant to this implementation */
[!INDENT "0"!]
  [!LOOP "node:order(FiMConfigSet/*/FiMSummaryEventId/*, 'FiMEventSumId')"!]
    [!VAR "Indention" = "''"!]
    [!IF "string-length($Spaces31) > string-length(name(.))"!]
      [!VAR "Indention" = "substring($Spaces31, string-length(name(.)))"!]
    [!ENDIF!]

    #if (defined FiMConf_FiMSummaryEventId_[!"name(.)"!])
    #error FiMConf_FiMSummaryEventId_[!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value */
    #define FiMConf_FiMSummaryEventId_[!"name(.)"!][!"$Indention"!] [!"FiMEventSumId"!]U

    #if (!defined FIM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
    #if (defined [!"name(.)"!])
    #error [!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
    #define [!"name(.)"!][!"$Indention"!] [!"FiMEventSumId"!]U

    #if (defined FiM_[!"name(.)"!])
    #error FiM_[!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value with module abbreviation as prefix
     [!WS!]** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
    #define FiM_[!"name(.)"!][!"$Indention"!] [!"FiMEventSumId"!]U
    #endif /* !defined FIM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
  [!ENDLOOP!]
[!ENDINDENT!]

/*==================[type definitions]=======================================*/

/*------------------[FiM_ConfigType]----------------------------------------*/

/** \brief This type defines a data structure for the post build
 ** parameters of the FiM
 **
 ** \note Type is unused, as only pre-compile time support is implemented. */
typedef uint8 FiM_ConfigType;

/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

#define FIM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/** \brief Configuration structure */
extern CONST(FiM_ConfigType, FIM_CONST) [!"name(FiMConfigSet/*[1])"!];

#define FIM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

#endif /* if !defined( FIM_CFG_H ) */
/*==================[end of file]============================================*/
