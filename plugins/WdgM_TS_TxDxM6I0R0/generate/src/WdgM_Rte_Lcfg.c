/**
 * \file
 *
 * \brief AUTOSAR WdgM
 *
 * This file contains the implementation of the AUTOSAR
 * module WdgM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!INCLUDE "../include/WdgM_Cfg.m"!][!//
[!CODE!][!//
[!AUTOSPACING!][!//

[!IF "$RteUsageEnabled"!][!//
/*==================[inclusions]=================================================================*/

#include <Std_Types.h>                                                         /* standard types */

/* Prevent redeclarations of symbols from Rte_WdgM.h by our public headers */
#define WDGM_INTERNAL_USAGE
/* !LINKSTO WDGM.EB.ASR32.WDGM026,1 */
#include <WdgM_Rte_Lcfg.h>                           /* Module internal Rte declarations and API */
#include <WdgIf.h>                                                             /* WdgIf_ModeType */

/*==================[macros]=====================================================================*/

/*==================[type definitions]===========================================================*/

/*==================[internal function declarations]=============================================*/

/*==================[internal data]==============================================================*/

/*==================[external data]==============================================================*/

/*==================[internal constants]=========================================================*/

#define WDGM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>
[!IF "$DefaultServiceAPI != 'NONE'"!]

CONST(WdgM_RteSwitchFunc, WDGM_CONST) WdgM_RteSwitch[WDGM_EB_SE_NUM] =
{
[!LOOP "node:order(WdgMGeneral/WdgMSupervisedEntity/*, 'WdgMSupervisedEntityId')"!][!//
  &Rte_Switch_mode_[!"name(.)"!]_currentMode,
[!ENDLOOP!][!//
};

CONST(WdgM_RteModeFunc, WDGM_CONST) WdgM_RteMode[WDGM_EB_SE_NUM] =
{
[!LOOP "node:order(WdgMGeneral/WdgMSupervisedEntity/*, 'WdgMSupervisedEntityId')"!][!//
  &Rte_Mode_mode_[!"name(.)"!]_currentMode,
[!ENDLOOP!][!//
};
[!ENDIF!]
[!IF "$ASR32ServiceAPIEnabled"!]

CONST(WdgM_ASR32_RteSwitchFunc, WDGM_CONST) WdgM_ASR32_RteSwitch[WDGM_EB_SE_NUM] =
{
[!LOOP "node:order(WdgMGeneral/WdgMSupervisedEntity/*, 'WdgMSupervisedEntityId')"!][!//
  &Rte_Switch_mode_ASR32_[!"name(.)"!]_currentMode,
[!ENDLOOP!][!//
};

CONST(WdgM_ASR32_RteModeFunc, WDGM_CONST) WdgM_ASR32_RteMode[WDGM_EB_SE_NUM] =
{
[!LOOP "node:order(WdgMGeneral/WdgMSupervisedEntity/*, 'WdgMSupervisedEntityId')"!][!//
  &Rte_Mode_mode_ASR32_[!"name(.)"!]_currentMode,
[!ENDLOOP!][!//
};
[!ENDIF!]
[!IF "$ASR40ServiceAPIEnabled"!]

CONST(WdgM_ASR40_RteSwitchFunc, WDGM_CONST) WdgM_ASR40_RteSwitch[WDGM_EB_SE_NUM] =
{
[!LOOP "node:order(WdgMGeneral/WdgMSupervisedEntity/*, 'WdgMSupervisedEntityId')"!][!//
  &Rte_Switch_mode_ASR40_[!"name(.)"!]_currentMode,
[!ENDLOOP!][!//
};

CONST(WdgM_ASR40_RteModeFunc, WDGM_CONST) WdgM_ASR40_RteMode[WDGM_EB_SE_NUM] =
{
[!LOOP "node:order(WdgMGeneral/WdgMSupervisedEntity/*, 'WdgMSupervisedEntityId')"!][!//
  &Rte_Mode_mode_ASR40_[!"name(.)"!]_currentMode,
[!ENDLOOP!][!//
};
[!ENDIF!]

#define WDGM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[external constants]=========================================================*/

/*==================[external function definitions]==============================================*/

/*==================[internal function definitions]==============================================*/

[!ELSE!][!//

/* Avoid empty translation unit according to ISO C90 */
typedef void TSPreventEmptyTranslationUnit;

[!ENDIF!][!//
/*==================[end of file]================================================================*/
[!ENDCODE!][!//
