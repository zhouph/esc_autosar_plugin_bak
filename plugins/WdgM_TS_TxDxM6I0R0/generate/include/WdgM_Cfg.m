[!/**
 * \file
 *
 * \brief AUTOSAR WdgM
 *
 * This file contains the implementation of the AUTOSAR
 * module WdgM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//
[!NOCODE!][!//

[!// Included by generated pre-compile time configuration file



[!IF "not(var:defined('WDGM_CFG_M'))"!]
[!VAR "WDGM_CFG_M"="'true'"!]

[!// EXPORTED VARIABLES (boolean parameters) -------------------------------------------------------

[!VAR "BswCompatibilityMode" = "node:value(WdgMGeneral/WdgMBSWCompatibilityMode)"!]
[!VAR "DevErrorDetectEnabled" = "WdgMGeneral/WdgMDevErrorDetect = 'true'"!]
[!VAR "VersionInfoApiEnabled" = "WdgMGeneral/WdgMVersionInfoApi = 'true'"!]
[!VAR "DemStoppedSupervisionReportEnabled" = "WdgMGeneral/WdgMDemStoppedSupervisionReport = 'true'"!]
[!VAR "ImmediateResetEnabled" = "WdgMGeneral/WdgMImmediateReset = 'true'"!]
[!VAR "OffModeEnabled" = "WdgMGeneral/WdgMOffModeEnabled = 'true'"!]
[!VAR "RteUsageEnabled" = "WdgMGeneral/WdgMRteUsage = 'true'"!]
[!VAR "DefensiveBehaviorEnabled" = "WdgMGeneral/WdgMDefensiveBehavior = 'true'"!]
[!VAR "CallerIdsEnabled" = "node:exists(WdgMGeneral/WdgMCallerIds)"!]
[!VAR "SleepModeEnabled" = "node:exists(WdgMConfigSet/*[1]/WdgMSleepMode)"!]
[!VAR "ServiceAPIEnabled" = "node:exists(WdgMGeneral/WdgMServiceAPI)"!]
[!VAR "SupervisorCalloutsEnabled" = "node:exists(WdgMGeneral/WdgMSupervisorCallouts)"!]
[!VAR "GetExpectedInitStateCalloutEnabled" = "node:exists(WdgMGeneral/WdgMSupervisorCallouts/WdgMGetExpectedInitStateCallout)"!]
[!VAR "GetExpectedWdgMModeCalloutEnabled" = "node:exists(WdgMGeneral/WdgMSupervisorCallouts/WdgMGetExpectedWdgMModeCallout)"!]
[!VAR "GetTimeCalloutEnabled" = "node:exists(WdgMGeneral/WdgMSupervisorCallouts/WdgMGetElapsedTimeCallout)"!]
[!VAR "IsPerformResetCalloutEnabled" = "node:exists(WdgMGeneral/WdgMSupervisorCallouts/WdgMIsPerformResetCallout)"!]
[!VAR "SupervisionExpiredCalloutEnabled" = "node:exists(WdgMGeneral/WdgMSupervisorCallouts/WdgMSupervisionExpiredCallout)"!]
[!VAR "IndividualModeSwitchCalloutEnabled" = "node:exists(WdgMGeneral/WdgMSupervisorCallouts/WdgMIndividualModeSwitchCallout)"!]
[!VAR "GlobalModeSwitchCalloutEnabled" = "node:exists(WdgMGeneral/WdgMSupervisorCallouts/WdgMGlobalModeSwitchCallout)"!]
[!VAR "DetCalloutEnabled" = "node:exists(WdgMGeneral/WdgMSupervisorCallouts/WdgMDetCallout)"!]
[!VAR "WdgMPartitioningEnabled" = "WdgMGeneral/WdgMPartitioningEnabled = 'true'"!]

[!IF "$ServiceAPIEnabled"!]
[!VAR "ActivateAliveSupervisionAPIEnabled" = "node:exists(WdgMGeneral/WdgMServiceAPI/WdgMEnableASR32ActivateAliveSupervisionAPI)"!]
[!VAR "DeactivateAliveSupervisionAPIEnabled" = "node:exists(WdgMGeneral/WdgMServiceAPI/WdgMEnableASR32DeactivateAliveSupervisionAPI)"!]
[!IF "$BswCompatibilityMode = 'AUTOSAR_31'"!]
[!VAR "ASR32ServiceAPIEnabled" = "'false'"!]
[!VAR "ASR40ServiceAPIEnabled" = "'false'"!]
[!VAR "DefaultServiceAPI" = "'AUTOSAR_32'"!]
[!ELSE!]
[!VAR "ASR32ServiceAPIEnabled" = "WdgMGeneral/WdgMServiceAPI/WdgMEnableASR32ServiceAPI = 'true'"!]
[!VAR "ASR40ServiceAPIEnabled" = "WdgMGeneral/WdgMServiceAPI/WdgMEnableASR40ServiceAPI = 'true'"!]
[!VAR "DefaultServiceAPI" = "node:value(WdgMGeneral/WdgMServiceAPI/WdgMDefaultASRServiceAPI)"!]
[!ENDIF!]
[!ELSE!]
[!VAR "ASR32ServiceAPIEnabled" = "'false'"!]
[!VAR "ASR40ServiceAPIEnabled" = "'false'"!]
[!VAR "ActivateAliveSupervisionAPIEnabled" = "'false'"!]
[!VAR "DeactivateAliveSupervisionAPIEnabled" = "'false'"!]
[!VAR "DefaultServiceAPI" = "'NONE'"!]
[!ENDIF!]

[!IF "$SupervisorCalloutsEnabled"!]
[!VAR "InitRedirectionCalloutAPIEnabled" = "not(node:empty(WdgMGeneral/WdgMSupervisorCallouts/WdgMInitRedirectionCallout))"!]
[!VAR "DeInitRedirectionCalloutAPIEnabled" = "not(node:empty(WdgMGeneral/WdgMSupervisorCallouts/WdgMDeInitRedirectionCallout))"!]
[!VAR "SetModeRedirectionCalloutAPIEnabled" = "not(node:empty(WdgMGeneral/WdgMSupervisorCallouts/WdgMSetModeRedirectionCallout))"!]
[!ELSE!]
[!VAR "InitRedirectionCalloutAPIEnabled" = "'false'"!]
[!VAR "DeInitRedirectionCalloutAPIEnabled" = "'false'"!]
[!VAR "SetModeRedirectionCalloutAPIEnabled" = "'false'"!]
[!ENDIF!]

[!VAR "NumberOfSupervisedEntities" = "num:i(count(WdgMGeneral/WdgMSupervisedEntity/*))"!]
[!VAR "NumberOfWdgModes" = "num:i(count(WdgMConfigSet/*[1]/WdgMMode/*))"!]
[!VAR "NumberOfWdgDrivers" = "num:i(count(WdgMGeneral/WdgMWatchdog/*))"!]
[!IF "$CallerIdsEnabled = 'true'"!]
[!VAR "NumberOfCallerIds" = "num:i(count(WdgMGeneral/WdgMCallerIds/WdgMCallerId/*))"!]
[!ELSE!]
[!VAR "NumberOfCallerIds" = "num:i(0)"!]
[!ENDIF!]

[!// Number of graphs is sum of configured internal and external graphs (for all WdgM modes)
[!VAR "NumberOfGraphs" = "num:i(count(WdgMGeneral/WdgMSupervisedEntity/*[num:i(count(WdgMInternalTransition/*))>0]) + count(WdgMConfigSet/*[1]/WdgMMode/*/WdgMExternalLogicalSupervision/*))"!]

[!// Number of DMs is sum of configured deadline supervision configs (for all WdgM modes)
[!VAR "NumberOfDMs" = "num:i(count(WdgMConfigSet/*[1]/WdgMMode/*/WdgMDeadlineSupervision/*))"!]

[!// Number of Alive Supervisions equals number of Checkpoints)
[!VAR "NumberOfCheckpoints" = "num:i(count(WdgMGeneral/WdgMSupervisedEntity/*/WdgMCheckpoint/*))"!]

[!// EXPORTED VARIABLES (string parameters) --------------------------------------------------------

[!IF "$ActivateAliveSupervisionAPIEnabled"!]
[!VAR "ActivateAliveSupervisionAPIName" = "node:value(WdgMGeneral/WdgMServiceAPI/WdgMEnableASR32ActivateAliveSupervisionAPI)"!]
[!ELSE!]
[!VAR "ActivateAliveSupervisionAPIName" = "''"!]
[!ENDIF!]

[!IF "$DeactivateAliveSupervisionAPIEnabled"!]
[!VAR "DeactivateAliveSupervisionAPIName" = "node:value(WdgMGeneral/WdgMServiceAPI/WdgMEnableASR32DeactivateAliveSupervisionAPI)"!]
[!ELSE!]
[!VAR "DeactivateAliveSupervisionAPIName" = "''"!]
[!ENDIF!]

[!IF "$InitRedirectionCalloutAPIEnabled"!]
[!VAR "InitRedirectionCalloutAPIName" = "node:value(WdgMGeneral/WdgMSupervisorCallouts/WdgMInitRedirectionCallout)"!]
[!ELSE!]
[!VAR "InitRedirectionCalloutAPIName" = "''"!]
[!ENDIF!]

[!IF "$DeInitRedirectionCalloutAPIEnabled"!]
[!VAR "DeInitRedirectionCalloutAPIName" = "node:value(WdgMGeneral/WdgMSupervisorCallouts/WdgMDeInitRedirectionCallout)"!]
[!ELSE!]
[!VAR "DeInitRedirectionCalloutAPIName" = "''"!]
[!ENDIF!]

[!IF "$SetModeRedirectionCalloutAPIEnabled"!]
[!VAR "SetModeRedirectionCalloutAPIName" = "node:value(WdgMGeneral/WdgMSupervisorCallouts/WdgMSetModeRedirectionCallout)"!]
[!ELSE!]
[!VAR "SetModeRedirectionCalloutAPIName" = "''"!]
[!ENDIF!]

[!IF "$GetExpectedInitStateCalloutEnabled"!]
[!VAR "GetExpectedInitStateCalloutName" = "node:value(WdgMGeneral/WdgMSupervisorCallouts/WdgMGetExpectedInitStateCallout)"!]
[!ELSE!]
[!VAR "GetExpectedInitStateCalloutName" = "''"!]
[!ENDIF!]

[!IF "$GetExpectedWdgMModeCalloutEnabled"!]
[!VAR "GetExpectedWdgMModeCalloutName" = "node:value(WdgMGeneral/WdgMSupervisorCallouts/WdgMGetExpectedWdgMModeCallout)"!]
[!ELSE!]
[!VAR "GetExpectedWdgMModeCalloutName" = "''"!]
[!ENDIF!]

[!IF "$GetTimeCalloutEnabled"!]
[!VAR "GetTimeCalloutName" = "node:value(WdgMGeneral/WdgMSupervisorCallouts/WdgMGetElapsedTimeCallout)"!]
[!VAR "TimeGranularity" = "node:value(WdgMGeneral/WdgMSupervisorCallouts/WdgMTimeGranularity)"!]
[!VAR "MainFunctionCycleTime" = "num:i(round(num:f(num:div(WdgMConfigSet/*[1]/WdgMMode/*[1]/WdgMSupervisionCycle, WdgMGeneral/WdgMSupervisorCallouts/WdgMTimeGranularity))))"!]
[!IF "$MainFunctionCycleTime = 0"!]
[!VAR "MainFunctionCycleTime" = "num:i(1)"!]
[!ENDIF!]
[!VAR "MainFunctionToleranceTime" = "num:i(round(num:f(num:div(node:value(WdgMGeneral/WdgMSupervisorCallouts/WdgMMainFunctionPeriodTolerance),node:value(WdgMGeneral/WdgMSupervisorCallouts/WdgMTimeGranularity)))))"!]
[!ELSE!]
[!VAR "GetTimeCalloutName" = "''"!]
[!VAR "MainFunctionCycleTime" = "num:i(0)"!]
[!VAR "MainFunctionToleranceTime" = "num:i(0)"!]
[!ENDIF!]

[!IF "$IsPerformResetCalloutEnabled"!]
[!VAR "IsPerformResetCalloutName" = "node:value(WdgMGeneral/WdgMSupervisorCallouts/WdgMIsPerformResetCallout)"!]
[!ELSE!]
[!VAR "IsPerformResetCalloutName" = "''"!]
[!ENDIF!]

[!IF "$SupervisionExpiredCalloutEnabled"!]
[!VAR "SupervisionExpiredCalloutName" = "node:value(WdgMGeneral/WdgMSupervisorCallouts/WdgMSupervisionExpiredCallout)"!]
[!ELSE!]
[!VAR "SupervisionExpiredCalloutName" = "''"!]
[!ENDIF!]

[!IF "$IndividualModeSwitchCalloutEnabled"!]
[!VAR "IndividualModeSwitchCalloutName" = "node:value(WdgMGeneral/WdgMSupervisorCallouts/WdgMIndividualModeSwitchCallout)"!]
[!ELSE!]
[!VAR "IndividualModeSwitchCalloutName" = "''"!]
[!ENDIF!]

[!IF "$GlobalModeSwitchCalloutEnabled"!]
[!VAR "GlobalModeSwitchCalloutName" = "node:value(WdgMGeneral/WdgMSupervisorCallouts/WdgMGlobalModeSwitchCallout)"!]
[!ELSE!]
[!VAR "GlobalModeSwitchCalloutName" = "''"!]
[!ENDIF!]

[!IF "$DetCalloutEnabled"!]
[!VAR "DetCalloutName" = "node:value(WdgMGeneral/WdgMSupervisorCallouts/WdgMDetCallout)"!]
[!ELSE!]
[!VAR "DetCalloutName" = "''"!]
[!ENDIF!]


[!// MACROS ----------------------------------------------------------------------------------------

[!// CONSISTENCY CHECKS ----------------------------------------------------------------------------

[!ENDIF!][!// multiple inclusion protection

[!ENDNOCODE!][!//