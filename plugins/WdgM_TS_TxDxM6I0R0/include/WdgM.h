/**
 * \file
 *
 * \brief AUTOSAR WdgM
 *
 * This file contains the implementation of the AUTOSAR
 * module WdgM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#if (!defined WDGM_H)
#define WDGM_H
/*==================[inclusions]=================================================================*/

/* !LINKSTO WDGM.ASR40.WDGM014.2,1 */
/* !LINKSTO WDGM.EB.ASR32.WDGM020,1 */
#include <WdgM_Types.h>                                                   /* WdgM's common Types */
/* !LINKSTO WDGM.EB.ASR32.WDGM020,1 */
#include <WdgM_BSW.h>                                                       /* WdgM's common API */

/*==================[macros]=====================================================================*/

/* !LINKSTO WDGM.EB.TIMEPM.WDGM020205,1 */
/* WdgM Generated interface version check */
#if !WDGM_EB_GENIF_VERSION_CHECK(1,1)
#error Version check between WdgM Static and WdgM Generated failed
#endif

/*==================[type definitions]===========================================================*/

/*==================[external function declarations]=============================================*/

/* !LINKSTO WDGM.EB.ASR32.WDGM100,1 */
/**
 * Wrapping macro for WdgM_UpdateAliveCounter to provide AUTOSAR 4.0 API as
 * default to other BSW modules
 */
#define WdgM_UpdateAliveCounter WdgM_ASR40_UpdateAliveCounter

/* !LINKSTO WDGM.EB.ASR32.WDGM100,1 */
/**
 * Wrapping macro for WdgM_CheckpointReached to provide AUTOSAR 4.0 API as
 * default to other BSW modules
 */
#define WdgM_CheckpointReached WdgM_ASR40_CheckpointReached

/*==================[external constants]=========================================================*/

/*==================[external data]==============================================================*/

#endif /* if !defined( WDGM_H ) */
/*==================[end of file]================================================================*/
