/**
 * \file
 *
 * \brief AUTOSAR WdgM
 *
 * This file contains the implementation of the AUTOSAR
 * module WdgM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 * Misra-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 19.6 (required)
 * #undef shall not be used
 *
 * Reason:
 * The macro names used for memory mapping are different between
 * AUTOSAR 3.x and AUTOSAR 4.x. For sake of compatibility between
 * these worlds, both macros are defined.
 * Since only one of them will be undefined within the MemMap.h file,
 * all macros are undefined after the memory section ends.
 * This avoids possible redundant macro definitions.
 *
 *
 * MISRA-2) Deviated Rule: 16.4 (required)
 * The identifiers used in the declaration and definition of a function shall be identical.
 *
 * Reason:
 * AUTOSAR provides the specifification of a PortDefinedArgumentValue via
 * a RunnableEntityArgument only till version 4.0.3. In older versions,
 * this specification was not possible leading to the generation
 * of vendor-specific argument names which may differ to the argument names
 * defined in the BSW.
 */


/*==================[inclusions]=================================================================*/

/* !LINKSTO WDGM.EB.Dbg3,1 */
#include <WdgM_Trace_Stc.h>
#include <Std_Types.h>                                                 /* AUTOSAR standard types */

/* to prevent duplicate declarations of symbols from Rte_WdgM.h by our public
 * headers */
#define WDGM_INTERNAL_USAGE
/* !LINKSTO WDGM.EB.ASR32.WDGM017,1 */
#include <WdgM_BSW.h>                                            /* Declaration of WdgM BSW APIs */
#include <WdgM_Int.h>                                                   /* Internal declarations */

#if (WDGM_EB_INCLUDE_RTE == STD_ON)
#if (WDGM_EB_ENABLE_ASR40_SERVICE_API == STD_ON)

/* !LINKSTO WDGM.EB.ASR32.WDGM017,1 */
#include <WdgM_Rte_Lcfg.h>  /* Declaration of WdgM APIs visible to RTE - consistency enforcement */

/*==================[macros]=====================================================================*/

#if (defined WDGM_EB_RTE_NUM_STATUS)
#error WDGM_EB_RTE_NUM_STATUS already defined
#endif
/* \brief number of status mappings */
#define WDGM_EB_RTE_NUM_STATUS  5U

/* Internal sanity check  that WdgM Global/Local Status values are correctly
 * mapped to Rte status values */
#if ((WDGM_LOCAL_STATUS_OK != 0U) || (WDGM_GLOBAL_STATUS_OK != 0U))
#error WDGM_LOCAL_STATUS_OK/WDGM_GLOBAL_STATUS_OK must equal 0U
#endif
#if ((WDGM_LOCAL_STATUS_FAILED != 1U) || (WDGM_GLOBAL_STATUS_FAILED != 1U))
#error WDGM_LOCAL_STATUS_FAILED/WDGM_GLOBAL_STATUS_FAILED must equal 1U
#endif
#if ((WDGM_LOCAL_STATUS_EXPIRED != 2U) || (WDGM_GLOBAL_STATUS_EXPIRED != 2U))
#error WDGM_LOCAL_STATUS_EXPIRED/WDGM_GLOBAL_STATUS_EXPIRED must equal 2U
#endif
#if (WDGM_GLOBAL_STATUS_STOPPED != 3U) /* There is no WDGM_LOCAL_STATUS_STOPPED */
#error WDGM_GLOBAL_STATUS_STOPPED must equal 3U
#endif
#if ((WDGM_LOCAL_STATUS_DEACTIVATED != 4U) || (WDGM_GLOBAL_STATUS_DEACTIVATED != 4U))
#error WDGM_LOCAL_STATUS_DEACTIVATED/WDGM_GLOBAL_STATUS_DEACTIVATED must equal 4U
#endif

/*==================[type definitions]===========================================================*/

/*==================[external function declarations]=============================================*/

/*==================[internal function declarations]=============================================*/

/*==================[external constants]=========================================================*/

/*==================[internal constants]=========================================================*/

#define WDGM_START_SEC_CONST_8BIT
#define WDGM_START_SEC_CONST_8
#include <MemMap.h>

/* \brief map WdgM global supervision status to the AUTOSAR 4.0 specific
 *  Rte_ModeType_WdgM_ASR40_Mode
 *
 * This mapping table is needed as RTE mode type is generated in alphabetical
 * order whereas the WdgM state has "logical" order */
STATIC CONST(Rte_ModeType_WdgM_ASR40_Mode, WDGM_CONST)
  WdgM_EB_ASR40_RteStatusMap[WDGM_EB_RTE_NUM_STATUS] =
{
  RTE_MODE_WdgM_ASR40_Mode_SUPERVISION_OK,          /* WDGM_GLOBAL/LOCAL_STATUS_OK */
  RTE_MODE_WdgM_ASR40_Mode_SUPERVISION_FAILED,      /* WDGM_GLOBAL/LOCAL_STATUS_FAILED */
  RTE_MODE_WdgM_ASR40_Mode_SUPERVISION_EXPIRED,     /* WDGM_GLOBAL/LOCAL_STATUS_EXPIRED */
  RTE_MODE_WdgM_ASR40_Mode_SUPERVISION_STOPPED,     /* WDGM_GLOBAL_STATUS_STOPPED */
  RTE_MODE_WdgM_ASR40_Mode_SUPERVISION_DEACTIVATED  /* WDGM_GLOBAL/LOCAL_STATUS_DEACTIVATED */
};

#define WDGM_STOP_SEC_CONST_8BIT
#define WDGM_STOP_SEC_CONST_8
#include <MemMap.h>
/* Deviation MISRA-1 <+4> */
#undef WDGM_START_SEC_CONST_8BIT
#undef WDGM_START_SEC_CONST_8
#undef WDGM_STOP_SEC_CONST_8BIT
#undef WDGM_STOP_SEC_CONST_8

/*==================[external data]==============================================================*/

/*==================[internal data]==============================================================*/

/*==================[internal function definitions]==============================================*/

/*==================[external function definitions]==============================================*/

#define WDGM_START_SEC_CODE
#include <MemMap.h>

FUNC(void, WDGM_CODE) WdgM_ASR40_RteGlobalModeSwitch
(
  WdgM_GlobalStatusType GlobalStatus
)
{
  /* !LINKSTO WDGM.EB.Dbg4,1 */
  DBG_WDGM_ASR40_RTEGLOBALMODESWITCH_ENTRY(GlobalStatus);

  /* Return value is ignored because mode switch indication to Rte is compliant to a
   * fire and forget policy from WdgM's point of view. */
  (void)Rte_Switch_WdgM_ASR40_globalMode_currentMode(WdgM_EB_ASR40_RteStatusMap[GlobalStatus]);

  /* !LINKSTO WDGM.EB.Dbg4,1 */
  DBG_WDGM_ASR40_RTEGLOBALMODESWITCH_EXIT(GlobalStatus);
}

FUNC(void, WDGM_CODE) WdgM_ASR40_RteIndividualModeSwitch
(
  WdgM_ASR40_SupervisedEntityIdType SEID,
  WdgM_LocalStatusType              LocalStatus
)
{
  /* !LINKSTO WDGM.EB.Dbg4,1 */
  DBG_WDGM_ASR40_RTEINDIVIDUALMODESWITCH_ENTRY(SEID,LocalStatus);

  /* Return value is ignored because mode switch indication to Rte is compliant to a
   * fire and forget policy from WdgM's point of view. */
  (void)WdgM_ASR40_RteSwitch[SEID](WdgM_EB_ASR40_RteStatusMap[LocalStatus]);

  /* !LINKSTO WDGM.EB.Dbg4,1 */
  DBG_WDGM_ASR40_RTEINDIVIDUALMODESWITCH_EXIT(SEID,LocalStatus);
}

/* !LINKSTO WDGM.EB.ASR32.WDGM109,1 */
/* Deviation MISRA-2 */
FUNC(Std_ReturnType, WDGM_CODE) WdgM_ASR40_Rte_UpdateAliveCounter
(
  WdgM_ASR40_SupervisedEntityIdType SEID
)
{
  Std_ReturnType RetVal;

  /* !LINKSTO WDGM.EB.Dbg4,1 */
  DBG_WDGM_ASR40_RTE_UPDATEALIVECOUNTER_ENTRY(SEID);

  RetVal = WdgM_ASR40_UpdateAliveCounter(SEID);

  /* !LINKSTO WDGM.EB.Dbg4,1 */
  DBG_WDGM_ASR40_RTE_UPDATEALIVECOUNTER_EXIT(RetVal,SEID);
  return RetVal;
}

/* !LINKSTO WDGM.EB.ASR32.WDGM109,1 */
/* Deviation MISRA-2 */
FUNC(Std_ReturnType, WDGM_CODE) WdgM_ASR40_Rte_CheckpointReached
(
  WdgM_ASR40_SupervisedEntityIdType SEID,
  WdgM_ASR40_CheckpointIdType       CheckpointID
)
{
  Std_ReturnType RetVal;

  /* !LINKSTO WDGM.EB.Dbg4,1 */
  DBG_WDGM_ASR40_RTE_CHECKPOINTREACHED_ENTRY(SEID,CheckpointID);

  RetVal = WdgM_ASR40_CheckpointReached(SEID, CheckpointID);

  /* !LINKSTO WDGM.EB.Dbg4,1 */
  DBG_WDGM_ASR40_RTE_CHECKPOINTREACHED_EXIT(RetVal,SEID,CheckpointID);
  return RetVal;
}

#define WDGM_STOP_SEC_CODE
#include <MemMap.h>

#endif /* (WDGM_EB_ENABLE_ASR40_SERVICE_API == STD_ON) */
#endif /* (WDGM_EB_INCLUDE_RTE == STD_ON) */

#if ((WDGM_EB_INCLUDE_RTE == STD_OFF) || \
     (WDGM_EB_ENABLE_ASR40_SERVICE_API == STD_OFF))

/* Avoid empty translation unit according to ISO C90 */
typedef void TSPreventEmptyTranslationUnit;

#endif /* ((WDGM_EB_INCLUDE_RTE == STD_OFF) || (WDGM_EB_ENABLE_ASR40_SERVICE_API == STD_OFF)) */


/*==================[end of file]================================================================*/
