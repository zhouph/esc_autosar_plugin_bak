/**
 * \file
 *
 * \brief AUTOSAR Dcm
 *
 * This file contains the implementation of the AUTOSAR
 * module Dcm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DCM_CFG_H)
#define DCM_CFG_H

/*==================[includes]===================================================================*/

[!AUTOSPACING!]
#include <Std_Types.h>

/*==================[macros]=====================================================================*/
/*------------------[Defensive programming]---------------------------------*/
[!SELECT "DcmDefensiveProgramming"!][!//

#if (defined DCM_DEFENSIVE_PROGRAMMING_ENABLED)
#error DCM_DEFENSIVE_PROGRAMMING_ENABLED is already defined
#endif
/** \brief Defensive programming usage
 **
 ** En- or disables the usage of the defensive programming */
#define DCM_DEFENSIVE_PROGRAMMING_ENABLED   [!//
[!IF "(../DcmConfigSet/*/DcmGeneral/DcmDevErrorDetect  = 'true') and (DcmDefProgEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_PRECONDITION_ASSERT_ENABLED)
#error DCM_PRECONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Precondition assertion usage
 **
 ** En- or disables the usage of precondition assertion checks */
#define DCM_PRECONDITION_ASSERT_ENABLED     [!//
[!IF "(../DcmConfigSet/*/DcmGeneral/DcmDevErrorDetect  = 'true') and (DcmDefProgEnabled = 'true') and (DcmPrecondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_POSTCONDITION_ASSERT_ENABLED)
#error DCM_POSTCONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Postcondition assertion usage
 **
 ** En- or disables the usage of postcondition assertion checks */
#define DCM_POSTCONDITION_ASSERT_ENABLED    [!//
[!IF "(../DcmConfigSet/*/DcmGeneral/DcmDevErrorDetect  = 'true') and (DcmDefProgEnabled = 'true') and (DcmPostcondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_UNREACHABLE_CODE_ASSERT_ENABLED)
#error DCM_UNREACHABLE_CODE_ASSERT_ENABLED is already defined
#endif
/** \brief Unreachable code assertion usage
 **
 ** En- or disables the usage of unreachable code assertion checks */
#define DCM_UNREACHABLE_CODE_ASSERT_ENABLED [!//
[!IF "(../DcmConfigSet/*/DcmGeneral/DcmDevErrorDetect  = 'true') and (DcmDefProgEnabled = 'true') and (DcmUnreachAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_INVARIANT_ASSERT_ENABLED)
#error DCM_INVARIANT_ASSERT_ENABLED is already defined
#endif
/** \brief Invariant assertion usage
 **
 ** En- or disables the usage of invariant assertion checks */
#define DCM_INVARIANT_ASSERT_ENABLED        [!//
[!IF "(../DcmConfigSet/*/DcmGeneral/DcmDevErrorDetect  = 'true') and (DcmDefProgEnabled = 'true') and (DcmInvariantAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_STATIC_ASSERT_ENABLED)
#error DCM_STATIC_ASSERT_ENABLED is already defined
#endif
/** \brief Static assertion usage
 **
 ** En- or disables the usage of static assertion checks */
#define DCM_STATIC_ASSERT_ENABLED           [!//
[!IF "(../DcmConfigSet/*/DcmGeneral/DcmDevErrorDetect  = 'true') and (DcmDefProgEnabled = 'true') and (DcmStaticAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!ENDSELECT!][!//

[!INDENT "0"!][!//
[!IF "count(as:modconf('Dcm')[1]/DcmConfigSet/*/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*/DcmDslProtocolTx[asc:getShortName(.) = ""]) = 0"!]
  [!VAR "SymbolProtocolTxA403Compliant" = "true()"!]
[!ELSE!]
  [!VAR "SymbolProtocolTxA403Compliant" = "false()"!]
[!ENDIF!]

[!LOOP "as:modconf('Dcm')/DcmConfigSet/*/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*"!][!//
  [!VAR "ConnCount" = "0"!][!//
  [!LOOP "DcmDslConnection/*/DcmDslProtocolTx"!][!//
    [!IF "$SymbolProtocolTxA403Compliant"!][!//
      [!VAR "SymbolName" = "asc:getShortName(.)"!][!//
      #if (defined DcmConf_[!"name(.)"!]_[!"$SymbolName"!])
      #error DcmConf_[!"name(.)"!]_[!"$SymbolName"!] already defined
      #endif
      /** \brief Export symbolic name value (AUTOSAR version >= 4.0 rev3) */
      #define DcmConf_[!"name(.)"!]_[!"$SymbolName"!] [!"./DcmDslTxConfirmationPduId"!]U
    [!ENDIF!]
    
    #if (defined DcmConf_[!"name(../../../.)"!]_[!"./@name"!]_[!"num:i($ConnCount)"!])
    #error DcmConf_[!"name(../../../.)"!]_[!"./@name"!]_[!"num:i($ConnCount)"!] already defined
    #endif
    /** \brief Export symbolic name value */
    #define DcmConf_[!"name(../../../.)"!]_[!"@name"!]_[!"num:i($ConnCount)"!] [!"num:i(./DcmDslTxConfirmationPduId)"!]

    #if (!defined DCM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
    #if (defined [!"name(../../../.)"!]_[!"name(.)"!]_[!"num:i($ConnCount)"!])
    #error [!"name(../../../.)"!]_[!"name(.)"!]_[!"num:i($ConnCount)"!] already defined
    #endif
    /** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
    #define [!"name(../../../.)"!]_[!"./@name"!]_[!"num:i($ConnCount)"!] [!"num:i(./DcmDslTxConfirmationPduId)"!]

    #if (defined Dcm_[!"name(../../../.)"!]_[!"name(.)"!]_[!"num:i($ConnCount)"!])
    #error Dcm_[!"name(../../../.)"!]_[!"name(.)"!]_[!"num:i($ConnCount)"!] already defined
    #endif
    /** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) **/
    #define Dcm_[!"name(../../../.)"!]_[!"@name"!]_[!"num:i($ConnCount)"!] [!"num:i(./DcmDslTxConfirmationPduId)"!]
    #endif /* !defined DCM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */

[!VAR "ConnCount" = " $ConnCount +1"!][!// 
  [!ENDLOOP!][!//
[!ENDLOOP!][!//
[!ENDINDENT!][!//

#if (defined DCM_NUM_RX_PDU_ID) /* To prevent double declaration */
#error DCM_NUM_RX_PDU_ID already defined
#endif /* if (defined DCM_NUM_RX_PDU_ID) */

[!LOOP "DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*/DcmDslProtocolRx/*"!][!//
#if (defined DcmConf_[!"name(../../../../.)"!]_[!"./@name"!])
#error DcmConf_[!"name(../../../../.)"!]_[!"./@name"!] already defined
#endif
/** \brief Export symbolic name value */
#define DcmConf_[!"name(../../../../.)"!]_[!"@name"!] [!"./DcmDslProtocolRxPduId"!]

#if (!defined DCM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(../../../../.)"!]_[!"name(.)"!])
#error [!"name(../../../../.)"!]_[!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"name(../../../../.)"!]_[!"./@name"!] [!"./DcmDslProtocolRxPduId"!]

#if (defined Dcm_[!"name(../../../../.)"!]_[!"name(.)"!])
#error Dcm_[!"name(../../../../.)"!]_[!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) **/
#define Dcm_[!"name(../../../../.)"!]_[!"@name"!] [!"./DcmDslProtocolRxPduId"!]
#endif /* !defined DCM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//

/** \brief Definition of number of reception(Rx) Pdu Ids **/
#define DCM_NUM_RX_PDU_ID                    [!//
[!"num:i( count( DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*/DcmDslProtocolRx/*))"!]U

#if (defined DCM_NUM_PROTOCOL)       /* To prevent double declaration */
#error DCM_NUM_PROTOCOL already defined
#endif /* if (defined DCM_NUM_PROTOCOL) */

/** \brief Definition of number of protocols  **/
#define DCM_NUM_PROTOCOL                     [!//
[!"num:i( count( DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*))"!]U

[!LOOP "DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*"!][!//
#if (defined DcmConf_[!"./@name"!])
#error DcmConf_[!"./@name"!] already defined
#endif
/** \brief Export symbolic name value */
#define DcmConf_[!"@name"!] [!"./DcmDslProtocolID"!]

#if (!defined DCM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"./@name"!] [!"./DcmDslProtocolID"!]

#if (defined Dcm_[!"name(.)"!])
#error Dcm_[!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) **/
#define Dcm_[!"@name"!] [!"./DcmDslProtocolID"!]
#endif /* !defined DCM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//

#if (defined DCM_NUM_BUFFER) /* To prevent double declaration */
#error DCM_NUM_BUFFER already defined
#endif /* if (defined DCM_NUM_BUFFER) */

/** \brief Definition of number of buffers **/
#define DCM_NUM_BUFFER                       [!//
[!"num:i( count( DcmConfigSet/*[1]/DcmDsl/DcmDslBuffer/*))"!]U


#if (defined DCM_NUM_TX_PDU_ID) /* To prevent double declaration */
#error DCM_NUM_TX_PDU_ID already defined
#endif /* if (defined DCM_NUM_TX_PDU_ID) */

/** \brief Definition of number of Transmission(Tx) Pdu Ids **/
#define DCM_NUM_TX_PDU_ID                    [!//
[!"num:i( count( DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*/DcmDslProtocolTx))"!]U

#if (defined DCM_NUM_CONF_PDU_ID) /* To prevent double declaration */
#error DCM_NUM_CONF_PDU_ID already defined
#endif /* if (defined DCM_NUM_CONF_PDU_ID) */

/** \brief Definition of number of Transmission(Tx) Confirmation Pdu Ids **/
#define DCM_NUM_CONF_PDU_ID                    [!//
[!"num:i( count( DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*/DcmDslProtocolTx))"!]U

#if (defined DCM_NUM_SID_TABLE) /* To prevent double declaration */
#error DCM_NUM_SID_TABLE already defined
#endif /* if (defined DCM_NUM_SID_TABLE) */

/** \brief Number Service ID table entries configured **/
#define DCM_NUM_SID_TABLE                    [!//
[!"num:integer( count( DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*))"!]U

#if (defined DCM_NUM_SES_TAB_ENTRY) /* To prevent double declaration */
#error DCM_NUM_SES_TAB_ENTRY already defined
#endif /* if (defined DCM_NUM_SES_TAB_ENTRY) */

/** \brief Number of configured diagnostic sessions **/
#define DCM_NUM_SES_TAB_ENTRY                [!//
[!"num:i(count(DcmConfigSet/*[1]/DcmDsp/DcmDspSession/*/DcmDspSessionRow/*))"!]U

#if (defined DCM_NUM_SEC_TAB_ENTRY) /* To prevent double declaration */
#error DCM_NUM_SEC_TAB_ENTRY already defined
#endif /* if (defined DCM_NUM_SEC_TAB_ENTRY) */

/** \brief Number of configured security levels **/
#define DCM_NUM_SEC_TAB_ENTRY                [!//
[!"num:integer( count( DcmConfigSet/*[1]/DcmDsp/DcmDspSecurity/DcmDspSecurityRow/*))"!]U

#if (defined DCM_NUM_ROUTINES) /* To prevent double declaration */
#error DCM_NUM_ROUTINES already defined
#endif /* if (defined DCM_NUM_ROUTINES) */

[!VAR "RoutineCount" = "0"!]
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*"!][!//
  [!IF "DcmDspRoutineUsed = 'true'"!]
    [!VAR "RoutineCount" = "$RoutineCount + 1"!]
  [!ENDIF!]
[!ENDLOOP!]

/** \brief Number of Routines configured **/
#define DCM_NUM_ROUTINES [!"num:integer($RoutineCount)"!]U

[!VAR "routineInfoCount" = "0"!][!//
[!VAR "routineInfoRefList"="'#'"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*"!][!//
  [!IF "not(contains($routineInfoRefList, concat('#', name(node:ref(DcmDspRoutineInfoRef)), '#')))"!]
    [!VAR "routineInfoCount" = "$routineInfoCount + 1"!][!//
    [!VAR "routineInfoRefList" = "concat($routineInfoRefList, name(node:ref(DcmDspRoutineInfoRef)), '#')"!]
  [!ENDIF!]
[!ENDLOOP!][!//
[!//

#if (defined DCM_NUM_ROUTINE_INFO) /* To prevent double declaration */
#error DCM_NUM_ROUTINE_INFO already defined
#endif /* if (defined DCM_NUM_ROUTINE_INFO) */

/** \brief Number of DcmDspRoutineInfos referenced by DcmDspRoutines **/
#define DCM_NUM_ROUTINE_INFO                [!//
[!"num:integer( $routineInfoCount)"!]U

#if (defined DCM_NUM_COMCONTROL_ALL_CHANNEL) /* To prevent double declaration */
#error DCM_NUM_COMCONTROL_ALL_CHANNEL already defined
#endif /* if (defined DCM_NUM_COMCONTROL_ALL_CHANNEL) */

/** \brief Number of Communication Control All channels **/
#define DCM_NUM_COMCONTROL_ALL_CHANNEL                [!//
[!"num:integer( count( DcmConfigSet/*[1]/DcmDsp/DcmDspComControl/DcmDspComControlAllChannel/*))"!]U

#if (defined DCM_NUM_COMCONTROL_SPECIFIC_CHANNEL) /* To prevent double declaration */
#error DCM_NUM_COMCONTROL_SPECIFIC_CHANNEL already defined
#endif /* if (defined DCM_NUM_COMCONTROL_SPECIFIC_CHANNEL) */

/** \brief Number of Communication Control Specific channels **/
#define DCM_NUM_COMCONTROL_SPECIFIC_CHANNEL                [!//
[!"num:integer( count( DcmConfigSet/*[1]/DcmDsp/DcmDspComControl/DcmDspComControlSpecificChannel/*))"!]U

[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '47']) > 0"!][!//
#if (defined DCM_DSP_RETURN_CONTROLTOECU_TIMEOUT) /* To prevent double declaration */
#error DCM_DSP_RETURN_CONTROLTOECU_TIMEOUT already defined
#endif /* if (defined DCM_DSP_RETURN_CONTROLTOECU_TIMEOUT) */

/** \brief Processing time allowed for returncontroltoecu completion **/
#define DCM_DSP_RETURN_CONTROLTOECU_TIMEOUT                [!//
[!"num:i( DcmConfigSet/*[1]/DcmDsp/DcmDspReturnControlToEcuTimeout div DcmConfigSet/*[1]/DcmGeneral/DcmTaskTime)"!]U

#if (defined DCM_DSP_RETURN_CONTROLTOECU_TIMEOUT_REPORT) /* To prevent double declaration */
#error DCM_DSP_RETURN_CONTROLTOECU_TIMEOUT_REPORT already defined
#endif /* if (defined DCM_DSP_RETURN_CONTROLTOECU_TIMEOUT_REPORT) */

/** \brief Det error report for returnControlToEcu is enables or not **/
#define DCM_DSP_RETURN_CONTROLTOECU_TIMEOUT_REPORT                [!//
[!IF "num:i(DcmConfigSet/*[1]/DcmDsp/DcmDspReturnControlToEcuTimeout) = '0'"!]STD_OFF[!ELSE!]STD_ON[!ENDIF!]
[!ENDIF!][!//

[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '17']) > 0"!][!//
#if (defined DCM_DSP_POWER_DOWN_TIME) /* To prevent double declaration */
#error DCM_DSP_POWER_DOWN_TIME already defined
#endif /* if (defined DCM_DSP_POWER_DOWN_TIME) */

/** \brief Minimum stand-by time the server will remain in the power-down sequence **/
#define DCM_DSP_POWER_DOWN_TIME                [!//
[!"num:i(DcmConfigSet/*[1]/DcmDsp/DcmDspPowerDownTime)"!]U
[!ENDIF!][!//

#if (defined DCM_PAGEDBUFFER_ENABLED)   /* To prevent double declaration */
#error DCM_PAGEDBUFFER_ENABLED already defined
#endif /* if (defined DCM_PAGEDBUFFER_ENABLED) */

/** \brief Definition if paged buffer handling is enabled **/
#define DCM_PAGEDBUFFER_ENABLED              [!//
[!IF "DcmConfigSet/*[1]/DcmPageBufferCfg/DcmPagedBufferEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]


#if (defined DCM_PAGED_BUFFER_TIMEOUT) /* To prevent double declaration */
#error DCM_PAGED_BUFFER_TIMEOUT already defined
#endif /* if (defined DCM_PAGED_BUFFER_TIMEOUT) */

/** \brief Definition of Timeout for Paged buffer transmission.  This timeout
 ** is counted in units of DCM_TASK_TIME **/
#define DCM_PAGED_BUFFER_TIMEOUT             [!//
[!IF "node:exists( DcmConfigSet/*[1]/DcmPageBufferCfg/DcmPagedBufferTimeout)"!][!//
[!"num:i( DcmConfigSet/*[1]/DcmPageBufferCfg/DcmPagedBufferTimeout div DcmConfigSet/*[1]/DcmGeneral/DcmTaskTime)"!]U
[!ELSE!][!//
0U
[!ENDIF!][!//

#if (defined DCM_DEV_ERROR_DETECT)
#error DCM_DEV_ERROR_DETECT already defined
#endif
/** \brief Switch, indicating if development error detection is activated for
 ** DCM */
#define DCM_DEV_ERROR_DETECT                 [!//
[!IF "DcmConfigSet/*[1]/DcmGeneral/DcmDevErrorDetect = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_DIAG_RESP_MAX_NUM_RESP_PEND )  /* To prevent double declaration */
#error DCM_DIAG_RESP_MAX_NUM_RESP_PEND already defined
#endif /* #if (defined DCM_DIAG_RESP_MAX_NUM_RESP_PEND ) */

/** \brief Maximum number of Pending responses possible **/
#define DCM_DIAG_RESP_MAX_NUM_RESP_PEND      [!//
[!"num:i( DcmConfigSet/*[1]/DcmDsl/DcmDslDiagResp/DcmDslDiagRespMaxNumRespPend)"!]U

#if (defined DCM_VERSION_INFO_API)
#error DCM_VERSION_INFO_API already defined
#endif
/** \brief Switch, indicating if Dcm_GetVersionInfo() API is activated for
 ** DCM */
#define DCM_VERSION_INFO_API                 [!//
[!IF "DcmConfigSet/*[1]/DcmGeneral/DcmVersionInfoApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_INCLUDE_RTE)
#error DCM_INCLUDE_RTE already defined
#endif
/** \brief Switch, indicating if RTE is available and can be used from DCM */
#define DCM_INCLUDE_RTE                      [!//
[!IF "DcmConfigSet/*[1]/DcmGeneral/DcmRteUsage = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_SECURITYACCESS_STRICT)
#error DCM_SECURITYACCESS_STRICT already defined
#endif
/** \brief Switch, indicating whether the strict SecurityAccess mechanism is activated for the DCM */
#define DCM_SECURITYACCESS_STRICT                      [!//
[!IF "DcmConfigSet/*[1]/DcmGeneral/DcmRequestSeedAfterFailedCompareKey = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_MODEDECLARATION_SUPPORT)
#error DCM_MODEDECLARATION_SUPPORT already defined
#endif
/** \brief Switch, indicating if Mode declaration support is enabled and SchM switch APIs
 ** are generated by RTE to be used for notifying mode switches to mode users */
#define DCM_MODEDECLARATION_SUPPORT                      [!//
[!IF "DcmConfigSet/*[1]/DcmGeneral/DcmModeDeclarationSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_MODE_DECLARATION_OEM_BL) /* To prevent double declaration */
#error DCM_MODE_DECLARATION_OEM_BL already defined
#endif /* if (defined DCM_MODE_DECLARATION_OEM_BL) */

/** \brief Indicates, if OEM boot support and mode declaration is enabled */
#define DCM_MODE_DECLARATION_OEM_BL           [!//
[!IF "(count(DcmConfigSet/*[1]/DcmDsp/DcmDspSession/*/DcmDspSessionRow/*[DcmDspSessionForBoot = 'DCM_OEM_BOOT']) > 0) and (DcmConfigSet/*[1]/DcmGeneral/DcmModeDeclarationSupport = 'true')"!] STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_MODE_DECLARATION_SYS_BL) /* To prevent double declaration */
#error DCM_MODE_DECLARATION_SYS_BL already defined
#endif /* if (defined DCM_MODE_DECLARATION_SYS_BL) */

/** \brief Indicates, if SYS boot support and mode declaration is enabled */
#define DCM_MODE_DECLARATION_SYS_BL           [!//
[!IF "(count(DcmConfigSet/*[1]/DcmDsp/DcmDspSession/*/DcmDspSessionRow/*[DcmDspSessionForBoot = 'DCM_SYS_BOOT']) > 0) and (DcmConfigSet/*[1]/DcmGeneral/DcmModeDeclarationSupport = 'true')"!] STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!VAR "ChannelList"="'#'"!]
[!VAR "LookUpTabSize" = "0"!]
  [!LOOP "DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*/DcmDslProtocolRx/*"!]
    [!IF "not( contains( $ChannelList, concat( '#', ./DcmDslProtocolRxChannelId,'#')))"!]
      [!VAR "LookUpTabSize" = "$LookUpTabSize + 1"!]
      [!VAR "ChannelList" = "concat( $ChannelList,'#',./DcmDslProtocolRxChannelId,'#')"!]
    [!ENDIF!]
  [!ENDLOOP!]
  [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspComControl/DcmDspComControlAllChannel/*"!]
    [!IF "not(contains($ChannelList, concat('#', as:ref(DcmDspAllComMChannelRef)/ComMChannelId, '#')))"!]
      [!VAR "LookUpTabSize" = "$LookUpTabSize + 1"!]
      [!VAR "ChannelList" = "concat($ChannelList, as:ref(DcmDspAllComMChannelRef)/ComMChannelId, '#')"!]
    [!ENDIF!]
  [!ENDLOOP!]
  [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspComControl/DcmDspComControlSpecificChannel/*"!][!//
    [!IF "not(contains($ChannelList, concat('#', as:ref(DcmDspSpecificComMChannelRef)/ComMChannelId, '#')))"!]
      [!VAR "LookUpTabSize" = "$LookUpTabSize + 1"!]
      [!VAR "ChannelList" = "concat($ChannelList, as:ref(DcmDspSpecificComMChannelRef)/ComMChannelId, '#')"!]
    [!ENDIF!]
  [!ENDLOOP!]

#if (defined DCM_COMCONTROL_LOOKUP_SIZE) /* To prevent double declaration */
#error DCM_COMCONTROL_LOOKUP_SIZE already defined
#endif /* if (defined DCM_COMCONTROL_LOOKUP_SIZE) */

/** \brief Size of array Dcm_ComControlLookUpTable[] */
#define DCM_COMCONTROL_LOOKUP_SIZE [!"num:integer( $LookUpTabSize)"!]U

/** \brief Switch, indicating availability of timing values in positive response of 0x10 and JumpFromBL */
#define DCM_TIMING_VALUES_IN_POSITIVE_RESPONSE                      [!//
[!IF "DcmConfigSet/*[1]/DcmDsp/DcmDspSessionControlTiming = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_ASYNC_SERVICE_HANDLER)
#error DCM_ASYNC_SERVICE_HANDLER already defined
#endif
/** \brief Switch, indicating if Async service handler is available */
#define DCM_ASYNC_SERVICE_HANDLER                      [!//
[!IF "DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*/DcmAsyncServiceExecution ='true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_ASYNC_DID_SERVICES)
#error DCM_ASYNC_DID_SERVICES already defined
#endif
/** \brief Switch, indicating if either of services 0x22, 0x2E or 0x2F use an Async service handler */
#define DCM_ASYNC_DID_SERVICES                      [!//
[!IF "DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*/DcmAsyncServiceExecution[../DcmDsdSidTabServiceId=34 or ../DcmDsdSidTabServiceId=46  or ../DcmDsdSidTabServiceId=47] ='true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!/* readAccWithBlkIdInterface holds true if there is any DID with read access and atleast one
     signal of it with data access interface configured as block-id. This will be used for the
     generation of the macro DCM_READ_DID_BLOCK_ID_CONFIGURED */!][!//

[!VAR "readAccWithBlkIdInterface" = "'false'"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
  [!IF "((node:exists(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidRead)) and count(../../../../../DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '34' and not(node:exists(DcmDsdSidTabFnc))]) > 0) or ( (node:exists(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidControl)) and (count(../../../../../DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '47' and not(node:exists(DcmDsdSidTabFnc))]) > 0) ) or ( not(node:exists(DcmDspDidExtRoe)) and count(../../../../../DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='134' and not(node:exists(DcmDsdSidTabFnc))]/*/*[DcmDsdSubServiceId='3' and not(node:exists(DcmDsdSubServiceFnc))]) > 0 ) or ( not(node:exists(DcmDspDidExtRoe)) and count(../../../../../DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='134' and not(node:exists(DcmDsdSidTabFnc))]) > 0 and (name(.) = name(node:ref(../../DcmDspRoe//DcmDspRoePreconfEventDetails/*))))"!][!//
    [!LOOP "DcmDspDidSignal/*"!][!//
      [!SELECT "node:ref(DcmDspDidDataRef)"!][!//
        [!IF "DcmDspDataUsePort = 'USE_BLOCK_ID'"!][!//
          [!VAR "readAccWithBlkIdInterface" = "'true'"!][!//
          [!BREAK!][!//
        [!ENDIF!][!//
      [!ENDSELECT!][!//
    [!ENDLOOP!][!//
    [!IF "$readAccWithBlkIdInterface"!]
      [!BREAK!][!//
    [!ENDIF!][!//
  [!ENDIF!][!//
[!ENDLOOP!][!//

#if (defined DCM_READ_DID_BLOCK_ID_CONFIGURED) /* To prevent double declaration */
#error DCM_READ_DID_BLOCK_ID_CONFIGURED already defined
#endif /* if (defined DCM_READ_DID_BLOCK_ID_CONFIGURED) */

/** \brief Indicates, if a DID is configured with read access and having signal with Data Access
 **  Interface as block-id */
#define DCM_READ_DID_BLOCK_ID_CONFIGURED           [!//
[!IF "($readAccWithBlkIdInterface)"!] STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_WRITE_DID_BLOCK_ID_CONFIGURED) /* To prevent double declaration */
#error DCM_WRITE_DID_BLOCK_ID_CONFIGURED already defined
#endif /* if (defined DCM_WRITE_DID_BLOCK_ID_CONFIGURED) */

[!/* writeAccWithBlkIdInterface holds true if there is any DID with write access and atleast one
     signal of it with data access interface configured as block-id. This will be used for the
     generation of the macro DCM_WRITE_DID_BLOCK_ID_CONFIGURED */!][!//

[!VAR "writeAccWithBlkIdInterface" = "'false'"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
  [!IF "(node:exists(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidWrite)) and (count(../../../../../DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '46' and not(node:exists(DcmDsdSidTabFnc))]) > 0)"!][!//
    [!LOOP "DcmDspDidSignal/*"!][!//
      [!SELECT "node:ref(DcmDspDidDataRef)"!][!//
        [!IF "DcmDspDataUsePort = 'USE_BLOCK_ID'"!][!//
          [!VAR "writeAccWithBlkIdInterface" = "'true'"!][!//
            [!BREAK!][!//
        [!ENDIF!][!//
      [!ENDSELECT!][!//
    [!ENDLOOP!][!//
    [!IF "$writeAccWithBlkIdInterface"!]
      [!BREAK!][!//
    [!ENDIF!][!//
  [!ENDIF!][!//
[!ENDLOOP!][!//

/** \brief Indicates, if a DID is configured with write access and having signal with Data Access
 **  Interface as block-id */
#define DCM_WRITE_DID_BLOCK_ID_CONFIGURED           [!//
[!IF "($writeAccWithBlkIdInterface)"!] STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_JUMP_TO_BL_ENABLED) /* To prevent double declaration */
#error DCM_JUMP_TO_BL_ENABLED already defined
#endif /* if (defined DCM_JUMP_TO_BL_ENABLED) */

/** \brief Indicates, if SYS or OEM boot support is enabled */
#define DCM_JUMP_TO_BL_ENABLED           [!//
[!IF "(count(DcmConfigSet/*[1]/DcmDsp/DcmDspSession/*/DcmDspSessionRow/*[DcmDspSessionForBoot = 'DCM_OEM_BOOT']) > 0) or (count(DcmConfigSet/*[1]/DcmDsp/DcmDspSession/*/DcmDspSessionRow/*[DcmDspSessionForBoot = 'DCM_SYS_BOOT']) > 0)"!] STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_ENDIANNESS_CONVERSION) /* To prevent double declaration */
#error DCM_ENDIANNESS_CONVERSION already defined
#endif /* if (defined DCM_ENDIANNESS_CONVERSION) */
/** \brief Indicates, if endianness conversion is enabled */
#define DCM_ENDIANNESS_CONVERSION           [!//
[!IF "count(DcmConfigSet/*/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*[DcmDslProtocolEndiannessConvEnabled = 'true']) > 0"!] STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_GET_SIZEOFEDRBYDTC_OPTIMIZATION)
#error DCM_GET_SIZEOFEDRBYDTC_OPTIMIZATION already defined
#endif
/** \brief Switch, for the optimized service handling of ReadDTCInformation(0x19),
 ** Subfunction 0x06.
 ** Handling of sub service reportDTCExtendedDataRecordByDTCNumber (0x06) of service
 ** ReadDTCInformation (0x19) is optimized if Dem provides the size of UDS Extended data record
 ** number along with the extended data bytes size (according to AUTOSAR R4.1) */
#define DCM_GET_SIZEOFEDRBYDTC_OPTIMIZATION                 [!//
[!IF "DcmConfigSet/*[1]/DcmGeneral/DcmGetSizeOfExtendedDataRecordByDTCOptimization  = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_RTE_TABLE_DID_SERVICES_SIZE)
#error DCM_RTE_TABLE_DID_SERVICES_SIZE already defined
#endif
/** \brief Did Services count */
#define DCM_RTE_TABLE_DID_SERVICES_SIZE   [!"num:i(count(DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*))"!]U

#if (defined DCM_RTE_TABLE_ROUTINE_SERVICES_SIZE)
#error DCM_RTE_TABLE_ROUTINE_SERVICES_SIZE already defined
#endif
/** \brief Routine Services count */
#define DCM_RTE_TABLE_ROUTINE_SERVICES_SIZE   [!"num:i(count(DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*))"!]U

[!IF "DcmConfigSet/*[1]/DcmGeneral/DcmRteUsage = 'true'"!]
#if (defined DCM_RTE_TABLE_CBK_REQUEST_SIZE)
#error DCM_RTE_TABLE_CBK_REQUEST_SIZE already defined
#endif
/** \brief Callback Request count */
#define DCM_RTE_TABLE_CBK_REQUEST_SIZE   [!"num:i(count(DcmConfigSet/*[1]/DcmDsl/DcmDslCallbackDCMRequestService/*))"!]U

[!ENDIF!]

#if (defined DCM_HSM_INST_MULTI_ENABLED)
#error DCM_HSM_INST_MULTI_ENABLED already defined
#endif
/** \brief Switch, indicating if multiple instances support is enabled */
#define DCM_HSM_INST_MULTI_ENABLED           [!//
[!IF "num:i( count( DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*)) > 1"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_TASK_TIME) /* To prevent double declaration */
#error DCM_TASK_TIME already defined
#endif /* if (defined DCM_TASK_TIME) */

/** \brief Definition for periodic cyclic task time in milliseconds */
#define DCM_TASK_TIME             [!//
[!"num:i(DcmConfigSet/*[1]/DcmGeneral/DcmTaskTime * 1000)"!]U

[!/*ChannelID holds the different 'DcmDslProtocolRxChannelId' configured. This will use for the
generation of the macro DCM_NUM_CHANNELID*/!][!//
[!VAR "ChannelID" =  "' '"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*/DcmDslProtocolRx/*"!][!//
  [!IF "not( contains( $ChannelID, concat( ' ', ./DcmDslProtocolRxChannelId,' ')))"!][!//
    [!VAR "ChannelID" = "concat( $ChannelID,' ',./DcmDslProtocolRxChannelId,' ')"!][!//
  [!ENDIF!][!//
[!ENDLOOP!][!//

/** \brief Definition of number of channel Identifiers configured. */
#define DCM_NUM_CHANNELID [!"num:i(count(text:split($ChannelID)))"!]U

#if (defined DCM_NUM_DID) /* To prevent double declaration */
#error DCM_NUM_DID already defined
#endif /* if (defined DCM_NUM_DID) */

/** \brief Number of DID configured */
#define DCM_NUM_DID             [!//
[!"num:i(count(DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']))"!]U

#if (defined DCM_ROE_PRECONF_SUPPORT) /* To prevent double declaration */
#error DCM_ROE_PRECONF_SUPPORT already defined
#endif /* if (defined DCM_ROE_PRECONF_SUPPORT) */

/** \brief Indicates, if pre-configuration support is enabled */
#define DCM_ROE_PRECONF_SUPPORT           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsp/DcmDspRoe/DcmDspRoePreconfEvent/*) > 0"!] STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_ROE_PRECONF_ONCHANGEOFDID_SUPPORT)       /* To prevent double declaration */
#error DCM_ROE_PRECONF_ONCHANGEOFDID_SUPPORT already defined
#endif /* if (defined DCM_ROE_PRECONF_ONCHANGEOFDID_SUPPORT) */

/** \brief Indicates, if OnChangeOfDid event is configured  **/
#define DCM_ROE_PRECONF_ONCHANGEOFDID_SUPPORT    [!//
[!IF "num:i( count( DcmConfigSet/*[1]/DcmDsp/DcmDspRoe/DcmDspRoePreconfEvent/*/DcmDspRoePreconfEventDetails = DcmDspRoePreconfOnChangeOfDataIdentifierDID)) > 0"!] STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_NUM_PRECONF_EVENT)
#error DCM_NUM_PRECONF_EVENT already defined
#endif /* if (defined DCM_NUM_PRECONF_EVENT) */

/** \brief Number of pre configured events */
#define DCM_NUM_PRECONF_EVENT   [!//
[!"num:i(count(DcmConfigSet/*[1]/DcmDsp/DcmDspRoe/DcmDspRoePreconfEvent/*))"!]U

#if (defined DCM_DSP_USE_MEMORY_ID) /* To prevent double declaration */
#error DCM_DSP_USE_MEMORY_ID already defined
#endif /* if (defined DCM_DSP_USE_MEMORY_ID) */

/** \brief Indicates presence of multiple memory devices to read/write from memory */
#define DCM_DSP_USE_MEMORY_ID             [!//
[!IF "DcmConfigSet/*[1]/DcmDsp/DcmDspMemory/DcmDspUseMemoryId = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_DSP_PRECONF_ADDRESS_LENGTH_FORMATIDENTIFIER)
#error DCM_DSP_PRECONF_ADDRESS_LENGTH_FORMATIDENTIFIER already defined
#endif /* if (defined DCM_DSP_PRECONF_ADDRESS_LENGTH_FORMATIDENTIFIER) */

/** \brief Indicates AddressAndLengthFormatIdentifier is configured or not */
#define DCM_DSP_PRECONF_ADDRESS_LENGTH_FORMATIDENTIFIER             [!//
[!IF "node:exists(DcmConfigSet/*[1]/DcmDsp/DcmDspMemory/DcmDspAddressAndLengthFormatIdentifier)"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!IF "node:exists(DcmConfigSet/*[1]/DcmDsp/DcmDspMemory/DcmDspAddressAndLengthFormatIdentifier)"!][!//

#if (defined DCM_NUM_ADDR_LENGTH_FORMAT_IDENTIFIER)
#error DCM_NUM_ADDR_LENGTH_FORMAT_IDENTIFIER already defined
#endif /* if (defined DCM_NUM_ADDR_LENGTH_FORMAT_IDENTIFIER) */

/** \brief number of AddressAndLengthFormatIdentifiers supported */
#define DCM_NUM_ADDR_LENGTH_FORMAT_IDENTIFIER  [!//
[!"num:i(count(DcmConfigSet/*[1]/DcmDsp/DcmDspMemory/DcmDspAddressAndLengthFormatIdentifier/DcmDspSupportedAddressAndLengthFormatIdentifier/*))"!]U

[!ENDIF!]

#if (defined DCM_NUM_MEMORY_IDINFO)
#error DCM_NUM_MEMORY_IDINFO defined
#endif /* if (defined DCM_NUM_MEMORY_IDINFO) */

/** \brief Number of MemoryIdInfos configured */
#define DCM_NUM_MEMORY_IDINFO   [!//
[!"num:i(count(DcmConfigSet/*[1]/DcmDsp/DcmDspMemory/DcmDspMemoryIdInfo/*))"!]U

#if (defined DCM_NUM_DID_DATA) /* To prevent double declaration */
#error DCM_NUM_DID_DATA already defined
#endif /* if (defined DCM_NUM_DID_DATA) */

/** \brief Number of DID Datas configured */
#define DCM_NUM_DID_DATA             [!//
[!"num:i(count(DcmConfigSet/*[1]/DcmDsp/DcmDspData/*))"!]U

#if (defined DCM_DID_REF_COUNT) /* To prevent double declaration */
#error DCM_DID_REF_COUNT already defined
#endif /* if (defined DCM_DID_REF_COUNT) */

/** \brief Total number of references of each DID */
#define DCM_DID_REF_COUNT             [!//
[!"num:i(count(DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']/DcmDspDidRef/*))"!]U

#if (defined DCM_READ_DID_MAX) /* To prevent double declaration */
#error DCM_READ_DID_MAX already defined
#endif /* if (defined DCM_DID_REFS) */

/** \brief Maximum number of Dids that can be requested in a single read */
#define DCM_READ_DID_MAX             [!//
[!"num:i(DcmConfigSet/*[1]/DcmDsp/DcmDspMaxDidToRead)"!]U

#if (defined DCM_DATA_MAX_SIZE) /* To prevent double declaration */
#error DCM_DATA_MAX_SIZE already defined
#endif /* if (defined DCM_DATA_MAX_SIZE) */

[!IF "count(DcmConfigSet/*[1]/DcmDsp/DcmDspData/*) > 0"!][!//
[!VAR "maxsize" = "num:max(DcmConfigSet/*[1]/DcmDsp/DcmDspData/*/DcmDspDataSize)"!][!//
[!VAR "wordsize" = "2"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspData/*"!]
[!IF "((DcmDspDataType = 'SINT32') or (DcmDspDataType = 'UINT32'))"!][!//
[!VAR "wordsize" = "4"!][!//
[!ENDIF!]
[!ENDLOOP!][!//
[!ELSE!][!//
[!VAR "maxsize" = "0"!][!//
[!ENDIF!][!//
/** \brief Maximum size of the signal configured in bytes */
[!IF "DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslProtocolEndiannessConvEnabled ='true'"!][!//
[!IF "count(DcmConfigSet/*[1]/DcmDsp/DcmDspData/*) > 0"!][!//
[!VAR "bytes" = "$wordsize"!][!//
[!IF "((num:i(($maxsize + 7) div 8)) mod $wordsize) > 0"!][!//
[!VAR "bytes" = "$bytes - ((num:i(($maxsize + 7) div 8)) mod $wordsize)"!][!//
[!ELSE!][!//
[!VAR "bytes" = "0"!][!//
[!ENDIF!][!//
[!ELSE!][!//
[!VAR "bytes" = "0"!][!//
[!ENDIF!][!//
#define DCM_DATA_MAX_SIZE       [!//
[!"num:i((($maxsize + 7) div 8) + $bytes)"!]U
[!ELSE!][!//
#define DCM_DATA_MAX_SIZE      [!//
[!"num:i(($maxsize + 7) div 8)"!]U
[!ENDIF!]

#if (defined DCM_REQUEST_MANUFACTURER_NOTIFICATION_ENABLED)
#error DCM_REQUEST_MANUFACTURER_NOTIFICATION_ENABLED already defined
#endif
/** \brief Switch, indicating if Service request manufacturer notification is available */
#define DCM_REQUEST_MANUFACTURER_NOTIFICATION_ENABLED       [!//
[!IF "DcmConfigSet/*[1]/DcmGeneral/DcmRequestManufacturerNotificationEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_REQUEST_SUPPLIER_NOTIFICATION_ENABLED)
#error DCM_REQUEST_SUPPLIER_NOTIFICATION_ENABLED already defined
#endif
/** \brief Switch, indicating if Service request supplier notification is available */
#define DCM_REQUEST_SUPPLIER_NOTIFICATION_ENABLED       [!//
[!IF "DcmConfigSet/*[1]/DcmGeneral/DcmRequestSupplierNotificationEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_RTE_TABLE_REQUEST_MANUFACTURER_SIZE)
#error DCM_RTE_TABLE_REQUEST_MANUFACTURER_SIZE already defined
#endif
/** \brief Service request manufacturer notification count */
#define DCM_RTE_TABLE_REQUEST_MANUFACTURER_SIZE   [!"num:i(count(DcmConfigSet/*[1]/DcmDsl/DcmDslServiceRequestManufacturerNotification/*))"!]U

#if (defined DCM_RTE_TABLE_REQUEST_SUPPLIER_SIZE)
#error DCM_RTE_TABLE_REQUEST_SUPPLIER_SIZE already defined
#endif
/** \brief Service request supplier notification count */
#define DCM_RTE_TABLE_REQUEST_SUPPLIER_SIZE   [!"num:i(count(DcmConfigSet/*[1]/DcmDsl/DcmDslServiceRequestSupplierNotification/*))"!]U

[!VAR "CommnBuff" = "'STD_OFF'"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*"!][!//
  [!VAR "TxBuff" = "node:value(DcmDslProtocolTxBufferID)"!][!//
  [!LOOP "DcmDslConnection/*/DcmDslProtocolRx/*"!][!//
    [!IF "$TxBuff = node:value(DcmDslProtocolRxBufferID)"!][!//
      [!VAR "CommnBuff" = "'STD_ON'"!][!//
      [!BREAK!][!//
    [!ENDIF!][!//
  [!ENDLOOP!][!//
[!ENDLOOP!][!//

/** \brief Macro to check that if common buffer is used for Rx and Tx */
#define DCM_COMMON_BUFF_CONFIGURED [!"$CommnBuff"!]

#if (DCM_INCLUDE_RTE == STD_OFF)
/*------------------[definitions of session levels]----------------------------------------------*/
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspSession/*/DcmDspSessionRow/*"!][!//
#if (defined DcmConf_[!"./@name"!])
#error DcmConf_[!"./@name"!] already defined
#endif
/** \brief Export symbolic name value */
#define DcmConf_[!"@name"!] [!"num:i(./DcmDspSessionLevel)"!]U

#if (!defined DCM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"./@name"!] [!"num:i(./DcmDspSessionLevel)"!]U

#if (defined Dcm_[!"name(.)"!])
#error Dcm_[!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) **/
#define Dcm_[!"@name"!] [!"num:i(./DcmDspSessionLevel)"!]U
#endif /* !defined DCM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//

/*------------------[definitions of security levels]----------------------------------------------*/
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspSecurity/DcmDspSecurityRow/*"!][!//
#if (defined DcmConf_[!"./@name"!])
#error DcmConf_[!"./@name"!] already defined
#endif
/** \brief Export symbolic name value */
#define DcmConf_[!"@name"!] [!"num:i(./DcmDspSecurityLevel)"!]U
#if (!defined DCM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)

#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"./@name"!] [!"num:i(./DcmDspSecurityLevel)"!]U

#if (defined Dcm_[!"name(.)"!])
#error Dcm_[!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) **/
#define Dcm_[!"@name"!] [!"num:i(./DcmDspSecurityLevel)"!]U
#endif /* !defined DCM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//

#endif /* if (DCM_INCLUDE_RTE == STD_OFF) */

#if (defined DCM_ROE_ENABLED) /* To prevent double declaration */
#error DCM_ROE_ENABLED already defined
#endif /* if (defined DCM_ROE_ENABLED) */

/** \brief Indicates, if ROE service is enabled */
#define DCM_ROE_ENABLED           [!//
[!IF "(count(DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*[. = 'DcmDslResponseOnEvent']) > 0 and count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '134']) > 0)"!] STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_ROE_DID_BUF_SIZE) /* To prevent double declaration */
#error DCM_ROE_DID_BUF_SIZE already defined
#endif /* if (defined DCM_ROE_DID_BUF_SIZE) */

/** \brief TheSize of the buffer used to manage internally OnChangeOfDataIdentifier subservice */
#define DCM_ROE_DID_BUF_SIZE           [!//
[!IF "node:exists(DcmConfigSet/*[1]/DcmDsp/DcmDspRoe/DcmDspRoeBufSize)"!][!//
[!"DcmConfigSet/*[1]/DcmDsp/DcmDspRoe/DcmDspRoeBufSize"!]U[!ELSE!]0U[!ENDIF!]

#if (defined DCM_ROE_INIT_ON_DSC) /* To prevent double declaration */
#error DCM_ROE_INIT_ON_DSC already defined
#endif /* if (defined DCM_ROE_INIT_ON_DSC) */

/** \brief If the ROE functionality is re-initialized on reception of service DiagnosticSessionControl */
#define DCM_ROE_INIT_ON_DSC           [!//
[!IF "(DcmConfigSet/*[1]/DcmDsp/DcmDspRoe/DcmDspRoeInitOnDSC = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_ROE_PERSISTENCE) /* To prevent double declaration */
#error DCM_ROE_PERSISTENCE already defined
#endif /* if (defined DCM_ROE_PERSISTENCE) */

/** \brief If the ROE persistence feature support is enabled */
#define DCM_ROE_PERSISTENCE           [!//
[!IF "(DcmConfigSet/*[1]/DcmDsp/DcmDspRoe/DcmDspRoePersistence = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_ROE_NVM_BLOCK_ID) /* To prevent double declaration */
#error DCM_ROE_NVM_BLOCK_ID already defined
#endif /* if (defined DCM_ROE_NVM_BLOCK_ID) */

/** \brief Block Id of NvM to store ROE Data for persistence */
#define DCM_ROE_NVM_BLOCK_ID           [!//
[!IF "(DcmConfigSet/*[1]/DcmDsp/DcmDspRoe/DcmDspRoePersistence = 'true')"!][!//
[!"node:ref(DcmConfigSet/*[1]/DcmDsp/DcmDspRoe/DcmDspRoeBlockIdRef)/NvMNvramBlockIdentifier"!]U[!ELSE!]0xFFU[!ENDIF!]

#if (defined DCM_ROE_QUEUE_ENABLED) /* To prevent double declaration */
#error DCM_ROE_QUEUE_ENABLED already defined
#endif /* if (defined DCM_ROE_QUEUE_ENABLED) */

/** \brief ROE transmission queue is enabled or not */
#define DCM_ROE_QUEUE_ENABLED           [!//
[!IF "(DcmConfigSet/*[1]/DcmDsp/DcmDspRoe/DcmDspRoeQueueEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_ROE_INTER_MSG_TIME) /* To prevent double declaration */
#error DCM_ROE_INTER_MSG_TIME already defined
#endif /* if (defined DCM_ROE_INTER_MSG_TIME) */

/** \brief The minimum time between two consecutive ROE transmissions */
#define DCM_ROE_INTER_MSG_TIME           [!//
[!IF "node:exists(DcmConfigSet/*[1]/DcmDsp/DcmDspRoe/DcmDspRoeInterMessageTime)"!][!//
[!"num:i(DcmConfigSet/*[1]/DcmDsp/DcmDspRoe/DcmDspRoeInterMessageTime div DcmConfigSet/*[1]/DcmGeneral/DcmTaskTime)"!]U[!ELSE!]0U[!ENDIF!]

#if (defined DCM_INT_MANAGED_DIDS) /* To prevent double declaration */
#error DCM_INT_MANAGED_DIDS already defined
#endif /* if (defined DCM_INT_MANAGED_DIDS) */

#define DCM_INT_MANAGED_DIDS           [!//
[!IF "(count(as:modconf('Dcm')[1]/DcmConfigSet/*/DcmDsp/DcmDspDid/*/DcmDspDidExtRoe) != count(as:modconf('Dcm')[1]/DcmConfigSet/*/DcmDsp/DcmDspDid/*)) and (count(as:modconf('Dcm')[1]/DcmConfigSet/*/DcmDsp/DcmDspDid/*) > 0)"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_EXT_MANAGED_DIDS) /* To prevent double declaration */
#error DCM_EXT_MANAGED_DIDS already defined
#endif /* if (defined DCM_EXT_MANAGED_DIDS) */

#define DCM_EXT_MANAGED_DIDS           [!//
[!IF "(count(as:modconf('Dcm')[1]/DcmConfigSet/*/DcmDsp/DcmDspDid/*/DcmDspDidExtRoe) > 0)"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/*------------------[Services configured]--------------------------------------------------------*/

#if (defined DCM_DSP_USE_SERVICE_0X3E ) /* To prevent double declaration */
#error DCM_DSP_USE_SERVICE_0X3E already defined
#endif /* if (defined DCM_DSP_USE_SERVICE_0X3E) */

#define DCM_DSP_USE_SERVICE_0X3E           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '62' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_DSP_USE_SERVICE_0X14 ) /* To prevent double declaration */
#error DCM_DSP_USE_SERVICE_0X14 already defined
#endif /* if (defined DCM_DSP_USE_SERVICE_0X14) */

#define DCM_DSP_USE_SERVICE_0X14           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '20' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_DSP_USE_SERVICE_0X19 ) /* To prevent double declaration */
#error DCM_DSP_USE_SERVICE_0X19 already defined
#endif /* if (defined DCM_DSP_USE_SERVICE_0X19) */

#define DCM_DSP_USE_SERVICE_0X19           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '25' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_DSP_USE_SERVICE_0X27 ) /* To prevent double declaration */
#error DCM_DSP_USE_SERVICE_0X27 already defined
#endif /* if (defined DCM_DSP_USE_SERVICE_0X27) */

#define DCM_DSP_USE_SERVICE_0X27           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '39' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_DSP_USE_SERVICE_0X10 ) /* To prevent double declaration */
#error DCM_DSP_USE_SERVICE_0X10  already defined
#endif /* if (defined DCM_DSP_USE_SERVICE_0X10 ) */

#define DCM_DSP_USE_SERVICE_0X10           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '16' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_DSP_USE_SERVICE_0X22 ) /* To prevent double declaration */
#error DCM_DSP_USE_SERVICE_0X22  already defined
#endif /* if (defined DCM_DSP_USE_SERVICE_0X22 ) */

#define DCM_DSP_USE_SERVICE_0X22           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '34' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_0X23_SVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X23_SVC_ENABLED  already defined
#endif /* if (defined DCM_0X23_SVC_ENABLED ) */

#define DCM_0X23_SVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '35']) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_0X3D_SVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X3D_SVC_ENABLED  already defined
#endif /* if (defined DCM_0X3D_SVC_ENABLED ) */

#define DCM_0X3D_SVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '61']) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_DSP_USE_SERVICE_0X2E ) /* To prevent double declaration */
#error DCM_DSP_USE_SERVICE_0X2E  already defined
#endif /* if (defined DCM_DSP_USE_SERVICE_0X2E ) */

#define DCM_DSP_USE_SERVICE_0X2E           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '46' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_DSP_USE_SERVICE_0X2F ) /* To prevent double declaration */
#error DCM_DSP_USE_SERVICE_0X2F  already defined
#endif /* if (defined DCM_DSP_USE_SERVICE_0X2F ) */

#define DCM_DSP_USE_SERVICE_0X2F           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '47' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_DSP_USE_SERVICE_0X31 ) /* To prevent double declaration */
#error DCM_DSP_USE_SERVICE_0X31  already defined
#endif /* if (defined DCM_DSP_USE_SERVICE_0X31 ) */

#define DCM_DSP_USE_SERVICE_0X31           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '49' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_DSP_USE_SERVICE_0X11 ) /* To prevent double declaration */
#error DCM_DSP_USE_SERVICE_0X11  already defined
#endif /* if (defined DCM_DSP_USE_SERVICE_0X11 ) */

#define DCM_DSP_USE_SERVICE_0X11           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '17' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_0X34_SVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X34_SVC_ENABLED  already defined
#endif /* if (defined DCM_0X34_SVC_ENABLED ) */

#define DCM_0X34_SVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '52' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_0X35_SVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X35_SVC_ENABLED  already defined
#endif /* if (defined DCM_0X35_SVC_ENABLED ) */

#define DCM_0X35_SVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '53' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_0X36_SVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X36_SVC_ENABLED  already defined
#endif /* if (defined DCM_0X36_SVC_ENABLED ) */

#define DCM_0X36_SVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '54' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_0X37_SVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X37_SVC_ENABLED  already defined
#endif /* if (defined DCM_0X37_SVC_ENABLED ) */

#define DCM_0X37_SVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '55' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_0X28_SVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X28_SVC_ENABLED  already defined
#endif /* if (defined DCM_0X28_SVC_ENABLED ) */

#define DCM_0X28_SVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '40' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_0X87_SVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X87_SVC_ENABLED  already defined
#endif /* if (defined DCM_0X87_SVC_ENABLED ) */

#define DCM_0X87_SVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '135' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_DSP_USE_SERVICE_0X86 ) /* To prevent double declaration */
#error DCM_DSP_USE_SERVICE_0X86  already defined
#endif /* if (defined DCM_DSP_USE_SERVICE_0X86 ) */

#define DCM_DSP_USE_SERVICE_0X86           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '134' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_0X85_SVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X85_SVC_ENABLED  already defined
#endif /* if (defined DCM_0X85_SVC_ENABLED ) */

#define DCM_0X85_SVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '133' and not(node:exists(DcmDsdSidTabFnc))]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DCM_EXTSVC_ENABLED ) /* To prevent double declaration */
#error DCM_EXTSVC_ENABLED  already defined
#endif /* if (defined DCM_EXTSVC_ENABLED ) */

/** \brief External handler is configured for any service or not */
#define DCM_EXTSVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*/DcmDsdSidTabFnc) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/*------------------[Sub Services configured ]----------------------------------------------------*/

#if (defined DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x01 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x01  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x01 ) */

/** \brief Subservice 0x01 for 0x10 enabled or not */
#define DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x01           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='16']/*/*[DcmDsdSubServiceId='1' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x02 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x02  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x02 ) */

/** \brief Subservice 0x02 for 0x10 enabled or not */
#define DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x02           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='16']/*/*[DcmDsdSubServiceId='2' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x03 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x03  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x03 ) */

/** \brief Subservice 0x03 for 0x10 enabled or not */
#define DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x03           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='16']/*/*[DcmDsdSubServiceId='3' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x2B ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x2B  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x2B ) */

/** \brief Subservice 0x2B for 0x10 enabled or not */
#define DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x2B           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='16']/*/*[DcmDsdSubServiceId='43' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x2C ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x2C  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x2C ) */

/** \brief Subservice 0x2C for 0x10 enabled or not */
#define DCM_DSP_SUBSERVICE_DIAGNOSTICSESSIONCONTROL_0x2C           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='16']/*/*[DcmDsdSubServiceId='44' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_HARDRESET_0x01 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_HARDRESET_0x01  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_HARDRESET_0x01 ) */

/** \brief Subservice 0x01 for 0x11 enabled or not */
#define DCM_DSP_SUBSERVICE_HARDRESET_0x01           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='17']/*/*[DcmDsdSubServiceId='1' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_KEYOFFONRESET_0x02 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_KEYOFFONRESET_0x02  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_KEYOFFONRESET_0x02 ) */

/** \brief Subservice 0x02 for 0x11 enabled or not */
#define DCM_DSP_SUBSERVICE_KEYOFFONRESET_0x02           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='17']/*/*[DcmDsdSubServiceId='2' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_SOFTRESET_0x03 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_SOFTRESET_0x03  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_SOFTRESET_0x03 ) */

/** \brief Subservice 0x03 for 0x11 enabled or not */
#define DCM_DSP_SUBSERVICE_SOFTRESET_0x03           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='17']/*/*[DcmDsdSubServiceId='3' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_ENABLERAPIDPOWERSHUTDOWN_0x04 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_ENABLERAPIDPOWERSHUTDOWN_0x04  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_ENABLERAPIDPOWERSHUTDOWN_0x04 ) */

/** \brief Subservice 0x04 for 0x11 enabled or not */
#define DCM_DSP_SUBSERVICE_ENABLERAPIDPOWERSHUTDOWN_0x04           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='17']/*/*[DcmDsdSubServiceId='4' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_DISABLERAPIDPOWERSHUTDOWN_0x05 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_DISABLERAPIDPOWERSHUTDOWN_0x05  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_DISABLERAPIDPOWERSHUTDOWN_0x05 ) */

/** \brief Subservice 0x05 for 0x11 enabled or not */
#define DCM_DSP_SUBSERVICE_DISABLERAPIDPOWERSHUTDOWN_0x05           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='17']/*/*[DcmDsdSubServiceId='5' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_0X28_0X00_SSVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X28_0X00_SSVC_ENABLED  already defined
#endif /* if (defined DCM_0X28_0X00_SSVC_ENABLED ) */

/** \brief Subservice 0x00 for 0x28 enabled or not */
#define DCM_0X28_0X00_SSVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='40']/*/*[DcmDsdSubServiceId='0' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_0X28_0X01_SSVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X28_0X01_SSVC_ENABLED  already defined
#endif /* if (defined DCM_0X28_0X01_SSVC_ENABLED ) */

/** \brief Subservice 0x01 for 0x28 enabled or not */
#define DCM_0X28_0X01_SSVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='40']/*/*[DcmDsdSubServiceId='1' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_0X28_0X02_SSVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X28_0X02_SSVC_ENABLED  already defined
#endif /* if (defined DCM_0X28_0X02_SSVC_ENABLED ) */

/** \brief Subservice 0x02 for 0x28 enabled or not */
#define DCM_0X28_0X02_SSVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='40']/*/*[DcmDsdSubServiceId='2' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_0X28_0X03_SSVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X28_0X03_SSVC_ENABLED  already defined
#endif /* if (defined DCM_0X28_0X03_SSVC_ENABLED ) */

/** \brief Subservice 0x03 for 0x28 enabled or not */
#define DCM_0X28_0X03_SSVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='40']/*/*[DcmDsdSubServiceId='3' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_0X19_0X01_SSVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X19_0X01_SSVC_ENABLED  already defined
#endif /* if (defined DCM_0X19_0X01_SSVC_ENABLED ) */

/** \brief Subservice 0x01 for 0x19 enabled or not */
#define DCM_0X19_0X01_SSVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='25']/*/*[DcmDsdSubServiceId='1' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x02 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x02  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x02 ) */

/** \brief Subservice 0x02 for 0x19 enabled or not */
#define DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x02           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='25']/*/*[DcmDsdSubServiceId='2' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x03 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x03  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x03 ) */

/** \brief Subservice 0x03 for 0x19 enabled or not */
#define DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x03           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='25']/*/*[DcmDsdSubServiceId='3' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x04 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x04  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x04 ) */

/** \brief Subservice 0x04 for 0x19 enabled or not */
#define DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x04           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='25']/*/*[DcmDsdSubServiceId='4' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x06 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x06  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x06 ) */

/** \brief Subservice 0x06 for 0x19 enabled or not */
#define DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x06           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='25']/*/*[DcmDsdSubServiceId='6' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x0A ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x0A  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x0A ) */

/** \brief Subservice 0x0A for 0x19 enabled or not */
#define DCM_DSP_SUBSERVICE_READDTCINFORMATION_0x0A           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='25']/*/*[DcmDsdSubServiceId='10' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x05 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x05  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x05 ) */

/** \brief Subservice 0x05 for 0x86 enabled or not */
#define DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x05           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='134']/*/*[DcmDsdSubServiceId='5' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x00 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x00  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x00 ) */

/** \brief Subservice 0x00 for 0x86 enabled or not */
#define DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x00           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='134']/*/*[DcmDsdSubServiceId='0' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x01 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x01  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x01 ) */

/** \brief Subservice 0x01 for 0x86 enabled or not */
#define DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x01           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='134']/*/*[DcmDsdSubServiceId='1' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x03 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x03  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x03 ) */

/** \brief Subservice 0x03 for 0x86 enabled or not */
#define DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x03           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='134']/*/*[DcmDsdSubServiceId='3' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x04 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x04  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x04 ) */

/** \brief Subservice 0x04 for 0x86 enabled or not */
#define DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x04           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='134']/*/*[DcmDsdSubServiceId='4' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x06 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x06  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x06 ) */

/** \brief Subservice 0x06 for 0x86 enabled or not */
#define DCM_DSP_SUBSERVICE_RESPONSEONEVENT_0x06           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='134']/*/*[DcmDsdSubServiceId='6' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_0X85_0X01_SSVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X85_0X01_SSVC_ENABLED  already defined
#endif /* if (defined DCM_0X85_0X01_SSVC_ENABLED ) */

/** \brief Subservice 0x01 for 0x85 enabled or not */
#define DCM_0X85_0X01_SSVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='133']/*/*[DcmDsdSubServiceId='1' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_0X85_0X02_SSVC_ENABLED ) /* To prevent double declaration */
#error DCM_0X85_0X02_SSVC_ENABLED  already defined
#endif /* if (defined DCM_0X85_0X02_SSVC_ENABLED ) */

/** \brief Subservice 0x02 for 0x85 enabled or not */
#define DCM_0X85_0X02_SSVC_ENABLED           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='133']/*/*[DcmDsdSubServiceId='2' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_LINKCONTROL_0x01 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_LINKCONTROL_0x01  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_LINKCONTROL_0x01 ) */

#define DCM_DSP_SUBSERVICE_LINKCONTROL_0x01           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='135']/*/*[DcmDsdSubServiceId='1' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_LINKCONTROL_0x02 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_LINKCONTROL_0x02  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_LINKCONTROL_0x02 ) */

#define DCM_DSP_SUBSERVICE_LINKCONTROL_0x02           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='135']/*/*[DcmDsdSubServiceId='2' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_DSP_SUBSERVICE_LINKCONTROL_0x03 ) /* To prevent double declaration */
#error DCM_DSP_SUBSERVICE_LINKCONTROL_0x03  already defined
#endif /* if (defined DCM_DSP_SUBSERVICE_LINKCONTROL_0x03 ) */

#define DCM_DSP_SUBSERVICE_LINKCONTROL_0x03           [!//
[!IF "count(DcmConfigSet/*/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId='135']/*/*[DcmDsdSubServiceId='3' and not(node:exists(DcmDsdSubServiceFnc))]) > 0"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]

#if (defined DCM_BSWM_API_ENABLED ) /* To prevent double declaration */
#error DCM_BSWM_API_ENABLED  already defined
#endif /* if defined (DCM_BSWM_API_ENABLED ) */

/** \brief BswM-Dcm interaction enabled or not */
#define DCM_BSWM_API_ENABLED           [!//
[!IF "node:exists(as:modconf('BswM')/BswMGeneral/BswMDcmEnabled) and as:modconf('BswM')/BswMGeneral/BswMDcmEnabled = 'true'"!]
STD_ON
[!ELSE!][!//
STD_OFF
[!ENDIF!]
/*==================[type definitions]===========================================================*/

/*------------------[Dcm_ConfigType]----------------------------------------*/

/** \brief This type of the external data structure shall contain the post
 **  build initialization data for the Dcm.
 **
 ** \note Type is unused, as only pre-compile time support is implemented. */
typedef uint8 Dcm_ConfigType;

/*==================[external function declarations]=============================================*/

[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspSession/*/DcmDspSessionRow/*"!][!//
[!VAR "StrName"="''"!]
[!VAR "StrTemp"="substring-after(name(.),'DCM_')"!]
[!IF "$StrTemp = ''"!][!VAR "StrTemp"="name(.)"!][!ENDIF!]
[!LOOP "text:split($StrTemp, '_')"!]
[!VAR "StrName"="concat($StrName, concat(substring(., 1,1), text:tolower(substring(., 2))))"!]
[!ENDLOOP!]

[!IF "not(contains($StrName,'AllSessionLevel'))"!]
extern FUNC(Std_ReturnType, DCM_CODE) Dcm_[!"$StrName"!]ModeEntry(void);
[!ENDIF!]
[!ENDLOOP!][!//

[!LOOP "DcmConfigSet/*/DcmProcessingConditions/DcmModeRule/*"!]
/** \brief Mode rule function declaration for [!"name(.)"!] */
extern FUNC(boolean, DCM_CODE) ModeRule_[!"name(.)"!]_Result(P2VAR(uint8, AUTOMATIC, DCM_VAR) Nrc);
[!ENDLOOP!]

/*==================[internal function declarations]=============================================*/

/*==================[external constants]=========================================================*/

#define DCM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/** \brief Configuration structure */
extern CONST(Dcm_ConfigType, DCM_CONST) [!"name(DcmConfigSet/*[1])"!];

#define DCM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[internal data]==============================================================*/

/*==================[external function definitions]==============================================*/

/*==================[internal function definitions]==============================================*/

#endif /* if !defined( DCM_CFG_H ) */
/*==================[end of file]================================================================*/
