[!/**
 * \file
 *
 * \brief AUTOSAR Dcm
 *
 * This file contains the implementation of the AUTOSAR
 * module Dcm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//
[!/*=== The code replaces the Endianness conversion for RoutineSignaltype ===*/!][!//

[!MACRO "DCM_ENDIANNESS_CONVERSION_OUTPARAM"!][!/*
*/!][!IF "((DcmDspRoutineSignalType = 'SINT16') or (DcmDspRoutineSignalType = 'UINT16'))"!][!/*
*/!]        uint16 outParam[!"num:i($outVarCount)"!]temp = ((uint16)((uint16)outParam[!"num:i($outVarCount)"!] >> 8U) | [!/*
*/!]          (uint16)((uint16)outParam[!"num:i($outVarCount)"!] << 8U)); [!/*
*/!]           outParam[!"num:i($outVarCount)"!] = ([!"text:tolower(DcmDspRoutineSignalType)"!])outParam[!"num:i($outVarCount)"!]temp; [!/*
*/!]        [!ELSEIF "((DcmDspRoutineSignalType = 'SINT32') or (DcmDspRoutineSignalType = 'UINT32'))"!][!/*
*/!]        uint32 outParam[!"num:i($outVarCount)"!]temp = (((uint32)outParam[!"num:i($outVarCount)"!]) >> 24U) | [!/*
*/!]          ((((uint32)outParam[!"num:i($outVarCount)"!]) << 8U)&0x00ff0000U) | [!/*
*/!]          ((((uint32)outParam[!"num:i($outVarCount)"!]) >> 8U)&0x0000ff00U)| [!/*
*/!]          (((uint32)outParam[!"num:i($outVarCount)"!]) << 24U); [!/*
*/!]           outParam[!"num:i($outVarCount)"!] = ([!"text:tolower(DcmDspRoutineSignalType)"!])outParam[!"num:i($outVarCount)"!]temp; [!/*
*/!]        [!ENDIF!] [!/*
*/!][!ENDMACRO!]

[!MACRO "DCM_ENDIANNESS_CONVERSION_INPARAM"!][!/*
*/!][!IF "((DcmDspRoutineSignalType = 'SINT16') or (DcmDspRoutineSignalType = 'UINT16'))"!][!/*
*/!]        uint16 inParam[!"num:i($inVarCount)"!]temp = ((uint16)((uint16)inParam[!"num:i($inVarCount)"!] >> 8U) | [!/*
*/!]          (uint16)((uint16)inParam[!"num:i($inVarCount)"!] << 8U)); [!/*
*/!]          inParam[!"num:i($inVarCount)"!] = ([!"text:tolower(DcmDspRoutineSignalType)"!])inParam[!"num:i($inVarCount)"!]temp; [!/*
*/!]        [!ELSEIF "((DcmDspRoutineSignalType = 'SINT32') or (DcmDspRoutineSignalType = 'UINT32'))"!][!/*
*/!]        uint32 inParam[!"num:i($inVarCount)"!]temp = ((uint32)(inParam[!"num:i($inVarCount)"!]) >> 24U) | [!/*
*/!]          ((((uint32)inParam[!"num:i($inVarCount)"!]) << 8U)&0x00ff0000U) | [!/*
*/!]          ((((uint32)inParam[!"num:i($inVarCount)"!]) >> 8U)&0x0000ff00U)| [!/*
*/!]          (((uint32)inParam[!"num:i($inVarCount)"!]) << 24U); [!/*
*/!]           inParam[!"num:i($inVarCount)"!] = ([!"text:tolower(DcmDspRoutineSignalType)"!])inParam[!"num:i($inVarCount)"!]temp; [!/*
*/!]        [!ENDIF!] [!/*
*/!][!ENDMACRO!]

[!/*=== The code generating the internal service handler  name ===*/!][!//

[!MACRO "DCM_SERVICE_HANDLER_NAME"!][!/*
*/!][!IF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x10'"!][!/*
*/!]Dcm_DspInternal_DiagnosticSessionControl_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x11'"!][!/*
*/!]Dcm_DspInternal_ECUReset_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x14'"!][!/*
*/!]Dcm_DspInternal_ClearDiagnosticInformation_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x19'"!][!/*
*/!]Dcm_DspInternal_ReadDTCInformation_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x22'"!][!/*
*/!]Dcm_DspInternal_ReadDataByIdentifier_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x23'"!][!/*
*/!]Dcm_DspInternal_ReadMemoryByAddress_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x27'"!][!/*
*/!]Dcm_DspInternal_SecurityAccessControl_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x28'"!][!/*
*/!]Dcm_DspInternal_CommunicationControl_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x2e'"!][!/*
*/!]Dcm_DspInternal_WriteDataByIdentifier_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x2f'"!][!/*
*/!]Dcm_DspInternal_InputOutputControlByIdentifier_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x31'"!][!/*
*/!]Dcm_DspInternal_RoutineControl_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x34'"!][!/*
*/!]Dcm_DspInternal_RequestDownloadHandler_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x35'"!][!/*
*/!]Dcm_DspInternal_RequestUploadHandler_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x36'"!][!/*
*/!]Dcm_DspInternal_TransferDataHandler_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x37'"!][!/*
*/!]Dcm_DspInternal_RequestTransferExitHandler_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x3e'"!][!/*
*/!]Dcm_DspInternal_TesterPresent_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x85'"!][!/*
*/!]Dcm_DspInternal_ControlDTCSetting_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x86'"!][!/*
*/!]Dcm_DspInternal_ResponseOnEvent_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x87'"!][!/*
*/!]Dcm_DspInternal_LinkControlHandler_SvcH[!/*
*/!][!ELSEIF "num:inttohex( DcmDsdSidTabServiceId, 2) = '0x3d'"!][!/*
*/!]Dcm_DspInternal_WriteMemoryByAddress_SvcH[!/*
*/!][!ENDIF!][!/*
*/!][!ENDMACRO!]

[!/*=== The code generating the internal Sub service handler  name ===*/!][!//
[!MACRO "DCM_SUB_SERVICE_HANDLER_NAME"!][!/*
*/!][!IF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x10'"!][!/*
*/!]Dcm_DspInternal_DiagnosticSessionControlCommon_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x19' and num:inttohex( DcmDsdSubServiceId, 2) = '0x01'"!][!/*
*/!]Dcm_DspInternal_reportNumberOfDTCByStatusMask_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x19' and num:inttohex( DcmDsdSubServiceId, 2) = '0x02'"!][!/*
*/!]Dcm_DspInternal_ReportDTCByStatusMask_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x19' and num:inttohex( DcmDsdSubServiceId, 2) = '0x03'"!][!/*
*/!]Dcm_DspInternal_ReportDtcSnapshotRecordIdentification_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x19' and num:inttohex( DcmDsdSubServiceId, 2) = '0x04'"!][!/*
*/!]Dcm_DspInternal_ReportDtcSnapshotRecordByDtcNumber_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x19' and num:inttohex( DcmDsdSubServiceId, 2) = '0x06'"!][!/*
*/!]Dcm_DspInternal_ReportDtcExtendedDataRecordByDtcNumber_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x19' and num:inttohex( DcmDsdSubServiceId, 2) = '0x0a'"!][!/*
*/!]Dcm_DspInternal_ReportSupportedDtcs_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x85' and num:inttohex( DcmDsdSubServiceId, 2) = '0x01'"!][!/*
*/!]Dcm_DspInternal_DTCSettingTypeOn_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x85' and num:inttohex( DcmDsdSubServiceId, 2) = '0x02'"!][!/*
*/!]Dcm_DspInternal_DTCSettingTypeOff_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x28' and num:inttohex( DcmDsdSubServiceId, 2) = '0x00'"!][!/*
*/!]Dcm_DspInternal_EnableRxAndTx_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x28' and num:inttohex( DcmDsdSubServiceId, 2) = '0x01'"!][!/*
*/!]Dcm_DspInternal_EnableRxAndDisableTx_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x28' and num:inttohex( DcmDsdSubServiceId, 2) = '0x02'"!][!/*
*/!]Dcm_DspInternal_DisableRxAndEnableTx_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x28' and num:inttohex( DcmDsdSubServiceId, 2) = '0x03'"!][!/*
*/!]Dcm_DspInternal_DisableRxAndTx_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x86' and num:inttohex( DcmDsdSubServiceId, 2) = '0x05'"!][!/*
*/!]Dcm_DspInternal_StartROE_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x86' and num:inttohex( DcmDsdSubServiceId, 2) = '0x00'"!][!/*
*/!]Dcm_DspInternal_StopROE_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x86' and num:inttohex( DcmDsdSubServiceId, 2) = '0x01'"!][!/*
*/!]Dcm_DspInternal_OnDTCStatusChange_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x86' and num:inttohex( DcmDsdSubServiceId, 2) = '0x03'"!][!/*
*/!]Dcm_DspInternal_OnChangeOfDataIdentifier_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x86' and num:inttohex( DcmDsdSubServiceId, 2) = '0x04'"!][!/*
*/!]Dcm_DspInternal_ReportActivatedEvents_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x86' and num:inttohex( DcmDsdSubServiceId, 2) = '0x06'"!][!/*
*/!]Dcm_DspInternal_ClearROE_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x11' and num:inttohex( DcmDsdSubServiceId, 2) = '0x01'"!][!/*
*/!]Dcm_DspInternal_HardReset_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x11' and num:inttohex( DcmDsdSubServiceId, 2) = '0x02'"!][!/*
*/!]Dcm_DspInternal_KeyOffOnReset_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x11' and num:inttohex( DcmDsdSubServiceId, 2) = '0x03'"!][!/*
*/!]Dcm_DspInternal_SoftReset_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x11' and num:inttohex( DcmDsdSubServiceId, 2) = '0x04'"!][!/*
*/!]Dcm_DspInternal_EnableRapidPowerShutDown_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x11' and num:inttohex( DcmDsdSubServiceId, 2) = '0x05'"!][!/*
*/!]Dcm_DspInternal_DisableRapidPowerShutDown_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x87' and num:inttohex( DcmDsdSubServiceId, 2) = '0x01'"!][!/*
*/!]Dcm_DspInternal_FixedBaudrateVerification_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x87' and num:inttohex( DcmDsdSubServiceId, 2) = '0x02'"!][!/*
*/!]Dcm_DspInternal_SpecificBaudrateVerification_SSvcH[!/*
*/!][!ELSEIF "num:inttohex(../.././DcmDsdSidTabServiceId,2) ='0x87' and num:inttohex( DcmDsdSubServiceId, 2) = '0x03'"!][!/*
*/!]Dcm_DspInternal_TransitionBaudrate_SSvcH[!/*
*/!][!ENDIF!][!/*
*/!][!ENDMACRO!]

[!MACRO "ROUTINECONTROL_GENERATE","operation"!][!/*
*/!][!IF "$operation = 'startRoutine'"!][!/*
*/!]  [!VAR "functionName" ="'Start'"!][!/*
*/!]  [!VAR "calloutName" ="'Start'"!][!/*
*/!]  [!VAR "parameterName" = "'StartRoutine'"!][!/*
*/!][!ELSEIF "$operation = 'stopRoutine'"!][!/*
*/!]  [!VAR "functionName" ="'Stop'"!][!/*
*/!]  [!VAR "calloutName" ="'Stop'"!][!/*
*/!]  [!VAR "parameterName" = "'RoutineStop'"!][!/*
*/!][!ELSEIF "$operation = 'requestRoutineResults'"!][!/*
*/!]  [!VAR "functionName" ="'RequestResults'"!][!/*
*/!]  [!VAR "parameterName" = "'RoutineRequestRes'"!][!/*
*/!][!ENDIF!]
STATIC FUNC(Std_ReturnType, DCM_CODE)Dcm_[!"node:name(.)"!]_[!"$functionName"!]([!/*
*/!][!INDENT "2"!]
  Dcm_OpStatusType OpStatus,
  P2VAR(Dcm_MsgContextType, AUTOMATIC, DCM_APPL_DATA)pMsgContext,
  P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode)[!/*
*/!][!ENDINDENT!]
{[!/*
*/!][!INDENT "2"!][!/*
*/!][!IF "((node:refvalid(DcmDspRoutineInfoRef)) and
           (((node:exists(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'In'))) and
             ((count(node:ref(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'In'))/*[name(.) = concat('DcmDsp',$parameterName,'InSignal')]/*)) > 0)
            ) or
            ((node:exists(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'Out'))) and
             ((count(node:ref(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'Out'))/*[name(.) = concat('DcmDsp',$parameterName,'OutSignal')]/*)) > 0)
            )
           ) 
          )"!]
  /* get configuration for current HSM instance */[!/*
*/!]  [!VAR "endiannessconversionrequired"="0"!][!/*
*/!]  [!IF "(node:exists(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'In')))"!][!/*
*/!]    [!LOOP "node:ref(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'In'))/*[name(.) = concat('DcmDsp',$parameterName,'InSignal')]/*"!][!/*
*/!]      [!IF "((DcmDspRoutineSignalType = 'SINT16') or (DcmDspRoutineSignalType = 'UINT16') or
                 (DcmDspRoutineSignalType = 'SINT32') or (DcmDspRoutineSignalType = 'UINT32'))"!][!/*
*/!]        [!VAR "endiannessconversionrequired"="1"!][!/*
*/!]        [!BREAK!][!/*
*/!]      [!ENDIF!][!/*
*/!]    [!ENDLOOP!][!/*
*/!]  [!ENDIF!][!/*

*/!]  [!IF "$endiannessconversionrequired != 1"!][!/*
*/!]    [!IF "(node:exists(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'Out')))"!][!/*
*/!]      [!LOOP "node:ref(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'Out'))/*[name(.) = concat('DcmDsp',$parameterName,'OutSignal')]/*"!][!/*
*/!]        [!IF "((DcmDspRoutineSignalType = 'SINT16') or (DcmDspRoutineSignalType = 'UINT16') or
                   (DcmDspRoutineSignalType = 'SINT32') or (DcmDspRoutineSignalType = 'UINT32'))"!][!/*
*/!]          [!VAR "endiannessconversionrequired"="1"!][!/*
*/!]          [!BREAK!][!/*
*/!]        [!ENDIF!][!/*
*/!]      [!ENDLOOP!][!/*
*/!]    [!ENDIF!][!/*
*/!]  [!ENDIF!][!/*

*/!]  [!IF "$endiannessconversionrequired = 1"!]
  CONSTP2CONST(Dcm_HsmConfigType, AUTOMATIC, DCM_CONST) curHsmConfig[!/*
*/!]    [!INDENT "4"!]
    = &DCM_HSM_CONFIG(Dcm_CurProtocolId);[!/*
*/!]    [!ENDINDENT!][!/*
*/!]  [!ENDIF!][!/*
*/!][!ENDIF!]

  Std_ReturnType retVal = DCM_E_OK;[!/*
*/!][!IF "DcmDspRoutineFixedLength = 'false'"!]
  /* Variable length input/output signal length */
  uint16 currentDataLength = 0U;[!/*
*/!][!ENDIF!]

  /* Parameter to store the length of input data (2U indicates the 2-byte RoutineID) received */
  uint16 inDataLength = (pMsgContext->reqDataLen) - 2U;[!/*
*/!][!/*---List the output signals configured---*/!][!/*
*/!][!VAR "outFixedDataLength"="0"!][!/*
*/!][!VAR "outVarCount"="0"!][!/*
*/!][!VAR "outParamList"="''"!][!/*
*/!][!VAR "PmsgoutParam"="''"!][!/*
*/!][!VAR "outVariableDataLength"="0"!][!/*

*/!][!IF "(node:exists(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'Out')))"!]

  /* Fixed length output signals */[!/*
*/!]  [!LOOP "node:order(node:ref(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'Out'))/*[name(.) = concat('DcmDsp',$parameterName,'OutSignal')]/*
                         , 'DcmDspRoutineSignalPos')"!][!/*
*/!]    [!VAR "outFixedDataLength"="DcmDspRoutineSignalPos"!][!/*
*/!]    [!IF "DcmDspRoutineSignalType = 'VARIABLE_LENGTH'"!][!/*
*/!]      [!VAR "outVariableDataLength"="DcmDspRoutineSignalLength"!][!/*
*/!]      [!IF "$CommonBufferCount = 0"!][!/*
*/!]        [!/* Rx and Tx buffer are different */!][!/*
*/!]        [!IF "(((DcmDspRoutineSignalPos mod 8) = 0) = 'true')"!][!/*
*/!]          [!/* Variable length signal is byte alligned */!][!/*
*/!]          [!VAR "PmsgoutParam"="concat('&(pMsgContext->resData[', num:i(3 + ($outFixedDataLength div 8)), ']), ')"!][!/*
*/!]        [!ELSE!][!/*
*/!]          [!/* Variable length signal is not byte alligned */!][!/*
*/!]          [!VAR "PmsgoutParam"="concat('&(pMsgContext->resData[', num:i(3 + ($outFixedDataLength div 8) + 1 ), ']), ')"!][!/*
*/!]        [!ENDIF!][!/*
*/!]      [!ELSE!][!/*
*/!]        [!/* Rx and Tx buffer are same, using a separate buffer */!][!/*
*/!]        [!VAR "PmsgoutParam"="concat('&(Dcm_RoutineControlBuffer[', num:i(0), ']), ')"!][!/*
*/!]      [!ENDIF!][!/*
*/!]    [!ELSE!][!/*
*/!]      [!VAR "outParamList"="concat($outParamList, '&outParam', num:i($outVarCount), ', ')"!]
  [!"text:tolower(DcmDspRoutineSignalType)"!] outParam[!"num:i($outVarCount)"!];[!/*
*/!]      [!/* Adding length of last signal */!][!/*
*/!]      [!IF "DcmDspRoutineSignalPos = num:max(../*/DcmDspRoutineSignalPos)"!][!/*
*/!]       [!VAR "outFixedDataLength"="$outFixedDataLength + DcmDspRoutineSignalLength"!][!/*
*/!]      [!ENDIF!][!/*
*/!]    [!ENDIF!][!/*
*/!]    [!VAR "outVarCount"="$outVarCount + 1"!][!/*
*/!]  [!ENDLOOP!][!/*
*/!][!ENDIF!][!/*
*/!][!/*---List the input signals configured---*/!][!/*
*/!][!VAR "inTotalDataLength"="0"!][!/*
*/!][!VAR "inFixedDataLength"="0"!][!/*
*/!][!VAR "inVarCount"="0"!][!/*
*/!][!VAR "inParamList"="''"!][!/*
*/!][!VAR "PmsginParam"="''"!][!/*

*/!][!IF "(node:exists(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'In')))"!]

  /* Fixed length input signals */[!/*
*/!]  [!LOOP "node:order(node:ref(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'In'))/*[name(.) = concat('DcmDsp',$parameterName,'InSignal')]/*, 'DcmDspRoutineSignalPos')"!][!/*
*/!]    [!VAR "inTotalDataLength"="DcmDspRoutineSignalPos"!][!/*
*/!]    [!VAR "inFixedDataLength"="$inTotalDataLength "!][!/*
*/!]    [!IF "not(DcmDspRoutineSignalType = 'VARIABLE_LENGTH')"!][!/*
*/!]      [!VAR "inParamList"="concat($inParamList, 'inParam', num:i($inVarCount), ', ')"!]
  [!"text:tolower(DcmDspRoutineSignalType)"!] inParam[!"num:i($inVarCount)"!];[!/*
*/!]    [!ELSE!][!/*
*/!]      [!IF "(((DcmDspRoutineSignalPos mod 8) = 0) = 'true')"!][!/*
*/!]        [!/* If Variable length signal is byte alligned */!][!/*
*/!]        [!VAR "PmsginParam"="concat('&(pMsgContext->reqData[', num:i(2 + ($inTotalDataLength div 8)), ']), ')"!][!/*
*/!]      [!ELSE!][!/*
*/!]        [!/* If Variable length signal is not byte alligned */!][!/*
*/!]        [!VAR "PmsginParam"="concat('&(pMsgContext->reqData[', num:i(2 + (($inTotalDataLength) div 8)+ 1), ']), ')"!]
  /* Shifting bits of the variable length signal to the right until the next byte boundary */
  Dcm_DspInternal_ShiftBitsInBuffer( (P2VAR(uint8, AUTOMATIC, DCM_VAR))&(pMsgContext->reqData[2]), [!"num:i($inFixedDataLength)"!]U,[!"num:i($inFixedDataLength + (8 -($inFixedDataLength mod 8)))"!]U,[!"DcmDspRoutineSignalLength"!]U, FALSE);[!/*
*/!]      [!ENDIF!][!/*
*/!]      [!VAR "inTotalDataLength"="$inTotalDataLength + DcmDspRoutineSignalLength"!][!/*
*/!]    [!ENDIF!][!/*
*/!]    [!/* Adding length of last signal , if it is not variable length */!][!/*
*/!]    [!IF "DcmDspRoutineSignalPos = num:max(../*/DcmDspRoutineSignalPos)"!][!/*
*/!]      [!IF "not(DcmDspRoutineSignalType = 'VARIABLE_LENGTH')"!][!/*
*/!]        [!VAR "inTotalDataLength"="$inTotalDataLength + DcmDspRoutineSignalLength"!][!/*
*/!]        [!/* inFixedDataLength will be equal to the total data length  */!][!/*
*/!]        [!VAR "inFixedDataLength"="$inTotalDataLength "!][!/*
*/!]      [!ENDIF!][!/*
*/!]    [!ENDIF!][!/*
*/!]    [!VAR "inVarCount"="$inVarCount + 1"!][!/*
*/!]  [!ENDLOOP!][!/*
*/!][!ENDIF!][!/*

*/!][!IF "DcmDspRoutineFixedLength = 'false'"!][!/*
*/!]  [!IF "$DcmRoutineVarLenInBytes = 'false'"!]
  /* Total received length for the variable length input signal minus the fixed data length,in bits. 
[!WS "3"!]This is at most an approximation, as we can never be sure
[!WS "3"!]about how many bits in the last received byte are actually valid */
  currentDataLength = (((pMsgContext->reqDataLen) - 2U) * 8U) - [!"num:i($inFixedDataLength)"!]U;[!/*
*/!]  [!ELSE!]
  /* Total received length for the variable length input signal, in bytes */
  currentDataLength = (((pMsgContext->reqDataLen) - 2U) - [!"num:i($inFixedDataLength div 8)"!]U);[!/*
*/!]  [!ENDIF!][!/*
*/!][!ENDIF!]

  /* Input data length validity check */[!/*
*/!][!VAR "inTotalDataLengthBytes"="num:i(($inTotalDataLength + 7) div 8)"!][!/*
*/!][!VAR "inFixedDataLengthBytes"="num:i(($inFixedDataLength + 7) div 8)"!][!/*
*/!][!IF "((node:exists(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'In'))) and
           ((count(node:ref(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'In'))/*[name(.) = concat('DcmDsp',$parameterName,'InSignal')]/*)) > 0)
          )"!][!/*
*/!]  [!IF "($inFixedDataLength > 0) and ($inTotalDataLength > $inFixedDataLength)"!][!/*
*/!]    [!/*---Both fixed and variable length signals are configured---*/!]
  if ((inDataLength >= [!"num:i($inFixedDataLengthBytes)"!]U) && (inDataLength <= [!"num:i($inTotalDataLengthBytes)"!]U))[!/*
*/!]  [!ELSEIF "($inFixedDataLength > 0)"!][!/*
*/!]    [!/*---Only fixed length signals are configured---*/!]
  if (inDataLength == [!"num:i($inFixedDataLengthBytes)"!]U)[!/*
*/!]  [!ELSEIF "($inTotalDataLength > 0)"!][!/*
*/!]    [!/*---Only variable length signals are configured---*/!]
  if (inDataLength <= [!"num:i($inTotalDataLengthBytes)"!]U)[!/*
*/!]  [!ENDIF!][!/*
*/!][!ELSE!][!/*
*/!]  [!/*---No input signals are configured---*/!]
  if (inDataLength == 0U)[!/*
*/!][!ENDIF!]
  {[!/*
*/!][!INDENT "4"!][!/*
*/!][!VAR "inVarCount"="0"!][!/*
*/!][!IF "(node:exists(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'In')))"!][!/*
*/!]  [!LOOP "node:order(node:ref(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'In'))/*[name(.) = concat('DcmDsp',$parameterName,'InSignal')]/*, 'DcmDspRoutineSignalPos')"!][!/*
*/!]    [!IF "DcmDspRoutineSignalType != 'VARIABLE_LENGTH'"!]
    /* Deviation MISRA-2 */
    Dcm_DspInternal_CopySignalData((P2VAR(uint8, AUTOMATIC, DCM_VAR))&(pMsgContext->reqData[2]), (P2VAR(uint8, AUTOMATIC, DCM_VAR))&inParam[!"num:i($inVarCount)"!], [!"DcmDspRoutineSignalPos"!]U, 0U, [!"DcmDspRoutineSignalLength"!]U);[!/*
*/!]    [!ENDIF!][!/*
*/!]    [!IF "((DcmDspRoutineSignalType = 'SINT16') or (DcmDspRoutineSignalType = 'UINT16') or
               (DcmDspRoutineSignalType = 'SINT32') or (DcmDspRoutineSignalType = 'UINT32')
              )"!]
    /* !LINKSTO Dcm641,3 */
    if(curHsmConfig->endianessConversion == TRUE)
    {[!/*
*/!]     [!INDENT "6"!]
[!WS "2"!][!CALL "DCM_ENDIANNESS_CONVERSION_INPARAM"!][!/*
*/!]     [!ENDINDENT!]
    }[!/*
*/!]    [!ENDIF!][!/*
*/!]    [!VAR "inVarCount"="$inVarCount + 1"!][!/*
*/!]  [!ENDLOOP!][!/*
*/!][!ENDIF!][!/*
*/!][!IF "DcmDspRoutineUsePort = 'true'"!][!/*
*/!]  [!INDENT "0"!]
#if (DCM_INCLUDE_RTE == STD_ON)[!/*
*/!]  [!ENDINDENT!][!/*
*/!]  [!IF "DcmDspRoutineFixedLength = 'false'"!]
    retVal = Rte_Call_RoutineServices_[!"node:name(.)"!]_[!"$functionName"!]([!"$inParamList"!][!"$PmsginParam"!]OpStatus,[!"$outParamList"!][!"$PmsgoutParam"!]&currentDataLength, ErrorCode);[!/*
*/!]  [!ELSE!]
    retVal = Rte_Call_RoutineServices_[!"node:name(.)"!]_[!"$functionName"!]([!"$inParamList"!]OpStatus, [!"$outParamList"!]ErrorCode);[!/*
*/!]  [!ENDIF!]

    if ( retVal == DCM_E_OK )
    {[!/*
*/!]  [!INDENT "6"!][!/*
*/!]  [!IF "DcmDspRoutineFixedLength = 'false'"!][!/*
*/!]    [!IF "num:i($outFixedDataLength) > 0"!][!/*
*/!]      [!IF "$DcmRoutineVarLenInBytes = 'false'"!]
      pMsgContext->resDataLen = (PduLengthType)DCM_GET_BYTES([!"num:i($outFixedDataLength)"!]U + currentDataLength) + 3U;[!/*
*/!]      [!ELSE!]
      if((currentDataLength * 8U) > [!"num:i($outVariableDataLength)"!]U)
      {[!/*
*/!]        [!INDENT "8"!]
        pMsgContext->resDataLen = (PduLengthType)DCM_GET_BYTES([!"num:i($outFixedDataLength)"!]U + [!"num:i($outVariableDataLength)"!]U) + 3U;[!/*
*/!]        [!ENDINDENT!]
      }
      else
      {[!/*
*/!]        [!INDENT "8"!]
        pMsgContext->resDataLen = (PduLengthType)DCM_GET_BYTES([!"num:i($outFixedDataLength)"!]U) + currentDataLength + 3U;[!/*
*/!]        [!ENDINDENT!]
      }[!/*
*/!]      [!ENDIF!][!/*
*/!]    [!ELSE!][!/*
*/!]      [!IF "$DcmRoutineVarLenInBytes = 'false'"!]
      pMsgContext->resDataLen = (PduLengthType)DCM_GET_BYTES(currentDataLength) + 3U;[!/*
*/!]      [!ELSE!]
      if((currentDataLength * 8U) > [!"num:i($outVariableDataLength)"!]U)
      {[!/*
*/!]      [!INDENT "8"!]
        pMsgContext->resDataLen = (PduLengthType)DCM_GET_BYTES([!"num:i($outVariableDataLength)"!]U) + 3U;[!/*
*/!]      [!ENDINDENT!]
      }
      else
      {[!/*
*/!]      [!INDENT "8"!]
        pMsgContext->resDataLen = currentDataLength + 3U;[!/*
*/!]      [!ENDINDENT!]
      }[!/*
*/!]      [!ENDIF!][!/*
*/!]    [!ENDIF!][!/*
*/!]  [!ELSE!]
      pMsgContext->resDataLen = [!"num:i((($outFixedDataLength + 7) div 8) + 3)"!]U;[!/*
*/!]  [!ENDIF!][!/*
*/!]  [!VAR "outVarCount"="0"!][!/*

*/!]  [!IF "(node:exists(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'Out')))"!][!/*
*/!]    [!LOOP "node:order(node:ref(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'Out'))/*[name(.) = concat('DcmDsp',$parameterName,'OutSignal')]/*, 'DcmDspRoutineSignalPos')"!][!/*
*/!]      [!IF "(DcmDspRoutineSignalType = 'VARIABLE_LENGTH')"!][!/*
*/!]        [!IF "$CommonBufferCount = 0"!][!/*
*/!]          [!/* Rx and Tx buffer are different */!][!/*
*/!]          [!IF "not((DcmDspRoutineSignalPos mod 8) = '0') "!][!/*
*/!]            [!/*Variable length signal is not byte alligned */!][!/*
*/!]            [!IF "$DcmRoutineVarLenInBytes = 'false'"!]
      Dcm_DspInternal_ShiftBitsInBuffer( (P2VAR(uint8, AUTOMATIC, DCM_VAR))&(pMsgContext->resData[3]), 
[!WS "8"!][!"num:i($outFixedDataLength + (8- ($outFixedDataLength mod 8)))"!]U,[!"num:i($outFixedDataLength)"!],currentDataLength, TRUE);[!/*
*/!]            [!ELSE!]
      Dcm_DspInternal_ShiftBitsInBuffer( (P2VAR(uint8, AUTOMATIC, DCM_VAR))&(pMsgContext->resData[3]),
[!WS "8"!][!"num:i($outFixedDataLength + (8 - ($outFixedDataLength mod 8)))"!]U,[!"num:i($outFixedDataLength)"!],(currentDataLength * 8), TRUE);[!/*
*/!]            [!ENDIF!][!/*
*/!]          [!ENDIF!][!/*
*/!]        [!ELSE!][!/*
*/!]          [!/* Rx and Tx buffers are the same, an auxiliary buffer needs to be used */!]
      Dcm_DspInternal_CopySignalData((P2VAR(uint8, AUTOMATIC, DCM_VAR))&Dcm_RoutineControlBuffer, (P2VAR(uint8, AUTOMATIC, DCM_VAR))&(pMsgContext->resData[3]), 0U, [!"DcmDspRoutineSignalPos"!]U,  [!"DcmDspRoutineSignalLength"!]U);[!/*
*/!]        [!ENDIF!][!/*
*/!]      [!ELSE!][!/*
*/!]        [!IF "((DcmDspRoutineSignalType = 'SINT16') or (DcmDspRoutineSignalType = 'UINT16') or
                   (DcmDspRoutineSignalType = 'SINT32') or (DcmDspRoutineSignalType = 'UINT32'))"!]
      if(curHsmConfig->endianessConversion == TRUE)
      {[!/*
*/!]        [!INDENT "8"!]
[!WS "2"!][!CALL "DCM_ENDIANNESS_CONVERSION_OUTPARAM"!][!/*
*/!]        [!ENDINDENT!]
      }[!/*
*/!]        [!ENDIF!]
      /* Deviation MISRA-2  */
      Dcm_DspInternal_CopySignalData((P2VAR(uint8, AUTOMATIC, DCM_VAR))&outParam[!"num:i($outVarCount)"!], (P2VAR(uint8, AUTOMATIC, DCM_VAR))&(pMsgContext->resData[3]),
[!WS "8"!]0U, [!"DcmDspRoutineSignalPos"!]U,  [!"DcmDspRoutineSignalLength"!]U);[!/*
*/!]      [!ENDIF!][!/*
*/!]      [!VAR "outVarCount"="$outVarCount + 1"!][!/*
*/!]    [!ENDLOOP!][!/*
*/!]  [!ENDIF!][!/*
*/!][!ENDINDENT!]
    }
    else if ( retVal == DCM_E_NO_DATA )
    {[!/*
*/!][!INDENT "6"!]
      retVal = DCM_E_PENDING;[!/*
*/!][!ENDINDENT!]
    }
    else
    {[!/*
*/!][!INDENT "6"!]
      if (( retVal != DCM_E_PENDING ) && ( retVal != DCM_E_FORCE_RCRRP ))
      {[!/*
*/!][!INDENT "8"!]
        retVal = DCM_E_NOT_OK;[!/*
*/!][!ENDINDENT!]
      }[!/*
*/!][!ENDINDENT!]
    }[!/*
*/!][!INDENT "0"!]
#endif[!/*
*/!][!ENDINDENT!][!/*
*/!][!ELSE!][!/*

*/!]  [!IF "DcmDspRoutineFixedLength = 'false'"!]
    retVal = [!"node:ref(concat('DcmDsp',$functionName,'RoutineFnc'))"!]([!"$inParamList"!][!"$PmsginParam"!]OpStatus, [!"$outParamList"!][!"$PmsgoutParam"!]&currentDataLength, ErrorCode);[!/*
*/!]  [!ELSE!]
    retVal = [!"node:ref(concat('DcmDsp',$functionName,'RoutineFnc'))"!]([!"$inParamList"!]OpStatus, [!"$outParamList"!]ErrorCode);[!/*
*/!]  [!ENDIF!]

    if ( retVal == E_OK )
    {[!/*
*/!][!INDENT "6"!][!/*
*/!]  [!IF "DcmDspRoutineFixedLength = 'false'"!][!/*
*/!]    [!IF "num:i($outFixedDataLength) > 0"!][!/*
*/!]      [!IF "$DcmRoutineVarLenInBytes = 'false'"!]
      pMsgContext->resDataLen = (PduLengthType)DCM_GET_BYTES([!"num:i($outFixedDataLength)"!] + currentDataLength) + 3U;[!/*
*/!]      [!ELSE!]
      if((currentDataLength * 8U) > [!"num:i($outVariableDataLength)"!]U)
      {[!/*
*/!]       [!INDENT "8"!]
        pMsgContext->resDataLen = (PduLengthType)DCM_GET_BYTES([!"num:i($outFixedDataLength)"!]U + [!"num:i($outVariableDataLength)"!]U) + 3U;[!/*
*/!]       [!ENDINDENT!]
      }
      else
      {[!/*
*/!]      [!INDENT "8"!]
        pMsgContext->resDataLen = (PduLengthType)DCM_GET_BYTES([!"num:i($outFixedDataLength)"!]U) + currentDataLength + 3U;[!/*
*/!]      [!ENDINDENT!]
      }[!/*
*/!]      [!ENDIF!][!/*
*/!]    [!ELSE!][!/*
*/!]      [!IF "$DcmRoutineVarLenInBytes = 'false'"!]
      pMsgContext->resDataLen = (PduLengthType)DCM_GET_BYTES(currentDataLength) + 3U;[!/*
*/!]      [!ELSE!]
      if((currentDataLength * 8U) > [!"num:i($outVariableDataLength)"!]U)
      {[!/*
*/!]        [!INDENT "8"!]
        pMsgContext->resDataLen = (PduLengthType)DCM_GET_BYTES([!"num:i($outVariableDataLength)"!]U) + 3U;[!/*
*/!]        [!ENDINDENT!]
      }
      else
      {[!/*
*/!]        [!INDENT "8"!]
        pMsgContext->resDataLen = currentDataLength + 3U;[!/*
*/!]        [!ENDINDENT!]
      }[!/*
*/!]      [!ENDIF!][!/*
*/!]    [!ENDIF!][!/*
*/!]  [!ELSE!]
      pMsgContext->resDataLen = [!"num:i((($outFixedDataLength + 7) div 8) + 3)"!]U;[!/*
*/!]  [!ENDIF!][!/*
*/!]  [!VAR "outVarCount"="0"!][!/*

*/!]  [!IF "(node:exists(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'Out')))"!][!/*
*/!]    [!LOOP "node:order(node:ref(concat(DcmDspRoutineInfoRef,'/DcmDsp',$parameterName,'Out'))/*[name(.) = concat('DcmDsp',$parameterName,'OutSignal')]/*, 'DcmDspRoutineSignalPos')"!][!/*
*/!]      [!IF "(DcmDspRoutineSignalType = 'VARIABLE_LENGTH')"!][!/*
*/!]        [!IF "$CommonBufferCount = 0"!][!/*
*/!]          [!/* Rx and Tx buffers are different and Variable length signal is not byte alligned */!][!/*
*/!]          [!IF "not((DcmDspRoutineSignalPos mod 8) = '0') "!][!/*
*/!]            [!/* Variable length signal is not byte alligned */!][!/*
*/!]            [!IF "$DcmRoutineVarLenInBytes = 'false'"!]
      Dcm_DspInternal_ShiftBitsInBuffer( (P2VAR(uint8, AUTOMATIC, DCM_VAR))&(pMsgContext->resData[3]), [!"num:i($outFixedDataLength + (8- ($outFixedDataLength mod 8)))"!]U,[!"$outFixedDataLength"!]U,currentDataLength, TRUE);[!/*
*/!]            [!ELSE!]
      Dcm_DspInternal_ShiftBitsInBuffer( (P2VAR(uint8, AUTOMATIC, DCM_VAR))&(pMsgContext->resData[3]), [!"num:i($outFixedDataLength + (8- ($outFixedDataLength mod 8)))"!]U,[!"$outFixedDataLength"!]U,(currentDataLength * 8), TRUE);[!/*
*/!]            [!ENDIF!][!/*
*/!]          [!ENDIF!][!/*
*/!]        [!ELSE!][!/*
*/!]          [!/* Rx and Tx buffers are the same, an auxiliary buffer needs to be used */!]
      Dcm_DspInternal_CopySignalData((P2VAR(uint8, AUTOMATIC, DCM_VAR))&Dcm_RoutineControlBuffer, (P2VAR(uint8, AUTOMATIC, DCM_VAR))&(pMsgContext->resData[3]), 0U, [!"DcmDspRoutineSignalPos"!]U,  [!"DcmDspRoutineSignalLength"!]U);[!/*
*/!]        [!ENDIF!][!/*
*/!]      [!ELSE!][!/*
*/!]        [!IF "((DcmDspRoutineSignalType = 'SINT16') or (DcmDspRoutineSignalType = 'UINT16') or
                   (DcmDspRoutineSignalType = 'SINT32') or (DcmDspRoutineSignalType = 'UINT32')
                  )"!]
      if(curHsmConfig->endianessConversion == TRUE)
      {[!/*
*/!]          [!INDENT "8"!]
[!WS "2"!][!CALL "DCM_ENDIANNESS_CONVERSION_OUTPARAM"!][!/*
*/!]          [!ENDINDENT!]
      }[!/*
*/!]        [!ENDIF!]
      /* Deviation MISRA-2  */
      Dcm_DspInternal_CopySignalData((P2VAR(uint8, AUTOMATIC, DCM_VAR))&outParam[!"num:i($outVarCount)"!], (P2VAR(uint8, AUTOMATIC, DCM_VAR))&(pMsgContext->resData[3]), 0U, [!"DcmDspRoutineSignalPos"!]U,  [!"DcmDspRoutineSignalLength"!]U);[!/*
*/!]      [!ENDIF!][!/*
*/!]      [!VAR "outVarCount"="$outVarCount + 1"!][!/*
*/!]    [!ENDLOOP!][!/*
*/!]  [!ENDIF!][!/*
*/!]  [!ENDINDENT!]
    }[!/*
*/!][!ENDIF!][!/*
*/!][!ENDINDENT!]
  }
  else
  {[!/*
*/!][!INDENT "4"!][!/*
*/!][!/*---Invalid data length; return error---*/!]
    *ErrorCode = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
    retVal = E_NOT_OK;[!/*
*/!][!ENDINDENT!]
  }
  return retVal;[!/*
*/!][!ENDINDENT!]
}
[!ENDMACRO!]
