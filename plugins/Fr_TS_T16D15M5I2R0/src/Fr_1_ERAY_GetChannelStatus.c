/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */
/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */

/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/
/******************************************************************************
 Global Functions
******************************************************************************/

/* check whether the API is enabled or not */
#if (FR_1_ERAY_GETCHANNELSTATUS_API_ENABLE == STD_ON)

/* start code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */


/**
 * \brief   Reads and resets the FlexRay aggregated channel status.
 *
 * \param Fr_CtrlIdx (in)                FlexRay controller index.
 * \param Fr_ChannelStatusPtr (out)      Address to write the aggregated channel
 *                                       status to.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 */
FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetChannelStatus
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelAStatusPtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelBStatusPtr
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_GETCHANNELSTATUS_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETCHANNELSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETCHANNELSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check pointer for being valid */
    else if( (Fr_ChannelAStatusPtr == NULL_PTR) || (Fr_ChannelBStatusPtr == NULL_PTR) )
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_GETCHANNELSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }
    else
#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */

    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_GETCHANNELSTATUS_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }
        else
        /* perform an FlexRay CC sync-check */
        {
            /* read ERAY status vector register */
            CONST(FCAL_ERAY_RegSegBType,AUTOMATIC) RegCCSV_B0 =
                FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    CCSV,
                    B0
                    );

            /* evaluate the POC-state */
            CONST(uint32,AUTOMATIC) tmpPOCState =
                FCAL_ERAY_GET_BIT(RegCCSV_B0,CCSV,POCS,B0);

            /* check if POC state is "normal-active" or "normal-passive" */
            /* if not abort function execution */
            if((tmpPOCState != ((VAR(uint32,AUTOMATIC))FCAL_ERAY_CCSV_POCS_NORMAL_ACTIVE)) &&
               (tmpPOCState != ((VAR(uint32,AUTOMATIC))FCAL_ERAY_CCSV_POCS_NORMAL_PASSIVE)) )
            {
                retval = E_NOT_OK;
            }
        }
        if(retval == E_OK)
        {
            /* read symbol window and NIT status */
            const FCAL_ERAY_RegSegHType RegSWNIT_H0 =
                FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    SWNIT,
                    H0);

            /* read aggregated channel status register */
            const FCAL_ERAY_RegSegHType RegACS_H0 =
                FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    ACS,
                    H0);

            /* reset symbol window and NIT status */
            /* reset only errors already detected! */
            FCAL_ERAY_SET(
                    FCAL_ERAY_GetCCHandle(),
                    SWNIT,
                    H0,
                    FCAL_ERAY_ALIGN_VALUE(RegSWNIT_H0,H0));

            /* reset aggregated channel status */
            /* reset only errors already detected! */
            FCAL_ERAY_SET(
                    FCAL_ERAY_GetCCHandle(),
                    ACS,
                    H0,
                    FCAL_ERAY_ALIGN_VALUE(RegACS_H0,H0));

            /* write symbol window and NIT status Channel A */
            *Fr_ChannelAStatusPtr = (uint16)(
                (FCAL_ERAY_RegSegHType)((FCAL_ERAY_RegSegHType) RegSWNIT_H0 &
                                        (FCAL_ERAY_RegSegHType) FCAL_ERAY_SWNIT_MTSA_H0_MASK) <<2U);

            *Fr_ChannelAStatusPtr |= (uint16)(
                (FCAL_ERAY_RegSegHType)((FCAL_ERAY_RegSegHType) RegSWNIT_H0 &
                (FCAL_ERAY_RegSegHType)((FCAL_ERAY_RegSegHType) FCAL_ERAY_SWNIT_SESA_H0_MASK |
                (FCAL_ERAY_RegSegHType)((FCAL_ERAY_RegSegHType) FCAL_ERAY_SWNIT_SBSA_H0_MASK |
                                        (FCAL_ERAY_RegSegHType) FCAL_ERAY_SWNIT_TCSA_H0_MASK))) <<9U);

            *Fr_ChannelAStatusPtr |= (uint16)(
                (FCAL_ERAY_RegSegHType)((FCAL_ERAY_RegSegHType) RegSWNIT_H0 &
                (FCAL_ERAY_RegSegHType)((FCAL_ERAY_RegSegHType) FCAL_ERAY_SWNIT_SENA_H0_MASK |
                                        (FCAL_ERAY_RegSegHType) FCAL_ERAY_SWNIT_SBNA_H0_MASK)) <<4U);

            /* write aggregated Channel A status */
            *Fr_ChannelAStatusPtr |= (uint16)((FCAL_ERAY_RegSegHType)RegACS_H0 &
                (FCAL_ERAY_RegSegHType)((FCAL_ERAY_RegSegHType) FCAL_ERAY_ACS_VFRA_H0_MASK |
                (FCAL_ERAY_RegSegHType)((FCAL_ERAY_RegSegHType) FCAL_ERAY_ACS_SEDA_H0_MASK |
                (FCAL_ERAY_RegSegHType)((FCAL_ERAY_RegSegHType) FCAL_ERAY_ACS_CEDA_H0_MASK |
                (FCAL_ERAY_RegSegHType)((FCAL_ERAY_RegSegHType) FCAL_ERAY_ACS_CIA_H0_MASK  |
                                        (FCAL_ERAY_RegSegHType) FCAL_ERAY_ACS_SBVA_H0_MASK)))));

            /* write symbol window and NIT status Channel B */
            *Fr_ChannelBStatusPtr = (uint16)(
                (FCAL_ERAY_RegSegHType)((FCAL_ERAY_RegSegHType) RegSWNIT_H0 &
                                        (FCAL_ERAY_RegSegHType) FCAL_ERAY_SWNIT_MTSB_H0_MASK) << 1U);

            *Fr_ChannelBStatusPtr |= (uint16)(
                (FCAL_ERAY_RegSegHType) ((FCAL_ERAY_RegSegHType) RegSWNIT_H0&
                (FCAL_ERAY_RegSegHType) ((FCAL_ERAY_RegSegHType) FCAL_ERAY_SWNIT_SESB_H0_MASK |
                (FCAL_ERAY_RegSegHType) ((FCAL_ERAY_RegSegHType) FCAL_ERAY_SWNIT_SBSB_H0_MASK |
                                         (FCAL_ERAY_RegSegHType) FCAL_ERAY_SWNIT_TCSB_H0_MASK))) <<6U);

            *Fr_ChannelBStatusPtr |= (uint16)(
                (FCAL_ERAY_RegSegHType) ((FCAL_ERAY_RegSegHType) RegSWNIT_H0 &
                (FCAL_ERAY_RegSegHType) ((FCAL_ERAY_RegSegHType) FCAL_ERAY_SWNIT_SENB_H0_MASK |
                                         (FCAL_ERAY_RegSegHType) FCAL_ERAY_SWNIT_SBNB_H0_MASK)) <<2U);

            /* write aggregated Channel B status */
            *Fr_ChannelBStatusPtr |= (uint16)(
                (FCAL_ERAY_RegSegHType) ((FCAL_ERAY_RegSegHType) RegACS_H0&
                (FCAL_ERAY_RegSegHType) ((FCAL_ERAY_RegSegHType) FCAL_ERAY_ACS_VFRB_H0_MASK |
                (FCAL_ERAY_RegSegHType) ((FCAL_ERAY_RegSegHType) FCAL_ERAY_ACS_SEDB_H0_MASK |
                (FCAL_ERAY_RegSegHType) ((FCAL_ERAY_RegSegHType) FCAL_ERAY_ACS_CEDB_H0_MASK |
                (FCAL_ERAY_RegSegHType) ((FCAL_ERAY_RegSegHType) FCAL_ERAY_ACS_CIB_H0_MASK  |
                                         (FCAL_ERAY_RegSegHType) FCAL_ERAY_ACS_SBVB_H0_MASK))))) >>8U);
        }
    }
    return retval;
}

/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

#endif  /* (FR_1_ERAY_GETCHANNELSTATUS_API_ENABLE == STD_ON) */

