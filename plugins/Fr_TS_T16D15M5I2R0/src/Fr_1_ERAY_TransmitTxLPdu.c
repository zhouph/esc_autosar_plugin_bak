/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */



/*
 * Misra-C:2004 Deviations:
 *
 * MISRA-3) Deviated Rule: 15.2 (required)
 * An unconditional 'break' statement shall terminate every non-empty switch clause.
 *
 * Reason:
 * For this memory copy routine, a manually loop unrolling is performed using a switch statement.
 * Inside the switch statement, fall throughs are used intentionally.
 *
 */
/* !LINKSTO Fr.ASR40.FR074,1 */
/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */
/******************************************************************************
 Local Macros
******************************************************************************/
/* adds another nibble to CRC intermediate result */
#define partialCRC_nibble(nHeaderCRC,nInput) \
    ((uint16)((uint16)((uint16)((uint16)(nHeaderCRC) << 4) & \
      (uint16)0x7ffU \
     ) ^ \
     FR_1_ERAY_headercrctable[ \
         (uint16)((uint16)((uint16)((nHeaderCRC) >> 7) & (uint16)0xfU) ^ \
         ((uint16)((nInput)&0xfU))) \
         ] \
    ))

/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/

/* start constant section declaration */
#define FR_1_ERAY_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

#if (FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE == STD_ON)
/* header CRC helper table */
STATIC CONST(uint16,FR_1_ERAY_CONST)
    FR_1_ERAY_headercrctable[16] =
{
    0x0000U, 0x0385U, 0x070AU, 0x048FU,
    0x0591U, 0x0614U, 0x029BU, 0x011EU,
    0x00A7U, 0x0322U, 0x07ADU, 0x0428U,
    0x0536U, 0x06B3U, 0x023CU, 0x01B9U
};

#endif /* FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE */


STATIC CONST(uint32,FR_1_ERAY_CONST) Fr_1_ERAY_PaddingPattern =
    (((uint32) FR_1_ERAY_PAYLOAD_PADDING_U8_VALUE << 24) |
     ((uint32) FR_1_ERAY_PAYLOAD_PADDING_U8_VALUE << 16) |
     ((uint32) FR_1_ERAY_PAYLOAD_PADDING_U8_VALUE << 8) |
      (uint32) FR_1_ERAY_PAYLOAD_PADDING_U8_VALUE
    );

/* stop constant section declaration */
#define FR_1_ERAY_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */


/******************************************************************************
 Local Functions
******************************************************************************/

/* start code section declaration */
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

#if (FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE == STD_ON)

/**
 * \brief   Configures a single ERAY Buffer
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller to initialize.
 * \param nBufIdx (in)               Message buffer Index.
 * \param nWRHS1 (in)                Content for register WRHS1.
 * \param nWRHS2 (in)                Content for register WRHS2.
 * \param nWRHS3 (in)                Content for register WRHS3.
 *
 * \note    This function can be called in POC-state 'ready' only.
 * \note    This function leaves the FlexRay CC in POC-state "ready".
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 */
STATIC INLINE FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_ConfigBufferAndCommitPayload
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16_least,AUTOMATIC) nBufIdx,
        VAR(uint32,AUTOMATIC) nWRHS1,
        VAR(uint32,AUTOMATIC) nWRHS2,
        VAR(uint32,AUTOMATIC) nWRHS3
    );

STATIC INLINE FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_ConfigBufferAndCommitPayload
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16_least,AUTOMATIC) nBufIdx,
        VAR(uint32,AUTOMATIC) nWRHS1,
        VAR(uint32,AUTOMATIC) nWRHS2,
        VAR(uint32,AUTOMATIC) nWRHS3
    )
{
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* This is a local API function only - avoid DET-Testing since this API function
     * is called by the well known, trusted API Fr_1_ERAY_ControllerInit().
     */

    /* parameter might not be used - depends on porting implementation*/
    TS_PARAM_UNUSED(Fr_CtrlIdx);

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);


    /* write register WRHS1 to ERAY */
    FCAL_ERAY_SET(
        FCAL_ERAY_GetCCHandle(),
        WRHS1,
        W0,
        nWRHS1
        );

    /* write register WRHS2 to ERAY */
    FCAL_ERAY_SET(
        FCAL_ERAY_GetCCHandle(),
        WRHS2,
        W0,
        nWRHS2
        );

    /* write register WRHS3 to ERAY */
    FCAL_ERAY_SET(
        FCAL_ERAY_GetCCHandle(),
        WRHS3,
        W0,
        nWRHS3
        );

    /* set "load header section" command */
    FCAL_ERAY_SETDC(
        FCAL_ERAY_GetCCHandle(),
        IBCM,
        B0,
        FCAL_ERAY_ALIGN_VALUE(FCAL_ERAY_IBCM_LHSH_W0_MASK|
                              FCAL_ERAY_IBCM_LDSH_W0_MASK|
                              FCAL_ERAY_IBCM_STXRH_W0_MASK
                              ,B0)
        );

    /* start buffer transfer */
    FCAL_ERAY_SETDC(
        FCAL_ERAY_GetCCHandle(),
        IBCR,
        B0,
        FCAL_ERAY_ALIGN_VALUE(nBufIdx,B0)
        );

}

#endif /* FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE */

/**
 * \brief   Transmits a LPdu.
 *
 * This service writes the payload of a LPdu into a FlexRay CC transmit buffer and commits it for
 * transmission. The service ensures that the byte order on the network is equal to the byte order
 * in memory (lower address - first, higher address - later).
 *
 * Note that this service doesn't check the transmit message buffer for being correctly configured.
 * Rational: The MCG ensures that buffer optimization is used only if Fr_PrepareLPdu() and
 * Fr_TransmitTxLPdu() are executed within a single FrIf Job. Therefore an invalid mesage buffer
 * configuration caused by reconfiguration is avoided.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * \param[in] Fr_CtrlIdx       FlexRay controller index.
 * \param[in] Fr_LPduIdx       LPdu index of PDU used for LSdu transmission.
 * \param[in] Fr_LSduPtr       Payload data pointer.
 * \param[in] Fr_LSduLength    Payload data length (in units of bytes).
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 */
 STATIC INLINE FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_TransmitTxLPdu_Internal
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx,
        P2CONST(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LSduPtr,
        VAR(uint8,AUTOMATIC) Fr_LSduLength
    );

STATIC INLINE FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_TransmitTxLPdu_Internal
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx,
        P2CONST(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LSduPtr,
        VAR(uint8,AUTOMATIC) Fr_LSduLength
    )
{
    Std_ReturnType retval = E_OK;
    
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

    TS_PARAM_UNUSED(Fr_CtrlIdx);

    /* implementation of funcionality */
    {
        /* get pointer to controller configuration */
        CONSTP2CONST(Fr_1_ERAY_CtrlCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pCtrlCfg = 
             &FR_1_ERAY_GET_CONFIG_ADDR
            (
                Fr_1_ERAY_CtrlCfgType,
                gFr_1_ERAY_ConfigPtr->pCtrlCfg
            )[FR_1_ERAY_CTRLIDX];
            
        /* get buffer configuration data structure */
        CONSTP2CONST(Fr_1_ERAY_BufferCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pLPdu =
            &FR_1_ERAY_GET_CONFIG_ADDR
            (
                Fr_1_ERAY_BufferCfgType,
                pCtrlCfg->pLPduCfg
            )[Fr_LPduIdx];


        /* get physical buffer index */
        CONST(uint8,AUTOMATIC) PhysBufIdx = pLPdu->nBufferCfgIdx;

/* check if development error detection is enabled */
#if ((FR_1_ERAY_DEV_ERROR_DETECT == STD_ON) || (FR_1_ERAY_PAYLOAD_PADDING_ENABLE == STD_ON))
        /* get buffer configured payload length */
        CONST(uint8,AUTOMATIC) BufPLLengthBytes = pLPdu->nPayloadLength;
#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */

        if(PhysBufIdx != FR_1_ERAY_INVALID_MSGBUFFER_INDEX)
        {
/* check if development error detection is enabled */
#if ((FR_1_ERAY_DEV_ERROR_DETECT == STD_ON) || (FR_1_ERAY_PAYLOAD_PADDING_ENABLE == STD_ON))

            if(Fr_LSduLength > BufPLLengthBytes)
            {
                /* Report to DET */
                FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_LENGTH, FR_1_ERAY_TRANSMITTXLPDU_SERVICE_ID);

                /* return error status */
                retval = E_NOT_OK;
            }
            else            
#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */


            /* check for complete buffer transfer (input buffer is ready) */
            if(Fr_1_ERAY_WaitWhileIBFBusy(FR_1_ERAY_CTRLIDX,FR_1_ERAY_TRANSMITTXLPDU_SERVICE_ID) != E_OK)
            {
                retval = E_NOT_OK;
            }
            else
            {

                /* write payload data to input buffer */
                /* Deviation MISRA-3 <+8> */
                FCAL_ERAY_MEM_TO_CTRL_NBO_ALIGN8(
                    FCAL_ERAY_GetCCHandle(),
                    FCAL_ERAY_WRDSn_W0_OFFSET,
                    Fr_LSduLength,
                    Fr_LSduPtr,
                    APPL_DATA
                    );

                {

#if (FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE == STD_ON)

                    /* get config parameter nFIDExt2 into local variable (since used several times) */
                    CONST(uint16_least,AUTOMATIC) nFIDExt2 = pLPdu->nFID_Ext2;

                    /* is this a dynamic payload length buffer (1:1 mapping to LPdu) ? */
                    if(FR_1_ERAY_GET_LPDUMODE(nFIDExt2) == FR_1_ERAY_LPDUMODE_DYNAMIC_PAYLOADLENGTH)
                    {

                        /* yes it is - load init values from config */
                        /* get frame payload length in 16bit-words */
                        CONST(uint8,AUTOMATIC) nPayloadLengthWords = (Fr_LSduLength + 1U)/2U;

                        /* read config value from configuration */
                        CONST(uint16,AUTOMATIC) nCRCExt1 = pLPdu->nCRC_Ext1;

                        /* variables holding the buffer configuration register values */
                        /* ERAY WRHS1 register variable */
                        CONST(uint32,AUTOMATIC) RegWRHS1_W0 =
                            FR_1_ERAY_GET_WRHS1(nCRCExt1,nFIDExt2,pLPdu->nCycle);

                        /* ERAY WRHS2 register variable */
                        /* don't consider CRC now */
                        CONST(uint32,AUTOMATIC) RegWRHS2tmp_W0 =
                            FR_1_ERAY_GET_WRHS2(0x00U,(uint32)nPayloadLengthWords);

                        /* create input value for remaining CRC calculation */
                        CONST(uint16,AUTOMATIC) CRCInput =
                            ((uint16)nPayloadLengthWords|((uint16)(RegWRHS1_W0<<7U)));

                        /* ERAY WRHS3 register variable */
                        CONST(uint32,AUTOMATIC) RegWRHS3_W0 =
                            FR_1_ERAY_GET_WRHS3(pLPdu->nDataPointer);

                        /* get intermediate CRC result (last two nibbles missing) */
                        const uint32 nCRCInit =
                            FR_1_ERAY_GET_WRHS2(nCRCExt1,(uint32)0x00U);

                        uint16 nCRC = (uint16)nCRCInit;

                        /* complete CRC calculation (last 2 nibbles) */
                        nCRC = partialCRC_nibble(nCRC,CRCInput>>4U);
                        nCRC = partialCRC_nibble(nCRC,CRCInput);

                        /* finally, initialize the message buffer */
                        Fr_1_ERAY_ConfigBufferAndCommitPayload(FR_1_ERAY_CTRLIDX,
                                                       PhysBufIdx,
                                                       RegWRHS1_W0,
                                                       RegWRHS2tmp_W0|nCRC,
                                                       RegWRHS3_W0);

                    }
                    else
#endif /* FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE */
                    {

/* fill rest of buffer with padding bytes */
#if (FR_1_ERAY_PAYLOAD_PADDING_ENABLE == STD_ON)

                        FCAL_ERAY_MEM_TO_CTRL_PADDING(
                            FCAL_ERAY_GetCCHandle(),
                            FCAL_ERAY_WRDSn_W0_OFFSET,
                            Fr_LSduLength,
                            BufPLLengthBytes
                            );

#endif /* FR_1_ERAY_PAYLOAD_PADDING_ENABLE */

                        /* set input buffer commands */
                        FCAL_ERAY_SETDC(
                            FCAL_ERAY_GetCCHandle(),
                            IBCM,
                            B0,
                            FCAL_ERAY_ALIGN_VALUE((FCAL_ERAY_IBCM_LDSH_W0_MASK|FCAL_ERAY_IBCM_STXRH_W0_MASK),B0)
                            );

                        /* start input buffer transfer */
                        FCAL_ERAY_SETDC(
                            FCAL_ERAY_GetCCHandle(),
                            IBCR,
                            B0,
                            FCAL_ERAY_ALIGN_VALUE(PhysBufIdx,B0)
                            );
                    }
                }
            }
        }
    }
    return retval;
}

    /******************************************************************************
 Global Data
******************************************************************************/
/******************************************************************************
 Global Functions
******************************************************************************/

/**
 * \brief   Transmits a LPdu.
 *
 * \param Fr_CtrlIdx (in)      FlexRay controller index.
 * \param Fr_LPduIdx (in)      LPdu index of PDU used for LSdu transmission.
 * \param Fr_LSduPtr (in)      Payload data pointer.
 * \param Fr_LSduLength (in)   Payload data length (in units of bytes).
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 *
 */
FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_TransmitTxLPdu
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx,
        P2CONST(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LSduPtr,
        VAR(uint8,AUTOMATIC) Fr_LSduLength
    )
{
    Std_ReturnType retval = E_OK;
    
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_TRANSMITTXLPDU_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_TRANSMITTXLPDU_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }
#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_TRANSMITTXLPDU_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check whether configuration has an entry for this FlexRay CC Idx */
    else if(FR_1_ERAY_CTRLIDX >= gFr_1_ERAY_ConfigPtr->nNumCtrl)
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_TRANSMITTXLPDU_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;

    }

    /* check that the virtual buffer index is within bounds */
    else if(Fr_LPduIdx >= ((&FR_1_ERAY_GET_CONFIG_ADDR(Fr_1_ERAY_CtrlCfgType,
                                                  gFr_1_ERAY_ConfigPtr->pCtrlCfg)
                                                  [FR_1_ERAY_CTRLIDX])->nNumLPdus))
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_LPDU_IDX, FR_1_ERAY_TRANSMITTXLPDU_SERVICE_ID);

        /* Return negative status code */
        retval = E_NOT_OK;
    }

    /* check pointer for beeing valid */
    else if(Fr_LSduPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_TRANSMITTXLPDU_SERVICE_ID);

        /* return error code */
        retval = E_NOT_OK;
    }
    else
#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */
    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_TRANSMITTXLPDU_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }
        else
        {
            retval = Fr_1_ERAY_TransmitTxLPdu_Internal(FR_1_ERAY_CTRLIDX,Fr_LPduIdx,Fr_LSduPtr,Fr_LSduLength);
        }
    }

    /* Return function status OK */
    return retval;
}

/* stop code section declaration */
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */


