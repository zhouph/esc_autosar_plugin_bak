/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */
/******************************************************************************
 Include Section
******************************************************************************/

#include "Fr_1_ERAY_Priv.h"

/******************************************************************************
 Local Macros
******************************************************************************/

/*==================[version check]=========================================*/
/*------------------[AUTOSAR vendor identification check]-------------------*/

#if (!defined FR_1_ERAY_VENDOR_ID) /* configuration check */
#error FR_1_ERAY_VENDOR_ID must be defined
#endif

#if (FR_1_ERAY_VENDOR_ID != 1U) /* vendor check */
#error FR_1_ERAY_VENDOR_ID has wrong vendor id
#endif

/*------------------[AUTOSAR release version identification check]----------*/

#if (!defined FR_1_ERAY_AR_RELEASE_MAJOR_VERSION) /* configuration check */
#error FR_1_ERAY_AR_RELEASE_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (FR_1_ERAY_AR_RELEASE_MAJOR_VERSION != 4U)
#error FR_1_ERAY_AR_RELEASE_MAJOR_VERSION wrong (!= 4U)
#endif

#if (!defined FR_1_ERAY_AR_RELEASE_MINOR_VERSION) /* configuration check */
#error FR_1_ERAY_AR_RELEASE_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (FR_1_ERAY_AR_RELEASE_MINOR_VERSION != 0U )
#error FR_1_ERAY_AR_RELEASE_MINOR_VERSION wrong (!= 0U)
#endif

#if (!defined FR_1_ERAY_AR_RELEASE_REVISION_VERSION) /* configuration check */
#error FR_1_ERAY_AR_RELEASE_REVISION_VERSION must be defined
#endif

/* revision version check */
#if (FR_1_ERAY_AR_RELEASE_REVISION_VERSION != 3U )
#error FR_1_ERAY_AR_RELEASE_REVISION_VERSION wrong (!= 3U)
#endif

/*------------------[AUTOSAR module version identification check]-----------*/

#if (!defined FR_1_ERAY_SW_MAJOR_VERSION) /* configuration check */
#error FR_1_ERAY_SW_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (FR_1_ERAY_SW_MAJOR_VERSION != 5U)
#error FR_1_ERAY_SW_MAJOR_VERSION wrong (!= 5U)
#endif

#if (!defined FR_1_ERAY_SW_MINOR_VERSION) /* configuration check */
#error FR_1_ERAY_SW_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (FR_1_ERAY_SW_MINOR_VERSION < 2U)
#error FR_1_ERAY_SW_MINOR_VERSION wrong (< 2U)
#endif

#if (!defined FR_1_ERAY_SW_PATCH_VERSION) /* configuration check */
#error FR_1_ERAY_SW_PATCH_VERSION must be defined
#endif

/* patch version check */
#if (FR_1_ERAY_SW_PATCH_VERSION < 5U)
#error FR_1_ERAY_SW_PATCH_VERSION wrong (< 5U)
#endif
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/
/******************************************************************************
 Global Data
******************************************************************************/
/******************************************************************************
 Global Functions
******************************************************************************/

#if (FR_1_ERAY_VERSION_INFO_API == STD_ON)

/* start code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h>
#endif /* TS_MERGED_COMPILE */


/**
 * \brief   Returns the module version information.
 *
 * \param versioninfo (out)      Address the version information should be written to.
 *
 */
FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_GetVersionInfo
    (
        P2VAR(Std_VersionInfoType,AUTOMATIC,FR_1_ERAY_APPL_DATA) VersioninfoPtr
    )
{

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check if controller index is within supported bounds */
    if(VersioninfoPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_GETVERSIONINFO_SERVICE_ID);
        return;
    }
    else

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */
    {
        /* assign logistic values to structure members */
        VersioninfoPtr->vendorID = (VAR(uint16,AUTOMATIC)) FR_1_ERAY_VENDOR_ID;
        VersioninfoPtr->moduleID = (VAR(uint8,AUTOMATIC)) FR_1_ERAY_MODULE_ID;
        VersioninfoPtr->sw_major_version = (VAR(uint8,AUTOMATIC)) FR_1_ERAY_SW_MAJOR_VERSION;
        VersioninfoPtr->sw_minor_version = (VAR(uint8,AUTOMATIC)) FR_1_ERAY_SW_MINOR_VERSION;
        VersioninfoPtr->sw_patch_version = (VAR(uint8,AUTOMATIC)) FR_1_ERAY_SW_PATCH_VERSION;
    }
}

/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h>
#endif /* TS_MERGED_COMPILE */

#endif /* FR_1_ERAY_VERSION_INFO_API */

