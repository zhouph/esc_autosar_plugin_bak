/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */
/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */

/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/
/******************************************************************************
 Global Data
******************************************************************************/
/******************************************************************************
 Global Functions
******************************************************************************/


/* start code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

/**
 * \brief   Checks the transmission status of a previously transmitted LPdu.
 *
 * \param Fr_CtrlIdx (in)           FlexRay controller index.
 * \param Fr_LPduIdx (in)           Virtual Buffer used for LSdu transmission.
 * \param Fr_LPduInfoPtr (out)      Address LPdu transmit status should be written to.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 *
 */
FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_CheckTxLPduStatus
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx,
        P2VAR(Fr_TxLPduStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_TxLPduStatusPtr
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_CHECKTXLPDUSTATUS_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_CHECKTXLPDUSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_CHECKTXLPDUSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check whether configuration has an entry for this FlexRay CC Idx */
    else if(FR_1_ERAY_CTRLIDX >= gFr_1_ERAY_ConfigPtr->nNumCtrl)
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_CHECKTXLPDUSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;

    }

    /* check that the virtual buffer index is within bounds */
    else if(Fr_LPduIdx >= ((&FR_1_ERAY_GET_CONFIG_ADDR(Fr_1_ERAY_CtrlCfgType,
                                                  gFr_1_ERAY_ConfigPtr->pCtrlCfg)
                                                  [FR_1_ERAY_CTRLIDX])->nNumLPdus))
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_LPDU_IDX, FR_1_ERAY_CHECKTXLPDUSTATUS_SERVICE_ID);

        /* Return negative status code */
        retval = E_NOT_OK;
    }

    /* check status pointer for beeing valid */
    else if(Fr_TxLPduStatusPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_CHECKTXLPDUSTATUS_SERVICE_ID);

        /* return error code */
        retval = E_NOT_OK;
    }
    else
#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */

    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_CHECKTXLPDUSTATUS_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }
        else
        /* check whether buffer is pending for transmission or not */
        {
            /* get pointer to controller configuration */
            CONSTP2CONST(Fr_1_ERAY_CtrlCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pCtrlCfg =
                 &FR_1_ERAY_GET_CONFIG_ADDR
                (
                    Fr_1_ERAY_CtrlCfgType,
                    gFr_1_ERAY_ConfigPtr->pCtrlCfg
                )[FR_1_ERAY_CTRLIDX];

            /* get buffer configuration data structure */
            CONSTP2CONST(Fr_1_ERAY_BufferCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pLPdu =
                &FR_1_ERAY_GET_CONFIG_ADDR
                (
                    Fr_1_ERAY_BufferCfgType,
                    pCtrlCfg->pLPduCfg
                )[Fr_LPduIdx];

            /* get physical buffer index */
            CONST(uint8,AUTOMATIC) TmpBufferIdx = pLPdu->nBufferCfgIdx;

            /* this is a dummy buffer - do not configure and return immediately with OK */
            if(TmpBufferIdx == FR_1_ERAY_INVALID_MSGBUFFER_INDEX)
            {
                /* no new data available */

                /* no transmission request is pending */
                *Fr_TxLPduStatusPtr = FR_TRANSMITTED;
            }
            else
            {
                /* translate buffer index into TXREQ register */
                CONST(uint16,AUTOMATIC) TmpRegOffset = ((uint16)((uint16)TmpBufferIdx)>>3)&((uint16)0xFCU);

                /* translate buffer index into TXREQ register mask */
                CONST(FCAL_ERAY_RegSegWType,AUTOMATIC) TmpBufferMsk =
                    (FCAL_ERAY_RegSegWType)(0x01UL<<(((uint16)TmpBufferIdx)&0x1FU));

                /* calculate offset of TxRequest register to be read */
                CONST(uint16_least,AUTOMATIC) TxReqOffset =
                    (uint16_least)(FCAL_ERAY_TXRQn_W0_OFFSET + TmpRegOffset);

                /* read TX-request register */
                CONST(FCAL_ERAY_RegSegWType,AUTOMATIC) TmpBufferStatus =
                    FCAL_ERAY_GET_U32(
                        FCAL_ERAY_GetCCHandle(),
                        TxReqOffset
                        );

                /* check whether buffer transmission request is set or not */
                if((TmpBufferStatus&TmpBufferMsk) != ((FCAL_ERAY_RegSegWType)0UL))
                {
                    /* transmission request is pennding */
                    *Fr_TxLPduStatusPtr = FR_NOT_TRANSMITTED;
                }
                else
                {
                    /* no transmission request is pending */
                    *Fr_TxLPduStatusPtr = FR_TRANSMITTED;
                }
            }
        }
    }

    return retval;
}

/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

