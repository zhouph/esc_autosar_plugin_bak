/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */
/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */

/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/
/******************************************************************************
 Global Functions
******************************************************************************/

/* check whether the API is enabled or not */
#if (FR_1_ERAY_GETCLOCKCORRECTION_API_ENABLE == STD_ON)

/* start code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetClockCorrection
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2VAR(sint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_RateCorrectionPtr,
        P2VAR(sint32,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_OffsetCorrectionPtr
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_GETCLOCKCORRECTION_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETCLOCKCORRECTION_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETCLOCKCORRECTION_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check pointer for being valid */
    else if(Fr_RateCorrectionPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_GETCLOCKCORRECTION_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check pointer for being valid */
    else if(Fr_OffsetCorrectionPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_GETCLOCKCORRECTION_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }
    else
#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */

    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_GETCLOCKCORRECTION_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }
        else
        /* implemented functionality */
        {
            /* read rate correction register */
            CONST(FCAL_ERAY_RegSegHType,AUTOMATIC) RegRCV_H0 =
                FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    RCV,
                    H0
                    );

            /* read offset correction register */
            CONST(FCAL_ERAY_RegSegWType,AUTOMATIC) RegOCV_W0 =
                FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    OCV,
                    W0
                    );

            /*
             * strict consistency among both values is not ensured here, since the values are
             * interpreted mainly separately and have no common aggregated meaning
             */
             VAR(sint32,AUTOMATIC) OffsetCorrectionValue;

             VAR(sint16,AUTOMATIC) RateCorrectionValue;

            /* perform 32bit sign extension for offset correction value */
            if((RegOCV_W0&(((FCAL_ERAY_OCV_OCV_W0_MASK + 1U)>>1U))) != 0U)
            {
                /* sign bit is set */

                /* convert from 2-s complement into a positive number */
                /* bitwise invert, cut off non relevant bits, add 1 */
                const uint32 OffsetCorrectionValueTmp = (((uint32)~RegOCV_W0)&FCAL_ERAY_OCV_OCV_W0_MASK) + 1U;
                OffsetCorrectionValue = (sint32)OffsetCorrectionValueTmp;

                /* convert into negative number, by arithmetic operation */
                OffsetCorrectionValue *= -1;
            }
            else
            {
                /* sign bit is not set cleared */
                /* we can convert register content to a positive signed value */
                OffsetCorrectionValue = (sint32)RegOCV_W0;
            }


            /* perform 16bit sign extension for rate correction value */
            if((RegRCV_H0&(((FCAL_ERAY_RCV_RCV_W0_MASK + 1UL)>>1U))) != 0U)
            {
                /* sign bit is set */

                /* convert from 2-s complement into a positive number */
                /* bitwise invert, cut off non relevant bits, add 1 */
                const uint16 RatecorrectionValueTmp = (((uint16)~RegRCV_H0)&(uint16)FCAL_ERAY_RCV_RCV_W0_MASK) + 1U;
                RateCorrectionValue = (sint16)RatecorrectionValueTmp;

                /* convert into negative number, by arithmetic operation */
                RateCorrectionValue *= -1;
            }
            else
            {
                /* sign bit is not set cleared */
                /* we can convert register content to a positive signed value */
                RateCorrectionValue = (sint16)RegRCV_H0;
            }


            /* write rate correction value to output parameter */
            *Fr_RateCorrectionPtr = RateCorrectionValue;

            /* write offset correction value to output parameter */
            *Fr_OffsetCorrectionPtr = OffsetCorrectionValue;
        }
    }
    return retval;
}

/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

#endif  /* FR_1_ERAY_GETCLOCKCORRECTION_API_ENABLE */

