/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 * Misra-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 19.12 (required)
 * "There shall be at most one occurrence of the # or ## preprocessor operators in a single
 * macro definition."
 *
 * Reason:
 * Usage of multiple ## per macro definition improves readability in a high degree.
 * Order of ## examination (even if it was random) has no impact on result.
 *
 * MISRA-2) Deviated Rule: 19.13 (advisory)
 * "The # and ## preprocessor operators should not be used."
 *
 * Reason:
 * String concatenation is used to create efficient readable code.
 *
 */

#if !defined _FR_1_ERAY_MAL_RW_ALLOW_UNALIGNED_H_
#define _FR_1_ERAY_MAL_RW_ALLOW_UNALIGNED_H_

/* Deviation MISRA-1, MISRA-2 <START> */ 

/*
 * the appendix 'ALIGN8' means 8-bit aligned addresses (=unaligned)
 * the appendix 'ALIGN16' means 16-bit aligned addresses
 */
/*
 * read unsigned values from non-volatile addresses
 */
#define MAL_READ_U16_STREAM(prefix,addr,ptclass) \
         (*((P2CONST(uint16,AUTOMATIC,prefix##_##ptclass))(addr)))

#define MAL_READ_U32_STREAM(prefix,addr,ptclass) \
         (*((P2CONST(uint32,AUTOMATIC,prefix##_##ptclass))(addr)))

#define MAL_READ_U16_ALIGN8(prefix,addr,ptclass) MAL_READ_U16(prefix,addr,ptclass)
#define MAL_READ_U32_ALIGN8(prefix,addr,ptclass) MAL_READ_U32(prefix,addr,ptclass)
#define MAL_READ_U32_ALIGN16(prefix,addr,ptclass) MAL_READ_U32(prefix,addr,ptclass)

/*
 * write unsigned values to non-volatile addresses
 */
#define MAL_WRITE_U16_STREAM(prefix,addr,ptclass,val) \
        (*((P2VAR(uint16,prefix##_##ptclass,AUTOMATIC))(addr))=((uint16)(val)))

#define MAL_WRITE_U32_STREAM(prefix,addr,ptclass,val) \
        (*((P2VAR(uint32,prefix##_##ptclass,AUTOMATIC))(addr))=((uint32)(val)))

#define MAL_WRITE_U16_ALIGN8(prefix,addr,ptclass,val) MAL_WRITE_U16(prefix,addr,ptclass,val)
#define MAL_WRITE_U32_ALIGN8(prefix,addr,ptclass,val) MAL_WRITE_U32(prefix,addr,ptclass,val)
#define MAL_WRITE_U32_ALIGN16(prefix,addr,ptclass,val) MAL_WRITE_U32(prefix,addr,ptclass,val)

/*
 * read unsigned values from volatile addresses
 */
#define MAL_READ_VOLATILE_U16_ALIGN8(prefix,addr,ptclass) MAL_READ_VOLATILE_U16(prefix,addr,ptclass)
#define MAL_READ_VOLATILE_U32_ALIGN8(prefix,addr,ptclass) MAL_READ_VOLATILE_U32(prefix,addr,ptclass)
#define MAL_READ_VOLATILE_U32_ALIGN16(prefix,addr,ptclass) MAL_READ_VOLATILE_U32(prefix,addr,ptclass)

/*
 * write unsigned values to volatile addresses
 */
#define MAL_WRITE_VOLATILE_U16_ALIGN8(prefix,addr,ptclass,val) \
           MAL_WRITE_VOLATILE_U16(prefix,addr,ptclass,val)
#define MAL_WRITE_VOLATILE_U32_ALIGN8(prefix,addr,ptclass,val) \
           MAL_WRITE_VOLATILE_U32(prefix,addr,ptclass,val)
#define MAL_WRITE_VOLATILE_U32_ALIGN16(prefix,addr,ptclass,val) \
           MAL_WRITE_VOLATILE_U32(prefix,addr,ptclass,val)

/* Deviation MISRA-1, MISRA-2 <STOP> */ 

#endif /* _FR_1_ERAY_MAL_RW_ALLOW_UNALIGNED_H_ */

