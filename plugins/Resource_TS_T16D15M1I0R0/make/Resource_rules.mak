# \file
#
# \brief AUTOSAR Resource
#
# This file contains the implementation of the AUTOSAR
# module Resource.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Resource_src

Resource_src_FILES      +=  $(Resource_CORE_PATH)\src\Mcal.c \
                      $(Resource_CORE_PATH)\src\Mcal_WdgLib.c \
                      $(Resource_CORE_PATH)\src\Mcal_TcLib.c \
                      $(Resource_CORE_PATH)\src\Mcal_DmaLib.c \
                      $(Resource_CORE_PATH)\src\SafetyReport.c\
                      $(Resource_CORE_PATH)\src\Test_Print.c\
                      $(Resource_CORE_PATH)\src\Test_Time.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Resource_src_FILES :=
endif
