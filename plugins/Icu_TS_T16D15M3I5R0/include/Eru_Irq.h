
#ifndef ERU_IRQ_H
#define ERU_IRQ_H

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

#include "Std_Types.h"                                                        

#ifdef OS_KERNEL_TYPE                                                           
#include <Os.h>        /* OS interrupt services */                              
#endif


/*******************************************************************************
**                      Public Macro Definitions                              **
*******************************************************************************/

/************************* interrupt CATEGORY *********************************/

#ifdef IRQ_CAT1
#if (IRQ_CAT1 != 1)
#error IRQ_CAT1 already defined with a wrong value
#endif
#else
#define IRQ_CAT1                    (1)
#endif

#ifdef IRQ_CAT23
#if (IRQ_CAT23 != 2)
#error IRQ_CAT23 already defined with a wrong value
#endif
#else
#define IRQ_CAT23                    (2)
#endif



/* The name of the ISRs shall be the same name than the ISR       *
 * functions provided by Infineon                                 */

/*************************SCUERUSR0_ISR*********************************/          

#ifdef SCUERUSR0_ISR
#define IRQ_SCU_ERU_SR0 STD_ON
#define IRQ_SCU_ERU_SR0_PRIO    SCUERUSR0_ISR_ISR_LEVEL
#define IRQ_SCU_ERU_SR0_CAT     SCUERUSR0_ISR_ISR_CATEGORY
#else 
#define IRQ_SCU_ERU_SR0 STD_OFF
#endif

#endif /* #ifndef ERU_IRQ_H */

