#if (!defined NM_CBK_H)
#define NM_CBK_H

/**
 * \file
 *
 * \brief AUTOSAR Nm
 *
 * This file contains the implementation of the AUTOSAR
 * module Nm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*==================[inclusions]============================================*/

#include <ComStack_Types.h>   /* AUTOSAR Communication Stack types */

#include <Nm_Version.h>       /* Module's version declaration */
#include <Nm_Cfg.h>           /* Module configuration */

#if ((NM_DEV_ERROR_DETECT == STD_OFF)                   \
  && (NM_COORDINATOR_SUPPORT_ENABLED == STD_OFF))
/* !LINKSTO Nm124_ComM_Nm,1 */
#include <ComM_Nm.h>           /* ComM callback API for direct mapping */
#endif

/*==================[macros]================================================*/


#include <SchM_Nm.h>            /* Header of Schedule Manager for Nm */

#if (NM_COORDINATOR_SUPPORT_ENABLED == STD_ON)
/** \brief Work around for typos in the AUTOSAR SWS documents */
#define Nm_RemoteSleepCancelation(nmNetworkHandle)      \
  Nm_RemoteSleepCancellation(nmNetworkHandle)

#endif

#if ((NM_DEV_ERROR_DETECT == STD_OFF)                   \
  && (NM_COORDINATOR_SUPPORT_ENABLED == STD_OFF))

/* Mapping of Network Management callback API calls
 *
 * The set of macros map the Network Management callback API to the callback
 * API of the Communication Manager Module */

/** \brief Map Nm_NetworkMode() to ComM_Nm_NetworkMode()
 **
 ** \param[in] nmChannelHandle Identification of the NM-channel. */
#define Nm_NetworkMode(nmChannelHandle)         \
  ComM_Nm_NetworkMode(nmChannelHandle)

/** \brief Map Nm_NetworkStartIndication() to ComM_Nm_NetworkStartIndication()
 **
 ** \param[in] nmChannelHandle Identification of the NM-channel. */
#define Nm_NetworkStartIndication(nmChannelHandle)      \
  ComM_Nm_NetworkStartIndication(nmChannelHandle)

/** \brief Map Nm_BusSleepMode() to ComM_Nm_BusSleepMode()
 **
 ** \param[in] nmChannelHandle Identification of the NM-channel. */
#define Nm_BusSleepMode(nmChannelHandle)        \
  ComM_Nm_BusSleepMode(nmChannelHandle)

/** \brief Map Nm_PrepareBusSleepMode() to ComM_Nm_PrepareBusSleepMode()
 **
 ** \param[in] nmChannelHandle Identification of the NM-channel. */
#define Nm_PrepareBusSleepMode(nmChannelHandle) \
  ComM_Nm_PrepareBusSleepMode(nmChannelHandle)

#endif /* ((NM_DEV_ERROR_DETECT == STD_OFF)
        && (NM_COORDINATOR_SUPPORT_ENABLED == STD_OFF)) */

#if (NM_COORDINATOR_SUPPORT_ENABLED == STD_OFF)

/** \brief Notification that all other nodes are ready to sleep.
 **
 ** If the Nm coordinator support is disabled this API function is implemented
 ** as empty function-like macro, as it provides no functionality. */
#define Nm_RemoteSleepIndication(nmNetworkHandle)

/** \brief Notification that not all other nodes are ready to sleep.
 **
 ** If the Nm coordinator support is disabled this callback function is
 ** implemented as function-like macro. */
#define Nm_RemoteSleepCancellation(nmNetworkHandle)

#endif

/*==================[external function declarations]======================*/

#define NM_START_SEC_CODE
#include <MemMap.h>

#if ((NM_DEV_ERROR_DETECT == STD_ON)                    \
  || (NM_COORDINATOR_SUPPORT_ENABLED == STD_ON))

/** \brief Notifies that the network management has entered Network Mode.
 **
 ** This function gives a notification that the network management has entered
 ** Network Mode. The callback function enables transmission of application
 ** messages.
 **
 ** Preconditions:
 ** - The channel handle should be valid and the module should have been
 **   initialized for this channel (checked).
 **
 ** \param[in] nmChannelHandle Identification of the NM-channel.
 **
 ** \ServiceID{30}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous} */
extern FUNC(void, NM_CODE) Nm_NetworkMode
(
  const NetworkHandleType nmNetworkHandle
);

/** \brief Notification that the network management has entered Prepare
 ** Bus-Sleep Mode.
 **
 ** This function provides a notification that the network management has
 ** entered Prepare Bus-Sleep Mode. The callback function disables
 ** transmission of application messages.
 **
 ** Preconditions:
 ** - The channel handle should be valid and the module should have been
 **   initialized for this channel (checked).
 **
 ** \param[in] nmChannelHandle Identification of the NM-channel.
 **
 ** \ServiceID{31}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void, NM_CODE) Nm_PrepareBusSleepMode
(
  const NetworkHandleType nmNetworkHandle
);

/** \brief Notifies that Bus Sleep Mode has been entered.
 **
 ** This function provides a notification that the network management has
 ** entered Bus-Sleep Mode.
 **
 ** Preconditions:
 ** - The channel handle should be valid and the module should have been
 **   initialized for this channel (checked).
 **
 ** \param[in] nmChannelHandle Identification of the NM-channel.
 **
 ** \ServiceID{32}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous} */
extern FUNC(void, NM_CODE) Nm_BusSleepMode
(
   const NetworkHandleType nmNetworkHandle
);

/** \brief Notifies that Network Mode has been entered.
 **
 ** This function provides a notification that a NM-message has been received
 ** in the Bus-Sleep Mode, which indicates that some nodes in the network have
 ** already entered the Network Mode. The callback function starts the network
 ** management state machine.
 **
 ** Preconditions:
 ** - The channel handle should be valid and the module should have been
 **   initialized for this channel (checked).
 **
 ** \param[in] nmChannelHandle Identification of the NM-channel.
 **
 ** \ServiceID{33}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous} */
extern FUNC(void, NM_CODE) Nm_NetworkStartIndication
(
  const NetworkHandleType nmNetworkHandle
);

#endif /* ((NM_DEV_ERROR_DETECT == STD_ON)
        * || (NM_COORDINATOR_SUPPORT_ENABLED == STD_ON)) */


#if (NM_COORDINATOR_SUPPORT_ENABLED == STD_ON)

/** \brief Notification that all other nodes are ready to sleep.
 **
 ** This function provides a notification that the network management has
 ** detected that all other nodes are ready to sleep. The NM gateway checks
 ** if the Bus is still required.
 **
 ** If the Nm coordinator support is disabled this API function is implemented
 ** as empty function-like macro, as it provides no functionality.
 **
 ** \param[in] nmNetworkHandle Identification of the NM-channel */
extern FUNC(void, NM_CODE) Nm_RemoteSleepIndication
(
  const NetworkHandleType nmNetworkHandle
);

/** \brief Notification that not all other nodes are ready to sleep.
 **
 ** Notification that the network management has detected that not all other
 ** nodes on the network are longer ready to enter Bus-Sleep Mode.
 **
 ** If the Nm coordinator support is disabled this callback function is
 ** implemented as function-like macro.
 **
 ** This function is not specified in AUTOSAR R3.x Nm SWS but in the R4.0
 ** draft SWS.
 **
 ** \param[in] nmNetworkHandle Identification of the NM-channel */
extern FUNC(void, NM_CODE) Nm_RemoteSleepCancellation
(
  const NetworkHandleType nmNetworkHandle
);

/** \brief Notification that this is a suitable point in time to
 **        initiate the coordination algorithm on
 **
 ** Notification NM Coordinator functionality that this is a
 ** suitable point in time to initiate the coordination algorithm on
 **
 ** If the Nm coordinator support is disabled this callback function is
 ** implemented as function-like macro.
 **
 ** This function is not specified in AUTOSAR R3.x Nm SWS but in the R4.0
 ** draft SWS.
 **
 ** \param[in] nmNetworkHandle Identification of the NM-channel */
extern FUNC(void, NM_CODE) Nm_SynchronizationPoint
(
  const NetworkHandleType nmNetworkHandle
);

#endif

/** \brief Notification service to indicate that an attempt to send a NM message
 **        failed.
 **
 ** This function provides an indication that an attempt to send an NM message
 ** failed.
 **
 ** \param[in] nmNetworkHandle Identification of the NM-channel */
extern FUNC(void, NM_CODE) Nm_TxTimeoutException
(
  const NetworkHandleType nmNetworkHandle
);

#if (NM_CAR_WAKEUPRX_INDICATION == STD_ON)
/** \brief Callback function to indicate car wakeup
 **
 ** This function is responsible to manage the Car Wake Up (CWU)
 ** request and distribute the Request to other Nm channels
 **
 ** The functionality needs to be implemented by the customer.
 **
 ** \param[in] nmNetworkHandle Identification of the NM-channel */
extern FUNC(void, NM_CODE) Nm_CarWakeUpIndication
(
  const NetworkHandleType nmChannelHandle
);
#endif

#if (NM_BUS_SYNCHRONIZATION_ENABLED == STD_ON)
/** \brief Indicate Ready to Sleep
 **
 ** Sets an indication, when the NM Coordinator Sleep Ready
 ** bit in the Control Bit Vector is set
 **
 ** The functionality needs to be implemented by the customer, as it is not
 ** specified by AUTOSAR.
 **
 ** \param[in] nmNetworkHandle Identification of the NM-channel */
extern FUNC(void, NM_CODE) Nm_CoordReadyToSleepIndication
(
 const NetworkHandleType nmChannelHandle
);
#endif

#if (NM_STATE_CHANGE_IND_ENABLED == STD_ON)

/** \brief Notification that an Nm changed state
 **
 ** This function provides a notification that a network management has
 ** changed state.
 **
 ** \param[in] nmNetworkHandle Identification of the NM-channel */
extern FUNC(void, NM_CODE) Nm_StateChangeNotification
(
  const NetworkHandleType nmNetworkHandle,
  const Nm_StateType nmPreviousState,
  const Nm_StateType nmCurrentState
);

#endif

#if (NM_PDU_RX_INDICATION_ENABLED == STD_ON)

/** \brief Indication that an Rx Pdu was received
 **
 ** Indication that a bus network management has received an NM message.
 **
 ** The functionality needs to be implemented by the customer, as it is not
 ** specified by AUTOSAR.
 **
 ** \param[in] nmNetworkHandle Identification of the NM-channel */
extern FUNC(void, NM_CODE) Nm_PduRxIndication
(
  const NetworkHandleType nmNetworkHandle
);

#endif

#if (NM_REPEAT_MESSAGE_INDICATION == STD_ON)

/** \brief NM message with set Repeat Message Request Bit has been received.
 **
 ** Indication that a NM message with set Repeat Message Request Bit has been
 ** received.
 **
 ** The functionality needs to be implemented by the customer, as it is not
 ** specified by AUTOSAR.
 **
 ** \param[in] nmNetworkHandle Identification of the NM-channel */
extern FUNC(void, NM_CODE) Nm_RepeatMessageIndication
(
  const NetworkHandleType nmNetworkHandle
);

#endif

#if (NM_STATE_CHANGE_NOTIFICATION_CALLOUT == STD_ON)

/** \brief User specific callout function which will be called from
 **        callback function Nm_StateChangeNotification() to notify about the
 **        state changes within the BusNm modules.
 **
 ** The functionality needs to be implemented by the customer, as it is not
 ** specified by AUTOSAR.
 **
 ** \param[in] nmNetworkHandle Identification of the NM channel
 **            nmPreviousState Previous state of the NM channel
 **            nmCurrentState  Current(new) state of the NM channel
 */
extern FUNC(void, NM_CODE) Nm_StateChangeNotificationCallout
(
  const NetworkHandleType nmNetworkHandle,
  const Nm_StateType nmPreviousState,
  const Nm_StateType nmCurrentState
);

#endif

#define NM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
#endif /* if !defined( NM_CBK_H ) */
