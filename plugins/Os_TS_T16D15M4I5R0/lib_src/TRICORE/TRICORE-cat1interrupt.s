/* TRICORE-cat1interrupt.s
 *
 * This file contains the category 1 interrupt exit routine
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-cat1interrupt.s 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef OS_ASM
#define OS_ASM
#endif

#include <Os_kernel.h>
#include <Os_tool.h>
#include <TRICORE/Os_TRICORE_core.h>
#include <Os_cpuload_kernel.h>

	_GLOBAL		OS_Cat1Exit

#ifndef OS_EXCLUDE_CPULOAD

#if OS_CPULOAD == 0x10

#define OS_CPULOAD_BIT	4

#else
/* The OS_CPULOAD bit in OS_configMode has changed. The definition of OS_CPULOAD_BIT
 * and the test (above) need modifying to match.
*/
#error "Assembler code for checking OS_CPULOAD bit of OS_configMode needs attention"
#endif

	_EXTERN	OS_cpuLoad
	_EXTERN	OS_configMode
	_EXTERN	OS_MeasureCpuLoad

#endif


/* OS_Cat1Exit
 *
 * This routine is the interrupt exit routine for Category 1 interrupts. It
 * jumped to by the interrupt vector stub and exits via RSLCX/RFE.
*/
	_TEXT

OS_Cat1Exit:

#ifndef OS_EXCLUDE_CPULOAD

	movh.a	a15, _IMM(#,_hiword(OS_configMode))		/* Skip CPU load computation if not configured. */
	ld.w	d15,[a15]_loword(OS_configMode)
	jz.t	_REGBIT(d15, OS_CPULOAD_BIT), OS_SkipCpuLoad


	movh.a	a15, _IMM(#,_hiword(OS_cpuLoad))		/* Decrement/test the busy nesting counter. */
	lea		a15, [a15]_loword(OS_cpuLoad)
	ld.w	d15, [a15]OS_CPULOAD_BUSYNESTINGCOUNTER
	add		d15, _IMM(#,-1)
	disable
	st.w	[a15]OS_CPULOAD_BUSYNESTINGCOUNTER, d15
	jnz		d15, OS_SkipCpuLoad						/* Jump if interrupted thread was also busy */

	call	OS_MeasureCpuLoad						/* Perform the CPU load computation */

OS_SkipCpuLoad:

#endif

	rslcx
	rfe

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
