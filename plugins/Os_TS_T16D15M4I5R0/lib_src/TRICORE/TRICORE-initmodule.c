/* TRICORE-initmodule.c
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-initmodule.c 17634 2014-02-05 14:04:01Z tojo2507 $
*/

#include <Os_kernel.h>
#include <TRICORE/Os_TRICORE_core.h>
#include <Os_tool.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_InitModule()
 *
 * Initialises a Tricore peripheral module
 *
 * Many Tricore peripherals need initialising before they can be
 * used. Failure to do so results in an exception trap when the
 * module registers are accessed.
*/
void OS_InitModule
(	os_tricoremodule_t *m,
	os_uint32_t divider,
	os_uint32_t switchoff)
{
	os_uint32_t p;
	os_uint32_t clc;

	/* We must set ENDINIT while fiddling with the clock-control
	 * register.
	*/
	OS_ClearEndinit(p);	/* It's a macro! */

	/* Write the divider value to the clock control register. Set the
	 * clock switchoff bit if required (also needs the SBWE bit). Ensure
	 * all other "disable" bits are 0.
	*/
	clc = OS_CLC_RMCval(divider) & OS_CLC_RMC;

	if ( switchoff != 0 )
	{
		clc |= (OS_CLC_SBWE | OS_CLC_SPEN);
	}

	m->clc = clc;


	/* Flush the pipeline by reading the register back.
	 *
	 * This expression should read the location It should not
	 * result in "expression has no effect" or similar warning/error
	 * because the register is volatile.
	*/
	m->clc;

	OS_SetEndinit(p);
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
