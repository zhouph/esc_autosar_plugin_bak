/* TRICORE-getcoreid.s
 *
 * This file contains the OS_TricoreGetCoreId fucntion
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-getcoreid.s 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef OS_ASM
#define OS_ASM
#endif

#include <Os_kernel.h>
#include <Os_tool.h>
#include <TRICORE/Os_TRICORE_cpu.h>
#include <TRICORE/Os_TRICORE_core.h>
#include <TRICORE/Os_TRICORE_stm.h>

	_GLOBAL		OS_TricoreGetCoreId
	
/* OS_TricoreGetCoreId
 *
 * This routine returns the id of the core where the caller is executing. For derivates which do 
 * not have an id register it is just a NOP.
 */
	_TEXT

OS_TricoreGetCoreId:

#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
   _mfcr d2, _IMM(#,OS_CORE_ID)
#endif
   ret

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
