/* lib-fastresumeosinterrupts.c
 *
 * This file contains the OS_FastResumeOsInterrupts() function
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: lib-fastresumeosinterrupts.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_api.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_FastResumeOsInterrupts()
 *
 * Implements the ResumeOSInterrupts() services.
 *
 * This function restores the interrupt state previously saved by
 * OS_FastSuspendOsInterrupts(). The "OS" nesting counter
 * is decremented, and if it reaches zero the "OS" old level
 * is restored.
 *
 * Re-entrancy considerations:
 * Assuming a well-behaved application, the nesting count is invariant
 * through interrupts. The old level could get changed by an interrupt routine,
 * but only when the nesting count is zero. In this function, that happens when
 * the nesting count goes to zero. In that branch, the old level is already
 * copied to a local variable, so we do not use the (possibly modified)
 * old level after setting the nesting count to zero.
*/
void OS_FastResumeOsInterrupts(void)
{
	os_intstatus_t is;

	if ( OS_fastSuspendResumeStatus.osNestCount == 1 )
	{
		is = OS_fastSuspendResumeStatus.osOldLevel;
		OS_fastSuspendResumeStatus.osNestCount = 0;
		OS_IntRestore(is);
	}
	else
	{
		OS_fastSuspendResumeStatus.osNestCount--;
	}
}

#include <memmap/Os_mm_code_end.h>

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserResumeInterrupts</name>
  <synopsis>Resume interrupts up to a given level</synopsis>
  <syntax>
    void OS_UserResumeInterrupts(os_intlocktype_t locktype)
  </syntax>
  <description>
	<para>
    <code>OS_UserResumeInterrupts()</code> restores the interrupt level of the processor
    or interrupt controller to the level that it was before the corresponding call to
    <code>OS_UserSuspendInterrupts()</code>.
    It is used to implement the <code>ResumeOSInterrupts()</code>, <code>ResumeAllInterrupts()</code>
    and <code>DisableAllInterrupts()</code> system services by calling it with the <code>locktype</code>
    parameter equal to <code>OS_LOCKTYPE_OS</code>, <code>OS_LOCKTYPE_ALL</code> and
    <code>OS_LOCKTYPE_NONEST</code>, respectively.
    </para>
    <para>
    Both <code>ResumeOSInterrupts()</code> and <code>ResumeAllInterrupts()</code> are nestable;
    this is implemented by a counter. The interrupt level is only truly manipulated on the outermost
    of the nested calls.
    </para>
    <para>
    If <code>ResumeOSInterrupts()</code> is called from a permitted context other than a Task or
    Category 2 ISR it is a no-operation, or if it is called within a code section that is controlled
    a <code>ResumeAllInterrupts()</code> or <code>DisableAllInterrupts()</code>, it is treated as
    a no-operation since interrupts are already blocked at a higher level.
    </para>
    <para>
    Interrupt lock timing is implemented for Tasks and ISRs; timing state that was saved by
    the corresponding <code>OS_UserSuspendInterrupts()</code> is restored.
    </para>
  </description>
  <availability>
  </availability>
</api>

 * CHECK: PARSE
*/
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
