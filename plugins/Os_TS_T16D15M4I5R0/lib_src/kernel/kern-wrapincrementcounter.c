/* kern-wrapincrementcounter.c
 *
 * This file contains the OS_WrapIncrementCounter wrapper function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-wrapincrementcounter.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_WrapIncrementCounter
 *
 * This function calls the kernel OS_KernIncrementCounter function with
 * a counter id as specified in the counter part of the alarm parameter union.
 *
 * !LINKSTO Kernel.Autosar.OSEK.Differences.AlarmActionIncrementCounter, 1
*/
void OS_WrapIncrementCounter(const os_alarm_t *a)
{
	os_intstatus_t is;
#if OS_KERNEL_TYPE!=OS_MICROKERNEL
	os_uint8_t save;

	save = OS_inFunction;
	OS_inFunction = OS_ININTERNAL;
#endif

	is = OS_IntDisable();
	/* no way to propagate errors -> ignore return value */
	(void) OS_AdvanceCounter(&OS_counter[a->object], &OS_counterDynamic[a->object], 1, is);
	OS_IntRestore(is);

#if OS_KERNEL_TYPE!=OS_MICROKERNEL
	OS_inFunction = save;
#endif
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
