/* kern-starttaskintlocktiming.c
 *
 * This file contains the OS_StartTaskIntLockTiming function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-starttaskintlocktiming.c 18959 2014-09-25 13:25:33Z ingi2575 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_StartTaskIntLockTiming
 *
 * This function starts interrupt-lock timing for the task.
*/
void OS_StartTaskIntLockTiming(const os_task_t *tp, os_intlocktype_t locktype)
{
	os_taskaccounting_t *acc = OS_GET_ACCT(tp->accounting);
	os_tick_t locktime;
	os_tick_t used;
	os_tick_t remaining;

	OS_PARAM_UNUSED(tp);

	locktime = OS_GET_ILOCK((locktype == OS_LOCKTYPE_OS) ? tp->osLockTime : tp->allLockTime);

	if ( locktime != 0 )
	{
		OS_ResetExecTimingInterrupt();
		used = OS_GetTimeUsed();

		OS_accounting.etUsed += used;

		if ( OS_GET_ACCT(((tp->flags & OS_TF_MEASUREEXEC) != 0)
				&& (tp->accounting->etMax < OS_accounting.etUsed)) )
		{
			OS_SET_ACCT(tp->accounting->etMax = OS_accounting.etUsed);
		}

		if ( (OS_accounting.etLimit != 0) && (OS_accounting.etUsed >= OS_accounting.etLimit) )
		{
			OS_ExceedExecTime();
		}
		else
		{
			if ( OS_accounting.etLimit == 0 )
			{
				remaining = OS_MAXTICK;
			}
			else
			{
				remaining = OS_accounting.etLimit - OS_accounting.etUsed;
			}

			if ( locktype == OS_LOCKTYPE_OS )
			{
				acc->osSaveType = OS_accounting.etType;
				acc->osSaveUsed = OS_accounting.etUsed;
				acc->osSaveLimit = OS_accounting.etLimit;
			}
			else
			{
				acc->allSaveType = OS_accounting.etType;
				acc->allSaveUsed = OS_accounting.etUsed;
				acc->allSaveLimit = OS_accounting.etLimit;
			}

			/* Never allow a task to extend its execution budget by disabling
			 * interrupts!
			*/
			OS_accounting.etUsed = 0;

			if (locktime >= remaining)
			{	/* old remaining time is shorter than new time */
				/* use old time for accounting (and leave accounting type unchanged) */
				OS_accounting.etLimit = remaining;
			}
			else
			{	/* new time is shorter than old remaining time */
				/* use new time for accounting and set new accounting type */
				OS_accounting.etLimit = locktime;
				OS_accounting.inFunction = OS_INTASK;
				OS_accounting.etType = OS_ACC_INTLOCK;
			}

			OS_SetExecTimingInterrupt(OS_accounting.frc, OS_accounting.etLimit);
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
