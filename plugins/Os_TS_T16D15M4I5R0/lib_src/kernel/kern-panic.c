/* kern-panic.c
 *
 * This file contains the OS_Panic function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-panic.c 20444 2015-02-16 14:15:48Z ingi2575 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 8.10 (required)
 * Variable 'OS_panicCode' should have internal linkage.
 *
 * Reason:
 * The variable may be inspected to find the reason for calling the panic function.
 *
*/

#include <Os_kernel.h>
#include <Os_panic.h>

#include <memmap/Os_mm_var_begin.h>
/* Deviation MISRA-1 */
os_panic_t OS_panicCode;	/* = OS_PANIC_NONE */
#include <memmap/Os_mm_var_end.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_Panic
 *
 * This function is called whenever the kernel detects an internal error
 * from which it cannot recover.
*/
os_result_t OS_Panic(os_panic_t panicCode)
{
	OS_panicCode = panicCode;
#if OS_KERNEL_TYPE==OS_MICROKERNEL
	for (;;) {}
#else
	OS_Shutdown(OS_E_PANIC);
#endif
	return OS_E_PANIC;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
