/* kern-initmem.c
 *
 * This file contains the OS_InitMem function, which initialises the memory pool by placing the
 * given memory block into it.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-initmem.c 17649 2014-02-06 15:35:01Z tojo2507 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 16.7 (required)
 * A pointer parameter in a function prototype should be declared as pointer
 * to const if the pointer is not used to modify the addressed object.
 *
 * Reason:
 * The "mem" parameter is used to calculate lump addresses, which, in turn,
 * are used to in fact modify the addressed object. Due to the address and
 * alignment calculations the MISRA-C checker can't deduce this fact.
*/

#include <Os_kernel.h>
#include <Os_memalloc.h>

#include <memmap/Os_mm_var_begin.h>
/* The head of the free memory list
*/
os_memblock_t OS_memHead;		/* = {0,OS_NULL} */
#include <memmap/Os_mm_var_end.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_InitMem()
 *
 * This function adds an arbitrary chunk of memory to the free list. The memory is first rounded to a
 * whole number of "lumps" correctly aligned.
 *
 * CAVEAT: this whole thing works on the assumption that pointers and integers can be freely assigned
 * to each other without losing any meaning, so it won't work on weird paged architectures like XC2000
*/
/* Deviation MISRA-1 */
os_result_t OS_InitMem(void *mem, os_size_t nbytes)
{
	os_address_t memaddr = (os_address_t)mem;
	os_address_t lumpaddr = ((memaddr + OS_MEMLUMP) - 1u)
							& (os_address_t) ~((os_address_t) OS_MEMLUMP - 1u);
	os_size_t nlumps = (nbytes - (lumpaddr - memaddr)) >> OS_MEMLUMP_SHIFT;
	os_result_t result = OS_E_OK;

	if ( nlumps == 0 )
	{
		result = OS_E_VALUE;
	}
	else
	if ( OS_memHead.next == OS_NULL )
	{
		OS_memHead.next = (os_memblock_t *)lumpaddr;
		OS_memHead.next->size = nlumps;
		OS_memHead.next->next = OS_NULL;
	}
	else
	{
		result = OS_FreeMem((void *)lumpaddr, (nlumps << OS_MEMLUMP_SHIFT));
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
