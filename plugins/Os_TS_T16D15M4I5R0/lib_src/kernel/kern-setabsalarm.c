/* kern-setabsalarm.c
 *
 * This file contains the OS_KernSetAbsAlarm function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-setabsalarm.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if at least one application exists.
 */

#define OS_SID OS_SID_SetAbsAlarm
#define OS_SIF OS_svc_SetAbsAlarm

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernSetAbsAlarm
 *
 * The alarm is set to expire in the specified number of timer ticks.
 * The alarm's period is also set as specified.
 *
 * !LINKSTO Kernel.API.Alarms.SetAbsAlarm.API, 1
*/
os_result_t OS_KernSetAbsAlarm
(	os_alarmid_t a,
	os_tick_t start,
	os_tick_t cyc
)
{
	os_result_t r = OS_E_OK;
	const os_alarm_t *as;
	os_alarmdynamic_t *ad;
	const os_counter_t *cs;
	os_counterdynamic_t *cd;
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_appcontext_t *app;
	os_uint8_t state;
	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0, (os_paramtype_t)a );
	OS_SAVE_PARAMETER_N(1, (os_paramtype_t)start );
	OS_SAVE_PARAMETER_N(2, (os_paramtype_t)cyc );

	OS_TRACE_SETABSALARM_ENTRY(a,start,cyc);

	if ( !OS_CallingContextCheck() )
	{
		r = OS_ERROR(OS_ERROR_WrongContext, OS_GET_PARAMETER_VAR() );
	}
	else
	if ( !OS_InterruptEnableCheck(OS_IEC_AUTOSAR) )
	{
		r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_GET_PARAMETER_VAR());
	}
	else
	if ( ! OS_IsValidAlarmId( a ) )
	{
		/* !LINKSTO Kernel.API.Alarms.SetAbsAlarm.UnknownAlarm, 1
		*/
		r = OS_ERROR(OS_ERROR_InvalidAlarmId, OS_GET_PARAMETER_VAR());
	}
	else
	{
		as = &OS_alarmTableBase[a];

		app = OS_CurrentApp();

		if ( !OS_HasPermission(app, as->permissions) )
		{
			r = OS_ERROR(OS_ERROR_Permission, OS_GET_PARAMETER_VAR());
		}
		else
		{
			cs = &OS_counterTableBase[as->counter];

			if ( (start > cs->maxallowedvalue) ||
				 ( (cyc != 0) &&
				   ( (cyc < cs->mincycle) || (cyc > cs->maxallowedvalue) ) ) )
			{
				/* !LINKSTO Kernel.API.Alarms.SetAbsAlarm.InvalidIncrement, 1
				 * !LINKSTO Kernel.API.Alarms.SetAbsAlarm.InvalidCycle, 1
				*/
				r = OS_ERROR(OS_ERROR_ParameterOutOfRange, OS_GET_PARAMETER_VAR());
			}
			else
			{
				ad = &OS_alarmDynamicBase[a];
				cd = &OS_counterDynamicBase[as->counter];

				/* !LINKSTO Kernel.API.Alarms.SetAbsAlarm.CyclicAlarm, 1
				 * !LINKSTO Kernel.API.Alarms.SetAbsAlarm.SingleAlarm, 1
				 * !LINKSTO Kernel.API.Alarms.SetAbsAlarm.Start, 1
				*/
				state = OS_SetAlarm(a, ad, cs, cd, start, cyc, OS_FALSE);

				if ( state == OS_ALARM_IDLE )
				{
					/* Do nothing. This branch makes the normal exit quicker. */
				}
				else
				if ( state == OS_ALARM_QUARANTINED )
				{
					r = OS_ERROR(OS_ERROR_Quarantined, OS_GET_PARAMETER_VAR());
				}
				else
				{
					/* !LINKSTO Kernel.API.Alarms.SetAbsAlarm.AlreadyInUse, 1
					*/
					r = OS_ERROR(OS_ERROR_AlarmInUse, OS_GET_PARAMETER_VAR());
				}
			}
		}
	}

	OS_TRACE_SETABSALARM_EXIT_P(r,a,start,cyc);
	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserSetAbsAlarm</name>
  <synopsis>Set an alarm at an absolute counter value</synopsis>
  <syntax>
    os_result_t OS_UserSetAbsAlarm
    (   os_alarmid_t a,    /@ ID of the alarm @/
        os_tick_t start,   /@ Time of first expiry @/
        os_tick_t cyc      /@ Time of subsequent expiries @/
    )
  </syntax>
  <description>
    <code>OS_UserSetAbsAlarm()</code> sets the specified alarm to expire
    the next time that its counter reaches the <code>start</code> value and,
    if the <code>cyc</code> parameter is non zero, thereafter every
    <code>cyc</code> ticks of the counter.
    <para>The values of <code>start</code> and <code>cyc</code> must lie within
    the permitted range configured for the counter.</para>
    <para>The specified alarm must not already be in use.</para>
    <para>If the counter is about to reach the <code>start</code> value, the
    alarm could expire before <code>OS_UserSetAbsAlarm()</code> returns.</para>
    <para>If the counter has already reached the specified <code>start</code>
    value, the alarm will not expire until the counter wraps around and reaches
    the value again. Depending on the configuration of the counter, this could
    be a <emphasis>very</emphasis> long time.</para>
  </description>
  <availability>
  </availability>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
