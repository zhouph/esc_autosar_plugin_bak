/* mka-getactiveapplicationmode.c
 *
 * This file contains the GetActiveApplicationMode function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: mka-getactiveapplicationmode.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#include <Os_kernel.h>
#include <public/Mk_autosar.h>
#include <Os_api_microkernel.h>

/*!
 * GetActiveApplicationMode
 *
 * Returns the current "application mode" (startup mode)
*/
AppModeType GetActiveApplicationMode(void)
{
	return OS_mode;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
