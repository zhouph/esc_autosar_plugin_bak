/* kern-terminateapplication.c
 *
 * This file contains the OS_KernTerminateApplication function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-terminateapplication.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if at least one application exists.
 */

#define OS_SID	OS_SID_TerminateApplication
#define OS_SIF	OS_svc_TerminateApplication

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernTerminateApplication implements the API TerminateApplication
 *
 * The current application is terminated.
 * If the restart option is set to RESTART the restart task is activated.
 * If no application can be identified, or if called from a context other
 * than task, Cat2 ISR or ErrorHook, the error handler is called.
 *
 * !LINKSTO Kernel.Autosar.API.SystemServices.TerminateApplication, 2
 * !LINKSTO Kernel.Autosar.OsApplication.Shutdown, 1
*/
os_result_t OS_KernTerminateApplication(os_restart_t restart)
{
	os_result_t r = OS_E_OK;

#if (OS_KERNEL_TYPE!=OS_MICROKERNEL)
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_appcontext_t *app = OS_NULL;

	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0,(os_paramtype_t)restart);

	OS_TRACE_TERMINATEAPPLICATION_ENTRY(restart);

	/* Service may only be called from a task or ISR, or from
	 * the ErrorHook. But first, check the parameter.
	*/
	if ( (restart != OS_APPL_RESTART) && (restart != OS_APPL_NO_RESTART) )
	{
		/* !LINKSTO Kernel.Autosar.API.SystemServices.TerminateApplication.OutOfRange, 1
		*/
		r = OS_ERROR(OS_ERROR_InvalidRestartOption, OS_GET_PARAMETER_VAR());
	}
	else
	if ( OS_inFunction == OS_INERRORHOOK )
	{
		/* If called from the error hook, we allow the error handler to
		 * do the job for us, unless the preprogrammed action is SHUTDOWN,
		 * in which case we leave it at that.
		*/
		app = OS_hookApp;

		if ( OS_AppIsNull(app) )
		{
			/* Called from the global ErrorHook(). Under strict Autosar rules this is not permitted.
			 * Under more relaxed rules killing the current application from the global error hook might be
			 * considered a useful feature. However, it's tricky to determine the application when the
			 * error was caused in the application error hook, so we'll leave the implementation of
			 * the extra functionality out for now.
			*/
			r = OS_ERROR(OS_ERROR_WrongContext, OS_GET_PARAMETER_VAR());
		}

		if ( r == OS_E_OK )
		{
			if ( !OS_InterruptEnableCheck(OS_IEC_AUTOSAR) )
			{
				r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_GET_PARAMETER_VAR());
			}
			else
			if ( OS_errorStatus.action != OS_ACTION_SHUTDOWN )
			{
				OS_errorStatus.action = (restart == OS_APPL_RESTART) ? OS_ACTION_RESTART : OS_ACTION_QUARANTINEAPP;
			}
			else
			{
				/* MISRA */
			}
		}

		if ( (r == OS_E_OK) && (!OS_AppIsNull(OS_hookApp)) )
		{
			r = OS_E_INTERNAL;
			if ( OS_killHookFunc != OS_NULL )
			{
				r = (*OS_killHookFunc)(&OS_eHookContext);
			}

			if (r != OS_E_OK)
			{
				/* If the hook cannot be killed for some reason we
				 * must shut down.
				*/
				OS_SHUTDOWN(r);
			}
		}
	}
	else
	{
		if ( OS_inFunction == OS_INTASK )
		{
			app = OS_GET_APP(OS_taskCurrent->app);
		}
		else
		if ( OS_inFunction == OS_INCAT2 )
		{
			app = OS_GET_APP(OS_isrTableBase[OS_isrCurrent].app);
		}
		else
		{
			/* !LINKSTO Kernel.Autosar.API.SystemServices.TerminateApplication.InvalidContext, 1
			*/
			r = OS_ERROR(OS_ERROR_WrongContext, OS_NULL);
		}

		if ( r == OS_E_OK )
		{
			if ( !OS_InterruptEnableCheck(OS_IEC_AUTOSAR) )
			{
				r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_GET_PARAMETER_VAR());
			}
			else
			if ( OS_AppIsNull(app) )
			{
				r = OS_ERROR(OS_ERROR_InvalidApplicationId, OS_GET_PARAMETER_VAR());
			}
			else
			{
				/* !LINKSTO Kernel.Autosar.OsApplication.Termination, 1
				*/
				OS_QUARANTINEAPPLICATION(app);

				if ( restart == OS_APPL_RESTART )
				{
					/* !LINKSTO Kernel.Autosar.API.SystemServices.TerminateApplication.Restart, 1
					*/
					OS_RESTARTAPPLICATION(app);
				}

				if ( ( OS_inFunction == OS_INCAT2 ) && ( OS_killIsrFunc != OS_NULL ) )
				{
					(*OS_killIsrFunc)(OS_isrCurrent);
				}
			}
		}
	}

	OS_TRACE_TERMINATEAPPLICATION_EXIT_P(r,restart);

#endif /* (OS_KERNEL_TYPE!=OS_MICROKERNEL) */

	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserTerminateApplication</name>
  <synopsis>Terminates the current application</synopsis>
  <syntax>
    os_result_t OS_UserTerminateApplication(os_restart_t)
  </syntax>
  <description>
    <code>OS_UserTerminateApplication(os_restart_t)</code> disables all ISRs,
    alarms, scheduletables and tasks of the current application.
    Afterwards a possibly configured restart task will be activated if the
    parameter of TerminateApplication is set to RESTART.
  </description>
  <availability>
  </availability>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
