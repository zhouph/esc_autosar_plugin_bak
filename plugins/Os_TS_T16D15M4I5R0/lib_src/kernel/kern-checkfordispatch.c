/* kern_checkfordispatch.c
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-checkfordispatch.c 17718 2014-02-17 15:31:32Z tojo2507 $
*/

#define OS_SID	OS_SID_Dispatch
#define OS_SIF	OS_svc_Dispatch

#include <Os_kernel.h>
#include <Os_panic.h>
#include <Os_taskqueue.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * CheckForDispatch
 *
 * called from asm handler
 * Isr's must be locked when calling this function
 * OS_inKernel is set back with old_inKernel, if no dispatching is done.
 * In case of dispatching OS_Dispatch sets Os_inKernel back.
 *
 * returns
 *  OS_NO_DISP           0: no dispatching
 *  OS_DISP_WITHOUT_SAVE 1: dispatch without save old context
 *  OS_DISP_WITH_SAVE    2: save old and dispatch
*/

os_unsigned_t OS_CheckForDispatch( os_uint8_t old_inKernel )
{

	os_unsigned_t disp = OS_NO_DISP;
	if ( OS_ExtraCheck(OS_inKernel == 0) )
	{
		/* Serious kernel error
		*/
		/* can't propagate the return value of OS_PANIC -> ignore it */
		(void) OS_PANIC(OS_PANIC_IncorrectKernelNesting);
	}
	else
	if ( old_inKernel == 0 )
	{
		if ( OS_taskQueueHead == OS_taskCurrent )
		{
			if ( OS_taskCurrent == OS_NULL )
			{
				/* Nothing to do - will end in idle task
				*/
				disp = OS_DISP_WITHOUT_SAVE;
			}
			else
			{
				/* Three possibilities for current task's state here:
				 *  RUNNING - no task switch, just return to caller
				 *  READY_SYNC - task waited for an event which was set in an ISR
				 *  NEW - task has chained self, or terminated with 2nd activation queued, or been killed & reactivated
				*/
				if ( OS_taskCurrent->dynamic->state == OS_TS_RUNNING )
				{
					/* No task switch - return to caller.
					 * But first set the task's running priority if necessary.
					*/
					if ( OS_taskCurrent->dynamic->prio < OS_taskCurrent->runPrio )
					{
						OS_taskCurrent->dynamic->prio = OS_taskCurrent->runPrio;
#if OS_USE_CLZ_QUEUE_ALGORITHM
						OS_InsertTask(OS_taskCurrent, OS_taskCurrent->runPrio);
#endif
					}
					/* need to continue timing supervision */
					OS_ARCH_STARTTASKEXECTIMING(OS_taskCurrent, OS_TS_RUNNING);
				}
				else if ( OS_taskCurrent->dynamic->state > OS_TS_MAX_TERMINATING )
				{
					disp = OS_DISP_WITH_SAVE;
				}
				else
				{
					/* Current task is terminating and restarting
					*/
					disp = OS_DISP_WITHOUT_SAVE;
				}
			}
		}
		else
		{
			if ( ( OS_taskCurrent == OS_NULL ) || ( OS_taskCurrent->dynamic->state <= OS_TS_MAX_TERMINATING ) )
			{
				disp = OS_DISP_WITHOUT_SAVE;
			}
			else
			{
				/* ASCOS-2610 */
				disp = OS_DISP_WITH_SAVE;
			}
		}
	}
	else
	{
		/* MISRA-C */
	}

	/* Trapping kernel use OS_inKernel as flag -> don't need to restore !
	 * Non-systemcall kernel use OS_inKernel as counter and need restore
	*/
#if (OS_KERNEL_TYPE==OS_FUNCTION_CALL)
	if( disp == OS_NO_DISP )
	{
		OS_inKernel = old_inKernel;
	}
#endif

	return disp;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
