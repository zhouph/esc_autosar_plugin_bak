/* kern-resumeisrexectiming.c
 *
 * This file contains the OS_ResumeIsrExecTiming function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-resumeisrexectiming.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_ResumeIsrExecTiming
 *
 * This function is called from the category 2 interrupt entry routine.
 * It restarts any saved ISR execution timing, unless the limit has been reached.
 *
 * !LINKSTO Kernel.Autosar.Protection.TimingProtection.ExecutionTime.Measurement.ISR, 1
*/
void OS_ResumeIsrExecTiming(os_accountingsave_t *save)
{
	OS_accounting.inFunction = save->inFunction;
	OS_accounting.etType = save->etType;
	OS_accounting.etUsed = save->etUsed;
	OS_accounting.etLimit = save->etLimit;

	if ( (OS_accounting.etLimit != 0) && (OS_accounting.etUsed >= OS_accounting.etLimit) )
	{
		OS_ExceedExecTime();
	}
	else
	{
		OS_accounting.frc = OS_ReadExecTimer();
		if ( OS_accounting.etLimit != 0 )
		{
			OS_SetExecTimingInterrupt(OS_accounting.frc, (OS_accounting.etLimit - OS_accounting.etUsed));
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
