/* kern-startupcheckstimer.c
 *
 * This file contains the OS_StartupChecksTimer function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-startupcheckstimer.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_panic.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_StartupChecksTimer
 *
 * This function performs a variety of checks of the hardware timer systems
*/
os_result_t OS_StartupChecksTimer(void)
{
	os_result_t result = OS_E_OK;
	os_unsigned_t i;
	const os_hwt_t *hwt;

	for ( i = 0; i < OS_nHwTimers; i++ )
	{
		hwt = &OS_hwTimer[i];

#if OS_HWT_POWEROF2
		/* If the architecture only supports power-of-2 timer wrap values we must check that
		 * the wrap masks are correct.
		*/
		if ( !OS_IsPowerOf2(hwt->wrapMask+1) )
		{
			result = OS_PANIC(OS_PANIC_SCHK_NonPowerOfTwoTimerWraparoundNotSupported);
		}
#endif

		if ( (hwt->wrapMask <= hwt->maxDelta) || (hwt->maxDelta <= hwt->defDelta) )
		{
			result = OS_PANIC(OS_PANIC_SCHK_TimerMaxDeltaAndDefDeltaInconsistent);
		}
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
