/* kern-setevent.c
 *
 * This file contains the OS_KernSetEvent function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-setevent.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if at least one application exists.
 */

#define OS_SID	OS_SID_SetEvent
#define OS_SIF	OS_svc_SetEvent

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernSetEvent
 *
 * If any of the specified events is already set for the current task, this
 * function simply returns. Otherwise the task is dequeued and goes
 * to the WAITING state.
 *
 * !LINKSTO Kernel.API.EventControl.SetEvent.API, 1
*/
os_result_t OS_KernSetEvent(os_taskid_t t, os_eventmask_t evt)
{
	const os_task_t *ts;
	os_result_t r = OS_E_OK;
	os_errorresult_t e;
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_appcontext_t *app;
	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0,(os_paramtype_t)t);
	OS_SAVE_PARAMETER_N(1,(os_paramtype_t)evt);

	OS_TRACE_SETEVENT_ENTRY(t,evt);

	if ( !OS_CallingContextCheck() )
	{
		r = OS_ERROR(OS_ERROR_WrongContext, OS_GET_PARAMETER_VAR());
	}
	else
	if ( !OS_InterruptEnableCheck(OS_IEC_OSEKEXTRA) )
	{
		r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_GET_PARAMETER_VAR());
	}
	else
	if ( !OS_IsValidTaskId(t) )
	{
		/* !LINKSTO Kernel.API.EventControl.SetEvent.InvalidTask, 1
		*/
		r = OS_ERROR(OS_ERROR_InvalidTaskId, OS_GET_PARAMETER_VAR());
	}
	else
	{
		ts = OS_TidToTp(t);

		app = OS_CurrentApp();

		if ( !OS_HasPermission(app, ts->permissions) )
		{
			r = OS_ERROR(OS_ERROR_Permission, OS_GET_PARAMETER_VAR());
		}
		else
		if ( (ts->flags & OS_TF_EXTENDED) != 0 )
		{
			e = OS_DoSetEvent(ts, evt);

			if ( e != OS_ERRORCODE_NOCHECK(OS_ERROR_NoError) )
			{
				r = OS_ERROR_NOCHECK(e, OS_GET_PARAMETER_VAR());
			}
		}
		else
		{
			/* !LINKSTO Kernel.API.EventControl.SetEvent.BasicTask, 1
			*/
			r = OS_ERROR(OS_ERROR_TaskNotExtended, OS_GET_PARAMETER_VAR());
		}
	}

	OS_TRACE_SETEVENT_EXIT_P(r,t,evt);
	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserSetEvent</name>
  <synopsis>Set one or more events for a task</synopsis>
  <syntax>
    os_result_t OS_UserSetEvent
    (   os_taskid_t t,     /@ ID of the task @/
        os_eventmask_t evt /@ Events to set @/
    )
  </syntax>
  <description>
    <code>OS_UserSetEvent()</code> sets the events given in <code>evt</code>
    for the specified task. If the task is waiting for one or more of the
    events, it will be reawakened and queued for execution.
    <para>The task must be an extended task.</para>
  </description>
  <availability>
  </availability>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
