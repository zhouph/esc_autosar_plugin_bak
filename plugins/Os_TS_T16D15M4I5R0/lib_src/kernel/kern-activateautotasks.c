/* kern-activateautotasks.c
 *
 * This file contains the OS_ActivateAutoTasks function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-activateautotasks.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_ActivateAutoTasks()
 *
 * Activate all tasks that are specified for the requested mode
*/
void OS_ActivateAutoTasks(void)
{
	os_uint16_t idx;

	idx = OS_startModeTasks[OS_mode];

	while ( OS_autoStartTasks[idx] < OS_GetNTasks() )
	{
		/* no way to propagate errors -> ignore return value */
		(void) OS_InternalActivateTask(OS_autoStartTasks[idx]);
		idx++;
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
