/* kern-getalarmdelta.c
 *
 * This file contains the OS_GetAlarmDelta function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-getalarmdelta.c 17713 2014-02-17 14:45:24Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_GetAlarmDelta
 *
 * The total number of ticks remaining before the alarm expires
 * is written to the indicated location.
 *
 * Must be called with interrupts disabled.
 *
 * Returns 0  normally.
 *         1  if alarm is not in use.
 *         -1 if alarm could not be found (corrupt alarm list).
*/
os_int_t OS_GetAlarmDelta
(	os_alarmid_t a,			/* Alarm id */
	const os_alarm_t *as,	/* Corresponding static structure */
	os_alarmdynamic_t *ad,	/* Corresponding dynamic structure */
	os_tick_t *out			/* Where to put the result */
)
{
	const os_counter_t *cs;
	os_counterdynamic_t *cd;
	os_int_t result = 0;
	os_alarmid_t ap;
	os_tick_t total;

	/* If the alarm is attached to a hardware counter we need
	 * to update the current value and the head delta. This might
     * result in some alarms expiring from the queue.
	*/
	cs = &OS_counterTableBase[as->counter];
	cd = &OS_counterDynamicBase[as->counter];

	if ( OS_CounterIsHw(cs) && (cd->lock == 0) )
	{
		OS_CtrUpdate(cs, cd);
	}

	if ( ad->inUse == OS_ALARM_INUSE )
	{
		/* Start with total = my delta, and find head of counter
		 * queue
		*/
		total = ad->delta;
		ap = OS_counterDynamicBase[as->counter].head;

		/* Add delta of all prior alarms in the counter queue
		*/
		while ( (ap < OS_totalAlarms) && (ap != a) )
		{
			ad = &OS_alarmDynamicBase[ap];
			total += ad->delta;
			ap = ad->next;
		}

		if ( ap == a )
		{
			/* !LINKSTO Kernel.API.Alarms.GetAlarm.Return, 1
			*/
			*out = total;
		}
		else
		{
			/* The alarm was not found in the delta list. This means
			 * a seriously corrupted list.
			*/
			result = -1;
		}
	}
	else
	{
		/* The alarm is not in use
		*/
		result = 1;
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
