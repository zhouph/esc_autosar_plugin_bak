/* kern-setscheduletableasync.c
 *
 * This file contains the OS_SetScheduleTableAsync function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-setscheduletableasync.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if at least one application exists.
 */

#define OS_SID	OS_SID_SetScheduleTableAsync
#define OS_SIF	OS_svc_SetScheduleTableAsync

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernSetScheduleTableAsync implements the API SetScheduleTableAsync
 *
 * !LINKSTO Kernel.Autosar.ScheduleTable.Synchronisation.Strategy.EXPLICIT.GlobalTime, 1
 * !LINKSTO Kernel.Autosar.API.SystemServices.SetScheduleTableAsync, 1
*/
os_result_t OS_KernSetScheduleTableAsync(os_scheduleid_t s)
{
	const os_schedule_t *ss;
	os_scheduledynamic_t *sd;
	os_result_t r = OS_E_OK;
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_appcontext_t *app;
	os_intstatus_t is;
	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0,(os_paramtype_t)s);

	OS_TRACE_SETSCHEDULETABLEASYNC_ENTRY(s);

	if ( !OS_CallingContextCheck() )
	{
		r = OS_ERROR(OS_ERROR_WrongContext, OS_GET_PARAMETER_VAR());
	}
	else
	if ( !OS_InterruptEnableCheck(OS_IEC_AUTOSAR) )
	{
		r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_GET_PARAMETER_VAR());
	}
	else
	if ( s >= OS_nSchedules )
	{
		/* !LINKSTO Kernel.Autosar.API.SystemServices.SetScheduleTableAsync.Invalid, 1
		*/
		r = OS_ERROR(OS_ERROR_InvalidScheduleId, OS_GET_PARAMETER_VAR());
	}
	else
	{
		ss = &OS_scheduleTableBase[s];
		app = OS_CurrentApp();

		if ( !OS_HasPermission(app, ss->permissions) )
		{
			r = OS_ERROR(OS_ERROR_Permission, OS_GET_PARAMETER_VAR());
		}
		else
		if ( (ss->flags & OS_ST_SYNCABLE) == 0 )
		{
			/* !LINKSTO Kernel.Autosar.API.SystemServices.SetScheduleTableAsync.NonSynch, 1
			*/
			r = OS_ERROR(OS_ERROR_NotSyncable, OS_GET_PARAMETER_VAR());
		}
		else
		{
			/* !LINKSTO Kernel.Autosar.ScheduleTable.Synchronisation.Strategy.EXPLICIT, 1
			*/
			sd = &OS_scheduleDynamicBase[s];

			is = OS_IntDisable();

			if ( (sd->status & OS_ST_STATE) == OS_ST_RUNNING )
			{
				/* Clear the synchronous flag and the sync direction bits.
				 * No sync adjustment will be done as long as SYNCDIR
				 * remains 0.
				 *
				 * !LINKSTO Kernel.Autosar.API.SystemServices.SetScheduleTableAsync.Running, 1
				 * !LINKSTO Kernel.Autosar.API.SystemServices.SetScheduleTableAsync.NoAdjust, 1
				*/
				sd->status &= (os_schedulestatus_t) ~(OS_ST_SYNCHRONOUS | OS_ST_SYNCDIR);
			}
			else
			{
				/* !LINKSTO Kernel.Autosar.API.SystemServices.SetScheduleTableAsync.State, 1
				*/
				r = OS_ERROR(OS_ERROR_NotRunning, OS_GET_PARAMETER_VAR());
			}

			OS_IntRestore(is);
		}
	}

	OS_TRACE_SETSCHEDULETABLEASYNC_EXIT_P(r,s);
	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserSetScheduleTableAsync</name>
  <synopsis>Synchronise a schedule table to global time</synopsis>
  <syntax>
    os_result_t OS_UserSetScheduleTableAsync(void)
  </syntax>
  <description>
    <code>OS_UserSetScheduleTableAsync()</code>
  </description>
  <availability>
    No restrictions.
  </availability>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
