/* kern-requeuedown.c
 *
 * This file contains the OS_RequeueDown function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-requeuedown.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_RequeueDown
 *
 * The specified task activation is moved from its position in the task queue to somewhere
 * further down the queue because it has decreased its priority.
 *
 * OS_RequeueDown() must be called with interrupts locked
*/
void OS_RequeueDown(os_tasklink_t tAct, os_prio_t prio)
{
	os_tasklink_t qAct = OS_taskActivations[0];
	os_tasklink_t qOld = 0;		/* Old predecessor in queue */
	os_tasklink_t qNew;			/* New predecessor in queue */
	os_tasklink_t qSkip = 0;	/* Old successor in queue */

	while ( (qAct != tAct) && (qAct != 0) )
	{
		qOld = qAct;
		qAct = OS_taskActivations[qAct];
	}

	if ( qAct == tAct )
	{
		qSkip = OS_taskActivations[tAct];
		qAct = qSkip;
		qNew = qOld;

		while ( (qAct != 0) && (OS_FindPrio(qAct) > prio) )
		{
			qNew = qAct;
			qAct = OS_taskActivations[qAct];
		}

		/* Now we remove the activation from its former position
		 * (after qOld) and insert it after qNew.
		*/
		if ( qOld == qNew )
		{
			/* No change */
		}
		else
		{
			/* Remove from old place in queue
			*/
			OS_taskActivations[qOld] = qSkip;
			OS_taskQueueHead = OS_taskPtrs[OS_taskActivations[0]];

			/* Insert at new place in queue
			*/
			OS_taskActivations[qNew] = tAct;
			OS_taskActivations[tAct] = qAct;
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
