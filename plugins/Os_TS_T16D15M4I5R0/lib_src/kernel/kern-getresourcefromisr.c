/* kern-getresource.c
 *
 * This file contains the OS_KernGetResource function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-getresourcefromisr.c 18659 2014-08-27 08:50:00Z mist8519 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if at least one application exists.
 *
 * TOOLDIAG-2) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if timing protection is enabled.
 */

#define OS_SID	OS_SID_GetResource
#define OS_SIF	OS_svc_GetResource

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_GetResourceFromIsr
 *
 * This function implements the GetResource system service in the kernel, when the caller is a Category 2 ISR
*/
os_result_t OS_GetResourceFromIsr(os_resourceid_t r, os_resourcedynamic_t *rd, os_paramtype_t *p)
{
	os_result_t result = OS_E_OK;
	const os_resource_t *rs = &OS_resourceTableBase[r];
	os_intstatus_t is;
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_appcontext_t *app;
	/* Possible diagnostic TOOLDIAG-2 <1> */
	const os_isr_t *isrp;

	OS_PARAM_UNUSED(p);

	/* !LINKSTO Kernel.API.ResourceManagement.GetResource.ISRC2, 1
	*/
	isrp = &OS_isrTableBase[OS_isrCurrent];
	app = OS_GET_APP(isrp->app);

	if ( !OS_HasPermission(app, rs->permissions) )
	{
		result = OS_ERROR(OS_ERROR_Permission, p);
	}
	else
	if ( OS_ResourceInvalidForIsr(isrp, rs) )
	{
		/* This happens if resource priority is a task priority as
		 * well as if the resource priority is an interrupt priority
		 * lower than the run-level of the current ISR
		*/
		result = OS_ERROR(OS_ERROR_ResourcePriorityError, p);
	}
	else
	{
		/* Now we must disable interrupts to prevent an ISR that
		 * takes the resource from overwriting what we write into
		 * the dynamic structure. This also prevents the execution-
		 * budget monitor from killing the ISR and leaving the
		 * resource in an undefined state.
		*/
		is = OS_IntDisable();

		/* Chain the resource into the ISR resource queue.
		 * !LINKSTO Kernel.API.ResourceManagement.GetResource.NestedResources, 1
		*/
		rd->next = OS_isrLastRes;
		OS_isrLastRes = r;

		/* Store the old ISR priority into the resource's
		 * save location.
		*/
		rd->lastPrio = OS_GetInterruptPriority(is);

		/* Set NULLTASK as taker of the resource
		*/
		rd->takenBy = OS_NULLTASK;

		/* Start resource-lock timing if necessary
		*/
		if ( OS_GET_RLOCK((isrp->resourceLockTime != OS_NULL) &&
				(OS_GET_RLOCK(isrp->resourceLockTime[r]) != 0)) )
		{
			OS_STARTRESLOCKTIMING(rd, isrp->resourceLockTime[r]);
		}

		/* If resource's priority is higher, set the new task
		 * priority.
		 * !LINKSTO Kernel.ResourceManagement.PriorityCeiling, 1
		*/
		if ( rs->prio > rd->lastPrio )
		{
			/* This branch sets the current level to the
			 * resource's level, so we must not
			 * restore the saved interrupt state.
			*/
			OS_SetIsrResourceLevel(rs->prio);
			OS_IntRestoreHardLock(is);
		}
		else
		{
			/* The ISR priority was already greater than the
			 * resource's priority, so we simply restore the
			 * original interrupt level
			*/
			OS_IntRestore(is);
		}
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
