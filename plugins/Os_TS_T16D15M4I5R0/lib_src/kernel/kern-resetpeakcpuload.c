/* kern-resetpeakcpuload.c
 *
 * This file contains the OS_KernResetPeakCpuLoad() function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-resetpeakcpuload.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_cpuload.h>
#include <Os_cpuload_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_KernResetPeakCpuLoad
 *
 * This function resets the peak load to the current load value.
 *
 * Before resetting the value, we update to include the current busy interval. To perform
 * the computation we "enter" the idle state and then "leave" it again.
*/
void OS_KernResetPeakCpuLoad(void)
{
	os_intstatus_t is;

	if ( (OS_configMode & OS_CPULOAD) != 0 )
	{
		is = OS_IntDisableAll();
		OS_MeasureCpuLoad();
		OS_cpuLoad.peakLoad = OS_cpuLoad.currentLoad;
		OS_LeaveIdleState();
		OS_IntRestoreAll(is);
	}
}

#include <memmap/Os_mm_code_end.h>

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserResetPeakCpuLoad</name>
  <synopsis>Reset the CPU load monitor's peak load detector.</synopsis>
  <syntax>
    void OS_UserResetPeakCpuLoad(void)
  </syntax>
  <description>
    <para>
    <code>OS_UserResetPeakCpuLoad()</code> resets the peak CPU load detector of the load monitoring
    system. The peak load latch is set to the current load, after first ensuring that the current load
    is up-to-date.
    </para>
    <para>
    If CPU load measurement is disabled, no action takes place.
    </para>
  </description>
  <availability>
    Include the <code>Os_salsa.h</code> header file. No restrictions.
  </availability>
</api>

<api API="OS_USER">
  <name>InitCpuLoad</name>
  <synopsis>Reset the CPU load monitor's peak load detector.</synopsis>
  <syntax>
    void InitCpuLoad(void)
  </syntax>
  <description>
    <para>
    <code>InitCpuLoad()</code> resets the peak CPU load detector of the load monitoring
    system. The peak load latch is set to the current load, after first ensuring that the current load
    is up-to-date.
    </para>
    <para>
    If CPU load measurement is disabled, no action takes place.
    </para>
  </description>
  <availability>
    Include the <code>Os_salsa.h</code> header file. This function must not be called from non-trusted
    applications; instead, <code>OS_UserResetPeakCpuLoad()</code> can be used.
  </availability>
</api>

 * CHECK: PARSE
*/

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
