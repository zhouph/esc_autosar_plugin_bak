/* kern-mmupagify.c
 *
 * This file contains the OS_MmuPagify function, which rounds a memory mapping
 * to whole aligned minimum-size pages.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-mmupagify.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_mmu.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_Pagify()
 *
 * This function rounds a list of memory chunks to whole minimum-size pages.
 * It is assumed that the minimum page size is always a power of 2.
*/
void OS_MmuPagify(os_pagemap_t *map, os_int_t nmaps)
{
	os_int_t i;
	os_address_t newbase;
	os_size_t newsize;

	for ( i = 0; i < nmaps; i++ )
	{
		newbase = map[i].base & OS_PAGEMASK_MIN;
		newsize = map[i].size + (map[i].base - newbase);
		newsize = (newsize + (~OS_PAGEMASK_MIN)) & OS_PAGEMASK_MIN;
		map[i].base = newbase;
		map[i].size = newsize;
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
