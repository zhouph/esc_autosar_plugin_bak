/* kern-initkernstack.c
 *
 * This file contains the OS_InitKernStack function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-initkernstack.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 * Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 * Pointer arithmetic is used for classical tasks like initializing the
 * stack to make them more readable and maintainable.
 *
 *
 * MISRA-2) Deviated Rule: 10.1 (required)
 * Implicit conversion of the expression of underlying type 'signed int'
 * to the type 'unsigned int' that is not a wider integer type of the
 * same signedness.
 *
 * Reason:
 * The underlying type 'signed int' is the result of substracting two
 * pointers to determine the number of stack elements. The implementation
 * ensures that the result can't be negative, so it is safe to assign it
 * to a variable of unsigned type. The implicit conversion can't be made
 * explicit by using a cast as this would be a deviation of rule 10.3.
*/

#include <Os_kernel.h>

/* If necessary, this can be defined in the architecture header file
*/
#ifndef OS_InitKernStackLimit
#if OS_STACKGROWS==OS_STACKGROWSDOWN
#define OS_InitKernStackLimit(StackBase, StackLen, CurrentSp) \
	( ((CurrentSp) - (StackBase)) - 1 )
#else
#define OS_InitKernStackLimit(StackBase, StackLen, CurrentSp) \
	( ( ((StackBase) + (StackLen)) - (CurrentSp) ) + 1 )
#endif
#endif

#include <memmap/Os_mm_code_begin.h>

/* OS_InitKernStack()
 *
 * Set the kernel stack to 0xee (or whatever the fill pattern is).
*/
void OS_InitKernStack(void)
{
	os_unsigned_t i;
	os_unsigned_t slen;
	os_unsigned_t slen2;
	os_stackelement_t *p;
	os_stackelement_t *sp;

	/* Initialise the kernel/interrupt stack. We're running in that stack,
	 * so we have to be very careful not to go past where we currently are.
	 *   - slen is the number of stackelements in the stack
	 *   - slen2 is the number of unused stackelelements
	 *   - we use the smaller of the two numbers.
	*/
	p = OS_iStackBase;		/* lowest stack address */
	sp = (os_stackelement_t *)OS_GetCurrentSp();
	slen = OS_iStackLen/sizeof(os_stackelement_t);
	/* Deviation MISRA-2 */
	slen2 = OS_InitKernStackLimit(p, slen, sp);

#if OS_STACKGROWS==OS_STACKGROWSDOWN
	/* filling from lowest to highest address */
#else
	/* filling from highest to lowest address */
	/* Deviation MISRA-1 */
	p += slen - 1u;			/* calculate highest stack address */
#endif

	if ( slen > slen2 )
	{
		slen = slen2;
	}

	for ( i = 0; i < slen; i++ )
	{
		*p = OS_STACKFILL;
#if OS_STACKGROWS==OS_STACKGROWSDOWN
		/* Deviation MISRA-1 */
		p++;
#else
		/* Deviation MISRA-1 */
		p--;
#endif
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
