/* kern-stoptaskintlocktiming.c
 *
 * This file contains the OS_StopTaskIntLockTiming function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-stoptaskintlocktiming.c 18659 2014-08-27 08:50:00Z mist8519 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_StopTaskIntLockTiming
 *
 * This function stops interrupt-lock timing for the task. If execution
 * time monitoring was previously running the time remaining is computed
 * and monitoring is restarted.
*/
void OS_StopTaskIntLockTiming(const os_task_t *tp, os_intlocktype_t locktype)
{
	os_taskaccounting_t *acc = OS_GET_ACCT(tp->accounting);
	os_tick_t locktime;
	os_tick_t used;

	OS_PARAM_UNUSED(tp);

	locktime = OS_GET_ILOCK((locktype == OS_LOCKTYPE_OS) ? tp->osLockTime : tp->allLockTime);

	/* No need to test if acc is OS_NULL - the generator ensures that it isn't
	*/
	if ( locktime != 0 )
	{
		OS_ResetExecTimingInterrupt();
		used = OS_GetTimeUsed();

		if ( locktype == OS_LOCKTYPE_OS )
		{
			OS_accounting.etUsed += used + acc->osSaveUsed;
			OS_accounting.etLimit = acc->osSaveLimit;
			OS_accounting.etType = acc->osSaveType;
		}
		else
		{
			OS_accounting.etUsed += used + acc->allSaveUsed;
			OS_accounting.etLimit = acc->allSaveLimit;
			OS_accounting.etType = acc->allSaveType;
		}

		if ( OS_GET_ACCT(((tp->flags & OS_TF_MEASUREEXEC) != 0)
				&& (tp->accounting->etMax < OS_accounting.etUsed)) )
		{
			OS_SET_ACCT(tp->accounting->etMax = OS_accounting.etUsed);
		}

		if ( OS_accounting.etLimit != 0 )
		{
			if ( OS_accounting.etUsed >= OS_accounting.etLimit )
			{
				OS_ExceedExecTime();
			}
			else
			{
				OS_SetExecTimingInterrupt(OS_accounting.frc, (OS_accounting.etLimit - OS_accounting.etUsed));
			}
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
