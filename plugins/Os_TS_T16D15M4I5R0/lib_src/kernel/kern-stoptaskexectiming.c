/* kern-stoptaskexectiming.c
 *
 * This file contains the OS_StopTaskExecTiming function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-stoptaskexectiming.c 18659 2014-08-27 08:50:00Z mist8519 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_StopTaskExecTiming
 *
 * This function stops the hardware timer used for execution timing.
 *
 * !LINKSTO Kernel.Autosar.Protection.TimingProtection.ExecutionTime.Measurement.Task, 1
*/
void OS_StopTaskExecTiming(const os_task_t *tp)
{
	os_taskaccounting_t *acc = OS_GET_ACCT(tp->accounting);
	os_tick_t used;

	OS_PARAM_UNUSED(tp);

	if ( acc != OS_NULL )
	{
		OS_ResetExecTimingInterrupt();
		used = OS_GetTimeUsed();

		/* We don't check the limit here. The task managed to get as far as the disable interrupts
		 * section in terminate task wihtout being killed, so we give it the benefit of the doubt.
		*/
		OS_accounting.etUsed += used;

		if ( OS_accounting.etUsed > acc->etMax )
		{
			acc->etMax = OS_accounting.etUsed;
		}

		OS_accounting.inFunction = OS_INNOTHING;
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
