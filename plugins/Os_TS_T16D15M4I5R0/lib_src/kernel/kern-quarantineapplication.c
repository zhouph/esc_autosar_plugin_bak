/* kern-quarantineapplication.c
 *
 * This file contains the OS_QuarantineApplication function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-quarantineapplication.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 * Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 * Pointer arithmetic is used for classical tasks like iterating through
 * an array to make them more readable and maintainable.
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_QuarantineApplication terminates the specified application.
 *
 * Terminating an application means:
 *  - disabling all ISRs
 *  - cancelling all alarms
 *  - stopping all schedule tables
 *  - killing all tasks (including the caller, if it is a task)
 *
 * Objects that are terminated are set to QUARANTINED to prevent reactivation.
 *
 * NOTE: this function does nothing about physically killing any ISRs that may be active.
 * The ISRs are marked as killed, but the error handler and the interrupt entry functions
 * are responsible for the actual killing because there may be unaffected ISRs also on the
 * stack.
 *
 * !LINKSTO Kernel.Autosar.OsApplication.Termination, 1
 * !LINKSTO Kernel.Autosar.Protection.ProtectionHook.KillApplication, 2
*/
void OS_QuarantineApplication(const os_appcontext_t *app)
{
	os_taskid_t taskid;
	os_isrid_t isrid;
	os_alarmid_t alarmid;
	os_scheduleid_t scheduleid;
	const os_task_t *task;
	const os_isr_t *isr;
	os_isrdynamic_t *isrd;
	const os_alarm_t *alarm;
	const os_schedule_t *schedule;

	/* Disable all interrupts belonging to this application
	*/
	isr = OS_isrTableBase;
	isrd = OS_isrDynamicBase;
	for ( isrid = 0; isrid < OS_nInterrupts; isrid++ )
	{
		if ( OS_GET_APP(isr->app) == app )
		{
			OS_DisableIsr(isr);
			OS_MarkIsrKilled(isrd);
			isrd->statusflags |= OS_ISRF_BLOCKED;
		}
		/* Deviation MISRA-1 <+2> */
		isr++;
		isrd++;
	}

	/* Cancel all alarms belonging to this application.
	 * Only true OSEK alarms are considered. Others (such as
	 * those belonging to schedule tables) will get cancelled
	 * when the object to which they belong gets cancelled.
	*/
	alarm = OS_alarmTableBase;
	for ( alarmid = 0; alarmid < OS_nAlarms; alarmid++ )
	{
		if ( OS_GET_APP(alarm->app) == app )
		{
			(*OS_killAlarmFunc)(alarmid, OS_ALARM_QUARANTINED);
		}
		/* Deviation MISRA-1 */
		alarm++;
	}

	/* Stop all schedule tables belonging to this application.
	*/
	schedule = OS_scheduleTableBase;
	for ( scheduleid = 0; scheduleid < OS_nSchedules; scheduleid++ )
	{
		if ( OS_GET_APP(schedule->app) == app )
		{
			(*OS_killScheduleFunc)(scheduleid, OS_ST_QUARANTINED);
		}
		/* Deviation MISRA-1 */
		schedule++;
	}

	/* Kill all tasks that belong to this application. We
	 * set the state to QUARANTINED to prevent other applications
	 * from activating the tasks later. OS_RestartApplication()
	 * will set the states back to SUSPENDED if it is called.
	*/
	task = OS_taskTableBase;
	for ( taskid = 0; taskid < OS_nTasks; taskid++ )
	{
		if ( OS_GET_APP(task->app) == app )
		{
			(*OS_killTaskFunc)(task, OS_TS_QUARANTINED);
		}
		/* Deviation MISRA-1 */
		task++;
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
