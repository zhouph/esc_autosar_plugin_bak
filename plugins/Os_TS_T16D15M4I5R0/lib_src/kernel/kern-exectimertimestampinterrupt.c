/* kern-exectimertimestampinterrupt.c
 *
 * This file contains the OS_ExecTimerTimestampInterrupt function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-exectimertimestampinterrupt.c 20211 2015-01-21 13:36:35Z masa8317 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: UnusedVariable
 *   Variable is never used.
 *
 * Reason: Not an issue, variable will be used if timing protection is enabled.
 */

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_ExecTimerTimestampInterrupt
 *
 * This function handles the interrupt that occurs when the execution-time
 * timeout interrupt occurs. It must calculate the time used and update
 * the appropriate etMax and perform execution-time aggregation.
 * Then it calls OS_ExceedExecTime() to kill the offending task or ISR.
 *
 * This function is used when the timestamp is attached to the execution timer.
 * See also OS_ExecTimerInterrupt() in kern-exectimerinterrupt.c
 *
 * !LINKSTO Kernel.Autosar.Protection.TimingProtection.ExecutionTime, 2
*/
void OS_ExecTimerTimestampInterrupt(void)
{
	os_tick_t used, remaining;

	/* Clear the timer interrupt
	*/
	OS_HwtStop(OS_executionTimer);

	/* Get time used since last call. This updates OS_accounting.frc, which we can then
	 * give to the timestamp.
	*/
	used = OS_GetTimeUsed();

	/* Advance the time stamp
	*/
	OS_AdvanceTimeStamp(OS_accounting.frc);

	/* Check if there's an execution timer running
	*/
	if ( OS_accounting.etType == OS_ACC_NONE )
	{
		/* No timing in progress - just set the timer interrupt for the
		 * slow tickover time.
		*/
		OS_HwtStart(OS_executionTimer, OS_accounting.frc, OS_executionTimer->maxDelta);
	}
	else
	{
		/* Add 'used' onto the current accounting sum, with saturation!
		*/
		if ( used > (OS_MAXTICK - OS_accounting.etUsed) )
		{
			OS_accounting.etUsed = OS_MAXTICK;
		}
		else
		{
			OS_accounting.etUsed += used;
		}

		if ( OS_accounting.etUsed >= OS_accounting.etLimit )
		{
			if ( OS_inFunction == OS_INTASK )
			{
				OS_accounting.taskTimingNesting = 1;

				if ( OS_GET_ACCT(((OS_taskCurrent->flags & OS_TF_MEASUREEXEC) != 0)
						&& (OS_taskCurrent->accounting->etMax < OS_accounting.etUsed)) )
				{
					OS_SET_ACCT(OS_taskCurrent->accounting->etMax = OS_accounting.etUsed);
				}
			}
			else
			if ( OS_inFunction == OS_INCAT2 )
			{	/* Possible diagnostic TOOLDIAG-1 <1> */
				const os_isr_t *isr = &OS_isrTableBase[OS_isrCurrent];

				if ( OS_GET_ACCT(((isr->flags & OS_ISRF_MEASUREEXEC) != 0)
						&& (isr->accounting->etMax < OS_accounting.etUsed)) )
				{
					OS_SET_ACCT(isr->accounting->etMax = OS_accounting.etUsed);
				}
			}
			else
			{
				/* MISRA */
			}

			OS_ExceedExecTime();

			OS_HwtStart(OS_executionTimer, OS_accounting.frc, OS_executionTimer->maxDelta);
		}
		else
		{
			remaining = OS_accounting.etLimit - OS_accounting.etUsed;

			OS_CtrStart(OS_executionTimer, OS_accounting.frc, remaining);
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
