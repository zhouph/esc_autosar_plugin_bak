/* kern-startupchecksisr.c
 *
 * This file contains the OS_StartupChecksIsr function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-startupchecksisr.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_panic.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_StartupChecksIsr
*/
os_result_t OS_StartupChecksIsr(void)
{
	os_result_t result = OS_E_OK;
	os_unsigned_t i;
	const os_isr_t *isr;
	os_int_t nIsrExecBudget = 0;
	os_int_t nIsrResLockBudget = 0;
	os_int_t nIsrIntLockBudget = 0;

	for ( i = 0; i < OS_nInterrupts; i++ )
	{
		isr = &OS_isrTable[i];

		if ( ((isr->flags & OS_ISRF_MEASUREEXEC) != 0) && (OS_GET_ACCT(isr->accounting) == OS_NULL) )
		{
			result = OS_PANIC(OS_PANIC_SCHK_IsrWithMeasureExecButNoAccountingStructure);
		}

		if ( (OS_GET_TP(isr->execBudget) != 0) ||
			 (OS_GET_ILOCK(isr->osLockTime) != 0) ||
			 (OS_GET_ILOCK(isr->allLockTime) != 0) ||
			 (OS_GET_RLOCK(isr->resourceLockTime) != OS_NULL) )
		{
			if ( OS_GET_ACCT(isr->accounting) == OS_NULL )
			{
				result = OS_PANIC(OS_PANIC_SCHK_IsrWithExecTimeLimitButNoAccountingStructure);
			}

			if ( OS_GET_TP(isr->execBudget) != 0 )
			{
				nIsrExecBudget++;
			}

			if ( OS_GET_RLOCK(isr->resourceLockTime) != OS_NULL )
			{
				nIsrResLockBudget++;
			}

			if ( (OS_GET_ILOCK(isr->osLockTime) != 0) || (OS_GET_ILOCK(isr->allLockTime) != 0) )
			{
				nIsrIntLockBudget++;
			}

			/* This check is here because there's an implicit assumption in SuspendOSInterrupts that
			 * if any kind of interrupts have been locked there's a time limit already running that is less
			 * than or equal to the OS lock limit, so no new timing is started.
			 *
			 * The check won't fail if both are zero - that's OK.
			 * It *will* fail if osLockTime != 0 and allLockTime == 0 - that's OK.
			*/
			if ( OS_GET_ILOCK(isr->osLockTime) < OS_GET_ILOCK(isr->allLockTime) )
			{
				result = OS_PANIC(OS_PANIC_SCHK_IsrWithOsIntLockTimeLessThanAllIntLockTime);
			}
		}
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
