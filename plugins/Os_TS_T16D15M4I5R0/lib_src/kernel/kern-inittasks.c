/* kern-inittasks.c
 *
 * This file contains the OS_InitTasks function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-inittasks.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 * Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 * Pointer arithmetic is used for classical tasks like iterating through
 * an initialization array to make them more readable and maintainable.
*/

#include <Os_kernel.h>
#include <Os_taskqueue.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_InitTasks
 *
 * The head of each task's last resource queue needs to be set to NULLRESOURCE.
 * This is because the default bss value (0) is a valid resource id.
*/
void OS_InitTasks(void)
{
	os_int_t i = (os_int_t) OS_nTasks;
	os_taskdynamic_t *t = OS_taskDynamic;

	while ( i > 0 )
	{
		t->lastRes = OS_NULLRESOURCE;
		/* Deviation MISRA-1 */
		t++;
		i--;
	}

#if OS_USE_CLZ_QUEUE_ALGORITHM
	for ( i = 0; i < OS_nPrioritySlots; i++ )
	{
		OS_prioritySlot[i] = OS_NULLTASK;
	}
#endif
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
