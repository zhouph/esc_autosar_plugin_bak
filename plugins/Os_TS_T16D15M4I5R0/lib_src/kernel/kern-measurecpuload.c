/* kern-measurecpuload.c
 *
 * This file contains the OS_MeasureCpuLoad() function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-measurecpuload.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/


#include <Os_kernel.h>
#include <Os_timestamp.h>
#include <Os_cpuload_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_MeasureCpuLoad
 *
 * This function measures the CPU load. It is called on entry to the idle loop. Interrupts
 * must be fully disabled so that no interrupts of any category can occur during its execution.
 *
 * CPU load is measured by noting the times of entry and exit to the idle loop.
 *
 * !LINKSTO Kernel.Feature.CpuLoadMeasurement.Execution, 1
 * !LINKSTO Kernel.Feature.CpuLoadMeasurement.Execution.Load, 1
*/
void OS_MeasureCpuLoad(void)
{
	os_timestamp_t idleEntryTime;
	os_timestamp_t intervalEndTime;
	os_intstatus_t is = OS_IntDisableAll();

	/* Get the time of entry to the idle loop. All computation done after this is therefore in the
	 * "idle time" and doesn't count as part of the CPU load.
	*/
	OS_GetTimeStamp(&idleEntryTime);

	/* If the idle exit time is zero, something went wrong.
	 * Should we report the error?
	 * In any case, assume a zero-length busy time to prevent it skewing the calculations
	*/
	if ( OS_TimeIsZero(OS_cpuLoad.idleExitTime) )
	{
		OS_TimeCopy(&OS_cpuLoad.idleExitTime, idleEntryTime);
	}

	/* First, we compute the time measurement for all intervals that have completed since the last time we were here.
	*/
	OS_TimeAdd32(&intervalEndTime, OS_cpuLoad.intervalStartTime, OS_cpuLoadCfg.intervalDuration);

	while ( OS_TimeGe(idleEntryTime, intervalEndTime) )
	{
		if ( OS_TimeLt(OS_cpuLoad.idleExitTime, intervalEndTime) )
		{
			/* Part of the last busy interval was in the old measurement interval.
			 * That portion is added to the busy time before the load calculation, and busy interval for the
             * new measurement interval is* deemed to start at the start of the interval.
			*/
			OS_cpuLoad.busyTime += OS_TimeSub32(intervalEndTime, OS_cpuLoad.idleExitTime);
			OS_TimeCopy(&OS_cpuLoad.idleExitTime, intervalEndTime);
		}

		/* Slide the window along one place...
		*/
		OS_cpuLoad.busyIndex++;
		if ( OS_cpuLoad.busyIndex >= OS_cpuLoadCfg.nIntervals )
		{
			OS_cpuLoad.busyIndex = 0;
		}

		/* Remove the old busyTime from the new slot out of the calculation of the average.
		*/
		OS_cpuLoad.busyTimeSum -= OS_cpuLoadCfg.busyBuffer[OS_cpuLoad.busyIndex];

		/* Put the new busy time into the slot and bring it into the calculation of the average.
		*/
		OS_cpuLoadCfg.busyBuffer[OS_cpuLoad.busyIndex] = OS_cpuLoad.busyTime;
		OS_cpuLoad.busyTimeSum += OS_cpuLoad.busyTime;

		/* Calculate the new average load over the last nIntervals slots. If that's a peak, remember it.
		*/
		if ( OS_cpuLoad.busyTimeSum < OS_cpuLoadCfg.busyOverflowLimit )
		{
			OS_cpuLoad.currentLoad = (os_uint8_t) (( (OS_cpuLoad.busyTimeSum * 100) + OS_cpuLoadCfg.rounding )
																				/ OS_cpuLoadCfg.windowDuration );
		}
		else
		{
			OS_cpuLoad.currentLoad = (os_uint8_t) (( OS_cpuLoad.busyTimeSum + OS_cpuLoadCfg.rounding100 )
																				/ OS_cpuLoadCfg.windowDuration100 );
		}

		if ( OS_cpuLoad.currentLoad > OS_cpuLoad.peakLoad )
		{
			OS_cpuLoad.peakLoad = OS_cpuLoad.currentLoad;
		}

		/* Restart busy time from zero for the next loop or idle-entry
		*/
		OS_cpuLoad.busyTime = 0;

		/* Calculate the start and end of the next interval.
		 * If the end time is in the future the loop terminates.
		*/
		OS_TimeCopy(&OS_cpuLoad.intervalStartTime, intervalEndTime);
		OS_TimeAdd32(&intervalEndTime, OS_cpuLoad.intervalStartTime, OS_cpuLoadCfg.intervalDuration);
	}

	/* idleExitTime is now in the current interval
	*/
	OS_cpuLoad.busyTime += OS_TimeSub32(idleEntryTime, OS_cpuLoad.idleExitTime);

	OS_TimeZero(&OS_cpuLoad.idleExitTime);

	OS_IntRestoreAll(is);
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
