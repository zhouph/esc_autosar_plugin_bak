/* kern-initapplicationdata.c
 *
 * This file contains the OS_InitApplicationData and
 * OS_InitAppData functions. The two are together in one file
 * because they will never be used separately.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-initappdata.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 * Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 * Pointer arithmetic is used for classical tasks like iterating through
 * an initialization array or initializing memory to make them more
 * readable and maintainable.
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_InitApplicationData()
 *
 * This function initialises the private data/bss areas for every
 * application, task and ISR.
*/
void OS_InitApplicationData(void)
{
	os_applicationid_t a;
	os_taskid_t t;
	os_isrid_t i;
	const os_appcontext_t *ap;
	const os_task_t *tp;
	const os_isr_t *ip;

	ap = OS_appTableBase;
	for ( a = 0; a < OS_nApps; a++ )
	{
		OS_SET_MP(OS_InitDataArea(ap->dataStart, ap->dataEnd,
							ap->idataStart, ap->idataEnd));
		/* Deviation MISRA-1 */
		ap++;
	}

	tp = OS_taskTableBase;
	for ( t = 0; t < OS_nTasks; t++ )
	{
		OS_SET_MP(OS_InitDataArea(tp->dataStart, tp->dataEnd,
							tp->idataStart, tp->idataEnd));
		/* Deviation MISRA-1 */
		tp++;
	}

	ip = OS_isrTableBase;
	for ( i = 0; i < OS_nInterrupts; i++ )
	{
		OS_SET_MP(OS_InitDataArea(ip->dataStart, ip->dataEnd,
							ip->idataStart, ip->idataEnd));
		/* Deviation MISRA-1 */
		ip++;
	}
}

/*!
 * OS_InitDataArea()
 *
 * This function initialises a single object's data/bss area.
*/
void OS_InitDataArea
(	os_uint8_t *dest,
	os_uint8_t *dlimit,
	os_uint8_t *src,
	os_uint8_t *slimit
)
{
	/* Only initialise the data area if there is one!
	*/
	if ( dest != OS_NULL )
	{
		/* Initialisation is only done if the initialisation data exists (address is not OS_NULL),
		 * thus permitting use of uninitialised data, memory-mapped peripherals etc.
		*/
		if ( src != OS_NULL )
		{
			/* Copy all of the initialisation data to the start of the data area.
			 * This initialises the aggregated ".data" section.
			 * If the region is entirely bss, the src and slimit (__IDAT_* and __IEND_*) should be equal.
			*/
			while ( src < slimit )
			{
				*dest = *src;
				/* Deviation MISRA-1 <+2> */
				dest++;
				src++;
			}

			/* Zero the rest of the data area.
			 * This initialises the aggregated ".bss" section
			*/
			while ( dest < dlimit )
			{
				*dest = 0;
				/* Deviation MISRA-1 */
				dest++;
			}
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
