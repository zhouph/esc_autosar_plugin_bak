/* mka-terminateapplication.c
 *
 * This file contains the OS_MKTerminateApplication function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: mka-terminateapplication.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#define OS_SID	OS_SID_TerminateApplication
#define OS_SIF	OS_svc_TerminateApplication

#include <Os_kernel.h>
#include <Os_microkernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

#if (OS_KERNEL_TYPE!=OS_MICROKERNEL)
#error "This file is only needed for a microkernel build! Check your makefiles!"
#endif

/*!
 * OS_MKTerminateApplication implements the termination of alarms and schedule tables
 * of the API call TerminateApplication. ISRs and tasks are terminated by the microkernel.
 *
 * !LINKSTO Kernel.Autosar.API.SystemServices.TerminateApplication, 2
 * !LINKSTO Kernel.Autosar.OsApplication.Shutdown, 1
*/
os_result_t OS_MKTerminateApplication(os_applicationid_t aId, os_restart_t restart)
{
	os_result_t r = OS_E_OK;

	os_alarmid_t alarmid;
	const os_alarm_t *alarm;
	os_uint8_t newAlarmState;

	os_scheduleid_t scheduleid;
	const os_schedule_t *schedule;
	os_schedulestatus_t newScheduleState;

	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0,(os_applicationid_t)aId);
	OS_SAVE_PARAMETER_N(1,(os_paramtype_t)restart);

	OS_TRACE_TERMINATEAPPLICATION_ENTRY(restart);

	if ( aId >= OS_nApps )
	{
		r = OS_ERROR(OS_ERROR_InvalidApplicationId, OS_GET_PARAMETER_VAR());
	}
	else
	{
		if ( (restart == OS_APPL_RESTART) || (restart == OS_APPL_NO_RESTART) )
		{
			if ( restart == OS_APPL_RESTART )
			{
				newAlarmState = OS_ALARM_IDLE;
				newScheduleState = OS_ST_STOPPED;
			}
			else
			{
				newAlarmState = OS_ALARM_QUARANTINED;
				newScheduleState = OS_ST_QUARANTINED;
			}

			/* Terminate all alarms and set to the selected state.
			 * Only true OSEK alarms are considered. Those belonging to schedule tables are
			 * terminated when the schedule table gets stopped.
			*/
			for ( alarmid = 0, alarm = OS_alarmTableBase;
				  alarmid < OS_nAlarms;
				  alarmid++, alarm++ )
			{
				if ( OS_GET_APPID(alarm->appId) == aId )
				{
					(*OS_killAlarmFunc)(alarmid, newAlarmState);
				}
			}

			/* Stop all schedule tables and set to the selected state.
			*/
			for ( scheduleid = 0, schedule = OS_scheduleTableBase;
				  scheduleid < OS_nSchedules;
				  scheduleid++, schedule++ )
			{
				if ( OS_GET_APPID(schedule->appId) == aId )
				{
					(*OS_killScheduleFunc)(scheduleid, newScheduleState);
				}
			}
		}
		else
		{
			r = OS_ERROR(OS_ERROR_InvalidRestartOption, OS_GET_PARAMETER_VAR());
		}
	}

	return r;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
