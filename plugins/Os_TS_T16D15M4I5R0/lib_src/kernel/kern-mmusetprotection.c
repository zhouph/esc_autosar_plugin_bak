/* kern-mmusetprotection.c
 *
 * This file contains the OS_SetProtection function for architectures with an MMU.
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection, 1
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-mmusetprotection.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_mmu.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_SetProtection
 *
 * This function loads the page table for the given task into the MMU
 *
 * If the task already "owns" the MMU nothing is done. This avoids constantly reprogramming
 * the MMU when a non-trusted task is preempted by trusted ISRs and/or tasks.
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection, 1
*/
void OS_SetProtection(const os_task_t *t)
{
	if ( (OS_mmuOwnerType == OS_OBJ_TASK) && (OS_mmuOwnerId == t->taskId) )
	{
		/* Task already owns the MMU, so there's nothing to do.
		*/
	}
	else
	if ( OS_TaskUsesMmu(t) )
	{
		OS_mmuOwnerType = OS_OBJ_TASK;
		OS_mmuOwnerId = t->taskId;

		OS_FillMmuForTask(t);
	}
	else
	{
		/* MISRA :-( */
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
