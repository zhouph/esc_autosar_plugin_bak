/* kern-mmupagemap.c
 *
 * This file contains the OS_MmuPageMap function, which determines a page mapping
 * for a given memory region.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-mmupagemap.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_mmu.h>

#include <memmap/Os_mm_const_begin.h>

const os_size_t OS_pageSize[]	= OS_PAGESIZE_ARRAY;

#include <memmap/Os_mm_const_end.h>


#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_MmuPageMap()
 *
 * This function calculates a page map for the given memory region.
 * The pages are stored into the map array starting at the given pointer
 * and the number of pages required is returned.
 *
 * If the function is called with a OS_NULL map pointer the map is not written. This can be used
 * for determining how many pages are needed before allocating the map.
*/
os_int_t OS_MmuPageMap(os_address_t addr, os_size_t len, os_uint32_t flags, os_pagemap_t *map)
{
	/* workbase is the start of the first smallest-page of the remaining region
	*/
	os_address_t workbase = addr & OS_PAGEMASK_MIN;

	/* worklen is the number of bytes of whole smallest-pages that will cover the remaining region
	*/
	os_address_t worklen = (len + (addr - workbase) + ~OS_PAGEMASK_MIN) & OS_PAGEMASK_MIN;

	os_int_t npages = 0;
	os_pagesize_t size;

	while ( worklen > 0 )
	{
		/* Start with the page size that best fits the start address of the rest of the region
		*/
		size = OS_MmuBestPageSize(workbase);

		/* Reduce the page size until reducing it any more means it won't fit.
		*/
		while ( ( size > 0 ) && ( OS_pageSize[size-1] >= worklen ) )
		{
			size--;
		}

		/* If reducing the size by one page of the next smaller size would also cover
		 * the remainder, then reduce to the next smaller page size. This avoids a chunk
		 * of 1, 2 or 3 of the smaller size being incorrectly mapped.
		*/
		if ( ( size > 0 ) && ( (OS_pageSize[size] - OS_pageSize[size-1]) >= worklen ) )
		{
			size--;
		}

		/* If we're writing a map, write it to the output buffer.
		*/
		if ( map != OS_NULL )
		{
			map->base = workbase;
			map->size = OS_pageSize[size];
			map->flags = flags;
			map++;
		}

		workbase += OS_pageSize[size];
		worklen -= OS_pageSize[size];
		npages++;
	}

	return npages;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
