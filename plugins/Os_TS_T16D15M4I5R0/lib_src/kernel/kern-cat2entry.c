/* kern-cat2entry.c
 *
 * This file contains the Cat2 interrupt handler wrapper.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-cat2entry.c 20210 2015-01-21 12:51:48Z masa8317 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 12.4 (required)
 * The right-hand operand of a logical && or || operator shall not contain side effects.
 *
 * Reason:
 * The rate monitoring function has to make modifications to the global system state.
 *
 *
 * MISRA-2) Deviated Rule: 11.1 (required)
 * Conversions shall not be performed between a pointer to a function and
 * any type other than an integral type.
 *
 * Reason:
 * Depending on the architecture, the prototype of common assembly code for
 * setting up an ISR's environment might use another function pointer type
 * than the one given here.
 *
 *
 * MISRA-3) Deviated Rule: 10.1 (required)
 * Implicit conversion of the binary ? left hand operand of underlying type
 * 'signed int' to 'signed char' that is not a wider integer type of the
 * same signedness
 *
 * Reason:
 * Tooling problem: all operands of the "?" operator in question are boolean.
 *
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_C_COMPL_010:path]
 *  The code shall adhere to the [HISSRCMETRIC] Metrics.
 *
 * Reason:
 *  The function OS_Cat2Entry uses macros and if-elseif-else-cascades to select
 *  the proper actions. Breaking it up in multiple smaller functions is not a
 *  valid option, because this is the interrupt entry function which needs
 *  to be as quick as possible.
 *  This leads to a high PATH value.
 *
 * DCG-2) Deviated Rule: [OS_C_COMPL_010:stmt]
 *  The code shall adhere to the [HISSRCMETRIC] Metrics.
 *
 * Reason:
 *  The function OS_Cat2Entry uses macros and if-elseif-else-cascades to select
 *  the proper actions. Breaking it up in multiple smaller functions is not a
 *  valid option, because this is the interrupt entry function which needs
 *  to be as quick as possible.
 *  This leads to a high stmt value.
 *
 * DCG-3) Deviated Rule: [OS_C_COMPL_010:vg]
 *  The code shall adhere to the [HISSRCMETRIC] Metrics.
 *
 * Reason:
 *  The function OS_Cat2Entry uses macros and if-elseif-else-cascades to select
 *  the proper actions. Breaking it up in multiple smaller functions is not a
 *  valid option, because this is the interrupt entry function which needs
 *  to be as quick as possible.
 *  This leads to a high VG value.
*/

#define OS_SID OS_SID_IsrHandler
#define OS_SIF OS_svc_IsrHandler

#include <Os_kernel.h>
#include <Os_cpuload_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/* MISRA-C checkers want prototypes for static helpers */
static void OS_CleanUpISRResources(os_isrdynamic_t *);
static os_boolean_t OS_IsrErrorChecksOk(const os_isr_t *isr, os_isrdynamic_t *id);

/* Helper to clean up an ISR's resources if the ISR exited without releasing them.
 * Also reports the corresponding error in case the ISR wasn't killed.
*/
static void OS_CleanUpISRResources(os_isrdynamic_t *id)
{
	os_resourceid_t r;
	os_resourcedynamic_t *rd;

	OS_PARAM_UNUSED(id);

	if ( !OS_IsIsrKilled(id) )
	{
		/* ISR wasn't killed, so this is an error in the ISR.
		*/
		/* can't propagate the return value of OS_ERROR -> ignore it */
		(void) OS_ERROR(OS_ERROR_HoldsResource, OS_NULL);
	}

	/* Release the resources that were taken by the ISR
	 *
	 * !LINKSTO Kernel.Autosar.Protection.ProtectionHook.KillISR, 1
	*/
	r = OS_isrLastRes;
	while ( r != OS_NULLRESOURCE )
	{
		rd = &OS_resourceDynamicPtr[r];
		r = rd->next;
		rd->next = OS_NULLRESOURCE;
		rd->takenBy = OS_NULLTASK;
		rd->lastPrio = 0;
	}
}

/* Helper to do ISR related error checks and reporting
 * returns false if we have a stack overflow problem or if we've exceeded
 * our rate limit; returns true if everything is fine and the ISR can run.
*/
static os_boolean_t OS_IsrErrorChecksOk(const os_isr_t *isr, os_isrdynamic_t *id)
{
	os_boolean_t allok = OS_FALSE;

	/* Deviation MISRA-3 */
	if ( OS_InsufficientStackForIsr(isr) )
	{
		/* can't propagate the return value of OS_ERROR -> ignore it */
		(void) OS_ERROR(OS_ERROR_InsufficientStack, OS_NULL);
	}
	else
	/* Deviation MISRA-1 */
	if ( OS_RATEMONITORCHECK(isr->rateMonitor) )
	{
		/* Rate monitoring
		 *
		 * !LINKSTO Kernel.Autosar.Protection.TimingProtection.RateLimit.Interrupts, 2
		*/
		id->statusflags |= OS_ISRF_RATEEX;
		OS_ClearIsr(isr);

		/* This error must not attempt to kill the current ISR, because it isn't running.
		*/
		/* can't propagate the return value of OS_ERROR -> ignore it */
		(void) OS_ERROR(OS_ERROR_RateLimitExceeded, OS_NULL);
	}
	else
	{
		/* everything ok */
		allok = OS_TRUE;
	}

	return allok;
}

/* OS_Cat2Entry() - call a Category 2 ISR.
 *
 * This function calls a category 2 ISR.
 *
 *  - The previous value if inKernel is saved, then inKernel is set
 *    to 1.
 *  - The previous value if inFunction is saved, then inFunction is set
 *    to INCAT2.
 *  - If execution timing is configured, any running timer is suspended
 *    and new one for the current ISR is started.
 *  - The pre-ISR hook (if any) is called.
 *  - Then the ISR is called via either the OS_CallIsrDirect wrapper or the OS_CallIsrIndirect wrapper.
 *    The indirect wrapper stores its own context as a way of returning from a killed ISR.
 *  - Then the post-ISR hook (if any) is called.
 *  - The exec timer is stopped and a the suspended timer
 *    (if any) is restarted.
 *  - Finally, the saved value of inFunction is restored, and the saved
 *    value of inKernel is returned. We must remain "in-kernel" because the
 *    interrupt exit routine might need to reschedule.
 *
 * !LINKSTO Kernel.HookRoutines.PrioISRC2, 1
 *		The hook routines are called outside the runPrio region.
 *
 * !LINKSTO Kernel.Feature.IsrHooks.PreIsrHook, 1
 * !LINKSTO Kernel.Feature.IsrHooks.PostIsrHook, 1
 * !LINKSTO Kernel.Feature.IsrHooks, 1
 *
 * !LINKSTO Kernel.Autosar.OsApplication.Trust.Nontrusted, 2
 * !LINKSTO Kernel.Autosar.OsApplication.Trust.Trusted, 2
 *		ISRs are called via an architecture-dependent macro that sets the protection boundaries appropriately
*/
/* Deviation DCG-1 <START> */
/* Deviation DCG-2 <START> */
/* Deviation DCG-3 <START> */
os_uint8_t OS_Cat2Entry(os_isrid_t iid)
{
	const os_isr_t *isr;
	os_isrdynamic_t *id;

	os_uint8_t inKernel;
	os_uint8_t inFunction;

	os_isrid_t oldIsr;

	OS_SAVEISRNESTSUSPEND_DECL(nestOs, nestAll)
	OS_SAVEISRLASTRES_DECL(oldLastRes)
	OS_CAT2PREEMPT_DECL(accSave)

	/* Generic CPU load measurement: enter a busy interval
	*/
	OS_CPULOAD_GENERICCAT2_ENTRY();

	/* Suspend any execution timing that is running
	*/
	OS_CAT2PREEMPTEXECTIMING(&accSave);

	/* Find the interrupt descriptor and get the address of the ISR itself
	*/
	isr = &OS_isrTable[iid];
	id = &OS_isrDynamic[iid];

	/* Save and set inKernel, inFunction, isrCurrent. Save accounting type.
	*/
	inKernel = OS_inKernel;
	OS_SetIsrinKernel();

	inFunction = OS_inFunction;
	OS_inFunction = OS_INCAT2;

	oldIsr = OS_isrCurrent;
	OS_isrCurrent = iid;

	/* !LINKSTO Kernel.Feature.StackCheck.Automatic, 1
	*/
	/* Deviation MISRA-3 */
	if ( OS_IsrErrorChecksOk(isr, id) )
	{
		OS_SaveIsrLastRes(&oldLastRes);
		OS_SaveIsrNestSuspend(&nestOs, &nestAll);

		/* Call the PreISRHook() if configured
		*/
		OS_CALLPREISRHOOK(iid);

		/* Do ISR exec timing if configured
		 *
		 * !LINKSTO Kernel.Autosar.Protection.TimingProtection.ExecutionTime.Measurement.ISR, 1
		*/
		OS_STARTISREXECTIMING(isr->execBudget);

		/* Clear the "killed" flag. This flag gets set if the ISR gets killed. It is used
		 * to avoid falsely reporting that the ISR has terminated without releasing resources
		 * and/or re-enabling interrupts.
		*/
		OS_MarkIsrNotKilled(id);

		OS_TRACE_STATE_ISR(iid, OS_TRACE_ISR_SUSPENDED, OS_TRACE_ISR_RUNNING);

		OS_HW_TRACE_ISR(iid);
		if ( OS_CallIsrDirectly() )
		{
			OS_CallIsrDirect(isr, id);
		}
		else
		{
			/* We can't set up the protection registers here because we don't
			 * yet know how much stack OS_CallIsr() requires.
			*/
			/* Deviation MISRA-2 */
			OS_CallIsrIndirect(isr, id);
		}

		OS_TRACE_STATE_ISR(iid, OS_TRACE_ISR_RUNNING, OS_TRACE_ISR_SUSPENDED);

		/* Stop the execution timing that was started earlier.
		*/
		OS_STOPISREXECTIMING(isr);

		/* Call the PostISRHook() if configured
		*/
		OS_CALLPOSTISRHOOK(iid);

		/* Restore the protection registers for the interrupted
		 * task or ISR.
		 * See comment above for why we can't do it in ArchCallIsr().
		 * WARNING: the registers are set back to their initial
		 * values for the interrupted task/ISR. This means that
		 * dynamic modification of the registers by a task or
		 * ISR is NOT SUPPORTED
		*/
		/* This implementation is not optimal - it always restores the protection
		 * registers even if they were not changed. A better implementation needs
		 * the concept of a "memory-protection owner" similar to that for MMU.
		 * See also ASCOS-1451.
		 * Remove this comment when implemented.
		*/
		if ( oldIsr < OS_nInterrupts )
		{
			OS_SETISRPROTECTION(&OS_isrTable[oldIsr], &OS_isrDynamic[oldIsr]);
		}
		else
		if ( OS_taskCurrent != OS_NULL )
		{
			OS_SETPROTECTION(OS_taskCurrent);
		}
		else
		{
			/* MISRA-C */
		}

		/* Check that all resources taken have been released.
		 * OS_isrCurrent still contains the ISR-ID, so can be used by
		 * the error handler to determine who is guilty.
		 * !LINKSTO Kernel.Autosar.ServiceErrors.Miscellaneous.IsrReturn.Resources, 2
		*/
		if ( OS_IsrOccupiesResource() )
		{
			OS_CleanUpISRResources(id);
		}
		else
		if ( OS_IsrSuspendNestingError(nestOs, nestAll) )
		{
			if ( !OS_IsIsrKilled(id) )
			{
				/* ISR wasn't killed, so this is an error in the ISR.
				 *
				 * !LINKSTO Kernel.Feature.RuntimeChecks.CheckSuspendResumeNesting, 1,
				 * !        Kernel.Feature.RuntimeChecks, 1,
				 * !        Kernel.Autosar.ServiceErrors.Miscellaneous.IsrReturn.DisabledInterrupts, 2,
				 */
				/* can't propagate the return value of OS_ERROR -> ignore it */
				(void) OS_ERROR(OS_ERROR_InterruptDisabled, OS_NULL);
			}
			/* No cleaning up to do - kernel manages interrupts perfectly
			 *
			 * !LINKSTO Kernel.Autosar.Protection.ProtectionHook.KillISR, 1
			*/
		}
		else
		{
			/* MISRA-C */
		}

		OS_RestoreIsrLastRes(oldLastRes);
		OS_RestoreNestSuspend(nestOs, nestAll);
	}

	/* Check for a kernel (or ISR) stack overflow. This shouldn't happen if trusted ISRs
	 * keep within their bounds, but there's always the possibility ...
	 *
	 * Note: On some CPU families, this code may run on a different stack than
	 * the ISR, so both need to get checked.
	 *
	 * !LINKSTO Kernel.Autosar.StackMonitoring.Detection, 2
	*/
	if ( OS_CAT2STACKOVERFLOW() )
	{
		/* !LINKSTO Kernel.Autosar.StackMonitoring.Reporting.NoProtectionHook, 1
		 * !LINKSTO Kernel.Autosar.StackMonitoring.Reporting.ProtectionHook, 1
		 * !LINKSTO Kernel.Feature.StackCheck.Automatic, 1
		 *
		 * The protection hook is always called (if configured).
		 * The default action (which is what happens if there is no protection hook) is to shut down the system.
		*/
		/* can't propagate the return value of OS_ERROR -> ignore it */
		(void) OS_ERROR(OS_ERROR_KernelStackOverflow, OS_NULL);
	}

	/* Restore previous values of isrCurrent and inFunction
	*/
	OS_isrCurrent = oldIsr;
	OS_inFunction = inFunction;
	/* Must be after OS_isrCurrent to avoid race with category 1 ISR tracing. */
	OS_HW_TRACE_ISR(oldIsr);

	/* Check if the interrupted ISR is still permitted to run (i.e.
	 * hasn't been killed by a higher-priority ISR or something
	*/
	if ( OS_KillNextIsr() )
	{
		/* On targets which require a late notification of the handled interrupt,
		 * this must done before killing further interrupts to properly end the
		 * (nested) interrupt(s) in the handler.
		 * NOTE: OS_KillIsr() won't return!
		*/
		OS_LateEndOfInterrupt(isr);

		/* no way to propagate errors, and none expected anyways -> ignore return value */
		(void) OS_KillIsr(oldIsr);
	}

	/* Now we resume the execution timing for the interrupted whatever.
	*/
	OS_CAT2RESUMEEXECTIMING(&accSave);

	/* inKernel remains set, but the old value is returned to the
	 * exit function so that it can determine if a task switch is needed.
	*/
	return inKernel;
}
/* Deviation DCG-3 <END> */
/* Deviation DCG-2 <END> */
/* Deviation DCG-1 <END> */

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
