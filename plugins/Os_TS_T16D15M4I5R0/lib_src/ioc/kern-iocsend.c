/* kern-iocwrite.c
 *
 * This file contains the function OS_IocWrite()
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-iocsend.c 17742 2014-02-25 15:51:05Z tojo2507 $
*/
#include <Os_types.h>
#include <Os_kernel.h>
#include <Os_userkernel.h>
#include <Ioc/Ioc.h>
#include <Ioc/Ioc_kern.h>

os_result_t OS_KernIocSend(os_uint32_t id, const void * const data[])
{
	os_uint32_t dIdx, dtIdx, j;
	os_intstatus_t is;
	os_result_t retval = IOC_E_OK;
	os_memoryaccess_t access;
	const Ioc_QDataConfigType * dConfig;

	/* check parameters */

	if ( id >= Ioc_NumOfQueuedData )
	{
		retval = IOC_E_SEG_FAULT;
	}
	else
	{
		dConfig = &(Ioc_QDataConfig[id]);

		/* check that data pointer array resides in valid memory */
		access = OS_KernIocCheckMemoryAccess(data,
				((os_size_t) dConfig->dataTypeEnd - (os_size_t) dConfig->dataTypeStart) * sizeof(void *));
		if ( (access & OS_MA_READ) == 0 )
		{
			retval = IOC_E_SEG_FAULT;
		}
		else
		{
			/* check that every data pointer points to valid memory */
			dIdx = 0;
			for ( dtIdx=dConfig->dataTypeStart; dtIdx<dConfig->dataTypeEnd; dtIdx++)
			{
				access = OS_KernIocCheckMemoryAccess(data[dIdx], Ioc_QDataTypeConfig[dtIdx].dataSize);
				if ( (access & OS_MA_READ) == 0 )
				{
					retval = IOC_E_SEG_FAULT;
					break;
				}
				dIdx++;
			}
		}
	}

	/* function starts here */

	if (retval == IOC_E_OK)
	{
		/* lock data structures */
		is = OS_IOC_LOCKDATA(id);

		if (IOC_QUEUE_FULL(Ioc_QMeta[id], Ioc_QDataConfig[id].dataQueueLen))
		{
			Ioc_QMeta[id].dataLost = 1;
			retval = IOC_E_LIMIT;
		}
		else
		{
			/* copy all data elements */
			dIdx = 0;
			for ( dtIdx=Ioc_QDataConfig[id].dataTypeStart; dtIdx<Ioc_QDataConfig[id].dataTypeEnd; dtIdx++)
			{
				Ioc_QIndexType startIdx = ((Ioc_QMeta[id].tail + Ioc_QMeta[id].count) % Ioc_QDataConfig[id].dataQueueLen)
					* Ioc_QDataTypeConfig[dtIdx].dataSize;
				for ( j=0; j<Ioc_QDataTypeConfig[dtIdx].dataSize; j++)
				{
					((os_uint8_t *)Ioc_QDataTypeConfig[dtIdx].dataBuffer)[j+startIdx] = ((const os_uint8_t *)data[dIdx])[j];
				}
				dIdx++;
			}
			Ioc_QMeta[id].count++;
		}

		/* unlock data structures */
		OS_IOC_UNLOCKDATA(id, is);
	}

	return retval;
}


/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
