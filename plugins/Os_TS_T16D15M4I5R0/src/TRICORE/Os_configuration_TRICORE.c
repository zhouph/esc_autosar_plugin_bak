/* Os_configuration_TRICORE.c
 *
 * This file provides architecture-specific kernel configuration data
 * for TRICORE.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_configuration_TRICORE.c 17931 2014-04-11 09:36:18Z dh $
*/

#include <Os_configuration.h>
#include <Os_syscalls.h>
#include <Os_kernel.h>
#include <board.h>
#include <Os_proosek.h>
#include <TRICORE/Os_TRICORE_cpu.h>
#include <TRICORE/Os_TRICORE_stm.h>
#include <Os_Version.h>			/* To support pathing older versions */

#include <memmap/Os_mm_const_begin.h>

/*!
 * OS_intDisableLevel
 *
 * The interrupt level for disabling OS (category 2) interrupts
*/
const os_intstatus_t OS_intDisableLevel = OS_CAT2LOCK|OS_ICR_IE;

/*!
 * OS_kernDisableLevel
 *
 * The interrupt level for disabling OS (category 2) interrupts AND
 * kernel interrupts.
*/
const os_intstatus_t OS_kernDisableLevel = OS_KERNLOCK|OS_ICR_IE;

/*!
 * OS_intDisableLevelAll
 *
 * The interrupt level for disabling all (category 1 and 2) interrupts
*/
const os_intstatus_t OS_intDisableLevelAll = OS_CAT1LOCK|OS_ICR_IE;


#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP) /* Aurix */

/* OS_cpuWdt contains the address of the WDT for the core on which the OS is configured to run.
 *
 * If we're patching an older version (< 53) the OS_TRICORE_CORE_ID is not available, so the
 * pointer needs to be a variable that's initialized at run-time (first use).
*/
#if (OS_TRICORE_CORE_ID >= 0) && (OS_TRICORE_CORE_ID <= 2)
os_tricore_aurix_wdt_t * const OS_cpuWdt = &OS_pwr.pwr_wdt[OS_TRICORE_CORE_ID];
#else
#error "Unsupported core id configured!"
#endif

/* OS_safetyWdt contains the address of the Safety WDT.
*/
os_tricore_aurix_wdt_t * const OS_safetyWdt = &OS_pwr.pwr_swdt;

/* OS_timestampStm
 *
 * The pointer to the STM module configured as timestamp timer
*/
os_tricorestm_t * const OS_timestampStm = (os_tricorestm_t *)OS_TRICORE_TIMESTAMPSTMBASE;

#endif /* (OS_TRICOREARCH == OS_TRICOREARCH_16EP) */

/* OS_kernDpr, OS_kernCpr, OS_kernDpm, OS_kernCpm
 *
 * Data and code protection register settings (set 0) for kernel mode.
 * The kernel is allowed read, write and execute access anywhere in the
 * physical address range (0x80000000 and up)
*/

#if (OS_TRICOREARCH != OS_TRICOREARCH_16EP)
#if ((OS_CONFIGMODE & OS_DBGPROT) == OS_DBGPROT_PART)
#define OS_KERNCPM	0
#define OS_USERCPM	0
#else
#define OS_KERNCPM	OS_CPM_XE0
#define OS_USERCPM	OS_CPM_XE0
#endif
#endif

const os_uint32_t OS_kernDpr[OS_N_DPR * 2] =
{	OS_PHYS_ADDR_SPACE_START, 0xffffffffu,	/* Entire physical space, read/write */
	0, 0,					/* Not used */
	0, 0,					/* Not used */
	0, 0					/* Not used */
};

const os_uint32_t OS_kernCpr[OS_N_CPR * 2] =
{	OS_PHYS_ADDR_SPACE_START, 0xffffffffu,	/* Entire physical space, executable */
	0, 0					/* Not used */
};

#include <memmap/Os_mm_const_end.h>

/*!
 * OS_arbCycles
 *
 * The number of arbitration cycles required for the generated
 * interrupt vector table.
*/
#include <memmap/Os_mm_const8_begin.h>
const os_uint8_t OS_arbCycles = OS_ARBCYCLES;
#include <memmap/Os_mm_const8_end.h>


#if (OS_TRICOREARCH != OS_TRICOREARCH_16EP)
#include <memmap/Os_mm_const32_begin.h>
const os_uint32_t  OS_kernDpm = OS_DPM_WE0|OS_DPM_RE0;
const os_uint32_t  OS_kernCpm = OS_KERNCPM;
const os_uint32_t  OS_userCpm = OS_USERCPM;
#include <memmap/Os_mm_const32_end.h>
#endif

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
