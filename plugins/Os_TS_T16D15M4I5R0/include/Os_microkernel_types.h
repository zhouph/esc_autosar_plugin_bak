/* Os_microkernel_types.h
 *
 * This file maps OS data types onto microkernel types
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_microkernel_types.h 18186 2014-05-20 14:45:39Z stpo8218 $
*/
#ifndef __OS_MICROKERNEL_TYPES_H
#define __OS_MICROKERNEL_TYPES_H

#include <Os_defs.h>
#include <public/Mk_defs.h>

/* This file only gets included when the OS is being compiled to run under the microkernel
 * This is a sanity check.
*/
#if OS_KERNEL_TYPE != OS_MICROKERNEL
#error "Os_microkernel_types.h should only be included when using the microkernel."
#endif

#include <public/Mk_public_types.h>
#include <public/Mk_autosar.h>

/* map to microkernel definition */
#define OS_ARCH_HAS_64BIT MK_HAS_INT64

/* endianess */
#if MK_HWENDIAN==MK_LITTLEENDIAN
#define OS_ENDIAN OS_LITTLEENDIAN
#elif MK_HWENDIAN==MK_BIGENDIAN
#define OS_ENDIAN OS_BIGENDIAN
#else
#error "MK_HWENDIAN value out of range."
#endif

/* Define all the types that the OS expects to see in terms of Microkernel data types
*/
#ifndef OS_ASM
typedef mk_int8_t os_int8_t;
typedef mk_int16_t os_int16_t;
typedef mk_int32_t os_int32_t;
typedef mk_uint8_t os_uint8_t;
typedef mk_uint16_t os_uint16_t;
typedef mk_uint32_t os_uint32_t;
typedef mk_address_t os_address_t;
typedef mk_size_t os_size_t;
typedef mk_parametertype_t os_intstatus_t;
typedef mk_parametertype_t os_paramtype_t;
typedef mk_stackelement_t os_stackelement_t;
typedef mk_uint16_t os_errorresult_t;

typedef struct os_appcontext_s os_appcontext_t;
#endif

#ifndef OS_NULL
#define OS_NULL	MK_NULL
#endif

/*!
 * os_tick_t
 *
 * Tick counter - only if not already defined
*/
#ifndef OS_ASM
typedef TickType os_tick_t;
#endif
#define OS_MAXTICK		0xffffffff
#define OS_SIZEOF_TICK	4

/*!
 * os_alarmbase_t
 *
 * Alarmbase structure for GetAlarmBase
*/
#ifndef OS_ASM
typedef AlarmBaseType os_alarmbase_t;
#endif

/*
 * os_timestamp_t
 *
 * Representation of the timestamp (tick) value.
*/
#ifndef OS_ASM
typedef mk_time_t os_timestamp_t;
#endif

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
