/* Os_types_TRICORE.h
 *
 * This file defines the basic data types for TRICORE
 *
 * See also: Os_types.h
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_types_TRICORE.h 19525 2014-10-29 09:12:57Z thdr9337 $
*/

#ifndef __OS_TYPES_TRICORE_H
#define __OS_TYPES_TRICORE_H

#if (OS_CPU == OS_TC10GP)
#include <TRICORE/TC10GP/Os_types_TC10GP.h>
#elif (OS_CPU == OS_TC1387)
#include <TRICORE/TC1387/Os_types_TC1387.h>
#elif (OS_CPU == OS_TC1766)
#include <TRICORE/TC1766/Os_types_TC1766.h>
#elif (OS_CPU == OS_TC1767)
#include <TRICORE/TC1767/Os_types_TC1767.h>
#elif (OS_CPU == OS_TC1775)
#include <TRICORE/TC1775/Os_types_TC1775.h>
#elif (OS_CPU == OS_TC1782)
#include <TRICORE/TC1782/Os_types_TC1782.h>
#elif (OS_CPU == OS_TC1791)
#include <TRICORE/TC1791/Os_types_TC1791.h>
#elif (OS_CPU == OS_TC1793)
#include <TRICORE/TC1793/Os_types_TC1793.h>
#elif (OS_CPU == OS_TC1796)
#include <TRICORE/TC1796/Os_types_TC1796.h>
#elif (OS_CPU == OS_TC1797)
#include <TRICORE/TC1797/Os_types_TC1797.h>
#elif (OS_CPU == OS_TC1798)
#include <TRICORE/TC1798/Os_types_TC1798.h>
#elif (OS_CPU == OS_TC1920)
#include <TRICORE/TC1920/Os_types_TC1920.h>
#elif (OS_CPU == OS_TC23XL)
#include <TRICORE/TC23XL/Os_types_TC23XL.h>
#elif (OS_CPU == OS_TC22XL)
#include <TRICORE/TC22XL/Os_types_TC22XL.h>
#elif (OS_CPU == OS_TC277)
#include <TRICORE/TC277/Os_types_TC277.h>
#elif (OS_CPU == OS_TC27XFPGA)
#include <TRICORE/TC27XFPGA/Os_types_TC27XFPGA.h>
#elif (OS_CPU == OS_TC2D5)
#include <TRICORE/TC2D5/Os_types_TC2D5.h>
#else
#error "Unsupported derivative."
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Architecture data type characteristics
*/
#define OS_ENDIAN		OS_LITTLEENDIAN
#define OS_ARCH_HAS_64BIT	0

/* Sizes for computing struct offsets for assembler
*/
#define OS_SIZEOF_PTR	4
#define OS_SIZEOF_FPTR	OS_SIZEOF_PTR

#ifndef OS_ASM
/* Fixed-length types
*/

typedef unsigned char		os_uint8_t;
typedef signed char			os_int8_t;
typedef unsigned short		os_uint16_t;
typedef signed short		os_int16_t;
typedef unsigned int		os_uint32_t;
typedef signed int			os_int32_t;

/* Types for address/size of memory object
*/
typedef unsigned int		os_address_t;
typedef unsigned int		os_size_t;

/* Type for a stack element.
*/
typedef unsigned int		os_stackelement_t;

/* Type for a service parameter.
 *
 * An integer type big enough to hold any OS Service parameter.
 * Used for error handling.
*/
typedef os_uint32_t os_paramtype_t;

#endif

#define OS_SIZEOF_STACKELEMENT	4

/*!
 * os_isrid_t
 *
 * Type big enough to hold an ISR Id. This type limits the number
 * of ISRs permissible in the OIL file (i.e. simultaneuosly usable interrupts).
 * 16-bit allows 65535 ISRs, plus OS_NULLISR.
*/
#ifndef OS_ASM
typedef os_uint16_t os_isrid_t;
#endif

/* OS_NULLISR - "invalid ISR" id.
 * A value of 0xfff limits the maximum number of ISRs to 4k which should
 * be big enough for any future derivative.
 * Mustn't conflict with the use in os_taskorisr_t, see below.
*/
#define OS_NULLISR  OS_U(0xfff)

/*!
 * os_taskorisr_t
 *
 * Variable big enough to hold either a task ID or an isr ID and be
 * capable of telling which type it is.
 *
 * Tasks run from 0 to 254. 255 means no specific task.
 * ISRs run from 256 upwards. (OS_TOI_ISR + OS_NULLISR) means no specific ISR.
*/
#ifndef OS_ASM
typedef os_uint16_t os_taskorisr_t;
#endif

#define OS_TOI_TASK				OS_U(0)
#define OS_TOI_ISR				OS_U(0x100)
#define OS_TOI_CURRENTCONTEXT	OS_U(0xffff)

#define OS_TaskToTOI(i)			((OS_TOI_TASK)+(i))
#define OS_IsrToTOI(i)			((OS_TOI_ISR)+(i))
#define OS_IsTaskId(i)			((i)<OS_TOI_ISR)
#define OS_TOIToTask(i)			(i)
#define OS_TOIToIsr(i)			((i)-OS_TOI_ISR)
#define OS_SplitTaskOrIsr(i)	( OS_IsTaskId(i) ? OS_TOIToTask(i) : ((i)-OS_TOI_ISR) )

/* os_clzword_t - type of a word for which a CLZ or similar instruction is defined.
*/
#define OS_SIZEOF_CLZWORD	4
#define OS_CLZWORD_NBITS	32
#ifndef OS_ASM
typedef os_uint32_t os_clzword_t;
#endif

#ifdef __cplusplus
}
#endif

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
