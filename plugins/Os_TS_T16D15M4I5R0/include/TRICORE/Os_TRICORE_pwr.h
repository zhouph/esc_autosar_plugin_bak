/* Os_TRICORE_pwr.h - Tricore PWR module description
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_TRICORE_pwr.h 19525 2014-10-29 09:12:57Z thdr9337 $
*/

/*	This file contains a description of Tricore's PWR module which
 *	contains the PLL control registers, watchdog, etc. On some variants
 *	this module is called the System Control Unit (SCU)
*/

#ifndef OS_TRICORE_PWR_H
#define OS_TRICORE_PWR_H

#include <Os_types.h>
#include <Os_usuffix.h>
#include <TRICORE/Os_TRICORE_core.h>

#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
#include <TRICORE/Os_TRICORE_aurix_wdt.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if !defined(OS_ASM)

typedef struct os_tricorepwr_s
{
	os_tricoremodule_t	pwr_module_hdr;

#if (OS_CPU == OS_TC1920)
	/* On TC1920 Tricores there is no PWR. There's a module called SCU,
	 * which looks similar but has some different registers and the
	 * order is different.
	 * We retain the name PWR here, and keep the register names the
	 * same as far as possible.
	*/
	os_reg32_t		reserved1[1];	/* 0c */
	os_reg32_t		pwr_rstreq;		/* 10 Reset request register */
	os_reg32_t		pwr_rstsr;		/* 14 Reset status register */
	os_reg32_t		pwr_nmisr;		/* 18 NMI status register */
	os_reg32_t		pwr_nmclr;		/* 1c NMI clear status register */
	os_reg32_t		pwr_wdtcon0;	/* 20 Watchdog timer control register 0 */
	os_reg32_t		pwr_wdtcon1;	/* 24 Watchdog timer control register 1 */
	os_reg32_t		pwr_wdtsr;		/* 28 Watchdog timer status register */
	os_reg32_t		pwr_stbcon;		/* 2c Watchdog timer status register */
	os_reg32_t		pwr_pmcon;		/* 30 Power management control register (not 1796) */
	os_reg32_t		pwr_pmcsr;		/* 34 Power management control & status register */
	os_reg32_t		pwr_pllclc;		/* 38 PLL clock control register */
	os_reg32_t		pwr_osccon;		/* 3c Oscillator control */
	os_reg32_t		pwr_scucon;		/* 40 SCU control register */
	os_reg32_t		pwr_scutrstat;	/* 44 Port 5 trace status register */
	os_reg32_t		reserved2[6];	/* 48,4c,50,54,58,5c */
	os_reg32_t		pwr_ptcr;		/* 60 Port driver temp compensation */
	os_reg32_t		pwr_tclr;		/* 64 Temp compensation level */
	os_reg32_t		pwr_ebpcr;		/* 68 External bus pin control */
	os_reg32_t		reserved3[1];	/* 6c */
	os_reg32_t		pwr_manid;		/* 70 Manufacturer identification register */
	os_reg32_t		pwr_chipid;		/* 74 Chip identification register */
	os_reg32_t		pwr_rtid;		/* 78 Redesign tracing identification register */
	os_reg32_t		reserved4[1];	/* 7c */
	os_reg32_t		pwr_eicr[5];	/* 80 External input control */
	os_reg32_t		reserved5[1];	/* 94 */
	os_reg32_t		pwr_eifr;		/* 98 External input flag */
	os_reg32_t		pwr_fmr;		/* 9c Flag modification */
	os_reg32_t		pwr_igcr[4];	/* a0 Interrupt gating */
	os_reg32_t		reserved6[3];	/* b0,b4,b8 */
	os_reg32_t		pwr_pdrr;		/* bc Pattern detection */
	os_reg32_t		pwr_drvctr[4];	/* c0 Driver control */
	os_reg32_t		reserved7[7];	/* d0,d4,d8,dc,e0,e4,e8 */
	os_reg32_t		pwr_src[5];		/* ec Service request */

#elif (OS_CPU == OS_TC1797) \
	|| (OS_CPU == OS_TC1767) \
	|| (OS_CPU == OS_TC1387) \
	|| (OS_CPU == OS_TC1782) \
	|| (OS_CPU == OS_TC1798) \
	|| (OS_CPU == OS_TC1791) \
	|| (OS_CPU == OS_TC1793)

	os_reg32_t		reserved1[1];	/* 0x00c */
	os_reg32_t		pwr_osccon;		/* 0x010  OSC Control Register */
	os_reg32_t		pwr_pllstat;	/* 0x014  PLL Status Register */
	os_reg32_t		pwr_pllcon[2];	/* 0x018+ PLL Configuration Register */
#if (OS_CPU == OS_TC1797)
	os_reg32_t		pwr_pllerayctr;	/* 0x020  PLL_ERAY Control and Status Register */
	os_reg32_t		reserved2[3];	/* 0x024 */
#elif (OS_CPU == OS_TC1387)  \
    || (OS_CPU == OS_TC1782) \
    || (OS_CPU == OS_TC1798) \
    || (OS_CPU == OS_TC1791) \
    || (OS_CPU == OS_TC1793)
	os_reg32_t		reserved2[1];		/* 0x020 */
	os_reg32_t		pwr_plleraystat;	/* 0x024  PLL ERAY Status Register */
	os_reg32_t		pwr_plleraycon[2];	/* 0x028+ PLL ERAY Configuration Register */
#else
	os_reg32_t		reserved2[4];	/* 0x020 */
#endif
	os_reg32_t		pwr_ccucon[2];	/* 0x030+ CCU Control Register */
	os_reg32_t		pwr_fdr;		/* 0x038  Fractional Divider Register */
#if (OS_CPU == OS_TC1387)
	os_reg32_t		reserved2b[1];	/* 0x03c  */
#else
	os_reg32_t		pwr_extcon;		/* 0x03c  External Clock Control Register */
#endif
	os_reg32_t		pwr_syscon;		/* 0x040  System Control Register */
#if (OS_CPU == OS_TC1798)\
	|| (OS_CPU == OS_TC1791) || (OS_CPU == OS_TC1793)
	os_reg32_t		pwr_ccucon2;	/* 0x044  CCU Control Register 2 */
	os_reg32_t		reserved3[2];	/* 0x48+ */
#else
	os_reg32_t		reserved3[3];	/* 0x044 */
#endif
	os_reg32_t		pwr_rststat;	/* 0x050  Reset Status Register */
	os_reg32_t		pwr_rstcntcon;	/* 0x054  Reset Counter Control Register */
	os_reg32_t		pwr_rstcon;		/* 0x058  Reset CON Register */
	os_reg32_t		pwr_arstdis;	/* 0x05c  Application Reset Disable Register*/
	os_reg32_t		pwr_swrstcon;	/* 0x060  Software Reset Configuration Register*/
	os_reg32_t		reserved4[3];	/* 0x064 */
#if (OS_CPU == OS_TC1387) || (OS_CPU == OS_TC1798) || (OS_CPU == OS_TC1791)	|| (OS_CPU == OS_TC1793)
	os_reg32_t		pwr_esrcfg[2];	/* 0x070+ ESR Configuration Register */
	os_reg32_t		reserved5[2];	/* 0x078+ */
#else
	os_reg32_t		pwr_esrcfg[3];	/* ESR Configuration Register */
	os_reg32_t		reserved5[1];	/* 0x07C*/
#endif
	os_reg32_t		pwr_eicr[2];	/* 0x080+ External Input Channel Register */
	os_reg32_t		pwr_eifr;		/* 0x088  External Input Flag Regsiter */
	os_reg32_t		pwr_fmr;		/* 0x08c  Flag Modification Register */
	os_reg32_t		pwr_pdrr;		/* 0x090  Pattern Detection Result Register */
	os_reg32_t		pwr_igcr[2];	/* 0x094+ Interrupt Gating Register */
	os_reg32_t		reserved6[1];	/* 0x09c */
	os_reg32_t		pwr_iocr;		/* 0x0a0  Input/Output Control Register */
	os_reg32_t		pwr_out;		/* 0x0a4  Output Register */
	os_reg32_t		pwr_omr;		/* 0x0a8  Output Modification Register*/
	os_reg32_t		pwr_in;			/* 0x0ac  Input Register */
	os_reg32_t		pwr_pmcsr;		/* 0x0b0  Power Management Control and Status Register */
	os_reg32_t		reserved7[3];	/* 0x0b4+ */
	os_reg32_t		pwr_ststat;		/* 0x0c0  Start-up Status Register */
	os_reg32_t		pwr_stcon;		/* 0x0c4  Start-up Configuration */
	os_reg32_t		reserved8[2];	/* 0x0c8+ */
#if (OS_CPU == OS_TC1387)    \
    || (OS_CPU == OS_TC1782) \
    || (OS_CPU == OS_TC1798) \
    || (OS_CPU == OS_TC1791) \
    || (OS_CPU == OS_TC1793)
	os_reg32_t		pwr_ecccon;		/* 0x0d0  ECC Error Trap Control */
	os_reg32_t		pwr_eccstat;	/* 0x0d4  ECC Error Trap Status */
	os_reg32_t		pwr_eccclr;		/* 0x0d8  ECC Error Trap Clear */
	os_reg32_t		reserved9[1];	/* 0x0dc */
#else
	os_reg32_t		pwr_petcr;		/* Parity Error Trap Control */
	os_reg32_t		pwr_petsr;		/* Parity Error Trap Status */
	os_reg32_t		reserved9[2];	/* 0xd8 */
#endif
	os_reg32_t		pwr_dtsstat;	/* 0x0e0  Die Temperature Sensor Status Register */
	os_reg32_t		pwr_dtscon;		/* 0x0e4  Die Temperature Sensor Control Register */
	os_reg32_t		reserved10[2];	/* 0x0e8+ */
	os_reg32_t		pwr_wdtcon0;	/* 0x0f0  WDT Control Register 0 */
	os_reg32_t		pwr_wdtcon1;	/* 0x0f4  WDT Control Register 1*/
	os_reg32_t		pwr_wdtsr;		/* 0x0f8  WDT Status Register */
	os_reg32_t		reserved11[1];	/* 0x0fc+ */
	os_reg32_t		pwr_emsr;		/* 0x100  Emergency Stop Register */
	os_reg32_t		reserved12[3];	/* 0x104+ */
	os_reg32_t		pwr_intstat;	/* 0x110  Interrupt Status Register */
	os_reg32_t		pwr_intset;		/* 0x114  Interrupt Set Register */
	os_reg32_t		pwr_intclr;		/* 0x118  Interrupt Clear Register */
	os_reg32_t		pwr_intdis;		/* 0x11c  Interrupt Disable Register */
	os_reg32_t		pwr_intnp;		/* 0x120  Interrupt Node Pointer Register */
	os_reg32_t		pwr_trapstat;	/* 0x124  Trap Status Register */
	os_reg32_t		pwr_trapset;	/* 0x128  Trap Set Register */
	os_reg32_t		pwr_trapclr;	/* 0x12c  Trap Clear Register */
	os_reg32_t		pwr_trapdis;	/* 0x130  Trap Disable Register */
	os_reg32_t		reserved13[3];	/* 0x134+ */
	os_reg32_t		pwr_chipid;		/* 0x140  Chip Identification */
	os_reg32_t		pwr_manid;		/* 0x144  Manufacture Identification Register */
#if (OS_CPU == OS_TC1387) || (OS_CPU == OS_TC1782)
	os_reg32_t		pwr_rtid;		/* 0x148  Redesign Trace Identification Register */
	os_reg32_t		reserved14[41];	/* 0x14c+ */
#elif (OS_CPU == OS_TC1798) || (OS_CPU == OS_TC1791) || (OS_CPU == OS_TC1793)
	os_reg32_t		pwr_rtid;		/* 0x148  Redesign Trace Identification Register */
	os_reg32_t		reserved14[5];	/* 0x14c+ */
	os_reg32_t		pwr_memtest;	/* 0x160  Memory Test Register */
	os_reg32_t		reserved14b[35];/* 0x164+ */
#else
	os_reg32_t		reserved14[42];	/* 0x148 */
#endif
	os_reg32_t		pwr_src3;		/* 0x1f0  Service Request Control Register 3 */
	os_reg32_t		pwr_src2;		/* 0x1f4  Service Request Control Register 2 */
	os_reg32_t		pwr_src1;		/* 0x1f8  Service Request Control Register 1 */
	os_reg32_t		pwr_src0;		/* 0x1fc  Service Request Control Register 0 */

#elif (OS_CPU == OS_TC1796) || (OS_CPU == OS_TC1766)
	/* The TC1796/TC1766 PWR ...
	*/
	os_reg32_t		pwr_sclkfdr;	/* System clock fractional divider (1796) */
	os_reg32_t		pwr_rstreq;		/* Reset request register */
	os_reg32_t		pwr_rstsr;		/* Reset status register */
	os_reg32_t		pwr_osccon;		/* Oscillator control */
	os_reg32_t		reserved1[1];
	os_reg32_t		pwr_wdtcon0;	/* Watchdog timer control register 0 */
	os_reg32_t		pwr_wdtcon1;	/* Watchdog timer control register 1 */
	os_reg32_t		pwr_wdtsr;		/* Watchdog timer status register */
	os_reg32_t		pwr_nmisr;		/* NMI status register */
	os_reg32_t		pwr_pmcon;		/* Power management control register (not 1796) */
	os_reg32_t		pwr_pmcsr;		/* Power management control & status register */
	os_reg32_t		pwr_sclir;		/* Software configuration latched inputs */
	os_reg32_t		reserved2[1];
	os_reg32_t		pwr_pllclc;		/* PLL clock control register */
	os_reg32_t		pwr_emsr;		/* Emergency stop register */
	os_reg32_t		pwr_tccon;		/* Temperature compensation control (1796) */
	os_reg32_t		reserved3[1];
	os_reg32_t		pwr_scucon;		/* SCU control register */
	os_reg32_t		pwr_scustat;	/* SCU status register */
	os_reg32_t		pwr_tclr[2];	/* Temperature compensation level registers (1796) */
	os_reg32_t		pwr_pscactl;	/* PCP SRAM control array control register (1796) */
	os_reg32_t		pwr_pscadin;	/* PCP SRAM control array data in register (1796) */
	os_reg32_t		pwr_pscadout;	/* PCP SRAM control array data out register (1796) */
	os_reg32_t		reserved4[1];
	os_reg32_t		pwr_manid;		/* Manufacturer identification register */
	os_reg32_t		pwr_chipid;		/* Chip identification register */
	os_reg32_t		pwr_rtid;		/* Redesign tracing identification register */
	os_reg32_t		reserved5[1];
	os_reg32_t		pwr_eicr[2];	/* External input channel registers (1796) */
	os_reg32_t		pwr_eifr;		/* External input flag register (1796) */
	os_reg32_t		pwr_fmr;		/* Flag modification register (1796) */
	os_reg32_t		pwr_pdrr;		/* Pattern detection result (1796) */
	os_reg32_t		pwr_igcr[2];	/* Interrupt gating registers (1796) */
	os_reg32_t		pwr_tgadc[2];	/* ADC trigger gating registers (1796) */
	os_reg32_t		reserved6[3];
	os_reg32_t		pwr_ptcon;		/* Pad test control register (1796) */
	os_reg32_t		pwr_ptdat[4];	/* Pad test data registers (1796) */
	os_reg32_t		reserved7[15];

#elif (OS_TRICOREARCH == OS_TRICOREARCH_16EP) /* Aurix */

	os_reg32_t		reserved1[1];		/* 0xC */
	os_reg32_t		pwr_osccon;			/* 0x10 OSC Control */
	os_reg32_t		pwr_pllstat;		/* 0x14	PLL Status */
	os_reg32_t		pwr_pllcon[3];		/* 0x18	PLL Configuration 0-2 */
	os_reg32_t		pwr_plleraystat;	/* 0x24	PLL_ERAY Status */
	os_reg32_t		pwr_plleraycon[2];	/* 0x28 PLL_ERAY Configuration 0 and 1 */
	os_reg32_t		pwr_ccucon[2];		/* 0x30	CCU Control 0 and 1 */
	os_reg32_t		pwr_fdr;			/* 0x38	Fractional Divider */
	os_reg32_t		pwr_extcon;			/* 0x3C	External Clock Control */
	os_reg32_t		pwr_ccucon_2[4];	/* 0x40 CCU Control 2, 3, 4 and 5 */
	os_reg32_t		pwr_rststat;		/* 0x50	Reset Status */
	os_reg32_t		reserved3[1];		/* 0x54 */
	os_reg32_t		pwr_rstcon;			/* 0x58 Reset CON */
	os_reg32_t		pwr_arstdis;		/* 0x5C	Application Reset Disable */
	os_reg32_t		pwr_swrstcon;		/* 0x60	Software Reset Configuration */
	os_reg32_t		pwr_rstcon2;		/* 0x64	Additional reset control */
	os_reg32_t		reserved4[1];		/* 0x68 */
	os_reg32_t		pwr_evrrstcon;		/* 0x6C EVR Reset Control */
	os_reg32_t		pwr_esrcfg[2];		/* 0x70 ESR0 and 1 Configuration */
	os_reg32_t		pwr_esrocfg;		/* 0x78 ESR Reset Output Configuration */
	os_reg32_t		pwr_syscon;			/* 0x7C System Control */

#if (OS_CPU == OS_TC23XL) || (OS_CPU == OS_TC22XL)
	os_reg32_t		pwr_ccucon6;		/* 0x80 CCU Control 6 */
	os_reg32_t		reserved5[2];		/* 0x84 */
	os_reg32_t		pwr_ccucon9;		/* 0x8C CCU Control 9 */
	os_reg32_t		reserved5_1[3];		/* 0x90 */
#else
	os_reg32_t		reserved5[7];		/* 0x80 */
#endif

	os_reg32_t		pwr_pdr;			/* 0x9C Pad Driver Mode */
	os_reg32_t		pwr_iocr;			/* 0xA0 Input/Output Control */
	os_reg32_t		pwr_out;			/* 0xA4	Output */
	os_reg32_t		pwr_omr;			/* 0xA8 Output Modification */
	os_reg32_t		pwr_in;				/* 0xAC	Input */
	os_reg32_t		pwr_evrstat;		/* 0xB0	EVR Status */
	os_reg32_t		reserved6[1];		/* 0xB4 */
	os_reg32_t		pwr_evr13con;		/* 0xB8	EVR13 Control */

#if (OS_CPU == OS_TC23XL) || (OS_CPU == OS_TC22XL)
	os_reg32_t		reserved18;			/* 0xBC */
#else
	os_reg32_t		pwr_evr33con;		/* 0xBC	EVR33 Control */
#endif

	os_reg32_t		pwr_ststat;			/* 0xC0 Start-up Status */

#if (OS_CPU == OS_TC23XL) || (OS_CPU == OS_TC22XL)
	os_reg32_t		reserved17;			/* 0xC4 */
#else
	os_reg32_t		pwr_stcon;			/* 0xC4	Start-up Configuration */
#endif

	os_reg32_t		pwr_pmswcr0;		/* 0xC8	Standby and Wakeup Control 0 */
	os_reg32_t		pwr_pmswstat;		/* 0xCC	Standby and Wakeup Status */
	os_reg32_t		pwr_pmswstatclr;	/* 0xD0 Standby and Wakeup Status Clear */

#if (OS_CPU == OS_TC23XL) || (OS_CPU == OS_TC22XL)
	os_reg32_t		pwr_pmcsr[1];		/* 0xD4 CPU0 Power Management Control and Status */
	os_reg32_t		reserved19[2];		/* 0xD8 */
#else
	os_reg32_t		pwr_pmcsr[3];		/* 0xD4 CPU0, 1 and 2 Power Management Control and Status */
#endif

	os_reg32_t		pwr_dtsstat;		/* 0xE0	Die Temperature Sensor Status */
	os_reg32_t		pwr_dtscon;			/* 0xE4 Die Temperature Sensor Control */
	os_reg32_t		pwr_pmswcr1;		/* 0xE8 Standby and Wakeup Control 1 */
	os_reg32_t		reserved7[1];		/* 0xEC */
	os_tricore_aurix_wdt_t pwr_swdt;	/* 0xF0 Safety WDT */
	os_reg32_t		pwr_emsr;			/* 0xFC Emergency Stop */

#if (OS_CPU == OS_TC23XL) || (OS_CPU == OS_TC22XL)
	os_tricore_aurix_wdt_t pwr_wdt[1];	/* 0x100 CPU WDT module */
	os_reg32_t		reserved20[6];		/* 0x10C */
#else
	os_tricore_aurix_wdt_t pwr_wdt[3];	/* 0x100 3 CPU WDT modules */
#endif

	os_reg32_t		pwr_trapstat;		/* 0x124 Trap Status */
	os_reg32_t		pwr_trapset;		/* 0x128 Trap Set */
	os_reg32_t		pwr_trapclr;		/* 0x12C Trap Clear */
	os_reg32_t		pwr_trapdis;		/* 0x130 Trap Disable */
	os_reg32_t		pwr_lclcon[2];		/* 0x134 CPU0 and 1 Lockstep Control */
	os_reg32_t		pwr_lcltest;		/* 0x13C Lockstep Test */
	os_reg32_t		pwr_chipid;			/* 0x140 Chip Identification */
	os_reg32_t		pwr_manid;			/* 0x144 Manufacture Identification */
	os_reg32_t		reserved8[7];		/* 0x148 */

#if (OS_CPU == OS_TC23XL) || (OS_CPU == OS_TC22XL)
	os_reg32_t		pwr_lbistctrl[3];	/* 0x164 Logic BIST Control 0, 1 and 2 */
	os_reg32_t		reserved9[9];		/* 0x170 */
#else
	os_reg32_t		pwr_lbistctrl[2];	/* 0x164 Logic BIST Control 0 and 1 */
	os_reg32_t		reserved9[10];		/* 0x16C */
#endif

#if (OS_CPU == OS_TC23XL) || (OS_CPU == OS_TC22XL)
	os_reg32_t		reserved16;			/* 0x194 */
#else
	os_reg32_t		pwr_ccuconsm;		/* 0x194 CCU Control Security Monitor */
#endif

#if (OS_CPU == OS_TC23XL) || (OS_CPU == OS_TC22XL)
	os_reg32_t		reserved10[1];		/* 0x198 */
	os_reg32_t		pwr_evradcstat;		/* 0x19C EVR ADC Status */
#else
	os_reg32_t		reserved10[2];		/* 0x198 */
#endif

	os_reg32_t		pwr_evruvmon;		/* 0x1A0 EVR Undervoltage Monitor */
	os_reg32_t		pwr_evrovmon;		/* 0x1A4 EVR Overvoltage Monitor */
	os_reg32_t		pwr_evrmonctrl;		/* 0x1A8 EVR Monitor Control */
	os_reg32_t		reserved11[1];		/* 0x1AC */
	os_reg32_t		pwr_evrsdctrl1;		/* 0x1B0 EVR SD Control 1 */

#if (OS_CPU == OS_TC23XL) || (OS_CPU == OS_TC22XL)
	os_reg32_t		pwr_evrsdctrl2;		/* 0x1B4 EVR SD Control 2 */
	os_reg32_t		pwr_evrsdctrl3;		/* 0x1B8 EVR SD Control 3 */
	os_reg32_t		reserved12[2];		/* 0x1BC */
	os_reg32_t		pwr_evrsdcoeff2;	/* 0x1C4 EVR SD Coefficient 2 */
	os_reg32_t		reserved12_1[5];	/* 0x1C8 */
	os_reg32_t		pwr_pmswutcnt;		/* 0x1DC Standby WUT Count */
#else
	os_reg32_t		reserved12[11];		/* 0x1B4 */
#endif

	os_reg32_t		pwr_ovcenable;		/* 0x1E0 Overlay Enable */
	os_reg32_t		pwr_ovccon;			/* 0x1E4 Overlay Control */
	os_reg32_t		reserved13[10];		/* 0x1E8 */
	os_reg32_t		pwr_eicr[4];		/* 0x210 External Input Channel 0, 1, 2 and 3 */
	os_reg32_t		pwr_eifr;			/* 0x220 External Input Flag */
	os_reg32_t		pwr_fmr;			/* 0x224 Flag Modification */
	os_reg32_t		pwr_pdrr;			/* 0x228 Pattern Detection Result */
	os_reg32_t		pwr_igcr[4];		/* 0x22C Interrupt Gating 0, 1, 2 and 3 */
	os_reg32_t		reserved14[1];		/* 0x23C */
	os_reg32_t		pwr_dtslim;			/* 0x240 Die Temperature Sensor Limit */

#if (OS_CPU == OS_TC23XL) || (OS_CPU == OS_TC22XL)
	os_reg32_t		reserved15[47];		/* 0x244 */
	os_reg32_t		pwr_pmswcr3;		/* 0x300 Standby and Wakeup Control 3 */
	os_reg32_t		reserved15_1[61];	/* 0x304 */
#else
	os_reg32_t		reserved15[109];	/* 0x244 */
#endif

	os_reg32_t		pwr_accen1;			/* 0x3F8 Access Enable 1 */
	os_reg32_t		pwr_accen0;			/* 0x3FC Access Enable 0 */

#else

#error "No PWR support for the selected processor."

#endif
} os_tricorepwr_t;

#endif

#if (OS_CPU == OS_TC1920)
#define OS_PWR_SRC_NtoI(n)	(4u-(n))
#endif

#ifdef OS_ASM

#if (OS_CPU == OS_TC1796) || (OS_CPU == OS_TC1766)
/*	The PWR registers sometimes need to be accessed from assembler modules. */
#define OS_PWRREG(x)	(OS_PWR_BASE+(x))
#define OS_PWRCLC		OS_PWRREG(0x00)
#define	OS_PWRID		OS_PWRREG(0x08)
#define OS_RSTREQ		OS_PWRREG(0x10)
#define OS_RSTSR		OS_PWRREG(0x14)
#define OS_WDTCON0		OS_PWRREG(0x20)
#define OS_WDTCON1		OS_PWRREG(0x24)
#define OS_WDTSR		OS_PWRREG(0x28)
#define OS_NMISR		OS_PWRREG(0x2c)
#define OS_PMCON		OS_PWRREG(0x30)
#define OS_PMCSR		OS_PWRREG(0x34)
#define OS_PLLCLC		OS_PWRREG(0x40)
#define OS_ECKCLC		OS_PWRREG(0x44)
#define OS_ICUCLC		OS_PWRREG(0x48)
#endif

#endif

/* PLL Clock Control Register */
#if ( (OS_CPU == OS_TC10GP)	|| \
	  (OS_CPU == OS_TC11IB)	|| \
	  (OS_CPU == OS_TC1775)	|| \
	  (OS_CPU == OS_PXD4260) )

#define OS_PLL_LOCK			OS_U(0x00000100)		/* PLL locked */
#define OS_PLL_VCOSEL		OS_U(0x00000200)		/* VCO select */
#define OS_PLL_VCOBYP		OS_U(0x00000400)		/* VCO bypass */
#define OS_PLL_N			OS_U(0x00007000)		/* N-factor */
#define OS_PLL_BYPASS		OS_U(0x00008000)		/* PLL bypass */
#define OS_PLL_K			OS_U(0x00070000)		/* K-divider */

#define OS_PLL_KfromInt(c)	((c) << 16u)
#define OS_PLL_KtoInt(c)	(((c) & OS_PLL_K) >> 16u)
#define OS_PLL_NtoInt(c)	(((c) & OS_PLL_N) >> 12u)
#define OS_PLL_PtoInt(c)	1u

#define OS_PLL_Kdivider(i)	(((i) == 1u) ? 16u : (((i) > 4u) ? ((i) + 3u) : ((i) + 2u)))
#define OS_PLL_Nfactor(i)	((i) + 8u)
#define OS_PLL_Pdivider(i)	1u

#elif (OS_CPU == OS_TC1920)

#define OS_PLL_LOCK			OS_U(0x00000001)	/* Lock status */
#define OS_PLL_PLL2EN		OS_U(0x00000002)	/* PLL2 output enable */
#define OS_PLL_PLL2SEL		OS_U(0x00000004)	/* PLL2 clock ratio */
#define OS_PLL_VCOSLEEP		OS_U(0x00000008)	/* VCO sleeps when bypassed */
#define OS_PLL_BYPASS		OS_U(0x00000010)	/* PLL Bypass status */
#define OS_PLL_VCOBYP		OS_U(0x00000020)	/* VCO Bypass status */
#define OS_PLL_VCOSEL		OS_U(0x000000c0)	/* VCO range select */
#define OS_PLL_K			OS_U(0x00000f00)
#define OS_PLL_P			OS_U(0x0000e000)
#define OS_PLL_N			OS_U(0x003f0000)

#define OS_PLL_KfromInt(c)	((c) << 8u)
#define OS_PLL_NfromInt(c)	((c) << 16u)
#define OS_PLL_PfromInt(c)	((c) << 13u)
#define OS_PLL_KtoInt(c)	(((c) & OS_PLL_K) >> 8u)
#define OS_PLL_NtoInt(c)	(((c) & OS_PLL_N) >> 16u)
#define OS_PLL_PtoInt(c)	(((c) & OS_PLL_P) >> 13u)

#define OS_PLL_Kdivider(i)	((i)+1u)
#define OS_PLL_Nfactor(i)	((i)+1u)
#define OS_PLL_Pdivider(i)	((i)+1u)

#elif (OS_CPU == OS_TC1796) || (OS_CPU == OS_TC1766)

#define OS_PLL_LOCK			OS_U(0x00000001)	/* Lock status */
#define OS_PLL_RESLD		OS_U(0x00000002)	/* Reset lock detection */
#define OS_PLL_SYSFS		OS_U(0x00000004)	/* SysClk = CpuClk (0 ==> SysClk = CpuClk/2) */
#define OS_PLL_VCOBYP		OS_U(0x00000020)
#define OS_PLL_VCOSEL		OS_U(0x000000c0)
#define OS_PLL_K			OS_U(0x00000f00)
#define OS_PLL_P			OS_U(0x0000e000)
#define OS_PLL_N			OS_U(0x007f0000)
#define OS_PLL_OSCDISC		OS_U(0x01000000)
#define OS_PLL_BYPPIN		OS_U(0x20000000)

#define OS_PLL_KfromInt(c)	((c) << 8u)
#define OS_PLL_NfromInt(c)	((c) << 16u)
#define OS_PLL_PfromInt(c)	((c) << 13u)
#define OS_PLL_KtoInt(c)	(((c) & OS_PLL_K) >> 8u)
#define OS_PLL_NtoInt(c)	(((c) & OS_PLL_N) >> 16u)
#define OS_PLL_PtoInt(c)	(((c) & OS_PLL_P) >> 13u)

#define OS_PLL_Kdivider(i)	((i)+1u)
#define OS_PLL_Nfactor(i)	((i)+1u)
#define OS_PLL_Pdivider(i)	((i)+1u)

#elif (OS_CPU == OS_TC1797) \
	|| (OS_CPU == OS_TC1767) \
	|| (OS_CPU == OS_TC1387) \
	|| (OS_CPU == OS_TC1782) \
	|| (OS_CPU == OS_TC1798) \
	|| (OS_CPU == OS_TC1791) \
	|| (OS_CPU == OS_TC1793) \
	|| (OS_CPU == OS_TC277)  \
	|| (OS_CPU == OS_TC23XL) \
	|| (OS_CPU == OS_TC22XL)

/* pwr_pll_con[0]: Bits for PLL configuration register 0 */
#define OS_PLL_RESLD		OS_U(0x00040000)		/* Reset lock detection */
#define OS_PLL_PLLPWD		OS_U(0x00010000)		/* Power saving mode (doc incorrect?) */
#define OS_PLL_OSCDISCDIS	OS_U(0x00000040)		/* Clear FINDIS on loss of lock */
#define OS_PLL_CLRFINDIS	OS_U(0x00000020)		/* Write 1 here to CLEAR FINDIS! */
#define OS_PLL_SETFINDIS	OS_U(0x00000010)		/* Disconnect OSC from VCO. */
#define OS_PLL_VCOBYP		OS_U(0x00000001)		/* VCO bypass */

/* pwr_pllstat: Bits for PLL status register */
#define OS_VCO_LOCK			OS_U(0x00000004)		/* PLL VCO Lock Status */

#define OS_PLL_SET_P(p)		( ((p)-1u) << 24u )		/* PLLCON0 : 1 <= p <= 16 */
#define OS_PLL_SET_N(n)		( ((n)-1u) << 9u )		/* PLLCON0 : 1 <= n <= 128 */
#define OS_PLL_SET_K1(k1)	( ((k1)-1u) << 16u)		/* PLLCON1 : 1 <= k1 <= 128 */
#define OS_PLL_SET_K2(k2)	((k2)-1u)				/* PLLCON1 : 1 <= k2 <= 128 */
#if (OS_CPU == OS_TC23XL) || (OS_CPU == OS_TC22XL)
#define OS_PLL_SET_K3(k3)	(((k3)-1u) << 8u)		/* PLLCON1 : 1 <= k3 <= 128 */
#endif

/* pwr_ccucon: Bits and masks for clock control register. */
#if (OS_CPU == OS_TC277) || (OS_CPU == OS_TC23XL) || (OS_CPU == OS_TC22XL)
#define OS_CCUCON_LCK_LOCKED					(1u << 31u) /* Register is locked and can't be updated anymore. */
#define OS_CCUCON_UP							(1u << 30u) /* Update Request */
#define OS_CCUCON0_CLKSEL_MASK					(3u << 28u) /* Mask to select all bits in the CLKSEL field. */
#define OS_CCUCON0_CLKSEL_BACKUP_CLK_AS_FSOURCE 0			/* Use backup clock as f_source. */
#define OS_CCUCON0_CLKSEL_PLL_CLK_AS_FSOURCE	(1u << 28u) /* Use PLL output as f_source. */
#define OS_CCUCON1_INSEL_MASK					(3u << 28u) /* Mask to select all bits in the INSEL field. */
#define OS_CCUCON1_INSEL_BACKUP_AS_CLK_SOURCE	0			/* Use backup clock as clock source. */
#define OS_CCUCON1_INSEL_FOSC_AS_CLK_SOURCE     (1u << 28u) /* Use f_osc as clock source. */
#else
#define OS_CCUCON0_LCK_LOCKED					(1u << 31u) /* Register is locked and cannot be updated. */
#endif /* S_CPU == OS_TC277 */

#elif (OS_CPU == OS_TC27XFPGA) || (OS_CPU == OS_TC2D5)


#else

#error "Choose the correct PLL implementation for your CPU"

#endif

/* External / ICU Clock Control Register */
#define OS_DISR				OS_U(0x00000001)		/* Disable request */
#define OS_DISS				OS_U(0x00000002)		/* Disable status */
#define OS_SUSPEN			OS_U(0x00000004)		/* Suspend enable */
#define OS_EXRDIS			OS_U(0x00000008)		/* External request disable */
#define OS_SBWE				OS_U(0x00000010)		/* Suspend write enable */
#define OS_DIVCLK_MSK		OS_U(0x00FF0000)		/* Clock divider */

#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
/* WDT constants for Aurix are defined in Os_TRICORE_aurix_wdt.h
*/
#else

/* Watchdog Timer Control Register 0 */
#define OS_ENDINIT			OS_U(0x00000001)		/* End of initialisation control */
#define OS_WDTLCK			OS_U(0x00000002)		/* Lock */
#define OS_WDTHPW0_MSK		OS_U(0x0000000C)		/* Hardware password 0 */
#define OS_WDTHPW1_MSK		OS_U(0x000000F0)		/* Hardware password 1 */
#define OS_WDTPW_MSK		OS_U(0x0000FF00)		/* User definable password */
#define OS_WDTREL_MSK		OS_U(0xFFFF0000)		/* Reload value for watchdog timer */

/* Watchdog Timer Control Register 1 */
#define OS_WDTIR			OS_U(0x00000004)		/* Input frequency request control */
#define OS_WDTDR			OS_U(0x00000008)		/* Disable request control */
#define OS_SCKCLR_MSK		OS_U(0x00000700)		/* Self check clear request */

/* Watchdog Timer Status Register */
#define OS_WDTAE			OS_U(0x00000001)		/* Access error */
#define OS_WDTOE			OS_U(0x00000002)		/* Overflow error */
#define OS_WDTIS			OS_U(0x00000004)		/* Input clock status */
#define OS_WDTDS			OS_U(0x00000008)		/* Enable/disable */
#define OS_WDTTO			OS_U(0x00000010)		/* Time out period */
#define OS_WDTPR			OS_U(0x00000020)		/* Reset prewarning */
#define OS_SCKERR_MSK		OS_U(0x0000FF00)		/* Self check error bits */
#define OS_WDTTIM_MSK		OS_U(0xFFFF0000)		/* Contents of watchdog timer */

#endif

/* Reset Status Register */
#define OS_RSSTM			OS_U(0x00000001)		/* Reset status system timer */
#define OS_RSDBG			OS_U(0x00000002)		/* Reset status debug unit */
#define OS_RSEXT			OS_U(0x00000004)		/* Reset status for external devices */
#define OS_HWCFG_MSK		OS_U(0x00070000)		/* Status of CFG[2..0] */
#define OS_HW_OCDS_E		OS_U(0x00080000)		/* Status of the OCDS_E pin */
#define OS_HW_BRK_IN		OS_U(0x00100000)		/* Status of the BRK_IN pin */
#define OS_PWORST			OS_U(0x08000000)		/* Power-on reset indicator */
#define OS_HDRST			OS_U(0x10000000)		/* Hardware reset indicator */
#define OS_SFTRST			OS_U(0x20000000)		/* Software reset indicator */
#define OS_WDTRST			OS_U(0x40000000)		/* Watchdog reset indicator */
#define OS_PWDRST			OS_U(0x80000000)		/* Power-down/Wake-up indicator */

/* Reset Request Register */
#define OS_RRSTM			OS_U(0x00000001)		/* System timer reset request */
#define OS_RRDBG			OS_U(0x00000002)		/* Debug unit reset request */
#define OS_RREXT			OS_U(0x00000004)		/* External devices reset request */
#define OS_SWCFG_MSK		OS_U(0x00070000)		/* Software boot configuration */
#define OS_SW_OCDS_E		OS_U(0x00080000)		/* Software OCDS_E signal boot value */
#define OS_SW_BRK_IN		OS_U(0x00100000)		/* Software BRK_IN signal boot value */
#define OS_SWBOOT			OS_U(0x01000000)		/* Software boot configuration */

/* NMI Status Register */
#define OS_NMI_EXT			OS_U(0x00000001)		/* External NMI */
#define OS_NMI_PLL			OS_U(0x00000002)		/* PLL loss of lock */
#define OS_NMI_WDT			OS_U(0x00000004)		/* Watchdog time-out */

#ifndef OS_ASM

os_uint32_t OS_PllCpuFreq(void);
os_uint32_t OS_PllModuleFreq(void);
void OS_WriteEndinit(os_uint32_t);

#define OS_ClearEndinit(s) \
	do {						\
		(s) = OS_MFCR(OS_ICR);	\
		OS_DISABLE();			\
		OS_WriteEndinit(0);		\
	} while (0)

#define OS_SetEndinit(s) \
	do {						\
		OS_WriteEndinit(1);		\
		OS_MTCR(OS_ICR, s);		\
	} while (0)

#endif

#ifdef __cplusplus
}
#endif

#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
