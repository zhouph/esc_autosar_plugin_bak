/* Os_tool_TRICORE_gnu.h - Tricore macros for gcc
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_tool_TRICORE_gnu.h 18545 2014-07-30 05:29:31Z mabr2343 $
*/

/*	This file contains macros (C and assembler) for use with Tricore
 *	and the gcc compiler.
*/

#ifndef __OS_TOOL_TRICORE_GNU_H
#define __OS_TOOL_TRICORE_GNU_H

/* The Gnu compiler implements a read of a volatile variable correctly
*/
#define OS_ReadbackVolatile(x)	(x)

#ifndef OS_TRICORE_BF_00
#error "Include error: Silicon bug workarounds have not been defined"
#endif

#if OS_TRICORE_BF_05 && OS_TRICORE_BF_11
#error "BF_05 and BF_11 together are incompatible with the current workaround code."
#endif

/* Including the workaround for BF_11 is optional because it adds CUP cycles to every interrupt.
 *
 * It is included by default because of the danger, but a system designed can exclude
 * it if it is considered safe.
*/
#ifndef OS_INCLUDE_BF11_WORKAROUND
#define OS_INCLUDE_BF11_WORKAROUND	1
#endif

/*	Macro expansion of stringified absolute values is strange (see
 *	gcc documentation cpp.html). This is a workaround...
*/
#define OS__XSTR(s) OS__STR(s)
#define OS__STR(s) #s

/* OS_PARAM_UNUSED(p) - mark parameter p as unused parameter (or variable).
 * Usage: Function-like.
 */
#define OS_PARAM_UNUSED(p) ((void)(p))

#ifdef OS_ASM
/* CHECK: SAVE
 * CHECK: RULE 402 OFF - These macros are for the assembler, not for C
*/

/*	The Gnu assembler expects registers to be called %something
*/
#define a0		%a0
#define a1		%a1
#define a2		%a2
#define a3		%a3
#define a4		%a4
#define a5		%a5
#define a6		%a6
#define a7		%a7
#define a8		%a8
#define a9		%a9
#define a10		%a10
#define a11		%a11
#define a12		%a12
#define a13		%a13
#define a14		%a14
#define a15		%a15
#define d0		%d0
#define d1		%d1
#define d2		%d2
#define d3		%d3
#define d4		%d4
#define d5		%d5
#define d6		%d6
#define d7		%d7
#define d8		%d8
#define d9		%d9
#define d10		%d10
#define d11		%d11
#define d12		%d12
#define d13		%d13
#define d14		%d14
#define d15		%d15
#define e0		%e0
#define e2		%e2
#define e4		%e4
#define e6		%e6
#define e8		%e8
#define e10		%e10
#define e12		%e12
#define e14		%e14
#define sp		%sp

/*	The Gnu assembler does not expect any special prefix for
 *	immediate operands.
*/
#define _IMM(p,x)		x

/*	The Gnu assembler uses EABI syntax for the bit specification in jz.t etc.
*/
#define _REGBIT(r,b)	r, b

/*	The Gnu assembler uses hi:x and lo:x to extract the
 *	high and low words from a literal.
*/
#define _hiword(x)		 hi:x
#define _loword(x)		 lo:x

/*	Gnu assembler uses .type, .globl and .extern as normal.
*/
#define _GTYPE(s,t)	.type	s,t
#define _TTYPE(s,t)	s:
#define _GLOBAL		.globl

/* The .extern is ignored by as and has no direct counterpart. Therefore
 * we simply start a comment for this line. */
#define _EXTERN		#


/*	Gnu assembler has standard .text and .data sections
*/
#define	_TEXT	.text
#define _DATA	.data

/*	The Gnu assembler uses "power of 2" notation for alignment
*/
#define _align(n,p)		.align p

/* The Gnu assembler has the .hword directive for 16-bit words */
#define _HWORD .hword

/* These macros implement the *16.* and *32.* opcodes
 * that are provided by some assemblers. The Gnu assembler
 * uses a .code16 directive instead.
 * WARNING: ld16 and st16 might be using a bug in gas.
 *
 * CHECK: NOPARSE
*/
	.macro	ret16
		.code16
		ret
	.endm

	.macro	mov16	p1,p2
		.code16
		mov		\p1,\p2
	.endm

	.macro	jeq16	p1,p2,p3
		.code16
		jeq		\p1,\p2,\p3
	.endm

	.macro	and16	p1,p2
		.code16
		and		\p1,\p2
	.endm

	.macro	or16	p1,p2
		.code16
		or	\p1,\p2
	.endm

	.macro	add16	p1,p2
		.code16
		add		\p1,\p2
	.endm

	.macro	bisr16	p1
		.code16
		bisr	\p1
	.endm

	.macro	bisr32	p1
		.code32
		bisr	\p1
	.endm

	.macro	nop16
		.code16
		nop
	.endm

	.macro	nop32
		.code32
		nop
	.endm

	.macro	ld16	p1,p2,p3
		.code16
		ld\p1	\p2,\p3
	.endm

	.macro	st16	p1,p2,p3
		.code16
		st\p1	\p2,\p3
	.endm

/*	Now some native assembler macros to redefine some op-codes
 *	safely. The macros are (mostly) named like the opcodes,
 *	with _ prepended. Simply converting to uppercase has no
 *	effect - opcodes are case-insensitive.
 *	The macro _mfcr simply maps to mfcr at the moment, but
 *	should be used in case there is ever the need to synchronise.
*/
	.macro	_dsync
		dsync
#if OS_TRICORE_BF_01==1
		nop
		nop
#endif
	.endm

	.macro	_mtcr	creg,reg
		_dsync
		mtcr	\creg,\reg
		isync
	.endm

	.macro	_mfcr	reg,creg
		mfcr	\reg,\creg
	.endm

	.macro	_loop	areg,label
#if OS_TRICORE_BF_02==1
		isync
#endif
		loop	\areg,\label
	.endm

/*	The _entry macro goes at the top of every function and
 *	ISR.
*/
	.macro	_entry
#if OS_TRICORE_BF_05==1
		_dsync
#endif
	.endm

/*	The _short macro goes into very short (no instructions, or
 *	only a single integer instruction) subroutines, to pad with
 *	a NOP of necessary
*/
	.macro	_short
#if OS_TRICORE_BF_08==1
#if OS_TRICORE_BF_05==1
		nop
#endif
#endif
	.endm

/*	The _jinop macro goes between a ld.a and a ji instruction
 *	to pad with a nop if necessary.
*/
	.macro	_jinop
#if OS_TRICORE_BF_03==1
		nop
#endif
	.endm

/*	The _dvloop macro goes between a dvstep and a loop
 *	to pad with a nop if necessary.
*/
	.macro	_dvloop
#if OS_TRICORE_BF_09==1
		nop
#endif
	.endm

/*	The GenIntVector macro generates an interrupt vector with the
 *	specified symbol, BISR-level, isr-id and entry function.
*/
	.macro	GenIntVector	name,bisrlvl,isrid,entry,exit
	.align	5
	.globl	\name
\name:
#if OS_TRICORE_BF_11 && OS_INCLUDE_BF11_WORKAROUND
	_mfcr	d15,OS_PCXI
	jnz.t	d15,OS_PCXI_PIE_BIT,\name.c
	mov		d15,\isrid
	j		OS_Bf11Workaround
\name.c:
#else
	_entry
#endif
	bisr	\bisrlvl
	mov		d4,\isrid
	call	\entry
	j		\exit
	.endm

	/*	The DirectVector macro generates an interrupt vector which jumps directly to the
	 *  entry. The entry needs to use the interrupt keyword.
	 */
	.macro	DirectVector	name,entry
		.align	5
		.globl	\name
\name:
		j		\entry
	.endm

/* CHECK: PARSE */
/* CHECK: RESTORE */
#else

#ifdef __cplusplus
extern "C" {
#endif

#define OS_DEBUG_BREAK()	__asm__ volatile ("debug")
#define OS_ISYNC()			__asm__ volatile ("isync")
#define OS_DSYNC_UNSAFE()	__asm__ volatile ("dsync")

#if OS_TRICORE_BF_01==1
#define OS_DSYNC() \
	__asm__ volatile ("dsync");	\
		__asm__ volatile ("nop");	\
	__asm__ volatile ("nop")
#else
#define OS_DSYNC()	__asm__ volatile ("dsync")
#endif

#define OS_NOP()	__asm__ volatile ("nop" : : : "memory")

#define OS_MTCR(csfr, val) \
	do {																\
		register int mtcrTmp = (val);									\
		OS_DSYNC();														\
		__asm__ volatile ("mtcr "OS__XSTR(csfr)",%0" : : "d" (mtcrTmp));\
		__asm__ volatile ("isync");										\
	} while(0)


/* Now it gets ugly. We want to avoid braces in expressions, so we cannot
 * use gcc's _mfcr intrinsic, which also uses braces in the expression to
 * provide the return value. This prevents us from using a macro.
 *
 * Since the CSFR ID needs to be encoded into the mfcr instruction, we cannot
 * use an inline function either.
 *
 * The ugly solution: Define one inline function per CSFR, and define the
 * OS_MFCR macro such that it uses the provided CSFR ID (e.g. OS_PSW) to call
 * the corresponding inline function. This of course only works when the macro
 * is used with the OS_xxx CSFR constants, but that should be acceptable.
 */
#define OS_MFCR_FCTEMPLATE(CSFRID) \
static __inline__ os_uint32_t OS_MFCR_ ##CSFRID(void) __attribute__((always_inline)); \
static __inline__ os_uint32_t OS_MFCR_ ##CSFRID(void) \
{ \
	register os_uint32_t mfcrRes; \
	__asm__ volatile ("mfcr %0,%1" : "=d" (mfcrRes) : "i" (CSFRID)); \
	return mfcrRes; \
}

#define OS_MFCR(CSFRID) OS_MFCR_ ##CSFRID()

#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
OS_MFCR_FCTEMPLATE(OS_DPR0_L)
OS_MFCR_FCTEMPLATE(OS_DPR0_U)
OS_MFCR_FCTEMPLATE(OS_DPR1_L)
OS_MFCR_FCTEMPLATE(OS_DPR1_U)
OS_MFCR_FCTEMPLATE(OS_DPR2_L)
OS_MFCR_FCTEMPLATE(OS_DPR2_U)
OS_MFCR_FCTEMPLATE(OS_DPR3_L)
OS_MFCR_FCTEMPLATE(OS_DPR3_U)
OS_MFCR_FCTEMPLATE(OS_DPR4_L)
OS_MFCR_FCTEMPLATE(OS_DPR4_U)
OS_MFCR_FCTEMPLATE(OS_CPR0_L)
OS_MFCR_FCTEMPLATE(OS_CPR0_U)
OS_MFCR_FCTEMPLATE(OS_CPR1_L)
OS_MFCR_FCTEMPLATE(OS_CPR1_U)
OS_MFCR_FCTEMPLATE(OS_DPRE_0)
OS_MFCR_FCTEMPLATE(OS_DPRE_1)
OS_MFCR_FCTEMPLATE(OS_DPWE_0)
OS_MFCR_FCTEMPLATE(OS_DPWE_1)
OS_MFCR_FCTEMPLATE(OS_CPXE_0)
OS_MFCR_FCTEMPLATE(OS_CPXE_1)

OS_MFCR_FCTEMPLATE(OS_CORE_ID)
#else
OS_MFCR_FCTEMPLATE(OS_DPR0_0L)
OS_MFCR_FCTEMPLATE(OS_DPR0_0U)
OS_MFCR_FCTEMPLATE(OS_DPR0_1L)
OS_MFCR_FCTEMPLATE(OS_DPR0_1U)
OS_MFCR_FCTEMPLATE(OS_DPR0_2L)
OS_MFCR_FCTEMPLATE(OS_DPR0_2U)
OS_MFCR_FCTEMPLATE(OS_DPR0_3L)
OS_MFCR_FCTEMPLATE(OS_DPR0_3U)
OS_MFCR_FCTEMPLATE(OS_DPR1_0L)
OS_MFCR_FCTEMPLATE(OS_DPR1_0U)
OS_MFCR_FCTEMPLATE(OS_DPR1_1L)
OS_MFCR_FCTEMPLATE(OS_DPR1_1U)
OS_MFCR_FCTEMPLATE(OS_DPR1_2L)
OS_MFCR_FCTEMPLATE(OS_DPR1_2U)
OS_MFCR_FCTEMPLATE(OS_DPR1_3L)
OS_MFCR_FCTEMPLATE(OS_DPR1_3U)
OS_MFCR_FCTEMPLATE(OS_CPR0_0L)
OS_MFCR_FCTEMPLATE(OS_CPR0_0U)
OS_MFCR_FCTEMPLATE(OS_CPR0_1L)
OS_MFCR_FCTEMPLATE(OS_CPR0_1U)
OS_MFCR_FCTEMPLATE(OS_CPR1_0L)
OS_MFCR_FCTEMPLATE(OS_CPR1_0U)
OS_MFCR_FCTEMPLATE(OS_CPR1_1L)
OS_MFCR_FCTEMPLATE(OS_CPR1_1U)
OS_MFCR_FCTEMPLATE(OS_DPM0)
OS_MFCR_FCTEMPLATE(OS_DPM1)
OS_MFCR_FCTEMPLATE(OS_CPM0)
OS_MFCR_FCTEMPLATE(OS_CPM1)
#endif

OS_MFCR_FCTEMPLATE(OS_DBGSR)
OS_MFCR_FCTEMPLATE(OS_GPRWB)
OS_MFCR_FCTEMPLATE(OS_EXEVT)
OS_MFCR_FCTEMPLATE(OS_CREVT)
OS_MFCR_FCTEMPLATE(OS_SWEVT)
OS_MFCR_FCTEMPLATE(OS_TR0EVT)
OS_MFCR_FCTEMPLATE(OS_TR1EVT)

OS_MFCR_FCTEMPLATE(OS_DMS)
OS_MFCR_FCTEMPLATE(OS_DCX)

OS_MFCR_FCTEMPLATE(OS_PCXI)
OS_MFCR_FCTEMPLATE(OS_PSW)
OS_MFCR_FCTEMPLATE(OS_PC)
OS_MFCR_FCTEMPLATE(OS_DBITEN)
OS_MFCR_FCTEMPLATE(OS_SYSCON)
OS_MFCR_FCTEMPLATE(OS_BIV)
OS_MFCR_FCTEMPLATE(OS_BTV)
OS_MFCR_FCTEMPLATE(OS_ISP)
OS_MFCR_FCTEMPLATE(OS_ICR)
OS_MFCR_FCTEMPLATE(OS_FCX)
OS_MFCR_FCTEMPLATE(OS_LCX)

static __inline__ void *OS_MFSP(void) __attribute__((always_inline));
static __inline__ void *OS_MFSP(void)
{
	register void *mfspRes;
	__asm__ volatile ("mov.aa %0,%%sp" : "=a" (mfspRes));
	return mfspRes;
}

static __inline__ void* OS_MFRA(void) __attribute__((always_inline));
static __inline__ void* OS_MFRA(void)
{
	register void *mfraRes;
	__asm__ volatile ("mov.aa %0,%%a11" : "=a" (mfraRes));
	return mfraRes;
}

static __inline__ void OS_MTSP(void* val) __attribute__((always_inline));
static __inline__ void OS_MTSP(void* val)
{
	__asm__ volatile ("mov.aa %%sp,%0" : : "a" (val));
}

static __inline__ void OS_MTRA(void* val) __attribute__((always_inline));
static __inline__ void OS_MTRA(void* val)
{
	__asm__ volatile ("mov.aa %%a11,%0" : : "a" (val));
}


static __inline__ void OS_MTD4(os_int_t val) __attribute__((always_inline));
static __inline__ void OS_MTD4(os_int_t val)
{
	__asm__ volatile ("mov    %%d4,%0" : : "d" (val));
}

#define OS_DISABLE()	__asm__ volatile ("disable":::"memory")
#define OS_ENABLE()		__asm__ volatile ("enable":::"memory")

/* Macro to restore lower context
*/
#define OS_RSLCX()	__asm__ volatile ("rslcx")

/* Macro to return from a JL instruction. We call it RFJL, but it is
 * really only a JI A11.
*/
#define OS_RFJL()	__asm__ volatile ("ji %a11")

/* Macro to return from interrupt.
*/
#define OS_RFE()	__asm__ volatile ("rfe")

/* Macro to count leading zeros in a word
*/
static __inline__ os_int_t OS_CLZ(os_int_t wurd) __attribute__((always_inline));
static __inline__ os_int_t OS_CLZ(os_int_t wurd)
{
	os_int_t OS_CLZ_nZeros;
	__asm__ volatile ("clz %0,%1" : "=d" (OS_CLZ_nZeros) : "d" (wurd));
	return OS_CLZ_nZeros;
}

/* Cache operations: flush, invalidate and flush+invalidate
*/
#define OS_ArchCacheFlushLine(p)			__asm__ volatile ("cachea.w [%0]0" : : "a"(p) : "memory")
#define OS_ArchCacheInvalidateLine(p)		__asm__ volatile ("cachea.i [%0]0" : : "a"(p) : "memory")
#define OS_ArchCacheFlushInvalidateLine(p)	__asm__ volatile ("cachea.wi [%0]0" : : "a"(p) : "memory")

/* TRICORE_IntEnable() sets the ICR to the "enable" state (IE=1, CCPN=0)
 * and returns the previous state.
*/
static __inline__ os_uint32_t OS_IntEnable(void) __attribute__((always_inline));
static __inline__ os_uint32_t OS_IntEnable(void)
{
	register os_uint32_t oldIcr = OS_MFCR(OS_ICR);
	OS_MTCR(OS_ICR, ((oldIcr & ~OS_ICR_CCPN) | OS_ICR_IE));
	return oldIcr;
}

/* TRICORE_IntDisable() sets the ICR to the "disable" state (from the
 * constant OS_kernDisableLevel) and returns the previous state.
*/
#define OS_IntDisable() OS_IntDisableLevel(OS_kernDisableLevel)

static __inline__ os_uint32_t OS_IntDisableLevel(os_uint32_t dislevel) __attribute__((always_inline));
static __inline__ os_uint32_t OS_IntDisableLevel(os_uint32_t dislevel)
{
	register os_uint32_t oldIcr = OS_MFCR(OS_ICR);
	OS_MTCR(OS_ICR, ((oldIcr & ~(OS_ICR_IE | OS_ICR_CCPN)) | dislevel));
	return oldIcr;
}

/* OS_IntDisableConditional() sets the ICR to the "disable" state (from the
 * constant OS_kernDisableLevel) provided that by doing so the level does not get reduced,
 *  and returns the previous state.
*/
#define OS_IntDisableConditional() OS_IntDisableConditionalLevel(OS_kernDisableLevel)
static __inline__ os_uint32_t OS_IntDisableConditionalLevel(os_uint32_t dislevel) __attribute__((always_inline));
static __inline__ os_uint32_t OS_IntDisableConditionalLevel(os_uint32_t dislevel)
{
	register os_uint32_t oldIcr = OS_MFCR(OS_ICR);
	if ( (oldIcr & OS_ICR_CCPN) < (dislevel & OS_ICR_CCPN) )
	{
		OS_MTCR(OS_ICR, ((oldIcr & ~(OS_ICR_IE | OS_ICR_CCPN)) | dislevel));
	}
	return oldIcr;
}


/* TRICORE_IntDisableAll() sets the ICR to the "disable" state (from the
 * constant OS_intDisableLevelAll) and returns the previous state.
*/
#define OS_IntDisableAll() OS_IntDisableAllLevel(OS_intDisableLevelAll)
static __inline__ os_uint32_t OS_IntDisableAllLevel(os_uint32_t dislevel) __attribute__((always_inline));
static __inline__ os_uint32_t OS_IntDisableAllLevel(os_uint32_t dislevel)
{
	register os_uint32_t oldIcr = OS_MFCR(OS_ICR);
	OS_MTCR(OS_ICR, ((oldIcr & ~(OS_ICR_IE | OS_ICR_CCPN)) | dislevel));
	return oldIcr;
}

/* TRICORE_IntRestore() restores a previously-saved IE/CCPN status.
 * No value is returned.
*/
#define OS_IntRestore(p) \
	OS_MTCR(OS_ICR, ((OS_MFCR(OS_ICR) & ~(OS_ICR_IE | OS_ICR_CCPN)) | ((p) & (OS_ICR_IE | OS_ICR_CCPN))))

/* A buggy GNU compiler uses an address register when storing 32-bit data. The st.a instructions
 * need 32-bit alignment.
*/
#define OS_ArchMisalignedData(b,l)	( ( ((l) > 2) && ((((os_uint32_t)(b)) & 0x0003) != 0) ) || \
									( ((l) > 1) && ((((os_uint32_t)(b)) & 0x0001) != 0) ) )

#ifdef __cplusplus
}
#endif

#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
