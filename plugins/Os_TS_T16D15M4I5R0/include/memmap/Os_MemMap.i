/* Os_memmap.i
 *
 * This include file provides section mapping support for assembler files.
 * It includes the appropriate architecture include file depending
 * on the chosen architecture.
 *
 * See also: Os_defs.h
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_MemMap.i 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef OS_MEMMAP_I_
#define OS_MEMMAP_I_

#include <Os_defs.h>

#if (OS_ARCH==OS_PA)
#include <memmap/PA/Os_memmap_PA.i>
#elif (OS_ARCH==OS_TRICORE)
#include <memmap/TRICORE/Os_memmap_TRICORE.i>
#elif (OS_ARCH==OS_V850)
#include <memmap/V850/Os_memmap_V850.i>
#elif (OS_ARCH==OS_NEWARCH)
#include <memmap/NEWARCH/Os_memmap_NEWARCH.i>
#elif (OS_ARCH==OS_S12X)
#include <memmap/S12X/Os_memmap_S12X.i>
#elif (OS_ARCH==OS_XC2000)
#include <memmap/XC2000/Os_asm_XC2000.i>
#elif (OS_ARCH==OS_EASYCAN)
#include <memmap/EASYCAN/Os_memmap_EASYCAN.i>
#elif (OS_ARCH==OS_MB91)
#include <memmap/MB91/Os_memmap_MB91.i>
#elif (OS_ARCH==OS_R32C)
#include <memmap/R32C/Os_memmap_R32C.i>
#elif (OS_ARCH==OS_WINDOWS)
#include <memmap/WINDOWS/Os_memmap_WINDOWS.i>
#elif (OS_ARCH==OS_SH2)
#include <memmap/SH2/Os_memmap_SH2.i>
#elif (OS_ARCH==OS_ARM)
#include <memmap/ARM/Os_memmap_ARM.i>
#elif (OS_ARCH==OS_DSPIC33)
#include <memmap/DSPIC33/Os_memmap_DSPIC33.i>
#else
#error "Unsupported OS_ARCH defined. Check your Makefiles!"
#endif


#endif /*OS_MEMMAP_I_*/
