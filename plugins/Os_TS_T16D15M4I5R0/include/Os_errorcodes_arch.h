/* Os_errorcodes_arch.h
 *
 * Architecture-dependent error-codes are included from the appropriate
 * ARCH/Os_errorcodes_ARCH.h include file depending on the chosen architecture.
 *
 * The Makefiles must ensure that the OS_ARCH and OS_CPU macros are
 * defined appropriately on the command line.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_errorcodes_arch.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef __OS_ERRORCODES_ARCH_H
#define __OS_ERRORCODES_ARCH_H

#include <Os_defs.h>

#if (OS_ARCH==OS_PA)
#include <PA/Os_errorcodes_PA.h>
#elif (OS_ARCH==OS_TRICORE)
#include <TRICORE/Os_errorcodes_TRICORE.h>
#elif (OS_ARCH==OS_RH850)
#include <RH850/Os_errorcodes_RH850.h>
#elif (OS_ARCH==OS_V850)
#include <V850/Os_errorcodes_V850.h>
#elif (OS_ARCH==OS_NEWARCH)
#include <NEWARCH/Os_errorcodes_NEWARCH.h>
#elif (OS_ARCH==OS_PIKEOS)
#include <PIKEOS/Os_errorcodes_PIKEOS.h>
#elif (OS_ARCH==OS_S12X)
#include <S12X/Os_errorcodes_S12X.h>
#elif (OS_ARCH==OS_XC2000)
#include <XC2000/Os_errorcodes_XC2000.h>
#elif (OS_ARCH==OS_EASYCAN)
#include <EASYCAN/Os_errorcodes_EASYCAN.h>
#elif (OS_ARCH==OS_MB91)
#include <MB91/Os_errorcodes_MB91.h>
#elif (OS_ARCH==OS_R32C)
#include <R32C/Os_errorcodes_R32C.h>
#elif (OS_ARCH==OS_WINDOWS)
#include <WINDOWS/Os_errorcodes_WINDOWS.h>
#elif (OS_ARCH==OS_SH2)
#include <SH2/Os_errorcodes_SH2.h>
#elif (OS_ARCH==OS_SH4)
#include <SH4/Os_errorcodes_SH4.h>
#elif (OS_ARCH==OS_ARM)
#include <ARM/Os_errorcodes_ARM.h>
#elif (OS_ARCH==OS_LINUX)
#include <LINUX/Os_errorcodes_LINUX.h>
#elif (OS_ARCH==OS_DSPIC33)
#include <DSPIC33/Os_errorcodes_DSPIC33.h>
#else
#error "Unsupported OS_ARCH defined. Check your Makefiles!"
#endif

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
