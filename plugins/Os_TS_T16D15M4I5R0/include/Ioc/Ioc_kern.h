/* Ioc_kern.h
 *
 * Declarations for in-kernel IOC
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Ioc_kern.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 8.12 (required)
 * When an array is declared with external linkage, its size shall be stated
 * explicitly or defined implicitly by initialization.
 *
 * Reason:
 * Array size and initialization are depending on configuration whereas the
 * array declaration is used by configuration-independent library code.
 */

#ifndef MULTICORE_IOC_KERN_H
#define MULTICORE_IOC_KERN_H

#include <Os_types.h>
#include <Ioc/Ioc.h>

typedef struct
{
	os_uint16_t dataTypeStart;
	os_uint16_t dataTypeEnd;
	os_uint8_t lockToUse;
} Ioc_NQDataConfigType;

typedef struct
{
	os_uint16_t dataTypeStart;
	os_uint16_t dataTypeEnd;
	os_uint16_t dataQueueLen;
	os_uint8_t lockToUse;
} Ioc_QDataConfigType;

typedef struct
{
	void * dataBuffer;
	os_uint16_t dataSize;
} Ioc_DataTypeConfigType;

extern const os_uint16_t Ioc_NumOfUnqueuedData;
extern const os_uint16_t Ioc_NumOfQueuedData;

/* Deviation MISRA-1 */
extern const Ioc_NQDataConfigType Ioc_NQDataConfig[];
/* Deviation MISRA-1 */
extern const Ioc_DataTypeConfigType Ioc_NQDataTypeConfig[];
/* Deviation MISRA-1 */
extern const Ioc_QDataConfigType Ioc_QDataConfig[];
/* Deviation MISRA-1 */
extern const Ioc_DataTypeConfigType Ioc_QDataTypeConfig[];

/* Deviation MISRA-1 */
extern Ioc_QMetaType Ioc_QMeta[];

extern os_memoryaccess_t OS_KernIocCheckMemoryAccess( const void *data, os_size_t len );

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
