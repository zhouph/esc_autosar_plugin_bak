/* Ioc.h
 *
 * Common Type definitions for Ioc
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Ioc.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef MULTICORE_IOC_H
#define MULTICORE_IOC_H

#include <Os_types.h>

#define IOC_E_OK            0U      /* RTE_E_OK         */
#define IOC_E_LIMIT         130U    /* RTE_E_LIMIT      */
#define IOC_E_LOST_DATA     64U     /* RTE_E_LOST_DATA  */
#define IOC_E_NO_DATA       131U    /* RTE_E_NO_DATA    */
#define IOC_E_SEG_FAULT     136U    /* RTE_E_SEG_FAULT  */

#define IOC_QUEUE_FULL(QMeta, QSize) ((QMeta).count == (QSize))
#define IOC_QUEUE_EMPTY(QMeta, QSize) ((QMeta).count == 0U)

#define OS_IOC_LOCK_LOCAL 0xffU

typedef os_uint16_t Ioc_QIndexType;

/** \brief meta data for Ioc ring buffers */
typedef struct
{
	Ioc_QIndexType  count;
	Ioc_QIndexType  tail;
	os_boolean_t    dataLost;
} Ioc_QMetaType;

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
