/* Os_autosar.h - AUTOSAR API
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_autosar.h 20176 2015-01-20 09:01:44Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 19.13 (advisory)
 * The # and ## preprocessor operators should not be used.
 *
 * Reason:
 * ## operator must be used to generate an identifier as demanded by specification.
 *
 *
 * MISRA-2) Deviated Rule: 19.10 (required)
 * In the definition of a function-like macro each instance of a parameter shall
 * be enclosed in parentheses unless it is used as the operand of # or ##.
 *
 * Reason:
 * Macro parameter is used in a way that doesn't allow the use of parentheses,
 * i.e. for designating parameter names in a function declaration.
 *
 *
 * MISRA-3) Deviated Rule: 5.1 (required)
 * Identifiers (internal and external) shall not rely on the significance of more
 * than 31 characters.
 *
 * Reason:
 * These names are mandated by Autosar.
*/

#ifndef __OS_AUTOSAR_H
#define __OS_AUTOSAR_H

#include <Os_defs.h>

#if (OS_KERNEL_TYPE==OS_MICROKERNEL)
#error "Os_autosar.h should not be used in a microkernel environment"
#endif

#include <Os_api.h>
#include <Os_error.h>

/* Autosar is an extension of OSEK
 *
 * !LINKSTO Kernel.Autosar.OSEK, 1
*/
#include <Os_osek.h>
#include <Os_removeapi.h>

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * AUTOSAR API
 *
 * These macros define the AUTOSAR data types in terms of
 * OSEMKP data types.
 *
 * !LINKSTO Kernel.Autosar.API.DataTypes.ApplicationType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.TrustedFunctionIndexType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.TrustedFunctionParameterRefType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.AccessType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.ObjectAccessType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.ObjectTypeType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.MemoryStartAddressType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.MemorySizeType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.ISRType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.ScheduleTableType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.ScheduleTableStatusType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.ScheduleTableStatusRefType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.CounterType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.ProtectionReturnType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.RestartType, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.PhysicalTimeType, 1
*/

#if !defined(RTE_TYPE_CounterType) /* needed for RTE interaction */
#define RTE_TYPE_CounterType
#define CounterType						os_int32_t
#endif

#define ApplicationType					os_applicationid_t
#define TrustedFunctionIndexType		os_functionid_t
#define TrustedFunctionParameterRefType	void*
#define AccessType						os_memoryaccess_t
#define ObjectAccessType				os_boolean_t
#define ObjectTypeType					os_objecttype_t
#define MemoryStartAddressType			void*
#define MemorySizeType					os_size_t
#define ISRType							os_isrid_t
#define ScheduleTableType				os_scheduleid_t
#define ScheduleTableStatusType			os_uint8_t
#define ScheduleTableStatusRefType		os_uint8_t*
#define ProtectionReturnType			os_erroraction_t
#define RestartType						os_restart_t
#define PhysicalTimeType				os_physicaltime_t

/*!
 * AUTOSAR API
 *
 * Constructional elements
 *
 * TRUSTED() isn't really part of the Autosar API, but we define it here
 * anyway. t is the name of the function, i and p are the formal parameter
 * names
 *
 * !LINKSTO Kernel.Autosar.TrustedFunctions.Prototype, 2
*/
/* Deviation MISRA-1, MISRA-2 */
#define TRUSTED(t,i,p)	OS_EXTERN_C void TRUSTED_##t(TrustedFunctionIndexType i, TrustedFunctionParameterRefType p)

/*!
 * AUTOSAR API
 *
 * These macros define the AUTOSAR API values in terms of the kernel's own
 * API constants.
 *
 * !LINKSTO Kernel.Autosar.API.DataTypes.ApplicationType.Constants, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.ObjectAccessType.Constants, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.ObjectTypeType.Constants, 2
 * !LINKSTO Kernel.Autosar.API.DataTypes.ISRType.Constants, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.ScheduleTableStatusType.Constants, 2
 * !LINKSTO Kernel.Autosar.API.DataTypes.ProtectionReturnType.Constants, 2
 * !LINKSTO Kernel.Autosar.API.DataTypes.RestartType.Constants, 1
*/
#define INVALID_OSAPPLICATION		OS_NULLAPP
#define ACCESS						OS_TRUE
#define NO_ACCESS					OS_FALSE
#define OBJECT_TASK					OS_OBJ_TASK
#define OBJECT_ISR					OS_OBJ_ISR
#define OBJECT_ALARM				OS_OBJ_ALARM
#define OBJECT_RESOURCE				OS_OBJ_RESOURCE
#define OBJECT_COUNTER				OS_OBJ_COUNTER
#define OBJECT_SCHEDULETABLE		OS_OBJ_SCHEDULETABLE
#define INVALID_ISR					OS_NULLISR
#define SCHEDULETABLE_STOPPED		OS_STE_STOPPED
#define SCHEDULETABLE_NEXT			OS_STE_NEXT
#define SCHEDULETABLE_WAITING		OS_STE_WAITING
#define SCHEDULETABLE_RUNNING		OS_STE_ASYNCHRONOUS
#define SCHEDULETABLE_RUNNING_AND_SYNCHRONOUS	OS_STE_SYNCHRONOUS
#define PRO_IGNORE					OS_ACTION_DONOTHING
#define PRO_TERMINATETASKISR		OS_ACTION_KILL
#define PRO_TERMINATEAPPL			OS_ACTION_QUARANTINEAPP
#define PRO_TERMINATEAPPL_RESTART	OS_ACTION_RESTART
#define PRO_SHUTDOWN				OS_ACTION_SHUTDOWN
#define RESTART						OS_APPL_RESTART
#define NO_RESTART					OS_APPL_NO_RESTART

/*!
 * AUTOSAR API
 *
 * These macros define the AUTOSAR error codes in terms of OS error codes.
 *
 * !LINKSTO Kernel.Autosar.API.ErrorCodes, 2
*/
#define E_OS_SERVICEID				OS_E_TFID
#define E_OS_ILLEGAL_ADDRESS		OS_E_ADDRESS
#define E_OS_MISSINGEND				OS_E_TASKRETURN
#define E_OS_DISABLEDINT			OS_E_INTDISABLE
#define E_OS_STACKFAULT				OS_E_STACKPROT
#define E_OS_PROTECTION_MEMORY		OS_E_MEMPROT
#define E_OS_PROTECTION_TIME		OS_E_TIMEPROT
#define E_OS_PROTECTION_ARRIVAL		OS_E_RATEPROT
#define E_OS_PROTECTION_LOCKED		OS_E_LOCKPROT
#define E_OS_PROTECTION_EXCEPTION	OS_E_EXCEPTPROT

/*!
 * AUTOSAR API
 *
 * Some required data-access macros.
 *
 * !LINKSTO Kernel.Autosar.API.DataTypes.AccessType.Macros.IS_READABLE, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.AccessType.Macros.IS_WRITEABLE, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.AccessType.Macros.IS_EXECUTABLE, 1
 * !LINKSTO Kernel.Autosar.API.DataTypes.AccessType.Macros.IS_STACKSPACE, 1
*/
/* Autosar 1.3 */
#define OSMEMORY_IS_READABLE(x)		(((x)&OS_MA_READ)!=0)
#define OSMEMORY_IS_WRITEABLE(x)	(((x)&OS_MA_WRITE)!=0)
#define OSMEMORY_IS_EXECUTABLE(x)	(((x)&OS_MA_EXEC)!=0)
#define OSMEMORY_IS_STACKSPACE(x)	(((x)&OS_MA_STACK)!=0)

/*!
 * AUTOSAR API
 *
 * These macros define the AUTOSAR API calls in
 * terms of the OS system-call stubs
 *
 * !LINKSTO Kernel.Autosar.API.SystemServices.GetApplicationID, 2
 * !LINKSTO Kernel.Autosar.API.SystemServices.GetISRID, 2
 * !LINKSTO Kernel.Autosar.API.SystemServices.CallTrustedFunction, 1
 * !LINKSTO Kernel.Autosar.API.SystemServices.CheckISRMemoryAccess, 2
 * !LINKSTO Kernel.Autosar.API.SystemServices.CheckTaskMemoryAccess, 2
 * !LINKSTO Kernel.Autosar.API.SystemServices.CheckObjectAccess, 2
 * !LINKSTO Kernel.Autosar.API.SystemServices.CheckObjectOwnership, 2
 * !LINKSTO Kernel.Autosar.API.SystemServices.StartScheduleTableRel, 2
 * !LINKSTO Kernel.Autosar.API.SystemServices.StartScheduleTableAbs, 2
 * !LINKSTO Kernel.Autosar.API.SystemServices.StartScheduleTableSynchron, 2
 * !LINKSTO Kernel.Autosar.API.SystemServices.StopScheduleTable, 2
 * !LINKSTO Kernel.Autosar.API.SystemServices.ChainScheduleTable, 2
 * !LINKSTO Kernel.Autosar.API.SystemServices.IncrementCounter, 1
 * !LINKSTO Kernel.Autosar.API.SystemServices.SyncScheduleTable, 2
 * !LINKSTO Kernel.Autosar.API.SystemServices.GetScheduleTableStatus, 2
 * !LINKSTO Kernel.Autosar.API.SystemServices.TerminateApplication, 2
 * !LINKSTO Kernel.Autosar.API.SystemServices.GetCounterValue, 1
 * !LINKSTO Kernel.Autosar.API.SystemServices.GetElapsedCounterValue, 2
*/
#ifndef OS_EXCLUDE_INCREMENTCOUNTER
#define IncrementCounter(c)				OS_UserIncrementCounter(c)
#endif

#ifndef OS_EXCLUDE_GETCOUNTERVALUE
#define GetCounterValue(c,vr)			OS_UserGetCounterValue(c,vr)
#endif

#ifndef OS_EXCLUDE_GETELAPSEDCOUNTERVALUE
/* AUTOSAR 4.0 renamed GetElapsedCounterValue - we provide both names */
#define GetElapsedCounterValue(c,p,vr)	OS_UserGetElapsedCounterValue(c,p,vr)
#define GetElapsedValue(c,p,vr)			OS_UserGetElapsedCounterValue(c,p,vr)
#endif

#ifndef OS_EXCLUDE_STARTSCHEDULETABLE
#define StartScheduleTableRel(t,o)		OS_UserStartScheduleTable(t,o,1)
#define StartScheduleTableAbs(t,o)		OS_UserStartScheduleTable(t,o,0)
#endif

#ifndef OS_EXCLUDE_STARTSCHEDULETABLESYNCHRON
#define StartScheduleTableSynchron(t)	OS_UserStartScheduleTableSynchron(t)
#endif

#ifndef OS_EXCLUDE_NEXTSCHEDULETABLE
#define NextScheduleTable(c,n)			OS_UserChainScheduleTable(c,n)
#endif

#ifndef OS_EXCLUDE_STOPSCHEDULETABLE
#define StopScheduleTable(t)			OS_UserStopScheduleTable(t)
#endif

#ifndef OS_EXCLUDE_SYNCSCHEDULETABLE
#define SyncScheduleTable(t,g)			OS_UserSyncScheduleTable(t,g)
#endif

#ifndef OS_EXCLUDE_SETSCHEDULETABLEASYNC
#define SetScheduleTableAsync(s)		OS_UserSetScheduleTableAsync(s)
#endif

#ifndef OS_EXCLUDE_GETSCHEDULETABLESTATUS
#define GetScheduleTableStatus(t,sr)	OS_GetScheduleTableStatus(t,sr)
#endif

#ifndef OS_EXCLUDE_TERMINATEAPPLICATION
#define TerminateApplication(r)			OS_UserTerminateApplication(r)
#endif

#ifndef OS_EXCLUDE_GETAPPLICATIONID
#define GetApplicationID()				OS_UserGetApplicationId()
#endif

#ifndef OS_EXCLUDE_GETISRID
#define GetISRID()						OS_UserGetIsrId()
#endif

#ifndef OS_EXCLUDE_CALLTRUSTEDFUNCTION
#define CallTrustedFunction(f,p)		OS_UserCallTrustedFunction(f,p)
#endif

#ifndef OS_EXCLUDE_CHECKISRMEMORYACCESS
#define CheckISRMemoryAccess(i,b,l)		OS_UserCheckIsrMemoryAccess(i,b,l)
#endif

#ifndef OS_EXCLUDE_CHECKTASKMEMORYACCESS
#define CheckTaskMemoryAccess(t,b,l)	OS_UserCheckTaskMemoryAccess(t,b,l)
#endif

#ifndef OS_EXCLUDE_CHECKOBJECTACCESS
#define CheckObjectAccess(a,t,o)		OS_UserCheckObjectAccess(a,t,o)
#endif

#ifndef OS_EXCLUDE_CHECKOBJECTOWNERSHIP
#define CheckObjectOwnership(t,o)		OS_UserCheckObjectOwnership(t,o)
#endif

/*!
 * AUTOSAR API
 *
 * These macros define the AUTOSAR Service IDs in
 * terms of the OS SIDs
*/
#define OSServiceID_IncrementCounter		OS_SID_IncrementCounter
#define OSServiceID_StartScheduleTableRel	OS_SID_StartScheduleTableRel
#define OSServiceID_StartScheduleTableAbs	OS_SID_StartScheduleTableAbs
#define OSServiceID_StartScheduleTableSynchron	OS_SID_StartScheduleTableSynchron
#define OSServiceID_NextScheduleTable		OS_SID_ChainScheduleTable
#define OSServiceID_StopScheduleTable		OS_SID_StopScheduleTable
#define OSServiceID_SyncScheduleTable		OS_SID_SyncScheduleTable
#define OSServiceID_SetScheduleTableAsync	OS_SID_SetScheduleTableAsync
#define OSServiceID_GetScheduleTableStatus	OS_SID_GetScheduleTableStatus
#define OSServiceID_TerminateApplication	OS_SID_TerminateApplication
#define OSServiceID_GetApplicationId		OS_SID_GetApplicationId
#define OSServiceID_GetIsrId				OS_SID_GetIsrId
#define OSServiceID_CallTrustedFunction		OS_SID_CallTrustedFunction
#define OSServiceID_CheckIsrMemoryAccess	OS_SID_CheckIsrMemoryAccess
#define OSServiceID_CheckTaskMemoryAccess	OS_SID_CheckTaskMemoryAccess
#define OSServiceID_CheckObjectAccess		OS_SID_CheckObjectAccess
#define OSServiceID_CheckObjectOwnership	OS_SID_CheckObjectOwnership
#define OSServiceID_GetCounterValue			OS_SID_GetCounterValue
#define OSServiceID_GetElapsedCounterValue	OS_SID_GetElapsedCounterValue
#define OSServiceID_GetElapsedValue			OS_SID_GetElapsedCounterValue

/* CHECK: SAVE
 * CHECK: RULE 306 OFF (Looks nicer with long lines)
 *
 * !LINKSTO Kernel.Autosar.OsApplication.ApplicationHooks.ErrorHook.Call.OsErrorMacros, 1
*/
/* Deviation MISRA-3 <START> */
#define OSError_IncrementCounter_CounterID()				((CounterType)						(OS_errorStatus.parameter[0]))
#define OSError_StartScheduleTableRel_ScheduleTableID()		((ScheduleTableType)				(OS_errorStatus.parameter[0]))
#define OSError_StartScheduleTableRel_Offset()				((TickType)							(OS_errorStatus.parameter[1]))
#define OSError_StartScheduleTableAbs_ScheduleTableID()		((ScheduleTableType)				(OS_errorStatus.parameter[0]))
#define OSError_StartScheduleTableAbs_Tickvalue()			((TickType)							(OS_errorStatus.parameter[1]))
#define OSError_ChainScheduleTable_SchedTableID_current()	((ScheduleTableType)				(OS_errorStatus.parameter[0]))
#define OSError_ChainScheduleTable_SchedTableID_next()		((ScheduleTableType)				(OS_errorStatus.parameter[1]))
#define OSError_StopScheduleTable_ScheduleTableID()			((ScheduleTableType)				(OS_errorStatus.parameter[0]))
#define OSError_SyncScheduleTable_SchedTableID()			((ScheduleTableType)				(OS_errorStatus.parameter[0]))
#define OSError_SyncScheduleTable_GlobalTime()				((TickType)							(OS_errorStatus.parameter[1]))
#define OSError_SetScheduleTableAsync_ScheduleID()			((ScheduleTableType)				(OS_errorStatus.parameter[0]))
#define OSError_GetScheduleTableStatus_ScheduleID()			((ScheduleTableType)				(OS_errorStatus.parameter[0]))
#define OSError_GetScheduleTableStatus_ScheduleStatus()		((ScheduleTableStatusRefType)		(OS_errorStatus.parameter[1]))
#define OSError_CallTrustedFunction_FunctionIndex()			((TrustedFunctionIndexType)			(OS_errorStatus.parameter[0]))
#define OSError_CallTrustedFunction_FunctionParams()		((TrustedFunctionParameterRefType)	(OS_errorStatus.parameter[1]))
#define OSError_CheckIsrMemoryAccess_ISRID()				((ISRType)							(OS_errorStatus.parameter[0]))
#define OSError_CheckIsrMemoryAccess_Address()				((MemoryStartAddressType)			(OS_errorStatus.parameter[1]))
#define OSError_CheckIsrMemoryAccess_Size()					((MemorySizeType)					(OS_errorStatus.parameter[2]))
#define OSError_CheckTaskMemoryAccess_TaskID()				((TaskType)							(OS_errorStatus.parameter[0]))
#define OSError_CheckTaskMemoryAccess_Address()				((MemoryStartAddressType)			(OS_errorStatus.parameter[1]))
#define OSError_CheckTaskMemoryAccess_Size()				((MemorySizeType)					(OS_errorStatus.parameter[2]))
#define OSError_CheckObjectAccess_ApplID()					((ApplicationType)					(OS_errorStatus.parameter[0]))
#define OSError_CheckObjectAccess_ObjectType()				((ObjectTypeType)					(OS_errorStatus.parameter[1]))
#define OSError_CheckObjectAccess_Object()					((os_objectid_t)					(OS_errorStatus.parameter[2]))
#define OSError_CheckObjectOwnership_ObjectType()			((ObjectTypeType)					(OS_errorStatus.parameter[0]))
#define OSError_CheckObjectOwnership_Object()				((os_objectid_t)					(OS_errorStatus.parameter[1]))
#define OSError_GetCounterValue_CounterID()					((CounterType)						(OS_errorStatus.parameter[0]))
#define OSError_GetCounterValue_Value()						((TickRefType)						(OS_errorStatus.parameter[1]))
#define OSError_StartScheduleTableSynchron_ScheduleTableID()	((ScheduleTableType)			(OS_errorStatus.parameter[0]))
#define OSError_GetElapsedCounterValue_CounterID()			((CounterType)						(OS_errorStatus.parameter[0]))
#define OSError_GetElapsedCounterValue_PreviousValue()		((TickType)							(OS_errorStatus.parameter[1]))
#define OSError_GetElapsedCounterValue_Value()				((TickRefType)						(OS_errorStatus.parameter[2]))
/* API may be referenced as GetElapsedCounterValue (<= 3.1) or as GetElapsedValue (>= 4.0) */
#define OSError_GetElapsedValue_CounterID()					((CounterType)						(OS_errorStatus.parameter[0]))
#define OSError_GetElapsedValue_PreviousValue()				((TickType)							(OS_errorStatus.parameter[1]))
#define OSError_GetElapsedValue_Value()						((TickRefType)						(OS_errorStatus.parameter[2]))
/* Deviation MISRA-3 <STOP> */
/* CHECK: RESTORE
*/

#ifndef OS_ASM
/* Library routines to map some OS system calls onto AUTOSAR APIs
*/
StatusType OS_GetScheduleTableStatus(ScheduleTableType, ScheduleTableStatusRefType);
#endif

#ifdef __cplusplus
}
#endif

#include <Os_callouts.h>

#endif

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
