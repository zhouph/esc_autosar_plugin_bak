/**
 * \file
 *
 * \brief AUTOSAR Dem
 *
 * This file contains the implementation of the AUTOSAR
 * module Dem.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* This file contains all definitions of the configuration of the AUTOSAR
 * module Dem that are target independent. */

[!AUTOSPACING!]
[!//
/*==================[inclusions]============================================*/
/* !LINKSTO dsn.Dem.IncludeStr,1 */

#include <TSAutosar.h>                    /* EB specific standard types */
#include <Std_Types.h>                    /* AUTOSAR standard types */
#include <Dem_Int.h>                      /* API and internal declarations,
                                           * dependent and static part */

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

#define DEM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

CONST(Dem_ConfigType, DEM_CONST) [!"name(DemConfigSet/*[1])"!] = {0U};

/*------------------[Freeze frame classes configuration]--------------------*/
[!//
[!INDENT "0"!]
  [!IF "count(DemGeneral/DemDidClass/*) > 0"!]
    [!LOOP "DemGeneral/DemDidClass/*"!]
      [!VAR "NumFFElement" = "count(DemDidDataElementClassRef/*)"!]

      /** \brief Indices of element on freeze frame data segment [!"name(.)"!] */
      STATIC CONST(Dem_DataElementIdxType, DEM_CONST)
        [!WS "2"!]Dem_DataElementIdx[!"name(.)"!][[!"num:integer($NumFFElement)"!]] =
      {
        [!LOOP "DemDidDataElementClassRef/*"!]
          [!VAR "NameInSegment" = "name(as:ref(.))"!]
          [!VAR "IdxCtr" = "0"!]
          [!LOOP "../../../../DemDataElementClass/*[. = 'DemExternalCSDataElementClass']"!]
            [!IF "$NameInSegment = name(.)"!]
              [!WS "2"!][!"num:integer($IdxCtr)"!]U, /* [!"$NameInSegment"!] */
              [!BREAK!]
            [!ELSE!][!//
              [!VAR "IdxCtr" = "$IdxCtr + 1"!]
            [!ENDIF!]
          [!ENDLOOP!]
        [!ENDLOOP!]
      };

      /** \brief Start-bytes of element on freeze frame data segment [!"name(.)"!] */
      STATIC CONST(Dem_DataElementOffsetType, DEM_CONST)
        [!WS "2"!]Dem_DataElementOffset[!"name(.)"!][[!"num:integer($NumFFElement)"!]] =
      {
        [!VAR "Offset" = "0"!]
        [!LOOP "DemDidDataElementClassRef/*"!]
          [!WS "2"!][!"num:integer($Offset)"!]U, [!//
          [!IF "as:ref(.) = 'DemInternalDataElementClass'"!]
            /* [!"as:ref(.)/DemInternalDataElement"!] */
          [!ELSE!][!//
            /* [!"name(as:ref(.))"!] */
            [!VAR "Offset" = "$Offset + as:ref(.)/DemDataElementDataSize"!]
          [!ENDIF!]
        [!ENDLOOP!]
      };
    [!ENDLOOP!]

    /** \brief freeze frame segments (DIDs) */
    CONST(Dem_FFSegmentType, DEM_CONST) Dem_FFSegment[DEM_NUM_FFSEGS] =
    {
      [!LOOP "DemGeneral/DemDidClass/*"!]
        [!WS "2"!]/* [!"name(.)"!] */
        [!WS "2"!]{
          [!WS "4"!][!"num:integer(count(DemDidDataElementClassRef/*))"!]U,
          [!WS "4"!]DEM_FFS_DID_[!"name(.)"!],
          [!WS "4"!]Dem_DataElementIdx[!"name(.)"!],
          [!WS "4"!]Dem_DataElementOffset[!"name(.)"!]
        [!WS "2"!]},
      [!ENDLOOP!]
    };
    [!LOOP "DemGeneral/DemFreezeFrameClass/*"!]

      /*---[Freeze frame class [!"name(.)"!]]---*/
      [!VAR "NumFFSegs" = "count(DemDidClassRef/*)"!]

      /** \brief Indices of segments on freeze frame class [!"name(.)"!] to ::Dem_FFSegment[] */
      STATIC CONST(Dem_FFSegIdxType, DEM_CONST) Dem_FFSegIdx[!"name(.)"!][[!"num:integer($NumFFSegs)"!]] =
      {
        [!LOOP "DemDidClassRef/*"!]
          [!WS "2"!][!"as:ref(.)/@index"!]U, /* [!"name(as:ref(.))"!] */
        [!ENDLOOP!]
      };

      /** \brief Start-bytes of segments on freeze frame class [!"name(.)"!] */
      STATIC CONST(Dem_FFStartByteType, DEM_CONST)
        [!WS "2"!]Dem_FFStartByte[!"name(.)"!][[!"num:integer($NumFFSegs)"!]] =
      {
        [!VAR "StartByte" = "0"!]
        [!LOOP "DemDidClassRef/*"!]
          [!WS "2"!][!"num:integer($StartByte)"!]U, /* [!"name(as:ref(.))"!] */
          [!LOOP "(as:ref(.)/DemDidDataElementClassRef/*)"!]
            [!IF "as:ref(.) != 'DemInternalDataElementClass'"!]
              [!VAR "StartByte" = "$StartByte + as:ref(.)/DemDataElementDataSize"!]
            [!ENDIF!]
          [!ENDLOOP!]
        [!ENDLOOP!]
      };
    [!ENDLOOP!]

    /** \brief Array containing all freeze frame classes */
    CONST(Dem_FFClassType, DEM_CONST) Dem_FFClass[DEM_NUM_FFCLS] =
    {
      [!INDENT "2"!]
        [!LOOP "DemGeneral/DemFreezeFrameClass/*"!]
          {
            [!VAR "NumFFSegs" = "count(DemDidClassRef/*)"!]
            [!WS "2"!][!"num:integer($NumFFSegs)"!]U, /* number of segments */
            [!WS "2"!]Dem_FFSegIdx[!"name(.)"!],
            [!WS "2"!]Dem_FFStartByte[!"name(.)"!]
          },
        [!ENDLOOP!]
        [!IF "count(DemConfigSet/*/DemEventParameter/*[not(node:exists(DemFreezeFrameClassRef))]) > 0"!]
          {
            [!WS "2"!]0U,
            [!WS "2"!]NULL_PTR,
            [!WS "2"!]NULL_PTR
          }
        [!ENDIF!]
      [!ENDINDENT!]
    };
  [!ENDIF!]
[!ENDINDENT!]

/*------------------[Freeze frame record number classes configuration]------*/
[!//
[!INDENT "0"!]
  [!IF "DemGeneral/DemTypeOfFreezeFrameRecordNumeration = 'DEM_FF_RECNUM_CONFIGURED' and count(DemConfigSet/*/DemEventParameter/*[node:exists(DemFreezeFrameClassRef)]) > 0"!]
    [!LOOP "DemGeneral/DemFreezeFrameRecNumClass/*"!]
      [!VAR "NumFFRecords" = "count(DemFreezeFrameRecordNumber/*)"!]

      /** \brief Freeze frame record numbers for [!"name(.)"!] */
      STATIC CONST(Dem_FFIdxType, DEM_CONST)
        [!WS "2"!]Dem_FFRecNumClass[!"name(.)"!][[!"num:integer($NumFFRecords)"!]] =
      {
        [!LOOP "DemFreezeFrameRecordNumber/*"!]
          [!WS "2"!][!"translate(num:inttohex(., 2), 'abcdef', 'ABCDEF')"!]U,
        [!ENDLOOP!]
      };
    [!ENDLOOP!]

    /** \brief Array containing all freeze frame record number classes */
    CONST(Dem_FFRecNumerationClassType, DEM_CONST)
      [!WS "2"!]Dem_FFRecNumerationClass[DEM_NUM_FFRECNUMCLS] =
    {
      [!LOOP "DemGeneral/DemFreezeFrameRecNumClass/*"!]
        [!WS "2"!]{
          [!WS "4"!][!"num:integer(count(DemFreezeFrameRecordNumber/*))"!]U, /* number of freeze frame records */
          [!WS "4"!]Dem_FFRecNumClass[!"name(.)"!]
        [!WS "2"!]},
      [!ENDLOOP!]
    };
  [!ENDIF!]
[!ENDINDENT!]

/*------------------[Extended data classes configuration]-------------------*/
[!//
[!INDENT "0"!]
  [!IF "count(DemGeneral/DemExtendedDataRecordClass/*) > 0"!]
    [!LOOP "DemGeneral/DemExtendedDataRecordClass/*"!]
      [!VAR "NumEDElement" = "count(DemDataElementClassRef/*)"!]

      /** \brief Indices of element on extended data segment [!"name(.)"!] */
      STATIC CONST(Dem_DataElementIdxType, DEM_CONST)
        [!WS "2"!]Dem_DataElementIdx[!"name(.)"!][[!"num:integer($NumEDElement)"!]] =
      {
        [!LOOP "DemDataElementClassRef/*"!]
          [!IF "as:ref(.) = 'DemInternalDataElementClass'"!]
            [!IF "as:ref(.)/DemInternalDataElement = 'DEM_AGINGCTR'"!]
              [!WS "2"!]DEM_INT_VAL_IDX_AGECTR,
            [!ELSEIF "as:ref(.)/DemInternalDataElement = 'DEM_OCCCTR'"!]
              [!WS "2"!]DEM_INT_VAL_IDX_OCCCTR,
            [!ELSEIF "as:ref(.)/DemInternalDataElement = 'DEM_OVFLIND'"!]
              [!WS "2"!]DEM_INT_VAL_IDX_OVFIND,
            [!ELSEIF "as:ref(.)/DemInternalDataElement = 'DEM_SIGNIFICANCE'"!]
              [!WS "2"!]DEM_INT_VAL_IDX_EVSIGNIF
            [!ENDIF!]
          [!ENDIF!]
          [!VAR "NameInSegment" = "name(as:ref(.))"!]
          [!VAR "IdxCtr" = "0"!]
          [!LOOP "../../../../DemDataElementClass/*[. = 'DemExternalCSDataElementClass']"!]
            [!IF "$NameInSegment = name(.)"!]
              [!WS "2"!][!"num:integer($IdxCtr)"!]U, /* [!"$NameInSegment"!] */
              [!BREAK!]
            [!ELSE!][!//
              [!VAR "IdxCtr" = "$IdxCtr + 1"!]
            [!ENDIF!]
          [!ENDLOOP!]
        [!ENDLOOP!]
      };

      /** \brief Start-bytes of element on extended data segment [!"name(.)"!] */
      STATIC CONST(Dem_DataElementOffsetType, DEM_CONST)
        [!WS "2"!]Dem_DataElementOffset[!"name(.)"!][[!"num:integer($NumEDElement)"!]] =
      {
        [!VAR "Offset" = "0"!]
        [!LOOP "DemDataElementClassRef/*"!]
          [!WS "2"!][!"num:integer($Offset)"!]U, [!//
          [!IF "as:ref(.) = 'DemInternalDataElementClass'"!]
            /* [!"as:ref(.)/DemInternalDataElement"!] */
          [!ELSE!][!//
            /* [!"name(as:ref(.))"!] */
            [!VAR "Offset" = "$Offset + as:ref(.)/DemDataElementDataSize"!]
          [!ENDIF!]
        [!ENDLOOP!]
      };
    [!ENDLOOP!]

    /** \brief Extended data segments (Records) */
    CONST(Dem_EDSegmentType, DEM_CONST) Dem_EDSegment[DEM_NUM_EDSEGS] =
    {
      [!LOOP "DemGeneral/DemExtendedDataRecordClass/*"!]
        [!WS "2"!]/* [!"name(.)"!] */
        [!WS "2"!]{
          [!WS "4"!][!"num:integer(count(DemDataElementClassRef/*))"!]U,
          [!WS "4"!]DEM_EDS_NUM_[!"name(.)"!],
          [!IF "not(node:exists(DemExtendedDataRecordUpdate))"!]
            [!WS "4"!]TRUE,
          [!ELSE!]
            [!IF "DemExtendedDataRecordUpdate = 'DEM_UPDATE_RECORD_YES'"!]
              [!WS "4"!]TRUE,
            [!ELSE!]
              [!WS "4"!]FALSE,
            [!ENDIF!]
          [!ENDIF!]
          [!WS "4"!]Dem_DataElementIdx[!"name(.)"!],
          [!WS "4"!]Dem_DataElementOffset[!"name(.)"!]
        [!WS "2"!]},
      [!ENDLOOP!]
    };
    [!LOOP "DemGeneral/DemExtendedDataClass/*"!]

      /*---[Extended data class [!"name(.)"!]]---*/
      [!VAR "NumEDSegs" = "count(DemExtendedDataRecordClassRef/*)"!]

      /** \brief Indices of segments on extended data class [!"name(.)"!] to ::Dem_EDSegment[] */
      STATIC CONST(Dem_EDSegIdxType, DEM_CONST) Dem_EDSegIdx[!"name(.)"!][[!"num:integer($NumEDSegs)"!]] =
      {
        [!LOOP "DemExtendedDataRecordClassRef/*"!]
          [!WS "2"!][!"as:ref(.)/@index"!]U, /* [!"name(as:ref(.))"!] */
        [!ENDLOOP!]
      };

      /** \brief Start-bytes of segments on extended data class [!"name(.)"!] */
      STATIC CONST(Dem_EDStartByteType, DEM_CONST)
        [!WS "2"!]Dem_EDStartByte[!"name(.)"!][[!"num:integer($NumEDSegs)"!]] =
      {
        [!VAR "StartByte" = "0"!]
        [!LOOP "DemExtendedDataRecordClassRef/*"!]
          [!WS "2"!][!"num:integer($StartByte)"!]U, /* [!"name(as:ref(.))"!] */
          [!LOOP "(as:ref(.)/DemDataElementClassRef/*)"!]
            [!IF "as:ref(.) != 'DemInternalDataElementClass'"!]
              [!VAR "StartByte" = "$StartByte + as:ref(.)/DemDataElementDataSize"!]
            [!ENDIF!]
          [!ENDLOOP!]
        [!ENDLOOP!]
      };
    [!ENDLOOP!]

    /** \brief Array containing all extended data classes */
    CONST(Dem_EDClassType, DEM_CONST) Dem_EDClass[DEM_NUM_EDCLS] =
    {
      [!INDENT "2"!]
        [!LOOP "DemGeneral/DemExtendedDataClass/*"!]
          {
            [!VAR "NumEDSegs" = "count(DemExtendedDataRecordClassRef/*)"!]
            [!WS "2"!][!"num:integer($NumEDSegs)"!]U, /* number of segments */
            [!WS "2"!]Dem_EDSegIdx[!"name(.)"!],
            [!WS "2"!]Dem_EDStartByte[!"name(.)"!]
          },
        [!ENDLOOP!]
        [!IF "count(DemConfigSet/*/DemEventParameter/*[not(node:exists(DemExtendedDataClassRef))]) > 0"!]
          {
            [!WS "2"!]0U,
            [!WS "2"!]NULL_PTR,
            [!WS "2"!]NULL_PTR
          }
        [!ENDIF!]
      [!ENDINDENT!]
    };
  [!ENDIF!]
[!ENDINDENT!]

/*------------------[Data element classes configuration]--------------------*/
[!//
[!IF "(count(DemGeneral/DemDataElementClass/*)) > 0"!]
  [!INDENT "0"!]

    /** \brief Array containing all data elements */
    CONST(Dem_DataElementType, DEM_CONST) Dem_DataElement[DEM_NUM_EXT_CS_DATAELEMENTS] =
    {
      [!LOOP "DemGeneral/DemDataElementClass/*[. = 'DemExternalCSDataElementClass']"!]
        [!WS "2"!]/* [!"name(.)"!] */
        [!WS "2"!]{
          [!WS "4"!][!"./DemDataElementDataSize"!]U,
            [!IF "node:exists(./DemDataElementReadFnc)"!]
              [!VAR "CallbackName" = "./DemDataElementReadFnc"!]
            [!ELSE!]
              [!VAR "CallbackName" = "concat('Rte_Call_CBReadData_', name(.), '_ReadData')"!]
            [!ENDIF!]
          [!WS "4"!]&[!"$CallbackName"!]
        [!WS "2"!]},
      [!ENDLOOP!]
    };
  [!ENDINDENT!]
[!ENDIF!]

/*------------------[DTC groups configuration]------------------------------*/

/** \brief Array containing the DTC group values */
CONST(Dem_DTCGroupType, DEM_CONST) Dem_DTCGroups[DEM_NUM_DTC_GROUPS] =
{
  [!/* sort DTC group values in ascending order, to support search-algorithm */!][!//
  [!LOOP "node:order(DemGeneral/DemGroupOfDTC/*, './DemGroupDTCs')"!]
    [!WS "2"!]DemConf_DemGroupOfDTC_[!"name(.)"!],
  [!ENDLOOP!]
};

/*------------------[Events configuration]----------------------------------*/

/** \brief Event configuration description table (uses 'bit packing scheme) */
CONST(Dem_EventDescType, DEM_CONST) Dem_EventDesc[DEM_NUMBER_OF_EVENTS] =
{
  { 0U, 0U, 0U },                                 /* invalid event id entry */
[!/* initialize counting variables */!][!//
  [!VAR "EventFailureClassIdx" = "0"!]
  [!VAR "DebounceCtrIdx"  = "0"!]
  [!VAR "DebounceTimeIdx" = "0"!]
  [!VAR "DebounceFrqIdx"  = "0"!]
  [!VAR "EnCondStartIdx" = "0"!]
  [!VAR "NumEnCond"      = "0"!]
[!/* create unique list of all configured priority values ordered in ascending order */!][!//
  [!VAR "PriorityValueList" = "'#'"!]
  [!LOOP "node:order(DemConfigSet/*/DemEventParameter/*/DemEventClass, 'DemEventPriority')"!]
    [!IF "not(contains($PriorityValueList, concat('#', DemEventPriority, '#')))"!]
[!/*  add non-existing higher priority value at the end of the list */!][!//
      [!VAR "PriorityValueList" = "concat($PriorityValueList, DemEventPriority, '#')"!]
    [!ENDIF!]
  [!ENDLOOP!]
[!/* create unique list of all configured failure cycle counter cycles and thresholds */!][!//
  [!VAR "FailureCycleCfgList" = "'#'"!]
  [!LOOP "(DemConfigSet/*/DemEventParameter/* [node:exists(./DemEventClass/DemEventFailureCycleRef)])"!]
    [!VAR "EventFailureCycle" = "node:name(as:ref(DemEventClass/DemEventFailureCycleRef))"!]
    [!VAR "EventFailureThreshold" = "node:value(DemEventClass/DemEventFailureCycleCounterThreshold)"!]
    [!IF "not(contains($FailureCycleCfgList, concat('#', $EventFailureCycle, '*', $EventFailureThreshold, '#')))"!]
[!/*  add non-existing cycle counter cycle and threshold to the list */!][!//
      [!VAR "FailureCycleCfgList" = "concat($FailureCycleCfgList, $EventFailureCycle, '*', $EventFailureThreshold, '#')"!]
    [!ENDIF!]
  [!ENDLOOP!]
[!/* generate event descriptions */!][!//
  [!LOOP "node:order(DemConfigSet/*/DemEventParameter/*, 'DemEventId')"!]
    [!WS "2"!]{ /* event: [!"name(.)"!] */
[!/*  -----[EvConf1]----- */!][!//
      [!IF "node:exists(DemDTCClassRef)"!]
        [!WS "4"!][!"translate(num:inttohex(as:ref(DemDTCClassRef)/DemUdsDTC, 6), 'abcdef', 'ABCDEF')"!]U[!WS "50"!]/* DTC value */
      [!ELSE!][!/* No DTC class configured */!][!//
        [!WS "4"!]( (uint32)(DEM_NO_DTC) )                                /* No DTC Value */
      [!ENDIF!]
      [!WS "4"!]| ( (uint32)[!//
      [!INDENT "0"!]
        [!/* exactly one event destination can be selected, ensured in xdm */!][!//
        [!IF "DemEventClass/DemEventDestination = 'DEM_DTC_ORIGIN_PRIMARY_MEMORY'"!]
          (DEM_DTC_ORIGIN_PRIMARY_MEMORY - 1U)[!//
        [!ENDIF!]
        [!IF "DemEventClass/DemEventDestination = 'DEM_DTC_ORIGIN_SECONDARY_MEMORY'"!]
          (DEM_DTC_ORIGIN_SECONDARY_MEMORY - 1U)[!//
        [!ENDIF!]
        [!IF "DemEventClass/DemEventDestination = 'DEM_DTC_ORIGIN_MIRROR_MEMORY'"!]
          (DEM_DTC_ORIGIN_MIRROR_MEMORY - 1U)[!//
        [!ENDIF!]
        [!WS!]<< DEM_DTCORIGIN_OFFSET )
      [!ENDINDENT!]
                                                           /* Origin of DTC */
[!/*  find group to which the configured DTC belongs to */!][!//
      [!IF "node:exists(DemDTCClassRef)"!]
        [!IF "node:exists(as:ref(DemDTCClassRef)/DemUdsDTC)"!]
          [!VAR "GroupName" = "substring(name(node:order(../../../../DemGeneral/DemGroupOfDTC/*[DemGroupDTCs < as:ref(node:current()/DemDTCClassRef)/DemUdsDTC], './DemGroupDTCs')[last()]), 15)"!]
        [!ELSEIF "node:exists(as:ref(DemDTCClassRef)/DemObdDTC)"!]
          [!VAR "GroupName" = "substring(name(node:order(../../../../DemGeneral/DemGroupOfDTC/*[DemGroupDTCs < as:ref(node:current()/DemDTCClassRef)/DemObdDTC], './DemGroupDTCs')[last()]), 15)"!]
        [!ENDIF!]
        [!WS "4"!]| ( (uint32)DEM_DTCGRP_IDX_[!"$GroupName"!] << DEM_DTCGROUP_OFFSET )
                                                            /* Group of DTC */
      [!ELSE!][!/* No DTC class configured */!][!//
        [!WS "4"!]| ( (uint32)DEM_DTCGRP_IDX_NO_DTC << DEM_DTCGROUP_OFFSET )
      [!ENDIF!]
      [!IF "node:exists(DemDTCClassRef)"!]
        [!IF "as:ref(DemDTCClassRef)/DemImmediateNvStorage = 'true'"!]
          [!WS "4"!]| ( (uint32)1U << DEM_IMMEDIATESTORAGE_OFFSET )
                                               /* Immediate storage enabled */
        [!ELSE!][!//
          [!WS "4"!]| ( (uint32)0U << DEM_IMMEDIATESTORAGE_OFFSET )
                                              /* Immediate storage disabled */
        [!ENDIF!]
      [!ENDIF!]
      [!WS "4"!],
[!/*  -----[EvConf2]----- */!][!//
      [!IF "node:exists(DemDTCClassRef) and node:exists(as:ref(DemDTCClassRef)/DemDTCSeverity)"!]
        [!IF "as:ref(DemDTCClassRef)/DemDTCSeverity = 'DEM_DTC_SEV_NO_SEVERITY'"!]
          [!WS "4"!]0U          /* Severity of DTC: corresponds to DEM_SEVERITY_NO_SEVERITY */
        [!ELSEIF "as:ref(DemDTCClassRef)/DemDTCSeverity = 'DEM_DTC_SEV_MAINTENANCE_ONLY'"!]
          [!WS "4"!]1U     /* Severity of DTC: corresponds to DEM_SEVERITY_MAINTENANCE_ONLY */
        [!ELSEIF "as:ref(DemDTCClassRef)/DemDTCSeverity = 'DEM_DTC_SEV_CHECK_AT_NEXT_HALT'"!]
          [!WS "4"!]2U   /* Severity of DTC: corresponds to DEM_SEVERITY_CHECK_AT_NEXT_HALT */
        [!ELSEIF "as:ref(DemDTCClassRef)/DemDTCSeverity = 'DEM_DTC_SEV_IMMEDIATELY'"!]
          [!WS "4"!]3U    /* Severity of DTC: corresponds to DEM_SEVERITY_CHECK_IMMEDIATELY */
[!/*      ELSE-case will never appear, ensured by enumeration in xdm */!][!//
        [!ENDIF!]
      [!ELSE!][!//
        [!WS "4"!]0U                                     /* No severity specified for DTC */
      [!ENDIF!]
      [!IF "DemEventClass/DemFFPrestorageSupported = 'true'"!]
        [!WS "4"!]| ( (uint32)1U << DEM_ENABLEPRESTORAGE_OFFSET )   /* Prestorage enabled */
      [!ELSE!][!//
        [!WS "4"!]| ( (uint32)0U << DEM_ENABLEPRESTORAGE_OFFSET )  /* Prestorage disabled */
      [!ENDIF!]
      [!IF "node:exists(DemFreezeFrameClassRef)"!]
        [!WS "4"!]| ( (uint32)DEM_FFCLS_IDX_[!"name(as:ref(DemFreezeFrameClassRef))"!] << DEM_FFCLASSIDX_OFFSET )
                                                      /* Freeze frame class */
        [!IF "../../../../DemGeneral/DemTypeOfFreezeFrameRecordNumeration = 'DEM_FF_RECNUM_CALCULATED'"!]
          [!WS "4"!]| ( (uint32)[!"DemMaxNumberFreezeFrameRecords"!]U << DEM_FFRECINFO_OFFSET )
                                         /* Maximal number of freeze frames */
        [!ELSE!][!//
          [!WS "4"!]| ( (uint32)DEM_FFRECNUMCLS_IDX_[!"name(as:ref(DemFreezeFrameRecNumClassRef))"!] << DEM_FFRECINFO_OFFSET )
                           /* Index of freeze frame record numeration class */
        [!ENDIF!]
      [!ELSE!][!//
        [!WS "4"!]| ( (uint32)DEM_FFCLS_NULL_IDX << DEM_FFCLASSIDX_OFFSET )
                                                   /* No freeze frame class */
        [!WS "4"!]| ( (uint32)0U << DEM_FFRECINFO_OFFSET )
                                                 /* No freeze frame records */
      [!ENDIF!]
      [!IF "node:exists(DemExtendedDataClassRef)"!]
        [!WS "4"!]| ( (uint32)DEM_EDCLS_IDX_[!"name(as:ref(DemExtendedDataClassRef))"!] << DEM_EDCLASSIDX_OFFSET )
                                                     /* Extended data class */
      [!ELSE!][!//
        [!WS "4"!]| ( (uint32)DEM_EDCLS_NULL_IDX << DEM_EDCLASSIDX_OFFSET )
                                                  /* No extended data class */
      [!ENDIF!]
        [!WS "4"!]| ( (uint32)[!"name(as:ref(DemEventClass/DemOperationCycleRef))"!] << DEM_OPCYCLEIDX_OFFSET )
                                                      /* Operation cycle Id */
      [!IF "DemEventClass/DemAgingAllowed = 'true'"!]
        [!WS "4"!]| ( (uint32)[!"DemEventClass/DemAgingCycleCounterThreshold"!]U << DEM_AGINGCYCLES_OFFSET )
                                               /* Limit of DTC aging cycles */
      [!ELSE!][!//
        [!WS "4"!]| ( (uint32)0U << DEM_AGINGCYCLES_OFFSET )
                                        /* Aging disabled (AgingCycles = 0) */
      [!ENDIF!]
      [!WS "4"!]| ( (uint32)[!"DemEventKind"!] << DEM_EVENTKIND_OFFSET )
                                                       /* Event kind is [!"substring-after(DemEventKind, 'DEM_EVENT_KIND_')"!] */
      [!IF "node:exists(DemEventClass/DemEventFailureCycleRef)"!]
        [!VAR "EventFailureCycle" = "node:name(as:ref(DemEventClass/DemEventFailureCycleRef))"!]
        [!VAR "EventFailureThreshold" = "node:value(DemEventClass/DemEventFailureCycleCounterThreshold)"!]
        [!VAR "FailureClassIdx" = "0"!]
[!/*    search list of failure cycle configurations for existing combination */!][!//
        [!LOOP "text:split($FailureCycleCfgList, '#')"!]
          [!IF ". = concat($EventFailureCycle, '*', $EventFailureThreshold)"!]
[!/*        failure cycle configuration item found, stop searching */!][!//
            [!BREAK!]
          [!ENDIF!]
          [!VAR "FailureClassIdx" = "num:i($FailureClassIdx)+1"!]
        [!ENDLOOP!]
        [!WS "4"!]| ( (uint32)[!"num:i($FailureClassIdx)"!]U << DEM_EVENTFAILURECLASSIDX_OFFSET )
                                    /* Index of failure class configuration */
      [!ELSE!][!//
        [!WS "4"!]| ( (uint32)DEM_NO_FAULT_CONFIRMATION << DEM_EVENTFAILURECLASSIDX_OFFSET )
                                       /* Fault confirmation not configured */
      [!ENDIF!]
      [!WS "4"!],
[!/*  -----[EvConf3]----- */!][!//
      [!VAR "NumEnCond" = "0"!]
      [!LOOP "DemEventClass[node:exists(DemEnableConditionGroupRef)]"!]
        [!VAR "NumEnCond" = "$NumEnCond + count(as:ref(DemEnableConditionGroupRef)/DemEnableConditionRef/*)"!]
      [!ENDLOOP!]
      [!IF "$NumEnCond > 0"!]
        [!WS "4"!][!"substring(concat(string(num:integer($EnCondStartIdx)), 'U    '), 1, 6)"!][!WS "34"!]/* Enable condition start index */
        [!VAR "EnCondStartIdx" = "$EnCondStartIdx + $NumEnCond"!]
      [!ELSE!][!//
        [!WS "4"!]0U     /* No enable conditions (refer to 'Number of enable conditions') */
      [!ENDIF!]
      [!WS "4"!]| ( (uint32)[!"num:i($NumEnCond)"!]U << DEM_NUMENCOND_OFFSET )
                                             /* Number of enable conditions */
      [!IF "count(DemEventClass/DemIndicatorAttribute/*) != 0"!]
        [!WS "4"!]| ( (uint32)1U << DEM_INDICATORUSED_OFFSET )  /* Warning indicator used */
      [!ELSE!][!//
        [!WS "4"!]| ( (uint32)0U << DEM_INDICATORUSED_OFFSET )
                                               /* No warning indicator used */
      [!ENDIF!]
      [!IF "../../../../DemGeneral/DemEventDisplacementSupport = 'true'"!]
[!/*    configured priority value [1..256] */!][!//
        [!VAR "PriorityValue" = "DemEventClass/DemEventPriority"!]
[!/*    internally packed priority value [0..(N-1)] */!][!//
        [!VAR "PriorityValueIdx" = "0"!]
[!/*    get index of configured priority from priority list */!][!//
        [!LOOP "text:split($PriorityValueList, '#')"!]
          [!IF ". = $PriorityValue"!]
            [!WS "4"!]| ( (uint32)[!"num:integer($PriorityValueIdx)"!]U << DEM_PRIORITY_OFFSET )
                            /* Internal event priority (0 = most important) */
          [!ENDIF!]
          [!VAR "PriorityValueIdx" = "$PriorityValueIdx + 1"!]
        [!ENDLOOP!]
      [!ELSE!][!//
        [!WS "4"!]| ( (uint32)0U << DEM_PRIORITY_OFFSET )
                /* No event priority used as event displacement is disabled */
      [!ENDIF!]
      [!IF "DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceCounterBased'"!]
        [!WS "4"!]| ( (uint32)DEM_DEBOUNCE_COUNTERBASED << DEM_DEBOUNCEALGO_OFFSET )
                                          /* Counter based event debouncing */
        [!WS "4"!]| ( (uint32)[!"num:i($DebounceCtrIdx)"!]U << DEM_DEBOUNCEIDX_OFFSET )
                                /* Index of debouncing configuration/status */
        [!VAR "DebounceCtrIdx" = "$DebounceCtrIdx + 1"!]
      [!ELSEIF "DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceTimeBase'"!]
        [!WS "4"!]| ( (uint32)DEM_DEBOUNCE_TIMEBASED << DEM_DEBOUNCEALGO_OFFSET )
                                             /* Time based event debouncing */
        [!WS "4"!]| ( (uint32)[!"num:i($DebounceTimeIdx)"!]U << DEM_DEBOUNCEIDX_OFFSET )
                                /* Index of debouncing configuration/status */
        [!VAR "DebounceTimeIdx" = "$DebounceTimeIdx + 1"!]
      [!ELSEIF "DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceFrequencyBased'"!]
        [!WS "4"!]| ( (uint32)DEM_DEBOUNCE_FREQUENCYBASED << DEM_DEBOUNCEALGO_OFFSET )
                                        /* Frequency based event debouncing */
        [!WS "4"!]| ( (uint32)[!"num:i($DebounceFrqIdx)"!]U << DEM_DEBOUNCEIDX_OFFSET )
                                /* Index of debouncing configuration/status */
        [!VAR "DebounceFrqIdx" = "$DebounceFrqIdx + 1"!]
      [!ELSE!][!/* monitor internal debouncing */!][!//
        [!WS "4"!]| ( (uint32)DEM_DEBOUNCE_MONITOR << DEM_DEBOUNCEALGO_OFFSET )
                                       /* Monitor internal event debouncing */
        [!WS "4"!]| ( (uint32)0U << DEM_DEBOUNCEIDX_OFFSET )            /* Index not used */
      [!ENDIF!]
      [!IF "node:exists(DemDTCClassRef)"!]
        [!WS "4"!]| ( (uint32)[!"as:ref(DemDTCClassRef)/DemDTCFunctionalUnit"!]U << DEM_DTCFUNCTIONALUNIT_OFFSET )
                                                     /* DTC Functional Unit */
      [!ELSE!][!/* No DTC class configured */!][!//
        [!WS "4"!]| ( DEM_NO_DTC_FUNCTIONAL_UNIT << DEM_DTCFUNCTIONALUNIT_OFFSET )
                                                  /* No DTC Functional Unit */
      [!ENDIF!]
      [!IF "node:exists(DemEventClass/DemAgingCycleRef) and ( name(as:ref(DemEventClass/DemOperationCycleRef)) != name(as:ref(DemEventClass/DemAgingCycleRef)) )"!]
        [!WS "4"!]| ( (uint32)([!"name(as:ref(DemEventClass/DemAgingCycleRef))"!] - DEM_NUM_OPCYCLES) << DEM_AGINGCYCLEIDX_OFFSET )
                                                 /* relative Aging cycle Id */
      [!ELSE!][!/* No (different/separate) aging cycle reference configured */!][!//
        [!WS "4"!]| ( (uint32)DEM_NUM_AGINGCYCLES << DEM_AGINGCYCLEIDX_OFFSET )
                                                       /* No aging cycle Id */
      [!ENDIF!]
    [!WS "2"!]},
  [!ENDLOOP!]
};

#define DEM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*------------------[Enable conditions configuration]-----------------------*/

[!INDENT "0"!]
  #define DEM_START_SEC_VAR_INIT_UNSPECIFIED
  #include <MemMap.h>

  [!IF "count(DemGeneral/DemEnableCondition/*) > 0"!]
    /** \brief Enable condition status */
    VAR(boolean, DEM_VAR) Dem_EnCondStatus[DEM_NUM_ENABLECONDITIONS] =
    {
      [!VAR "MaxEnableConditionId" = "count(DemGeneral/DemEnableCondition/*) - 1"!]
      [!FOR "EnableConditionId" = "0" TO "$MaxEnableConditionId"!]
        [!SELECT "DemGeneral/DemEnableCondition/*[DemEnableConditionId = $EnableConditionId]"!]
          [!IF "DemEnableConditionStatus = 'true'"!]
            [!WS "2"!]TRUE,  [!//
          [!ELSE!]
            [!WS "2"!]FALSE, [!//
          [!ENDIF!]
          /* condition name: [!"name(.)"!] */
        [!ENDSELECT!]
      [!ENDFOR!]
    };
  [!ENDIF!]

  #define DEM_STOP_SEC_VAR_INIT_UNSPECIFIED
  #include <MemMap.h>

  #define DEM_START_SEC_CONST_UNSPECIFIED
  #include <MemMap.h>

  [!IF "count(DemConfigSet/*/DemEventParameter/*/DemEventClass[node:exists(DemEnableConditionGroupRef)]) > 0"!]
    /** \brief Enable Condition Id / Event Id mapping table */
    CONST(Dem_EnableConditionIdType, DEM_CONST)
      [!WS "2"!]Dem_EnCondLink[DEM_NUM_ENCOND_LINKS] =
    {
      [!LOOP "node:order(DemConfigSet/*/DemEventParameter/*[node:exists(DemEventClass/DemEnableConditionGroupRef)], 'DemEventId')"!]
        [!LOOP "as:ref(DemEventClass/DemEnableConditionGroupRef)/DemEnableConditionRef/*"!]
          [!WS "2"!][!"as:ref(.)/DemEnableConditionId"!]U, /* condition name: [!"name(as:ref(.))"!] */
        [!ENDLOOP!]
      [!ENDLOOP!]
    };
  [!ENDIF!]
[!ENDINDENT!]

/*------------------[Warning indicators configuration]----------------------*/

[!INDENT "0"!]
  [!IF "count(DemGeneral/DemIndicator/*) > 0"!]
    /** \brief Warning indicator configuration description table */
    CONST(Dem_IndicatorDescType, DEM_CONST)
      [!WS "2"!]Dem_IndicatorDesc[DEM_NUMBER_OF_INDICATORS] =
    {
      [!VAR "IndicatorLnkIdx" = "0"!]
      [!VAR "MaxIndicatorId" = "count(DemGeneral/DemIndicator/*) - 1"!]
      [!FOR "IndicatorId" = "0" TO "$MaxIndicatorId"!]
        [!VAR "IndicatorLnkNum" = "count(DemConfigSet/*/DemEventParameter/*/DemEventClass/DemIndicatorAttribute/*[as:ref(DemIndicatorRef)/DemIndicatorID = $IndicatorId])"!]
        [!INDENT "2"!]
          { /* warning indicator: [!"name(DemGeneral/DemIndicator/*[DemIndicatorID = $IndicatorId])"!] (= ID [!"num:i($IndicatorId)"!]) */
            [!WS "2"!]/* number: */ [!"num:i($IndicatorLnkNum)"!]U, /* index: */ [!"num:i($IndicatorLnkIdx)"!]U
          }[!//
          ,
        [!ENDINDENT!]
        [!VAR "IndicatorLnkIdx" = "$IndicatorLnkIdx + $IndicatorLnkNum"!]
      [!ENDFOR!]
    };
  [!ENDIF!]

  [!IF "count(DemGeneral/DemIndicator/*) > 0"!]
    #if (DEM_NUM_INDICATOR_LINKS != 0)
    /** \brief Warning indicator-link configuration link table */
    CONST(Dem_IndicatorLinkType, DEM_CONST)
      [!WS "2"!]Dem_IndicatorLink[DEM_NUM_INDICATOR_LINKS] =
    {
      [!INDENT "2"!]
        [!VAR "MaxIndicatorId" = "count(DemGeneral/DemIndicator/*) - 1"!]
        [!VAR "CtrDataIdx" = "0"!]
        [!FOR "IndicatorId" = "0" TO "$MaxIndicatorId"!]
          [!IF "count(DemConfigSet/*/DemEventParameter/*/DemEventClass/DemIndicatorAttribute/*[as:ref(DemIndicatorRef)/DemIndicatorID = $IndicatorId]) = 0"!]
            [!WS "2"!]/* no links */
          [!ENDIF!]
          [!LOOP "node:order(DemConfigSet/*/DemEventParameter/*, 'DemEventId')"!]
            [!LOOP "./DemEventClass/DemIndicatorAttribute/*[as:ref(./DemIndicatorRef)/DemIndicatorID = $IndicatorId != 0]"!]
              {
                [!INDENT "4"!]
                DemConf_DemEventParameter_[!"name(../../../.)"!],
                [!VAR "IndicatorBehaviour" = "../../../DemEventClass/DemIndicatorAttribute/*[as:ref(DemIndicatorRef)/DemIndicatorID = $IndicatorId]/DemIndicatorBehaviour"!]
                [!IF "$IndicatorBehaviour = 'DEM_INDICATOR_CONTINUOUS'"!]
                  DEM_INDICATOR_CONTINUOUS,
                [!ELSEIF "$IndicatorBehaviour = 'DEM_INDICATOR_BLINKING'"!][!//
                  DEM_INDICATOR_BLINKING,
                [!ELSE!][!//
                  DEM_INDICATOR_BLINK_CONT,
                [!ENDIF!]
                [!"num:integer($CtrDataIdx)"!]U, /* Index of IndicatorCycleCounter[] */
                [!VAR "CtrDataIdx" = "$CtrDataIdx + 1"!]
                [!VAR "HealingCycle" = "node:name(as:ref(./DemIndicatorHealingCycleRef))"!]
                [!VAR "HealingThreshold" = "node:value(./DemIndicatorHealingCycleCounterThreshold)"!]
                [!VAR "HealList" = "'#'"!]
                [!VAR "HealCtr" = "0"!]
                [!LOOP "../../../../*"!]
                  [!LOOP "./DemEventClass/DemIndicatorAttribute/*"!]
                    [!IF "not(contains($HealList, concat('#', node:name(as:ref(./DemIndicatorHealingCycleRef)),'*',node:value(./DemIndicatorHealingCycleCounterThreshold), '#')))"!]
                      [!VAR "HealList" = "concat($HealList, node:name(as:ref(./DemIndicatorHealingCycleRef)),'*',node:value(./DemIndicatorHealingCycleCounterThreshold), '#')"!]
                      [!VAR "HealCtr" = "$HealCtr + 1"!]
                      [!IF "text:match($HealingCycle, node:name(as:ref(./DemIndicatorHealingCycleRef))) and $HealingThreshold = node:value(./DemIndicatorHealingCycleCounterThreshold)"!]
                        [!VAR "HealingCycleCtrInfoIdx" = "$HealCtr - 1"!]
                        [!"num:integer($HealingCycleCtrInfoIdx)"!]U, /* Index of Dem_HealingCycleCounterInfo[] */
                      [!ENDIF!]
                    [!ENDIF!]
                  [!ENDLOOP!]
                [!ENDLOOP!]
                [!IF "./DemIndicatorFailureCycleSource = 'DEM_FAILURE_CYCLE_INDICATOR'"!]
                  [!VAR "FailureCycle" = "node:name(as:ref(./DemIndicatorFailureCycleRef))"!]
                  [!VAR "FailureThreshold" = "node:value(./DemIndicatorFailureCycleCounterThreshold)"!]
                [!ELSE!]
                  [!VAR "FailureCycle" = "node:name(as:ref(../../DemEventFailureCycleRef))"!]
                  [!VAR "FailureThreshold" = "node:value(../../DemEventFailureCycleCounterThreshold)"!]
                [!ENDIF!]
                [!VAR "FailList" = "'#'"!]
                [!VAR "FailCtr" = "0"!]
                [!LOOP "../../../../*"!]
                  [!LOOP "./DemEventClass/DemIndicatorAttribute/*"!]
                    [!IF "./DemIndicatorFailureCycleSource = 'DEM_FAILURE_CYCLE_INDICATOR'"!]
                      [!VAR "FailCyc" = "node:name(as:ref(./DemIndicatorFailureCycleRef))"!]
                      [!VAR "FailThrsld" = "node:value(./DemIndicatorFailureCycleCounterThreshold)"!]
                    [!ELSE!]
                      [!VAR "FailCyc" = "node:name(as:ref(../../DemEventFailureCycleRef))"!]
                      [!VAR "FailThrsld" = "node:value(../../DemEventFailureCycleCounterThreshold)"!]
                    [!ENDIF!]
                    [!IF "not(contains($FailList, concat('#', $FailCyc,'*', $FailThrsld, '#')))"!]
                      [!VAR "FailList" = "concat($FailList, $FailCyc,'*', $FailThrsld, '#')"!]
                      [!VAR "FailCtr" = "$FailCtr + 1"!]
                      [!IF "text:match($FailureCycle, $FailCyc) and $FailureThreshold = $FailThrsld"!]
                        [!VAR "FailingCycleCtrInfoIdx" = "$FailCtr - 1"!]
                        [!"num:integer($FailingCycleCtrInfoIdx)"!]U /* Index of Dem_FailureCycleCounterInfo[] */
                      [!ENDIF!]
                    [!ENDIF!]
                  [!ENDLOOP!]
                [!ENDLOOP!]
                [!ENDINDENT!]
              }[!//
            ,
            [!ENDLOOP!]
          [!ENDLOOP!]
        [!ENDFOR!]
      [!ENDINDENT!]
    };
    #endif
  [!ENDIF!]

  [!IF "count(DemGeneral/DemIndicator/*) > 0"!]
    #if (DEM_NUM_HEALINGCYC_COUNTER_INFO_ELEMENTS != 0)
    /** \brief Warning indicator configuration healing cycle-counter information table */
    CONST(Dem_CounterInfoType, DEM_CONST)
      [!WS "2"!]Dem_HealingCycleCounterInfo[DEM_NUM_HEALINGCYC_COUNTER_INFO_ELEMENTS] =
    {
      [!VAR "HealList"="'#'"!]  [!/* list of all unique healing cycle and threshold pair */!][!//
      [!LOOP "DemConfigSet/*[1]/DemEventParameter/*"!]
        [!LOOP "./DemEventClass/DemIndicatorAttribute/*"!]
          [!IF "not(contains($HealList, concat('#', node:name(as:ref(./DemIndicatorHealingCycleRef)),'*',node:value(./DemIndicatorHealingCycleCounterThreshold), '#')))"!]
            [!VAR "HealList" = "concat($HealList, node:name(as:ref(./DemIndicatorHealingCycleRef)),'*',node:value(./DemIndicatorHealingCycleCounterThreshold), '#')"!]
            [!INDENT "2"!]
            {
              [!WS "2"!][!"node:name(as:ref(./DemIndicatorHealingCycleRef))"!],
              [!WS "2"!][!"node:value(./DemIndicatorHealingCycleCounterThreshold)"!]U /* Healing threshold */
            },
            [!ENDINDENT!]
          [!ENDIF!]
        [!ENDLOOP!]
      [!ENDLOOP!]
    };
    #endif
  [!ENDIF!]

  [!IF "count(DemGeneral/DemIndicator/*) > 0"!]
    #if (DEM_NUM_FAILURECYC_COUNTER_INFO_ELEMENTS != 0)
    /** \brief Warning indicator configuration failure cycle-counter information table */
    CONST(Dem_CounterInfoType, DEM_CONST)
      [!WS "2"!]Dem_FailureCycleCounterInfo[DEM_NUM_FAILURECYC_COUNTER_INFO_ELEMENTS] =
    {
      [!VAR "FailList" = "'#'"!]  [!/* list of all unique failure cycle and threshold pair */!][!//
      [!LOOP "DemConfigSet/*[1]/DemEventParameter/*"!]
        [!LOOP "./DemEventClass/DemIndicatorAttribute/*"!]
          [!IF "./DemIndicatorFailureCycleSource = 'DEM_FAILURE_CYCLE_INDICATOR'"!]
            [!VAR "FailureCycle" = "node:name(as:ref(./DemIndicatorFailureCycleRef))"!]
            [!VAR "FailureThreshold" = "node:value(./DemIndicatorFailureCycleCounterThreshold)"!]
          [!ELSE!]
            [!VAR "FailureCycle" = "node:name(as:ref(../../DemEventFailureCycleRef))"!]
            [!VAR "FailureThreshold" = "node:value(../../DemEventFailureCycleCounterThreshold)"!]
          [!ENDIF!]
          [!IF "not(contains($FailList, concat('#', $FailureCycle,'*', $FailureThreshold, '#')))"!]
            [!VAR "FailList" = "concat($FailList, $FailureCycle,'*', $FailureThreshold, '#')"!]
            [!INDENT "2"!]
            {
              [!WS "2"!][!"$FailureCycle"!],
              [!WS "2"!][!"$FailureThreshold"!]U /* Failure threshold */
            },
            [!ENDINDENT!]
          [!ENDIF!]
        [!ENDLOOP!]
      [!ENDLOOP!]
    };
    #endif
  [!ENDIF!]

  [!IF "count(DemConfigSet/*/DemEventParameter/*[count(DemEventClass/DemIndicatorAttribute/*) != 0]) > 0"!]
    /** \brief List of all events which are configured for warning indicators */
    CONST(Dem_EventIdType, DEM_CONST)
      [!WS "2"!]Dem_EventIndicatorUsed[DEM_NUM_EVENT_INDICATOR_USED] =
    {
      [!VAR "NumEventindicatorConfigured" = "0"!]
      [!LOOP "DemConfigSet/*/DemEventParameter/*[count(DemEventClass/DemIndicatorAttribute/*) != 0]"!]
        [!WS "2"!][!"node:name(.)"!],
        [!VAR "NumEventindicatorConfigured" = "$NumEventindicatorConfigured + 1"!]
      [!ENDLOOP!]
    };
  [!ENDIF!]

  [!IF "count(DemConfigSet/*/DemEventParameter/*[DemEventKind = 'DEM_EVENT_KIND_BSW' and count(DemEventClass/DemIndicatorAttribute/*) != 0]) > 0"!]
    /** \brief List of BSW events which are configured for warning indicators */
    CONST(Dem_EventIdType, DEM_CONST)
      [!WS "2"!]Dem_BSWEventIndicatorUsed[DEM_NUM_BSWEVENT_INDICATOR_USED] =
    {
      [!VAR "NumEventindicatorConfigured" = "0"!]
      [!LOOP "DemConfigSet/*/DemEventParameter/*[count(DemEventClass/DemIndicatorAttribute/*) != 0 and DemEventKind = 'DEM_EVENT_KIND_BSW']"!]
        [!WS "2"!][!"node:name(.)"!],
        [!VAR "NumEventindicatorConfigured" = "$NumEventindicatorConfigured + 1"!]
      [!ENDLOOP!]
    };
  [!ENDIF!]
[!ENDINDENT!]

/*------------------[Debouncing configuration]------------------------------*/

[!VAR "NumPermFDCs" = "0"!][!//
[!LOOP "DemConfigSet/*/DemEventParameter/*[DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceCounterBased']"!]
  [!IF "node:value(./DemEventClass/DemDebounceAlgorithmClass/DemDebounceCounterStorage) = 'true'"!]
    [!VAR "NumPermFDCs" = "$NumPermFDCs + 1"!][!//
  [!ENDIF!]
[!ENDLOOP!]

[!INDENT "0"!]
  /** \brief Counter based debouncing configuration description table */
  [!IF "count(DemConfigSet/*/DemEventParameter/*[DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceCounterBased']) > 0"!]
    CONST(Dem_DebounceCounterCfgType, DEM_CONST)
      [!WS "2"!]Dem_DebounceCounterCfg[DEM_NUM_DEBOUNCE_COUNTER] =
    {
      [!LOOP "node:order(DemConfigSet/*/DemEventParameter/*[DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceCounterBased'], 'DemEventId')"!]
        [!SELECT "DemEventClass/DemDebounceAlgorithmClass"!]
          [!WS "2"!]{ /* event: [!"name(../..)"!] */
            [!WS "4"!][!"DemDebounceCounterDecrementStepSize"!]U,
            [!WS "4"!][!"DemDebounceCounterIncrementStepSize"!]U,
            [!WS "4"!][!"DemDebounceCounterPassedThreshold"!],
            [!WS "4"!][!"DemDebounceCounterFailedThreshold"!],
            [!WS "4"!][!"DemDebounceCounterJumpDownValue"!],
            [!WS "4"!][!"DemDebounceCounterJumpUpValue"!],
            [!IF "DemDebounceCounterJumpDown = 'true'"!]
              [!WS "4"!]TRUE,
            [!ELSE!]
              [!WS "4"!]FALSE,
            [!ENDIF!]
            [!IF "DemDebounceCounterJumpUp = 'true'"!]
              [!WS "4"!]TRUE,
            [!ELSE!]
              [!WS "4"!]FALSE,
            [!ENDIF!]
            [!IF "$NumPermFDCs > 0"!]
              [!IF "DemDebounceCounterStorage = 'true'"!]
                [!WS "4"!]TRUE
              [!ELSE!]
                [!WS "4"!]FALSE
              [!ENDIF!]
            [!ENDIF!]
          [!WS "2"!]}[!//
        [!ENDSELECT!]
        ,
      [!ENDLOOP!]
    };
  [!ELSE!]
    /* none */
  [!ENDIF!]

  /** \brief Time based debouncing configuration description table */
  [!IF "count(DemConfigSet/*/DemEventParameter/*[DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceTimeBase']) > 0"!]
    CONST(Dem_DebounceTimeCfgType, DEM_CONST)
      [!WS "2"!]Dem_DebounceTimeCfg[DEM_NUM_DEBOUNCE_TIME] =
    {
      [!LOOP "node:order(DemConfigSet/*/DemEventParameter/*[DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceTimeBase'], 'DemEventId')"!]
        [!SELECT "DemEventClass"!]
          [!WS "2"!]{ /* event: [!"name(..)"!] */
            [!/* TimeFailedThreshold and TimePassedThreshold are stored in main
               * function "ticks", not in ms! */!][!//
            [!WS "4"!][!//
            [!"num:i(ceiling((DemDebounceAlgorithmClass/DemDebounceTimeFailedThreshold * 1000) div (as:modconf('Dem')[1]/DemGeneral/DemTaskTime * 1000)))"!]U,
            [!WS "4"!][!//
            [!"num:i(ceiling((DemDebounceAlgorithmClass/DemDebounceTimePassedThreshold * 1000) div (as:modconf('Dem')[1]/DemGeneral/DemTaskTime * 1000)))"!]U,
            [!WS "4"!][!//
            DemConf_DemEventParameter_[!"name(..)"!]
          [!WS "2"!]},
        [!ENDSELECT!]
      [!ENDLOOP!]
    };
  [!ELSE!]
    /* none */
  [!ENDIF!]

  /** \brief Frequency based debouncing configuration description table */
  [!IF "count(DemConfigSet/*/DemEventParameter/*[DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceFrequencyBased']) > 0"!]
    CONST(Dem_DebounceFrequencyCfgType, DEM_CONST)
      [!WS "2"!]Dem_DebounceFrequencyCfg[DEM_NUM_DEBOUNCE_FREQUENCY] =
    {
      [!LOOP "node:order(DemConfigSet/*[1]/DemEventParameter/*[DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceFrequencyBased'], 'DemEventId')"!]
        [!SELECT "DemEventClass"!]
          [!WS "2"!]{ /* event: [!"name(..)"!] */
            [!/* DurationOfTimeWindow is stored in main function "ticks", not in ms! */!][!//
            [!WS "4"!][!//
            [!"num:i(ceiling((DemDebounceAlgorithmClass/DemDurationOfTimeWindow * 1000) div (as:modconf('Dem')[1]/DemGeneral/DemTaskTime * 1000)))"!]U,
            [!WS "4"!][!//
            [!"num:i(DemDebounceAlgorithmClass/DemThresholdForEventTestedFailed - 1)"!]U,
            [!WS "4"!][!//
            [!"num:i(DemDebounceAlgorithmClass/DemThresholdForEventTestedPassed - 1)"!]U,
            [!WS "4"!][!//
            DemConf_DemEventParameter_[!"name(..)"!]
          [!WS "2"!]},
        [!ENDSELECT!]
      [!ENDLOOP!]
    };
  [!ELSE!]
    /* none */
  [!ENDIF!]
[!ENDINDENT!]

/*------------------[Fault confirmation configuration]----------------------*/

[!INDENT "0"!]
  /** \brief Failure cycle and failure counter threshold configuration */
  [!IF "count(DemConfigSet/*/DemEventParameter/*[node:exists(DemEventClass/DemEventFailureCycleRef)]) > 0"!]
    CONST(Dem_EventFailureCycleCfgType, DEM_CONST)
      [!WS "2"!]Dem_EventFailureCycleCfg[DEM_NUM_FAILURECYCLES] =
    {
      [!LOOP "text:split($FailureCycleCfgList, '#')"!]
        [!VAR "EventFailureCycle" = "text:split(., '*')[position() = 1]"!]
        [!VAR "EventFailureThreshold" = "text:split(., '*')[position() = 2]"!]
        [!WS "2"!]{
          [!WS "4"!][!//
          [!"$EventFailureCycle"!],
          [!WS "4"!][!//
          [!"num:i($EventFailureThreshold)"!]U
        [!WS "2"!]},
      [!ENDLOOP!]
    };
  [!ELSE!]
    /* none */
  [!ENDIF!]
[!ENDINDENT!]

/*------------------[RTE / C-callback notifications]------------------------*/

[!INDENT "0"!]
  /** \brief Event callback property table */
  CONST(Dem_CbPropertyType, DEM_CONST) Dem_CbProperty[DEM_NUMBER_OF_EVENTS] =
  {
    [!WS "2"!]0U,                                             /* invalid event id entry */
    [!LOOP "node:order(DemConfigSet/*/DemEventParameter/*, 'DemEventId')"!]
      [!WS "2"!]/* event: [!"name(.)"!] */
      [!WS "4"!]
      [!IF "node:exists(DemCallbackInitMForE)"!]
        DEM_GEN_CB_INIT_MONITOR | [!//
      [!ELSE!]
        /* no init monitor callback */ [!//
      [!ENDIF!]
      [!IF "count(DemCallbackEventStatusChanged/*) > 0"!]
        [!IF "count(DemCallbackEventStatusChanged/*[not(node:exists(DemCallbackEventStatusChangedFnc))]) > 0"!]
          DEM_RTE_CB_TRIG_ON_EVST | [!//
        [!ENDIF!]
        [!IF "node:exists(DemCallbackEventStatusChanged/*/DemCallbackEventStatusChangedFnc)"!]
          DEM_C_CB_TRIG_ON_EVST | [!//
        [!ENDIF!]
      [!ELSE!]
        /* no trigger on event status callback(s) */ [!//
      [!ENDIF!]
      [!IF "node:exists(DemCallbackEventDataChanged)"!]
        [!IF "not(node:exists(DemCallbackEventDataChanged/DemCallbackEventDataChangedFnc))"!]
          DEM_RTE_CB_TRIG_ON_EVDAT | [!//
        [!ELSE!]
          DEM_C_CB_TRIG_ON_EVDAT | [!//
        [!ENDIF!]
      [!ELSE!]
        /* no trigger on event data changed callback */ [!//
      [!ENDIF!]
      [!IF "node:exists(DemCallbackClearEventAllowed)"!]
        [!IF "not(node:exists(DemCallbackClearEventAllowed/DemCallbackClearEventAllowedFnc))"!]
          DEM_RTE_CB_CLEAR_EVENT_ALLOWED | [!//
        [!ELSE!]
          DEM_C_CB_CLEAR_EVENT_ALLOWED | [!//
        [!ENDIF!]
      [!ELSE!]
        /* no clearEventAllowed callback */ [!//
      [!ENDIF!]
      [!IF "DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceMonitorInternal'"!]
        [!IF "node:exists(DemEventClass/DemDebounceAlgorithmClass/DemCallbackGetFDC)"!]
          DEM_GEN_CB_FAULT_DETECTION_CTR | [!//
        [!ELSE!]
          /* no fault detection counter callback */ [!//
        [!ENDIF!]
      [!ELSE!]
        /* DEM-internal fault detection counter */ [!//
      [!ENDIF!]
      DEM_ZERO_END,
    [!ENDLOOP!]
  };

  /*---[INIT_MONITOR]---*/

  #if (DEM_CB_TABLE_INIT_MONITOR_SIZE != 0U)
  /** \brief Generic callback function pointer table for InitMonitor */
  CONST(Dem_CbFuncPtrInitMonitorType, DEM_CONST)
    [!WS "2"!]Dem_CbFuncPtrInitMonitor[DEM_CB_TABLE_INIT_MONITOR_SIZE] =
  {
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackInitMForE"!]
      [!IF "node:exists(DemCallbackInitMForEFnc)"!]
        [!VAR "CallbackName" = "DemCallbackInitMForEFnc"!]
        [!WS "2"!]/* event: [!"name(../.)"!] */
      [!ELSE!]
        [!VAR "CallbackName" = "concat('Rte_Call_CBInitEvt_', name(../.), '_InitMonitorForEvent')"!]
      [!ENDIF!]
      [!WS "2"!]&[!"$CallbackName"!],
    [!ENDLOOP!]
  };
  #endif

  #if (DEM_LOOKUP_TABLE_INIT_MONITOR_SIZE != 0U)
  /** \brief Generic lookup table for InitMonitor */
  CONST(Dem_EventIdType, DEM_CONST)
    [!WS "2"!]Dem_CbLookupTableInitMonitor[DEM_LOOKUP_TABLE_INIT_MONITOR_SIZE] =
  {
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackInitMForE"!]
      [!WS "2"!]DemConf_DemEventParameter_[!"name(../.)"!], [!//
      [!IF "node:exists(DemCallbackInitMForEFnc)"!]
        /* -> [!"DemCallbackInitMForEFnc"!]() */
      [!ELSE!]
        /* -> Rte_Call */
      [!ENDIF!]
    [!ENDLOOP!]
  };
  #endif

  /*---[TRIGGER_ON_EVENT_STATUS]---*/

  #if (DEM_C_CALLBACK_TABLE_TRIG_ON_EVST_SIZE != 0U)
  /** \brief C-Callback function pointer table for TriggerOnEventStatus */
  CONST(Dem_CbFuncPtrTrigOnEvStType, DEM_CONST)
    [!WS "2"!]Dem_CbFuncPtrTrigOnEvSt[DEM_C_CALLBACK_TABLE_TRIG_ON_EVST_SIZE] =
  {
    [!VAR "CbNameList" = "'#'"!]  [!/* list of all unique callbacks */!][!//
    [!VAR "CCbCtr" = "0"!]        [!/* number of unique callbacks */!][!//
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackEventStatusChanged/*[node:exists(DemCallbackEventStatusChangedFnc)]"!]
      [!IF "not(contains($CbNameList, concat('#', DemCallbackEventStatusChangedFnc, '#')))"!]
        [!VAR "CallbackName" = "DemCallbackEventStatusChangedFnc"!]
        [!WS "2"!]&[!"$CallbackName"!],
        [!VAR "CbNameList" = "concat($CbNameList, $CallbackName, '#')"!]
        [!VAR "CCbCtr" = "$CCbCtr + 1"!]
      [!ENDIF!]
    [!ENDLOOP!]
  };
  #endif

  #if (DEM_C_LOOKUP_TABLE_TRIG_ON_EVST_SIZE != 0U)
  /** \brief C-Lookup table for TriggerOnEventStatus */
  CONST(Dem_CbLookupTableType, DEM_CONST)
    [!WS "2"!]Dem_CbLookupTableTrigOnEvSt[DEM_C_LOOKUP_TABLE_TRIG_ON_EVST_SIZE] =
  {
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackEventStatusChanged/*[node:exists(DemCallbackEventStatusChangedFnc)]"!]
      [!VAR "CallbackName" = "DemCallbackEventStatusChangedFnc"!]
      [!VAR "SearchStr" = "substring-after($CbNameList, '#')"!]
      [!/* finding the index of the callback in array Dem_CbFuncPtrTrigOnEvSt[] */!][!//
      [!FOR "FuncPtrTableIdx" = "0" TO "$CCbCtr"!]
        [!VAR "FuncNameStr" = "substring-before($SearchStr, '#')"!]
        [!VAR "SearchStr" = "substring-after($SearchStr, '#')"!][!//
        [!IF "$FuncNameStr = $CallbackName"!]
          [!/* current event-name and index to the array Dem_CbFuncPtrTrigOnEvSt[] */!][!//
          [!WS "2"!]{ DemConf_DemEventParameter_[!"name(../../.)"!], [!"num:integer($FuncPtrTableIdx)"!]U }, [!//
          /* -> [!"$CallbackName"!]() */
          [!BREAK!]
        [!ENDIF!]
      [!ENDFOR!]
    [!ENDLOOP!]
  };
  #endif

  #if (DEM_RTE_CALLBACK_TABLE_TRIG_ON_EVST_SIZE != 0U)
  /** \brief RTE-Callback function pointer table for TriggerOnEventStatus */
  CONST(Dem_RteFuncPtrTrigOnEvStType, DEM_CONST)
    [!WS "2"!]Dem_RteFuncPtrTrigOnEvSt[DEM_RTE_CALLBACK_TABLE_TRIG_ON_EVST_SIZE] =
  {
    [!LOOP "DemConfigSet/*/DemEventParameter/*"!]
      [!VAR "RunningNr" = "0"!]
      [!LOOP "DemCallbackEventStatusChanged/*[not(node:exists(DemCallbackEventStatusChangedFnc))]"!]
        [!VAR "RunningNr" = "num:integer($RunningNr + 1)"!]
        [!VAR "CallbackName" = "concat('Rte_Call_CBStatusEvt_', name(../../.), '_', $RunningNr, '_EventStatusChanged')"!]
        [!WS "2"!]&[!"$CallbackName"!],
      [!ENDLOOP!]
    [!ENDLOOP!]
  };
  #endif

  #if (DEM_RTE_LOOKUP_TABLE_TRIG_ON_EVST_SIZE != 0U)
  /** \brief RTE-Lookup table for TriggerOnEventStatus */
  CONST(Dem_EventIdType, DEM_CONST)
    [!WS "2"!]Dem_RteLookupTableTrigOnEvSt[DEM_RTE_LOOKUP_TABLE_TRIG_ON_EVST_SIZE] =
  {
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackEventStatusChanged/*[not(node:exists(DemCallbackEventStatusChangedFnc))]"!]
      [!WS "2"!]DemConf_DemEventParameter_[!"name(../../.)"!],
    [!ENDLOOP!]
  };
  #endif

  /*---[TRIGGER_ON_DTC_STATUS]---*/

  #if (DEM_CB_TABLE_TRIG_ON_DTCST_SIZE != 0U)
  /** \brief Generic callback function pointer table for TriggerOnDTCStatus */
  CONST(Dem_CbFuncPtrTrigOnDTCStType, DEM_CONST)
    [!WS "2"!]Dem_CbFuncPtrTrigOnDTCSt[DEM_CB_TABLE_TRIG_ON_DTCST_SIZE] =
  {
    [!VAR "RunningNr" = "0"!]
    [!LOOP "DemGeneral/DemCallbackDTCStatusChanged/*"!]
      [!IF "node:exists(DemCallbackDTCStatusChangedFnc)"!]
        [!VAR "CallbackName" = "DemCallbackDTCStatusChangedFnc"!]
      [!ELSE!]
        [!VAR "RunningNr" = "num:integer($RunningNr + 1)"!]
        [!VAR "CallbackName" = "concat('Rte_Call_CBStatusDTC_', $RunningNr, '_DTCStatusChanged')"!]
      [!ENDIF!]
      [!WS "2"!]&[!"$CallbackName"!],
    [!ENDLOOP!]
  };
  #endif

  /*---[TRIGGER_ON_EVENT_DATA_CHANGED]---*/

  #if (DEM_C_CB_TABLE_TRIG_ON_EVDAT_SIZE != 0U)
  /** \brief C-Callback function pointer table for TriggerOnEventDataChanged */
  CONST(Dem_CbFuncPtrTrigOnEvDatType, DEM_CONST)
    [!WS "2"!]Dem_CbFuncPtrTrigOnEvDat[DEM_C_CB_TABLE_TRIG_ON_EVDAT_SIZE] =
  {
    [!VAR "CbNameList" = "'#'"!]  [!/* list of all unique callbacks */!][!//
    [!VAR "CCbCtr" = "0"!]        [!/* number of unique callbacks */!][!//
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackEventDataChanged[node:exists(DemCallbackEventDataChangedFnc)]"!]
      [!IF "not(contains($CbNameList, concat('#', DemCallbackEventDataChangedFnc, '#')))"!]
        [!VAR "CallbackName" = "DemCallbackEventDataChangedFnc"!]
        [!WS "2"!]&[!"$CallbackName"!],
        [!VAR "CbNameList" = "concat($CbNameList, $CallbackName, '#')"!]
        [!VAR "CCbCtr" = "$CCbCtr + 1"!]
      [!ENDIF!]
    [!ENDLOOP!]
  };
  #endif

  #if (DEM_C_LOOKUP_TABLE_TRIG_ON_EVDAT_SIZE != 0U)
  /** \brief C-Lookup table for TriggerOnEventDataChanged */
  CONST(Dem_CbLookupTableType, DEM_CONST)
    [!WS "2"!]Dem_CbLookupTableTrigOnEvDat[DEM_C_LOOKUP_TABLE_TRIG_ON_EVDAT_SIZE] =
  {
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackEventDataChanged[node:exists(DemCallbackEventDataChangedFnc)]"!]
      [!VAR "CallbackName" = "DemCallbackEventDataChangedFnc"!]
      [!VAR "SearchStr" = "substring-after($CbNameList, '#')"!]
      [!/* finding the index of the callback in array Dem_CbFuncPtrTrigOnEvDat[] */!][!//
      [!FOR "FuncPtrTableIdx" = "0" TO "$CCbCtr"!]
        [!VAR "FuncNameStr" = "substring-before($SearchStr, '#')"!]
        [!VAR "SearchStr" = "substring-after($SearchStr, '#')"!]
        [!IF "$FuncNameStr = $CallbackName"!]
          [!/* current event-name and index to the array Dem_CbFuncPtrTrigOnEvDat[] */!][!//
          [!WS "2"!]{ DemConf_DemEventParameter_[!"name(../.)"!], [!"num:integer($FuncPtrTableIdx)"!]U }, [!//
          /* -> [!"$CallbackName"!]() */
          [!BREAK!]
        [!ENDIF!]
      [!ENDFOR!]
    [!ENDLOOP!]
  };
  #endif

  #if (DEM_RTE_CB_TABLE_TRIG_ON_EVDAT_SIZE != 0U)
  /** \brief RTE-Callback function pointer table for TriggerOnEventDataChanged */
  CONST(Dem_RteFuncPtrTrigOnEvDatType, DEM_CONST)
    [!WS "2"!]Dem_RteFuncPtrTrigOnEvDat[DEM_RTE_CB_TABLE_TRIG_ON_EVDAT_SIZE] =
  {
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackEventDataChanged[not(node:exists(DemCallbackEventDataChangedFnc))]"!]
      [!VAR "CallbackName" = "concat('Rte_Call_CBDataEvt_', name(../.), '_EventDataChanged')"!]
      [!WS "2"!]&[!"$CallbackName"!],
    [!ENDLOOP!]
  };
  #endif

  #if (DEM_RTE_LOOKUP_TABLE_TRIG_ON_EVDAT_SIZE != 0U)
  /** \brief RTE-Lookup table for TriggerOnEventDataChanged */
  CONST(Dem_EventIdType, DEM_CONST)
    [!WS "2"!]Dem_RteLookupTableTrigOnEvDat[DEM_RTE_LOOKUP_TABLE_TRIG_ON_EVDAT_SIZE] =
  {
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackEventDataChanged[not(node:exists(DemCallbackEventDataChangedFnc))]"!]
      [!WS "2"!]DemConf_DemEventParameter_[!"name(../.)"!],
    [!ENDLOOP!]
  };
  #endif

  /*---[CLEAR_EVENT_ALLOWED]---*/

  #if (DEM_C_CB_TABLE_CLEAR_EVENT_ALLOWED_SIZE != 0U)
  /** \brief C-Callback function pointer table for ClearEventAllowed */
  CONST(Dem_CbFuncPtrClearEventAllowedType, DEM_CONST)
    [!WS "2"!]Dem_CbFuncPtrClearEventAllowed[DEM_C_CB_TABLE_CLEAR_EVENT_ALLOWED_SIZE] =
  {
    [!VAR "CbNameList" = "'#'"!]  [!/* list of all unique callbacks */!][!//
    [!VAR "CCbCtr" = "0"!]        [!/* number of unique callbacks */!][!//
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackClearEventAllowed[node:exists(DemCallbackClearEventAllowedFnc)]"!]
      [!IF "not(contains($CbNameList, concat('#', DemCallbackClearEventAllowedFnc, '#')))"!]
        [!VAR "CallbackName" = "DemCallbackClearEventAllowedFnc"!]
        [!WS "2"!]&[!"$CallbackName"!],
        [!VAR "CbNameList" = "concat($CbNameList, $CallbackName, '#')"!]
        [!VAR "CCbCtr" = "$CCbCtr + 1"!]
      [!ENDIF!]
    [!ENDLOOP!]
  };
  #endif

  #if (DEM_C_LOOKUP_TABLE_CLEAR_EVENT_ALLOWED_SIZE != 0U)
  /** \brief C-Lookup table for ClearEventAllowed */
  CONST(Dem_CbLookupTableType, DEM_CONST)
    [!WS "2"!]Dem_CbLookupTableClearEventAllowed[DEM_C_LOOKUP_TABLE_CLEAR_EVENT_ALLOWED_SIZE] =
  {
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackClearEventAllowed[node:exists(DemCallbackClearEventAllowedFnc)]"!]
      [!VAR "CallbackName" = "DemCallbackClearEventAllowedFnc"!]
      [!VAR "SearchStr" = "substring-after($CbNameList, '#')"!]
      [!/* finding the index of the callback in array Dem_CbFuncPtrClearEventAllowed[] */!][!//
      [!FOR "FuncPtrTableIdx" = "0" TO "$CCbCtr"!]
        [!VAR "FuncNameStr" = "substring-before($SearchStr, '#')"!]
        [!VAR "SearchStr" = "substring-after($SearchStr, '#')"!]
        [!IF "$FuncNameStr = $CallbackName"!]
          [!/* current event-name and index to the array Dem_CbFuncPtrClearEventAllowed[] */!][!//
          [!WS "2"!]{ DemConf_DemEventParameter_[!"name(../.)"!], [!"num:integer($FuncPtrTableIdx)"!]U }, [!//
          /* -> [!"$CallbackName"!]() */
          [!BREAK!]
        [!ENDIF!]
      [!ENDFOR!]
    [!ENDLOOP!]
  };
  #endif

  #if (DEM_RTE_CB_TABLE_CLEAR_EVENT_ALLOWED_SIZE != 0U)
  /** \brief RTE-Callback function pointer table for ClearEventAllowed */
  CONST(Dem_CbFuncPtrClearEventAllowedType, DEM_CONST)
    [!WS "2"!]Dem_RteFuncPtrClearEventAllowed[DEM_RTE_CB_TABLE_CLEAR_EVENT_ALLOWED_SIZE] =
  {
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackClearEventAllowed[not(node:exists(DemCallbackClearEventAllowedFnc))]"!]
      [!VAR "CallbackName" = "concat('Rte_Call_CBClrEvt_', name(../.), '_ClearEventAllowed')"!]
      [!WS "2"!]&[!"$CallbackName"!],
    [!ENDLOOP!]
  };
  #endif

  #if (DEM_RTE_LOOKUP_TABLE_CLEAR_EVENT_ALLOWED_SIZE != 0U)
  /** \brief RTE-Lookup table for ClearEventAllowed */
  CONST(Dem_EventIdType, DEM_CONST)
    [!WS "2"!]Dem_RteLookupTableClearEventAllowed[DEM_RTE_LOOKUP_TABLE_CLEAR_EVENT_ALLOWED_SIZE] =
  {
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackClearEventAllowed[not(node:exists(DemCallbackClearEventAllowedFnc))]"!]
      [!WS "2"!]DemConf_DemEventParameter_[!"name(../.)"!],
    [!ENDLOOP!]
  };
  #endif

  /*---[GET_FDC]---*/

  #if (DEM_CB_TABLE_GET_FDC_SIZE != 0U)
  /** \brief Generic callback function pointer table for GetFaultDetectionCounter */
  CONST(Dem_CbFuncPtrGetFDCType, DEM_CONST)
    [!WS "2"!]Dem_CbFuncPtrGetFDC[DEM_CB_TABLE_GET_FDC_SIZE] =
  {
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemEventClass/DemDebounceAlgorithmClass[. = 'DemDebounceMonitorInternal']/DemCallbackGetFDC"!]
      [!IF "node:exists(DemCallbackGetFDCFnc)"!]
        [!VAR "CallbackName" = "DemCallbackGetFDCFnc"!]
        [!WS "2"!]/* event: [!"name(../../../.)"!] */
      [!ELSE!]
        [!VAR "CallbackName" = "concat('Rte_Call_CBFaultDetectCtr_', name(../../../.), '_GetFaultDetectionCounter')"!]
      [!ENDIF!]
      [!WS "2"!]&[!"$CallbackName"!],
    [!ENDLOOP!]
  };
  #endif

  #if (DEM_LOOKUP_TABLE_GET_FDC_SIZE != 0U)
  /** \brief Generic lookup table for GetFaultDetectionCounter */
  CONST(Dem_EventIdType, DEM_CONST)
    [!WS "2"!]Dem_CbLookupTableGetFDC[DEM_LOOKUP_TABLE_GET_FDC_SIZE] =
  {
    [!LOOP "DemConfigSet/*/DemEventParameter/*/DemEventClass/DemDebounceAlgorithmClass[. = 'DemDebounceMonitorInternal']/DemCallbackGetFDC"!]
      [!WS "2"!]DemConf_DemEventParameter_[!"name(../../../.)"!], [!//
      [!IF "node:exists(DemCallbackGetFDCFnc)"!]
        /* -> [!"DemCallbackGetFDCFnc"!]() */
      [!ELSE!]
        /* -> Rte_Call */
      [!ENDIF!]
    [!ENDLOOP!]
  };
  #endif
[!ENDINDENT!]

#define DEM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
