/**
 * \file
 *
 * \brief AUTOSAR Dem
 *
 * This file contains the implementation of the AUTOSAR
 * module Dem.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DEM_CFG_H)
#define DEM_CFG_H

/* This file contains all target independent public configuration declarations
 * for the AUTOSAR module Dem. */

[!AUTOSPACING!]
[!INCLUDE "Dem_Include.m"!]
[!//
/*==================[includes]==============================================*/
/* !LINKSTO dsn.Dem.IncludeStr,1 */

#include <Dem_IntErrId.h>        /* BSW Event ID symbols */
#include <Dem_IntEvtId.h>        /* SW-C Event ID symbols */

[!IF "(count(DemConfigSet/*/DemEventParameter/*[node:refexists(DemDTCClassRef) and node:value(as:ref(DemDTCClassRef)/DemImmediateNvStorage) = 'true']) > 0) or (DemGeneral/DemClearDTCBehavior != 'DEM_CLRRESP_VOLATILE')"!]
#include <NvM_Types.h>           /* NvM types */
[!ENDIF!]

/*==================[macros]================================================*/

/*------------------[DTC configuration]-------------------------------------*/

#if (defined DEM_NO_DTC)
#error DEM_NO_DTC already defined
#endif
/** \brief No DTC (neither UDS nor OBD) configured for event */
#define DEM_NO_DTC                 0U

#if (defined DEM_NO_DTC_FUNCTIONAL_UNIT)
#error DEM_NO_DTC_FUNCTIONAL_UNIT already defined
#endif
/** \brief No DTC functional unit configured for event */
#define DEM_NO_DTC_FUNCTIONAL_UNIT 0U

/*------------------[General]-----------------------------------------------*/

#if (defined DEM_VERSION_INFO_API)
#error DEM_VERSION_INFO_API already defined
#endif
/** \brief Switch, indicating if Version Info is activated for Dem */
[!IF "DemGeneral/DemVersionInfoApi = 'true'"!]
  [!WS "0"!]#define DEM_VERSION_INFO_API         STD_ON
[!ELSE!][!//
  [!WS "0"!]#define DEM_VERSION_INFO_API         STD_OFF
[!ENDIF!]

#if (defined DEM_DTC_STATUS_AVAILABILITY_MASK)
#error DEM_DTC_STATUS_AVAILABILITY_MASK already defined
#endif
/** \brief DTC status availability mask */
#define DEM_DTC_STATUS_AVAILABILITY_MASK    [!"num:integer(DemGeneral/DemDtcStatusAvailabilityMask)"!]U

#if (defined DEM_STATUS_BIT_STORAGE_TF)
#error DEM_STATUS_BIT_STORAGE_TF already defined
#endif
/** \brief Switch, indicating if permanent storage of the "TestFailed" status bit is activated */
[!IF "DemGeneral/DemStatusBitStorageTestFailed = 'true'"!]
  [!WS "0"!]#define DEM_STATUS_BIT_STORAGE_TF    STD_ON
[!ELSE!][!//
  [!WS "0"!]#define DEM_STATUS_BIT_STORAGE_TF    STD_OFF
[!ENDIF!]

#if (defined DEM_USE_PERMANENT_STORAGE)
#error DEM_USE_PERMANENT_STORAGE already defined
#endif
/** \brief Switch, indicating if Permanent storage of event memory entries in NvM is activated for
 ** Dem */
[!IF "count(DemGeneral/DemNvRamBlockId/*) > 0"!]
  [!WS "0"!]#define DEM_USE_PERMANENT_STORAGE    STD_ON
[!ELSE!][!//
  [!WS "0"!]#define DEM_USE_PERMANENT_STORAGE    STD_OFF
[!ENDIF!]

#if (defined DEM_OPCYC_NVSTORAGE)
#error DEM_OPCYC_NVSTORAGE already defined
#endif
/** \brief Switch, indicating if permanent storage of operation cycle entries
 ** to NVRAM is enabled */
[!IF "DemGeneral/DemOperationCycleStatusStorage = 'true'"!]
  [!WS "0"!]#define DEM_OPCYC_NVSTORAGE          STD_ON
[!ELSE!][!//
  [!WS "0"!]#define DEM_OPCYC_NVSTORAGE          STD_OFF
[!ENDIF!]

#if (defined DEM_OPCYCLESTATE)
#error DEM_OPCYCLESTATE already defined
#endif
/** \brief Macro to get the operation cycle states based on permanent storage
 ** of operation cycle entries to NVRAM */
[!IF "DemGeneral/DemOperationCycleStatusStorage = 'true'"!]
/* !LINKSTO Dem.OperationCyclePermStorage.FDC,1 */
  [!WS "0"!]#define DEM_OPCYCLESTATE             Dem_NvData.Dem_OpCycleState
[!ELSE!][!//
  [!WS "0"!]#define DEM_OPCYCLESTATE             Dem_OpCycleState
[!ENDIF!]

#if (defined DEM_NUM_DEBOUNCE_COUNTER_PERM)
#error DEM_NUM_DEBOUNCE_COUNTER_PERM already defined
#endif
/** \brief Number of counter debounced events with permanent debounce
 ** counter storage
 **/
[!VAR "NumPermFDCs" = "0"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*[DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceCounterBased']"!]
  [!IF "node:value(./DemEventClass/DemDebounceAlgorithmClass/DemDebounceCounterStorage) = 'true'"!]
    [!VAR "NumPermFDCs" = "$NumPermFDCs + 1"!][!//
  [!ENDIF!]
[!ENDLOOP!]
#define DEM_NUM_DEBOUNCE_COUNTER_PERM  [!"num:integer($NumPermFDCs)"!]U

#if (defined DEM_NVM_BLOCK_ID_INVALID)
#error DEM_NVM_BLOCK_ID_INVALID already defined
#endif
/** \brief NvM block Id does not exist as it is not configured */
#define DEM_NVM_BLOCK_ID_INVALID      0U

[!IF "count(DemGeneral/DemNvRamBlockId/*) > 0"!]
  [!INDENT "0"!]
    [!IF "(count(DemConfigSet/*/DemEventParameter/*[node:refexists(DemDTCClassRef) and node:value(as:ref(DemDTCClassRef)/DemImmediateNvStorage) = 'true']) > 0) or (DemGeneral/DemClearDTCBehavior != 'DEM_CLRRESP_VOLATILE')"!]
      #if (defined DEM_NVM_BLOCK_ID_DEFAULT)
      #error DEM_NVM_BLOCK_ID_DEFAULT already defined
      #endif
      /** \brief NvM block Id for Dem default block */
      #define DEM_NVM_BLOCK_ID_DEFAULT   [!//
      [!SELECT "DemGeneral/DemNvRamBlockId/*[name(.) = 'DEM_NVM_BLOCK_ID_DEFAULT']"!][!//
        [!"name(as:ref(DemNvRamBlockIdRef))"!]
      [!ENDSELECT!]

      #if (defined DEM_NVM_BLOCK_ID_PRIMARY)
      #error DEM_NVM_BLOCK_ID_PRIMARY already defined
      #endif
      /** \brief NvM block Id for Dem primary block */
      #define DEM_NVM_BLOCK_ID_PRIMARY   [!//
      [!SELECT "DemGeneral/DemNvRamBlockId/*[name(.) = 'DEM_NVM_BLOCK_ID_PRIMARY']"!][!//
        [!"name(as:ref(DemNvRamBlockIdRef))"!]
      [!ENDSELECT!]

      #if (defined DEM_NVM_BLOCK_ID_MIRROR)
      #error DEM_NVM_BLOCK_ID_MIRROR already defined
      #endif
      [!IF "count(DemConfigSet/*/DemEventParameter/*/DemEventClass[DemEventDestination = 'DEM_DTC_ORIGIN_MIRROR_MEMORY']) > 0"!]
        /** \brief NvM block Id for Dem mirror block */
        #define DEM_NVM_BLOCK_ID_MIRROR    [!//
        [!SELECT "DemGeneral/DemNvRamBlockId/*[name(.) = 'DEM_NVM_BLOCK_ID_MIRROR']"!][!//
          [!"name(as:ref(DemNvRamBlockIdRef))"!]
        [!ENDSELECT!]
      [!ELSE!]
        /** \brief Invalid block Id for Dem mirror block as no event using mirror memory is configured */
        #define DEM_NVM_BLOCK_ID_MIRROR    DEM_NVM_BLOCK_ID_INVALID
      [!ENDIF!]

      #if (defined DEM_NVM_BLOCK_ID_PERMANENT)
      #error DEM_NVM_BLOCK_ID_PERMANENT already defined
      #endif
      /** \brief NvM block Id for Dem permanent block
       ** This shall be implicitly derived from the existence of DemObdDTC */
      #define DEM_NVM_BLOCK_ID_PERMANENT DEM_NVM_BLOCK_ID_INVALID

      #if (defined DEM_NVM_BLOCK_ID_SECONDARY)
      #error DEM_NVM_BLOCK_ID_SECONDARY already defined
      #endif
      [!IF "count(DemConfigSet/*/DemEventParameter/*/DemEventClass[DemEventDestination = 'DEM_DTC_ORIGIN_SECONDARY_MEMORY']) > 0"!]
        /** \brief NvM block Id for Dem secondary block */
        #define DEM_NVM_BLOCK_ID_SECONDARY [!//
        [!SELECT "DemGeneral/DemNvRamBlockId/*[name(.) = 'DEM_NVM_BLOCK_ID_SECONDARY']"!][!//
          [!"name(as:ref(DemNvRamBlockIdRef))"!]
        [!ENDSELECT!]
      [!ELSE!]
        /** \brief Invalid block Id for Dem secondary block as no event using secondary memory is configured */
        #define DEM_NVM_BLOCK_ID_SECONDARY DEM_NVM_BLOCK_ID_INVALID
      [!ENDIF!]
    [!ELSE!][!/* no immediate non-volatile storage usage */!][!//
      #if (defined DEM_NVM_BLOCK_ID_DEFAULT)
      #error DEM_NVM_BLOCK_ID_DEFAULT already defined
      #endif
      /** \brief NvM block Id for Dem default block */
      #define DEM_NVM_BLOCK_ID_DEFAULT  [!//
      [!"name(as:ref(DemGeneral/DemNvRamBlockId/*[1]/DemNvRamBlockIdRef))"!]
    [!ENDIF!]
  [!ENDINDENT!]
[!ENDIF!]

#if (defined DEM_TYPE_OF_ORIGIN_SUPPORTED)
#error DEM_TYPE_OF_ORIGIN_SUPPORTED already defined
#endif
[!VAR "SupportedEventMemories" = "0"!]
[!/* check for primary origin */!][!//
[!IF "DemGeneral/DemMaxNumberEventEntryPrimary > 0"!]
  [!VAR "SupportedEventMemories" = "bit:bitset($SupportedEventMemories, 0)"!]
[!ENDIF!]
[!/* check for secondary origin */!][!//
[!IF "DemGeneral/DemMaxNumberEventEntrySecondary > 0"!]
  [!VAR "SupportedEventMemories" = "bit:bitset($SupportedEventMemories, 1)"!]
[!ENDIF!]
[!/* check for mirror origin */!][!//
[!IF "DemGeneral/DemMaxNumberEventEntryMirror > 0"!]
  [!VAR "SupportedEventMemories" = "bit:bitset($SupportedEventMemories, 2)"!]
[!ENDIF!]
[!/* check for permanent origin */!][!//
[!IF "DemGeneral/DemMaxNumberEventEntryPermanent > 0"!]
  [!VAR "SupportedEventMemories" = "bit:bitset($SupportedEventMemories, 3)"!]
[!ENDIF!]
/** \brief Supported origin types
 **
 ** This macro is only exported and not used internally.
 ** It is derived from ::DEM_MAX_NUMBER_EVENT_ENTRY_PRI,
 ** ::DEM_MAX_NUMBER_EVENT_ENTRY_SEC, ::DEM_MAX_NUMBER_EVENT_ENTRY_MIR, and
 ** ::DEM_MAX_NUMBER_EVENT_ENTRY_PER.
 **
 ** \note Macro name is still existing for compatibility reasons with AR2.1.
 **/
#define DEM_TYPE_OF_ORIGIN_SUPPORTED [!"num:integer($SupportedEventMemories)"!]U

#if (defined DEM_DEV_ERROR_DETECT)
#error DEM_DEV_ERROR_DETECT already defined
#endif
/* !LINKSTO Dem113,1 */
/** \brief Switch, indicating if development error detection is activated for
 ** Dem */
[!IF "DemGeneral/DemDevErrorDetect = 'true'"!]
  [!WS "0"!]#define DEM_DEV_ERROR_DETECT         STD_ON
[!ELSE!][!//
  [!WS "0"!]#define DEM_DEV_ERROR_DETECT         STD_OFF
[!ENDIF!]

#if (defined DEM_TYPE_OF_DTC_SUPPORTED)
#error DEM_TYPE_OF_DTC_SUPPORTED already defined
#endif
/** \brief Returned DTC translation format by Dem_GetTranslationType()
 **
 ** \note Macro name is conform to the vague definition in chapter 10 of SWS.
 **/
#define DEM_TYPE_OF_DTC_SUPPORTED    [!"DemGeneral/DemTypeOfDTCSupported"!]

#if (defined DEM_INCLUDE_RTE)
#error DEM_INCLUDE_RTE already defined
#endif
/** \brief Switch, indicating if RTE is available and can be used from Dem */
[!IF "DemGeneral/DemRteUsage = 'true'"!]
  [!WS "0"!]#define DEM_INCLUDE_RTE              STD_ON
[!ELSE!][!//
  [!WS "0"!]#define DEM_INCLUDE_RTE              STD_OFF
[!ENDIF!]

#if (defined DEM_DCM_ENABLED)
#error DEM_DCM_ENABLED already defined
#endif
/** \brief Switch, indicating whether Dcm module should be a part of Dem */
[!IF "DemGeneral/DemDcmUsage = 'true'"!]
  [!WS "0"!]#define DEM_DCM_ENABLED              STD_ON
[!ELSE!][!//
  [!WS "0"!]#define DEM_DCM_ENABLED              STD_OFF
[!ENDIF!]

#if (defined DEM_GET_SIZEOFEDRBYDTC_OPTIMIZATION)
#error DEM_GET_SIZEOFEDRBYDTC_OPTIMIZATION already defined
#endif
/** \brief Switch, indicating optimization for calculating size in
 ** Dem_GetSizeOfExtendedDataRecordByDTC() provided to the Dcm */
[!IF "DemGeneral/DemGetSizeOfExtendedDataRecordByDTCOptimization = 'true'"!]
  [!WS "0"!]#define DEM_GET_SIZEOFEDRBYDTC_OPTIMIZATION      STD_ON
[!ELSE!][!//
  [!WS "0"!]#define DEM_GET_SIZEOFEDRBYDTC_OPTIMIZATION      STD_OFF
[!ENDIF!]

#if (defined DEM_TRIGGER_FIM_REPORTS)
#error DEM_TRIGGER_FIM_REPORTS already defined
#endif
/** \brief Switch, indicating if notification to FiM is activated for Dem */
[!IF "DemGeneral/DemTriggerFiMReports = 'true'"!]
  [!WS "0"!]#define DEM_TRIGGER_FIM_REPORTS      STD_ON
[!ELSE!][!//
  [!WS "0"!]#define DEM_TRIGGER_FIM_REPORTS      STD_OFF
[!ENDIF!]

#if (defined DEM_TRIGGER_DCM_REPORTS)
#error DEM_TRIGGER_DCM_REPORTS already defined
#endif
/** \brief Switch, indicating if notification to Dcm is activated for Dem */
[!IF "DemGeneral/DemTriggerDcmReports = 'true'"!]
  [!WS "0"!]#define DEM_TRIGGER_DCM_REPORTS      STD_ON
[!ELSE!][!//
  [!WS "0"!]#define DEM_TRIGGER_DCM_REPORTS      STD_OFF
[!ENDIF!]


#if (defined DEM_USE_DYNAMIC_DTCS)
#error DEM_USE_DYNAMIC_DTCS already defined
#endif
/** \brief Macro for Enabling/Disabling dynamic DTC fetching */
[!IF "node:exists(DemGeneral/DemCalloutDynamicDTCFnc)"!]
  [!WS "0"!]#define DEM_USE_DYNAMIC_DTCS   STD_ON
[!ELSE!][!//
  [!WS "0"!]#define DEM_USE_DYNAMIC_DTCS   STD_OFF
[!ENDIF!]

#if (defined DEM_USE_EVENT_DISPLACEMENT)
#error DEM_USE_EVENT_DISPLACEMENT already defined
#endif
/** \brief Switch, indicating if event displacement support is activated for
 ** Dem */
[!IF "DemGeneral/DemEventDisplacementSupport = 'true'"!]
  [!WS "0"!]#define DEM_USE_EVENT_DISPLACEMENT   STD_ON
[!ELSE!][!//
  [!WS "0"!]#define DEM_USE_EVENT_DISPLACEMENT   STD_OFF
[!ENDIF!]

#ifndef DEM_STATUS_BIT_AGING_AND_DISPLACEMENT
/** \brief The "TestFailedSinceLastClear" status bits are reset to 0, if
 ** aging or displacement applies (like done for the "ConfirmedDTC" status
 ** bits) */
#define DEM_STATUS_BIT_AGING_AND_DISPLACEMENT  0x00U
#endif

#ifndef DEM_STATUS_BIT_NORMAL
/** \brief The aging and displacement has no impact on the
 ** "TestFailedSinceLastClear" status bits */
#define DEM_STATUS_BIT_NORMAL                  0x01U
#endif

#if (defined DEM_STATUS_BIT_HANDLING_TESTFAILEDSINCELASTCLEAR)
#error DEM_STATUS_BIT_HANDLING_TESTFAILEDSINCELASTCLEAR already defined
#endif
/** \brief Switch, indicating if the aging and displacement mechanism shall be
 ** applied to the "TestFailedSinceLastClear" status bits */
#define DEM_STATUS_BIT_HANDLING_TESTFAILEDSINCELASTCLEAR \
  [!"DemGeneral/DemStatusBitHandlingTestFailedSinceLastClear"!]

#if (defined DEM_CLRRESP_VOLATILE)
#error DEM_CLRRESP_VOLATILE already defined
#endif
#define DEM_CLRRESP_VOLATILE            0U

#if (defined DEM_CLRRESP_NONVOLATILE_TRIGGER)
#error DEM_CLRRESP_NONVOLATILE_TRIGGER already defined
#endif
#define DEM_CLRRESP_NONVOLATILE_TRIGGER 1U

#if (defined DEM_CLRRESP_NONVOLATILE_FINISH)
#error DEM_CLRRESP_NONVOLATILE_FINISH already defined
#endif
#define DEM_CLRRESP_NONVOLATILE_FINISH  2U

#if (defined DEM_CLEAR_DTC_BEHAVIOR)
#error DEM_CLEAR_DTC_BEHAVIOR already defined
#endif
/** \brief Behaviour of ClearDTC */
#define DEM_CLEAR_DTC_BEHAVIOR  [!"DemGeneral/DemClearDTCBehavior"!]

/*------------------[Freeze frame configuration]----------------------------*/

/* Symbolic names of configured freeze frame data IDs */
[!LOOP "DemGeneral/DemDidClass/*"!]
  [!INDENT "0"!]

    #if (defined DEM_FFS_DID_[!"name(.)"!])
    #error DEM_FFS_DID_[!"name(.)"!] already defined
    #endif
    [!IF "node:exists(DemDidIdentifier)"!]
      /** \brief Symbolic name of data ID [!"name(.)"!] */
      #define DEM_FFS_DID_[!"name(.)"!] [!"num:integer(DemDidIdentifier)"!]U
    [!ENDIF!]
  [!ENDINDENT!]
[!ENDLOOP!]

#if (defined DEM_FF_RECNUM_CALCULATED)
#error DEM_FF_RECNUM_CALCULATED already defined
#endif
/** \brief Freeze frame records will be numbered consecutive starting by 1 in
 ** their chronological order */
#define DEM_FF_RECNUM_CALCULATED     0x00U

#if (defined DEM_FF_RECNUM_CONFIGURED)
#error DEM_FF_RECNUM_CONFIGURED already defined
#endif
/** \brief Freeze frame records will be numbered based on the given
 ** configuration in their chronological order */
#define DEM_FF_RECNUM_CONFIGURED     0x01U

#if (defined DEM_FREEZE_FRAME_REC_NUMERATION_TYPE)
#error DEM_FREEZE_FRAME_REC_NUMERATION_TYPE already defined
#endif
/** \brief Type of assignment of freeze frame record numbers
 **
 ** This switch defines the type for assigning freeze frame record numbers for
 ** event-specific freeze frame records.
 **/
#define DEM_FREEZE_FRAME_REC_NUMERATION_TYPE  [!//
[!"DemGeneral/DemTypeOfFreezeFrameRecordNumeration"!]

#if (defined DEM_MAX_NUMBER_PRESTORED_FF)
#error DEM_MAX_NUMBER_PRESTORED_FF already defined
#endif
/** \brief Maximum number of available prestored freeze frames
 **
 ** \note Macro name is conform to the vague definition in chapter 10 of SWS.
 **/
#define DEM_MAX_NUMBER_PRESTORED_FF [!"num:integer(count(DemConfigSet/*/DemEventParameter/*[DemEventClass/DemFFPrestorageSupported = 'true']))"!]U

/*------------------[Extended data configuration]---------------------------*/

/* Symbolic names of configured extended data record numbers */
[!LOOP "DemGeneral/DemExtendedDataRecordClass/*"!]
  [!INDENT "0"!]

    #if (defined DEM_EDS_NUM_[!"name(.)"!])
    #error DEM_EDS_NUM_[!"name(.)"!] already defined
    #endif
    /** \brief Symbolic name of extended data [!"name(.)"!] */
    #define DEM_EDS_NUM_[!"name(.)"!] [!"num:integer(DemExtendedDataRecordNumber)"!]U
  [!ENDINDENT!]
[!ENDLOOP!]

/*------------------[Fault confirmation configurations]---------------------*/

#if (defined DEM_NUM_FAILURECYCLES)
#error DEM_NUM_FAILURECYCLES already defined
#endif
[!/* create unique list of all configured failure cycle counter cycles and thresholds */!][!//
[!VAR "FailureCycleCfgList" = "'#'"!]
[!VAR "EventFailureClassIdx" = "0"!]
[!LOOP "(DemConfigSet/*/DemEventParameter/*[node:exists(./DemEventClass/DemEventFailureCycleRef)])"!]
  [!VAR "EventFailureCycle" = "node:name(as:ref(DemEventClass/DemEventFailureCycleRef))"!]
  [!VAR "EventFailureThreshold" = "node:value(DemEventClass/DemEventFailureCycleCounterThreshold)"!]
  [!IF "not(contains($FailureCycleCfgList, concat('#', $EventFailureCycle, '*', $EventFailureThreshold, '#')))"!]
[!/* add non-existing cycle counter cycle and threshold to the list */!][!//
    [!VAR "FailureCycleCfgList" = "concat($FailureCycleCfgList, $EventFailureCycle, '*', $EventFailureThreshold, '#')"!]
[!/* increment counter of unique combinations */!][!//
    [!VAR "EventFailureClassIdx" = "$EventFailureClassIdx + 1"!]
  [!ENDIF!]
[!ENDLOOP!]
/** \brief Number of fault confirmation configurations */
#define DEM_NUM_FAILURECYCLES [!"num:integer($EventFailureClassIdx)"!]U

#if (defined DEM_EVENTFAILURECOUNTER_BUFFER_SIZE)
#error DEM_EVENTFAILURECOUNTER_BUFFER_SIZE already defined
#endif
/** \brief Maximal number of event failure counter buffer entries */
#define DEM_EVENTFAILURECOUNTER_BUFFER_SIZE [!"num:integer(DemGeneral/DemEventFailureCycleCounterBufferSize)"!]U

/*------------------[Memory size configuration]-----------------------------*/

/* !LINKSTO dsn.Dem.ErrorQueueOpt,1 */
#if (defined DEM_BSW_ERROR_BUFFER_SIZE)
#error DEM_BSW_ERROR_BUFFER_SIZE already defined
#endif
/** \brief Maximal number of error-queue entries
 **
 ** \note Macro name is conform to the vague definition in chapter 10 of SWS.
 **/
#define DEM_BSW_ERROR_BUFFER_SIZE [!"num:integer(DemGeneral/DemBswErrorBufferSize)"!]U

[!VAR "ListPrm" = "''"!]
[!VAR "ListSec" = "''"!]
[!VAR "ListMir" = "''"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*"!]
  [!VAR "IntValSize" = "0"!]
  [!IF "./DemEventClass/DemAgingAllowed = 'true'"!]
    [!VAR "IntValSize" = "$IntValSize + 1"!]
  [!ENDIF!]
  [!VAR "EDSize" = "0"!]
  [!IF "node:refexists(DemExtendedDataClassRef)"!]
    [!LOOP "as:ref(DemExtendedDataClassRef)/DemExtendedDataRecordClassRef/*"!]
      [!LOOP "as:ref(.)/DemDataElementClassRef/*"!]
[!/*    internal data elements will not be stored inside ED-area */!][!//
        [!IF "as:ref(.) != 'DemInternalDataElementClass'"!]
          [!VAR "EDSize" = "$EDSize + as:ref(.)/DemDataElementDataSize"!]
        [!ENDIF!]
      [!ENDLOOP!]
    [!ENDLOOP!]
  [!ENDIF!]
  [!VAR "FFSize" = "0"!]
  [!VAR "NumFFRecords" = "0"!]
  [!IF "node:refexists(DemFreezeFrameClassRef)"!]
    [!LOOP "as:ref(DemFreezeFrameClassRef)/DemDidClassRef/*"!]
      [!LOOP "as:ref(.)/DemDidDataElementClassRef/*"!]
[!/*    internal data elements can not be configured for FFs */!][!//
        [!VAR "FFSize" = "$FFSize + as:ref(.)/DemDataElementDataSize"!]
      [!ENDLOOP!]
    [!ENDLOOP!]
    [!IF "../../../../DemGeneral/DemTypeOfFreezeFrameRecordNumeration = 'DEM_FF_RECNUM_CALCULATED'"!]
      [!VAR "NumFFRecords" = "DemMaxNumberFreezeFrameRecords"!]
    [!ELSE!]
      [!VAR "NumFFRecords" = "count(as:ref(DemFreezeFrameRecNumClassRef)/DemFreezeFrameRecordNumber/*)"!]
    [!ENDIF!]
  [!ENDIF!]
  [!VAR "EntrySize" = "num:integer($IntValSize + $EDSize + $NumFFRecords * $FFSize)"!]
  [!INDENT "0"!]
    /* [!"DemEventClass/DemEventDestination"!] - Event [!"name(.)"!]: [!//
    [!IF "./DemEventClass/DemAgingAllowed = 'true'"!]
      aging counter value [!//
    [!ELSE!]
      no internal values [!//
    [!ENDIF!]
    + [!//
    [!IF "node:refexists(DemExtendedDataClassRef)"!]
      [!"name(as:ref(DemExtendedDataClassRef))"!] [!//
    [!ELSE!]
      no extended data [!//
    [!ENDIF!]
    + [!//
    [!IF "node:refexists(DemFreezeFrameClassRef)"!]
      [!"num:integer($NumFFRecords)"!] * [!"name(as:ref(DemFreezeFrameClassRef))"!] [!//
    [!ELSE!]
      no freeze frames [!//
    [!ENDIF!]
    = [!"$EntrySize"!] */
  [!ENDINDENT!]
  [!IF "DemEventClass/DemEventDestination = 'DEM_DTC_ORIGIN_PRIMARY_MEMORY'"!]
    [!VAR "ListPrm" = "concat($ListPrm, ' ', $EntrySize)"!]
  [!ENDIF!]
  [!IF "DemEventClass/DemEventDestination = 'DEM_DTC_ORIGIN_SECONDARY_MEMORY'"!]
    [!VAR "ListSec" = "concat($ListSec, ' ', $EntrySize)"!]
  [!ENDIF!]
  [!IF "DemEventClass/DemEventDestination = 'DEM_DTC_ORIGIN_MIRROR_MEMORY'"!]
    [!VAR "ListMir" = "concat($ListMir, ' ', $EntrySize)"!]
  [!ENDIF!]
[!ENDLOOP!]

/* entry data sizes (IntVal + ED + Num * FF) of prm memory: [!"$ListPrm"!] */
/* entry data sizes (IntVal + ED + Num * FF) of sec memory: [!"$ListSec"!] */
/* entry data sizes (IntVal + ED + Num * FF) of mir memory: [!"$ListMir"!] */

#if (defined DEM_SIZE_GATE_ENTRY_DATA_PRIMARY)
#error DEM_SIZE_GATE_ENTRY_DATA_PRIMARY already defined
#endif
/** \brief Gate entry size for primary memory
 **
 ** If immediate storage is enabled for any of the events in primary memory
 ** then this size is maximum of the highest primary event memory size
 ** considering IntVal, extended data and freeze frames.
 ** If maximum calculated size is 0, then default 1 is used.
 */
[!VAR "MaxEntrySize" = "1"!]
[!FOR "j" = "1" TO "count(text:split($ListPrm))"!]
  [!VAR "CurEntrySize" = "text:split($ListPrm)[position() = $j]"!]
  [!IF "$MaxEntrySize < $CurEntrySize"!]
    [!VAR "MaxEntrySize" = "$CurEntrySize"!]
  [!ENDIF!]
[!ENDFOR!]
#define DEM_SIZE_GATE_ENTRY_DATA_PRIMARY   [!"num:integer($MaxEntrySize)"!]U

#if (defined DEM_SIZE_GATE_ENTRY_DATA_SECONDARY)
#error DEM_SIZE_GATE_ENTRY_DATA_SECONDARY already defined
#endif
/** \brief Gate entry size for secondary memory
 **
 ** If immediate storage is enabled for any of the events in secondary memory
 ** then this size is maximum of the highest secondary event memory size
 ** considering IntVal, extended data and freeze frames.
 ** If maximum calculated size is 0, then default 1 is used.
 */
[!VAR "MaxEntrySize" = "1"!]
[!FOR "j" = "1" TO "count(text:split($ListSec))"!]
  [!VAR "CurEntrySize" = "text:split($ListSec)[position() = $j]"!]
  [!IF "$MaxEntrySize < $CurEntrySize"!]
    [!VAR "MaxEntrySize" = "$CurEntrySize"!]
  [!ENDIF!]
[!ENDFOR!]
#define DEM_SIZE_GATE_ENTRY_DATA_SECONDARY [!"num:integer($MaxEntrySize)"!]U

#if (defined DEM_SIZE_GATE_ENTRY_DATA_MIRROR)
#error DEM_SIZE_GATE_ENTRY_DATA_MIRROR already defined
#endif
/** \brief Gate entry size for mirror memory
 **
 ** If immediate storage is enabled for any of the events in mirror memory
 ** then this size is maximum of the highest mirror event memory size
 ** considering IntVal, extended data and freeze frames.
 ** If maximum calculated size is 0, then default 1 is used.
 */
[!VAR "MaxEntrySize" = "1"!]
[!FOR "j" = "1" TO "count(text:split($ListMir))"!]
  [!VAR "CurEntrySize" = "text:split($ListMir)[position() = $j]"!]
  [!IF "$MaxEntrySize < $CurEntrySize"!]
    [!VAR "MaxEntrySize" = "$CurEntrySize"!]
  [!ENDIF!]
[!ENDFOR!]
#define DEM_SIZE_GATE_ENTRY_DATA_MIRROR    [!"num:integer($MaxEntrySize)"!]U

#if (defined DEM_SIZE_GATE_ENTRY_DATA_PERMANENT)
#error DEM_SIZE_GATE_ENTRY_DATA_PERMANENT already defined
#endif
/** \brief Gate entry size for permanent memory
 **
 ** As this event memory type is not supported this shall be default 1.
 */
#define DEM_SIZE_GATE_ENTRY_DATA_PERMANENT 1U

#if (defined DEM_USE_IMMEDIATE_NV_STORAGE)
#error DEM_USE_IMMEDIATE_NV_STORAGE already defined
#endif
/** \brief Switch, indicating if Immediate storage of event memory entries in
 ** NvM is activated for Dem
 **
 ** This can be the case if any DTC is configured to be stored immediately, or
 ** if ClearDTC behavior is configured to non-volatile triggered or finished.
 */
#define DEM_USE_IMMEDIATE_NV_STORAGE  [!//
[!IF "((count(DemConfigSet/*/DemEventParameter/*[node:refexists(DemDTCClassRef) and node:value(as:ref(DemDTCClassRef)/DemImmediateNvStorage) = 'true']) > 0) and (count(DemGeneral/DemNvRamBlockId/*) > 0)) or contains(node:value(DemGeneral/DemClearDTCBehavior), 'NONVOLATILE')"!]
  STD_ON
[!ELSE!][!//
  STD_OFF
[!ENDIF!]

#if (defined DEM_NV_STORAGE_EMPTY_EVMEM_ENTRIES)
#error DEM_NV_STORAGE_EMPTY_EVMEM_ENTRIES already defined
#endif
/* !LINKSTO Dem.NvStorageEmptyEvMemEntries.Config,1 */
/** \brief Switch, indicating if empty event memory entries will be
 ** initialized in NVRAM
 **
 ** If the restoration of an event memory entry from NvM was not successful,
 ** this switch controls (esp. for ::DEM_USE_IMMEDIATE_NV_STORAGE = ON) if
 ** this entry is initialized with default values during next shutdown.
 */
#define DEM_NV_STORAGE_EMPTY_EVMEM_ENTRIES  [!//
[!IF "not(node:exists(DemGeneral/DemNvStorageEmptyEventMemoryEntries)) or (node:value(DemGeneral/DemNvStorageEmptyEventMemoryEntries) = 'true')"!]
  STD_ON
[!ELSE!][!//
  STD_OFF
[!ENDIF!]

#if (defined DEM_IMMEDIATE_CLEARED)
#error DEM_IMMEDIATE_CLEARED already defined
#endif
/** \brief Immediate storage configuration value: store CLEARED entries */
#define DEM_IMMEDIATE_CLEARED 0U

#if (defined DEM_IMMEDIATE_CHANGED)
#error DEM_IMMEDIATE_CHANGED already defined
#endif
/** \brief Immediate storage configuration value: store ALL entries */
#define DEM_IMMEDIATE_CHANGED 1U

#if (defined DEM_IMMEDIATE_NV_STORAGE_TYPE)
#error DEM_IMMEDIATE_NV_STORAGE_TYPE already defined
#endif
/** \brief Immediate storage configuration */
#define DEM_IMMEDIATE_NV_STORAGE_TYPE  [!//
[!IF "(count(DemConfigSet/*/DemEventParameter/*[node:refexists(DemDTCClassRef) and node:value(as:ref(DemDTCClassRef)/DemImmediateNvStorage) = 'true']) > 0) and (count(DemGeneral/DemNvRamBlockId/*) > 0)"!]
  DEM_IMMEDIATE_CHANGED
[!ELSE!][!//
  DEM_IMMEDIATE_CLEARED
[!ENDIF!]

#if (defined DEM_IMMEDIATE_NV_STORAGE_LIMIT)
#error DEM_IMMEDIATE_NV_STORAGE_LIMIT already defined
#endif
/** \brief Immediate NvM storage limit
 **
 ** Defines the maximum number of occurrences, a specific event memory entry
 ** is allowed, to be stored in NVRAM immediately.
 ** Its value is set to 0 when the immediate storage limit feature is
 ** disabled by the user. In that case, there is no limit for the storage to
 ** NVRAM and whenever the entry gets updated, it can be stored immediately
 ** to NVRAM if immediate storage is enabled for it.
 **/
#define DEM_IMMEDIATE_NV_STORAGE_LIMIT   [!//
[!IF "node:exists(DemGeneral/DemImmediateNvStorageLimit)"!]
  [!"num:integer(DemGeneral/DemImmediateNvStorageLimit)"!]U
[!ELSE!][!//
  0U
[!ENDIF!]

#if (defined DEM_ZERO_END)
#error DEM_ZERO_END already defined
#endif
/** \brief Zero value to be added to each size, whose involved patterns could
 ** be none */
#define DEM_ZERO_END                  0U

#if (defined DEM_SIZE_ENTRY_DATA_PRIMARY)
#error DEM_SIZE_ENTRY_DATA_PRIMARY already defined
#endif
/** \brief Size of dynamic primary entry data
 **
 ** The N (primary memory entry number) maximal sizes are added.
 **
 ** \note The DEM_ZERO_END is not necessary here, because primary event memory
 **       is always > 0.
 **/
#define DEM_SIZE_ENTRY_DATA_PRIMARY   \
  ([!//
[!FOR "i" = "1" TO "DemGeneral/DemMaxNumberEventEntryPrimary"!]
  [!VAR "MaxEntrySize" = "0"!]
  [!FOR "j" = "1" TO "count(text:split($ListPrm))"!]
    [!VAR "CurEntrySize" = "text:split($ListPrm)[position() = $j]"!]
    [!IF "$MaxEntrySize < $CurEntrySize"!]
      [!VAR "MaxEntrySize" = "$CurEntrySize"!]
    [!ENDIF!]
  [!ENDFOR!]
  [!INDENT "0"!]
    [!"num:integer($MaxEntrySize)"!]U[!//
    [!IF "$i < DemGeneral/DemMaxNumberEventEntryPrimary"!]
      [!WS!]+ [!//
    [!ENDIF!]
  [!ENDINDENT!]
  [!VAR "ListPrm" = "text:replace($ListPrm, $MaxEntrySize, '')"!]
[!ENDFOR!]
)

#if (defined DEM_SIZE_ENTRY_DATA_SECONDARY)
#error DEM_SIZE_ENTRY_DATA_SECONDARY already defined
#endif
/** \brief Size of dynamic secondary entry data
 **
 ** The N (secondary memory entry number) maximal sizes are added.
 **/
#define DEM_SIZE_ENTRY_DATA_SECONDARY \
  ([!//
[!FOR "i" = "1" TO "DemGeneral/DemMaxNumberEventEntrySecondary"!]
  [!VAR "MaxEntrySize" = "0"!]
  [!FOR "j" = "1" TO "count(text:split($ListSec))"!]
    [!VAR "CurEntrySize" = "text:split($ListSec)[position() = $j]"!]
    [!IF "$MaxEntrySize < $CurEntrySize"!]
      [!VAR "MaxEntrySize" = "$CurEntrySize"!]
    [!ENDIF!]
  [!ENDFOR!]
  [!WS "0"!][!"num:integer($MaxEntrySize)"!]U + [!//
  [!VAR "ListSec" = "text:replace($ListSec, $MaxEntrySize, '')"!]
[!ENDFOR!]
DEM_ZERO_END)

#if (defined DEM_SIZE_ENTRY_DATA_MIRROR)
#error DEM_SIZE_ENTRY_DATA_MIRROR already defined
#endif
/** \brief Size of dynamic mirror entry data
 **
 ** The N (mirror memory entry number) maximal sizes are added.
 **/
#define DEM_SIZE_ENTRY_DATA_MIRROR    \
  ([!//
[!FOR "i" = "1" TO "DemGeneral/DemMaxNumberEventEntryMirror"!]
  [!VAR "MaxEntrySize" = "0"!]
  [!FOR "j" = "1" TO "count(text:split($ListMir))"!]
    [!VAR "CurEntrySize" = "text:split($ListMir)[position() = $j]"!]
    [!IF "$MaxEntrySize < $CurEntrySize"!]
      [!VAR "MaxEntrySize" = "$CurEntrySize"!]
    [!ENDIF!]
  [!ENDFOR!]
  [!WS "0"!][!"num:integer($MaxEntrySize)"!]U + [!//
  [!VAR "ListMir" = "text:replace($ListMir, $MaxEntrySize, '')"!]
[!ENDFOR!]
DEM_ZERO_END)

#if (defined DEM_SIZE_ENTRY_DATA_PERMANENT)
#error DEM_SIZE_ENTRY_DATA_PERMANENT already defined
#endif
/** \brief Size of dynamic permanent entry data
 **
 ** The N (permanent memory entry number) maximal sizes are added.
 **/
#define DEM_SIZE_ENTRY_DATA_PERMANENT DEM_ZERO_END

/*------------------[Events configuration]----------------------------------*/

#if (defined DEM_MAX_NUMBER_EVENT_ENTRY_PRI)
#error DEM_MAX_NUMBER_EVENT_ENTRY_PRI already defined
#endif
/** \brief Maximum number of events which can be stored in the primary memory
 **
 ** \note Macro name is conform to the vague definition in chapter 10 of SWS.
 **/
#define DEM_MAX_NUMBER_EVENT_ENTRY_PRI [!"num:integer(DemGeneral/DemMaxNumberEventEntryPrimary)"!]U

#if (defined DEM_MAX_NUMBER_EVENT_ENTRY_SEC)
#error DEM_MAX_NUMBER_EVENT_ENTRY_SEC already defined
#endif
/** \brief Maximum number of events which can be stored in the secondary
 ** memory
 **
 ** \note Macro name is conform to the vague definition in chapter 10 of SWS.
 **/
#define DEM_MAX_NUMBER_EVENT_ENTRY_SEC [!"num:integer(DemGeneral/DemMaxNumberEventEntrySecondary)"!]U

#if (defined DEM_MAX_NUMBER_EVENT_ENTRY_MIR)
#error DEM_MAX_NUMBER_EVENT_ENTRY_MIR already defined
#endif
/** \brief Maximum number of events which can be stored in the mirror memory
 **
 ** \note Macro name is conform to the vague definition in chapter 10 of SWS.
 **/
#define DEM_MAX_NUMBER_EVENT_ENTRY_MIR [!"num:integer(DemGeneral/DemMaxNumberEventEntryMirror)"!]U

#if (defined DEM_MAX_NUMBER_EVENT_ENTRY_PER)
#error DEM_MAX_NUMBER_EVENT_ENTRY_PER already defined
#endif
/** \brief Maximum number of events which can be stored in the permanent memory
 **
 ** \note Macro name is conform to the vague definition in chapter 10 of SWS.
 **/
#define DEM_MAX_NUMBER_EVENT_ENTRY_PER [!"num:integer(DemGeneral/DemMaxNumberEventEntryPermanent)"!]U

#if (defined DEM_NUMBER_OF_EVENTS)
#error DEM_NUMBER_OF_EVENTS already defined
#endif
/** \brief Number of events which are present in the system
 **
 ** Calculated by number of event IDs listed in Dem_IntErrId.h and
 ** Dem_IntEvtId.h including ::DEM_EVENT_ID_INVALID.
 **
 ** \note Macro name is still existing for compatibility reasons with AR2.1.
 **/
#define DEM_NUMBER_OF_EVENTS [!"num:integer(count(DemConfigSet/*/DemEventParameter/*) + 1)"!]U

#if (defined DEM_NUM_SWC_EVENTS)
#error DEM_NUM_SWC_EVENTS already defined
#endif
/** \brief Number of SW-C events present in the system
 **/
#define DEM_NUM_SWC_EVENTS [!"num:integer(count(DemConfigSet/*/DemEventParameter/*[DemEventKind = 'DEM_EVENT_KIND_SWC']))"!]U

/*------------------[DTC groups configuration]------------------------------*/

/* Symbolic names of configured DTC groups */
[!INDENT "0"!]
  [!LOOP "node:order(DemGeneral/DemGroupOfDTC/*, './DemGroupDTCs')"!]
    [!VAR "PredefinedGroup" = "'true'"!]
    [!/* create group name only for commentation */!][!//
    [!VAR "CommentGroupName" = "name(.)"!]
    [!IF "    $CommentGroupName = 'DEM_DTC_GROUP_EMISSION_REL_DTCS'"!]
      [!VAR "CommentGroupName" = "'OBD-relevant'"!]
    [!ELSEIF "$CommentGroupName = 'DEM_DTC_GROUP_POWERTRAIN_DTCS'"!]
      [!VAR "CommentGroupName" = "'powertrain'"!]
    [!ELSEIF "$CommentGroupName = 'DEM_DTC_GROUP_CHASSIS_DTCS'"!]
      [!VAR "CommentGroupName" = "'chassis'"!]
    [!ELSEIF "$CommentGroupName = 'DEM_DTC_GROUP_BODY_DTCS'"!]
      [!VAR "CommentGroupName" = "'body'"!]
    [!ELSEIF "$CommentGroupName = 'DEM_DTC_GROUP_NETWORK_COM_DTCS'"!]
      [!VAR "CommentGroupName" = "'network communication'"!]
    [!ELSE!]
      [!VAR "PredefinedGroup" = "'false'"!]
      [!VAR "CommentGroupName" = "substring($CommentGroupName, 15)"!]
    [!ENDIF!]

    #if (defined DemConf_DemGroupOfDTC_[!"name(.)"!])
    #error DemConf_DemGroupOfDTC_[!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value of DTC group [!"$CommentGroupName"!] */
    #define DemConf_DemGroupOfDTC_[!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"translate(num:inttohex(DemGroupDTCs, 6), 'abcdef', 'ABCDEF')"!]U

    [!IF "$PredefinedGroup = 'true'"!]
      #if (defined [!"name(.)"!])
      #error [!"name(.)"!] already defined
      #endif
      /** \brief Export PREDEFINED symbolic name value (without prefix) as per definition of ::Dem_DTCGroupType in SWS */
      #define [!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"translate(num:inttohex(DemGroupDTCs, 6), 'abcdef', 'ABCDEF')"!]U

    [!ENDIF!]
    #if (!defined DEM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
    [!IF "$PredefinedGroup = 'false'"!]
      #if (defined [!"name(.)"!])
      #error [!"name(.)"!] already defined
      #endif
      /** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
      #define [!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"translate(num:inttohex(DemGroupDTCs, 6), 'abcdef', 'ABCDEF')"!]U

    [!ENDIF!]
    #if (defined Dem_[!"name(.)"!])
    #error Dem_[!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
    #define Dem_[!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"translate(num:inttohex(DemGroupDTCs, 6), 'abcdef', 'ABCDEF')"!]U
    #endif /* !defined DEM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
  [!ENDLOOP!]
[!ENDINDENT!]

/*------------------[Warning indicators configuration]----------------------*/

/* Symbolic names of configured warning indicator IDs */
[!LOOP "DemGeneral/DemIndicator/*"!]
  [!INDENT "0"!]

    #if (defined DemConf_DemIndicator_[!"name(.)"!])
    #error DemConf_DemIndicator_[!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value */
    #define DemConf_DemIndicator_[!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"num:integer(DemIndicatorID)"!]U

    #if (!defined DEM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
    #if (defined [!"name(.)"!])
    #error [!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
    #define [!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"num:integer(DemIndicatorID)"!]U

    #if (defined Dem_[!"name(.)"!])
    #error Dem_[!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) **/
    #define Dem_[!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"num:integer(DemIndicatorID)"!]U
    #endif /* !defined DEM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
  [!ENDINDENT!]
[!ENDLOOP!]


#if (defined DEM_NUMBER_OF_INDICATORS)
#error DEM_NUMBER_OF_INDICATORS already defined
#endif
/** \brief Number of warning indicators which are present in the system
 **
 ** \note Macro name is conform to the vague definition in chapter 10 of SWS.
 **/
#define DEM_NUMBER_OF_INDICATORS [!"num:integer(count(DemGeneral/DemIndicator/*))"!]U

[!VAR "NumEventindicatorConfigured" = "0"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*"!]
  [!IF "count(DemEventClass/DemIndicatorAttribute/*) != 0"!]
    [!VAR "NumEventindicatorConfigured" = "$NumEventindicatorConfigured + 1"!]
  [!ENDIF!]
[!ENDLOOP!]

#if (defined DEM_NUM_EVENT_INDICATOR_USED)
#error DEM_NUM_EVENT_INDICATOR_USED already defined
#endif
/** \brief Number of all events which are configured for warning indicator */
#define DEM_NUM_EVENT_INDICATOR_USED [!"num:integer($NumEventindicatorConfigured)"!]U

[!VAR "NumBSWEventindicatorConfigured" = "0"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*"!]
  [!IF "count(DemEventClass/DemIndicatorAttribute/*) != 0 and ./DemEventKind = 'DEM_EVENT_KIND_BSW'"!]
    [!VAR "NumBSWEventindicatorConfigured" = "$NumBSWEventindicatorConfigured + 1"!]
  [!ENDIF!]
[!ENDLOOP!]

#if (defined DEM_NUM_BSWEVENT_INDICATOR_USED)
#error DEM_NUM_BSWEVENT_INDICATOR_USED already defined
#endif
/** \brief Number of BSW events which are configured for warning indicator */
#define DEM_NUM_BSWEVENT_INDICATOR_USED [!"num:integer($NumBSWEventindicatorConfigured)"!]U

#if (defined DEM_NUM_INDICATOR_LINKS)
#error DEM_NUM_INDICATOR_LINKS already defined
#endif
/** \brief Number of warning indicator links */
#define DEM_NUM_INDICATOR_LINKS   [!"num:integer(count(DemConfigSet/*/DemEventParameter/*/DemEventClass/DemIndicatorAttribute/*))"!]U

/*------------------[Enable conditions configuration]-----------------------*/

/* Symbolic names of configured enable conditions */
[!LOOP "DemGeneral/DemEnableCondition/*"!]
  [!INDENT "0"!]

    #if (defined DemConf_DemEnableCondition_[!"name(.)"!])
    #error DemConf_DemEnableCondition_[!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value */
    #define DemConf_DemEnableCondition_[!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"num:integer(DemEnableConditionId)"!]U

    #if (!defined DEM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
    #if (defined [!"name(.)"!])
    #error [!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
    #define [!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"num:integer(DemEnableConditionId)"!]U

    #if (defined Dem_[!"name(.)"!])
    #error Dem_[!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) **/
    #define Dem_[!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"num:integer(DemEnableConditionId)"!]U
    #endif /* !defined DEM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
  [!ENDINDENT!]
[!ENDLOOP!]


#if (defined DEM_NUM_ENABLECONDITIONS)
#error DEM_NUM_ENABLECONDITIONS already defined
#endif
/** \brief Number of enable conditions */
#define DEM_NUM_ENABLECONDITIONS [!"num:integer(count(DemGeneral/DemEnableCondition/*))"!]U

/*------------------[Operation and aging cycles configuration]--------------*/

#ifndef DEM_PROCESS_OPCYC_STATE
/** \brief Operation cycle processing is triggered by START/END */
#define DEM_PROCESS_OPCYC_STATE        0x00U
#endif

#ifndef DEM_PROCESS_OPCYC_COUNTER
/** \brief Operation cycle processing is triggered by collecting an external
 ** counter value */
#define DEM_PROCESS_OPCYC_COUNTER      0x01U
#endif

#if (defined DEM_OPERATION_CYCLE_PROCESSING)
#error DEM_OPERATION_CYCLE_PROCESSING already defined
#endif
/** \brief Operation cycle processing mode
 **
 ** This processing mode defines whether the operation cycles are triggered by
 ** START/END reports via Dem_SetOperationCycleState(), or collecting an
 ** external counter value via Dem_SetOperationCycleCntValue().
 **
 ** \note Macro name is NOT present in SWS. This is as per bugzilla ticket
 ** #47189.
 **/
#define DEM_OPERATION_CYCLE_PROCESSING [!"DemGeneral/DemOperationCycleProcessing"!]

#ifndef DEM_PROCESS_AGINGCTR_INTERN
/** \brief Aging counter processing is triggered internally */
#define DEM_PROCESS_AGINGCTR_INTERN      0x00U
#endif

#ifndef DEM_PROCESS_AGINGCTR_EXTERN
/** \brief Aging counter processing is triggered by collecting an external
 ** counter value */
#define DEM_PROCESS_AGINGCTR_EXTERN      0x01U
#endif

#if (defined DEM_AGINGCYCLE_COUNTER_PROCESSING)
#error DEM_AGINGCYCLE_COUNTER_PROCESSING already defined
#endif
/** \brief Aging counter processing mode
 **
 ** This processing mode defines whether aging is triggered via
 ** Dem_SetOperationCycleState() and Dem_SetAgingCycleState(), or via an
 ** external counter value reported by Dem_SetAgingCycleCounterValue().
 **/
#define DEM_AGINGCYCLE_COUNTER_PROCESSING [!"DemGeneral/DemAgingCycleCounterProcessing"!]

#if (defined DEM_PROCESS_OCCCTR_CDTC)
#error DEM_PROCESS_OCCCTR_CDTC already defined
#endif
/** \brief Occurrence counter processing is triggered after the fault confirmation
 ** was successfull */
#define DEM_PROCESS_OCCCTR_CDTC    0x00U

#if (defined DEM_PROCESS_OCCCTR_TF)
#error DEM_PROCESS_OCCCTR_TF already defined
#endif
/** \brief Occurrence counter processing is triggered by the TestFailed bit only */
#define DEM_PROCESS_OCCCTR_TF      0x01U

#if (defined DEM_OCCURRENCE_COUNTER_PROCESSING)
#error DEM_OCCURRENCE_COUNTER_PROCESSING already defined
#endif
/** \brief Occurrence counter processing mode
 **
 ** This switch defines the consideration of the fault confirmation process for the
 ** occurrence counter.
 **/
#define DEM_OCCURRENCE_COUNTER_PROCESSING  [!//
[!"DemGeneral/DemOccurrenceCounterProcessing"!]

/* Symbolic names of configured operation cycle types */
[!VAR "OpCycleTypeNr" = "0"!]
[!LOOP "DemGeneral/DemOperationCycle/*"!]
  [!IF "DemOperationCycleType = 'DEM_OPCYC_IGNITION'"!]
    [!VAR "OpCycleTypeNr" = "0"!]
  [!ELSEIF "DemOperationCycleType = 'DEM_OPCYC_OBD_DCY'"!]
    [!VAR "OpCycleTypeNr" = "1"!]
  [!ELSEIF "DemOperationCycleType = 'DEM_OPCYC_POWER'"!]
    [!VAR "OpCycleTypeNr" = "2"!]
  [!ELSEIF "DemOperationCycleType = 'DEM_OPCYC_WARMUP'"!]
    [!VAR "OpCycleTypeNr" = "3"!]
  [!ELSEIF "DemOperationCycleType = 'DEM_OPCYC_TIME'"!]
    [!VAR "OpCycleTypeNr" = "4"!]
  [!ELSE!]
    [!VAR "OpCycleTypeNr" = "5"!]
  [!ENDIF!]
  [!INDENT "0"!]

    #if (defined [!"name(.)"!]_TYPE)
    #error [!"name(.)"!]_TYPE already defined
    #endif
    /** \brief Symbolic name of configured operation cycle type [!"DemOperationCycleType"!] for [!"name(.)"!].
    [!WS!]** Not used in the current implementation */
    #define [!"name(.)"!]_TYPE [!CALL "Indent", "Length" = "31 - string-length(name(.))"!][!"num:integer($OpCycleTypeNr)"!]U
  [!ENDINDENT!]
[!ENDLOOP!]

[!VAR "CycNameList" = "'#'"!]
[!VAR "CycCtr" = "0"!]
[!//
/* Symbolic names of configured operation cycles and failure cycles */
[!VAR "OpCycCtr" = "0"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*/DemEventClass"!]
  [!IF "not(contains($CycNameList, concat('#', name(as:ref(DemOperationCycleRef)), '#')))"!]
    [!INDENT "0"!]

      #if (defined [!"name(as:ref(DemOperationCycleRef))"!])
      #error [!"name(as:ref(DemOperationCycleRef))"!] already defined
      #endif
      /** \brief Symbolic name of operation cycle [!"name(as:ref(DemOperationCycleRef))"!] */
      #define [!"name(as:ref(DemOperationCycleRef))"!][!CALL "Indent", "Length" = "31 - string-length(name(as:ref(DemOperationCycleRef)))"!] [!"num:integer($CycCtr)"!]U
    [!ENDINDENT!]
    [!VAR "CycCtr" = "$CycCtr + 1"!]
    [!VAR "OpCycCtr" = "$OpCycCtr + 1"!]
    [!VAR "CycNameList" = "concat($CycNameList, name(as:ref(DemOperationCycleRef)), '#')"!]
  [!ENDIF!]
[!//
  [!IF "node:exists(DemEventFailureCycleRef) and not(contains($CycNameList, concat('#', name(as:ref(DemEventFailureCycleRef)), '#')))"!]
    [!INDENT "0"!]

      #if (defined [!"name(as:ref(DemEventFailureCycleRef))"!])
      #error [!"name(as:ref(DemEventFailureCycleRef))"!] already defined
      #endif
      /** \brief Symbolic name of operation cycle [!"name(as:ref(DemEventFailureCycleRef))"!] */
      #define [!"name(as:ref(DemEventFailureCycleRef))"!][!CALL "Indent", "Length" = "31 - string-length(name(as:ref(DemEventFailureCycleRef)))"!] [!"num:integer($CycCtr)"!]U
    [!ENDINDENT!]
    [!VAR "CycCtr" = "$CycCtr + 1"!]
    [!VAR "OpCycCtr" = "$OpCycCtr + 1"!]
    [!VAR "CycNameList" = "concat($CycNameList, name(as:ref(DemEventFailureCycleRef)), '#')"!]
  [!ENDIF!]
[!ENDLOOP!]

#if (defined DEM_NUM_OPCYCLES)
#error DEM_NUM_OPCYCLES already defined
#endif
/** \brief Number of operation cycles */
#define DEM_NUM_OPCYCLES [!"num:integer($OpCycCtr)"!]U

/* Symbolic names of configured aging cycles */
[!VAR "AgeCycCtr" = "0"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*/DemEventClass/DemAgingCycleRef"!]
  [!IF "not(contains($CycNameList, concat('#', name(as:ref(.)), '#')))"!]
    [!INDENT "0"!]

      #if (defined [!"name(as:ref(.))"!])
      #error [!"name(as:ref(.))"!] already defined
      #endif
      /** \brief Symbolic name of aging cycle [!"name(as:ref(.))"!] */
      #define [!"name(as:ref(.))"!][!CALL "Indent", "Length" = "31 - string-length(name(as:ref(.)))"!] [!"num:integer($CycCtr)"!]U
    [!ENDINDENT!]
    [!VAR "CycCtr" = "$CycCtr + 1"!]
    [!VAR "AgeCycCtr" = "$AgeCycCtr + 1"!]
    [!VAR "CycNameList" = "concat($CycNameList, name(as:ref(.)), '#')"!]
  [!ENDIF!]
[!ENDLOOP!]

#if (defined DEM_NUM_AGINGCYCLES)
#error DEM_NUM_AGINGCYCLES already defined
#endif
/** \brief Number of aging cycles
 **
 ** For these cycles, aging will not be performed based on the operation cycle
 ** of the respective event.
 **
 ** The value is always 0, if ::DEM_USE_AGING is switched off. */
#define DEM_NUM_AGINGCYCLES [!"num:integer($AgeCycCtr)"!]U

/*==================[type definitions]======================================*/

/*------------------[Dem_ConfigType]----------------------------------------*/

/** \brief This type of the external data structure shall contain the post
 **  build initialization data for the Dem.
 **
 ** \note Type is unused, as only pre-compile time support is implemented. */
typedef uint8 Dem_ConfigType;

/*==================[external function declarations]========================*/

[!IF "(count(DemConfigSet/*/DemEventParameter/*[node:refexists(DemDTCClassRef) and node:value(as:ref(DemDTCClassRef)/DemImmediateNvStorage) = 'true']) > 0) or (DemGeneral/DemClearDTCBehavior != 'DEM_CLRRESP_VOLATILE')"!]
extern void [!"DemGeneral/DemCallbackMemStackMainFuncTrigger"!](const NvM_BlockIdType BlockId);
[!ENDIF!]

[!IF "node:exists(DemGeneral/DemCalloutDynamicDTCFnc)"!]
extern uint32 [!"DemGeneral/DemCalloutDynamicDTCFnc"!](const uint16 EventId, uint32 DTC);
[!ENDIF!]

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

#define DEM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/** \brief Configuration structure */
extern CONST(Dem_ConfigType, DEM_CONST) [!"name(DemConfigSet/*[1])"!];

#define DEM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( DEM_CFG_H ) */
/*==================[end of file]===========================================*/
