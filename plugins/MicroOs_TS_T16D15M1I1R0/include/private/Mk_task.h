/* Mk_task.h - task configuration and handling
 *
 * This file contains definitions for configuration and handling of tasks.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_task.h 15821 2014-04-16 04:55:15Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 5.4 (required)
 *  A tag name shall be a unique identifier.
 *
 * Reason:
 *  False positive because the struct is used in a forward type definition.
*/

#ifndef MK_TASK_H
#define MK_TASK_H

#include <private/Mk_thread.h>
#include <private/Mk_interrupt.h>
#include <private/Mk_event.h>
#include <private/Mk_types_forward.h>
#include <public/Mk_error.h>

#ifndef MK_ASM

typedef struct mk_task_s mk_task_t;
typedef void (*mk_taskfunc_t)(void);

/* Deviation MISRA-1 */
struct mk_taskcfg_s
{
	mk_threadcfg_t threadCfg;
	mk_task_t *dynamic;
	mk_thread_t *thread;
	mk_stackelement_t *stack;
	mk_int_fast16_t maxActivations;
	mk_eventstatus_t *eventStatus;
};

/* !LINKSTO	Microkernel.Thread.Task.Config.State.ActivationCounter, 1
 * !doctype src
 */
struct mk_task_s
{
	mk_int_fast16_t activationCount;
};

extern const mk_objquantity_t MK_nTasks;
extern const mk_taskcfg_t * const MK_taskCfg;

/* Construction macros for task config structure.
 *
 * !LINKSTO         Microkernel.Thread.Task.ObjectType,1,
 * !                Microkernel.Thread.Task.Config.MaxActivation.Limit,1
 * !doctype         src
*/
#define MK_TASKCFG(dyn, thr, reg, name, stk, sp, ent, pm, ilvl, ie, fpu, hws, qpri, rpri, maxa, partIdx, tid,	\
				   exBgt, lkBgt, aid)																			\
{																												\
	MK_THREADCFG((reg), (name), (sp), (ent), (pm), (ilvl), (ie), (fpu), (hws), (qpri), (rpri), (partIdx),		\
										(tid), MK_OBJTYPE_TASK, (exBgt), (lkBgt), (aid)),						\
	(dyn),																										\
	(thr),																										\
	(stk),																										\
	(maxa),																										\
	MK_NULL																										\
}

#define MK_ETASKCFG(dyn, thr, reg, name, stk, sp, ent, pm, ilvl, ie, fpu, hws, qpri, rpri, eindex, partIdx,		\
					tid, exBgt, lkBgt, aid)																		\
{																												\
	MK_THREADCFG((reg), (name), (sp), (ent), (pm), (ilvl), (ie), (fpu), (hws), (qpri), (rpri), (partIdx),		\
															(tid), MK_OBJTYPE_TASK, (exBgt), (lkBgt), (aid)),	\
	(dyn),																										\
	(thr),																										\
	(stk),																										\
	1,																											\
	&MK_eventStatus[(eindex)]																					\
}

#endif

/* MK_ActivateTask() - internal kernel function to activate given task. Returns error code
*/
mk_errorid_t MK_ActivateTask(mk_objectid_t);

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
