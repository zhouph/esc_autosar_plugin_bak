/* Mk_foreign.h - foreign threads
 *
 * This file contains definitions for the microkernel's handling of foreign threads
 * which are used to run QM-OS functions and trusted functions.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_foreign.h 15821 2014-04-16 04:55:15Z masa8317 $
*/

#ifndef MK_FOREIGN_H
#define MK_FOREIGN_H

#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>

/* Function pointer type used for trusted functions.
*/
typedef void (*mk_trustedfunction_t)(mk_objectid_t, void *);

/* Prototype for the function that actually starts the foreign thread
*/
void MK_StartForeignThread(mk_objectid_t, mk_parametertype_t, mk_parametertype_t, mk_parametertype_t, mk_boolean_t);

/* Defines the number of configured trusted functions; this is configuration dependent
*/
extern const mk_objquantity_t MK_nTrustedFunctions;

/* MK_TRUSTEDFUNCTIONS_MIN is defined in Mk_syscallindex.h (must be public)
*/

/* Defines the last index interpreted as a trusted function index
*/
#define MK_TRUSTEDFUNCTIONS_MAX ((MK_TRUSTEDFUNCTIONS_MIN) + (MK_nTrustedFunctions - 1))

/* Pointer to an array of function pointers of all known trusted functions
*/
extern const mk_trustedfunction_t * const MK_trustedFunctionsPtr;

#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
