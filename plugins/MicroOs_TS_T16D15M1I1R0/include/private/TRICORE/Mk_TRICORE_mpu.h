/* Mk_TRICORE_mpu.h - Tricore MPU header
 *
 * This file selects the appropriate MPU header for the core.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_mpu.h 15947 2014-04-28 10:16:48Z masa8317 $
*/
#ifndef MK_TRICORE_MPU_H
#define MK_TRICORE_MPU_H

#include <public/TRICORE/Mk_TRICORE_cpu_characteristics.h>

#if MK_TRICORE_CORE == MK_TRICORE_TC16
#include <private/TRICORE/Mk_TRICORE_mpu_tc16.h>
#elif MK_TRICORE_CORE == MK_TRICORE_TC161
#include <private/TRICORE/Mk_TRICORE_mpu_tc161.h>
#else
#error "MK_TRICORE_CORE indicates a core for which there is no MPU support. Check your makefiles!"
#endif

/* MPU characteristics
*/
#define MK_HWMPUUSESLASTADDRESSOFREGION		0							/* Upper bound is first non-accessible addr. */
#define MK_HWMPUREGIONFIXEDBITS				MK_TRICORE_MPU_FIXED_BITS	/* Defined by core */
#define MK_HWN_REGION_DESCRIPTOR			MK_TRICORE_NDPR				/* Defined by core */

/* The following values are used to select the permission of a memory region.
 * READ and WRITE can be combined.
*/
#define MK_TRICORE_PERM_READ		0x01u
#define MK_TRICORE_PERM_WRITE		0x02u
#define MK_TRICORE_PERM_EXECUTE		0x04u
#define MK_TRICORE_PERM_RWMASK		(MK_TRICORE_PERM_READ|MK_TRICORE_PERM_WRITE)

/* The following defines are used in the configuration and are mapped to Tricore-specific values
*/
#define MK_MPERM_S_RX				(MK_TRICORE_PERM_READ|MK_TRICORE_PERM_EXECUTE)
#define MK_MPERM_S_RW				(MK_TRICORE_PERM_READ|MK_TRICORE_PERM_WRITE)
#define MK_MPERM_S_R				(MK_TRICORE_PERM_READ)
#define MK_MPERM_U_RX				(MK_TRICORE_PERM_READ|MK_TRICORE_PERM_EXECUTE)
#define MK_MPERM_U_RW				(MK_TRICORE_PERM_READ|MK_TRICORE_PERM_WRITE)
#define MK_MPERM_U_R				(MK_TRICORE_PERM_READ)

#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
