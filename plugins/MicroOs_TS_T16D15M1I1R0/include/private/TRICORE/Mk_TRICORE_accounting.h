/* Mk_TRICORE_accounting.h - private microkernel header
 *
 * This file contains the TRICORE support for thread accounting.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_accounting.h 16204 2014-05-21 14:41:23Z nibo2437 $
 *
 * !LINKSTO Microkernel.TRICORE.HwAccounting.Mk_TRICORE_accounting, 1
 * !doctype src
*/
#ifndef MK_TRICORE_ACCOUNTING_H
#define MK_TRICORE_ACCOUNTING_H

#include <private/TRICORE/Mk_TRICORE_stm.h>
#include <private/TRICORE/Mk_TRICORE_compiler.h>

/* TPS_TIMER is always cleared on kernel entry, therefore MK_HwUpdateExecutionTimeAlarm() needs to
 * be identical to MK_HwSetExecutionTimeAlarm().
*/
#define MK_HwUpdateExecutionTimeAlarm(x)			MK_HwSetExecutionTimeAlarm(x)

/* Low word of STM doubles as execution timer
*/
#define MK_HwReadExecutionTimer()					(MK_stm->stm_tim0)

/* Nothing to do: TPS_TIMER is always cleared on kernel entry
*/
#define MK_HwDispatcherClearExecutionTimeAlarm()	do { } while (0)

/* Convert ticks and write to TPS_TIMER0
*/
#define MK_HwSetExecutionTimeAlarm(x)				\
	do {											\
		MK_MTTPS_TIMER0(MK_ExecTickConvert(x));		\
	} while (0)

/* 32-bit unsigned subtraction. Wraps perfectly.
*/
#define MK_HwExecutionTimeDelta(now, prev)			( (now) - (prev) )

/* Activates the TPS (temporal protection system) module
*/
#define MK_HwInitExecutionTimer()					do { MK_EnableTps(); } while (0)


/* Value chosen to represent 5us at 100MHz - a bigger value might
 * make sense for slower clocks, but then it would need to depend
 * on the board file, which we need to avoid for the library
*/
#define MK_MIN_EXECUTIONTIME						500


/* Conversion function
*/
extern mk_uint32_t MK_ExecTickConvert(mk_uint32_t);

/* Enable TPS
*/
extern void MK_EnableTps(void);

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
