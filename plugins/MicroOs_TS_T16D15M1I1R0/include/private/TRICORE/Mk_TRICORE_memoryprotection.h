/* Mk_TRICORE_memoryprotection.h - private microkernel header (Tricore architecture)
 *
 * This file provides the interface between the hardware-independent microkernel
 * and the processor- (and perhaps derivative-) specific memory protection for
 * Tricore architecture processors.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_memoryprotection.h 18690 2015-03-05 12:13:57Z nibo2437 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 8.12 (required)
 *  When an array is declared with external linkage, its size shall be stated explicitly or defined
 *  implicitly by initialisation.
 *
 * Reason:
 *  Information hiding and independence of configuration. The library can be compiled prior to knowing
 *  how many elements the array will contain in a specific project.
 *
*/
#ifndef MK_TRICORE_MEMORYPROTECTION_H
#define MK_TRICORE_MEMORYPROTECTION_H

#include <public/Mk_basic_types.h>
#include <private/TRICORE/Mk_TRICORE_mpu.h>

#ifndef MK_ASM

/* The MPU cache
 * --------------
 * This array contains for every partition - minus the global and kernel partitions - a
 * pre-constructed image of all dynamic MPU region registers of bank 1.
 * It is filled at startup by MK_FillMpuCache() below.
*/
/* Deviation MISRA-1 */
extern mk_mpubounds_t MK_MpuRegCache[];

/* Number of static partitions in configuration.
*/
extern const mk_objquantity_t MK_nStaticPartitions;

/* MK_HwSetFirstVariableRegion() - not used on TRICORE
*/
#define MK_HwSetFirstVariableRegion(n)		do { /* Nothing */ } while (0)

/* MK_HwSetStaticPartition() writes the specified partition to the static portion of the MPU.
*/
void MK_HwSetStaticMemoryPartition(mk_objectid_t);

/* MK_HwSetDynamicPartition() writes the specified partition to the dynamic portion of the MPU.
*/
void MK_HwSetDynamicMemoryPartition(mk_objectid_t);

/* MK_FillMpuCache() is called during startup and fills the MPU cache
*/
void MK_FillMpuCache(void);

/* TRICORE has no architecture specific memory region member */
#define MK_HAVE_CPUFAMILYMEMORYREGION_T 0

#endif /* MK_ASM */

/* mk_errorid_t MK_HwCheckMemoryProtection(void)
 *
 * Check the static configuration of the MPU.
 * @return MK_eid_InvalidSelftestFeature since the check is not implemented.
 */
#define MK_HwCheckMemoryProtection()		(MK_eid_InvalidSelftestFeature)

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
