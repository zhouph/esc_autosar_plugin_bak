/* Mk_TRICORE_startup.h - private header file for start-up of a TRICORE-based board
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_startup.h 15947 2014-04-28 10:16:48Z masa8317 $
*/
#ifndef MK_TRICORE_STARTUP_H
#define MK_TRICORE_STARTUP_H

#include <public/Mk_basic_types.h>
#include <private/TRICORE/Mk_TRICORE_core.h>

/* The vector tables are declared in C as simple constants. They are never used as such.
 * The C code simply takes the address of the table.
*/
#ifndef MK_ASM
extern const mk_uint32_t MK_ExceptionTable;
extern const mk_uint32_t MK_StartupExceptionTable;
extern const mk_uint32_t MK_InterruptTable;
#endif

#ifndef MK_ASM
#if (MK_TRICORE_NCORES > 1)

/* For multi-core derivatives call the MK_CheckCoreId() function
*/
#define MK_CHECK_CORE_ID()		do { MK_CheckCoreId(); } while (0)

/* MK_targetCore
 * This designates the id of the core on which the microkernel should be executing.
*/
extern const mk_uint32_t MK_targetCore;

/* MK_CheckCoreId()
 * Function to check that the microkernel is running on the correct core
*/
void MK_CheckCoreId(void);

#else

/* Nothing to do on single-core derivatives
*/
#define MK_CHECK_CORE_ID()		do { /* nothing */ } while (0)

#endif
#endif

/* The boundaries of the memory reserved for CSA lists are declared in C as simple variables.
 * They are never used as such. The function that initialises the global CSA list uses these symbols
 * as the start and end of the block of RAM.
 *
 * The number of CSAs reserved for handling the "Free CSA list depleted" exception can be overridden by the
 * derivative header. The default value is 16, which should be more than enough to shut down the microkernel.
*/
#ifndef MK_ASM
extern mk_uint32_t MK_RSA_MK_Csa;
extern mk_uint32_t MK_RLA_MK_Csa;
#endif

#ifndef MK_CFG_NCSAEXTRA
#define MK_CFG_NCSAEXTRA	MK_U(16)
#endif

/* Minimum number of contexts required for our csa initialization code
 * This is the number of CSAs where we have exactly one CSA that can be
 * used without causing a CSA-list underflow exception.
*/
#define MK_NCSAMIN		(MK_CFG_NCSAEXTRA + MK_U(2))

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
