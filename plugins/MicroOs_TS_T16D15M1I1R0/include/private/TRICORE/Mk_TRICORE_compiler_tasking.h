/* Mk_TRICORE_compiler_tasking.h
 *
 * This file defines macros to permit the microkernel to be compiled with the Gnu compiler (gcc)
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_compiler_tasking.h 15947 2014-04-28 10:16:48Z masa8317 $
*/
#ifndef MK_TRICORE_COMPILER_TASKING_H
#define MK_TRICORE_COMPILER_TASKING_H

#include <private/TRICORE/Mk_TRICORE_core.h>

/* MK_DSYNC() executes a DSYNC instruction
*/
#define MK_DSYNC() \
	do {					\
		__asm(" dsync");	\
	} while (0)


/* MK_MTFCX() writes the content of the specified variable to the FCX register.
 *
 * The offset of the FCX register is verified in the preprocessor. This ensures that the inline
 * assembly language matches the assembly language files.
*/
#define MK_MTFCX(v) \
	do {																	\
		register mk_uint32_t MK_mtcrTmp = (v);								\
		__asm(" dsync\n mtcr  #0xfe38, %0\n isync" : : "d" (MK_mtcrTmp));	\
	} while (0)

#if MK_FCX != 0xfe38
#error "MK_FCX has a different value from the value used in MK_MTFCX!"
#endif


/* MK_MTLCX() writes the content of the specified variable to the LCX register.
 *
 * The offset of the LCX register is verified in the preprocessor. This ensures that the inline
 * assembly language matches the assembly language files.
*/
#define MK_MTLCX(v) \
	do {																	\
		register mk_uint32_t MK_mtcrTmp = (v);								\
		__asm(" dsync\n mtcr  #0xfe3c, %0\n isync" : : "d" (MK_mtcrTmp));	\
	} while (0)

#if MK_LCX != 0xfe3c
#error "MK_LCX has a different value from the value used in MK_MTLCX!"
#endif


/* MK_MTTPS_CON() writes the content of the specified variable to the TPS_CON register.
 *
 * The offset of the TPS_CON register is verified in the preprocessor. This ensures that the
 * inline assembly language matches the assembly language files.
*/
#define MK_MTTPS_CON(v) \
	do {																	\
		register mk_uint32_t MK_mtcrTmp = (v);								\
		__asm(" dsync\n mtcr  #0xe400, %0\n isync" : : "d" (MK_mtcrTmp));	\
	} while (0)

#if MK_TPS_CON != 0xe400
#error "MK_TPS_CON has a different value from the value used in MK_MTTPS_CON!"
#endif


/* MK_MTTPS_TIMER0() writes the content of the specified variable to the TPS_TIMER0 register.
 *
 * The offset of the TPS_TIMER0 register is verified in the preprocessor. This ensures that the
 * inline assembly language matches the assembly language files.
*/
#define MK_MTTPS_TIMER0(v) \
	do {																	\
		register mk_uint32_t MK_mtcrTmp = (v);								\
		__asm(" dsync\n mtcr  #0xe404, %0\n isync" : : "d" (MK_mtcrTmp));	\
	} while (0)

#if MK_TPS_TIMER0 != 0xe404
#error "MK_TPS_TIMER0 has a different value from the value used in MK_MTTPS_TIMER0!"
#endif


/* MK_GetCoreId() returns the core id of the core this is executed.
*/
#define MK_GetCoreId()		__mfcr(MK_CORE_ID)

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
