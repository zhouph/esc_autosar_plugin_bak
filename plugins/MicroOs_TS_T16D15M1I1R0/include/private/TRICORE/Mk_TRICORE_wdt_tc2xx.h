/* Mk_TRICORE_wdt_tc2xx.h - Tricore WDT module description for the "tc2xx" variant.
 *
 * This file contains a minimal description of Tricore's WDT (watchdoch) module (the
 * variant used on the TC2xx "Aurix" line of prcoessors). The WDT is part of the SCU module.
 *
 * This file only provides the addresses and and bitmasks of the SCU
 * registers that are actually used in the safety-related files of the microkernel.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_wdt_tc2xx.h 16177 2014-05-20 13:25:48Z nibo2437 $
*/
#ifndef MK_TRICORE_WDT_TC2XX_H
#define MK_TRICORE_WDT_TC2XX_H

#include <public/Mk_public_types.h>
#include <private/TRICORE/Mk_TRICORE_core.h>

/* These offsets were found to be identical in the documentation for
 * TC24x, TC26x, TC27x, and TC29x processors
*/

#define MK_MODBASE_WDTS		(MK_MODBASE_SCU + 0x0F0u)
#define MK_MODBASE_WDTX		(MK_MODBASE_SCU + 0x100u)


/* Bit fields in Watchdog Timer Control Register 0
*/
#define MK_ENDINIT			0x00000001u		/* End of initialisation control */
#define MK_WDTLCK			0x00000002u		/* Lock */
#define MK_WDTPW_LSB		0x00000004u		/* Least significant bit of password field */
#define MK_WDTPW0_MSK		0x000000FCu		/* Bits 7:2 of password field */
#define MK_WDTPW1_MSK		0x0000FF00u		/* Bits 15:8 of password field */
#define MK_WDTPW_MSK		(MK_WDTPW0_MSK|MK_WDTPW1_MSK)
#define MK_WDTREL_MSK		0xFFFF0000u		/* Reload value for watchdog timer */

#define MK_WDT_PAS			0x00000080u	/* Password auto-sequence status flag */
#define MK_WDT_TCS			0x00000100u	/* Timer check status flag */

#define MK_WDT_TIMEGUESS	0x00010000u     /* Estimated time for password computation */

#ifndef MK_ASM
struct mk_wdtcon_s
{
	mk_reg32_t	wdtxcon0;	/* Control Register 0 */
	mk_reg32_t	wdtxcon1;	/* Control Register 1 */
	mk_reg32_t	wdtxssr;	/* Status Register */
};

typedef struct mk_wdtcon_s mk_wdtcon_t;

/* MK_scu_wdtxcon
 *
 * Pointer to the watchdog registers. Will be optimized to be a macro, when the derivative
 * only has a single (logical) core.
*/
#if (MK_TRICORE_NCORES > 1)
extern mk_wdtcon_t * const MK_scu_wdtxcon;
#else
#define MK_scu_wdtxcon ((mk_wdtcon_t *)MK_MODBASE_WDTX)
#endif

/* MK_scu_wdtscon
 *
 * Pointer-like macro to the safety watchdog registers
*/
#define MK_scu_wdtscon	((mk_wdtcon_t *)MK_MODBASE_WDTS)

void MK_WriteSafetyEndinit(mk_uint32_t);

#endif /* ifndef MK_ASM */

#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
