/* Mk_types_forward.h - private types ("forward" declarations)
 *
 * This file declares the types for the resource and jobqueue structures without
 * declaring the structures, so that the types can be used as targets in pointer declarations.
 *
 * If the actual structure is needed the header file that declares the structure
 * should be included instead of this.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_types_forward.h 15821 2014-04-16 04:55:15Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 18.1 (required)
 *  All structure or union types shall be complete at the end of a translation unit.
 *
 * Reason:
 *  Information hiding. For example, the structure should be visible in a debugger but not in the
 *  application source code, or a pointer type is declared before the structure can be fully defined.
*/
#ifndef MK_TYPES_FORWARD_H
#define MK_TYPES_FORWARD_H

#ifndef MK_ASM
/* Deviation MISRA-1 <+7> */
typedef struct mk_jobqueue_s mk_jobqueue_t;
typedef struct mk_resource_s mk_resource_t;
typedef struct mk_memorypartition_s mk_memorypartition_t;
typedef struct mk_memoryregion_s mk_memoryregion_t;
typedef struct mk_memoryregionmap_s mk_memoryregionmap_t;
typedef struct mk_taskcfg_s mk_taskcfg_t;
/* Note: MISRA-C:2012 Dir 4.8 advises that incomplete declarations should be used where possible
*/
#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
