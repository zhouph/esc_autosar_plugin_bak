/* Mk_Cfg.h - microkernel configuration header
 *
 * This file is included by anything that needs to see the microkernel configuration
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_Cfg.h 18706 2015-03-05 21:02:03Z mist8519 $
*/

#ifndef MK_CFG_H
#define MK_CFG_H

#include <public/Mk_hwsel.h>

/* Include the headers that define the types of the variables that we declare here
*/
#include <public/Mk_basic_types.h>
#include <private/Mk_thread.h>

/* These includes provide the prototypes for optional interrupt or syscall handlers.
 */
#if MK_HWN_TICKER_CHANNELS > 0
#include <private/Mk_ticker.h>
#endif
#if (MK_HAS_SST == 1)
#include <private/Mk_sst_private.h>
#endif

/* This will be needed for the PSW values in the generated headers, among other things
*/
#include MK_HWSEL_PRV_CORE

/* Include the generated headers
*/
#include <Mk_gen_user.h>
#include <Mk_gen_config.h>

#include <Mk_board.h>

/* Include configuration patch file if told to do so
*/
#ifndef MK_INCLUDE_CONFIG_PATCH
#define MK_INCLUDE_CONFIG_PATCH		0
#endif
#if MK_INCLUDE_CONFIG_PATCH
#include <Mk_config_patch.h>
#endif

/* Declarations of configurable-sized arrays that are needed across modules
*/
#ifndef MK_ASM

#if MK_NTASKTHREADS > 0
extern mk_thread_t MK_taskThreads[MK_NTASKTHREADS];
#endif

#endif /* MK_ASM */
/* Time stamp clock factors
 * The board header file, or whatever provides the timestamp conversion macros, should
 * also indicate that the have been provided by defining the corresponding "HAS" macro to 1.
 * If the HAS macro is undefined, no ROM constant will be defined. This is achieved by defining
 * the macros to 0 if they are undefined.
*/
#ifndef MK_HAS_TIMESTAMPCLOCKFACTOR100U
#define MK_HAS_TIMESTAMPCLOCKFACTOR100U		0
#endif

#ifndef MK_HAS_TIMESTAMPCLOCKFACTOR10U
#define MK_HAS_TIMESTAMPCLOCKFACTOR10U		0
#endif

#ifndef MK_HAS_TIMESTAMPCLOCKFACTOR1U
#define MK_HAS_TIMESTAMPCLOCKFACTOR1U		0
#endif

/* Error check for the timestamp conversion macros
*/
#if MK_HAS_TIMESTAMPCLOCKFACTOR100U
#ifndef MK_TIMESTAMPCLOCKFACTOR100U
#error "Configuration claims to provide MK_TIMESTAMPCLOCKFACTOR100U but fails to do so. Please check the configuration."
#endif
#endif

#if MK_HAS_TIMESTAMPCLOCKFACTOR10U
#ifndef MK_TIMESTAMPCLOCKFACTOR10U
#error "Configuration claims to provide MK_TIMESTAMPCLOCKFACTOR10U but fails to do so. Please check the configuration."
#endif
#endif

#if MK_HAS_TIMESTAMPCLOCKFACTOR1U
#ifndef MK_TIMESTAMPCLOCKFACTOR1U
#error "Configuration claims to provide MK_TIMESTAMPCLOCKFACTOR1U but fails to do so. Please check the configuration."
#endif
#endif

/* Default configuration.
 * The following features are not controlled by the generator but
 * can be hand-configured by the system designer if desired.
 * If they are not defined, we assume the feature is enabled.
*/
#ifndef MK_ERRORINFO_ENABLED
#define MK_ERRORINFO_ENABLED		1
#endif

#ifndef MK_PROTECTIONINFO_ENABLED
#define MK_PROTECTIONINFO_ENABLED	1
#endif

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
