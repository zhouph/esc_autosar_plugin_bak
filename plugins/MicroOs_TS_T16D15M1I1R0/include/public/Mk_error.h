/* Mk_error.h
 *
 * This file contains definitions for the error codes returned by
 * kernel API functions, and the error codes used by the kernel internally,
 * since these are available via the error status API.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_error.h 17823 2014-12-08 13:39:44Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 9.3 (required)
 *  In an enumerator list, the "=" construct shall not be used to explicitly
 *  initialise members other than the first, unless all items are explicitly
 *  initialised.
 *
 * Reason:
 *  The explicitly initialized value in the end of the enumeration is used as
 *  a sentinel value to force the type to be 32-bit.
*/

#ifndef MK_ERROR_H
#define MK_ERROR_H

#include <public/Mk_public_types.h>
#include <public/Mk_syscallindex.h>
#include <public/Mk_exceptioninfo.h>

#ifndef MK_ASM

/* Internal error codes, made visible by the error-status API.
 *
 * The order is as follows:
 *	- MK_eid_NoError is first (enum value 0)
 *  - MK errors follow in alphabetical order as defined by sort(1)
 *  - OS errors (Mk_OsBla) follow in alphabetical order
 *	- MK_eid_Unknown is last
 *
 * This matches the order in the SDD (Table 17.3)!
*/
/* Deviation MISRA-1 */
enum mk_errorid_e
{
	MK_eid_NoError = 0,
	/*34567890123456789012345678901		31-character symbol length */
	MK_eid_AlignmentException,
	MK_eid_ArithmeticException,
	MK_eid_CallNestingException,
	MK_eid_DebugEventException,
	MK_eid_ExecutionBudgetExceeded,
	MK_eid_HardwareUnavailableException,
	MK_eid_IllegalInstructionException,
	MK_eid_InvalidForeignFunction,
	MK_eid_InvalidInterruptConfiguration,
	MK_eid_InvalidIsrId,
	MK_eid_InvalidMpuConfiguration,
	MK_eid_InvalidOsApplication,
	MK_eid_InvalidResourceId,
	MK_eid_InvalidRestartParameter,
	MK_eid_InvalidSelftestFeature,
	MK_eid_InvalidTaskId,
	MK_eid_InvalidTickerId,
	MK_eid_LockBudgetExceeded,
	MK_eid_MaxActivationsExceeded,
	MK_eid_MemoryProtectionException,
	MK_eid_ProcessorSpecificException,
	MK_eid_QmOsCallFromWrongContext,
	MK_eid_Quarantined,
	MK_eid_ResourceAlreadyOccupied,
	MK_eid_ResourceNestingLimitExceeded,
	MK_eid_ResourceNotOccupied,
	MK_eid_ResourceNotOccupiedByCaller,
	MK_eid_ResourceReleaseSequenceError,
	MK_eid_TaskIsNotExtended,
	MK_eid_TfCallFromWrongContext,
	MK_eid_ThreadIsNotATask,
	MK_eid_ThreadIsNotIdle,
	MK_eid_ThreadOccupiesResource,
	MK_eid_WatchdogException,
	MK_eid_WithoutPermission,

	/* The MK_eid_Os* error codes below this point are for use in the QM-OS.
	 * They should not be used in the microkernel.
	*/
	MK_eid_OsAlarmInUse,
	MK_eid_OsAlarmNotInQueue,
	MK_eid_OsAlarmNotInUse,
	MK_eid_OsCounterIsHw,
	MK_eid_OsDifferentCounters,
	MK_eid_OsImplicitSyncStartRel,
	MK_eid_OsIncrementZero,
	MK_eid_OsInterruptDisabled,
	MK_eid_OsInvalidAlarmId,
	MK_eid_OsInvalidApplication,
	MK_eid_OsInvalidCounterId,
	MK_eid_OsInvalidScheduleId,
	MK_eid_OsInvalidStartMode,
	MK_eid_OsNotChained,
	MK_eid_OsNotRunning,
	MK_eid_OsNotStopped,
	MK_eid_OsNotSyncable,
	MK_eid_OsParameterOutOfRange,
	MK_eid_OsPermission,
	MK_eid_OsQuarantined,
	MK_eid_OsScheduleTableNotIdle,
	MK_eid_OsUnknownSystemCall,
	MK_eid_OsWriteProtect,
	MK_eid_OsWrongContext,

	MK_eid_Unknown,					/* Must be the last of the sequentially-allocated error IDs */

	MK_eid_Sentinel = 0x7fffffff	/* Sentinel value to force the type to be 32-bit. */
};

typedef enum mk_errorid_e mk_errorid_t;

/* AUTOSAR-defined error codes
*/
enum mk_osekerror_e
{
	MK_E_OK = 0,					/* == E_OK, but we cannot define that! */
	E_OS_ACCESS = 1,				/* Value defined by OSEK/VDX */
	E_OS_CALLEVEL = 2,				/* Value defined by OSEK/VDX */
	E_OS_ID = 3,					/* Value defined by OSEK/VDX */
	E_OS_LIMIT = 4,					/* Value defined by OSEK/VDX */
	E_OS_NOFUNC = 5,				/* Value defined by OSEK/VDX */
	E_OS_RESOURCE = 6,				/* Value defined by OSEK/VDX */
	E_OS_STATE = 7,					/* Value defined by OSEK/VDX */
	E_OS_VALUE = 8,					/* Value defined by OSEK/VDX */

	E_OS_SERVICEID = 9,				/* Defined by AUTOSAR, value is implementation-defined */
	E_OS_ILLEGAL_ADDRESS = 10,		/* Defined by AUTOSAR, value is implementation-defined */
	E_OS_MISSINGEND = 11,			/* Defined by AUTOSAR, value is implementation-defined */
	E_OS_DISABLEDINT = 12,			/* Defined by AUTOSAR, value is implementation-defined */
	E_OS_STACKFAULT = 13,			/* Defined by AUTOSAR, value is implementation-defined */
	E_OS_PROTECTION_MEMORY = 14,	/* Defined by AUTOSAR, value is implementation-defined */
	E_OS_PROTECTION_TIME = 15,		/* Defined by AUTOSAR, value is implementation-defined */
	E_OS_PROTECTION_ARRIVAL = 16,	/* Defined by AUTOSAR, value is implementation-defined */
	E_OS_PROTECTION_LOCKED = 17,	/* Defined by AUTOSAR, value is implementation-defined */
	E_OS_PROTECTION_EXCEPTION = 18,	/* Defined by AUTOSAR, value is implementation-defined */

	MK_E_INTERNAL = 19,				/* Defined by EB */

	MK_E_THREADQUEUEEMPTY = 20,		/* Defined by EB. Shutdown only. */
	MK_E_PANIC = 21,				/* Defined by EB. Shutdown only. */

	MK_E_KILLED = 22,				/* Defined by EB. Request could not be performed, thread was killed */
	MK_E_ERROR = 23					/* Must be last */
};

typedef enum mk_osekerror_e mk_osekerror_t;

/* Protection-hook return values
 *
 * The EB-defined values have the MK_ prefix to prevent naming clashes.
*/
enum mk_protectionaction_e
{
	MK_PRO_CONTINUE = 0,			/* Continue and perform the request anyway. */
	PRO_IGNORE = 1,					/* Do not perform the offending request, but take no other action. */
	PRO_TERMINATETASKISR = 2,		/* Terminate the task or ISR, shutdown if offender is neither. */
	PRO_TERMINATEAPPL = 3,			/* Terminate the application */
	PRO_TERMINATEAPPL_RESTART = 4,	/* Terminate and restart the application */
	PRO_SHUTDOWN = 5,				/* Perform an orderly shut down */
	MK_PRO_TERMINATE = 6,			/* Terminate the offending thread no matter what it is */
	MK_PRO_TERMINATEALL = 7,		/* Terminate the offending thread and all queued instances of same
									   executable object
									*/
	MK_PRO_QUARANTINE = 8,			/* Terminate the offending thread, ensure that it cannot ever be restarted.
									   Implies MK_PRO_TERMINATEALL.
									*/
	MK_PRO_PANIC = 9,				/* Try to shutdown the microkernel */
	MK_PRO_PANICSTOP = 10,			/* Go to emergency stop state */
	MK_PRO_INVALIDACTION = 11,		/* Fallback value */

	MK_PRO_SENTINEL = 0x7fffffff	/* Sentinel value to force the type to be 32-bit. */
};

typedef enum mk_protectionaction_e mk_protectionaction_t;

/* Service IDs
 *
 * These are defined in terms of the system call indexes so that the OS system call index can
 * be used as a service ID without a translation table.
 * Service IDs that are not system calls come afterwards.
*/
/* !LINKSTO Microkernel.Interface.API.ServiceId, 1,
 * !        Microkernel.Interface.API.Indirect.QMOSCalls, 1
 * !doctype src
*/
enum mk_serviceid_e
{
	/* Microkernel system calls */
	MK_sid_TerminateSelf			= MK_SC_TerminateSelf,
	MK_sid_ActivateTask				= MK_SC_ActivateTask,
	MK_sid_ChainTask				= MK_SC_ChainTask,
	MK_sid_Schedule					= MK_SC_Schedule,
	MK_sid_GetResource				= MK_SC_GetResource,
	MK_sid_ReleaseResource			= MK_SC_ReleaseResource,
	MK_sid_Shutdown					= MK_SC_Shutdown,
	MK_sid_StartOs					= MK_SC_StartOs,
	MK_sid_SetEvent					= MK_SC_SetEvent,
	MK_sid_ClearEvent				= MK_SC_ClearEvent,
	MK_sid_WaitEvent				= MK_SC_WaitEvent,
	MK_sid_WaitGetClearEvent		= MK_SC_WaitGetClearEvent,
	MK_sid_GetTaskId				= MK_SC_GetTaskId,
	MK_sid_GetTaskState				= MK_SC_GetTaskState,
	MK_sid_GetIsrId					= MK_SC_GetIsrId,
	MK_sid_ReportError				= MK_SC_ReportError,
	MK_sid_StartForeignThread		= MK_SC_StartForeignThread,
	MK_sid_GetApplicationId			= MK_SC_GetApplicationId,
	MK_sid_TerminateApplication		= MK_SC_TerminateApplication,
	MK_sid_Selftest					= MK_SC_Selftest,
	MK_sid_DisableInterruptSource	= MK_SC_DisableInterruptSource,
	MK_sid_EnableInterruptSource	= MK_SC_EnableInterruptSource,
	MK_sid_InitTicker				= MK_SC_InitTicker,

	/* Threaded QM-OS calls */
	MK_sid_SetRelAlarm				= MK_SC_SetRelAlarm,
	MK_sid_SetAbsAlarm				= MK_SC_SetAbsAlarm,
	MK_sid_CancelAlarm				= MK_SC_CancelAlarm,
	MK_sid_IncrementCounter			= MK_SC_IncrementCounter,
	MK_sid_StartScheduleTable		= MK_SC_StartScheduleTable,
	MK_sid_StartScheduleTableSync	= MK_SC_StartScheduleTableSync,
	MK_sid_NextScheduleTable		= MK_SC_NextScheduleTable,
	MK_sid_StopScheduleTable		= MK_SC_StopScheduleTable,
	MK_sid_SyncScheduleTable		= MK_SC_SyncScheduleTable,
	MK_sid_SetScheduleTableAsync	= MK_SC_SetScheduleTableAsync,
	MK_sid_SimTimerAdvance			= MK_SC_SimTimerAdvance,
	MK_sid_GetCounterValue			= MK_SC_GetCounterValue,
	MK_sid_GetAlarm					= MK_SC_GetAlarm,

	MK_sid_OS_TerminateApplication	= MK_SC_OS_TerminateApplication,
	MK_sid_OS_StartOs				= MK_SC_OS_StartOs,
	MK_sid_UnconfiguredOsCall		= MK_NSYSCALL_TOTAL,		/* Always the highest of the assigned values! */

	/* OS pseudo-services that are not directly called. */
	MK_sid_StartScheduleTableAbs	= (MK_NSYSCALL_TOTAL+1),	/* Variable SID for StartScheduleTable */
	MK_sid_StartScheduleTableRel	= (MK_NSYSCALL_TOTAL+2),	/* Variable SID for StartScheduleTable */
	MK_sid_KillAlarm				= (MK_NSYSCALL_TOTAL+3),
	MK_sid_RunSchedule				= (MK_NSYSCALL_TOTAL+4),

	/* OS library functions */
	MK_sid_GetAlarmBase				= (MK_NSYSCALL_TOTAL+5),
	MK_sid_GetScheduleTableStatus	= (MK_NSYSCALL_TOTAL+6),

	/* Microkernel library functions */
	MK_sid_GetEvent					= (MK_NSYSCALL_TOTAL+7),

	/* Microkernel pseudo-services */
	MK_sid_ExceptionHandling		= (MK_NSYSCALL_TOTAL+8),
	MK_sid_UnknownService			= (MK_NSYSCALL_TOTAL+9),	/* Used for out-of-range syscall index */
	MK_sid_InvalidServiceId			= (MK_NSYSCALL_TOTAL+10),	/* Never reported by microkernel */
	MK_sid_nsid						= (MK_NSYSCALL_TOTAL+11),	/* Must be last of the sequentially-allocated SIDs */

	MK_sid_Sentinel					= 0x7fffffff				/* Sentinel to force the type to be 32-bit */
};

typedef enum mk_serviceid_e mk_serviceid_t;

/* Panic codes
 *
 * These values are passed as parameters to MK_Panic and therefore to MK_PanicStop
 *
 * MK_Panic sets the global MK_panicReason to the first panic that occurs.
 *
 * The values can also be passed to MK_StartupPanic and therefore to MK_USERPANICSTOP. In this
 * case MK_panicReason does not get modified.
*/
enum mk_panic_e
{
	MK_panic_None = 0,
	/*34567890123456789012345678901		31-character symbol length */
	MK_panic_DataSectionsNotInitialized,
	MK_panic_ExceptionNoThread,							/* MK_ExceptionIsSaneXxx */
	MK_panic_ExceptionCurrentThreadNotAtHeadOfQueue,	/* MK_ExceptionIsSaneXxx */
	MK_panic_ExceptionFromKernel,						/* MK_ExceptionIsSaneXxx */
	MK_panic_UnexpectedSystemCall,
	MK_panic_UnexpectedInterrupt,
	MK_panic_UnexpectedThreadWithParent,
	MK_panic_InterruptNoThread,
	MK_panic_CurrentThreadNotAtHeadOfQueue,
	MK_panic_ThreadNotFoundInQueue,
	MK_panic_JobIsNotTask,
	MK_panic_JqAppend_ThreadHasNoQueue,
	MK_panic_JqAppend_QueueIsFull,
	MK_panic_JqUnexpectedJobQueue,
	MK_panic_ExpectedHardwareNotFound,					/* Startup */
	MK_panic_ProtectionFaultInProtectionHook,
	MK_panic_PanicFromProtectionHook,
	MK_panic_MemoryPartitionIsTooLarge,					/* Startup */
	MK_panic_IncorrectStartupKey,						/* Startup */
	MK_panic_MpuNotInitializedCorrectly,				/* Startup */
	MK_panic_StartupCheckFailed,						/* Startup */
	MK_panic_InvalidMpuConfiguration,					/* Startup */
	MK_panic_VectorTableIncorrectlyInitialized,			/* Startup */
	MK_panic_UndocumentedException,
	MK_panic_RegisterStoreIsNotClean,
	MK_panic_ProcessorLimitExceeded,
	MK_panic_MisalignedVectorTable,
	MK_panic_MisalignedSymbol,
	MK_panic_UnexpectedTaskActivationFailure,
	MK_panic_UnexpectedOsApplication,					/* MK_InitApplications */
	MK_panic_InvalidTaskIdInConfiguration,				/* MK_InitApplications */
	MK_panic_InvalidInterruptLevel,						/* ARM / VIM-specific */
	MK_panic_FaultVimDetected,							/* ARM / VIM-specific */
	MK_panic_UnexpectedExceptionLevel,					/* RH850-specific */
	MK_panic_UnexpectedHardwareResponse,
	MK_panic_InsufficientResources,

	MK_panic_Unknown	/* Must be last */
};

typedef enum mk_panic_e mk_panic_t;

/* MK_panicReason
 * The panic reason for the first panic is stored here so that it can be examined in a debugger and in the
 * shutdown hook.
 * This variable also serves as an indication that a panic has already occurred.
*/
extern mk_panic_t MK_panicReason;

/* Error information structures.
*/
#define MK_MAXPARAMS	4

/* !LINKSTO Microkernel.ErrorChecking.LastError.ErrorInfo, 1
 * !doctype src
*/
typedef struct mk_errorinfo_s mk_errorinfo_t;
typedef struct mk_protectioninfo_s mk_protectioninfo_t;
typedef mk_thread_t *mk_culprit_t;

struct mk_errorinfo_s
{
	mk_serviceid_t serviceId;
	mk_errorid_t errorId;
	mk_osekerror_t osekError;
	mk_objectid_t culpritId;
	mk_objecttype_t culpritType;
	const mk_char_t *culpritName;
	mk_culprit_t culprit;
	mk_parametertype_t parameter[MK_MAXPARAMS];
};

/* !LINKSTO Microkernel.ErrorChecking.LastError.ProtectionInfo, 1
 * !doctype src
*/
struct mk_protectioninfo_s
{
	mk_serviceid_t serviceId;
	mk_errorid_t errorId;
	mk_osekerror_t osekError;
	mk_culprit_t culprit;
};

/* Error information structures
*/
extern mk_errorinfo_t * const MK_errorInfo;
extern mk_protectioninfo_t * const MK_protectionInfo;

/* Error code conversion - internal to external
*/
mk_osekerror_t MK_ErrorInternalToOsek(mk_errorid_t);

/* Retrieve service-ID from error-info structure
*/
mk_serviceid_t MK_ErrorGetServiceId(void);

/* Retrieve parameters from error-info culprit.
*/
mk_parametertype_t MK_ErrorGetParameter(mk_int_fast16_t);

/* Configurable function to call if there's a panic during startup.
*/
typedef void (*mk_paniccallout_t)(mk_panic_t);

#endif

#endif

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
