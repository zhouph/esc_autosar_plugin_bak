/* Mk_public_api.h
 *
 * This file declares the public API that the user may use (directly or
 * indirectly) to call the microkernel or the OS that runs under it.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_public_api.h 16005 2014-05-06 07:39:34Z dh $
*/
#ifndef MK_PUBLIC_API_H
#define MK_PUBLIC_API_H

#include <public/Mk_hwsel.h>
#include <public/Mk_public_types.h>
#include <public/Mk_statustype.h>
#include <public/Mk_error.h>

#include MK_HWSEL_PUB_API

#ifndef MK_ASM
/* This function is called directly by the startup code in order to set the
 * microkernel running.
*/
void MK_StartKernel(mk_uint32_t, mk_uint32_t, mk_uint32_t, mk_uint32_t);

mk_parametertype_t MK_UsrTerminateSelf(void);
void MK_UsrTerminateSelfWithResult(mk_parametertype_t);							/* Alias */
#define MK_HAS_DUAL_RETURN_VALUE	1
void MK_UsrTerminateSelfWithValue(mk_parametertype_t, mk_parametertype_t);		/* Alias */
mk_parametertype_t MK_UsrActivateTask(mk_objectid_t);
mk_parametertype_t MK_UsrChainTask(mk_objectid_t);
mk_parametertype_t MK_UsrDisableInterruptSource(mk_objectid_t);
mk_parametertype_t MK_UsrEnableInterruptSource(mk_objectid_t);
mk_parametertype_t MK_UsrSchedule(void);
mk_parametertype_t MK_UsrGetResource(mk_objectid_t);
mk_parametertype_t MK_UsrReleaseResource(mk_objectid_t);
mk_parametertype_t MK_UsrShutdown(mk_uint8_t);
mk_parametertype_t MK_UsrStartOs(mk_objectid_t);
mk_parametertype_t MK_UsrSetEvent(mk_objectid_t, mk_uint32_t);
mk_parametertype_t MK_UsrClearEvent(mk_uint32_t);
mk_parametertype_t MK_UsrWaitEvent(mk_uint32_t);
mk_statusandvalue_t MK_UsrWaitGetClearEvent(mk_uint32_t);
mk_statusandvalue_t MK_UsrGetTaskId(mk_parametertype_t);
mk_statusandvalue_t MK_UsrGetTaskState(mk_objectid_t, mk_parametertype_t);
mk_parametertype_t MK_UsrGetIsrId(void);
mk_parametertype_t MK_UsrReportError(mk_objectid_t, mk_errorid_t, mk_parametertype_t, mk_parametertype_t);
mk_parametertype_t MK_UsrGetApplicationId(void);
mk_parametertype_t MK_UsrTerminateApplication(mk_objectid_t, mk_boolean_t);
mk_parametertype_t MK_UsrSelftest(mk_parametertype_t, mk_uint32_t);

/* For MK_UsrStartForeignThread(), we need to have five prototypes, depending on how
 * many parameters and return value there are. This avoids warnings from the compiler.
*/
/*
 * !LINKSTO Microkernel.Interface.API.Indirect.QMOSCalls, 1
 * !doctype src
*/
mk_parametertype_t MK_UsrStartForeignThread(mk_objectid_t);
mk_parametertype_t MK_UsrStartForeignThread1(mk_objectid_t, mk_parametertype_t);
mk_parametertype_t MK_UsrStartForeignThread2(mk_objectid_t, mk_parametertype_t, mk_parametertype_t);
mk_parametertype_t MK_UsrStartForeignThread3(mk_objectid_t,
											 mk_parametertype_t,
											 mk_parametertype_t,
											 mk_parametertype_t);
mk_statusandvalue_t MK_UsrStartForeignThread2V(mk_objectid_t, mk_parametertype_t, mk_parametertype_t);

/* System services that run in the context of the caller.
*/
mk_objquantity_t MK_LibGetNTasks(void);
StatusType MK_LibConditionalGetResource(mk_objectid_t);
void MK_LibFinishSelftest(mk_uint32_t);
mk_boolean_t MK_LibIsScheduleNecessary(void);

extern const mk_objectid_t MK_resLockCat2;
extern const mk_objectid_t MK_resLockCat1;

/* !LINKSTO      Microkernel.Function.MK_ScheduleIfNecessary, 1
 * !doctype      src
*/
#define MK_ScheduleIfNecessary()    ( MK_LibIsScheduleNecessary() ? MK_UsrSchedule() : MK_E_OK )

/* The microkernel's "time" API.
 *
 * If the hardware has direct support for a long-duration timer, the microkernel uses it.
 * Otherwise a short-duration timer is extended using the generic function.
 *
 * With the exception of MK_ElapsedMicroseconds and the macros that use it, all times are in units of ticks
 * of the hardware timer. For absolute time these are "ticks since counting began", which is processor-
 * dependent (usually the time of the last reset).
 *
 * These functions are all normal library functions that can be called from any thread that has read access
 * to the timer.
*/
#if MK_HW_HAS_TIMESTAMP

/* !LINKSTO Microkernel.Timestamp.MK_ReadTime, 1
 * !doctype src
*/
void MK_HwReadTime(mk_time_t *);
#define MK_ReadTime(t)	MK_HwReadTime(t)

#else

void MK_GenericReadTime(mk_time_t *);
#define MK_ReadTime(t)	MK_GenericReadTime(t)

#endif

void MK_DiffTime(mk_time_t *, const mk_time_t *, const mk_time_t *);
void MK_ElapsedTime(mk_time_t *, mk_time_t *);
mk_tick_t MK_DiffTime32(const mk_time_t *, const mk_time_t *);
mk_tick_t MK_ElapsedTime32(mk_time_t *);
void MK_ElapsedMicroseconds(mk_tick_t *, mk_tick_t *, mk_uint16_t);

mk_uint32_t MK_MulDiv(mk_uint32_t, mk_uint16_t, mk_uint16_t);

/* MK_timestampClockFactor100u, -10u, -1u
 *
 * These constants contain the number of ticks that occur in 100, 10 and 1 microseconds for the
 * hardware timer that is used for the timestamp (MK_HwReadTime).
 *
 * That means they contain the frequency of the hardware clock, in MHz, multiplied by 100, 10 and 1
 *
 * The values must be no greater than 65535
*/
extern const mk_uint16_t MK_timestampClockFactor100u;
extern const mk_uint16_t MK_timestampClockFactor10u;
extern const mk_uint16_t MK_timestampClockFactor1u;

/* MK_ElapsedTime100u, -10u, -1u
 *
 * These macros provide the time that has elapsed since the last call (using the same "prev" variable) in
 * units of 100us, 10us and 1us respectively.
 *
 * In all three cases, both parameters are pointers to mk_tick_t variables and should not point to
 * the same variable.
 *
 * !LINKSTO Microkernel.Timestamp.MK_ElapsedTime100u, 1,
 * !        Microkernel.Timestamp.MK_ElapsedTime10u, 1,
 * !        Microkernel.Timestamp.MK_ElapsedTime1u, 1
 * !doctype src
*/
#define MK_ElapsedTime100u(prev, elapsed)		MK_ElapsedMicroseconds(prev, elapsed, MK_timestampClockFactor100u)
#define MK_ElapsedTime10u(prev, elapsed)		MK_ElapsedMicroseconds(prev, elapsed, MK_timestampClockFactor10u)
#define MK_ElapsedTime1u(prev, elapsed)			MK_ElapsedMicroseconds(prev, elapsed, MK_timestampClockFactor1u)

#endif

/* MK_WaitGetClearEvent() - a combined WaitEvent, GetEvent and ClearEvent API
*/
#ifndef MK_ASM
mk_parametertype_t MK_WaitGetClearEvent(mk_uint32_t, mk_uint32_t *);
#endif

#define MK_GetClearEvent(ep)			MK_WaitGetClearEvent(0u, (ep))

#define MK_HAS_WAITGETCLEAREVENT	1
#define MK_HAS_GETCLEAREVENT		1

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
