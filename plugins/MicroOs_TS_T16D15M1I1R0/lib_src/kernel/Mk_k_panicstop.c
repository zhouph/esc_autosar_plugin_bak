/* Mk_k_panicstop.c
 *
 * This file contains the MK_PanicStop() function.
 *
 * This function is called by MK_Panic whenever a normal MK_Panic() is not possible.
 * Typical calls are from the startup function or from MK_Panic() for a nested panic.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_panicstop.c 15821 2014-04-16 04:55:15Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 14.2 (required)
 *  All non-null statements shall either a) have at least one side effect however executed,
 *  or b) cause control flow to change.
 *
 * Reason:
 *  MK_PanicStop is the last exit the kernel takes, typically during startup, if it cannot
 *  continue and an orderly shutdown is not possible. The function does not use its parameter
 *  itself, it is only provided for debugging purposes. To keep compilers from reporting an
 *  unused parameter warning, the toolchain-dependent macro MK_PARAM_UNUSED() is used which
 *  usually evaluates to a null-statement.
*/

#include <private/Mk_panic.h>
#include <private/Mk_tool.h>

/* MK_PanicStop() reports kernel panics (problems of the "cannot continue" type)
 *
 * There's really nothing we can do except sit in a loop and wait till a watchdog
 * times out, or hope that there's a debugger attached.
 *
 * !LINKSTO Microkernel.Function.MK_PanicStop, 1
 * !doctype src
*/
void MK_PanicStop(mk_panic_t panicReason)
{
	/* Deviation MISRA-1 */
	MK_PARAM_UNUSED(panicReason);

	for (;;)
	{
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
