/* Mk_k_getapplicationid.c
 *
 * This file contains the function MK_GetApplicationId()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_getapplicationid.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>

/* MK_GetApplicationId() - returns the ID of the current OS-Application
 *
 * It works by walking along the thread queue and looking for the first
 * task/ISR thread that is not in the new state, i.e. running or ready.
 *
 * All other thread types inherit the "running application" from their parent
 * thread (trusted functions) or the thread that triggered them (hooks). This
 * inheritance works transitively.
 *
 * !LINKSTO Microkernel.Function.MK_GetApplicationId, 1
 * !doctype src
*/
mk_objectid_t MK_GetApplicationId(void)
{
	mk_thread_t *t = MK_threadQueueHead;
	mk_objectid_t appId = MK_APPL_NONE;
	mk_boolean_t foundApp = MK_FALSE;

	/* We loop until we find the first running/ready thread that is a task or an ISR
	*/
	while ( (t != MK_NULL) && (foundApp == MK_FALSE) )
	{
		if ( (t->objectType == MK_OBJTYPE_TASK) || (t->objectType == MK_OBJTYPE_ISR) )
		{
			if ( t->state != MK_THS_NEW )
			{
				/* for global tasks/ISRs, this ID will be INVALID_OSAPPLICATION */
				appId = t->applicationId;
				/* we are done here, exit the loop */
				foundApp = MK_TRUE;
			}
		}

		t = t->next;
	}

	return appId;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
