/* Mk_k_restartthread.c
 *
 * This file contains the MK_RestartThread() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_restartthread.c 15821 2014-04-16 04:55:15Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 2.4 (advisory)
 *  Section of code should not be "commented out".
 *
 * Reason:
 *  This is not code, this is an implementation hint.
*/

#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
/* Deviation MISRA-1 */
/*
#include <Dbg.h>
*/

/* MK_RestartThread() (re)starts a thread with existing/already initialized registers
 *
 * Parameters:
 *	thread		- the address of the thread structure
 *	threadCfg	- address of a thread config block for this thread
 *
 * Precondition:
 *	the thread is idle
 *
 * !LINKSTO Microkernel.Function.MK_RestartThread, 1
 * !doctype src
*/
void MK_RestartThread
(	mk_thread_t *thread,
	const mk_threadcfg_t *threadCfg
)
{
	thread->currentObject = threadCfg->currentObject;
	thread->objectType = threadCfg->objectType;

	thread->queueingPriority = threadCfg->queuePrio;
	thread->runningPriority = threadCfg->runningPriority;
	thread->memoryPartition = threadCfg->memoryPartition;
	thread->name = threadCfg->name;
	thread->regs = threadCfg->regs;
	thread->applicationId = threadCfg->applicationId;

	/* Copy and initialise the accounting information.
	*/
	thread->accounting.acctCfg = &threadCfg->acctCfg;
	thread->accounting.timeRemaining = threadCfg->acctCfg.execBudget;

	/* Enqueue the thread at its queueing priority
	*/
	thread->currentPriority = thread->queueingPriority;

	/* Deviation MISRA-1 */
	/*
		MK_TRACE_STATE_THREAD(thread->objectType, thread->currentObject,
								thread->state, MK_THS_NEW);
	*/
	thread->state = MK_THS_NEW;

	MK_EnqueueThread(thread);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
