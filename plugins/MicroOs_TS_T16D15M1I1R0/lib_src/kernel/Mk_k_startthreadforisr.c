/* Mk_k_startthreadforisr.c
 *
 * This file contains the MK_StartThreadForIsr() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_startthreadforisr.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <private/Mk_isr.h>
#include <private/Mk_interrupt.h>

/* MK_StartThreadForIsr() starts a thread to run the given isr
 *
 * Precondition:
 *	the thread is idle
 *
 * !LINKSTO Microkernel.Function.MK_StartThreadForIsr, 1
 * !doctype src
*/
void MK_StartThreadForIsr
(	mk_objectid_t isrIndex,
	mk_hwvectorcode_t vectorCode
)
{
	const mk_isrcfg_t *isr;
	mk_thread_t *thread;

	if ( (isrIndex >= 0) && (isrIndex < MK_nIsrs) )
	{
		/* If MK_isrCfg is null, MK_nIsrs is 0, so this branch does not get executed
		 * because no integer can be >= 0 and < 0.
		 * This means that no test for null is necessary.
		*/
		isr = &MK_isrCfg[isrIndex];
		thread = isr->thread;

		MK_StartThread(thread, &isr->threadCfg);
	}
	else
	{
		MK_UnknownInterrupt(isrIndex, vectorCode);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
