/* Mk_k_sysclearevent.c
 *
 * This file contains the MK_SysClearEvent() function.
 *
 * This function is called by the system call function whenever the ClearEvent system call is made.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysclearevent.c 15908 2014-04-24 07:14:25Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_thread.h>
#include <private/Mk_task.h>
#include <private/Mk_errorhandling.h>

/*
 * MK_SysClearEvent() clears the specified events from the current EXTENDED task's pending events
 *
 * This function is present in the system call table and is called whenever a thread makes
 * a "ClearEvent" system call.
 *
 * !LINKSTO Microkernel.Function.MK_SysClearEvent, 1
 * !doctype src
*/
void MK_SysClearEvent(void)
{
	mk_uint32_t eventsToClear;
	mk_eventstatus_t *eventStatus;

	mk_errorid_t errorCode = MK_eid_Unknown;

	if ( MK_threadCurrent->objectType == MK_OBJTYPE_TASK )
	{
		eventStatus = MK_taskCfg[MK_threadCurrent->currentObject].eventStatus;

		if ( eventStatus == MK_NULL )
		{
			/* Error: the caller is a BASIC task
			*/
			errorCode = MK_eid_TaskIsNotExtended;
		}
		else
		{
			/* The caller is an EXTENDED task
			*/
			errorCode = MK_eid_NoError;

			eventsToClear = MK_HwGetParameter1(MK_threadCurrent);

			eventStatus->pendingEvents &= ~eventsToClear;
		}
	}
	else
	{
		/* Error: the caller isn't a task
		*/
		errorCode = MK_eid_ThreadIsNotATask;
	}

	if ( errorCode == MK_eid_NoError )
	{
		MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
	}
	else
	{
		(void)MK_ReportError(MK_sid_ClearEvent, errorCode, MK_threadCurrent);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
