/* Mk_k_sysshutdown.c
 *
 * This file contains the MK_SysShutdown() function.
 *
 * This function is called by the system call function whenever the ShutdownOS system call is made.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysshutdown.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_shutdown.h>
#include <private/Mk_thread.h>

/*
 * MK_SysShutdown() shuts down the complete system on demand.
 *
 * This function is present in the system call table and is called whenever a thread makes
 * a "ShutdownOS" system call.
 *
 * !LINKSTO Microkernel.Function.MK_SysShutdown, 1
 * !doctype src
 */
void MK_SysShutdown(void)
{
	mk_osekerror_t shutdownCode = (mk_osekerror_t)MK_HwGetParameter1(MK_threadCurrent);

	MK_Shutdown(shutdownCode);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
