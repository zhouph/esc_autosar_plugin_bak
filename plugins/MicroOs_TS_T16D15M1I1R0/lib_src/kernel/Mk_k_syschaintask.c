/* Mk_k_syschaintask.c
 *
 * This file contains the MK_SysChainTask() function.
 *
 * This function is called by the system call function whenever the ChainTask system call is made.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_syschaintask.c 15908 2014-04-24 07:14:25Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_thread.h>
#include <private/Mk_task.h>
#include <private/Mk_errorhandling.h>

/*
 * MK_SysChainTask() terminates the caller and activates a new task.
 *
 * This function is present in the system call table and is called whenever a thread makes
 * a "ChainTask" system call. As with all system calls, the parameters are obtained from
 * the current thread's register context.
 *
 * Note: ChainTask *never* returns to the caller, so the current thread is terminated
 * no matter what happens.
 *
 * !LINKSTO Microkernel.Function.MK_SysChainTask, 1
 * !doctype src
*/
void MK_SysChainTask(void)
{
	mk_objectid_t taskId = (mk_objectid_t)MK_HwGetParameter1(MK_threadCurrent);
	mk_errorid_t errorCode = MK_eid_Unknown;

	/* If the caller is a task, decrement the activation counter
	*/
	if ( MK_threadCurrent->objectType == MK_OBJTYPE_TASK )
	{
		MK_taskCfg[MK_threadCurrent->currentObject].dynamic->activationCount--;
	}

	/* Dequeue the caller (current thread) from the head of the thread queue.
	 * (Precondition for MK_TerminateThread())
	*/
	MK_threadQueueHead = MK_threadQueueHead->next;
	MK_threadCurrent->next = MK_NULL;

	/* Terminate the thread
	*/
	MK_HwFreeThreadRegisters(MK_threadCurrent->regs);
	MK_TerminateThread(MK_threadCurrent);

	if ( (taskId >= 0) && (taskId < MK_nTasks) )
	{
		errorCode = MK_ActivateTask(taskId);
	}
	else
	{
		errorCode = MK_eid_InvalidTaskId;
	}

	/* There's no need to set a return value; the calling thread has terminated.
	*/
	if ( errorCode != MK_eid_NoError )
	{
		(void)MK_ReportError(MK_sid_ChainTask, errorCode, MK_threadCurrent);

		/* The calling thread has been terminated. If there are no more threads
		 * the problem needs to be handled.
		*/
		if ( MK_threadQueueHead == MK_NULL )
		{
			MK_ThreadQueueEmpty();
		}
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
