/* Mk_k_sysactivatetask.c
 *
 * This file contains the MK_SysActivateTask() function.
 *
 * This function is called by the system call function whenever the ActivateTask system call is made.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysactivatetask.c 15908 2014-04-24 07:14:25Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_thread.h>
#include <private/Mk_task.h>
#include <private/Mk_errorhandling.h>

/*
 * MK_SysActivateTask() activates a task.
 *
 * This function is present in the system call table and is called whenever a thread makes
 * an "ActivateTask" system call. As with all system calls, the parameters are obtained from
 * the current thread's register context.
 *
 * !LINKSTO Microkernel.Function.MK_SysActivateTask, 1
 * !doctype src
*/
void MK_SysActivateTask(void)
{
	mk_objectid_t taskId = (mk_objectid_t)MK_HwGetParameter1(MK_threadCurrent);
	mk_errorid_t errorCode = MK_eid_Unknown;

	if ( (taskId >= 0) && (taskId < MK_nTasks) )
	{
		errorCode = MK_ActivateTask(taskId);
	}
	else
	{
		errorCode = MK_eid_InvalidTaskId;
	}

	if ( errorCode == MK_eid_NoError )
	{
		MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
	}
	else
	{
		(void)MK_ReportError(MK_sid_ActivateTask, errorCode, MK_threadCurrent);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
