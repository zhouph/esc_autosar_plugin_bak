/* Mk_k_dispatch.c
 *
 * This file contains the MK_Dispatch function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_dispatch.c 17627 2014-11-03 14:10:37Z stpo8218 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 16.2 (required)
 *  Functions shall not call themselves, either directly or indirectly.
 *
 * Reason:
 *  MK_Dispatch can call itself again indirectly via MK_ReportProtectionFault and MK_Panic,
 *  but this can only happen if the protection hook thread has an execution budget and exceeds it.
 *  In the unlikely case that also the shutdown thread has and exceeded execution budget,
 *  the calls to MK_ReportProtectionFault and MK_Panic will result in a call to MK_PanicStop.
 *  So there is a maximum nesting of two calls to MK_Dispatch possible.
 *
 * MISRA-2) Deviated Rule: 2.4 (advisory)
 *  Section of code should not be "commented out".
 *
 * Reason:
 *  This is not code, this is an implementation hint.
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_C_STORAGE_010]
 *  All variables defined in functions shall have automatic storage duration.
 *
 * Reason:
 *  Defining this variable outside the function scope leads to a deviation of MISRA Rule 8.7.
 *
 * DCG-2) Deviated Rule: [OS_C_COMPL_010:path]
 *  The code shall adhere to the [HISSRCMETRIC] Metrics.
 *
 * Reason:
 *  The function MK_Dispatch() needs a complex loop, leading to a high PATH
 *  value, to provide the proper handling of execution budgeting which cannot
 *  reasonably be split into subroutines without impairing comprehensibility
 *  and maintainability. Design and code inspection assure these properties.
 *
 * DCG-3) Deviated Rule: [OS_C_COMPL_010:vg]
 *  The code shall adhere to the [HISSRCMETRIC] Metrics.
 *
 * Reason:
 *  The function MK_Dispatch() needs a complex loop, leading to a high VG
 *  value, to provide the proper handling of execution budgeting which cannot
 *  reasonably be split into subroutines without impairing comprehensibility
 *  and maintainability. Design and code inspection assure these properties.
 *
 * DCG-4) Deviated Rule: [OS_C_COMPL_010:vocf]
 *  The code shall adhere to the [HISSRCMETRIC] Metrics.
 *
 * Reason:
 *  The function MK_Dispatch() uses macros for reading / setting the execution
 *  timer. Depending on the CPU-family, these may increase the amount of
 *  identical operators and operands, which in turn breaks the VOCF limit.
 *  This is ok, since the use of macros for hardware-dependent functionality
 *  is a design pattern of the microkernel and is used to increase the
 *  maintainability, which is the goal of the VOCF metric.
*/

/* Deviation DCG-2 <*>, DCG-3 <*>, DCG-4 <*> */
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <private/Mk_event.h>
#include <private/Mk_interrupt.h>
#include <private/Mk_memoryprotection.h>
#include <private/Mk_errorhandling.h>
/* Deviation MISRA-2 */
/*
#include <Dbg.h>
*/


/* MK_Dispatch is the thread dispatcher.
 *
 * It selects the most eligible thread (the head of the thread queue) and makes it the current thread.
 * If that action involves a change of thread and the outgoing thread has not terminated, then it is
 * placed in the READY state.
 *
 * The correct memory partition for the incoming thread is loaded.
 *
 * The remaining execution budget for the incoming thread is programmed. The hardware dependent part of
 * this mechanism might need different handling for the cases where a change of thread has or has not occurred.
 *
 * The thread's priority is increased to its configured running priority if necessary (thus acquiring
 * any internal resources and becoming non-preemptive if so configured).
 *
 * Finally the thread's copy of the processor registers is loaded into the processor, thus resuming the
 * thread from where it left off. Most of this is processor-specific and needs to be done in assembly language,
 * so MK_HwResumeThread() is called.
 *
 * !LINKSTO Microkernel.Function.MK_Dispatch, 1
 * !doctype src
*/
/* Deviation MISRA-1 */
void MK_Dispatch(void)
{
	/* Deviation DCG-1 */
	static mk_tick_t dispatchTime;
	mk_tick_t prevDispatchTime;
	mk_tick_t deltaTime;
	mk_accounting_t *acct;
	mk_boolean_t changeThread = MK_TRUE;		/* Used for programming the execution budget interval */

	do
	{
		if ( MK_execBudgetIsConfigured )
		{
			prevDispatchTime = dispatchTime;
			dispatchTime = MK_HwReadExecutionTimer();

			acct = &MK_threadCurrent->accounting;

			if ( (acct->timeRemaining != MK_EXECBUDGET_INFINITE) && (MK_threadCurrent->state != MK_THS_NEW) )
			{
				/* Outgoing thread has an accounting monitor.
				 * Calculate the time used and subtract from the thread's time-remaining accumulator.
				*/
				deltaTime = MK_HwExecutionTimeDelta(dispatchTime, prevDispatchTime);

				if ( acct->timeRemaining > deltaTime )
				{
					acct->timeRemaining -= deltaTime;
				}
				else
				{
					/* The thread has already reached or exceeded its budget.
					 * Set the time-remaining accumulator to 0. The fault will be picked
					 * up when the thread gets dispatched, which might be immediate if there's no
					 * change of thread. If might also never happen - the thread might have terminated
					 * or waited for an event)
					*/
					acct->timeRemaining = 0;
				}
			}
		}

		/* Head of queue becomes the running thread.
		 */
		if ( MK_threadCurrent == MK_threadQueueHead )
		{
			/* No thread change.
			*/
			changeThread = MK_FALSE;
		}
		else
		{
			/* Change of thread.
			*/
			changeThread = MK_TRUE;

			if ( MK_threadCurrent->state == MK_THS_RUNNING )
			{
				/* Deviation MISRA-2 */
				/*
					MK_TRACE_STATE_THREAD(MK_threadCurrent->objectType, MK_threadCurrent->currentObject,
											MK_threadCurrent->state, MK_THS_READY);
				*/
				MK_threadCurrent->state = MK_THS_READY;

			}

			MK_threadCurrent = MK_threadQueueHead;
		}


		if ( MK_execBudgetIsConfigured )
		{
			/* Is there an accounting monitor for the incoming thread?
			*/
			acct = &MK_threadCurrent->accounting;

			if ( acct->timeRemaining == MK_EXECBUDGET_INFINITE )
			{
				/* No accounting, or infinite budget: turn off the timer
				*/
				MK_HwDispatcherClearExecutionTimeAlarm();
			}
			else
			{
				/* Has the incoming thread used up its budget?
				 * (The comparison against a minumum time attempts to avoid starting a thread whose
				 * budget will expire immediately)
				*/
				if ( acct->timeRemaining <= MK_MIN_EXECUTIONTIME )
				{
					/* In the future, the error could also be MK_eid_LockBudgetExceeded, so we'll
					 * need to distinguish between the two here.
					*/
					MK_ReportProtectionFault(MK_sid_ExceptionHandling, MK_eid_ExecutionBudgetExceeded);
				}
				else
				if ( changeThread || (MK_threadCurrent->state == MK_THS_NEW) )
				{
					/* Thread (or instance of executable object in same thread) has changed.
					 * The execution timer needs to be programmed.
					*/
					MK_HwSetExecutionTimeAlarm(acct->timeRemaining);
				}
				else
				{
					/* Thread has not changed. The execution timer might need to be updated
					 * or reprogrammed, depending on the hardware characteristics.
					*/
					MK_HwUpdateExecutionTimeAlarm(acct->timeRemaining);
				}
			}
		}
	} while ( MK_threadCurrent != MK_threadQueueHead );

	/* Change state of the selected thread to RUNNING
	*/
	/* Deviation MISRA-2 */
	/*
		MK_TRACE_STATE_THREAD(MK_threadCurrent->objectType, MK_threadCurrent->currentObject,
								MK_threadCurrent->state, MK_THS_RUNNING);
	*/
	MK_threadCurrent->state = MK_THS_RUNNING;

	/* If the thread is a task that is returning from WaitGetClearEvent
	 * (indicated by the eventStatus pointer being non-NULL), do the Get and Clear parts
	 *  - place the pending events into the 2nd return value
	 *  - clear the events from the pending events
	 *  - set the waiting state to "not waiting"
	 *  - clear the eventStatus pointer so that this doesn't happen again until the next WaitGetClearEvent
	*/
	if ( MK_threadCurrent->eventStatus != MK_NULL )
	{
		MK_HwSetReturnValue2(MK_threadCurrent, MK_threadCurrent->eventStatus->pendingEvents);
		MK_threadCurrent->eventStatus->pendingEvents = 0u;
		MK_threadCurrent->eventStatus = MK_NULL;
	}

	/* Set the thread's running priority - only if not pre-empted, and
	 * only if the running priority is higher than the queued priority.
	 * This boils down to testing the 2nd condition only - preempted
	 * threads will have a priority not less than their running priority
	 * anyway.
	*/
	if ( MK_threadCurrent->currentPriority < MK_threadCurrent->runningPriority )
	{
		MK_threadCurrent->currentPriority = MK_threadCurrent->runningPriority;
	}

	/* Set up the context for the new thread.
	*/
	MK_HwIntLevelThreadToHardware(MK_threadCurrent);

	if ( MK_currentMemoryPartition != MK_threadCurrent->memoryPartition )
	{
		MK_currentMemoryPartition = MK_threadCurrent->memoryPartition;
		MK_HwSetDynamicMemoryPartition(MK_currentMemoryPartition);
	}

	MK_HwResumeThread(MK_threadCurrent);		/* MK_HwResumeThread() never returns. */
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
