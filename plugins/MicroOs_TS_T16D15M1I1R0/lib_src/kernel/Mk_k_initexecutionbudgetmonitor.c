/* Mk_k_initexecutionbudgetmonitor.c
 *
 * This file contains the MK_InitExecutionBudgetMonitor function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_initexecutionbudgetmonitor.c 15947 2014-04-28 10:16:48Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_accounting.h>

/* MK_InitExecutionBudgetMonitor() initializes the execution budget monitoring subsystem.
 *
 * !LINKSTO Microkernel.Function.MK_InitExecutionBudgetMonitor, 1
 * !doctype src
*/
void MK_InitExecutionBudgetMonitor(void)
{
	MK_execBudgetIsConfigured = MK_IsExecutionBudgetConfigured();
	/* On some CPU families, the timestamp timer is identical to the
	 * execution budget timer.
	 * FUTURE: Separate the two properly.
	 */
	MK_HwInitExecutionTimer();
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
