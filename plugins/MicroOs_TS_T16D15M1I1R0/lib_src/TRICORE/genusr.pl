#!/usr/bin/perl -w
#
# Generates a set of "usr" function stubs from a list.
#
# For the Microkernel the files are generated once and checked in,
# otherwise this script (and maybe Perl too) would need to be verified
# to ASIL-D.
#
# NOT FOR DELIVERY!
#
# (c) Elektrobit Automotive GmbH

use strict;
use warnings;

my @syscall_list =
(
	"ActivateTask",
	"ChainTask",
	"ClearEvent",
	"DisableInterruptSource",
	"EnableInterruptSource",
	"GetApplicationId",
	"GetEvent",
	"GetIsrId",
	"GetResource",
	"GetTaskId",
	"GetTaskState",
	"InitTicker",
	"ReleaseResource",
	"ReportError",
	"Schedule",
	"Selftest",
	"SetEvent",
	"Shutdown",
	"StartForeignThread",
	"StartOs",
	"TerminateApplication",
	"TerminateThread",
	"WaitEvent"
);

foreach my $sc (@syscall_list)
{
	my $lsc = lc($sc);
	my $filename = "Mk_TRICORE_usr".$lsc.".s";
	my $funcname = "MK_Usr".$sc;
	my $scid = "MK_SC_".$sc;

#	print "$sc $lsc\n";

	if ( -e $filename )
	{
		print STDERR "$filename already exists - will not modify\n";
	}
	else
	{
		if ( open(SRCFILE, '>', $filename) )
		{

			print SRCFILE "/* $filename\n";
			print SRCFILE " *\n";
			print SRCFILE " * This file contains the function $funcname()\n";
			print SRCFILE " *\n";
			print SRCFILE " * (c) Elektrobit Automotive GmbH\n";
			print SRCFILE " *\n";
			print SRCFILE " * \$Id\$\n";
			print SRCFILE "*/\n";
			print SRCFILE "#include <private/Mk_asm.h>		/* Must be first! */\n";
			print SRCFILE "#include <private/Mk_syscall.h>\n";
			print SRCFILE "\n";
			print SRCFILE "	MK_file($filename)\n";
			print SRCFILE "\n";
			print SRCFILE "	MK_ASM_SDECL_TEXT\n";
			print SRCFILE "	MK_ASM_SECTION_TEXT\n";
			print SRCFILE "	MK_ASM_ALIGN_TEXT\n";
			print SRCFILE "\n";
			print SRCFILE "	MK_global $funcname\n";
			print SRCFILE "\n";
			print SRCFILE "$funcname:\n";
			print SRCFILE "	syscall	MK_imm(#, $scid)\n";
			print SRCFILE "	ret\n";

			close(SRCFILE);

		}
		else
		{
			print STDERR "Could not open $filename for writing\n";
		}
	}
}
Mk_TRICORE_usractivatetask.s
Mk_TRICORE_usrchaintask.s
Mk_TRICORE_usrclearevent.s
Mk_TRICORE_usrdisableinterruptsource.s
Mk_TRICORE_usrenableinterruptsource.s
Mk_TRICORE_usrgetapplicationid.s
Mk_TRICORE_usrgetevent.s
Mk_TRICORE_usrgetisrid.s
Mk_TRICORE_usrgetresource.s
Mk_TRICORE_usrgettaskid.s
Mk_TRICORE_usrgettaskstate.s
Mk_TRICORE_usrreleaseresource.s
Mk_TRICORE_usrreporterror.s
Mk_TRICORE_usrschedule.s
Mk_TRICORE_usrselftest.s
Mk_TRICORE_usrsetevent.s
Mk_TRICORE_usrshutdown.s
Mk_TRICORE_usrstartforeignthread.s
Mk_TRICORE_usrstartos.s
Mk_TRICORE_usrterminateapplication.s
Mk_TRICORE_usrterminateself.s
Mk_TRICORE_usrterminatethread.s
Mk_TRICORE_usrwaitevent.s
