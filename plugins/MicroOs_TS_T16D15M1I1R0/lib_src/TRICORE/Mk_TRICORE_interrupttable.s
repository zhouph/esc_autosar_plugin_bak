/* Mk_TRICORE_interrupttable.s
 *
 * This file contains the interrupt table for the Tricore.
 *
 * The interrupt vector table in this file converts the hardware vectoring scheme used
 * by Tricore processors into the software vectorign scheme preferred by the Microkernel.
 * While this is not the most efficient option, it is the quickest and simplest to develop
 * and has the advantage that the interrupt table is of a fixed size.
 *
 * Note: the vector code passed to the dispatcher is one less than the vector number to
 * avoid having a dummy entry in the software vector table.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_interrupttable.s 15947 2014-04-28 10:16:48Z masa8317 $
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_ASM_STRUCT_020]
 *  Each assembly file shall contain exactly one return instruction, which is the last instruction.
 *
 * Reason:
 *  This file implements the interrupt vector table for the TRICORE processor. Each entry represents a
 *  kernel entry point. There is no return here, because the kernel exit is implemented in a different
 *  routine.
 *
*/
/* Deviation DCG-1 <*> */

#include <private/Mk_asm.h>		/* Must be first! */
#include <private/TRICORE/Mk_TRICORE_core.h>
#include <private/TRICORE/Mk_TRICORE_interruptcontroller.h>

	MK_file(Mk_TRICORE_interrupttable.s)

	MK_ASM_SDECL_INTERRUPTTABLE
	MK_ASM_SECTION_INTERRUPTTABLE

	MK_global	MK_InterruptTable

	MK_extern	MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptTable:
MK_InterruptVector_01:		/* Vector 0x01 (1) */
	svlcx
	mov		d5, MK_imm(#, 0)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_02:		/* Vector 0x02 (2) */
	svlcx
	mov		d5, MK_imm(#, 1)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_03:		/* Vector 0x03 (3) */
	svlcx
	mov		d5, MK_imm(#, 2)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_04:		/* Vector 0x04 (4) */
	svlcx
	mov		d5, MK_imm(#, 3)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_05:		/* Vector 0x05 (5) */
	svlcx
	mov		d5, MK_imm(#, 4)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_06:		/* Vector 0x06 (6) */
	svlcx
	mov		d5, MK_imm(#, 5)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_07:		/* Vector 0x07 (7) */
	svlcx
	mov		d5, MK_imm(#, 6)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_08:		/* Vector 0x08 (8) */
	svlcx
	mov		d5, MK_imm(#, 7)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_09:		/* Vector 0x09 (9) */
	svlcx
	mov		d5, MK_imm(#, 8)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_0a:		/* Vector 0x0a (10) */
	svlcx
	mov		d5, MK_imm(#, 9)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_0b:		/* Vector 0x0b (11) */
	svlcx
	mov		d5, MK_imm(#, 10)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_0c:		/* Vector 0x0c (12) */
	svlcx
	mov		d5, MK_imm(#, 11)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_0d:		/* Vector 0x0d (13) */
	svlcx
	mov		d5, MK_imm(#, 12)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_0e:		/* Vector 0x0e (14) */
	svlcx
	mov		d5, MK_imm(#, 13)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_0f:		/* Vector 0x0f (15) */
	svlcx
	mov		d5, MK_imm(#, 14)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_10:		/* Vector 0x10 (16) */
	svlcx
	mov		d5, MK_imm(#, 15)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_11:		/* Vector 0x11 (17) */
	svlcx
	mov		d5, MK_imm(#, 16)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_12:		/* Vector 0x12 (18) */
	svlcx
	mov		d5, MK_imm(#, 17)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_13:		/* Vector 0x13 (19) */
	svlcx
	mov		d5, MK_imm(#, 18)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_14:		/* Vector 0x14 (20) */
	svlcx
	mov		d5, MK_imm(#, 19)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_15:		/* Vector 0x15 (21) */
	svlcx
	mov		d5, MK_imm(#, 20)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_16:		/* Vector 0x16 (22) */
	svlcx
	mov		d5, MK_imm(#, 21)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_17:		/* Vector 0x17 (23) */
	svlcx
	mov		d5, MK_imm(#, 22)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_18:		/* Vector 0x18 (24) */
	svlcx
	mov		d5, MK_imm(#, 23)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_19:		/* Vector 0x19 (25) */
	svlcx
	mov		d5, MK_imm(#, 24)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_1a:		/* Vector 0x1a (26) */
	svlcx
	mov		d5, MK_imm(#, 25)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_1b:		/* Vector 0x1b (27) */
	svlcx
	mov		d5, MK_imm(#, 26)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_1c:		/* Vector 0x1c (28) */
	svlcx
	mov		d5, MK_imm(#, 27)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_1d:		/* Vector 0x1d (29) */
	svlcx
	mov		d5, MK_imm(#, 28)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_1e:		/* Vector 0x1e (30) */
	svlcx
	mov		d5, MK_imm(#, 29)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_1f:		/* Vector 0x1f (31) */
	svlcx
	mov		d5, MK_imm(#, 30)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_20:		/* Vector 0x20 (32) */
	svlcx
	mov		d5, MK_imm(#, 31)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_21:		/* Vector 0x21 (33) */
	svlcx
	mov		d5, MK_imm(#, 32)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_22:		/* Vector 0x22 (34) */
	svlcx
	mov		d5, MK_imm(#, 33)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_23:		/* Vector 0x23 (35) */
	svlcx
	mov		d5, MK_imm(#, 34)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_24:		/* Vector 0x24 (36) */
	svlcx
	mov		d5, MK_imm(#, 35)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_25:		/* Vector 0x25 (37) */
	svlcx
	mov		d5, MK_imm(#, 36)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_26:		/* Vector 0x26 (38) */
	svlcx
	mov		d5, MK_imm(#, 37)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_27:		/* Vector 0x27 (39) */
	svlcx
	mov		d5, MK_imm(#, 38)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_28:		/* Vector 0x28 (40) */
	svlcx
	mov		d5, MK_imm(#, 39)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_29:		/* Vector 0x29 (41) */
	svlcx
	mov		d5, MK_imm(#, 40)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_2a:		/* Vector 0x2a (42) */
	svlcx
	mov		d5, MK_imm(#, 41)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_2b:		/* Vector 0x2b (43) */
	svlcx
	mov		d5, MK_imm(#, 42)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_2c:		/* Vector 0x2c (44) */
	svlcx
	mov		d5, MK_imm(#, 43)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_2d:		/* Vector 0x2d (45) */
	svlcx
	mov		d5, MK_imm(#, 44)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_2e:		/* Vector 0x2e (46) */
	svlcx
	mov		d5, MK_imm(#, 45)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_2f:		/* Vector 0x2f (47) */
	svlcx
	mov		d5, MK_imm(#, 46)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_30:		/* Vector 0x30 (48) */
	svlcx
	mov		d5, MK_imm(#, 47)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_31:		/* Vector 0x31 (49) */
	svlcx
	mov		d5, MK_imm(#, 48)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_32:		/* Vector 0x32 (50) */
	svlcx
	mov		d5, MK_imm(#, 49)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_33:		/* Vector 0x33 (51) */
	svlcx
	mov		d5, MK_imm(#, 50)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_34:		/* Vector 0x34 (52) */
	svlcx
	mov		d5, MK_imm(#, 51)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_35:		/* Vector 0x35 (53) */
	svlcx
	mov		d5, MK_imm(#, 52)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_36:		/* Vector 0x36 (54) */
	svlcx
	mov		d5, MK_imm(#, 53)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_37:		/* Vector 0x37 (55) */
	svlcx
	mov		d5, MK_imm(#, 54)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_38:		/* Vector 0x38 (56) */
	svlcx
	mov		d5, MK_imm(#, 55)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_39:		/* Vector 0x39 (57) */
	svlcx
	mov		d5, MK_imm(#, 56)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_3a:		/* Vector 0x3a (58) */
	svlcx
	mov		d5, MK_imm(#, 57)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_3b:		/* Vector 0x3b (59) */
	svlcx
	mov		d5, MK_imm(#, 58)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_3c:		/* Vector 0x3c (60) */
	svlcx
	mov		d5, MK_imm(#, 59)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_3d:		/* Vector 0x3d (61) */
	svlcx
	mov		d5, MK_imm(#, 60)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_3e:		/* Vector 0x3e (62) */
	svlcx
	mov		d5, MK_imm(#, 61)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_3f:		/* Vector 0x3f (63) */
	svlcx
	mov		d5, MK_imm(#, 62)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_40:		/* Vector 0x40 (64) */
	svlcx
	mov		d5, MK_imm(#, 63)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_41:		/* Vector 0x41 (65) */
	svlcx
	mov		d5, MK_imm(#, 64)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_42:		/* Vector 0x42 (66) */
	svlcx
	mov		d5, MK_imm(#, 65)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_43:		/* Vector 0x43 (67) */
	svlcx
	mov		d5, MK_imm(#, 66)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_44:		/* Vector 0x44 (68) */
	svlcx
	mov		d5, MK_imm(#, 67)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_45:		/* Vector 0x45 (69) */
	svlcx
	mov		d5, MK_imm(#, 68)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_46:		/* Vector 0x46 (70) */
	svlcx
	mov		d5, MK_imm(#, 69)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_47:		/* Vector 0x47 (71) */
	svlcx
	mov		d5, MK_imm(#, 70)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_48:		/* Vector 0x48 (72) */
	svlcx
	mov		d5, MK_imm(#, 71)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_49:		/* Vector 0x49 (73) */
	svlcx
	mov		d5, MK_imm(#, 72)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_4a:		/* Vector 0x4a (74) */
	svlcx
	mov		d5, MK_imm(#, 73)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_4b:		/* Vector 0x4b (75) */
	svlcx
	mov		d5, MK_imm(#, 74)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_4c:		/* Vector 0x4c (76) */
	svlcx
	mov		d5, MK_imm(#, 75)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_4d:		/* Vector 0x4d (77) */
	svlcx
	mov		d5, MK_imm(#, 76)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_4e:		/* Vector 0x4e (78) */
	svlcx
	mov		d5, MK_imm(#, 77)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_4f:		/* Vector 0x4f (79) */
	svlcx
	mov		d5, MK_imm(#, 78)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_50:		/* Vector 0x50 (80) */
	svlcx
	mov		d5, MK_imm(#, 79)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_51:		/* Vector 0x51 (81) */
	svlcx
	mov		d5, MK_imm(#, 80)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_52:		/* Vector 0x52 (82) */
	svlcx
	mov		d5, MK_imm(#, 81)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_53:		/* Vector 0x53 (83) */
	svlcx
	mov		d5, MK_imm(#, 82)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_54:		/* Vector 0x54 (84) */
	svlcx
	mov		d5, MK_imm(#, 83)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_55:		/* Vector 0x55 (85) */
	svlcx
	mov		d5, MK_imm(#, 84)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_56:		/* Vector 0x56 (86) */
	svlcx
	mov		d5, MK_imm(#, 85)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_57:		/* Vector 0x57 (87) */
	svlcx
	mov		d5, MK_imm(#, 86)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_58:		/* Vector 0x58 (88) */
	svlcx
	mov		d5, MK_imm(#, 87)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_59:		/* Vector 0x59 (89) */
	svlcx
	mov		d5, MK_imm(#, 88)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_5a:		/* Vector 0x5a (90) */
	svlcx
	mov		d5, MK_imm(#, 89)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_5b:		/* Vector 0x5b (91) */
	svlcx
	mov		d5, MK_imm(#, 90)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_5c:		/* Vector 0x5c (92) */
	svlcx
	mov		d5, MK_imm(#, 91)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_5d:		/* Vector 0x5d (93) */
	svlcx
	mov		d5, MK_imm(#, 92)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_5e:		/* Vector 0x5e (94) */
	svlcx
	mov		d5, MK_imm(#, 93)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_5f:		/* Vector 0x5f (95) */
	svlcx
	mov		d5, MK_imm(#, 94)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_60:		/* Vector 0x60 (96) */
	svlcx
	mov		d5, MK_imm(#, 95)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_61:		/* Vector 0x61 (97) */
	svlcx
	mov		d5, MK_imm(#, 96)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_62:		/* Vector 0x62 (98) */
	svlcx
	mov		d5, MK_imm(#, 97)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_63:		/* Vector 0x63 (99) */
	svlcx
	mov		d5, MK_imm(#, 98)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_64:		/* Vector 0x64 (100) */
	svlcx
	mov		d5, MK_imm(#, 99)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_65:		/* Vector 0x65 (101) */
	svlcx
	mov		d5, MK_imm(#, 100)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_66:		/* Vector 0x66 (102) */
	svlcx
	mov		d5, MK_imm(#, 101)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_67:		/* Vector 0x67 (103) */
	svlcx
	mov		d5, MK_imm(#, 102)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_68:		/* Vector 0x68 (104) */
	svlcx
	mov		d5, MK_imm(#, 103)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_69:		/* Vector 0x69 (105) */
	svlcx
	mov		d5, MK_imm(#, 104)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_6a:		/* Vector 0x6a (106) */
	svlcx
	mov		d5, MK_imm(#, 105)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_6b:		/* Vector 0x6b (107) */
	svlcx
	mov		d5, MK_imm(#, 106)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_6c:		/* Vector 0x6c (108) */
	svlcx
	mov		d5, MK_imm(#, 107)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_6d:		/* Vector 0x6d (109) */
	svlcx
	mov		d5, MK_imm(#, 108)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_6e:		/* Vector 0x6e (110) */
	svlcx
	mov		d5, MK_imm(#, 109)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_6f:		/* Vector 0x6f (111) */
	svlcx
	mov		d5, MK_imm(#, 110)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_70:		/* Vector 0x70 (112) */
	svlcx
	mov		d5, MK_imm(#, 111)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_71:		/* Vector 0x71 (113) */
	svlcx
	mov		d5, MK_imm(#, 112)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_72:		/* Vector 0x72 (114) */
	svlcx
	mov		d5, MK_imm(#, 113)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_73:		/* Vector 0x73 (115) */
	svlcx
	mov		d5, MK_imm(#, 114)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_74:		/* Vector 0x74 (116) */
	svlcx
	mov		d5, MK_imm(#, 115)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_75:		/* Vector 0x75 (117) */
	svlcx
	mov		d5, MK_imm(#, 116)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_76:		/* Vector 0x76 (118) */
	svlcx
	mov		d5, MK_imm(#, 117)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_77:		/* Vector 0x77 (119) */
	svlcx
	mov		d5, MK_imm(#, 118)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_78:		/* Vector 0x78 (120) */
	svlcx
	mov		d5, MK_imm(#, 119)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_79:		/* Vector 0x79 (121) */
	svlcx
	mov		d5, MK_imm(#, 120)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_7a:		/* Vector 0x7a (122) */
	svlcx
	mov		d5, MK_imm(#, 121)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_7b:		/* Vector 0x7b (123) */
	svlcx
	mov		d5, MK_imm(#, 122)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_7c:		/* Vector 0x7c (124) */
	svlcx
	mov		d5, MK_imm(#, 123)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_7d:		/* Vector 0x7d (125) */
	svlcx
	mov		d5, MK_imm(#, 124)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_7e:		/* Vector 0x7e (126) */
	svlcx
	mov		d5, MK_imm(#, 125)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_7f:		/* Vector 0x7f (127) */
	svlcx
	mov		d5, MK_imm(#, 126)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_80:		/* Vector 0x80 (128) */
	svlcx
	mov		d5, MK_imm(#, 127)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_81:		/* Vector 0x81 (129) */
	svlcx
	mov		d5, MK_imm(#, 128)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_82:		/* Vector 0x82 (130) */
	svlcx
	mov		d5, MK_imm(#, 129)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_83:		/* Vector 0x83 (131) */
	svlcx
	mov		d5, MK_imm(#, 130)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_84:		/* Vector 0x84 (132) */
	svlcx
	mov		d5, MK_imm(#, 131)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_85:		/* Vector 0x85 (133) */
	svlcx
	mov		d5, MK_imm(#, 132)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_86:		/* Vector 0x86 (134) */
	svlcx
	mov		d5, MK_imm(#, 133)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_87:		/* Vector 0x87 (135) */
	svlcx
	mov		d5, MK_imm(#, 134)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_88:		/* Vector 0x88 (136) */
	svlcx
	mov		d5, MK_imm(#, 135)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_89:		/* Vector 0x89 (137) */
	svlcx
	mov		d5, MK_imm(#, 136)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_8a:		/* Vector 0x8a (138) */
	svlcx
	mov		d5, MK_imm(#, 137)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_8b:		/* Vector 0x8b (139) */
	svlcx
	mov		d5, MK_imm(#, 138)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_8c:		/* Vector 0x8c (140) */
	svlcx
	mov		d5, MK_imm(#, 139)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_8d:		/* Vector 0x8d (141) */
	svlcx
	mov		d5, MK_imm(#, 140)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_8e:		/* Vector 0x8e (142) */
	svlcx
	mov		d5, MK_imm(#, 141)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_8f:		/* Vector 0x8f (143) */
	svlcx
	mov		d5, MK_imm(#, 142)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_90:		/* Vector 0x90 (144) */
	svlcx
	mov		d5, MK_imm(#, 143)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_91:		/* Vector 0x91 (145) */
	svlcx
	mov		d5, MK_imm(#, 144)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_92:		/* Vector 0x92 (146) */
	svlcx
	mov		d5, MK_imm(#, 145)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_93:		/* Vector 0x93 (147) */
	svlcx
	mov		d5, MK_imm(#, 146)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_94:		/* Vector 0x94 (148) */
	svlcx
	mov		d5, MK_imm(#, 147)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_95:		/* Vector 0x95 (149) */
	svlcx
	mov		d5, MK_imm(#, 148)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_96:		/* Vector 0x96 (150) */
	svlcx
	mov		d5, MK_imm(#, 149)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_97:		/* Vector 0x97 (151) */
	svlcx
	mov		d5, MK_imm(#, 150)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_98:		/* Vector 0x98 (152) */
	svlcx
	mov		d5, MK_imm(#, 151)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_99:		/* Vector 0x99 (153) */
	svlcx
	mov		d5, MK_imm(#, 152)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_9a:		/* Vector 0x9a (154) */
	svlcx
	mov		d5, MK_imm(#, 153)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_9b:		/* Vector 0x9b (155) */
	svlcx
	mov		d5, MK_imm(#, 154)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_9c:		/* Vector 0x9c (156) */
	svlcx
	mov		d5, MK_imm(#, 155)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_9d:		/* Vector 0x9d (157) */
	svlcx
	mov		d5, MK_imm(#, 156)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_9e:		/* Vector 0x9e (158) */
	svlcx
	mov		d5, MK_imm(#, 157)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_9f:		/* Vector 0x9f (159) */
	svlcx
	mov		d5, MK_imm(#, 158)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_a0:		/* Vector 0xa0 (160) */
	svlcx
	mov		d5, MK_imm(#, 159)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_a1:		/* Vector 0xa1 (161) */
	svlcx
	mov		d5, MK_imm(#, 160)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_a2:		/* Vector 0xa2 (162) */
	svlcx
	mov		d5, MK_imm(#, 161)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_a3:		/* Vector 0xa3 (163) */
	svlcx
	mov		d5, MK_imm(#, 162)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_a4:		/* Vector 0xa4 (164) */
	svlcx
	mov		d5, MK_imm(#, 163)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_a5:		/* Vector 0xa5 (165) */
	svlcx
	mov		d5, MK_imm(#, 164)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_a6:		/* Vector 0xa6 (166) */
	svlcx
	mov		d5, MK_imm(#, 165)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_a7:		/* Vector 0xa7 (167) */
	svlcx
	mov		d5, MK_imm(#, 166)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_a8:		/* Vector 0xa8 (168) */
	svlcx
	mov		d5, MK_imm(#, 167)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_a9:		/* Vector 0xa9 (169) */
	svlcx
	mov		d5, MK_imm(#, 168)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_aa:		/* Vector 0xaa (170) */
	svlcx
	mov		d5, MK_imm(#, 169)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_ab:		/* Vector 0xab (171) */
	svlcx
	mov		d5, MK_imm(#, 170)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_ac:		/* Vector 0xac (172) */
	svlcx
	mov		d5, MK_imm(#, 171)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_ad:		/* Vector 0xad (173) */
	svlcx
	mov		d5, MK_imm(#, 172)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_ae:		/* Vector 0xae (174) */
	svlcx
	mov		d5, MK_imm(#, 173)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_af:		/* Vector 0xaf (175) */
	svlcx
	mov		d5, MK_imm(#, 174)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_b0:		/* Vector 0xb0 (176) */
	svlcx
	mov		d5, MK_imm(#, 175)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_b1:		/* Vector 0xb1 (177) */
	svlcx
	mov		d5, MK_imm(#, 176)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_b2:		/* Vector 0xb2 (178) */
	svlcx
	mov		d5, MK_imm(#, 177)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_b3:		/* Vector 0xb3 (179) */
	svlcx
	mov		d5, MK_imm(#, 178)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_b4:		/* Vector 0xb4 (180) */
	svlcx
	mov		d5, MK_imm(#, 179)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_b5:		/* Vector 0xb5 (181) */
	svlcx
	mov		d5, MK_imm(#, 180)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_b6:		/* Vector 0xb6 (182) */
	svlcx
	mov		d5, MK_imm(#, 181)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_b7:		/* Vector 0xb7 (183) */
	svlcx
	mov		d5, MK_imm(#, 182)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_b8:		/* Vector 0xb8 (184) */
	svlcx
	mov		d5, MK_imm(#, 183)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_b9:		/* Vector 0xb9 (185) */
	svlcx
	mov		d5, MK_imm(#, 184)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_ba:		/* Vector 0xba (186) */
	svlcx
	mov		d5, MK_imm(#, 185)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_bb:		/* Vector 0xbb (187) */
	svlcx
	mov		d5, MK_imm(#, 186)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_bc:		/* Vector 0xbc (188) */
	svlcx
	mov		d5, MK_imm(#, 187)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_bd:		/* Vector 0xbd (189) */
	svlcx
	mov		d5, MK_imm(#, 188)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_be:		/* Vector 0xbe (190) */
	svlcx
	mov		d5, MK_imm(#, 189)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_bf:		/* Vector 0xbf (191) */
	svlcx
	mov		d5, MK_imm(#, 190)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_c0:		/* Vector 0xc0 (192) */
	svlcx
	mov		d5, MK_imm(#, 191)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_c1:		/* Vector 0xc1 (193) */
	svlcx
	mov		d5, MK_imm(#, 192)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_c2:		/* Vector 0xc2 (194) */
	svlcx
	mov		d5, MK_imm(#, 193)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_c3:		/* Vector 0xc3 (195) */
	svlcx
	mov		d5, MK_imm(#, 194)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_c4:		/* Vector 0xc4 (196) */
	svlcx
	mov		d5, MK_imm(#, 195)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_c5:		/* Vector 0xc5 (197) */
	svlcx
	mov		d5, MK_imm(#, 196)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_c6:		/* Vector 0xc6 (198) */
	svlcx
	mov		d5, MK_imm(#, 197)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_c7:		/* Vector 0xc7 (199) */
	svlcx
	mov		d5, MK_imm(#, 198)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_c8:		/* Vector 0xc8 (200) */
	svlcx
	mov		d5, MK_imm(#, 199)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_c9:		/* Vector 0xc9 (201) */
	svlcx
	mov		d5, MK_imm(#, 200)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_ca:		/* Vector 0xca (202) */
	svlcx
	mov		d5, MK_imm(#, 201)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_cb:		/* Vector 0xcb (203) */
	svlcx
	mov		d5, MK_imm(#, 202)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_cc:		/* Vector 0xcc (204) */
	svlcx
	mov		d5, MK_imm(#, 203)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_cd:		/* Vector 0xcd (205) */
	svlcx
	mov		d5, MK_imm(#, 204)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_ce:		/* Vector 0xce (206) */
	svlcx
	mov		d5, MK_imm(#, 205)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_cf:		/* Vector 0xcf (207) */
	svlcx
	mov		d5, MK_imm(#, 206)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_d0:		/* Vector 0xd0 (208) */
	svlcx
	mov		d5, MK_imm(#, 207)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_d1:		/* Vector 0xd1 (209) */
	svlcx
	mov		d5, MK_imm(#, 208)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_d2:		/* Vector 0xd2 (210) */
	svlcx
	mov		d5, MK_imm(#, 209)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_d3:		/* Vector 0xd3 (211) */
	svlcx
	mov		d5, MK_imm(#, 210)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_d4:		/* Vector 0xd4 (212) */
	svlcx
	mov		d5, MK_imm(#, 211)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_d5:		/* Vector 0xd5 (213) */
	svlcx
	mov		d5, MK_imm(#, 212)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_d6:		/* Vector 0xd6 (214) */
	svlcx
	mov		d5, MK_imm(#, 213)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_d7:		/* Vector 0xd7 (215) */
	svlcx
	mov		d5, MK_imm(#, 214)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_d8:		/* Vector 0xd8 (216) */
	svlcx
	mov		d5, MK_imm(#, 215)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_d9:		/* Vector 0xd9 (217) */
	svlcx
	mov		d5, MK_imm(#, 216)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_da:		/* Vector 0xda (218) */
	svlcx
	mov		d5, MK_imm(#, 217)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_db:		/* Vector 0xdb (219) */
	svlcx
	mov		d5, MK_imm(#, 218)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_dc:		/* Vector 0xdc (220) */
	svlcx
	mov		d5, MK_imm(#, 219)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_dd:		/* Vector 0xdd (221) */
	svlcx
	mov		d5, MK_imm(#, 220)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_de:		/* Vector 0xde (222) */
	svlcx
	mov		d5, MK_imm(#, 221)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_df:		/* Vector 0xdf (223) */
	svlcx
	mov		d5, MK_imm(#, 222)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_e0:		/* Vector 0xe0 (224) */
	svlcx
	mov		d5, MK_imm(#, 223)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_e1:		/* Vector 0xe1 (225) */
	svlcx
	mov		d5, MK_imm(#, 224)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_e2:		/* Vector 0xe2 (226) */
	svlcx
	mov		d5, MK_imm(#, 225)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_e3:		/* Vector 0xe3 (227) */
	svlcx
	mov		d5, MK_imm(#, 226)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_e4:		/* Vector 0xe4 (228) */
	svlcx
	mov		d5, MK_imm(#, 227)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_e5:		/* Vector 0xe5 (229) */
	svlcx
	mov		d5, MK_imm(#, 228)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_e6:		/* Vector 0xe6 (230) */
	svlcx
	mov		d5, MK_imm(#, 229)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_e7:		/* Vector 0xe7 (231) */
	svlcx
	mov		d5, MK_imm(#, 230)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_e8:		/* Vector 0xe8 (232) */
	svlcx
	mov		d5, MK_imm(#, 231)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_e9:		/* Vector 0xe9 (233) */
	svlcx
	mov		d5, MK_imm(#, 232)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_ea:		/* Vector 0xea (234) */
	svlcx
	mov		d5, MK_imm(#, 233)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_eb:		/* Vector 0xeb (235) */
	svlcx
	mov		d5, MK_imm(#, 234)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_ec:		/* Vector 0xec (236) */
	svlcx
	mov		d5, MK_imm(#, 235)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_ed:		/* Vector 0xed (237) */
	svlcx
	mov		d5, MK_imm(#, 236)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_ee:		/* Vector 0xee (238) */
	svlcx
	mov		d5, MK_imm(#, 237)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_ef:		/* Vector 0xef (239) */
	svlcx
	mov		d5, MK_imm(#, 238)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_f0:		/* Vector 0xf0 (240) */
	svlcx
	mov		d5, MK_imm(#, 239)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_f1:		/* Vector 0xf1 (241) */
	svlcx
	mov		d5, MK_imm(#, 240)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_f2:		/* Vector 0xf2 (242) */
	svlcx
	mov		d5, MK_imm(#, 241)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_f3:		/* Vector 0xf3 (243) */
	svlcx
	mov		d5, MK_imm(#, 242)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_f4:		/* Vector 0xf4 (244) */
	svlcx
	mov		d5, MK_imm(#, 243)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_f5:		/* Vector 0xf5 (245) */
	svlcx
	mov		d5, MK_imm(#, 244)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_f6:		/* Vector 0xf6 (246) */
	svlcx
	mov		d5, MK_imm(#, 245)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_f7:		/* Vector 0xf7 (247) */
	svlcx
	mov		d5, MK_imm(#, 246)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_f8:		/* Vector 0xf8 (248) */
	svlcx
	mov		d5, MK_imm(#, 247)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_f9:		/* Vector 0xf9 (249) */
	svlcx
	mov		d5, MK_imm(#, 248)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_fa:		/* Vector 0xfa (250) */
	svlcx
	mov		d5, MK_imm(#, 249)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_fb:		/* Vector 0xfb (251) */
	svlcx
	mov		d5, MK_imm(#, 250)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_fc:		/* Vector 0xfc (252) */
	svlcx
	mov		d5, MK_imm(#, 251)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_fd:		/* Vector 0xfd (253) */
	svlcx
	mov		d5, MK_imm(#, 252)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_fe:		/* Vector 0xfe (254) */
	svlcx
	mov		d5, MK_imm(#, 253)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)
MK_InterruptVector_ff:		/* Vector 0xff (255) */
	svlcx
	mov		d5, MK_imm(#, 254)
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreDispatchInterruptSoft

	MK_align(32, 5)

/* Editor settings: DO NOT DELETE
 * vi:set ts=4:
*/
