/* Mk_TRICORE_initcsalist.c
 *
 * This file contains the special function MK_InitCsaList().
 *
 * The function must never be called from C code!
 *
 * The function is intended to be called only from assembly language code at startup. It expects
 * that the very first CSA from the CSA region defined by MK_RSA_MK_Csa and MK_RLA_MK_Csa is already
 * used to call this function. It then links the remaining space of that region to a new list of
 * CSAs that are used as freelist (FCX).
 *
 * Before calling, it is essential to ensure that the CSA memory cannot be modified by any
 * outside agency. Disabling interrupts guarantees this for the processor, but for
 * other bus masters such as DMA controllers and the other cores on a multi-core microcontroller
 * further measures may be necessary.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_initcsalist.c 15947 2014-04-28 10:16:48Z masa8317 $
*/

#include <public/Mk_public_types.h>
#include <private/Mk_panic.h>
#include <private/TRICORE/Mk_TRICORE_core.h>
#include <private/TRICORE/Mk_TRICORE_startup.h>
#include <private/TRICORE/Mk_TRICORE_compiler.h>
#include <private/TRICORE/Mk_TRICORE_exceptionfunctions.h>

/* MK_InitCsaList() - initialise the global CSA list, set FCX, LCX and PCXI accordingly.
 *
 * !LINKSTO Microkernel.TRICORE.Function.MK_InitCsaList, 1
 * !doctype src
*/
void MK_InitCsaList(void)
{
	/* This function is called before there is any stack available, hence all variables use the "register"
	 * keyword even though it is only a recommendation to the compiler. Since the Tricore has enough
	 * registers available, this should not be a problem. All compilers tested so far have always used
	 * registers here as expected.
	*/
	register mk_uint32_t cx;
	register mk_uint32_t fcx;
	register mk_uint32_t nCx;
	register mk_uint32_t limitIdx;
	register mk_uint32_t i;
	register mk_upperctx_t *ctx;

	/* Total number of contexts available
	 *
	 * This calculation may return an invalid value when the start/limit symbols are not properly aligned. This is,
	 * however, not a problem, since the result is not used before the correct alignment has been successfully tested.
	*/
	nCx = (((mk_uint32_t)&MK_RLA_MK_Csa) - ((mk_uint32_t)&MK_RSA_MK_Csa)) / sizeof(mk_upperctx_t);

	/* Check that CSA start and end addresses has correct alignment (64 bytes) and that
	 * start address is not greater (or equal) to end address. If the constraint on the start
	 * address is violated, it is possible that we didn't even arrive here.
	*/
	if ( ((((mk_uint32_t)&MK_RSA_MK_Csa) & (~MK_CSA_MASK)) != 0u) ||
	     ((((mk_uint32_t)&MK_RLA_MK_Csa) & (~MK_CSA_MASK)) != 0u) ||
		 (((mk_uint32_t)&MK_RSA_MK_Csa) >= ((mk_uint32_t)&MK_RLA_MK_Csa))
	   )
	{
		/* We are now calling the panic stop function despite knowing that there are no CSAs.
		 * Optimizing compilers are likely to implement this as an unconditional jump in which
		 * case everything will be fine. Otherwise we will end up in the FCU-exception.
		 * Either way the system will be sitting in an endless loop, waiting for the developer
		 * to fix the linker script.
		 *
		 * !LINKSTO Microkernel.TRICORE.Panic.MK_panic_MisalignedSymbol, 1
		 * !doctype src
		*/
		MK_StartupPanic(MK_panic_MisalignedSymbol);
	}
	else if ( nCx < MK_NCSAMIN )
	{
		/* Call to MK_StartupPanic may fail. See comment above.
		*/
		MK_StartupPanic(MK_panic_InsufficientResources);
	}
	else
	{
		/* Initialize CSAs starting from the second one (the first one has been used
		 * to call this function and is currently on the used-list).
		*/

		ctx = (mk_upperctx_t *)(void *)&MK_RSA_MK_Csa;

		/* FCX register needs to point to second element
		*/
		fcx = MK_AddrToPcx((mk_uint32_t)&(ctx[1]));
		cx = fcx;

		/* Now build rest of list (except last element which gets a special treatment)
		*/
		for ( i=1; i<(nCx-1); i++)
		{
			cx++;
			ctx[i].pcxi = cx;
		}

		/* Tail of list. Terminated using 0.
		*/
		ctx[i].pcxi = 0;

		/* Finally set FCX register to head of the newly created list.
		*/
		MK_MTFCX(fcx);

		/* Index of the CSA which will become LCX, counted from the second CSA.
		 * This CSA is followed by MK_CFG_NCSAEXTRA CSAs in the list.
		*/
		limitIdx = nCx - MK_CFG_NCSAEXTRA - 2u;

		/* Low water mark --> LCX register. An exception occurs if FCX reaches here.
		*/
		MK_MTLCX(fcx + limitIdx);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
