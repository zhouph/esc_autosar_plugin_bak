/* Mk_TRICORE_entry2.s - startup file for Tricore
 *
 * The code here performs what must be done before any C routines can be called.
 * This is the "safe boot" entry point. It can be used when another module such as a bootloader
 * has performed the hardware initialisation at boot time, such as register initialisation and
 * ECC RAM initialisation.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_entry2.s 15947 2014-04-28 10:16:48Z masa8317 $
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_ASM_STRUCT_020]
 *  Each assembly file shall contain exactly one return instruction, which is the last instruction.
 *
 * Reason:
 *  The code in this file is the startup code, not a function. MK_Entry2 is not expected to be called,
 *  so there is nowhere to return to. Furthermore, MK_Startup is not expected to return. If it should
 *  happen to return because of some hardware fault, the only thing we can do here is enter an endless
 *  loop and wait until a watchdog trips.
*/
/* Deviation DCG-1 <*> */

#include <private/Mk_asm.h>			/* Must be first */
#include <private/Mk_startup.h>
#include <public/TRICORE/Mk_TRICORE_characteristics.h>
#include <private/TRICORE/Mk_TRICORE_core.h>

	MK_file(Mk_TRICORE_entry2.s)

	MK_global	MK_Entry2

	MK_extern	MK_kernelSpInitial

	MK_extern	MK_StartupExceptionTable
	MK_extern	MK_WriteEndinit
	MK_extern	MK_InitCsaList
	MK_extern	MK_Startup
	MK_extern	MK_InterruptTable

	MK_extern	MK_INITIAL_A0
	MK_extern	MK_INITIAL_A1
	MK_extern	MK_INITIAL_A8
	MK_extern	MK_INITIAL_A9
	MK_extern	MK_RSA_MK_Csa

	MK_ASM_SDECL_TEXT
	MK_ASM_SECTION_TEXT
	MK_ASM_ALIGN_TEXT

/* MK_Entry2()
 *
 * !LINKSTO        Microkernel.Start.MK_Entry2, 1,
 * !               Microkernel.TRICORE.Function.Asm.MK_entry2, 1
 * !description    The processor runs this code first after a soft boot.
 * !doctype        src
 *
 * This is the entry point for the microkernel.
 *
 * Conditions for entry:
 *	- the processor is in supervisor mode. If not, the attempt to write to BTV will fail with a trap
 *    back to the caller's exception handling.
*/

MK_Entry2:

/* Preload the registers d12 to d15 with the 128-bit startup key. This key is passed through all the phases
 * of the startup, with XOR transformations each time, to make it very unlikely that the startup code can
 * be partially executed without being detected.
 *
 * !LINKSTO        Microkernel.Start.MK_Entry2.StartKey,1
 * !description    A Startup Key shall be passed from each phase of the startup to the next
 * !               with a transformation at each stage.
 * !doctype        src
 *
 * Disable interrupts
 *
 * Interrupt disabling is done interleaved with loading the key to reduce to near zero the likelihood
 * of loading the key and still having interrupts enabled.
*/
	disable
	mov		d12, MK_imm(#, MK_lo(MK_STARTKEY_1))
	addih	d12, d12, MK_imm(#, MK_hi(MK_STARTKEY_1))
	disable
	mov		d13, MK_imm(#, MK_lo(MK_STARTKEY_2))
	addih	d13, d13, MK_imm(#, MK_hi(MK_STARTKEY_2))
	disable
	mov		d14, MK_imm(#, MK_lo(MK_STARTKEY_3))
	addih	d14, d14, MK_imm(#, MK_hi(MK_STARTKEY_3))
	disable
	mov		d15, MK_imm(#, MK_lo(MK_STARTKEY_4))
	addih	d15, d15, MK_imm(#, MK_hi(MK_STARTKEY_4))
	disable

/* Set the stack pointer to 0x01000000. This will cause a virtual address or data access exception if the
 * stack gets used unexpectedly before it is safe to rely on the content of RAM.
*/
	movh.a	a10, MK_imm(#, 0x0100)

/* The next important step is to set up the startup exception vector table. This can, however
 * only be done if the ENDINIT flag is cleared. Clearing this flag in assembler is tedious and
 * cannot be done without loops, so it is implemented in C. This means we need to initialize
 * at least one CSA to be able to call a function
*/

/* Use start of CSA-area to set up FCX
*/
	mov     d4, MK_imm(#, MK_lo(MK_RSA_MK_Csa))			/* load address of first CSA */
	addih   d4, d4, MK_imm(#, MK_hi(MK_RSA_MK_Csa))

	mov.a	a4, d4										/* set PXCI of this CSA to zero */
	mov		d10, MK_imm(#, 0)
	st.w	[a4]0, d10

	/* now calculate FCX-value from CSA address */
	sh		d5, d4, MK_imm(#, -12)						/* d5 = d4 >> 12 */
	extr.u	d4, d4, MK_imm(#, 6), MK_imm(#, 16)			/* d4 = (d4 & 0x3fffc0) >> 6 */
	movh    d6, MK_imm(#, 0xf)							/* d6 = 0x000f0000 */
	and		d5, d6										/* d5 &= d6 */
	or		d4, d5										/* d4 |= d5 */

	mtcr	MK_imm(#, MK_FCX), d4

/* Set PCXI and LCX to 0
*/
	mtcr	MK_imm(#, MK_PCXI), d10
	mtcr	MK_imm(#, MK_LCX), d10

/* Initialize the PSW (value includes write permission for the global address registers).
*/
	mov		d4, MK_imm(#, MK_lo(MK_INITIAL_PSW))
	addih	d4, d4, MK_imm(#, MK_hi(MK_INITIAL_PSW))
	dsync
	mtcr	MK_imm(#, MK_PSW), d4
	isync

/* We may now call the C-function to clear the ENDINIT-flag */
	mov		d4, MK_imm(#, 0)
	call	MK_WriteEndinit

/* Initialize BTV with the base of the startup vector table.
*/
	mov		d4, MK_imm(#, MK_lo(MK_StartupExceptionTable))
	addih	d4, d4, MK_imm(#, MK_hi(MK_StartupExceptionTable))
	dsync
	mtcr	MK_imm(#, MK_BTV), d4
	isync

/* Initialize BIV with 0. This forces a virtual address or null pointer exception if an interrupt
 * occurs before the correct vector base is loaded.
*/
	mov		d4, MK_imm(#, 0)
	dsync
	mtcr	MK_imm(#, MK_BIV), d4
	isync

/* Set the COMPAT register to desired value
*/
	mov		d4, MK_imm(#, MK_lo(MK_COMPAT_VALUE))
	addih	d4, d4, MK_imm(#, MK_hi(MK_COMPAT_VALUE))
	dsync
	mtcr	MK_imm(#, MK_COMPAT), d4
	isync

/* Initialize ISP and place the same value in the stack pointer.
*/
	movh.a	a0, MK_imm(#, MK_hi(MK_kernelSpInitial))
	ld.w	d4, [a0]MK_lo(MK_kernelSpInitial)
	mov.a	a10, d4
	dsync
	mtcr	MK_imm(#, MK_ISP), d4
	isync

/* Initialize Icache enable */
	mov		d4, MK_imm(#, 0)
	mtcr	MK_imm(#, MK_PCON0), d4
	isync

/* set ENDINIT again */
	mov		d4, MK_imm(#, 1)
	call	MK_WriteEndinit

/* Initialize the global address registers a0, a1, a8 and a9
*/
	movh.a	a0, MK_imm(#, MK_hi(MK_INITIAL_A0))
	lea		a0, [a0]  MK_lo(MK_INITIAL_A0)

	movh.a	a1, MK_imm(#, MK_hi(MK_INITIAL_A1))
	lea		a1, [a0]  MK_lo(MK_INITIAL_A1)

	movh.a	a8, MK_imm(#, MK_hi(MK_INITIAL_A8))
	lea		a8, [a0]  MK_lo(MK_INITIAL_A8)

	movh.a	a9, MK_imm(#, MK_hi(MK_INITIAL_A9))
	lea		a9, [a0]  MK_lo(MK_INITIAL_A9)


/* Initialize the global free CSA list.
*/
	call		MK_InitCsaList

/* jump to MK_Startup()
 *
 * We now have a workable (although not quite complete) C environment,
 * so the remainder of the startup is controlled by the C function MK_Startup.
 *
 * Parameters to MK_Startup:
 *	p1 = k2 xor k3 = d13 xor d14 --> d4
 *	p2 = k3 xor k4 = d14 xor d15 --> d5
 *	p3 = k4 xor k1 = d15 xor d12 --> d6
 *	p4 = k1 xor k2 = d12 xor d13 --> d7
*/
	xor		d4, d13, d14
	xor		d5, d14, d15
	xor		d6, d15, d12
	xor		d7, d12, d13

	xor		d12, d12				/* Erase the original key, otherwise it could hang around in registers */
	xor		d13, d13
	xor		d14, d14
	xor		d15, d15

	j		MK_Startup

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
