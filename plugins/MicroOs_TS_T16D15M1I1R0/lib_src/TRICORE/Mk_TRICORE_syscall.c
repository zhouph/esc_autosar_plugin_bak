/* Mk_TRICORE_syscall.c
 *
 * This file contains the function MK_TricoreSyscall()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_syscall.c 15947 2014-04-28 10:16:48Z masa8317 $
*/
#include <public/Mk_basic_types.h>
#include <private/TRICORE/Mk_TRICORE_exceptionhandling.h>
#include <private/Mk_syscall.h>

#include <private/TRICORE/Mk_TRICORE_exceptionfunctions.h>

/* MK_TricoreSyscall()
 *
 * This function is entered via a jump instruction from the System Call exception vector.
 * The vector provides the thread's PCXI value and the system-call index as parameters.
 *
 * !LINKSTO Microkernel.TRICORE.Function.MK_TricoreSyscall, 1
 * !doctype src
*/
void MK_TricoreSyscall(mk_uint32_t pcxiValue, mk_uint32_t d15Value)
{
	MK_ExceptionHandlerCommon(pcxiValue, d15Value, MK_FALSE); /* Doesn't return if exception is not "sane" */

	MK_Syscall();
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
