/* Mk_TRICORE_usrstartforeignthread.s
 *
 * This file contains the function MK_UsrStartForeignThread(), MK_UsrStartForeignThread1()
 * MK_UsrStartForeignThread2(), MK_UsrStartForeignThread3()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_usrstartforeignthread.s 15947 2014-04-28 10:16:48Z masa8317 $
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_LAYOUT_020]
 *  Source files shall contain at most one routine having external linkage.
 *
 * Reason:
 *  This file contains four related microkernel API functions that are identical at the assembly-language level,
 *  and are aliased to the same code.
 *  The exception is permitted by the DCG.
*/
/* Deviation DCG-1 <*> */

#include <private/Mk_asm.h>		/* Must be first! */
#include <private/Mk_syscall.h>

	MK_file(Mk_TRICORE_usrstartforeignthread.s)

	MK_ASM_SDECL_TEXT
	MK_ASM_SECTION_TEXT
	MK_ASM_ALIGN_TEXT

	MK_global MK_UsrStartForeignThread
	MK_global MK_UsrStartForeignThread1
	MK_global MK_UsrStartForeignThread2
	MK_global MK_UsrStartForeignThread2V
	MK_global MK_UsrStartForeignThread3

/* !LINKSTO Microkernel.Interface.API.Direct.UsrStartForeignThread, 1
 * !doctype src
*/
MK_UsrStartForeignThread:
MK_UsrStartForeignThread1:
MK_UsrStartForeignThread2:
MK_UsrStartForeignThread2V:
MK_UsrStartForeignThread3:
	syscall	MK_imm(#, MK_SC_StartForeignThread)
	ret
