/* Mk_TRICORE_hwinitinterruptcontroller.c
 *
 * This file contains the function MK_HwInitInterruptController()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_hwinitinterruptcontroller.c 15947 2014-04-28 10:16:48Z masa8317 $
*/
#include <public/Mk_basic_types.h>
#include <private/TRICORE/Mk_TRICORE_interruptcontroller.h>
#include <private/TRICORE/Mk_TRICORE_wdt.h>


/* MK_HwInitInterruptController()
 *
 * This function programs the interrupt table's base address in the respective processor
 * register.
*/
void MK_HwInitInterruptController(void)
{
	/* BIV can only be programmed with ENDINIT disabled.
	*/
	MK_WriteEndinit(0);

	/* Call assembler function to set BIV with corrected table base address.
	*/
	MK_SetupInterruptTable(MK_INTVEC2BIV(&MK_InterruptTable));

	MK_WriteEndinit(1);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
