/* Mk_TRICORE_handlecontexttrap.c
 *
 * This file contains the function MK_TRICORE_HandleContextTrap()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_handlecontexttrap.c 15947 2014-04-28 10:16:48Z masa8317 $
*/
#include <public/Mk_basic_types.h>
#include <private/TRICORE/Mk_TRICORE_exceptionhandling.h>
#include <public/TRICORE/Mk_TRICORE_exceptioninfo.h>
#include <public/Mk_error.h>
#include <private/Mk_panic.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_exceptionhandling.h>
#include <private/Mk_thread.h>
#include <private/Mk_syscall.h>

#include <private/TRICORE/Mk_TRICORE_exceptionfunctions.h>

#define MK_TRICORE_RETURN_64BIT			0
#define MK_TRICORE_RETURN_POINTER		0

/* MK_HandleContextTrap()
 *
 * This function is entered via a jump instruction from the Context Management exception vector.
 * The vector provides the thread's PCXI value and the TIN as parameters.
 *
 * The Context List Underflow (PCX==0) exception is used by the microkernel to detect when a thread
 * returns from its top-level function. The remaining traps are reported. The two traps that report
 * a shortage of free CSAs must be treated as fatal.
 *
 * !LINKSTO Microkernel.TRICORE.Function.MK_HandleContextTrap, 1,
 * !        Microkernel.TRICORE.Exception.ReportProtectionFault, 1
 * !doctype src
*/
void MK_HandleContextTrap(mk_uint32_t pcxiValue, mk_uint32_t d15Value)
{
	MK_ExceptionHandlerCommon(pcxiValue, d15Value, MK_FALSE); /* Doesn't return if exception is not "sane" */

	if ( (d15Value == MK_TIN_ContextListUnderflowPcxEq0) )
	{
		/* Terminate the calling thread.
		 * This is performed by calling MK_SysTerminateSelf(), which expects the return value to be
		 * placed in the first parameter register. Before making the call the return value is copied
		 * from D2 to D4. This supports a single 32-bit scalar return value. To support 64-bit
		 * return values we would need to copy D3 to D5 as well. To support pointers we would need to
		 * copy A2 to A4 (and also A3 to A5 for 64-bit, e.g. structure)
		*/
#if MK_TRICORE_RETURN_POINTER == 0
		MK_threadCurrent->regs->lower->d4 = MK_threadCurrent->regs->lower->d2;
#if MK_TRICORE_RETURN_64BIT != 0
		MK_threadCurrent->regs->lower->d5 = MK_threadCurrent->regs->lower->d3;
#endif
#else
		MK_threadCurrent->regs->lower->a4 = MK_threadCurrent->regs->lower->a2;
#if MK_TRICORE_RETURN_64BIT != 0
		MK_threadCurrent->regs->lower->a5 = MK_threadCurrent->regs->lower->a3;
#endif
#endif
		MK_SysTerminateSelf();
	}
	else
	{
		/* all remaining exception types are considered an "exception"
		*/
		MK_FillExceptionInfo(MK_exc_ContextManagement, d15Value);

		/* Note: MK_TIN_FreeContextListUnderflowFcxEq0 can never reach this place, because it
		 *       triggers an endless loop in the assembler part of the exception handler
		*/
		if (d15Value == MK_TIN_FreeContextListDepletionFcxEqLcx)
		{
			/* Context list has reached low watermark.
			 * The only way to handle this is to shut down the system. Terminating
			 * all the running threads will free some CSAs to permit the shutdown to proceed.
			 * !LINKSTO Microkernel.TRICORE.Panic.MK_panic_ProcessorLimitExceeded, 1
			 * !doctype src
			*/
			MK_Panic(MK_panic_ProcessorLimitExceeded);
		}
		else
		if ( (d15Value == MK_TIN_CallDepthOverflow) ||
			 (d15Value == MK_TIN_CallDepthUnderflow) ||
			 (d15Value == MK_TIN_ContextTypePcxiUlWrong) ||
			 (d15Value == MK_TIN_NestingErrorRfeNonZeroCallDepth) )
		{
			MK_ReportProtectionFault(MK_sid_ExceptionHandling, MK_eid_CallNestingException);
		}
		else
		{
			/* !LINKSTO Microkernel.TRICORE.Panic.MK_panic_UndocumentedException, 1
			 * !doctype src
			*/
			MK_Panic(MK_panic_UndocumentedException);
		}
	}

	MK_Dispatch();
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
