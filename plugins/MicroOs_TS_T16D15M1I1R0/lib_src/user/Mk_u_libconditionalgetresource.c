/* Mk_u_libconditionalgetresource.c
 *
 * This file contains the MK_LibConditionalGetResource() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_u_libconditionalgetresource.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_api.h>
#include <public/Mk_error.h>
#include <private/Mk_resource.h>
#include <private/Mk_thread.h>
#include <public/Mk_autosar.h>

/* MK_LibConditionalGetResource()
 *
 * This function only gets the resource if the current priority of the
 * requesting thread is less than the ceiling priority of the resource.
 *
 * This function assumes global read-only access to the microkernel's variables.
 *
 * !LINKSTO      Microkernel.Function.MK_LibConditionalGetResource, 1
 * !doctype      src
*/
StatusType MK_LibConditionalGetResource(mk_objectid_t resourceId)
{
	StatusType errorCode = (StatusType)E_OS_NOFUNC;
	const mk_resourcecfg_t *resourceCfg;

	if ( (resourceId >= 0) && (resourceId < MK_nResources) )
	{
		resourceCfg = &MK_resourceCfg[resourceId];

		if ( MK_threadCurrent->currentPriority < resourceCfg->ceilingPriority )
		{
			errorCode = (StatusType)MK_UsrGetResource(resourceId);
		}
	}

	return errorCode;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
