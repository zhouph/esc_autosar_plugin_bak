/* Mk_u_liberrorgetparameter.c
 *
 * This file contains the MK_ErrorGetParameter() function.
 *
 * This function returns the specified parameter from the error-information structures 'culprit'.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_u_liberrorgetparameter.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_errorhandling.h>
#include <public/Mk_error.h>

/* MK_ErrorGetParameter() returns the specified parameter from the error culprit.
 *
 * CAVEAT: the service might not return valid information if called from outside the error hook.
 *
 * Parameter:
 *  paramNo - parameter number, 1..4
 *
 * !LINKSTO Microkernel.Function.MK_ErrorGetParameter, 1
 * !doctype src
*/
mk_parametertype_t MK_ErrorGetParameter(mk_int_fast16_t paramNo)
{
	mk_parametertype_t pVal;

	if ( MK_errorInfo == MK_NULL )
	{
		pVal = (mk_parametertype_t)(-1);
	}
	else
	{
		if ( (paramNo >= 1) && (paramNo <= 4) )
		{
			pVal = MK_errorInfo->parameter[paramNo-1];
		}
		else
		{
			pVal = (mk_parametertype_t)(-1);
		}
	}

	return pVal;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
