/* Mk_u_libdifftime32.c
 *
 * This file contains the MK_DiffTime32() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_u_libdifftime32.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <public/Mk_public_api.h>

/* MK_DiffTime32() - calculates the difference between two absolute times.
 *
 * void MK_DiffTime(mk_time_t *diffTime, const mk_time_t *newTime, const mk_time_t *oldTime)
 * calculates the difference between the old time as a saturating 32-bit value and returns
 * the result. "Saturating" means that if the 64-bit difference is too large to fit into
 * 32 bits, the return value is the maximum time rather the difference with underflow.
 *
 * !LINKSTO      Microkernel.Function.MK_DiffTime32, 1
 * !doctype      src
*/
mk_tick_t MK_DiffTime32(const mk_time_t *newTime, const mk_time_t *oldTime)
{
	mk_time_t diffTime;

	MK_DiffTime(&diffTime, newTime, oldTime);

	if ( diffTime.timeHi != 0 )
	{
		diffTime.timeLo = 0xffffffffu;
	}

	return diffTime.timeLo;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
