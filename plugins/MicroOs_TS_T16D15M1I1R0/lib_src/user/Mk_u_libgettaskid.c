/* Mk_u_libgettaskid.c
 *
 * This file contains the GetTaskID() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_u_libgettaskid.c 15837 2014-04-16 13:00:41Z dh $
*/
#include <public/Mk_public_types.h>
#include <public/Mk_public_api.h>
#include <public/Mk_autosar.h>
#include <private/Mk_thread.h>

/* GetTaskID() - Implements the AUTOSAR service.
 *
 * This function returns the ID of the current task.
 * If called from a task, the result is simply the caller's ID. This is derived from
 * the current thread.
 *
 * If the caller is not a task, the thread queue needs to be searched for the appropriate
 * thread. This is done by a system call to the microkernel via the MK_UsrGetTaskId API.
 * The function is implemented as a system call for concurrency reasons.
 * If the system call returns the status code is E_OK, the requested value is placed in
 * the referenced TaskType variable and the function returns the status code.
 *
 * !LINKSTO      Microkernel.Function.GetTaskID, 1
 * !doctype      src
*/
StatusType GetTaskID(TaskRefType taskIdRef)
{
	mk_statusandvalue_t syscallReturn;
	StatusType result;

	if ( MK_threadCurrent->objectType == MK_OBJTYPE_TASK )
	{
		*taskIdRef = (TaskType)MK_threadCurrent->currentObject;

		result = E_OK;
	}
	else
	{
		syscallReturn = MK_UsrGetTaskId((mk_parametertype_t)taskIdRef);

		if ( syscallReturn.statusCode == E_OK )
		{
			*taskIdRef = (TaskType)syscallReturn.requestedValue;
		}

		result = (StatusType)syscallReturn.statusCode;
	}

	return result;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
