/* Mk_TRICORE_entry.s - startup file for Tricore
 *
 * The code here performs what must be done after a cold start before any C routines can be called.
 * The code in the RESET section must be no more than 32 bytes, so that it can fit into the
 * unused entry 0 in the interrupt vector table on TC1xxx Tricores. This allows the vector table to
 * be placed at the start of the flash memory without leaving a large hole in the address space to
 * guarantee the alignment.
 *
 * Warning: This file has not been developed in accordance with a safety standard (no ASIL)!
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_entry.s 15947 2014-04-28 10:16:48Z masa8317 $
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_ASM_STRUCT_020]
 *  Each assembly file shall contain exactly one return instruction, which is the last instruction.
 *
 * Reason:
 *  The code in this file is the startup code, not a function. MK_Entry is not expected to be called;
 *  control is transferred there by the processor after a reset, so there is nowhere to return to.
 *  Furthermore, MK_Entry2 does not return.
*/
/* Deviation DCG-1 <*> */

#include <private/Mk_asm.h>		/* Must be first! */

	MK_file(Mk_TRICORE_entry.s)

	MK_global	MK_Entry
	MK_extern	MK_Entry2
	MK_extern	MK_InterruptTable		/* Forces MK_InterruptTable to be linked */

	MK_ASM_SDECL_RESET
	MK_ASM_SECTION_RESET

MK_Entry:
	disable
	j		MK_Entry2 
    

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
