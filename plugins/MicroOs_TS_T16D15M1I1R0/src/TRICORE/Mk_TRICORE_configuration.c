/* Mk_TRICORE_configuration.c
 *
 * Configuration constants for the TRICORE-specific part of the microkernel.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_configuration.c 18690 2015-03-05 12:13:57Z nibo2437 $
*/
#include <public/Mk_public_types.h>
#include <private/TRICORE/Mk_TRICORE_memoryprotection.h>
#include <private/TRICORE/Mk_TRICORE_stm.h>
#include <private/TRICORE/Mk_TRICORE_wdt.h>
#include <Mk_Cfg.h>

#if MK_TRICORE_CORE == MK_TRICORE_TC16
mk_uint32_t MK_MemoryPartitionDps1Val[] = MK_MEMPARTDPS1VALUES;
#elif MK_TRICORE_CORE == MK_TRICORE_TC161
mk_mpurwpermission_t MK_MemoryPartitionPermissions[] = MK_MEMPARTPERMISSIONS;
#endif

#if (MK_N_STMS > 1)
mk_tricorestm_t * const MK_stm = ((mk_tricorestm_t *)MK_TRICORE_TIMESTAMPSTMBASE);
#endif

#if ((MK_TRICORE_SCU == MK_SCU_TC2XX) && (MK_TRICORE_NCORES > 1))

#if (MK_TRICORE_CORE_ID >= MK_TRICORE_NCORES)
#error "Macro MK_TRICORE_CORE_ID refers to an invalid core for this derivative!"
#endif

mk_wdtcon_t * const MK_scu_wdtxcon = &(((mk_wdtcon_t *)MK_MODBASE_WDTX)[MK_TRICORE_CORE_ID]);

#endif /* ((MK_TRICORE_SCU == MK_SCU_TC2XX) && (MK_TRICORE_NCORES > 1)) */

#if (MK_TRICORE_NCORES > 1)
const mk_uint32_t MK_targetCore = MK_TRICORE_CORE_ID;
#endif

const mk_objquantity_t MK_nStaticPartitions = MK_NSTATICPARTITIONS;

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
