/* Mk_TRICORE_fillmpucache.c
 *
 * This file contains the function MK_FillMpuCache(). The function fills the MPU
 * region cache from the configured memory partitions
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_fillmpucache.c 18684 2015-03-05 08:19:53Z nibo2437 $
*/
#include <public/Mk_basic_types.h>
#include <public/Mk_public_types.h>
#include <private/Mk_memoryprotection.h>
#include <private/TRICORE/Mk_TRICORE_mpu.h>
#include <Mk_Cfg.h>

/* number of dynamic regions available (configuration dependent)
*/
#if MK_TRICORE_CORE == MK_TRICORE_TC161
#define MK_MEMPART_DYNAMIC_SIZE MK_DYNREGIONS_MAX
#else
#define MK_MEMPART_DYNAMIC_SIZE	(MK_TRICORE_NDPR-MK_MEMPART_GLOBAL_SIZE)
#endif

/* There are valid configurations without thread partitions or without dynamic regions.
 * In these cases no MPU switching occurs and the cache can be omitted.
*/
#if (MK_MEMPART_DYNAMIC_SIZE > 0) && (MK_NMEMORYPARTITIONS > MK_NSTATICPARTITIONS)
mk_mpubounds_t MK_MpuRegCache[(MK_NMEMORYPARTITIONS-MK_NSTATICPARTITIONS)*MK_MEMPART_DYNAMIC_SIZE];
#endif

/* MK_FillMpuCache() - Fills the MPU cache
 *
 * This function caches all dynamic MPU region registers for each thread memory partition (i.e., all but the
 * global and kernel partitions). If a partition contains too many regions, it silently ignores the excess
 * regions, knowing that the hardware-independent code will also detect this later (and halt the system
 * as a consequence).
 *
 * This function is empty when the configuration excludes dynamic MPU switching.
 *
 * !LINKSTO Microkernel.TRICORE.Function.MK_FillMpuCache, 1
 * !doctype src
*/
void MK_FillMpuCache(void)
{
#if (MK_MEMPART_DYNAMIC_SIZE > 0) && (MK_NMEMORYPARTITIONS > MK_NSTATICPARTITIONS)
	mk_int_t p;
	mk_int_t sr;
	mk_int_t dr;
	const mk_memorypartition_t *mempart;
	const mk_memoryregion_t *region;
	mk_mpubounds_t *partCache;

	for ( p=MK_NSTATICPARTITIONS; p<MK_NMEMORYPARTITIONS; p++ )
	{
		mempart = &MK_memoryPartitions[p];
		partCache = &MK_MpuRegCache[(p-MK_NSTATICPARTITIONS)*MK_MEMPART_DYNAMIC_SIZE];
		dr = 0;
		for ( sr=0; sr < mempart->nRegions; sr++ )
		{
			region = mempart->regionMap[sr].region;

			if ( (region - MK_memoryRegions) >= MK_NSTATICREGIONS )
			{
				if ( dr >= MK_MEMPART_DYNAMIC_SIZE )
				{
					/* !LINKSTO Microkernel.Panic.MK_panic_MemoryPartitionIsTooLarge, 1
					 * !doctype src
					*/
					MK_StartupPanic(MK_panic_MemoryPartitionIsTooLarge);
				}

				partCache[dr].lower = (mk_uint32_t)region->mr_startaddr;
				partCache[dr].upper = (mk_uint32_t)region->mr_limitaddr;
				dr++;
			}
		}
	}
#endif
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
