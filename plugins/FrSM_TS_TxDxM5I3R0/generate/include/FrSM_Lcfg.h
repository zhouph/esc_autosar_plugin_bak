/**
 * \file
 *
 * \brief AUTOSAR FrSM
 *
 * This file contains the implementation of the AUTOSAR
 * module FrSM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#ifndef _FRSM_LCFG_H_
#define _FRSM_LCFG_H_

/*
 * Include Section
 */

#include <FrSM.h>      /* FrSM interface headerfile */

/*
 * Global Function Declarations
 */

#define FRSM_START_SEC_CODE
#include <MemMap.h>  /* !LINKSTO FrSm.ASR40.FrSm057,1 */

[!LOOP "FrSMConfig/*[1]/FrSMCluster/*"!]
/**
 * \brief FrSM main function for FlexRay cluster with FrIfClstIdx [!"as:ref(./FrSMFrIfClusterRef)/FrIfClstIdx"!]
 *
 * \ServiceID{0x80}
 */
extern FUNC(void,FRSM_CODE) FrSM_MainFunction_[!"as:ref(./FrSMFrIfClusterRef)/FrIfClstIdx"!](void);
[!ENDLOOP!]

#define FRSM_STOP_SEC_CODE
#include <MemMap.h>  /* !LINKSTO FrSm.ASR40.FrSm057,1 */


#endif /* _FRSM_LCFG_H_ */


/*
 * = eof ======================================================================
 */
