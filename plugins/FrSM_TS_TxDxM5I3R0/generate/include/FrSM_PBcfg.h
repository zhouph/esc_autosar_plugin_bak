/**
 * \file
 *
 * \brief AUTOSAR FrSM
 *
 * This file contains the implementation of the AUTOSAR
 * module FrSM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#ifndef _FRSM_PBCFG_H_
#define _FRSM_PBCFG_H_

/*==================[includes]==============================================*/

#include <FrSM.h>      /* FrSM interface headerfile */

/*==================[macros]================================================*/

/** \brief Map name of post-build configuration to internal representation */
#define [!"FrSMConfig/*[1]/@name"!] ([!"FrSMConfig/*[1]/@name"!]Layout.RootCfg)

/*==================[type definitions]======================================*/

/** \brief Internal post-build configuration structure type */
typedef struct
{
    /** \brief Type passed to \a FrSM_Init() */
    const FrSM_ConfigType RootCfg;
[!IF "num:integer(count(FrSMConfig/*[1]/FrSMCluster/*)) > 1"!][!//
    /** \brief FlexRay cluster configurations, except the first one contained in \a RootCfg */
    const FrSM_ClstCfgType aClst[[!"num:integer(count(FrSMConfig/*[1]/FrSMCluster/*) - 1)"!]U];
[!ENDIF!][!//
}FrSM_CfgType;

/*==================[external constants]====================================*/

#define FRSM_START_SEC_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>  /* !LINKSTO FrSm.ASR40.FrSm057,1 */

/** \brief Internal post-build configuration structure */
extern CONST(FrSM_CfgType, FRSM_APPL_CONST) [!"FrSMConfig/*[1]/@name"!]Layout;

#define FRSM_STOP_SEC_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>  /* !LINKSTO FrSm.ASR40.FrSm057,1 */



#endif /* _FRSM_PBCFG_H_ */

/*==================[end of file]===========================================*/
