/**
 * \file
 *
 * \brief AUTOSAR FrSM
 *
 * This file contains the implementation of the AUTOSAR
 * module FrSM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined FRSM_TRACE_H)
#define FRSM_TRACE_H
/*==================[inclusions]============================================*/

#include <FrSM_Int.h>
[!IF "node:exists(as:modconf('Dbg'))"!]
#include <Dbg.h>

[!ENDIF!]
/*==================[macros]================================================*/

#ifndef DBG_FRSM_TICK_ENTRY
/** \brief Entry point of function FrSM_Tick() */
#define DBG_FRSM_TICK_ENTRY(a,b,c,d,e)
#endif

#ifndef DBG_FRSM_TICK_EXIT
/** \brief Exit point of function FrSM_Tick() */
#define DBG_FRSM_TICK_EXIT(a,b,c,d,e)
#endif

#ifndef DBG_FRSM_FE_DEM_SYNC_LOSS_ENTRY
/** \brief Entry point of function FrSM_FE_DEM_SYNC_LOSS() */
#define DBG_FRSM_FE_DEM_SYNC_LOSS_ENTRY(a)
#endif

#ifndef DBG_FRSM_FE_DEM_SYNC_LOSS_EXIT
/** \brief Exit point of function FrSM_FE_DEM_SYNC_LOSS() */
#define DBG_FRSM_FE_DEM_SYNC_LOSS_EXIT(a)
#endif

#ifndef DBG_FRSM_FE_DEM_SYNC_LOSS_PASSED_ENTRY
/** \brief Entry point of function FrSM_FE_DEM_SYNC_LOSS_PASSED() */
#define DBG_FRSM_FE_DEM_SYNC_LOSS_PASSED_ENTRY(a)
#endif

#ifndef DBG_FRSM_FE_DEM_SYNC_LOSS_PASSED_EXIT
/** \brief Exit point of function FrSM_FE_DEM_SYNC_LOSS_PASSED() */
#define DBG_FRSM_FE_DEM_SYNC_LOSS_PASSED_EXIT(a)
#endif

#ifndef DBG_FRSM_HANDLETRANSITIONS_T30_T31_T32_T33_ENTRY
/** \brief Entry point of function FrSM_HandleTransitions_T30_T31_T32_T33() */
#define DBG_FRSM_HANDLETRANSITIONS_T30_T31_T32_T33_ENTRY(a,b,c)
#endif

#ifndef DBG_FRSM_HANDLETRANSITIONS_T30_T31_T32_T33_EXIT
/** \brief Exit point of function FrSM_HandleTransitions_T30_T31_T32_T33() */
#define DBG_FRSM_HANDLETRANSITIONS_T30_T31_T32_T33_EXIT(a,b,c)
#endif

#ifndef DBG_FRSM_DOTRANSITION_T03_ENTRY
/** \brief Entry point of function FrSM_DoTransition_T03() */
#define DBG_FRSM_DOTRANSITION_T03_ENTRY(a,b,c)
#endif

#ifndef DBG_FRSM_DOTRANSITION_T03_EXIT
/** \brief Exit point of function FrSM_DoTransition_T03() */
#define DBG_FRSM_DOTRANSITION_T03_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRSM_HANDLETRANSITIONS_T10AB_T16AB_T20AB_ENTRY
/** \brief Entry point of function FrSM_HandleTransitions_T10ab_T16ab_T20ab() */
#define DBG_FRSM_HANDLETRANSITIONS_T10AB_T16AB_T20AB_ENTRY(a,b,c,d,e)
#endif

#ifndef DBG_FRSM_HANDLETRANSITIONS_T10AB_T16AB_T20AB_EXIT
/** \brief Exit point of function FrSM_HandleTransitions_T10ab_T16ab_T20ab() */
#define DBG_FRSM_HANDLETRANSITIONS_T10AB_T16AB_T20AB_EXIT(a,b,c,d,e,f)
#endif

#ifndef DBG_FRSM_CONDITIONWUREASON_ENTRY
/** \brief Entry point of function FrSM_ConditionWUReason() */
#define DBG_FRSM_CONDITIONWUREASON_ENTRY(a,b,c)
#endif

#ifndef DBG_FRSM_CONDITIONWUREASON_EXIT
/** \brief Exit point of function FrSM_ConditionWUReason() */
#define DBG_FRSM_CONDITIONWUREASON_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRSM_INIT_ENTRY
/** \brief Entry point of function FrSM_Init() */
#define DBG_FRSM_INIT_ENTRY(a)
#endif

#ifndef DBG_FRSM_PASSIVESTATE
/** \brief Change of FrSM_PassiveState */
#define DBG_FRSM_PASSIVESTATE(a,b)
#endif

#ifndef DBG_FRSM_INITSTATUS
/** \brief Change of FrSM_InitStatus */
#define DBG_FRSM_INITSTATUS(a,b)
#endif

#ifndef DBG_FRSM_INIT_EXIT
/** \brief Exit point of function FrSM_Init() */
#define DBG_FRSM_INIT_EXIT(a)
#endif

#ifndef DBG_FRSM_REQUESTCOMMODE_ENTRY
/** \brief Entry point of function FrSM_RequestComMode() */
#define DBG_FRSM_REQUESTCOMMODE_ENTRY(a,b)
#endif

#ifndef DBG_FRSM_REQUESTCOMMODE_EXIT
/** \brief Exit point of function FrSM_RequestComMode() */
#define DBG_FRSM_REQUESTCOMMODE_EXIT(a,b,c)
#endif

#ifndef DBG_FRSM_GETCURRENTCOMMODE_ENTRY
/** \brief Entry point of function FrSM_GetCurrentComMode() */
#define DBG_FRSM_GETCURRENTCOMMODE_ENTRY(a,b)
#endif

#ifndef DBG_FRSM_GETCURRENTCOMMODE_EXIT
/** \brief Exit point of function FrSM_GetCurrentComMode() */
#define DBG_FRSM_GETCURRENTCOMMODE_EXIT(a,b,c)
#endif

#ifndef DBG_FRSM_FE_TRCV_STANDBY_ENTRY
/** \brief Entry point of function FrSM_FE_TRCV_STANDBY() */
#define DBG_FRSM_FE_TRCV_STANDBY_ENTRY(a,b)
#endif

#ifndef DBG_FRSM_FE_TRCV_STANDBY_EXIT
/** \brief Exit point of function FrSM_FE_TRCV_STANDBY() */
#define DBG_FRSM_FE_TRCV_STANDBY_EXIT(a,b)
#endif

#ifndef DBG_FRSM_FE_TRCV_NORMAL_ENTRY
/** \brief Entry point of function FrSM_FE_TRCV_NORMAL() */
#define DBG_FRSM_FE_TRCV_NORMAL_ENTRY(a,b)
#endif

#ifndef DBG_FRSM_FE_TRCV_NORMAL_EXIT
/** \brief Exit point of function FrSM_FE_TRCV_NORMAL() */
#define DBG_FRSM_FE_TRCV_NORMAL_EXIT(a,b)
#endif

#ifndef DBG_FRSM_HANDLESTATE_FRSM_STATE_HALT_REQ_ENTRY
/** \brief Entry point of function FrSM_HandleState_FRSM_STATE_HALT_REQ() */
#define DBG_FRSM_HANDLESTATE_FRSM_STATE_HALT_REQ_ENTRY(a,b,c)
#endif

#ifndef DBG_FRSM_HANDLESTATE_FRSM_STATE_HALT_REQ_EXIT
/** \brief Exit point of function FrSM_HandleState_FRSM_STATE_HALT_REQ() */
#define DBG_FRSM_HANDLESTATE_FRSM_STATE_HALT_REQ_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRSM_HANDLESTATE_FRSM_STATE_READY_ENTRY
/** \brief Entry point of function FrSM_HandleState_FRSM_STATE_READY() */
#define DBG_FRSM_HANDLESTATE_FRSM_STATE_READY_ENTRY(a,b,c,d)
#endif

#ifndef DBG_FRSM_HANDLESTATE_FRSM_STATE_READY_EXIT
/** \brief Exit point of function FrSM_HandleState_FRSM_STATE_READY() */
#define DBG_FRSM_HANDLESTATE_FRSM_STATE_READY_EXIT(a,b,c,d,e)
#endif

#ifndef DBG_FRSM_HANDLESTATE_FRSM_STATE_WAKEUP_ENTRY
/** \brief Entry point of function FrSM_HandleState_FRSM_STATE_WAKEUP() */
#define DBG_FRSM_HANDLESTATE_FRSM_STATE_WAKEUP_ENTRY(a,b,c,d,e)
#endif

#ifndef DBG_FRSM_HANDLESTATE_FRSM_STATE_WAKEUP_EXIT
/** \brief Exit point of function FrSM_HandleState_FRSM_STATE_WAKEUP() */
#define DBG_FRSM_HANDLESTATE_FRSM_STATE_WAKEUP_EXIT(a,b,c,d,e,f)
#endif

#ifndef DBG_FRSM_HANDLESTATE_FRSM_STATE_STARTUP_ENTRY
/** \brief Entry point of function FrSM_HandleState_FRSM_STATE_STARTUP() */
#define DBG_FRSM_HANDLESTATE_FRSM_STATE_STARTUP_ENTRY(a,b,c,d,e,f,g)
#endif

#ifndef DBG_FRSM_HANDLESTATE_FRSM_STATE_STARTUP_EXIT
/** \brief Exit point of function FrSM_HandleState_FRSM_STATE_STARTUP() */
#define DBG_FRSM_HANDLESTATE_FRSM_STATE_STARTUP_EXIT(a,b,c,d,e,f,g,h)
#endif

#ifndef DBG_FRSM_HANDLESTATE_FRSM_STATE_ONLINE_PASSIVE_ENTRY
/** \brief Entry point of function FrSM_HandleState_FRSM_STATE_ONLINE_PASSIVE() */
#define DBG_FRSM_HANDLESTATE_FRSM_STATE_ONLINE_PASSIVE_ENTRY(a,b,c,d)
#endif

#ifndef DBG_FRSM_HANDLESTATE_FRSM_STATE_ONLINE_PASSIVE_EXIT
/** \brief Exit point of function FrSM_HandleState_FRSM_STATE_ONLINE_PASSIVE() */
#define DBG_FRSM_HANDLESTATE_FRSM_STATE_ONLINE_PASSIVE_EXIT(a,b,c,d,e)
#endif

#ifndef DBG_FRSM_HANDLESTATE_FRSM_STATE_ONLINE_ENTRY
/** \brief Entry point of function FrSM_HandleState_FRSM_STATE_ONLINE() */
#define DBG_FRSM_HANDLESTATE_FRSM_STATE_ONLINE_ENTRY(a,b,c,d)
#endif

#ifndef DBG_FRSM_HANDLESTATE_FRSM_STATE_ONLINE_EXIT
/** \brief Exit point of function FrSM_HandleState_FRSM_STATE_ONLINE() */
#define DBG_FRSM_HANDLESTATE_FRSM_STATE_ONLINE_EXIT(a,b,c,d,e)
#endif

#ifndef DBG_FRSM_HANDLESTATE_FRSM_STATE_KEYSLOT_ONLY_ENTRY
/** \brief Entry point of function FrSM_HandleState_FRSM_STATE_KEYSLOT_ONLY() */
#define DBG_FRSM_HANDLESTATE_FRSM_STATE_KEYSLOT_ONLY_ENTRY(a,b,c,d)
#endif

#ifndef DBG_FRSM_HANDLESTATE_FRSM_STATE_KEYSLOT_ONLY_EXIT
/** \brief Exit point of function FrSM_HandleState_FRSM_STATE_KEYSLOT_ONLY() */
#define DBG_FRSM_HANDLESTATE_FRSM_STATE_KEYSLOT_ONLY_EXIT(a,b,c,d,e)
#endif

#ifndef DBG_FRSM_MAINFUNCTIONINTERNAL_ENTRY
/** \brief Entry point of function FrSM_MainFunctionInternal() */
#if (FRSM_SINGLE_CLST_OPT_ENABLE != STD_ON)
#define DBG_FRSM_MAINFUNCTIONINTERNAL_ENTRY(a)
#else
#define DBG_FRSM_MAINFUNCTIONINTERNAL_ENTRY()
#endif
#endif

#ifndef DBG_FRSM_MAINFUNCTIONINTERNAL_EXIT
/** \brief Exit point of function FrSM_MainFunctionInternal() */
#if (FRSM_SINGLE_CLST_OPT_ENABLE != STD_ON)
#define DBG_FRSM_MAINFUNCTIONINTERNAL_EXIT(a)
#else
#define DBG_FRSM_MAINFUNCTIONINTERNAL_EXIT(a)
#endif
#endif

#ifndef DBG_FRSM_MAINFUNCTION_ENTRY
/** \brief Entry point of function FrSM_MainFunction() */
#define DBG_FRSM_MAINFUNCTION_ENTRY(a)
#endif

#ifndef DBG_FRSM_MAINFUNCTION_EXIT
/** \brief Exit point of function FrSM_MainFunction() */
#define DBG_FRSM_MAINFUNCTION_EXIT(a)
#endif

#ifndef DBG_FRSM_GETVERSIONINFO_ENTRY
/** \brief Entry point of function FrSM_GetVersionInfo() */
#define DBG_FRSM_GETVERSIONINFO_ENTRY(a)
#endif

#ifndef DBG_FRSM_GETVERSIONINFO_EXIT
/** \brief Exit point of function FrSM_GetVersionInfo() */
#define DBG_FRSM_GETVERSIONINFO_EXIT(a)
#endif

#ifndef DBG_FRSM_SETECUPASSIVE_ENTRY
/** \brief Entry point of function FrSM_SetEcuPassive() */
#define DBG_FRSM_SETECUPASSIVE_ENTRY(a)
#endif

#ifndef DBG_FRSM_SETECUPASSIVE_EXIT
/** \brief Exit point of function FrSM_SetEcuPassive() */
#define DBG_FRSM_SETECUPASSIVE_EXIT(a,b)
#endif

#ifndef DBG_FRSM_ALLSLOTS_ENTRY
/** \brief Entry point of function FrSM_AllSlots() */
#define DBG_FRSM_ALLSLOTS_ENTRY(a)
#endif

#ifndef DBG_FRSM_ALLSLOTS_EXIT
/** \brief Exit point of function FrSM_AllSlots() */
#define DBG_FRSM_ALLSLOTS_EXIT(a,b)
#endif

#ifndef DBG_FRSM_REQUESTEDSTATE_GRP
#define DBG_FRSM_REQUESTEDSTATE_GRP(a,b,c)
#endif

#ifndef DBG_FRSM_STATE_GRP
#define DBG_FRSM_STATE_GRP(a,b,c)
#endif

#ifndef DBG_FRSM_LOOKUPCLSTIDX_ENTRY
/** \brief Entry point of function FrSM_LookupClstIdx() */
#define DBG_FRSM_LOOKUPCLSTIDX_ENTRY(a)
#endif

#ifndef DBG_FRSM_LOOKUPCLSTIDX_EXIT
/** \brief Exit point of function FrSM_LookupClstIdx() */
#define DBG_FRSM_LOOKUPCLSTIDX_EXIT(a,b)
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[external data]=========================================*/

#endif /* (!defined FRSM_TRACE_H) */
/*==================[end of file]===========================================*/
