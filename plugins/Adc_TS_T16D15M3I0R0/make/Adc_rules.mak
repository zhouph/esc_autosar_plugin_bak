# \file
#
# \brief AUTOSAR Adc
#
# This file contains the implementation of the AUTOSAR
# module Adc.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Adc_src

Adc_src_FILES       += $(Adc_CORE_PATH)\src\Adc.c
Adc_src_FILES       += $(Adc_CORE_PATH)\src\Adc_Calibration.c
Adc_src_FILES       += $(Adc_CORE_PATH)\src\Adc_ConvHandle.c
Adc_src_FILES       += $(Adc_CORE_PATH)\src\Adc_HwHandle.c
Adc_src_FILES       += $(Adc_CORE_PATH)\src\Adc_Ver.c
Adc_src_FILES       += $(Adc_CORE_PATH)\src\Adc_Irq.c
Adc_src_FILES       += $(Adc_OUTPUT_PATH)\src\Adc_PBCfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Adc_src_FILES :=
endif
