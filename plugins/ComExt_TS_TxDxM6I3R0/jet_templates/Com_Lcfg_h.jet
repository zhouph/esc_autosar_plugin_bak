<%@ jet package="dreisoft.tresos.com.generated.jet" class="Com_Lcfg_h"
        imports="dreisoft.tresos.generator.ng.api.ant.JavaGenContext
            dreisoft.tresos.com.generator.ComLCfgDataModel
            dreisoft.tresos.com.generator.Com_ConfigLayoutType
            java.io.IOException
            dreisoft.tresos.datamodel2.api.model.DCtxt
            dreisoft.tresos.com.generator.ComCfgDataModel
            dreisoft.tresos.com.generator.GeneratorHelper
            dreisoft.tresos.com.generator.input.entity.Cache
            dreisoft.tresos.com.generated.operationstatus.GeneratorOperationStatus" %>
<%
    JavaGenContext context = null;
    try {
        context = JavaGenContext.get( argument, stringBuffer );
    } catch( IOException e ) {
        e.printStackTrace();
        return "";
    }

    DCtxt comDCtxt = context.gen.getModuleConfigurationDCtxtVariable( "Com" );
    boolean comGenerationSuccessful = !context.gen.isVerifying();
    long comPBRAMSize;

    ComLCfgDataModel comLCfgDataModel = new ComLCfgDataModel(comDCtxt);
    Cache cache = (Cache) context.gen.getVariable("Com.cache", Cache.class, null);
    ComCfgDataModel comCfgDataModel = (ComCfgDataModel) context.gen.getVariable("comCfgDataModel", ComCfgDataModel.class, null);
    Com_ConfigLayoutType comPBCfgDataModel = (Com_ConfigLayoutType) context.gen.getVariable("comPBCfgDataModel", Com_ConfigLayoutType.class, null);

    context.gen.setVariable("comLCfgDataModel", comLCfgDataModel);

    // if maximum PB RAM size if given => use it; otherwise use required size
    if (comCfgDataModel.getComDataMemSize() > 0) {
        comPBRAMSize = comCfgDataModel.getComDataMemSize();
    }
    else {
        comPBRAMSize = comPBCfgDataModel.getRequiredPBRAMSize();
    }

%>
#if (!defined COM_LCFG_H)
#define COM_LCFG_H

<%@ include file="AutoCoreHeader.jet" %>
/*==================[includes]==============================================*/

#include <TSAutosar.h>         /* EB specific standard types */
#include <Com_Api.h>           /* Module interface */
#include <Com_Callout.h>
#include <Com_Cbk.h>
#include <Com_Cfg.h>


/*==================[macros]================================================*/

/**
 * Size of internal Com data in units of bytes (static memory allocation).
 * Memory required by post-build configuration must be smaller than this constant.
 */
#define COM_DATA_MEM_SIZE <%= comPBRAMSize %>

#if (COM_RAM_SIZE_MAX < COM_DATA_MEM_SIZE)
#error (COM_RAM_SIZE_MAX < COM_DATA_MEM_SIZE)
#endif /* #if (COM_RAM_SIZE_MAX < COM_DATA_MEM_SIZE) */


#if (defined COM_TX_CALL_OUT_FUNC_PTR_ARRAY_SIZE) /* To prevent double declaration */
#error COM_TX_CALL_OUT_FUNC_PTR_ARRAY_SIZE already defined
#endif /* if (defined COM_TX_CALL_OUT_FUNC_PTR_ARRAY_SIZE) */

/** \brief Define COM_TX_CALL_OUT_FUNC_PTR_ARRAY_SIZE */
#define COM_TX_CALL_OUT_FUNC_PTR_ARRAY_SIZE <%= comPBCfgDataModel.getTxIPDUCallOuts().size() %>U

#if (defined COM_RX_CALL_OUT_FUNC_PTR_ARRAY_SIZE) /* To prevent double declaration */
#error COM_RX_CALL_OUT_FUNC_PTR_ARRAY_SIZE already defined
#endif /* if (defined COM_RX_CALL_OUT_FUNC_PTR_ARRAY_SIZE) */

/** \brief Define COM_RX_CALL_OUT_FUNC_PTR_ARRAY_SIZE */
#define COM_RX_CALL_OUT_FUNC_PTR_ARRAY_SIZE <%= comPBCfgDataModel.getRxIPDUCallOuts().size() %>U


#if (defined COM_CBK_TX_ACK_PTR_ARRAY_SIZE) /* To prevent double declaration */
#error COM_CBK_TX_ACK_PTR_ARRAY_SIZE already defined
#endif /* if (defined COM_CBK_TX_ACK_PTR_ARRAY_SIZE) */

/** \brief Define COM_CBK_TX_ACK_PTR_ARRAY_SIZE */
#define COM_CBK_TX_ACK_PTR_ARRAY_SIZE <%= comPBCfgDataModel.getSigTxAckNotifications().size() %>U


#if (defined COM_CBK_RX_ACK_PTR_ARRAY_SIZE) /* To prevent double declaration */
#error COM_CBK_RX_ACK_PTR_ARRAY_SIZE already defined
#endif /* if (defined COM_CBK_RX_ACK_PTR_ARRAY_SIZE) */

/** \brief Define COM_CBK_RX_ACK_PTR_ARRAY_SIZE */
#define COM_CBK_RX_ACK_PTR_ARRAY_SIZE <%= comPBCfgDataModel.getSigRxAckNotifications().size() %>U


#if (defined COM_CBK_TX_T_OUT_ARRAY_SIZE) /* To prevent double declaration */
#error COM_CBK_TX_T_OUT_ARRAY_SIZE already defined
#endif /* if (defined COM_CBK_TX_T_OUT_ARRAY_SIZE) */

/** \brief Define COM_CBK_TX_T_OUT_ARRAY_SIZE */
#define COM_CBK_TX_T_OUT_ARRAY_SIZE <%= comPBCfgDataModel.getSigTxTOutNotifications().size() %>U


#if (defined COM_CBK_RX_T_OUT_ARRAY_SIZE) /* To prevent double declaration */
#error COM_CBK_RX_T_OUT_ARRAY_SIZE already defined
#endif /* if (defined COM_CBK_RX_T_OUT_ARRAY_SIZE) */

/** \brief Define COM_CBK_RX_T_OUT_ARRAY_SIZE */
#define COM_CBK_RX_T_OUT_ARRAY_SIZE <%= comPBCfgDataModel.getSigRxTOutNotifications().size() %>U


#if (defined COM_CBK_TX_ERR_PTR_ARRAY_SIZE) /* To prevent double declaration */
#error COM_CBK_TX_ERR_PTR_ARRAY_SIZE already defined
#endif /* if (defined COM_CBK_TX_ERR_PTR_ARRAY_SIZE) */

/** \brief Define COM_CBK_TX_ERR_PTR_ARRAY_SIZE */
#define COM_CBK_TX_ERR_PTR_ARRAY_SIZE <%= comPBCfgDataModel.getSigTxErrNotifications().size() %>U


#if (defined COM_LCFG_SIGNATURE) /* To prevent double declaration */
#error COM_LCFG_SIGNATURE already defined
#endif /* if (defined COM_LCFG_SIGNATURE) */

/** \brief Define COM_LCFG_SIGNATURE */
#define COM_LCFG_SIGNATURE <%= GeneratorHelper.hex( cache.properties.signatures.linktimeSignature ) %>U

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/** @} doxygen end group definition */
#endif /* if !defined( COM_LCFG_H ) */
/*==================[end of file]===========================================*/
<%
if (comGenerationSuccessful == true)
{
    context.gen.addMessage( GeneratorOperationStatus.GENERATED_FILE( "Com_Lcfg.h" ) );
}
else {
    return "";
}

%>
