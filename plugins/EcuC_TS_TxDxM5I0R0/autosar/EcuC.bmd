<?xml version='1.0'?>
<AUTOSAR xmlns="http://autosar.org/schema/r4.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://autosar.org/schema/r4.0 AUTOSAR_4-0-3.xsd">
  <AR-PACKAGES>
    <AR-PACKAGE>
      <SHORT-NAME>TS_TxDxM5I0R0</SHORT-NAME>
      <ELEMENTS>
        <ECUC-MODULE-DEF UUID="ECUC:f72efedf-ee71-4a1b-b4d6-b77adfbf4cf2">
          <SHORT-NAME>EcuC</SHORT-NAME>
          <DESC>
            <L-2 L="FOR-ALL">&lt;html&gt;
                        Virtual module to collect ECU Configuration specific / global configuration information.
                      &lt;/html&gt;</L-2>
          </DESC>
          <ADMIN-DATA>
            <LANGUAGE>EN</LANGUAGE>
            <DOC-REVISIONS>
              <DOC-REVISION>
                <REVISION-LABEL>4.2.0</REVISION-LABEL>
                <ISSUED-BY>AUTOSAR</ISSUED-BY>
                <DATE>2011-11-09T11:36:22Z</DATE>
              </DOC-REVISION>
              <DOC-REVISION>
                <REVISION-LABEL>5.0.5</REVISION-LABEL>
                <ISSUED-BY>Elektrobit Automotive GmbH</ISSUED-BY>
                <DATE>2013-02-28T23:59:59Z</DATE>
              </DOC-REVISION>
            </DOC-REVISIONS>
          </ADMIN-DATA>
          <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
          <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
          <SUPPORTED-CONFIG-VARIANTS>
            <SUPPORTED-CONFIG-VARIANT>VARIANT-POST-BUILD</SUPPORTED-CONFIG-VARIANT>
          </SUPPORTED-CONFIG-VARIANTS>
          <CONTAINERS>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ECUC:a00be3e0-8783-9123-2d52-1eb616737ca6">
              <SHORT-NAME>CommonPublishedInformation</SHORT-NAME>
              <DESC>
                <L-2 L="EN">
                    &lt;html&gt;
                      Common container, aggregated by all modules. It contains published information about vendor and versions.
                  &lt;/html&gt;</L-2>
              </DESC>
              <PARAMETERS>
                <ECUC-INTEGER-PARAM-DEF UUID="ECUC:948f3f2e-f129-4bc5-b4e1-7a7bdb8599e1">
                  <SHORT-NAME>ArMajorVersion</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Major version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>3</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="ECUC:2893b920-59d5-4ac2-b2c1-e23742e66d70">
                  <SHORT-NAME>ArMinorVersion</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Minor version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>2</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="ECUC:6428eb9b-8790-488a-b9a3-0fba52d0f59e">
                  <SHORT-NAME>ArPatchVersion</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Patch level version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="ECUC:605b18ae-3f9a-41d4-9225-67c9c5f6fc34">
                  <SHORT-NAME>SwMajorVersion</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Major version number of the vendor specific implementation of the module.
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>5</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="a7fe44dc-ea25-4b8d-8fad-9fbcae86d56f">
                  <SHORT-NAME>SwMinorVersion</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Minor version number of the vendor specific implementation of the module. The numbering is vendor specific.
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="a318d2d9-0e75-49da-ac43-e7e4e682e2f9">
                  <SHORT-NAME>SwPatchVersion</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Patch level version number of the vendor specific implementation of the module. The numbering is vendor specific.
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>5</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="ECUC:78bc8362-080f-4253-b3da-804ab69a7154">
                  <SHORT-NAME>ModuleId</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Module ID of this module from Module List
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>10</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="ECUC:01b0f467-6943-4558-b4f2-3fa1fad28449">
                  <SHORT-NAME>VendorId</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Vendor ID of the dedicated implementation of this module according to the AUTOSAR vendor list
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>1</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-STRING-PARAM-DEF UUID="ECUC:1c68a547-f24e-4a4e-9540-69fbd466ec89">
                  <SHORT-NAME>VendorApiInfix</SHORT-NAME>
                  <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                  <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <ECUC-STRING-PARAM-DEF-VARIANTS>
                    <ECUC-STRING-PARAM-DEF-CONDITIONAL>
                      <DEFAULT-VALUE></DEFAULT-VALUE>
                    </ECUC-STRING-PARAM-DEF-CONDITIONAL>
                  </ECUC-STRING-PARAM-DEF-VARIANTS>
                </ECUC-STRING-PARAM-DEF>
                <ECUC-STRING-PARAM-DEF UUID="ECUC:1c68a547-f24e-4a4e-9540-69fbd533ec89">
                  <SHORT-NAME>Release</SHORT-NAME>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <ECUC-STRING-PARAM-DEF-VARIANTS>
                    <ECUC-STRING-PARAM-DEF-CONDITIONAL>
                      <DEFAULT-VALUE></DEFAULT-VALUE>
                    </ECUC-STRING-PARAM-DEF-CONDITIONAL>
                  </ECUC-STRING-PARAM-DEF-VARIANTS>
                </ECUC-STRING-PARAM-DEF>
              </PARAMETERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ECUC:ed28c0d5-4e0c-4826-89fa-449203a3ce8e">
              <SHORT-NAME>EcucPartitionCollection</SHORT-NAME>
              <DESC>
                <L-2 L="FOR-ALL">&lt;html&gt;
                        Collection of Partitions defined for this ECU.
                      &lt;/html&gt;</L-2>
              </DESC>
              <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
              <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
              <SUB-CONTAINERS>
                <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ECUC:c11de67e-41cd-4da2-8cfc-01c5e34209be">
                  <SHORT-NAME>EcucPartition</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                        Definition of one Partition on this ECU. One Partition will be implemented using one Os-Application.
                      &lt;/html&gt;</L-2>
                  </DESC>
                  <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                  <UPPER-MULTIPLICITY-INFINITE>1</UPPER-MULTIPLICITY-INFINITE>
                  <POST-BUILD-CHANGEABLE>false</POST-BUILD-CHANGEABLE>
                  <PARAMETERS>
                    <ECUC-BOOLEAN-PARAM-DEF UUID="ECUC:72f87183-bf87-4f64-a990-38b29817aa2c">
                      <SHORT-NAME>EcucPartitionBswModuleExecution</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">&lt;html&gt;
                        Denotes that this partition will execute all BSW Modules. BSW Modules can only be executed in one partition.
                      &lt;/html&gt;</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                      <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                    </ECUC-BOOLEAN-PARAM-DEF>
                    <ECUC-BOOLEAN-PARAM-DEF UUID="ECUC:4fbca4af-d17a-463a-9b70-fad218ab42a9">
                      <SHORT-NAME>PartitionCanBeRestarted</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">&lt;html&gt;
                        Specifies the requirement whether the Partition can be restarted. If set to true all software executing in this partition shall be capable of handling a restart.
                      &lt;/html&gt;</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                      <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                    </ECUC-BOOLEAN-PARAM-DEF>
                  </PARAMETERS>
                  <REFERENCES>
                    <ECUC-INSTANCE-REFERENCE-DEF UUID="ECUC:fcd3eca6-d43f-42f7-86a1-80043415256b">
                      <SHORT-NAME>EcucPartitionSoftwareComponentInstanceRef</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">&lt;html&gt;
                        References the SW Component instances from the Ecu Extract that shall be executed in this partition.
                      &lt;/html&gt;</L-2>
                      </DESC>
                      <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                      <UPPER-MULTIPLICITY-INFINITE>1</UPPER-MULTIPLICITY-INFINITE>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                      <DESTINATION-CONTEXT>ROOT-SW-COMPOSITION-PROTOTYPE</DESTINATION-CONTEXT>
                      <DESTINATION-TYPE>SW-COMPONENT-PROTOTYPE</DESTINATION-TYPE>
                    </ECUC-INSTANCE-REFERENCE-DEF>
                  </REFERENCES>
                </ECUC-PARAM-CONF-CONTAINER-DEF>
              </SUB-CONTAINERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ECUC:cece0b62-6981-43fb-a5b7-d573fdfb2344">
              <SHORT-NAME>EcucPduCollection</SHORT-NAME>
              <DESC>
                <L-2 L="FOR-ALL">&lt;html&gt;
                        Collection of all Pdu objects flowing through the Com-Stack.
                      &lt;/html&gt;</L-2>
              </DESC>
              <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
              <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
              <PARAMETERS>
                <ECUC-ENUMERATION-PARAM-DEF UUID="ECUC:58955203-89bd-4dcf-b3d4-c5e83c87080b">
                  <SHORT-NAME>PduIdTypeEnum</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                        The PduIdType is used within the entire AUTOSAR Com Stack except for bus drivers. The size of this global type depends on the maximum number of PDUs used within one software module. If no software module deals with more PDUs that 256, this type can be set to uint8. If at least one software module handles more than 256 PDUs, this type must be set to uint16. See AUTOSAR_SWS_CommunicationStackTypes for more details.
                      &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>UINT16</DEFAULT-VALUE>
                  <LITERALS>
                    <ECUC-ENUMERATION-LITERAL-DEF>
                      <SHORT-NAME>UINT16</SHORT-NAME>
                      <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                    </ECUC-ENUMERATION-LITERAL-DEF>
                    <ECUC-ENUMERATION-LITERAL-DEF>
                      <SHORT-NAME>UINT8</SHORT-NAME>
                      <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                    </ECUC-ENUMERATION-LITERAL-DEF>
                  </LITERALS>
                </ECUC-ENUMERATION-PARAM-DEF>
                <ECUC-ENUMERATION-PARAM-DEF UUID="ECUC:3ffce745-6c26-4c88-a57f-cd785b21e467">
                  <SHORT-NAME>PduLengthTypeEnum</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                        The PduLengthType is used within the entire AUTOSAR Com Stack except for bus drivers. The size of this global type depends on the maximum length of PDUs to be sent by an ECU. If no segmentation is used the length depends on the maximum payload size of a frame of the underlying communication system (for FlexRay maximum size is 255 bytes, therefore uint8). If segementation is used it depends on the maximum length of a  segmeneted N-PDU (in general uint16 is used). See AUTOSAR_SWS_CommunicationStackTypes for more details.
                      &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>UINT16</DEFAULT-VALUE>
                  <LITERALS>
                    <ECUC-ENUMERATION-LITERAL-DEF>
                      <SHORT-NAME>UINT16</SHORT-NAME>
                      <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                    </ECUC-ENUMERATION-LITERAL-DEF>
                    <ECUC-ENUMERATION-LITERAL-DEF>
                      <SHORT-NAME>UINT32</SHORT-NAME>
                      <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                    </ECUC-ENUMERATION-LITERAL-DEF>
                    <ECUC-ENUMERATION-LITERAL-DEF>
                      <SHORT-NAME>UINT8</SHORT-NAME>
                      <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                    </ECUC-ENUMERATION-LITERAL-DEF>
                  </LITERALS>
                </ECUC-ENUMERATION-PARAM-DEF>
              </PARAMETERS>
              <SUB-CONTAINERS>
                <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ECUC:422a7abf-add2-418a-81a6-af5cdeae1930">
                  <SHORT-NAME>Pdu</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                        One Pdu flowing through the COM-Stack. This Pdu is used by all Com-Stack modules to agree on referencing the same Pdu.
                      &lt;/html&gt;</L-2>
                  </DESC>
                  <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                  <UPPER-MULTIPLICITY-INFINITE>1</UPPER-MULTIPLICITY-INFINITE>
                  <POST-BUILD-CHANGEABLE>true</POST-BUILD-CHANGEABLE>
                  <PARAMETERS>
                    <ECUC-INTEGER-PARAM-DEF UUID="ECUC:3996a201-ffdf-43d1-a5e8-f88592a78a35">
                      <SHORT-NAME>PduLength</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">&lt;html&gt;
                        Length of the Pdu in bytes.  It should be noted that in former AUTOSAR releases (Rel 2.1, Rel 3.0, Rel 3.1, Rel 4.0 Rev. 1) this parameter was defined in bits.
                      &lt;/html&gt;</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                      <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                      <MAX>4095</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                  </PARAMETERS>
                  <REFERENCES>
                    <ECUC-FOREIGN-REFERENCE-DEF UUID="ECUC:44ff5259-e0d4-4c83-bba5-1d113db79352">
                      <SHORT-NAME>SysTPduToFrameMappingRef</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">&lt;html&gt;
                        Optional reference to the PduToFrameMapping from the SystemTemplate which this Pdu represents.
                      &lt;/html&gt;</L-2>
                      </DESC>
                      <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                      <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                      <DESTINATION-TYPE>PDU-TO-FRAME-MAPPING</DESTINATION-TYPE>
                    </ECUC-FOREIGN-REFERENCE-DEF>
                  </REFERENCES>
                </ECUC-PARAM-CONF-CONTAINER-DEF>
              </SUB-CONTAINERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ECUC:973d00a5-5e98-4b38-8b7b-ddd518d40317">
              <SHORT-NAME>EcucVariationResolver</SHORT-NAME>
              <DESC>
                <L-2 L="FOR-ALL">&lt;html&gt;
                        Collection of PredefinedVariant elements containing definition of values for SwSystemconst which shall be applied when resolving the variability during ECU Configuration.
                      &lt;/html&gt;</L-2>
              </DESC>
              <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
              <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
              <REFERENCES>
                <ECUC-FOREIGN-REFERENCE-DEF UUID="ECUC:84013509-59a1-46d4-ba0d-5bcfb76b941f">
                  <SHORT-NAME>PredefinedVariantRef</SHORT-NAME>
                  <LOWER-MULTIPLICITY>1</LOWER-MULTIPLICITY>
                  <UPPER-MULTIPLICITY-INFINITE>1</UPPER-MULTIPLICITY-INFINITE>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                  <DESTINATION-TYPE>PREDEFINED-VARIANT</DESTINATION-TYPE>
                </ECUC-FOREIGN-REFERENCE-DEF>
              </REFERENCES>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
          </CONTAINERS>
        </ECUC-MODULE-DEF>
      </ELEMENTS>
    </AR-PACKAGE>
  </AR-PACKAGES>
</AUTOSAR>
