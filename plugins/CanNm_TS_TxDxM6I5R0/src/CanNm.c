/**
 * \file
 *
 * \brief AUTOSAR CanNm
 *
 * This file contains the implementation of the AUTOSAR
 * module CanNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */


/*  MISRA-C:2004 Deviation List
 *
 *  MISRA-1) Deviated Rule: 19.10 (required)
 *    "Parameter instance shall be enclosed in parentheses."
 *
 *  Reason:
 *  It is used in function parameter declarations and definitions
 *  or as structure member.
 *
 */

/*===============================[includes]=================================*/

#include <CanNm_Trace.h>
#include <Std_Types.h>          /* AUTOSAR standard types */
#include <TSAutosar.h>          /* EB specific standard types */
#include <TSMem.h>

/* !LINKSTO CANNM306,1 */
#include <CanNm.h>              /* CanNm API definitions (own interface) */
#include <CanNm_Int.h>          /* CanNm internal interface */
#include <CanNm_Cbk.h>          /* CanNm callback API (own interface) */
#include <CanNm_Hsm.h>          /* public API of CanNm_Hsm.c */
#include <CanNm_HsmCanNm.h>     /* public statechart model definitions, */

/* !LINKSTO CANNM307,1 */
#include <Nm_Cbk.h>             /* Nm API for Nm_StateChangeNotification() */
/* !LINKSTO CANNM310,1 */
#include <SchM_CanNm.h>         /* SchM-header for CanNm */
/* !LINKSTO CANNM312,1 */
#include <CanIf.h>              /* CanIf API, CanIf_Transmit() */

#if ((CANNM_COM_USER_DATA_SUPPORT ==STD_ON) || (CANNM_PN_EIRA_CALC_ENABLED == STD_ON))
/* !LINKSTO CANNM326,1 */
#include <PduR_CanNm.h>         /* PduR API for CanNm */
#endif

#if (CANNM_DEV_ERROR_DETECT == STD_ON)
/* Development Error Tracer (CANNM_DET_REPORT_ERROR() macros) */
/* !LINKSTO CANNM308,1 */
#include <Det.h>
#endif

/*===========================[macro definitions]============================*/

/*------------------[AUTOSAR vendor identification check]-------------------*/

#if (!defined CANNM_VENDOR_ID) /* configuration check */
#error CANNM_VENDOR_ID must be defined
#endif

#if (CANNM_VENDOR_ID != 1U) /* vendor check */
#error CANNM_VENDOR_ID has wrong vendor id
#endif

/*------------------[AUTOSAR release version identification check]----------*/

#if (!defined CANNM_AR_RELEASE_MAJOR_VERSION) /* configuration check */
#error CANNM_AR_RELEASE_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (CANNM_AR_RELEASE_MAJOR_VERSION != 4U)
#error CANNM_AR_RELEASE_MAJOR_VERSION wrong (!= 4U)
#endif

#if (!defined CANNM_AR_RELEASE_MINOR_VERSION) /* configuration check */
#error CANNM_AR_RELEASE_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (CANNM_AR_RELEASE_MINOR_VERSION != 0U )
#error CANNM_AR_RELEASE_MINOR_VERSION wrong (!= 0U)
#endif

#if (!defined CANNM_AR_RELEASE_REVISION_VERSION) /* configuration check */
#error CANNM_AR_RELEASE_REVISION_VERSION must be defined
#endif

/* revision version check */
#if (CANNM_AR_RELEASE_REVISION_VERSION != 3U )
#error CANNM_AR_RELEASE_REVISION_VERSION wrong (!= 3U)
#endif

/*------------------[AUTOSAR module version identification check]-----------*/

#if (!defined CANNM_SW_MAJOR_VERSION) /* configuration check */
#error CANNM_SW_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (CANNM_SW_MAJOR_VERSION != 6U)
#error CANNM_SW_MAJOR_VERSION wrong (!= 6U)
#endif

#if (!defined CANNM_SW_MINOR_VERSION) /* configuration check */
#error CANNM_SW_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (CANNM_SW_MINOR_VERSION < 5U)
#error CANNM_SW_MINOR_VERSION wrong (< 5U)
#endif

#if (!defined CANNM_SW_PATCH_VERSION) /* configuration check */
#error CANNM_SW_PATCH_VERSION must be defined
#endif

/* patch version check */
#if (CANNM_SW_PATCH_VERSION < 9U)
#error CANNM_SW_PATCH_VERSION wrong (< 9U)
#endif

/*------------------[module internal macros]--------------------------------*/

/* Event Flags */
#define CANNM_DISENABLE_COM_STATE                          0U
#define CANNM_DISENABLE_COM_TOGGLE                         1U

#define CANNM_RELEASEQUEST_NET_STATE                       2U
#define CANNM_RELEASEQUEST_NET_TOGGLE                      3U

#define CANNM_RXIND                                        4U
#define CANNM_TXCONF                                       5U

#define CANNM_PASSIVESTARTUP                               6U
#define CANNM_REPMSGREQ                                    7U

#define CANNM_REPMSGBITIND                                 0U

#define CANNM_APPLY_FILTER_MASK                            1U
#define CANNM_DO_NOT_APPLY_FILTER_MASK                     0U

/* Bitmasks for Event Flags */
#define CANNM_DISENABLE_COM_STATE_MASK          \
   CANNM_BITPOS2MASK(CANNM_DISENABLE_COM_STATE)
#define CANNM_DISENABLE_COM_TOGGLE_MASK         \
   CANNM_BITPOS2MASK(CANNM_DISENABLE_COM_TOGGLE)

#define CANNM_RELEASEQUEST_NET_STATE_MASK               \
   CANNM_BITPOS2MASK(CANNM_RELEASEQUEST_NET_STATE)
#define CANNM_RELEASEQUEST_NET_TOGGLE_MASK              \
   CANNM_BITPOS2MASK(CANNM_RELEASEQUEST_NET_TOGGLE)

#define CANNM_RXIND_MASK                        \
   CANNM_BITPOS2MASK(CANNM_RXIND)
#define CANNM_TXCONF_MASK                       \
   CANNM_BITPOS2MASK(CANNM_TXCONF)

#define CANNM_PASSIVESTARTUP_MASK               \
   CANNM_BITPOS2MASK(CANNM_PASSIVESTARTUP)
#define CANNM_REPMSGREQ_MASK                    \
   CANNM_BITPOS2MASK(CANNM_REPMSGREQ)

#define CANNM_REPMSGBITIND_MASK                 \
   CANNM_BITPOS2MASK(CANNM_REPMSGBITIND)

/* just an abbreviating macro */
#define CANNM_EMIT(a,b)                                 \
        CANNM_HSMEMITINST(&CanNm_HsmScCanNm, (a),( b))

/* Deviation MISRA-1 <+13> */
#define CANNM_HANDLE_TICK(instIdx, timer, event)         \
  do                                                     \
  {                                                      \
    if (CANNM_CHANNEL_STATUS(instIdx).timer > 0U)        \
   {                                                     \
     --CANNM_CHANNEL_STATUS(instIdx).timer;              \
     if (CANNM_CHANNEL_STATUS(instIdx).timer == 0U)      \
     {                                                   \
       (void)CANNM_EMIT(instIdx, event);                 \
     }                                                   \
   }                                                     \
  }                                                      \
  while (0)

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/
/* !LINKSTO CANNM311,1 */
#define CANNM_START_SEC_CODE
#include <MemMap.h>

/** \brief Process all timers.
 ** \param instIdx index of state machine instance to work on */
STATIC FUNC(void, CANNM_CODE) CanNm_HandleTimerTick(
  CANNM_PDL_SF(const uint8 instIdx));

#if (CANNM_PN_EIRA_CALC_ENABLED == STD_ON)
/** \brief Process all Pn timers.
 ** */
STATIC FUNC(void, CANNM_CODE) CanNm_HandlePnTimers
(
  void
);
/** \brief Aggregate ERA (remote/external requests).
 ** */
STATIC FUNC(void, CANNM_CODE) CanNm_AggregateEra
(
  const PduIdType instIdx,
  CONSTP2CONST(uint8, AUTOMATIC, CANNM_APPL_CONST) PnInfo,
  uint8 applyFilterMask
);
#endif
#define CANNM_STOP_SEC_CODE
#include <MemMap.h>

/*=====================[external constants]=================================*/

/*=====================[internal constants]=================================*/

/*=======================[external data]====================================*/

#define CANNM_START_SEC_VAR_FAST_NO_INIT_UNSPECIFIED
#include <MemMap.h>

VAR(CanNm_ChanStatusType, CANNM_VAR_FAST)
  CanNm_ChanStatus[CANNM_NUMBER_OF_CHANNELS];

#define CANNM_STOP_SEC_VAR_FAST_NO_INIT_UNSPECIFIED
#include <MemMap.h>

/*=======================[internal data]====================================*/

#define CANNM_START_SEC_VAR_INIT_BOOLEAN
#include <MemMap.h>

/** \brief Intialization information of CanNm module */
STATIC VAR(boolean, CANNM_VAR) CanNm_Initialized = FALSE;

#define CANNM_STOP_SEC_VAR_INIT_BOOLEAN
#include <MemMap.h>

#define CANNM_START_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>

/** \brief Partial netoworking info: EiraValue and EiraTimer */
#if (CANNM_PN_EIRA_CALC_ENABLED == STD_ON)
VAR(CanNm_PnStatusType, CANNM_VAR_FAST) CanNm_PnStatus;
#endif

#define CANNM_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>

/*=====================[external functions definitions]=====================*/

#define CANNM_START_SEC_CODE
#include <MemMap.h>

/*------------------[CanNm_Init]---------------------------------------------*/

FUNC(void, CANNM_CODE) CanNm_Init
(
  CONSTP2CONST(CanNm_ConfigType, AUTOMATIC, CANNM_APPL_CONST) cannmConfigPtr
)
{
  NetworkHandleType ChIdx;
  /* No protection against races required, as there is no concurrent
   * calling of functions expected at initialization time of the ComStack */

  /* CANNM060 (rejected) */

  TS_PARAM_UNUSED(cannmConfigPtr);
  DBG_CANNM_INIT_ENTRY(cannmConfigPtr);

  for(ChIdx = 0U; ChIdx < CANNM_NUMBER_OF_CHANNELS; ChIdx++)
  {
    CANNM_CHANNEL_STATUS(ChIdx).CurState = NM_STATE_BUS_SLEEP;
    CANNM_CHANNEL_STATUS(ChIdx).CanNmTimer = 0U;
    CANNM_CHANNEL_STATUS(ChIdx).UniversalTimer = 0U;
    CANNM_CHANNEL_STATUS(ChIdx).RmsTimer = 0U;
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
    CANNM_CHANNEL_STATUS(ChIdx).ImmediateNmTransmissionCounter = 0U;
    CANNM_CHANNEL_STATUS(ChIdx).MsgCycleTimer = 0U;
#if (CANNM_IMMEDIATE_TXCONF_ENABLED == STD_OFF)
    CANNM_CHANNEL_STATUS(ChIdx).TimeoutTimer = 0U;
#endif
#endif
    CANNM_CHANNEL_STATUS(ChIdx).ChanStatus &= (uint8)(~CANNM_NETWORK_REQUESTED);
    CANNM_CHANNEL_STATUS(ChIdx).ChanStatus &= (uint8)(~CANNM_COM_DISABLED);
#if (CANNM_PN_ENABLED == STD_ON)
    CANNM_CHANNEL_STATUS(ChIdx).PnFilterEnabled = FALSE;
#endif
#if (CANNM_PN_EIRA_CALC_ENABLED == STD_ON)
    TS_MemSet(CANNM_CHANNEL_STATUS(ChIdx).PnInfoEra, 0U, CANNM_PN_INFO_LENGTH);
#endif
  }

  /* CANNM141, CANNM144 */
  /* Initialize CanNm state machine, perform initial transitions */
  CanNm_HsmInit(&CanNm_HsmScCanNm);
#if (CANNM_PN_EIRA_CALC_ENABLED == STD_ON)
  /* !LINKSTO CANNM424,1 */
  TS_MemSet(CanNm_PnStatus.EiraValue,0x00U,CANNM_PN_INFO_LENGTH);
  TS_MemSet(CanNm_PnStatus.EiraTimer,0x00U,sizeof(CanNm_PnStatus.EiraTimer));
#endif

  CanNm_Initialized = TRUE;

  DBG_CANNM_INIT_EXIT(cannmConfigPtr);
}

/*------------------[CanNm_PassiveStartUp]-----------------------------------*/

FUNC(Std_ReturnType, CANNM_CODE) CanNm_PassiveStartUp
(
  const NetworkHandleType nmChannelHandle
)
{
  Std_ReturnType retVal = E_NOT_OK;

  DBG_CANNM_PASSIVESTARTUP_ENTRY(nmChannelHandle);

/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_PASSIVESTARTUP, CANNM_E_NO_INIT);
  }
  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_PASSIVESTARTUP, CANNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
    const NetworkHandleType index
       = CanNm_IndexFromNmChannelHandle[nmChannelHandle];
#else
    TS_PARAM_UNUSED(nmChannelHandle);
#endif

    /* CANNM128 */
    if (CANNM_CHANNEL_STATUS(index).CurState == NM_STATE_BUS_SLEEP)
    {
      /* Emit the event to trigger transition to RepeatMessageState */
      CANNM_EMIT(index, CANNM_HSM_CANNM_EV_NETWORK_START);

      retVal = E_OK;
    }
    else
    {
      /* CANNM147, CANNM212 */
      retVal = E_NOT_OK;
    }
  }


  DBG_CANNM_PASSIVESTARTUP_EXIT(retVal,nmChannelHandle);

  return retVal;
}

/*------------------[CanNm_NetworkRequest]-----------------------------------*/

#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
FUNC(Std_ReturnType, CANNM_CODE) CanNm_NetworkRequest
(
  const NetworkHandleType nmChannelHandle
)
{
  Std_ReturnType retVal = E_NOT_OK;

  DBG_CANNM_NETWORKREQUEST_ENTRY(nmChannelHandle);

/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_NETWORKREQUEST, CANNM_E_NO_INIT);
  }
  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_NETWORKREQUEST, CANNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
    const NetworkHandleType index
       = CanNm_IndexFromNmChannelHandle[nmChannelHandle];
#else
    TS_PARAM_UNUSED(nmChannelHandle);
#endif

    /* Enter critical section to protect from concurrent access
     * to different bits in 'ChanStatus' in different APIs.
     */
    SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
    /* Set the network requested flag */
    CANNM_CHANNEL_STATUS(index).ChanStatus |= CANNM_NETWORK_REQUESTED;
    SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();

    /* Emit the event to trigger the transition */
    CANNM_EMIT(index, CANNM_HSM_CANNM_EV_NET_REQ_STATUS_CHANGED);

    retVal = E_OK;
  }


  DBG_CANNM_NETWORKREQUEST_EXIT(retVal,nmChannelHandle);

  return retVal;
}

/*------------------[CanNm_NetworkRelease]-----------------------------------*/

FUNC(Std_ReturnType, CANNM_CODE) CanNm_NetworkRelease
(
  const NetworkHandleType nmChannelHandle
)
{
  Std_ReturnType retVal = E_NOT_OK;

  DBG_CANNM_NETWORKRELEASE_ENTRY(nmChannelHandle);

/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_NETWORKRELEASE, CANNM_E_NO_INIT);
  }
  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_NETWORKRELEASE, CANNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
    const NetworkHandleType index
       = CanNm_IndexFromNmChannelHandle[nmChannelHandle];
#else
    TS_PARAM_UNUSED(nmChannelHandle);
#endif

    /* Enter critical section to protect from concurrent access
     * to different bits in 'ChanStatus' in different APIs.
     */
    SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
    /* Clear the Network requested flag */
    CANNM_CHANNEL_STATUS(index).ChanStatus &= (uint8)(~CANNM_NETWORK_REQUESTED);
    SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();

      CANNM_EMIT(index, CANNM_HSM_CANNM_EV_NET_REQ_STATUS_CHANGED);

      retVal = E_OK;
  }


  DBG_CANNM_NETWORKRELEASE_EXIT(retVal,nmChannelHandle);

  return retVal;
}

#endif /* (CANNM_PASSIVE_MODE_ENABLED == STD_OFF) */

/*------------------[CanNm_DisableCommunication]-----------------------------*/

#if (CANNM_COM_CONTROL_ENABLED == STD_ON)
FUNC(Std_ReturnType, CANNM_CODE) CanNm_DisableCommunication
(
  const NetworkHandleType nmChannelHandle
)
{
  Std_ReturnType retVal = E_NOT_OK;

  DBG_CANNM_DISABLECOMMUNICATION_ENTRY(nmChannelHandle);

/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_DISABLECOMMUNICATION, CANNM_E_NO_INIT);
  }
  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_DISABLECOMMUNICATION, CANNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {

#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
    const NetworkHandleType index
       = CanNm_IndexFromNmChannelHandle[nmChannelHandle];
#else
    TS_PARAM_UNUSED(nmChannelHandle);
#endif

    SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();

    if (((CANNM_CHANNEL_STATUS(index).CurState == NM_STATE_REPEAT_MESSAGE) ||
         (CANNM_CHANNEL_STATUS(index).CurState == NM_STATE_NORMAL_OPERATION)
        ) ||
        (CANNM_CHANNEL_STATUS(index).CurState == NM_STATE_READY_SLEEP)
       )
    {
      if ((CANNM_CHANNEL_STATUS(index).ChanStatus & CANNM_COM_DISABLED) == 0U)
      {
        CANNM_CHANNEL_STATUS(index).ChanStatus |= CANNM_COM_DISABLED;

        CANNM_EMIT(index, CANNM_HSM_CANNM_EV_COM_CONTROL);

        retVal = E_OK;
      }
      else
      {
        /* CANNM177_Implicit */
        retVal = E_NOT_OK;
      }
    }
    else
    {
      /* CANNM172 */
      retVal = E_NOT_OK;
    }

    SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();

#else
  /* CANNM298 */
    retVal = E_NOT_OK;
#endif
  }


  DBG_CANNM_DISABLECOMMUNICATION_EXIT(retVal,nmChannelHandle);

  return retVal;
}

/*------------------[CanNm_EnableCommunication]------------------------------*/

/* This function only exists if communication control is on,
 * so we do not need to care for passive mode */

FUNC(Std_ReturnType, CANNM_CODE) CanNm_EnableCommunication
(
  const NetworkHandleType nmChannelHandle
)
{
  Std_ReturnType retVal = E_NOT_OK;

  DBG_CANNM_ENABLECOMMUNICATION_ENTRY(nmChannelHandle);

/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_ENABLECOMMUNICATION, CANNM_E_NO_INIT);
  }
  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_ENABLECOMMUNICATION,
      CANNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {

#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
    const NetworkHandleType index
       = CanNm_IndexFromNmChannelHandle[nmChannelHandle];
#else
    TS_PARAM_UNUSED(nmChannelHandle);
#endif

    SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();

    if (((CANNM_CHANNEL_STATUS(index).CurState == NM_STATE_REPEAT_MESSAGE) ||
         (CANNM_CHANNEL_STATUS(index).CurState == NM_STATE_NORMAL_OPERATION)
        ) ||
        (CANNM_CHANNEL_STATUS(index).CurState == NM_STATE_READY_SLEEP)
       )
    {
      if ((CANNM_CHANNEL_STATUS(index).ChanStatus & CANNM_COM_DISABLED) != 0U)
      {
        /* CANNM176 */
        CANNM_CHANNEL_STATUS(index).ChanStatus &= (uint8)(~CANNM_COM_DISABLED);

        CANNM_EMIT(index, CANNM_HSM_CANNM_EV_COM_CONTROL);

        retVal = E_OK;
      }
      else
      {
        /* CANNM177 */
        retVal = E_NOT_OK;
      }
    }
    else
    {
      /* CANNM295 */
      retVal = E_NOT_OK;
    }

    SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();

#else
    /* CANNM297 */
    retVal = E_NOT_OK;
#endif
  }


  DBG_CANNM_ENABLECOMMUNICATION_EXIT(retVal,nmChannelHandle);

  return retVal;
}
#endif /* (CANNM_COM_CONTROL_ENABLED == STD_ON) */

/*------------------------[CanNm_SetUserData]--------------------------------*/
#if ((CANNM_USER_DATA_ENABLED == STD_ON)        \
  && (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)    \
  && (CANNM_COM_USER_DATA_SUPPORT == STD_OFF))
FUNC(Std_ReturnType, CANNM_CODE) CanNm_SetUserData
(
  const NetworkHandleType nmChannelHandle,
  CONSTP2CONST(uint8, AUTOMATIC, CANNM_APPL_CONST) nmUserDataPtr
)
{
  Std_ReturnType retVal = E_NOT_OK;

  DBG_CANNM_SETUSERDATA_ENTRY(nmChannelHandle,nmUserDataPtr);

  /* Perform checks, get index */
/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_SETUSERDATA, CANNM_E_NO_INIT);
  }
  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_SETUSERDATA, CANNM_E_INVALID_CHANNEL);
  }
  else if (nmUserDataPtr == NULL_PTR)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_SETUSERDATA, CANNM_E_NULL_POINTER);
  }
  else
#endif
  {
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
    const NetworkHandleType index
       = CanNm_IndexFromNmChannelHandle[nmChannelHandle];
#else
    TS_PARAM_UNUSED(nmChannelHandle);
#endif

/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
    if (0U == CANNM_CHANNEL_CONFIG(index).UserDataLength)
    {
      CANNM_DET_REPORT_ERROR(
        nmChannelHandle, CANNM_SERVID_SETUSERDATA, CANNM_E_INVALID_FUNCTION_ARG);
    }
    else
#endif
    {
      TS_MemCpy(
        &CANNM_CHANNEL_CONFIG(index).TxPduPtr[CANNM_CHANNEL_CONFIG(index).UDFBPos],
        nmUserDataPtr, (uint16)CANNM_CHANNEL_CONFIG(index).UserDataLength);

    retVal = E_OK;
    }
  }


  DBG_CANNM_SETUSERDATA_EXIT(retVal,nmChannelHandle,nmUserDataPtr);

  return retVal;
}

#endif /* ((CANNM_USER_DATA_ENABLED == STD_ON)
        * && (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
        * && (CANNM_COM_USER_DATA_SUPPORT == STD_OFF)) */

/*------------------------[CanNm_GetUserData]--------------------------------*/

#if (CANNM_USER_DATA_ENABLED == STD_ON)
FUNC(Std_ReturnType, CANNM_CODE) CanNm_GetUserData
(
  const NetworkHandleType                       nmChannelHandle,
  CONSTP2VAR(uint8, AUTOMATIC, CANNM_APPL_DATA) nmUserDataPtr
)
{
  Std_ReturnType retVal = E_NOT_OK;

  DBG_CANNM_GETUSERDATA_ENTRY(nmChannelHandle,nmUserDataPtr);

  /* Perform checks, get index */
/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETUSERDATA, CANNM_E_NO_INIT);
  }
  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETUSERDATA, CANNM_E_INVALID_CHANNEL);
  }
  else if (nmUserDataPtr == NULL_PTR)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETUSERDATA, CANNM_E_NULL_POINTER);
  }
  else
#endif
  {
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
    const NetworkHandleType index
       = CanNm_IndexFromNmChannelHandle[nmChannelHandle];
#else
    TS_PARAM_UNUSED(nmChannelHandle);
#endif

/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
    if (0U == CANNM_CHANNEL_CONFIG(index).UserDataLength)
    {
      CANNM_DET_REPORT_ERROR(
        nmChannelHandle, CANNM_SERVID_GETUSERDATA, CANNM_E_INVALID_FUNCTION_ARG);
    }
    else
#endif
    {
      SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();

      /* Copy user data */
      TS_MemCpy(
        nmUserDataPtr,
        &CANNM_CHANNEL_CONFIG(index).RxPduPtr[CANNM_CHANNEL_CONFIG(index).UDFBPos],
        (uint16)CANNM_CHANNEL_CONFIG(index).UserDataLength);

      SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();

      retVal = E_OK;
    }
  }


  DBG_CANNM_GETUSERDATA_EXIT(retVal,nmChannelHandle,nmUserDataPtr);

  return retVal;
}
#endif /* (CANNM_USER_DATA_ENABLED == STD_ON) */


/*------------------------[CanNm_GetNodeIdentifier]--------------------------*/

#if (CANNM_NODE_ID_ENABLED == STD_ON)
FUNC(Std_ReturnType, CANNM_CODE) CanNm_GetNodeIdentifier
(
  const NetworkHandleType                       nmChannelHandle,
  CONSTP2VAR(uint8, AUTOMATIC, CANNM_APPL_DATA) nmNodeIdPtr
)
{
  Std_ReturnType retVal = E_NOT_OK;

  DBG_CANNM_GETNODEIDENTIFIER_ENTRY(nmChannelHandle,nmNodeIdPtr);

  /* Perform checks, get index */
/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETNODEIDENTIFIER, CANNM_E_NO_INIT);
  }
  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETNODEIDENTIFIER, CANNM_E_INVALID_CHANNEL);
  }
  else if (nmNodeIdPtr == NULL_PTR)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETNODEIDENTIFIER, CANNM_E_NULL_POINTER);
  }
  else
#endif
  {
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
    const NetworkHandleType index
       = CanNm_IndexFromNmChannelHandle[nmChannelHandle];
#else
    TS_PARAM_UNUSED(nmChannelHandle);
#endif

    /* Copy Node Identifier */
    *nmNodeIdPtr = CANNM_CHANNEL_CONFIG(index).RxPduPtr[CANNM_CHANNEL_CONFIG(index).NidPos];

    retVal = E_OK;
  }


  DBG_CANNM_GETNODEIDENTIFIER_EXIT(retVal,nmChannelHandle,nmNodeIdPtr);

  return retVal;
}

/*------------------------[CanNm_GetLocalNodeIdentifier]---------------------*/

FUNC(Std_ReturnType, CANNM_CODE) CanNm_GetLocalNodeIdentifier
(
  const NetworkHandleType                       nmChannelHandle,
  CONSTP2VAR(uint8, AUTOMATIC, CANNM_APPL_DATA) nmNodeIdPtr
)
{
  Std_ReturnType retVal = E_NOT_OK;

  DBG_CANNM_GETLOCALNODEIDENTIFIER_ENTRY(nmChannelHandle,nmNodeIdPtr);

/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETLOCALNODEIDENTIFIER, CANNM_E_NO_INIT);
  }

  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETLOCALNODEIDENTIFIER,
      CANNM_E_INVALID_CHANNEL);
  }

  else if (nmNodeIdPtr == NULL_PTR)
  {
    CANNM_DET_REPORT_ERROR(nmChannelHandle,CANNM_SERVID_GETLOCALNODEIDENTIFIER,
      CANNM_E_NULL_POINTER);
  }

  else
#endif
  {
    /* Copy Node Id */
    *nmNodeIdPtr
      = CanNm_ChanConfig[CanNm_IndexFromNmChannelHandle[nmChannelHandle]].NodeId;

    retVal = E_OK;
  }


  DBG_CANNM_GETLOCALNODEIDENTIFIER_EXIT(retVal,nmChannelHandle,nmNodeIdPtr);

  return retVal;
}
#endif /* (CANNM_NODE_ID_ENABLED == STD_ON) */

/*------------------------[CanNm_RepeatMessageRequest]---------------------*/

#if (CANNM_NODE_DETECTION_ENABLED == STD_ON)
FUNC(Std_ReturnType, CANNM_CODE) CanNm_RepeatMessageRequest
(
  const NetworkHandleType nmChannelHandle
)
{
  Std_ReturnType retVal = E_NOT_OK;

  DBG_CANNM_REPEATMESSAGEREQUEST_ENTRY(nmChannelHandle);

/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_REPEATMESSAGEREQUEST, CANNM_E_NO_INIT);
  }
  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_REPEATMESSAGEREQUEST,
      CANNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
    const NetworkHandleType index
       = CanNm_IndexFromNmChannelHandle[nmChannelHandle];
#else
    TS_PARAM_UNUSED(nmChannelHandle);
#endif

    if ((CANNM_CHANNEL_STATUS(index).CurState == NM_STATE_NORMAL_OPERATION)
        || (CANNM_CHANNEL_STATUS(index).CurState == NM_STATE_READY_SLEEP))
    {
      CONSTP2VAR(uint8, AUTOMATIC, AUTOSAR_COMSTACKDATA) TxPduPtr
        = &CANNM_CHANNEL_CONFIG(index).TxPduPtr[
          CANNM_CHANNEL_CONFIG(index).CbvPos];

      /* CANNM113, CANNM121 */
      TS_AtomicSetBit_8(TxPduPtr, CANNM_CBV_REPEATMESSAGEBIT);

      CANNM_EMIT(index, CANNM_HSM_CANNM_EV_REPEAT_MESSAGE_REASON);

      retVal = E_OK;
    }
    else
    {
      /* CANNM137 */
      retVal = E_NOT_OK;
    }
  }


  DBG_CANNM_REPEATMESSAGEREQUEST_EXIT(retVal,nmChannelHandle);

  return retVal;
}
#endif /*  (CANNM_NODE_DETECTION_ENABLED == STD_ON)  */

/*------------------------[CanNm_GetPduData]---------------------------------*/

#if ((CANNM_USER_DATA_ENABLED == STD_ON)        \
  || (CANNM_NODE_ID_ENABLED == STD_ON)          \
  || (CANNM_NODE_DETECTION_ENABLED == STD_ON))
FUNC(Std_ReturnType, CANNM_CODE) CanNm_GetPduData
(
  const NetworkHandleType                       nmChannelHandle,
  CONSTP2VAR(uint8, AUTOMATIC, CANNM_APPL_DATA) nmPduDataPtr
)
{
  Std_ReturnType retVal = E_NOT_OK;

  DBG_CANNM_GETPDUDATA_ENTRY(nmChannelHandle,nmPduDataPtr);

  /* Perform checks, get index */
/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETPDUDATA, CANNM_E_NO_INIT);
  }
  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETPDUDATA, CANNM_E_INVALID_CHANNEL);
  }
  else if (nmPduDataPtr == NULL_PTR)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETPDUDATA, CANNM_E_NULL_POINTER);
  }
  else
#endif
  {
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
    const NetworkHandleType index
       = CanNm_IndexFromNmChannelHandle[nmChannelHandle];
#else
    TS_PARAM_UNUSED(nmChannelHandle);
#endif

    /* PduLength cannot be set to zero.
     * This is ensured by the code generator/configuration schema.
     */
    /* Enter critical section */
    SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();

    /* Copy Pdu data */
    TS_MemCpy(
      nmPduDataPtr, CANNM_CHANNEL_CONFIG(index).RxPduPtr,
      CANNM_CHANNEL_CONFIG(index).PduLength);

    /* Leave critical section */
    SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();

    retVal = E_OK;

  }


  DBG_CANNM_GETPDUDATA_EXIT(retVal,nmChannelHandle,nmPduDataPtr);

  return retVal;
}
#endif /* if ((CANNM_USER_DATA_ENABLED == STD_ON)
        * || (CANNM_NODE_ID_ENABLED == STD_ON)
        * || (CANNM_NODE_DETECTION_ENABLED == STD_ON)) */


/*------------------------[CanNm_GetState]-----------------------------------*/

FUNC(Std_ReturnType, CANNM_CODE) CanNm_GetState
(
  const NetworkHandleType                              nmChannelHandle,
  CONSTP2VAR(Nm_StateType, AUTOMATIC, CANNM_APPL_DATA) nmStatePtr,
  CONSTP2VAR(Nm_ModeType, AUTOMATIC, CANNM_APPL_DATA)  nmModePtr
)
{
  Nm_StateType CurState;
  Std_ReturnType retVal = E_NOT_OK;

  DBG_CANNM_GETSTATE_ENTRY(nmChannelHandle,nmStatePtr,nmModePtr);

  /* Check if the module has been initialized */
/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETSTATE, CANNM_E_NO_INIT);
  }
  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETSTATE, CANNM_E_INVALID_CHANNEL);
  }
  else if (nmStatePtr == NULL_PTR)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETSTATE, CANNM_E_NULL_POINTER);
  }
  else if (nmModePtr == NULL_PTR)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_GETSTATE, CANNM_E_NULL_POINTER);
  }
  else
#endif
  {
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
    const NetworkHandleType index
       = CanNm_IndexFromNmChannelHandle[nmChannelHandle];
#else
    TS_PARAM_UNUSED(nmChannelHandle);
#endif

    CurState = CANNM_CHANNEL_STATUS(index).CurState;

    /* !LINKSTO CANNM092,1 */
    switch (CurState)
    {
      /* !LINKSTO CANNM094,1 */
      case NM_STATE_NORMAL_OPERATION: /* fall through */
      case NM_STATE_REPEAT_MESSAGE: /* fall through */
      case NM_STATE_READY_SLEEP:
        *nmModePtr = NM_MODE_NETWORK;
        *nmStatePtr = CurState;
        retVal = E_OK;
        break;
      case NM_STATE_PREPARE_BUS_SLEEP:
        *nmModePtr = NM_MODE_PREPARE_BUS_SLEEP;
        *nmStatePtr = CurState;
        retVal = E_OK;
        break;
      default:
        /* By default, Bus sleep state is the current state
           after initialization */
        *nmModePtr = NM_MODE_BUS_SLEEP;
        *nmStatePtr = CurState;
        retVal = E_OK;
        break;
    }
  }


  DBG_CANNM_GETSTATE_EXIT(retVal,nmChannelHandle,nmStatePtr,nmModePtr);

  return retVal;
}

/*------------------------[CanNm_GetVersionInfo]-----------------------------*/

#if (CANNM_VERSION_INFO_API == STD_ON)
FUNC(void, CANNM_CODE) CanNm_GetVersionInfo
(
  P2VAR(Std_VersionInfoType, AUTOMATIC, CANNM_APPL_DATA) versioninfo
)
{
  DBG_CANNM_GETVERSIONINFO_ENTRY(versioninfo);

/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (NULL_PTR == versioninfo)
  {
    CANNM_DET_REPORT_ERROR(
      CANNM_INSTANCE_ID, CANNM_SERVID_GETVERSIONINFO, CANNM_E_NULL_POINTER);
  }
  else
#endif
  {
    versioninfo->vendorID         = CANNM_VENDOR_ID;
    versioninfo->moduleID         = CANNM_MODULE_ID;
    versioninfo->sw_major_version = CANNM_SW_MAJOR_VERSION;
    versioninfo->sw_minor_version = CANNM_SW_MINOR_VERSION;
    versioninfo->sw_patch_version = CANNM_SW_PATCH_VERSION;
  }

  DBG_CANNM_GETVERSIONINFO_EXIT(versioninfo);
}
#endif /* (CANNM_VERSION_INFO_API == STD_ON) */

/*------------------------[CanNm_RequestBusSynchronization]------------------*/
/* !LINKSTO CANNM072,1 */
#if ((CANNM_BUS_SYNCHRONIZATION_ENABLED == STD_ON)      \
  && (CANNM_PASSIVE_MODE_ENABLED == STD_OFF))
FUNC(Std_ReturnType, CANNM_CODE) CanNm_RequestBusSynchronization
(
 const NetworkHandleType nmChannelHandle
)
{
  Std_ReturnType retVal = E_NOT_OK;

  DBG_CANNM_REQUESTBUSSYNCHRONIZATION_ENTRY(nmChannelHandle);

/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_REQUESTBUSSYNCHRONIZATION,
      CANNM_E_NO_INIT);
  }
  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_REQUESTBUSSYNCHRONIZATION,
      CANNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
    const NetworkHandleType index
       = CanNm_IndexFromNmChannelHandle[nmChannelHandle];
#else
    TS_PARAM_UNUSED(nmChannelHandle);
#endif

#if (CANNM_COM_CONTROL_ENABLED == STD_ON)
    if ((CANNM_CHANNEL_STATUS(index).ChanStatus & CANNM_COM_DISABLED) != 0U)
    {
      /* CANNM181 */
      retVal = E_NOT_OK;
    }
    else
#endif
    {
      if ((CANNM_CHANNEL_STATUS(index).CurState == NM_STATE_BUS_SLEEP)
          || (CANNM_CHANNEL_STATUS(index).CurState == NM_STATE_PREPARE_BUS_SLEEP))
      {
        /* CANNM187 */
        retVal = E_NOT_OK;
      }
      else
      {
        PduInfoType pduInfo;         /* for sending */

        /* prepare pduInfo */
        pduInfo.SduDataPtr = CANNM_CHANNEL_CONFIG(index).TxPduPtr;
        pduInfo.SduLength  = CANNM_CHANNEL_CONFIG(index).PduLength;
#if (CANNM_COM_USER_DATA_SUPPORT == STD_ON)
        /* CANNM328 */
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
        CanNm_GetPduUserData(index, &pduInfo);
#else
        CanNm_GetPduUserData(0U, &pduInfo);
#endif
#endif /* (CANNM_COM_USER_DATA_SUPPORT == STD_ON) */
        if (E_OK == CanIf_Transmit(CANNM_CHANNEL_CONFIG(index).TxPduId, &pduInfo))
        {
          retVal = E_OK;

#if (CANNM_IMMEDIATE_TXCONF_ENABLED == STD_ON)
          CANNM_EMIT(index, CANNM_HSM_CANNM_EV_TX_CONFIRMATION);

#endif
        }

        /* It may be necessary to reset the user data; bug 19201 */
      }
    }
  }


  DBG_CANNM_REQUESTBUSSYNCHRONIZATION_EXIT(retVal,nmChannelHandle);

  return retVal;
}
#endif /* if ((CANNM_BUS_SYNCHRONIZATION == STD_ON)
          && (CANNM_PASSIVE_MODE_ENABLED == STD_OFF))*/

/*------------------------[CanNm_CheckRemoteSleepIndication]-----------------*/

#if (CANNM_REMOTE_SLEEP_IND_ENABLED == STD_ON)
FUNC(Std_ReturnType, CANNM_CODE) CanNm_CheckRemoteSleepIndication
(
  const NetworkHandleType                         nmChannelHandle,
  CONSTP2VAR(boolean, AUTOMATIC, CANNM_APPL_DATA) nmRemoteSleepIndPtr
)
{
  /* It makes no sense to protect this function against interruption, as
   * if the result shall be reliable for the caller, it must protect the
   * whole call against interruption anyway. */

  Std_ReturnType retVal = E_NOT_OK;

  DBG_CANNM_CHECKREMOTESLEEPINDICATION_ENTRY(nmChannelHandle,nmRemoteSleepIndPtr);

/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_CHECKREMOTESLEEPINDICATION,
      CANNM_E_NO_INIT);
  }
  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_CHECKREMOTESLEEPINDICATION,
      CANNM_E_INVALID_CHANNEL);
  }
  else if (nmRemoteSleepIndPtr == NULL_PTR)
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_CHECKREMOTESLEEPINDICATION,
      CANNM_E_NULL_POINTER);
  }
  else
#endif
  {
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
    const NetworkHandleType index
       = CanNm_IndexFromNmChannelHandle[nmChannelHandle];
#else
    TS_PARAM_UNUSED(nmChannelHandle);
#endif

    if ((CANNM_CHANNEL_STATUS(index).CurState == NM_STATE_NORMAL_OPERATION)
        || (CANNM_CHANNEL_STATUS(index).CurState == NM_STATE_READY_SLEEP))
    {
      if ((CANNM_CHANNEL_STATUS(index).ChanStatus & CANNM_RSI_MASK) != CANNM_RSI_REJECT)
      {
        /* CANNM153 */
        if ((CANNM_CHANNEL_STATUS(index).ChanStatus & CANNM_RSI_MASK) == CANNM_RSI_TRUE)
        {
          *nmRemoteSleepIndPtr = TRUE;
          retVal = E_OK;
        }
        else
        {
          *nmRemoteSleepIndPtr = FALSE;
          retVal = E_OK;
        }
      }
      else
      {
        *nmRemoteSleepIndPtr = FALSE;
        retVal = E_OK;
      }
    }
    else
    {
      /* CANNM154 */
      retVal = E_NOT_OK;
    }
  }


  DBG_CANNM_CHECKREMOTESLEEPINDICATION_EXIT(retVal,nmChannelHandle,nmRemoteSleepIndPtr);

  return retVal;
}
#endif /* (CANNM_REMOTE_SLEEP_IND_ENABLED == STD_ON) */


/*------------------------[CanNm_MainFunction]-------------------------------*/

FUNC(void, CANNM_CODE) CanNm_MainFunction(void)
{
  /* Always perform initialization check w/o Det report. See EB
   * req. CanNm.MainFunction.InitCheck. Related SWS requirements are not
   * fulfilled. */

  DBG_CANNM_MAINFUNCTION_ENTRY();
  if (TRUE == CanNm_Initialized)
  {
#if (CANNM_PN_EIRA_CALC_ENABLED == STD_ON)
    uint8 Index;
    uint8 EiraValueOld[CANNM_PN_INFO_LENGTH];
    boolean EiraChanged = FALSE;
#endif

    /* process timers and emit timout evants */
#if (CANNM_HSM_CANNM_NUM_INST > 1U)
    uint8 i;
    for (i = 0U; i < CANNM_HSM_CANNM_NUM_INST; ++i)
#endif
    {
      CanNm_HandleTimerTick(CANNM_PL_SF(i));
    }

    /* Main processing function of HSM */
    CanNm_HsmMain(&CanNm_HsmScCanNm);

#if (CANNM_PN_EIRA_CALC_ENABLED == STD_ON)

    TS_MemCpy(EiraValueOld, CanNm_PnStatus.EiraValue, CANNM_PN_INFO_LENGTH);

    CanNm_HandlePnTimers();

    /* Aggregate EIRA  for each channel */
    for (Index = 0U; Index < CANNM_NUMBER_OF_CHANNELS; Index++)
    {
      VAR (uint8, AUTOMATIC) PnInfoEra[CANNM_PN_INFO_LENGTH];
      SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
      /* copy and clear external requests */
      TS_MemCpy(PnInfoEra, CANNM_CHANNEL_STATUS(Index).PnInfoEra, CANNM_PN_INFO_LENGTH);
      TS_MemSet(CANNM_CHANNEL_STATUS(Index).PnInfoEra, 0U, CANNM_PN_INFO_LENGTH);
      SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
      CanNm_HandlePnEira(PnInfoEra);
    }

    for(Index = 0; Index < CANNM_PN_INFO_LENGTH; Index++)
    {
      if(EiraValueOld[Index] != CanNm_PnStatus.EiraValue[Index])
      {
         EiraChanged = TRUE;
      }
    }
    /* Publish EIRA to ComM (via PduR and Com) if value has change */
    if (EiraChanged == TRUE)
    {
      PduInfoType pduInfo;
      pduInfo.SduDataPtr = CanNm_PnStatus.EiraValue;
      pduInfo.SduLength = CANNM_PN_INFO_LENGTH;
      PduR_CanNmRxIndication(CANNM_PN_EIRA_PDUID,&pduInfo);
    }
#endif
  }

  DBG_CANNM_MAINFUNCTION_EXIT();
}

/*------------------------[CanNm_TxConfirmation]-----------------------------*/
/* !LINKSTO CANNM072,1 */
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)             \
  && (CANNM_IMMEDIATE_TXCONF_ENABLED == STD_OFF)
FUNC(void, CANNM_CODE) CanNm_TxConfirmation
(
 PduIdType TxPduId
)
{
  DBG_CANNM_TXCONFIRMATION_ENTRY(TxPduId);

  /* Perform checks, get index */
/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      CANNM_INSTANCE_ID, CANNM_SERVID_TXCONFIRMATION, CANNM_E_NO_INIT);
  }
  else
#endif
  {
#if ((CANNM_NUMBER_OF_CHANNELS > 1U) || (CANNM_DEV_ERROR_DETECT == STD_ON))
    NetworkHandleType index = 0U;

    /* lookup CanNm channel related given TxPduId */
    for (index = 0U;
         (index < CANNM_NUMBER_OF_CHANNELS)
           && (CANNM_CHANNEL_CONFIG(index).TxConfPduId != TxPduId);
         ++index)
    {
      /* empty search loop */
    }
#endif
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
    if (index >= CANNM_NUMBER_OF_CHANNELS)
    {
      CANNM_DET_REPORT_ERROR(
        CANNM_INVALID_PDU_INSTANCE_ID, CANNM_SERVID_TXCONFIRMATION,
        CANNM_E_INVALID_TXPDUID);
    }
    else
#endif
    {
#if (CANNM_COM_USER_DATA_SUPPORT == STD_ON)
      /* CANNM329 */
      if (0U != CANNM_CHANNEL_CONFIG(index).UserDataLength)
      {
        PduR_CanNmTxConfirmation(CANNM_CHANNEL_CONFIG(index).UserTxConfPduId);
      }
#endif /* (CANNM_COM_USER_DATA_SUPPORT == STD_ON) */

      CANNM_EMIT(index, CANNM_HSM_CANNM_EV_TX_CONFIRMATION);
    }
  }
#if ((CANNM_HSM_INST_MULTI_ENABLED == STD_OFF) && (CANNM_DEV_ERROR_DETECT == STD_OFF))
  TS_PARAM_UNUSED(TxPduId);
#endif

  DBG_CANNM_TXCONFIRMATION_EXIT(TxPduId);
}
#endif /* (CANNM_PASSIVE_MODE_ENABLED == STD_OFF) &&
          (CANNM_IMMEDIATE_TXCONF_ENABLED == STD_OFF) */

/*------------------------[CanNm_RxIndication]-------------------------------*/

FUNC(void, CANNM_CODE) CanNm_RxIndication
(
 PduIdType                                      RxPduId,
 P2VAR(PduInfoType, AUTOMATIC, CANNM_APPL_DATA) PduInfoPtr
)
{
#if (CANNM_COM_USER_DATA_SUPPORT == STD_ON)
  VAR(PduInfoType, CANNM_APPL_DATA) UserPdu;
#endif /* (CANNM_COM_USER_DATA_SUPPORT == STD_ON) */

  DBG_CANNM_RXINDICATION_ENTRY(RxPduId,PduInfoPtr);

/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      CANNM_INSTANCE_ID, CANNM_SERVID_RXINDICATION, CANNM_E_NO_INIT);
  }
  else if (RxPduId >= CANNM_NUMBER_OF_CHANNELS)
  {
    CANNM_DET_REPORT_ERROR(
      CANNM_INVALID_PDU_INSTANCE_ID, CANNM_SERVID_RXINDICATION,
      CANNM_E_INVALID_RXPDUID);
  }
  else if (((PduInfoPtr == NULL_PTR) ||
            (PduInfoPtr->SduDataPtr == NULL_PTR)
           ) ||
           (PduInfoPtr->SduLength > CANNM_CHANNEL_CONFIG(RxPduId).PduLength)
          )
  {
   /* Here we report FRNM_E_INVALID_POINTER for length overflow,too */
    CANNM_DET_REPORT_ERROR(
      CanNm_ChanConfig[RxPduId].nmChannelId,
      CANNM_SERVID_RXINDICATION, CANNM_E_NULL_POINTER);
  }
  else
#endif
  {
#if (CANNM_PN_ENABLED == STD_ON)
    boolean ValidPnMessage = FALSE;
#endif

    SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
    /* CANNM035 */
    /* Copy Pdu Data to internal buffer */
    TS_MemCpy(
      CanNm_ChanConfig[RxPduId].RxPduPtr, PduInfoPtr->SduDataPtr, PduInfoPtr->SduLength);

#if (CANNM_PN_EIRA_CALC_ENABLED == STD_ON)
    ValidPnMessage = CanNm_IsValidPnMessage(RxPduId);
    if (ValidPnMessage == TRUE)
    {
      /* CanNm_RxIndication could be called more than once within the interval of 2 main function
       * calls. Therefore, the external requests to be aggregated.
       */
      CanNm_AggregateEra(RxPduId, &CanNm_ChanConfig[RxPduId].RxPduPtr[CANNM_PN_INFO_OFFSET], CANNM_APPLY_FILTER_MASK);
    }
#endif
SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();

#if (CANNM_PN_ENABLED == STD_ON)
    if (((ValidPnMessage == TRUE) ||
          /* CANNM409 : Partial networking is not supported */
         (CanNm_ChanConfig[RxPduId].PnEnabled == FALSE)
        ) ||
        /* CANNM410 */
        ((CanNm_ChanConfig[RxPduId].AllNmMessagesKeepAwake == TRUE) ||
         /* CANNM403 : Filtering is not yet enabled */
         (CanNm_ChanStatus[RxPduId].PnFilterEnabled == FALSE)
        )
       )
#endif /*(CANNM_PN_ENABLED == STD_ON)*/
    {
#if (CANNM_COM_USER_DATA_SUPPORT == STD_ON)
      if((CanNm_ChanConfig[RxPduId].UserRxPduEnabled == TRUE) &&
         (0U != CanNm_ChanConfig[RxPduId].UserDataLength)
        )
      {
        UserPdu.SduLength = CanNm_ChanConfig[RxPduId].UserDataLength;
        UserPdu.SduDataPtr =
          &(CanNm_ChanConfig[RxPduId].RxPduPtr[CanNm_ChanConfig[RxPduId].UDFBPos]);

        PduR_CanNmRxIndication(CanNm_ChanConfig[RxPduId].UserRxPduId, &UserPdu);
      }
#endif /* (CANNM_COM_USER_DATA_SUPPORT == STD_ON) */

#if (CANNM_NODE_DETECTION_ENABLED == STD_ON)
      /* Notify again if the RepeatMessageBit is set */
      if (TS_IsBitSet(&(CanNm_ChanConfig[RxPduId].RxPduPtr[CanNm_ChanConfig[RxPduId].CbvPos]),
                      CANNM_CBV_REPEATMESSAGEBIT, uint8
                     )
         )
      {
#if (CANNM_REPEAT_MSG_IND_ENABLED == STD_ON)
        uint8 Index;

        /* Nm_RepeatMessageIndication has to be called for all the channels
           which are configured with this Rx Pdu */
        for (Index = 0U; Index < CANNM_NUMBER_OF_CHANNELS; Index++)
        {
          if (CANNM_CHANNEL_CONFIG(Index).RxPduId == RxPduId)
          {
            /* CANNM014 */
            Nm_RepeatMessageIndication(CanNm_ChanConfig[Index].nmChannelId);
          }
        }
#endif
        CANNM_EMIT((uint8)RxPduId, CANNM_HSM_CANNM_EV_REPEAT_MESSAGE_REASON);
      }
#endif

      CANNM_EMIT((uint8)RxPduId, CANNM_HSM_CANNM_EV_RX_INDICATION);
    }
  }

  DBG_CANNM_RXINDICATION_EXIT(RxPduId,PduInfoPtr);
}

/*------------------------[CanNm_Transmit]-----------------------------------*/
#if ( CANNM_COM_USER_DATA_SUPPORT == STD_ON )
FUNC(Std_ReturnType, CANNM_CODE) CanNm_Transmit(
    PduIdType CanTxPduId,
    P2CONST(PduInfoType, AUTOMATIC, CANNM_APPL_DATA) PduInfoPtr
)
{
  TS_PARAM_UNUSED(CanTxPduId);
  TS_PARAM_UNUSED(PduInfoPtr);

  DBG_CANNM_TRANSMIT_ENTRY(CanTxPduId,PduInfoPtr);

  /* CANNM331, CANNM330 */

  DBG_CANNM_TRANSMIT_EXIT(E_OK,CanTxPduId,PduInfoPtr);

  return E_OK;
}
#endif

/*------------------------[CanNm_GetPduUserData]-----------------------------*/
#if ((CANNM_COM_USER_DATA_SUPPORT == STD_ON) || (CANNM_PN_EIRA_CALC_ENABLED == STD_ON))
FUNC(void, CANNM_CODE)CanNm_GetPduUserData(
const uint8 instIdx,
P2VAR(PduInfoType, AUTOMATIC, CANNM_APPL_DATA) pduInfo )
{
#if (((CANNM_COM_USER_DATA_SUPPORT != STD_ON) || \
      (CANNM_MAX_USERDATA_LENGTH == 0 ) || \
      (CANNM_HSM_INST_MULTI_ENABLED != STD_ON)) && \
     (CANNM_PN_EIRA_CALC_ENABLED != STD_ON))
  TS_PARAM_UNUSED(instIdx);
  TS_PARAM_UNUSED(pduInfo);
#endif
  DBG_CANNM_GETPDUUSERDATA_ENTRY(instIdx,pduInfo);

#if ((CANNM_COM_USER_DATA_SUPPORT == STD_ON) && ( CANNM_MAX_USERDATA_LENGTH != 0 ))
  if( 0U != CANNM_CHANNEL_CONFIG(instIdx).UserDataLength )
  {
    uint8 temp_buf[CANNM_MAX_USERDATA_LENGTH];
    /* CANNM328 */
    uint8 UDFBPos = CANNM_CHANNEL_CONFIG(instIdx).UDFBPos;
    PduInfoType PduUserInfo;

    PduUserInfo.SduDataPtr = &(temp_buf[0]);
    PduUserInfo.SduLength  = pduInfo->SduLength - UDFBPos;
    if (E_OK ==
        PduR_CanNmTriggerTransmit(CANNM_CHANNEL_CONFIG(instIdx).UserTxConfPduId, &PduUserInfo)
       )
    {
      /* Only Copy if valid data is there. Otherwise proceed with old data */
      TS_MemCpy( &(pduInfo->SduDataPtr[UDFBPos]), &(temp_buf[0]),
                  PduUserInfo.SduLength);
    }
  }
#endif
#if (CANNM_PN_EIRA_CALC_ENABLED == STD_ON)
  /* Protect aggregation of EIRA as this is being called from RxIndication too */
  SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
  CanNm_AggregateEra((PduIdType)instIdx, &pduInfo->SduDataPtr[CANNM_PN_INFO_OFFSET], CANNM_DO_NOT_APPLY_FILTER_MASK);
  SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
#endif

  DBG_CANNM_GETPDUUSERDATA_EXIT(instIdx,pduInfo);
}
#endif /* ( CANNM_COM_USER_DATA_SUPPORT == STD_ON ) */


/*------------------------[CanNm_ConfirmPnAvailability]----------------------*/
#if (CANNM_PN_ENABLED == STD_ON)
FUNC(void, CANNM_CODE) CanNm_ConfirmPnAvailability
(
  const NetworkHandleType nmChannelHandle
)
{
  DBG_CANNM_CONFIRMPNAVAILABILITY_ENTRY(nmChannelHandle);

/* !LINKSTO CANNM243,1 */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  if (CanNm_Initialized == FALSE)
  {
    CANNM_DET_REPORT_ERROR(
      CANNM_INSTANCE_ID, CANNM_SERVID_CONFIRMPNAVAILABILITY, CANNM_E_NO_INIT);
  }
  else if ((nmChannelHandle >= CANNM_INDEXFROMNMCHANNELHANDLE_NUM)
    || (CanNm_IndexFromNmChannelHandle[nmChannelHandle]
      >= CANNM_NUMBER_OF_CHANNELS))
  {
    CANNM_DET_REPORT_ERROR(
      nmChannelHandle, CANNM_SERVID_CONFIRMPNAVAILABILITY, CANNM_E_INVALID_CHANNEL);
  }
  else
#endif
  {
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
    const NetworkHandleType index
       = CanNm_IndexFromNmChannelHandle[nmChannelHandle];
#else
    TS_PARAM_UNUSED(nmChannelHandle);
#endif
#if (CANNM_PN_ENABLED == STD_ON)
   if (CANNM_CHANNEL_CONFIG(index).PnEnabled == TRUE)
   {
     CANNM_CHANNEL_STATUS(index).PnFilterEnabled = TRUE;
   }
#endif
  }

  DBG_CANNM_CONFIRMPNAVAILABILITY_EXIT(nmChannelHandle);
}
#endif /* (CANNM_PN_ENABLED == STD_ON) */

/*=====================[external Non API function definitions]=====================*/
#if (CANNM_PN_EIRA_CALC_ENABLED == STD_ON)
FUNC(boolean, CANNM_CODE) CanNm_IsValidPnMessage
(
  const PduIdType PduId
)
{
  boolean ValidMessage = FALSE;
  P2CONST(uint8, AUTOMATIC, AUTOMATIC) PnInfo =
          &CanNm_ChanConfig[PduId].RxPduPtr[CANNM_PN_INFO_OFFSET];

  DBG_CANNM_ISVALIDPNMESSAGE_ENTRY(PduId);

  if ( (CanNm_ChanStatus[PduId].PnFilterEnabled == TRUE) &&
       (TS_IsBitSet(&(CanNm_ChanConfig[PduId].RxPduPtr[CanNm_ChanConfig[PduId].CbvPos]),
                    CANNM_CBV_PNINFOBIT, uint8
                   )
       )
     )
  {
    uint8 PnPduId;
    for (PnPduId=0U; ((PnPduId < CANNM_PN_INFO_LENGTH) && (ValidMessage == FALSE)); PnPduId++)
    {
      /* !LINKSTO CANNM418,1 */
      if ((PnInfo[PnPduId] & CanNm_PnFilterMask[PnPduId]) != 0U)
      {
        ValidMessage = TRUE;
      }
    }
  }

  DBG_CANNM_ISVALIDPNMESSAGE_EXIT((ValidMessage),PduId);

  return (ValidMessage);
}

FUNC(void, CANNM_CODE) CanNm_HandlePnEira
(
  CONSTP2CONST(uint8, AUTOMATIC, CANNM_APPL_CONST) MessageBuffer
)
{
  uint8 PnIndex;

  DBG_CANNM_HANDLEPNEIRA_ENTRY(MessageBuffer);

  for (PnIndex=0U; PnIndex < CANNM_PN_INFO_LENGTH; PnIndex++)
  {
    uint8 UnmaskedEiraValueByte = MessageBuffer[PnIndex] &  CanNm_PnFilterMask[PnIndex];
    uint8 PnBitPos = 0U;

    CanNm_PnStatus.EiraValue[PnIndex] |= MessageBuffer[PnIndex];
    /* Wind up timer for unmasked PNC active bits */
    while (UnmaskedEiraValueByte != 0U)
    {
      /* Check if the current LSB is set to 1 and if so reset the corresponding timer */
      if ((UnmaskedEiraValueByte & 0x01U) != 0U)
      {
          /* load timer value */
          CanNm_PnStatus.EiraTimer[CanNm_EiraTimerMap[(PnIndex * 8U) + PnBitPos]]
                = CANNM_PN_RESET_TIME;
      }
      /* Shift the LSB because it was checked in the current loop */
      UnmaskedEiraValueByte >>= 1U;
      PnBitPos++;
    }
  }
  DBG_CANNM_HANDLEPNEIRA_EXIT(MessageBuffer);
}

#endif
/*=====================[internal functions definitions]=====================*/

STATIC FUNC(void, CANNM_CODE) CanNm_HandleTimerTick(
   CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HANDLETIMERTICK_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HANDLETIMERTICK_ENTRY();
#endif

  CANNM_HANDLE_TICK(instIdx, UniversalTimer, CANNM_HSM_CANNM_EV_UNI_TIMEOUT);
  CANNM_HANDLE_TICK(instIdx, RmsTimer, CANNM_HSM_CANNM_EV_RMS_TIMEOUT);
#if ((CANNM_IMMEDIATE_TXCONF_ENABLED == STD_OFF) \
  && (CANNM_PASSIVE_MODE_ENABLED == STD_OFF))
  CANNM_HANDLE_TICK(instIdx, TimeoutTimer, CANNM_HSM_CANNM_EV_TX_TIMEOUT);
#endif
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
  CANNM_HANDLE_TICK(instIdx, MsgCycleTimer, CANNM_HSM_CANNM_EV_MSG_CYCLE_TIMEOUT);
#endif
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
  /*ready to expire*/
  if(CANNM_CHANNEL_STATUS(instIdx).CanNmTimer == 1U)
  {
    CANNM_CHANNEL_STATUS(instIdx).NmTimerExpired = TRUE;
  }
#endif
  CANNM_HANDLE_TICK(instIdx, CanNmTimer, CANNM_HSM_CANNM_EV_NM_TIMEOUT);

#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HANDLETIMERTICK_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HANDLETIMERTICK_EXIT();
#endif
}

#if (CANNM_PN_EIRA_CALC_ENABLED == STD_ON)
STATIC FUNC(void, CANNM_CODE) CanNm_HandlePnTimers
(
  void
)
{
  uint8 PnIndex;

  DBG_CANNM_HANDLEPNTIMERS_ENTRY();

  for (PnIndex = 0U; PnIndex < CANNM_PN_EIRA_TIMER_ARR_SIZE; PnIndex++)
  {
    /* Get EiraTimer index from mapped Eira timer array.
     *
     * Timers in EiraTimer holds values corresponding to each PN (each bit)
     * by mapping through CanNm_EiraTimerMap.
     * For example EiraTimer[0] is the timer for PN0 (Bit 0 of EiraValue[0]) and
     * EiraTimer[9] is the timer for PN9 (Bit 1 of EiraValue[1]). */
    uint8 EiraIndex = CanNm_EiraTimerMap[PnIndex];

    if (CANNM_EIRA_TIMER_INVALID != EiraIndex)
    {
      if (0U < CanNm_PnStatus.EiraTimer[EiraIndex])
      {
        /* Decrement PN timer. */
        CanNm_PnStatus.EiraTimer[EiraIndex]--;

        if (0U == CanNm_PnStatus.EiraTimer[EiraIndex])
        {
          /* If PN timer get expired, then clear the corresponding PN status bit
           * and set the flag to indicate a change in Eira status */
          CanNm_PnStatus.EiraValue[PnIndex/8U] &= (uint8)~((uint8)(1U << (PnIndex%8U)));
        }
      }
    }
    else
    {
      /* For internal messages the timer is not started so on the next cycle the corresponding
       * EIRA bit is reseted; these bits are set in CanNm_HandlePnEira function */
      CanNm_PnStatus.EiraValue[PnIndex/8U] &= (uint8)~((uint8)(1U << (PnIndex%8U)));
    }
  }
  DBG_CANNM_HANDLEPNTIMERS_EXIT();
}
STATIC FUNC(void, CANNM_CODE) CanNm_AggregateEra
(
  const PduIdType instIdx,
  CONSTP2CONST(uint8, AUTOMATIC, CANNM_APPL_CONST) PnInfo,
  uint8 applyFilterMask
)
{
  uint8 PnIndex;

#if (CANNM_HSM_INST_MULTI_ENABLED == STD_OFF)
  TS_PARAM_UNUSED(instIdx);
#endif

  DBG_CANNM_AGGREGATEERA_ENTRY(instIdx,PnInfo);

  for (PnIndex=0U; PnIndex < CANNM_PN_INFO_LENGTH; PnIndex++)
  {
    /* Aggregate external requests */
    if(applyFilterMask == CANNM_APPLY_FILTER_MASK)
    {
        /*In case of external messages apply Filter mask*/
        CANNM_CHANNEL_STATUS(instIdx).PnInfoEra[PnIndex] |=
         (uint8)(PnInfo[PnIndex] &  CanNm_PnFilterMask[PnIndex]);
    }
    else
    {
        /*In case of internal messages do not apply Filter mask*/
        CANNM_CHANNEL_STATUS(instIdx).PnInfoEra[PnIndex] |= (uint8)PnInfo[PnIndex];
    }
  }

  DBG_CANNM_AGGREGATEERA_EXIT(instIdx,PnInfo);
}
#endif
#define CANNM_STOP_SEC_CODE
#include <MemMap.h>

/*==============================[end of file]===============================*/
