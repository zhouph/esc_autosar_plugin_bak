/**
 * \file
 *
 * \brief AUTOSAR CanNm
 *
 * This file contains the implementation of the AUTOSAR
 * module CanNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined CANNM_TRACE_H)
#define CANNM_TRACE_H
/*==================[inclusions]============================================*/

#include <TSAutosar.h>

[!IF "node:exists(as:modconf('Dbg'))"!]
#include <Dbg.h>
[!ENDIF!]

/*==================[macros]================================================*/

[!VAR "Parameter01"="''"!][!//
[!VAR "Parameter12"="'a'"!][!//
[!IF "num:i(count(CanNmGlobalConfig/*[1]/CanNmChannelConfig/*)) > 1"!][!//
  [!VAR "Parameter01"="'a'"!][!//
  [!VAR "Parameter12"="'a, b'"!][!//
[!ENDIF!][!//


#ifndef DBG_CANNM_GENERIC_GRP
/** \brief Gegeric state change */
#define DBG_CANNM_GENERIC_GRP(a,b,c,d) TS_PARAM_UNUSED(c)
#endif

#ifndef DBG_CANNM_INIT_ENTRY
/** \brief Entry point of function CanNm_Init() */
#define DBG_CANNM_INIT_ENTRY(a)
#endif

#ifndef DBG_CANNM_INIT_EXIT
/** \brief Exit point of function CanNm_Init() */
#define DBG_CANNM_INIT_EXIT(a)
#endif

#ifndef DBG_CANNM_PASSIVESTARTUP_ENTRY
/** \brief Entry point of function CanNm_PassiveStartUp() */
#define DBG_CANNM_PASSIVESTARTUP_ENTRY(a)
#endif

#ifndef DBG_CANNM_PASSIVESTARTUP_EXIT
/** \brief Exit point of function CanNm_PassiveStartUp() */
#define DBG_CANNM_PASSIVESTARTUP_EXIT(a,b)
#endif

#ifndef DBG_CANNM_NETWORKREQUEST_ENTRY
/** \brief Entry point of function CanNm_NetworkRequest() */
#define DBG_CANNM_NETWORKREQUEST_ENTRY(a)
#endif

#ifndef DBG_CANNM_NETWORKREQUEST_EXIT
/** \brief Exit point of function CanNm_NetworkRequest() */
#define DBG_CANNM_NETWORKREQUEST_EXIT(a,b)
#endif

#ifndef DBG_CANNM_NETWORKRELEASE_ENTRY
/** \brief Entry point of function CanNm_NetworkRelease() */
#define DBG_CANNM_NETWORKRELEASE_ENTRY(a)
#endif

#ifndef DBG_CANNM_NETWORKRELEASE_EXIT
/** \brief Exit point of function CanNm_NetworkRelease() */
#define DBG_CANNM_NETWORKRELEASE_EXIT(a,b)
#endif

#ifndef DBG_CANNM_DISABLECOMMUNICATION_ENTRY
/** \brief Entry point of function CanNm_DisableCommunication() */
#define DBG_CANNM_DISABLECOMMUNICATION_ENTRY(a)
#endif

#ifndef DBG_CANNM_DISABLECOMMUNICATION_EXIT
/** \brief Exit point of function CanNm_DisableCommunication() */
#define DBG_CANNM_DISABLECOMMUNICATION_EXIT(a,b)
#endif

#ifndef DBG_CANNM_ENABLECOMMUNICATION_ENTRY
/** \brief Entry point of function CanNm_EnableCommunication() */
#define DBG_CANNM_ENABLECOMMUNICATION_ENTRY(a)
#endif

#ifndef DBG_CANNM_ENABLECOMMUNICATION_EXIT
/** \brief Exit point of function CanNm_EnableCommunication() */
#define DBG_CANNM_ENABLECOMMUNICATION_EXIT(a,b)
#endif

#ifndef DBG_CANNM_SETUSERDATA_ENTRY
/** \brief Entry point of function CanNm_SetUserData() */
#define DBG_CANNM_SETUSERDATA_ENTRY(a,b)
#endif

#ifndef DBG_CANNM_SETUSERDATA_EXIT
/** \brief Exit point of function CanNm_SetUserData() */
#define DBG_CANNM_SETUSERDATA_EXIT(a,b,c)
#endif

#ifndef DBG_CANNM_GETUSERDATA_ENTRY
/** \brief Entry point of function CanNm_GetUserData() */
#define DBG_CANNM_GETUSERDATA_ENTRY(a,b)
#endif

#ifndef DBG_CANNM_GETUSERDATA_EXIT
/** \brief Exit point of function CanNm_GetUserData() */
#define DBG_CANNM_GETUSERDATA_EXIT(a,b,c)
#endif

#ifndef DBG_CANNM_GETNODEIDENTIFIER_ENTRY
/** \brief Entry point of function CanNm_GetNodeIdentifier() */
#define DBG_CANNM_GETNODEIDENTIFIER_ENTRY(a,b)
#endif

#ifndef DBG_CANNM_GETNODEIDENTIFIER_EXIT
/** \brief Exit point of function CanNm_GetNodeIdentifier() */
#define DBG_CANNM_GETNODEIDENTIFIER_EXIT(a,b,c)
#endif

#ifndef DBG_CANNM_GETLOCALNODEIDENTIFIER_ENTRY
/** \brief Entry point of function CanNm_GetLocalNodeIdentifier() */
#define DBG_CANNM_GETLOCALNODEIDENTIFIER_ENTRY(a,b)
#endif

#ifndef DBG_CANNM_GETLOCALNODEIDENTIFIER_EXIT
/** \brief Exit point of function CanNm_GetLocalNodeIdentifier() */
#define DBG_CANNM_GETLOCALNODEIDENTIFIER_EXIT(a,b,c)
#endif

#ifndef DBG_CANNM_REPEATMESSAGEREQUEST_ENTRY
/** \brief Entry point of function CanNm_RepeatMessageRequest() */
#define DBG_CANNM_REPEATMESSAGEREQUEST_ENTRY(a)
#endif

#ifndef DBG_CANNM_REPEATMESSAGEREQUEST_EXIT
/** \brief Exit point of function CanNm_RepeatMessageRequest() */
#define DBG_CANNM_REPEATMESSAGEREQUEST_EXIT(a,b)
#endif

#ifndef DBG_CANNM_GETPDUDATA_ENTRY
/** \brief Entry point of function CanNm_GetPduData() */
#define DBG_CANNM_GETPDUDATA_ENTRY(a,b)
#endif

#ifndef DBG_CANNM_GETPDUDATA_EXIT
/** \brief Exit point of function CanNm_GetPduData() */
#define DBG_CANNM_GETPDUDATA_EXIT(a,b,c)
#endif

#ifndef DBG_CANNM_GETSTATE_ENTRY
/** \brief Entry point of function CanNm_GetState() */
#define DBG_CANNM_GETSTATE_ENTRY(a,b,c)
#endif

#ifndef DBG_CANNM_GETSTATE_EXIT
/** \brief Exit point of function CanNm_GetState() */
#define DBG_CANNM_GETSTATE_EXIT(a,b,c,d)
#endif

#ifndef DBG_CANNM_GETVERSIONINFO_ENTRY
/** \brief Entry point of function CanNm_GetVersionInfo() */
#define DBG_CANNM_GETVERSIONINFO_ENTRY(a)
#endif

#ifndef DBG_CANNM_GETVERSIONINFO_EXIT
/** \brief Exit point of function CanNm_GetVersionInfo() */
#define DBG_CANNM_GETVERSIONINFO_EXIT(a)
#endif

#ifndef DBG_CANNM_REQUESTBUSSYNCHRONIZATION_ENTRY
/** \brief Entry point of function CanNm_RequestBusSynchronization() */
#define DBG_CANNM_REQUESTBUSSYNCHRONIZATION_ENTRY(a)
#endif

#ifndef DBG_CANNM_REQUESTBUSSYNCHRONIZATION_EXIT
/** \brief Exit point of function CanNm_RequestBusSynchronization() */
#define DBG_CANNM_REQUESTBUSSYNCHRONIZATION_EXIT(a,b)
#endif

#ifndef DBG_CANNM_CHECKREMOTESLEEPINDICATION_ENTRY
/** \brief Entry point of function CanNm_CheckRemoteSleepIndication() */
#define DBG_CANNM_CHECKREMOTESLEEPINDICATION_ENTRY(a,b)
#endif

#ifndef DBG_CANNM_CHECKREMOTESLEEPINDICATION_EXIT
/** \brief Exit point of function CanNm_CheckRemoteSleepIndication() */
#define DBG_CANNM_CHECKREMOTESLEEPINDICATION_EXIT(a,b,c)
#endif

#ifndef DBG_CANNM_MAINFUNCTION_ENTRY
/** \brief Entry point of function CanNm_MainFunction() */
#define DBG_CANNM_MAINFUNCTION_ENTRY()
#endif

#ifndef DBG_CANNM_MAINFUNCTION_EXIT
/** \brief Exit point of function CanNm_MainFunction() */
#define DBG_CANNM_MAINFUNCTION_EXIT()
#endif

#ifndef DBG_CANNM_TXCONFIRMATION_ENTRY
/** \brief Entry point of function CanNm_TxConfirmation() */
#define DBG_CANNM_TXCONFIRMATION_ENTRY(a)
#endif

#ifndef DBG_CANNM_TXCONFIRMATION_EXIT
/** \brief Exit point of function CanNm_TxConfirmation() */
#define DBG_CANNM_TXCONFIRMATION_EXIT(a)
#endif

#ifndef DBG_CANNM_RXINDICATION_ENTRY
/** \brief Entry point of function CanNm_RxIndication() */
#define DBG_CANNM_RXINDICATION_ENTRY(a,b)
#endif

#ifndef DBG_CANNM_RXINDICATION_EXIT
/** \brief Exit point of function CanNm_RxIndication() */
#define DBG_CANNM_RXINDICATION_EXIT(a,b)
#endif

#ifndef DBG_CANNM_TRANSMIT_ENTRY
/** \brief Entry point of function CanNm_Transmit() */
#define DBG_CANNM_TRANSMIT_ENTRY(a,b)
#endif

#ifndef DBG_CANNM_TRANSMIT_EXIT
/** \brief Exit point of function CanNm_Transmit() */
#define DBG_CANNM_TRANSMIT_EXIT(a,b,c)
#endif

#ifndef DBG_CANNM_GETPDUUSERDATA_ENTRY
/** \brief Entry point of function CanNm_GetPduUserData() */
#define DBG_CANNM_GETPDUUSERDATA_ENTRY(a,b)
#endif

#ifndef DBG_CANNM_GETPDUUSERDATA_EXIT
/** \brief Exit point of function CanNm_GetPduUserData() */
#define DBG_CANNM_GETPDUUSERDATA_EXIT(a,b)
#endif

#ifndef DBG_CANNM_CONFIRMPNAVAILABILITY_ENTRY
/** \brief Entry point of function CanNm_ConfirmPnAvailability() */
#define DBG_CANNM_CONFIRMPNAVAILABILITY_ENTRY(a)
#endif

#ifndef DBG_CANNM_CONFIRMPNAVAILABILITY_EXIT
/** \brief Exit point of function CanNm_ConfirmPnAvailability() */
#define DBG_CANNM_CONFIRMPNAVAILABILITY_EXIT(a)
#endif

#ifndef DBG_CANNM_ISVALIDPNMESSAGE_ENTRY
/** \brief Entry point of function CanNm_IsValidPnMessage() */
#define DBG_CANNM_ISVALIDPNMESSAGE_ENTRY(a)
#endif

#ifndef DBG_CANNM_ISVALIDPNMESSAGE_EXIT
/** \brief Exit point of function CanNm_IsValidPnMessage() */
#define DBG_CANNM_ISVALIDPNMESSAGE_EXIT(a,b)
#endif

#ifndef DBG_CANNM_HANDLEPNEIRA_ENTRY
/** \brief Entry point of function CanNm_HandlePnEira() */
#define DBG_CANNM_HANDLEPNEIRA_ENTRY(a)
#endif

#ifndef DBG_CANNM_HANDLEPNEIRA_EXIT
/** \brief Exit point of function CanNm_HandlePnEira() */
#define DBG_CANNM_HANDLEPNEIRA_EXIT(a)
#endif

#ifndef DBG_CANNM_HANDLETIMERTICK_ENTRY
/** \brief Entry point of function CanNm_HandleTimerTick() */
#define DBG_CANNM_HANDLETIMERTICK_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HANDLETIMERTICK_EXIT
/** \brief Exit point of function CanNm_HandleTimerTick() */
#define DBG_CANNM_HANDLETIMERTICK_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HANDLEPNTIMERS_ENTRY
/** \brief Entry point of function CanNm_HandlePnTimers() */
#define DBG_CANNM_HANDLEPNTIMERS_ENTRY()
#endif

#ifndef DBG_CANNM_HANDLEPNTIMERS_EXIT
/** \brief Exit point of function CanNm_HandlePnTimers() */
#define DBG_CANNM_HANDLEPNTIMERS_EXIT()
#endif

#ifndef DBG_CANNM_AGGREGATEERA_ENTRY
/** \brief Entry point of function CanNm_AggregateEra() */
#define DBG_CANNM_AGGREGATEERA_ENTRY(a,b)
#endif

#ifndef DBG_CANNM_AGGREGATEERA_EXIT
/** \brief Exit point of function CanNm_AggregateEra() */
#define DBG_CANNM_AGGREGATEERA_EXIT(a,b)
#endif

#ifndef DBG_CANNM_INITINTVAR_ENTRY
/** \brief Entry point of function CanNm_InitIntVar() */
#define DBG_CANNM_INITINTVAR_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_INITINTVAR_EXIT
/** \brief Exit point of function CanNm_InitIntVar() */
#define DBG_CANNM_INITINTVAR_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_NMTIMERSTART_ENTRY
/** \brief Entry point of function CanNm_NmTimerStart() */
#define DBG_CANNM_NMTIMERSTART_ENTRY([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_NMTIMERSTART_EXIT
/** \brief Exit point of function CanNm_NmTimerStart() */
#define DBG_CANNM_NMTIMERSTART_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_NMTIMERSTOP_ENTRY
/** \brief Entry point of function CanNm_NmTimerStop() */
#define DBG_CANNM_NMTIMERSTOP_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_NMTIMERSTOP_EXIT
/** \brief Exit point of function CanNm_NmTimerStop() */
#define DBG_CANNM_NMTIMERSTOP_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_UNITIMERSTART_ENTRY
/** \brief Entry point of function CanNm_UniTimerStart() */
#define DBG_CANNM_UNITIMERSTART_ENTRY([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_UNITIMERSTART_EXIT
/** \brief Exit point of function CanNm_UniTimerStart() */
#define DBG_CANNM_UNITIMERSTART_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_UNITIMERSTOP_ENTRY
/** \brief Entry point of function CanNm_UniTimerStop() */
#define DBG_CANNM_UNITIMERSTOP_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_UNITIMERSTOP_EXIT
/** \brief Exit point of function CanNm_UniTimerStop() */
#define DBG_CANNM_UNITIMERSTOP_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_RMSTIMERSTART_ENTRY
/** \brief Entry point of function CanNm_RmsTimerStart() */
#define DBG_CANNM_RMSTIMERSTART_ENTRY([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_RMSTIMERSTART_EXIT
/** \brief Exit point of function CanNm_RmsTimerStart() */
#define DBG_CANNM_RMSTIMERSTART_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_MSGCYCLETIMERSTART_ENTRY
/** \brief Entry point of function CanNm_MsgCycleTimerStart() */
#define DBG_CANNM_MSGCYCLETIMERSTART_ENTRY([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_MSGCYCLETIMERSTART_EXIT
/** \brief Exit point of function CanNm_MsgCycleTimerStart() */
#define DBG_CANNM_MSGCYCLETIMERSTART_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_MSGCYCLETIMERSTOP_ENTRY
/** \brief Entry point of function CanNm_MsgCycleTimerStop() */
#define DBG_CANNM_MSGCYCLETIMERSTOP_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_MSGCYCLETIMERSTOP_EXIT
/** \brief Exit point of function CanNm_MsgCycleTimerStop() */
#define DBG_CANNM_MSGCYCLETIMERSTOP_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HANDLERXINDICATIONCOMMON_ENTRY
/** \brief Entry point of function CanNm_HandleRxIndicationCommon() */
#define DBG_CANNM_HANDLERXINDICATIONCOMMON_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HANDLERXINDICATIONCOMMON_EXIT
/** \brief Exit point of function CanNm_HandleRxIndicationCommon() */
#define DBG_CANNM_HANDLERXINDICATIONCOMMON_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HANDLERXINDICATION_NOSTATE_RA_ENTRY
/** \brief Entry point of function CanNm_HandleRxIndication_NOState_RA() */
#define DBG_CANNM_HANDLERXINDICATION_NOSTATE_RA_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HANDLERXINDICATION_NOSTATE_RA_EXIT
/** \brief Exit point of function CanNm_HandleRxIndication_NOState_RA() */
#define DBG_CANNM_HANDLERXINDICATION_NOSTATE_RA_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HANDLERXINDICATION_NOSTATE_ENTRY
/** \brief Entry point of function CanNm_HandleRxIndication_NOState() */
#define DBG_CANNM_HANDLERXINDICATION_NOSTATE_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HANDLERXINDICATION_NOSTATE_EXIT
/** \brief Exit point of function CanNm_HandleRxIndication_NOState() */
#define DBG_CANNM_HANDLERXINDICATION_NOSTATE_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HANDLETXCONFIRMATION_ENTRY
/** \brief Entry point of function CanNm_HandleTxConfirmation() */
#define DBG_CANNM_HANDLETXCONFIRMATION_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HANDLETXCONFIRMATION_EXIT
/** \brief Exit point of function CanNm_HandleTxConfirmation() */
#define DBG_CANNM_HANDLETXCONFIRMATION_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HANDLECOMCONTROL_ENTRY
/** \brief Entry point of function CanNm_HandleComControl() */
#define DBG_CANNM_HANDLECOMCONTROL_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HANDLECOMCONTROL_EXIT
/** \brief Exit point of function CanNm_HandleComControl() */
#define DBG_CANNM_HANDLECOMCONTROL_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HANDLETRANSMIT_ENTRY
/** \brief Entry point of function CanNm_HandleTransmit() */
#define DBG_CANNM_HANDLETRANSMIT_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HANDLETRANSMIT_EXIT
/** \brief Exit point of function CanNm_HandleTransmit() */
#define DBG_CANNM_HANDLETRANSMIT_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMINIT_ENTRY
/** \brief Entry point of function CanNm_HsmInit */
#define DBG_CANNM_HSMINIT_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMINIT_EXIT
/** \brief Exit point of function CanNm_HsmInit */
#define DBG_CANNM_HSMINIT_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMINITINST_ENTRY
/** \brief Entry point of function CANNM_HSMINITINST */
#define DBG_CANNM_HSMINITINST_ENTRY([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMINITINST_EXIT
/** \brief Exit point of function CANNM_HSMINITINST */
#define DBG_CANNM_HSMINITINST_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMEMITINST_ENTRY
/** \brief Entry point of function CANNM_HSMEMITINST */
#define DBG_CANNM_HSMEMITINST_ENTRY([!"$Parameter12"!],c)
#endif

#ifndef DBG_CANNM_HSMEMITINST_EXIT
/** \brief Exit point of function CANNM_HSMEMITINST */
#define DBG_CANNM_HSMEMITINST_EXIT([!"$Parameter12"!],c)
#endif

#ifndef DBG_CANNM_HSMEMITTOSELFINST_ENTRY
/** \brief Entry point of function CANNM_HSMEMITTOSELFINST */
#define DBG_CANNM_HSMEMITTOSELFINST_ENTRY([!"$Parameter12"!],c)
#endif

#ifndef DBG_CANNM_HSMEMITTOSELFINST_EXIT
/** \brief Exit point of function CANNM_HSMEMITTOSELFINST */
#define DBG_CANNM_HSMEMITTOSELFINST_EXIT([!"$Parameter12"!],c)
#endif

#ifndef DBG_CANNM_HSMMAIN_ENTRY
/** \brief Entry point of function CanNm_HsmMain */
#define DBG_CANNM_HSMMAIN_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMMAIN_EXIT
/** \brief Exit point of function CanNm_HsmMain */
#define DBG_CANNM_HSMMAIN_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMMAININST_ENTRY
/** \brief Entry point of function CANNM_HSMMAININST */
#define DBG_CANNM_HSMMAININST_ENTRY([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMMAININST_EXIT
/** \brief Exit point of function CANNM_HSMMAININST */
#define DBG_CANNM_HSMMAININST_EXIT([!"$Parameter12"!],c)
#endif

#ifndef DBG_CANNM_HSMSETTRACING_ENTRY
/** \brief Entry point of function CanNm_HsmSetTracing */
#define DBG_CANNM_HSMSETTRACING_ENTRY(a)
#endif

#ifndef DBG_CANNM_HSMSETTRACING_EXIT
/** \brief Exit point of function CanNm_HsmSetTracing */
#define DBG_CANNM_HSMSETTRACING_EXIT(a)
#endif

#ifndef DBG_CANNM_HSMDELFROMQUEUE_ENTRY
/** \brief Entry point of function CANNM_HSMDELFROMQUEUE */
#define DBG_CANNM_HSMDELFROMQUEUE_ENTRY([!"$Parameter12"!],c)
#endif

#ifndef DBG_CANNM_HSMDELFROMQUEUE_EXIT
/** \brief Exit point of function CANNM_HSMDELFROMQUEUE */
#define DBG_CANNM_HSMDELFROMQUEUE_EXIT([!"$Parameter12"!],c)
#endif

#ifndef DBG_CANNM_HSMTRAN_ENTRY
/** \brief Entry point of function CANNM_HSMTRAN */
#define DBG_CANNM_HSMTRAN_ENTRY([!"$Parameter12"!],c)
#endif

#ifndef DBG_CANNM_HSMTRAN_EXIT
/** \brief Exit point of function CANNM_HSMTRAN */
#define DBG_CANNM_HSMTRAN_EXIT([!"$Parameter12"!],c)
#endif

#ifndef DBG_CANNM_HSMINITSUBSTATES_ENTRY
/** \brief Entry point of function CANNM_HSMINITSUBSTATES */
#define DBG_CANNM_HSMINITSUBSTATES_ENTRY([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMINITSUBSTATES_EXIT
/** \brief Exit point of function CANNM_HSMINITSUBSTATES */
#define DBG_CANNM_HSMINITSUBSTATES_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMFINDEVTODISPATCH_ENTRY
/** \brief Entry point of function CANNM_HSMFINDEVTODISPATCH */
#define DBG_CANNM_HSMFINDEVTODISPATCH_ENTRY([!"$Parameter12"!],c,d)
#endif

#ifndef DBG_CANNM_HSMFINDEVTODISPATCH_EXIT
/** \brief Exit point of function CANNM_HSMFINDEVTODISPATCH */
#define DBG_CANNM_HSMFINDEVTODISPATCH_EXIT([!"$Parameter12"!],c,d,e)
#endif

#ifndef DBG_CANNM_HSMDISPATCHEVENT_ENTRY
/** \brief Entry point of function CANNM_HSMDISPATCHEVENT */
#define DBG_CANNM_HSMDISPATCHEVENT_ENTRY([!"$Parameter12"!],c)
#endif

#ifndef DBG_CANNM_HSMDISPATCHEVENT_EXIT
/** \brief Exit point of function CANNM_HSMDISPATCHEVENT */
#define DBG_CANNM_HSMDISPATCHEVENT_EXIT([!"$Parameter12"!],c,d)
#endif

#ifndef DBG_CANNM_HSMCANNMSFTOPENTRY_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfTOPEntry */
#define DBG_CANNM_HSMCANNMSFTOPENTRY_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFTOPENTRY_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfTOPEntry */
#define DBG_CANNM_HSMCANNMSFTOPENTRY_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFBUSSLEEPMODEENTRY_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfBusSleepModeEntry */
#define DBG_CANNM_HSMCANNMSFBUSSLEEPMODEENTRY_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFBUSSLEEPMODEENTRY_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfBusSleepModeEntry */
#define DBG_CANNM_HSMCANNMSFBUSSLEEPMODEENTRY_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFBUSSLEEPMODEACTION1_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfBusSleepModeAction1 */
#define DBG_CANNM_HSMCANNMSFBUSSLEEPMODEACTION1_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFBUSSLEEPMODEACTION1_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfBusSleepModeAction1 */
#define DBG_CANNM_HSMCANNMSFBUSSLEEPMODEACTION1_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFBUSSLEEPMODEGUARD3_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfBusSleepModeGuard3 */
#define DBG_CANNM_HSMCANNMSFBUSSLEEPMODEGUARD3_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFBUSSLEEPMODEGUARD3_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfBusSleepModeGuard3 */
#define DBG_CANNM_HSMCANNMSFBUSSLEEPMODEGUARD3_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNETWORKMODEENTRY_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNetworkModeEntry */
#define DBG_CANNM_HSMCANNMSFNETWORKMODEENTRY_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNETWORKMODEENTRY_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNetworkModeEntry */
#define DBG_CANNM_HSMCANNMSFNETWORKMODEENTRY_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNETWORKMODEEXIT_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNetworkModeExit */
#define DBG_CANNM_HSMCANNMSFNETWORKMODEEXIT_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNETWORKMODEEXIT_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNetworkModeExit */
#define DBG_CANNM_HSMCANNMSFNETWORKMODEEXIT_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNETWORKMODEACTION1_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNetworkModeAction1 */
#define DBG_CANNM_HSMCANNMSFNETWORKMODEACTION1_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNETWORKMODEACTION1_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNetworkModeAction1 */
#define DBG_CANNM_HSMCANNMSFNETWORKMODEACTION1_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNETWORKMODEACTION2_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNetworkModeAction2 */
#define DBG_CANNM_HSMCANNMSFNETWORKMODEACTION2_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNETWORKMODEACTION2_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNetworkModeAction2 */
#define DBG_CANNM_HSMCANNMSFNETWORKMODEACTION2_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNETWORKMODEACTION3_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNetworkModeAction3 */
#define DBG_CANNM_HSMCANNMSFNETWORKMODEACTION3_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNETWORKMODEACTION3_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNetworkModeAction3 */
#define DBG_CANNM_HSMCANNMSFNETWORKMODEACTION3_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNETWORKMODEGUARD4_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNetworkModeGuard4 */
#define DBG_CANNM_HSMCANNMSFNETWORKMODEGUARD4_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNETWORKMODEGUARD4_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNetworkModeGuard4 */
#define DBG_CANNM_HSMCANNMSFNETWORKMODEGUARD4_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEENTRY_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfReadySleepStateEntry */
#define DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEENTRY_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEENTRY_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfReadySleepStateEntry */
#define DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEENTRY_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEGUARD1_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfReadySleepStateGuard1 */
#define DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEGUARD1_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEGUARD1_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfReadySleepStateGuard1 */
#define DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEGUARD1_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTEACTIVITYGUARD1_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfReadySleepRemoteActivityGuard1 */
#define DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTEACTIVITYGUARD1_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTEACTIVITYGUARD1_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfReadySleepRemoteActivityGuard1 */
#define DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTEACTIVITYGUARD1_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPGUARD1_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfReadySleepRemoteSleepGuard1 */
#define DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPGUARD1_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPGUARD1_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfReadySleepRemoteSleepGuard1 */
#define DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPGUARD1_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION2_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfReadySleepRemoteSleepAction2 */
#define DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION2_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION2_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfReadySleepRemoteSleepAction2 */
#define DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION2_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION3_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfReadySleepRemoteSleepAction3 */
#define DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION3_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION3_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfReadySleepRemoteSleepAction3 */
#define DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION3_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFSENDINGSUBMODEENTRY_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfSendingSubModeEntry */
#define DBG_CANNM_HSMCANNMSFSENDINGSUBMODEENTRY_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFSENDINGSUBMODEENTRY_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfSendingSubModeEntry */
#define DBG_CANNM_HSMCANNMSFSENDINGSUBMODEENTRY_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFSENDINGSUBMODEEXIT_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfSendingSubModeExit */
#define DBG_CANNM_HSMCANNMSFSENDINGSUBMODEEXIT_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFSENDINGSUBMODEEXIT_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfSendingSubModeExit */
#define DBG_CANNM_HSMCANNMSFSENDINGSUBMODEEXIT_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION1_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfSendingSubModeAction1 */
#define DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION1_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION1_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfSendingSubModeAction1 */
#define DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION1_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION2_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfSendingSubModeAction2 */
#define DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION2_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION2_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfSendingSubModeAction2 */
#define DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION2_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION3_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfSendingSubModeAction3 */
#define DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION3_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION3_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfSendingSubModeAction3 */
#define DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION3_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEENTRY_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNormalOperationStateEntry */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEENTRY_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEENTRY_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNormalOperationStateEntry */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEENTRY_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEEXIT_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNormalOperationStateExit */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEEXIT_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEEXIT_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNormalOperationStateExit */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEEXIT_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYENTRY_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNormalOperationRemoteActivityEntry */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYENTRY_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYENTRY_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNormalOperationRemoteActivityEntry */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYENTRY_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYACTION1_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNormalOperationRemoteActivityAction1 */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYACTION1_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYACTION1_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNormalOperationRemoteActivityAction1 */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYACTION1_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD2_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNormalOperationRemoteActivityGuard2 */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD2_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD2_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNormalOperationRemoteActivityGuard2 */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD2_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD4_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNormalOperationRemoteActivityGuard4 */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD4_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD4_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNormalOperationRemoteActivityGuard4 */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD4_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPENTRY_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNormalOperationRemoteSleepEntry */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPENTRY_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPENTRY_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNormalOperationRemoteSleepEntry */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPENTRY_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPGUARD1_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNormalOperationRemoteSleepGuard1 */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPGUARD1_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPGUARD1_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNormalOperationRemoteSleepGuard1 */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPGUARD1_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION2_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNormalOperationRemoteSleepAction2 */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION2_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION2_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNormalOperationRemoteSleepAction2 */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION2_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION3_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfNormalOperationRemoteSleepAction3 */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION3_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION3_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfNormalOperationRemoteSleepAction3 */
#define DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION3_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEENTRY_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfRepeatMessageStateEntry */
#define DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEENTRY_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEENTRY_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfRepeatMessageStateEntry */
#define DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEENTRY_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEEXIT_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfRepeatMessageStateExit */
#define DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEEXIT_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEEXIT_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfRepeatMessageStateExit */
#define DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEEXIT_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD1_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfRepeatMessageStateGuard1 */
#define DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD1_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD1_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfRepeatMessageStateGuard1 */
#define DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD1_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD2_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfRepeatMessageStateGuard2 */
#define DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD2_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD2_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfRepeatMessageStateGuard2 */
#define DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD2_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEENTRY_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfPrepareBusSleepModeEntry */
#define DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEENTRY_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEENTRY_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfPrepareBusSleepModeEntry */
#define DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEENTRY_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEGUARD1_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfPrepareBusSleepModeGuard1 */
#define DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEGUARD1_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEGUARD1_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfPrepareBusSleepModeGuard1 */
#define DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEGUARD1_EXIT([!"$Parameter12"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION1_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfPrepareBusSleepModeAction1 */
#define DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION1_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION1_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfPrepareBusSleepModeAction1 */
#define DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION1_EXIT([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION2_ENTRY
/** \brief Entry point of function CanNm_HsmCanNmSfPrepareBusSleepModeAction2 */
#define DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION2_ENTRY([!"$Parameter01"!])
#endif

#ifndef DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION2_EXIT
/** \brief Exit point of function CanNm_HsmCanNmSfPrepareBusSleepModeAction2 */
#define DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION2_EXIT([!"$Parameter01"!])
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[external data]=========================================*/

#endif /* (!defined CANNM_TRACE_H) */
/*==================[end of file]===========================================*/
