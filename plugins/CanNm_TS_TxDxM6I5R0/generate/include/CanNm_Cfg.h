/**
 * \file
 *
 * \brief AUTOSAR CanNm
 *
 * This file contains the implementation of the AUTOSAR
 * module CanNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
/* !LINKSTO CANNM304,1 */
#if (!defined CANNM_CFG_H)
#define CANNM_CFG_H

[!/*  select first of the multiple config containers */!][!//
[!SELECT "CanNmGlobalConfig/*[1]"!][!//

/*==================[includes]==============================================*/

#include <Std_Types.h>             /* AUTOSAR standard types */
#include <TSAutosar.h>    /* EB specific standard types      */
[!INCLUDE "CanNm_Checks.m"!][!//

/*==================[macros]================================================*/

#define CANNM_VERSION_INFO_API            [!//
[!IF "CanNmVersionInfoApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define CANNM_COM_CONTROL_ENABLED         [!//
[!IF "CanNmComControlEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define CANNM_STATE_CHANGE_IND_ENABLED    [!//
[!IF "CanNmStateChangeIndEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define CANNM_PASSIVE_MODE_ENABLED        [!//
[!IF "CanNmPassiveModeEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define CANNM_PDU_RX_INDICATION_ENABLED   [!//
[!IF "CanNmPduRxIndicationEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define CANNM_IMMEDIATE_RESTART_ENABLED   [!//
[!IF "CanNmImmediateRestartEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define CANNM_IMMEDIATE_TXCONF_ENABLED    [!//
[!IF "CanNmImmediateTxconfEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define CANNM_USER_DATA_ENABLED           [!//
[!IF "CanNmUserDataEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define CANNM_COM_USER_DATA_SUPPORT       [!//
[!IF "CanNmComUserDataSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define CANNM_REMOTE_SLEEP_IND_ENABLED    [!//
[!IF "CanNmRemoteSleepIndEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define CANNM_NODE_ID_ENABLED             [!//
[!IF "count(CanNmChannelConfig/*[CanNmPduNidPosition != 'CANNM_PDU_OFF']) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define CANNM_NODE_DETECTION_ENABLED      [!//
[!IF "CanNmNodeDetectionEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define CANNM_BUS_SYNCHRONIZATION_ENABLED [!//
[!IF "CanNmBusSynchronizationEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define CANNM_BUS_LOAD_REDUCTION_ENABLED  [!//
[!IF "CanNmBusLoadReductionEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define CANNM_REPEAT_MSG_IND_ENABLED      [!//
[!IF "CanNmRepeatMsgIndEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define CANNM_IMMEDIATE_TRANSMISSION  [!//
[!IF "num:i(count(CanNmChannelConfig/*[CanNmImmediateNmTransmissions > 0])) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Switch for DET usage */
#define CANNM_DEV_ERROR_DETECT            [!//
[!IF "CanNmDevErrorDetect = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Maximum number of channels */
#define CANNM_NUMBER_OF_CHANNELS               [!//
[!"num:i(count(CanNmChannelConfig/*))"!]U

/** \brief Number of elements in array CanNm_IndexFromComMChannelHandle[] */
#define CANNM_INDEXFROMNMCHANNELHANDLE_NUM     [!//
[!"num:i(num:max(node:refs(CanNmChannelConfig/*/CanNmComMNetworkHandleRef)/ComMChannelId)+1)"!]U

/** \brief Maximum length of User Data from all the channels */
#define CANNM_MAX_USERDATA_LENGTH              [!//
[!"num:max(as:modconf('CanNm')[1]/CanNmGlobalConfig/*[1]/CanNmChannelConfig/*/CanNmUserDataLength)"!]U

/** \brief Macro to indicate if Active wake up bit is present in CBV */
#define CANNM_ACTIVE_WAKEUP_BIT_ENABLED        [!//
[!IF "CanNmActiveWakeupBitEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/*------------------[PN Extensions]-------------------------------------*/
[!IF "count(CanNmChannelConfig/*[CanNmPnEnabled='true']) > 0"!]
/** CanNmPnInfoLength **/
#define CANNM_PN_INFO_LENGTH     [!//
              [!"CanNmPnInfo/CanNmPnInfoLength"!]U

/* Size of array for mapping PN EIRA timer */
#define CANNM_PN_EIRA_TIMER_ARR_SIZE  (CANNM_PN_INFO_LENGTH * 8U)
[!//
[!// Calculate size of array required for EIRA timer
[!IF "CanNmPnEiraCalcEnabled = 'true'"!][!//
[!NOCODE!][!//
[!VAR "EiraTimerSize" = "0"!][!//
[!LOOP "CanNmPnInfo/CanNmPnFilterMaskByte/*"!][!//
  [!FOR "BitPos" = "0" TO "7"!][!//
    [!IF "bit:getbit(CanNmPnFilterMaskByteValue, $BitPos) = 'true'"!][!//
      [!// Update Eira timer array size if PN bit is set in 'CanNmPnFilterMaskByteValue'
      [!VAR "EiraTimerSize" = "$EiraTimerSize + 1"!][!//
    [!ENDIF!][!//
  [!ENDFOR!][!//
[!ENDLOOP!][!//
[!ENDNOCODE!][!//
#define CANNM_EIRA_TIMER_SIZE    [!"num:i($EiraTimerSize)"!]U  /* CanNm Eira Timer array size */
[!ENDIF!]
/** CanNmPnInfoOffset **/
#define CANNM_PN_INFO_OFFSET     [!//
              [!"CanNmPnInfo/CanNmPnInfoOffset"!]U

/** CanNmPnEiraCalcEnabled **/
#define CANNM_PN_EIRA_CALC_ENABLED             [!//
[!IF "CanNmPnEiraCalcEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** CanNmPnResetTime **/
#define CANNM_PN_RESET_TIME [!//
    [!"num:i((CanNmPnResetTime * 1000) div (CanNmMainFunctionPeriod * 1000))"!]U

[!/* get Eira PduId by EcuC reference from PduR, existence is checked in xdm */!][!//
[!IF "CanNmPnEiraCalcEnabled = 'true'"!][!//
#define CANNM_PN_EIRA_PDUID  [!//
    [!"as:modconf('PduR')[1]/PduRRoutingTables/*/PduRRoutingTable/*/PduRRoutingPath/*/PduRSrcPdu[PduRSrcPduRef=node:current()/CanNmPnEiraRxNSduRef
]/PduRSourcePduHandleId"!]U/* CanNmPnEraRx Id*/
[!ENDIF!]

#define CANNM_PN_ENABLED  STD_ON
[!ELSE!]
#define CANNM_PN_ENABLED  STD_OFF
#define CANNM_PN_EIRA_CALC_ENABLED STD_OFF
[!ENDIF!]

/*------------------[Symbolic Name for CanNmRxPduId]------------------------*/
[!LOOP "CanNmChannelConfig/*/CanNmRxPdu"!][!/*
*/!]
/* It is possible to change SHORT-NAME of containers with multiplicity
 * of one within EB tresos Studio. So, the symbolic name generation shall be
 * according to requirement [ecuc_sws_2108] if a SHORT-NAME for the container
 * is specified and the macro is defined as follows:
 *   #define CanNmConf_CanNmRxPdu_<CTR_SHORT_NAME>
 *
 * If a SHORT-NAME is not specified the macro is defined as follows:
 *   #define CanNmConf_CanNmChannelConfig_<CHANNEL_NAME>_<CTR_NAME>
 */
[!CALL "GetSymbolName","ShortNameRef"="'.'"!]
#if (defined [!"$SymbolName"!])
#error [!"$SymbolName"!] already defined
#endif

/** \brief Symbolic name for the CanNm channel "[!"name(..)"!]" */
#define [!"$SymbolName"!]  [!"CanNmRxPduId"!]U

#if (!defined CANNM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(..)"!]_[!"name(.)"!])
#error [!"name(..)"!]_[!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) [!"CanNmRxPduId"!] */
#define [!"name(..)"!]_[!"name(.)"!]  [!"CanNmRxPduId"!]U

#if (defined CanNm_[!"name(..)"!]_[!"name(.)"!])
#error CanNm_[!"name(..)"!]_[!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define CanNm_[!"name(..)"!]_[!"name(.)"!]  [!"CanNmRxPduId"!]U
#endif /* CANNM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//

/*------------[Symbolic Name for CanNmTxConfirmationPduId]-------------------*/
[!LOOP "CanNmChannelConfig/*/CanNmTxPdu"!][!/*
*/!]
/* It is possible to change short name values of containers with multiplicity
 * of one within EB tresos Studio. So, the symbolic name generation shall be
 * according to requirement [ecuc_sws_2108] if a SHORT-NAME for the container
 * is specified and the macro is defined as follows:
 *   #define CanNmConf_CanNmTxPdu_<CTR_SHORT_NAME>
 *
 * If a SHORT-NAME is not specified the macro is defined as follows:
 *   #define CanNmConf_CanNmChannelConfig_<CHANNEL_NAME>_<CTR_NAME>
 */
[!/* The LOOP over node-set excludes OPTIONAL containers from processing thus
   * the symbolic name macros should just be generated in case of their presence.
   */!]
[!CALL "GetSymbolName","ShortNameRef"="'.'""!]
#if (defined [!"$SymbolName"!])
#error [!"$SymbolName"!] already defined
#endif

/** \brief Symbolic name for the CanNm channel "[!"name(..)"!]" */
#define [!"$SymbolName"!]  [!"CanNmTxConfirmationPduId"!]U

#if (!defined CANNM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(..)"!]_[!"name(.)"!])
#error [!"name(..)"!]_[!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) [!"CanNmTxConfirmationPduId"!] */
#define [!"name(..)"!]_[!"name(.)"!]  [!"CanNmTxConfirmationPduId"!]U

#if (defined CanNm_[!"name(..)"!]_[!"name(.)"!])
#error CanNm_[!"name(..)"!]_[!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define CanNm_[!"name(..)"!]_[!"name(.)"!]  [!"CanNmTxConfirmationPduId"!]U
#endif /* CANNM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//

/*-------------[Symbolic Name for CanNmTxUserDataPduId]--------------------*/
[!LOOP "CanNmChannelConfig/*/CanNmUserDataTxPdu"!][!/*
*/!]
/* It is possible to change short name values of containers with multiplicity
 * of one within EB tresos Studio. So, the symbolic name generation shall be
 * according to requirement [ecuc_sws_2108] if a SHORT-NAME for the container
 * is specified and the macro is defined as follows:
 *   #define CanNmConf_CanNmUserDataTxPdu_<CTR_SHORT_NAME>
 *
 * If a SHORT-NAME is not specified the macro is defined as follows:
 *   #define CanNmConf_CanNmChannelConfig_<CHANNEL_NAME>_<CTR_NAME>
 */
[!/* The LOOP over node-set excludes OPTIONAL containers from processing thus
   * the symbolic name macros should just be generated in case of their presence.
   */!]
[!CALL "GetSymbolName","ShortNameRef"="'.'""!]
#if (defined [!"$SymbolName"!])
#error [!"$SymbolName"!] already defined
#endif

/** \brief Symbolic name for the CanNm channel "[!"name(..)"!]" */
#define [!"$SymbolName"!]  [!"CanNmTxUserDataPduId"!]U

#if (!defined CANNM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(..)"!]_[!"name(.)"!])
#error [!"name(..)"!]_[!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) [!"CanNmTxUserDataPduId"!] */
#define [!"name(..)"!]_[!"name(.)"!]  [!"CanNmTxUserDataPduId"!]U

#if (defined CanNm_[!"name(..)"!]_[!"name(.)"!])
#error CanNm_[!"name(..)"!]_[!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define CanNm_[!"name(..)"!]_[!"name(.)"!]  [!"CanNmTxUserDataPduId"!]U
#endif /* CANNM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//

/*------------------[Hsm configuration]-------------------------------------*/

#if (CANNM_NUMBER_OF_CHANNELS > 1U)
#define CANNM_HSM_INST_MULTI_ENABLED STD_ON
#else
#define CANNM_HSM_INST_MULTI_ENABLED STD_OFF
#endif

/*==================[type definitions]======================================*/

/** \brief Type for the module config
 **
 ** This type describes the config data of the module.
 **
 ** This CanNm implementation does not support link or post build time
 ** configuration.  Therefore the argument of the init function is ignored and
 ** this data type is defined only for compatibility. */
typedef uint8 CanNm_ConfigType;

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

#define CANNM_START_SEC_CONST_8
#include <MemMap.h>

extern CONST(CanNm_ConfigType, CANNM_CONST) [!"name(.)"!];

[!IF "CanNmPnEiraCalcEnabled = 'true'"!][!//
/* Mapping between Eira Timer array to Configured PNs through
 * CanNm_EiraTimerMap array */
extern CONST(uint8, CANNM_CONST) CanNm_EiraTimerMap[CANNM_PN_EIRA_TIMER_ARR_SIZE];
[!ENDIF!][!//

#define CANNM_STOP_SEC_CONST_8
#include <MemMap.h>

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

[!ENDSELECT!][!//
#endif /* #if (!defined CANNM_CFG_H) */
/*==================[end of file]===========================================*/
