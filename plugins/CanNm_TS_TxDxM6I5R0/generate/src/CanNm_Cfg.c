/**
 * \file
 *
 * \brief AUTOSAR CanNm
 *
 * This file contains the implementation of the AUTOSAR
 * module CanNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!AUTOSPACING!][!//

/*==================[inclusions]============================================*/
[!INCLUDE "../include/CanNm_Checks.m"!][!//

#include <Std_Types.h>        /* AUTOSAR standard types */

#include <CanNm.h>            /* CanNm API definitions (own interface) */
#include <CanNm_Int.h>        /* CanNm internal dependent information */

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[internal data]=========================================*/

[!/*  select first of the multiple config containers */!][!//
[!SELECT "CanNmGlobalConfig/*[1]"!][!//

#define CANNM_START_SEC_VAR_NO_INIT_8
#include <MemMap.h>

[!LOOP "node:order(CanNmChannelConfig/*,'CanNmRxPdu/CanNmRxPduId')"!][!//
STATIC VAR(uint8, AUTOSAR_COMSTACKDATA)
  CanNm_RxPdu[!"name(.)"!][[!"as:ref(CanNmRxPdu/CanNmRxPduRef)/PduLength"!]U];
[!IF "../../CanNmPassiveModeEnabled = 'false'"!][!//
STATIC VAR(uint8, AUTOSAR_COMSTACKDATA)
  CanNm_TxPdu[!"name(.)"!][[!"as:ref(CanNmRxPdu/CanNmRxPduRef)/PduLength"!]U];
[!ENDIF!][!//
[!ENDLOOP!][!//

#define CANNM_STOP_SEC_VAR_NO_INIT_8
#include <MemMap.h>

/*==================[internal constants]====================================*/

/*==================[external constants]====================================*/
#define CANNM_START_SEC_CONST_8
#include <MemMap.h>

CONST(CanNm_ConfigType, CANNM_CONST) [!"name(.)"!] = 0U;

[!IF "count(CanNmChannelConfig/*[CanNmPnEnabled='true']) > 0"!]
[!SELECT "CanNmPnInfo"!][!//
CONST(uint8, CANNM_CONST) CanNm_PnFilterMask[[!"CanNmPnInfoLength"!]U] =
{
[!FOR "bytePos"="0" TO "CanNmPnInfoLength - 1"!]
[!"num:inttohex(CanNmPnFilterMaskByte/*[CanNmPnFilterMaskByteIndex = $bytePos]/CanNmPnFilterMaskByteValue)"!]U[!//
,[!//
[!ENDFOR!]
};
[!ENDSELECT!][!//
[!ENDIF!]
[!//
[!INDENT "0"!]
[!IF "CanNmPnEiraCalcEnabled = 'true'"!][!//
  [!VAR "Index" = "0"!]
  /* Mapping between PN Index and EIRA timer array */
  CONST(uint8, CANNM_CONST) CanNm_EiraTimerMap[CANNM_PN_EIRA_TIMER_ARR_SIZE] =
  {
    [!LOOP "CanNmPnInfo/CanNmPnFilterMaskByte/*"!][!//
      [!FOR "BitPos" = "0" TO "7"!][!//
        [!IF "bit:getbit(CanNmPnFilterMaskByteValue, $BitPos) = 'true'"!][!//
          [!WS "2"!][!"num:i($Index)"!]U,
          [!VAR "Index" = "$Index + 1"!][!//
        [!ELSE!][!//
          [!WS "2"!]CANNM_EIRA_TIMER_INVALID,
        [!ENDIF!][!//
      [!ENDFOR!][!//
    [!ENDLOOP!]
  };
[!ENDIF!]
[!ENDINDENT!]

#define CANNM_STOP_SEC_CONST_8
#include <MemMap.h>

#define CANNM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

[!// Channel Configuration
/* !LINKSTO CANNM299,1 */
CONST(CanNm_ChanConfigType, CANNM_CONST)
  CanNm_ChanConfig[CANNM_NUMBER_OF_CHANNELS] =
{
[!LOOP "node:order(CanNmChannelConfig/*,'CanNmRxPdu/CanNmRxPduId')"!][!//
  /* Configuration of CAN NM channel [!"name(.)"!] with RxPduId [!"CanNmRxPdu/CanNmRxPduId"!] : */
  {
    CanNm_RxPdu[!"name(.)"!], /* buffer for PDU reception */
[!IF "../../CanNmPassiveModeEnabled = 'false'"!][!//
    CanNm_TxPdu[!"name(.)"!], /* buffer for PDU transmission */
[!ENDIF!]
    /* !LINKSTO CANNM246,1 */
    [!"num:i((CanNmTimeoutTime * 1000) div (../../CanNmMainFunctionPeriod * 1000))"!], /* Nm Timeout Time */
    /* !LINKSTO CANNM247,1 */
    [!"num:i((CanNmRepeatMessageTime * 1000) div (../../CanNmMainFunctionPeriod * 1000))"!], /* Repeat Message Time */
    /* !LINKSTO CANNM248,1 */
    [!"num:i((CanNmWaitBusSleepTime * 1000) div (../../CanNmMainFunctionPeriod * 1000))"!], /* Wait Bus Sleep Time */
    /* !LINKSTO CANNM249,1 */
    [!VAR "CanNmRemoteSleepIndTimeVar" = "0"!]
    [!IF "../../CanNmRemoteSleepIndEnabled = 'true'"!]
      [!VAR "CanNmRemoteSleepIndTimeVar" = "CanNmRemoteSleepIndTime"!]
    [!ENDIF!]
    [!"num:i(($CanNmRemoteSleepIndTimeVar * 1000) div (../../CanNmMainFunctionPeriod * 1000))"!]U, /* Remote Sleep Indication Time */
[!IF "../../CanNmPassiveModeEnabled = 'false'"!][!//
    [!"CanNmImmediateNmTransmissions"!]U, /* Immediate Nm Transmissions count */
    [!"num:i((CanNmImmediateNmCycleTime * 1000) div (../../CanNmMainFunctionPeriod * 1000))"!]U, /* Immediate Nm Cycle Time */
    [!"num:i((CanNmMsgCycleTime * 1000) div (../../CanNmMainFunctionPeriod * 1000))"!]U, /* Msg Cycle Time */
    [!"num:i((CanNmMsgCycleOffset * 1000) div (../../CanNmMainFunctionPeriod * 1000))"!]U, /* Msg Cycle Offset */
[!IF "../../CanNmImmediateTxconfEnabled = 'false'"!][!//
    [!"num:i((CanNmMsgTimeoutTime * 1000) div (../../CanNmMainFunctionPeriod * 1000))"!]U, /* Msg Timeout Time */
[!ENDIF!][!//
[!IF "../../CanNmBusLoadReductionEnabled = 'true'"!][!//
[!IF "CanNmBusLoadReductionActive = 'true'"!][!//
    [!"num:i((CanNmMsgReducedTime * 1000) div (../../CanNmMainFunctionPeriod * 1000)) "!]U, /* Msg Reduced Time */
[!ELSE!][!//
    0U, /* Msg Reduced Time */
[!ENDIF!][!//
[!ENDIF!][!// Bus Load Reduction enabled
[!ENDIF!][!// Passive mode not enabled
    [!"as:ref(CanNmRxPdu/CanNmRxPduRef)/PduLength"!]U, /* PDU Length */
[!IF "../../CanNmPassiveModeEnabled = 'false'"!][!//
    [!/* get Tx PduId by EcuC reference from CanIf, existence is checked in xdm */!][!//
    [!"as:modconf('CanIf')[1]/CanIfInitCfg/*/CanIfTxPduCfg/*[
       CanIfTxPduRef = node:current()/CanNmTxPdu/CanNmTxPduRef]/CanIfTxPduId"!]U, /* Tx PduId */
    [!"CanNmTxPdu/CanNmTxConfirmationPduId"!]U, /* Tx Confirmation PduId */
[!ENDIF!][!// Passive mode not enabled
[!IF "../../CanNmNodeDetectionEnabled = 'true' and ../../CanNmRepeatMsgIndEnabled = 'true'"!][!//
    [!"CanNmRxPdu/CanNmRxPduId"!]U, /* Rx PduId */
[!ENDIF!]
[!IF "../../CanNmComUserDataSupport = 'true'"!]
[!IF "node:exists(CanNmUserDataTxPdu)"!]
    [!"CanNmUserDataTxPdu/CanNmTxUserDataPduId"!]U, /* User Data Tx PduId */[!/*
    Get User Data TxConf PduId by EcuC reference from PduR;
    existence is already checked in CanNm_Checks.m.
    */!]
    [!"as:modconf('PduR')[1]/PduRRoutingTables/*/PduRRoutingTable/*/PduRRoutingPath/*/
       PduRDestPdu/*[PduRDestPduRef=node:current()/CanNmUserDataTxPdu/CanNmTxUserDataPduRef]/
       PduRDestPduHandleId"!]U, /* User Data Tx Confirmation PduId */
[!ELSE!][!//
    0U, /* User Data Tx PduId */
    0U, /* User Data Tx Confirmation PduId */
[!ENDIF!][!//
[!IF "node:exists(CanNmUserDataRxPdu)"!][!/*
    Get Rx PduId by EcuC reference from PduR, existence is already checked in CanNm_Checks.m
    */!][!//
    [!"as:modconf('PduR')[1]/PduRRoutingTables/*/PduRRoutingTable/*/PduRRoutingPath/*/
       PduRSrcPdu[PduRSrcPduRef=node:current()/CanNmUserDataRxPdu/CanNmRxUserDataPduRef]/
       PduRSourcePduHandleId"!]U, /* User Data Rx PduId */
[!ELSE!][!//
    0U, /* User Data Rx PduId */
[!ENDIF!][!//
[!ENDIF!][!// UserDataSupport enabled
    [!"num:i(as:ref(CanNmComMNetworkHandleRef)/ComMChannelId)"!]U, /* ComMChannelId */
[!IF "count(../../CanNmChannelConfig/*[CanNmPduNidPosition != 'CANNM_PDU_OFF']) > 0"!][!//
    [!"CanNmNodeId"!]U, /* Node Id */
[!ENDIF!][!// Node ID enabled
[!IF "CanNmBusLoadReductionActive = 'true'"!][!//
    CANNM_BITPOS2MASK(CANNM_BUSLOADREDACTIVE), /* Channel Config Flags */
[!ELSE!][!//
    0U, /* Channel Config Flags */
[!ENDIF!][!//
    [!"CanNmUserDataLength"!]U, /* User Data Length */
    [!"CanNmPduNidPosition"!], /* Nid Position */
    [!"CanNmPduCbvPosition"!], /* Cbv Position */
[!IF "../../CanNmUserDataEnabled = 'false'"!][!//
    CANNM_PDU_OFF, /* UserData first byte position */
[!ELSEIF "(CanNmPduNidPosition = 'CANNM_PDU_BYTE_1') or (CanNmPduCbvPosition = 'CANNM_PDU_BYTE_1')"!][!//
    2U,            /* UserData first byte position */
[!ELSEIF "(CanNmPduNidPosition = 'CANNM_PDU_BYTE_0') or (CanNmPduCbvPosition = 'CANNM_PDU_BYTE_0')"!][!//
    1U,            /* UserData first byte position */
[!ELSE!][!//
    0U,            /* UserData first byte position */
[!ENDIF!][!//
[!IF "CanNmAllNmMessagesKeepAwake = 'true'"!][!//
    TRUE, /* AllNmMessagesKeepAwake */
[!ELSE!][!//
    FALSE, /* AllNmMessagesKeepAwake */
[!ENDIF!][!//
[!IF "CanNmPnEnabled = 'true'"!][!//
    TRUE, /* PnEnabled */
[!ELSE!][!//
    FALSE, /* PnEnabled */
[!ENDIF!][!//
[!IF "CanNmPnHandleMultipleNetworkRequests = 'true'"!][!//
    TRUE, /* PnHandleMultipleNetworkRequests */
[!ELSE!][!//
    FALSE, /* PnHandleMultipleNetworkRequests */
[!ENDIF!][!//
[!IF "../../CanNmComUserDataSupport = 'true'"!]
[!IF "node:exists(CanNmUserDataRxPdu)"!]
    TRUE,  /* UserRxPduEnabled */
[!ELSE!][!//
    FALSE, /* UserRxPduEnabled */
[!ENDIF!][!//
[!ENDIF!][!// UserDataSupport enabled
[!IF "CanNmRetryFirstMessageRequest = 'true'"!]
    TRUE  /* RetryFirstMessageRequest */
[!ELSE!][!//
    FALSE /* RetryFirstMessageRequest */
[!ENDIF!][!// RetryFirstMessageRequest enabled
  },
[!ENDLOOP!][!//
};

CONST(NetworkHandleType, CANNM_CONST)
  CanNm_IndexFromNmChannelHandle[CANNM_INDEXFROMNMCHANNELHANDLE_NUM] =
{
[!FOR "ComMChannelId"="0" TO "num:i(num:max(node:refs(CanNmChannelConfig/*/CanNmComMNetworkHandleRef)/ComMChannelId))"!][!//
[!IF "count(CanNmChannelConfig/*[node:ref(CanNmComMNetworkHandleRef)/ComMChannelId = $ComMChannelId]) > 0"!][!//
[!SELECT "CanNmChannelConfig/*[node:ref(CanNmComMNetworkHandleRef)/ComMChannelId = $ComMChannelId]"!][!//
  [!"CanNmRxPdu/CanNmRxPduId"!]U, [!//
/* RxPduId of channel "[!"name(.)"!]" with ComMChannelHandle [!"$ComMChannelId"!] */
[!ENDSELECT!][!//
[!ELSE!][!//
  0xFFU, /* no CanNm channel for ComMChannelHandle [!"$ComMChannelId"!] */
[!ENDIF!][!//
[!ENDFOR!]
};

#define CANNM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[external data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

[!ENDSELECT!][!//
/*==================[end of file]===========================================*/
